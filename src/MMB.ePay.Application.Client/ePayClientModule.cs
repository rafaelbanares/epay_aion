﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace MMB.ePay
{
    public class ePayClientModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ePayClientModule).GetAssembly());
        }
    }
}
