﻿namespace AionWindowServiceUpload
{
    public static class AppSettings
    {
        public static IConfiguration Configuration { get; set; }
        public static string ServerUrlBase { get; set; }
        public static int TenantId { get; set; }
        public static string TenancyName { get; set; }
        public static string FilePath { get; set; }
        public static string ClientId { get; set; }
        public static string ClientSecret { get; set; }
        public static string AllowedScopes { get; set; }
        public static string Password { get; set; }
        public static string Salt { get; set; }

    }
}
