﻿using AionWindowServiceUpload.Dtos;

namespace AionWindowServiceUpload.Interface
{
    public interface IRawTimeLogStagingAppService
    {
        Task<int> AutomatedUpload();
    }
}
