﻿using Abp.Runtime.Security;
using Abp.UI;
using AionWindowServiceUpload.Dtos;
using AionWindowServiceUpload.Interface;
using IdentityModel.Client;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using System.Globalization;
using System.Text;

namespace AionWindowServiceUpload.Services
{
    public class RawTimeLogStagingAppService : IRawTimeLogStagingAppService
    {
        private readonly ILogger<Worker> _logger;
        private readonly string _uploadPath = $"{AppSettings.FilePath}upload\\";
        private readonly string _errorPath = $"{AppSettings.FilePath}error\\";

        public RawTimeLogStagingAppService(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        protected async Task<bool> CreateAttendanceUploads(List<AttendanceUploadsDto> input)
        {
            var attendanceUploads = new List<AttendanceUploadsDto>();

            input.ForEach(attendanceUpload =>
            {
                attendanceUploads.Add(new AttendanceUploadsDto
                {
                    EmployeeCode = attendanceUpload.EmployeeCode,
                    Date = attendanceUpload.Date,
                    TimeIn = attendanceUpload.TimeIn,
                    LunchBreaktimeIn = attendanceUpload.LunchBreaktimeIn,
                    LunchBreaktimeOut = attendanceUpload.LunchBreaktimeOut,
                    PMBreaktimeIn = attendanceUpload.PMBreaktimeIn,
                    PMBreaktimeOut = attendanceUpload.PMBreaktimeOut,
                    TimeOut = attendanceUpload.TimeOut
                });
            });

            using var client = new HttpClient();
            var accessToken = await GetAccessTokenByPasswordAsync();

            if (string.IsNullOrEmpty(accessToken))
            {
                return false;
            }
            else
            {
                client.SetBearerToken(accessToken);

                for (var i = 0; i < attendanceUploads.Count; i += 100)
                {
                    var currentAttendanceUploads = attendanceUploads.Skip(i).Take(Math.Min(100, attendanceUploads.Count - i));
                    var json = JsonConvert.SerializeObject(new CreateAttendanceUploadsDto
                    {
                        AttendanceUploads = currentAttendanceUploads.ToList()
                    });

                    var jsonContent = new StringContent(json, Encoding.UTF8, "application/json");

                    var response = await client.PostAsync(
                        $"{AppSettings.ServerUrlBase}api/services/app/attendances/uploadAttendanceUploads",
                        jsonContent);

                    if (!response.IsSuccessStatusCode)
                    {
                        _logger.LogInformation("Error: " + response.StatusCode.ToString());
                        return false;
                    }
                }

                return true;
            }
        }

        protected async Task<bool> CreateRawTimeLogStaging(List<RawTimeLogStagingDto> input)
        {
            var rawTimeLogs = new List<RawTimeLogStagingDto>();

            input.ForEach(rawTimeLog =>
            {
                rawTimeLogs.Add(new RawTimeLogStagingDto
                {
                    SwipeCode = rawTimeLog.SwipeCode,
                    LogTime = rawTimeLog.LogTime,
                    InOut = rawTimeLog.InOut
                });
            });

            using var client = new HttpClient();
            var accessToken = await GetAccessTokenByPasswordAsync();

            if (string.IsNullOrEmpty(accessToken))
            {
                return false;
            }
            else
            {
                client.SetBearerToken(accessToken);

                for (var i = 0; i < rawTimeLogs.Count; i += 100)
                {
                    var currentrawTimeLogs = rawTimeLogs.Skip(i).Take(Math.Min(100, rawTimeLogs.Count - i));
                    var json = JsonConvert.SerializeObject(new CreateRawTimeLogStagingDto
                    {
                        RawTimeLogStaging = currentrawTimeLogs.ToList(),
                        UploadSessionId = Guid.NewGuid()
                    });

                    var jsonContent = new StringContent(json, Encoding.UTF8, "application/json");

                    var response = await client.PostAsync(
                        $"{AppSettings.ServerUrlBase}api/services/app/attendances/uploadRawTimeLogStaging",
                        jsonContent);

                    if (!response.IsSuccessStatusCode)
                    {
                        _logger.LogInformation("Error: " + response.StatusCode.ToString());
                        return false;
                    }
                }

                return true;
            }
        }

        protected async Task<string> GetAccessTokenByPasswordAsync()
        {
            var client = new HttpClient();

            var disco = await client.GetDiscoveryDocumentAsync(AppSettings.ServerUrlBase);
            if (disco.IsError)
            {
                throw new Exception(disco.Error);
            }

            client.DefaultRequestHeaders.Add("Abp.TenantId", AppSettings.TenantId.ToString());  //Set TenantId

            var salt = Encoding.ASCII.GetBytes(AppSettings.Salt);
            var tokenResponse = await client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = AppSettings.ClientId,
                ClientSecret = AppSettings.ClientSecret,
                Scope = AppSettings.AllowedScopes,
                UserName = "admin-upload",
                Password = SimpleStringCipher.Instance.Decrypt(AppSettings.Password, "!L3t$Cr3ateSecur3P@$$phr@s3s!", salt)
            });

            if (tokenResponse.IsError)
            {
                _logger.LogInformation("Error: " + tokenResponse.Error);
                return string.Empty;
            }
            //else
            //{
            //    _logger.LogInformation(tokenResponse.Json.ToString());
            //}

            return tokenResponse.AccessToken;
        }

        public async Task<int> AutomatedUpload()
        {
            var tenancyName = AppSettings.TenancyName;
            var filePaths = Directory.EnumerateFiles(AppSettings.FilePath).ToArray();

            new FileInfo(_uploadPath).Directory?.Create();
            new FileInfo(_errorPath).Directory?.Create();

            return tenancyName switch
            {
                "Tobys" => await UploadTobys(filePaths),
                "Taisho" => await UploadTaisho(filePaths),
                _ => UploadDefault()
            };
        }

        private static void FileMove(FileMoveDto dto)
        {
            if (dto.IsSuccessful)
            {
                File.Move(dto.FilePath, dto.UploadPath + dto.FileName);
            }
            else
            {
                File.Move(dto.FilePath, dto.ErrorPath + dto.FileName);
            }
        }

        protected static int UploadDefault()
        {
            return 0;
        }

        protected async Task<int> UploadTobys(string[] filePaths)
        {
            var totalCount = 0;

            foreach (var filePath in filePaths)
            {
                var fileInfo = new FileInfo(filePath);

                if (fileInfo.Extension == ".xls")
                {
                    totalCount += await UploadTobysFormat1(filePath);
                }
                else if (fileInfo.Extension == ".xlsx")
                {
                    totalCount += await UploadTobysFormat2(filePath);
                }
            }

            return totalCount;
        }

        protected async Task<int> UploadTobysFormat1(string filePath)
        {
            var input = new List<RawTimeLogStagingDto>();
            var totalCount = 0;

            var isSuccessful = true;
            var fileInfo = new FileInfo(filePath);

            try
            {
                using var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                var stream = fileStream as Stream;

                var workbook = new HSSFWorkbook(stream);
                if (workbook.NumberOfSheets == 0)
                {
                    throw new Exception("File is empty.");
                }

                var worksheet = workbook.GetSheetAt(0);
                var headerRow = worksheet.GetRow(0);
                var header = new string[]
                {
                    "UserID", "EmployeeCode", "Name", "Dept.", "Att_Time"
                };

                var firstRow = new string[]
                {
                    headerRow.GetCell(0).StringCellValue,
                    headerRow.GetCell(1).StringCellValue,
                    headerRow.GetCell(2).StringCellValue,
                    headerRow.GetCell(3).StringCellValue,
                    headerRow.GetCell(4).StringCellValue
                };

                if (!header.SequenceEqual(firstRow))
                {
                    throw new Exception("File is not in the correct format.");
                }

                for (int row = 1; row <= worksheet.LastRowNum; row++)
                {
                    var worksheetRow = worksheet.GetRow(row);

                    input.Add(new RawTimeLogStagingDto
                    {
                        SwipeCode = worksheetRow.GetCell(0).StringCellValue,
                        LogTime = DateTime.Parse(worksheetRow.GetCell(4).StringCellValue),
                        InOut = "I"
                    });
                }

                var success = await CreateRawTimeLogStaging(input);
                if (!success)
                {
                    isSuccessful = false;
                }

                totalCount += input.Count;
            }
            catch (Exception ex)
            {
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(filePath);

                using var stream = File.Create(_errorPath + fileNameWithoutExtension + "_error.txt");
                byte[] errorMessage = new UTF8Encoding(true).GetBytes(ex.Message);
                stream.Write(errorMessage, 0, errorMessage.Length);

                isSuccessful = false;
            }

            FileMove(new FileMoveDto
            {
                IsSuccessful = isSuccessful,
                FilePath = filePath,
                UploadPath = _uploadPath,
                ErrorPath = _errorPath,
                FileName = fileInfo.Name
            });

            return totalCount;
        }

        protected async Task<int> UploadTobysFormat2(string filePath)
        {
            var input = new List<AttendanceUploadsDto>();
            var totalCount = 0;

            var isSuccessful = true;
            var fileInfo = new FileInfo(filePath);

            try
            {
                using var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                var stream = fileStream as Stream;

                var workbook = new XSSFWorkbook(stream);
                if (workbook.NumberOfSheets == 0)
                {
                    throw new Exception("File is empty.");
                }

                var worksheet = workbook.GetSheetAt(0);
                var headerRow = worksheet.GetRow(0);
                var header = new string[]
                {
                    "USER ID", "EMPLOYEE CODE", "NAME", "DEPT", "DATE", "TIME IN", "LUNCH OUT", "LUNCH IN",
                    "DURATION", "BREAK OUT", "BREAK IN", "DURATION", "TIME OUT", "DURATION"
                };

                var firstRow = new string[]
                {
                    headerRow.GetCell(0).StringCellValue.Trim(),
                    headerRow.GetCell(1).StringCellValue.Trim(),
                    headerRow.GetCell(2).StringCellValue.Trim(),
                    headerRow.GetCell(3).StringCellValue.Trim(),
                    headerRow.GetCell(4).StringCellValue.Trim(),
                    headerRow.GetCell(5).StringCellValue.Trim(),
                    headerRow.GetCell(6).StringCellValue.Trim(),
                    headerRow.GetCell(7).StringCellValue.Trim(),
                    headerRow.GetCell(8).StringCellValue.Trim(),
                    headerRow.GetCell(9).StringCellValue.Trim(),
                    headerRow.GetCell(10).StringCellValue.Trim(),
                    headerRow.GetCell(11).StringCellValue.Trim(),
                    headerRow.GetCell(12).StringCellValue.Trim(),
                    headerRow.GetCell(13).StringCellValue.Trim()
                };

                if (!header.SequenceEqual(firstRow))
                {
                    throw new Exception("File is not in the correct format.");
                }

                for (int row = 1; row <= worksheet.LastRowNum; row++)
                {
                    var worksheetRow = worksheet.GetRow(row);

                    input.Add(new AttendanceUploadsDto
                    {
                        EmployeeCode = worksheetRow.GetCell(1).StringCellValue,
                        Date = DateTime.Parse(worksheetRow.GetCell(4).StringCellValue), //change to exact
                        TimeIn = worksheetRow.GetCell(5).StringCellValue,
                        LunchBreaktimeIn = worksheetRow.GetCell(6).StringCellValue,
                        LunchBreaktimeOut = worksheetRow.GetCell(7).StringCellValue,
                        PMBreaktimeIn = worksheetRow.GetCell(9).StringCellValue,
                        PMBreaktimeOut = worksheetRow.GetCell(10).StringCellValue,
                        TimeOut = worksheetRow.GetCell(12).StringCellValue
                    });
                }

                var success = await CreateAttendanceUploads(input);
                if (!success)
                {
                    isSuccessful = false;
                }

                totalCount += input.Count;
            }
            catch (Exception ex)
            {
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(filePath);

                using var stream = File.Create(_errorPath + fileNameWithoutExtension + "_error.txt");
                byte[] errorMessage = new UTF8Encoding(true).GetBytes(ex.Message);
                stream.Write(errorMessage, 0, errorMessage.Length);

                isSuccessful = false;
            }

            FileMove(new FileMoveDto
            {
                IsSuccessful = isSuccessful,
                FilePath = filePath,
                UploadPath = _uploadPath,
                ErrorPath = _errorPath,
                FileName = fileInfo.Name
            });

            return totalCount;
        }

        protected async Task<int> UploadTaisho(string[] filePaths)
        {
            var input = new List<RawTimeLogStagingDto>();
            var totalCount = 0;

            var isSuccessful = true;

            foreach (var filePath in filePaths)
            {
                var fileInfo = new FileInfo(filePath);

                try
                {
                    using var stream = new StreamReader(filePath);
                    while (stream.Peek() >= 0)
                    {
                        var record = await stream.ReadLineAsync();
                        var cols = record?.Split("|");

                        if (cols?.Length == 4)
                        {
                            var swipeCode = cols[0];
                            var logTime = string.Format("{0} {1}", cols[1], cols[2]);
                            var inOut = cols[3];

                            input.Add(new RawTimeLogStagingDto
                            {
                                SwipeCode = swipeCode.TrimEnd(),
                                LogTime = DateTime.ParseExact(logTime, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                                InOut = inOut == "1" ? "I" : "O"
                            });
                        }
                    }

                    var success = await CreateRawTimeLogStaging(input);
                    if (!success)
                    {
                        isSuccessful = false;
                    }

                    totalCount += input.Count;
                }
                catch (Exception ex)
                {
                    var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(filePath);

                    using var stream = File.Create(_errorPath + fileNameWithoutExtension + "_error.txt");
                    byte[] errorMessage = new UTF8Encoding(true).GetBytes(ex.Message);
                    stream.Write(errorMessage, 0, errorMessage.Length);

                    isSuccessful = false;
                }

                FileMove(new FileMoveDto
                {
                    IsSuccessful = isSuccessful,
                    FilePath = filePath,
                    UploadPath = _uploadPath,
                    ErrorPath = _errorPath,
                    FileName = fileInfo.Name
                });

                //await Task.Delay(TimeSpan.FromMinutes(1));
            }

            return totalCount;
        }
    }
}
