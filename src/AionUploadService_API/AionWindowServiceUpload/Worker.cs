using AionWindowServiceUpload.Interface;
using AionWindowServiceUpload.Services;

namespace AionWindowServiceUpload
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IRawTimeLogStagingAppService _rawTimeLogsRepository;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
            _rawTimeLogsRepository = new RawTimeLogStagingAppService(logger); 
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);

                var count = await _rawTimeLogsRepository.AutomatedUpload();

                if (count > 0)
                {
                    _logger.LogInformation($"{count} Record(s) have been uploaded.");
                }

                //await Task.Delay(10000, stoppingToken); //10 sec
                await Task.Delay(TimeSpan.FromMinutes(1));
                //await Task.Delay(TimeSpan.FromHours(1), stoppingToken);
            }
        }

        public override Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Starting...");
            return base.StartAsync(stoppingToken);
        }

        public override Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Stop!");
            return base.StopAsync(stoppingToken);
        }
    }
}