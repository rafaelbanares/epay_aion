using AionWindowServiceUpload;
using Serilog;

var progData = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .WriteTo.File(Path.Combine(progData, "AionWindowServicesUpload", "servicelog.txt"))
    .CreateLogger();

IHost host = Host.CreateDefaultBuilder(args)
    .UseWindowsService()
    .UseSerilog()
    .ConfigureServices((hostContext, services) =>
    {
        IConfiguration configuration = hostContext.Configuration;
        AppSettings.Configuration = configuration;
        AppSettings.TenantId = int.Parse(configuration.GetSection("Tenant")["Id"]);
        AppSettings.TenancyName = configuration.GetSection("Tenant")["TenancyName"];
        AppSettings.ServerUrlBase = configuration.GetSection("Configuration")["ServerUrlBase"];
        AppSettings.FilePath = configuration.GetSection("Configuration")["FilePath"];
        AppSettings.ClientId = configuration.GetSection("Configuration")["ClientId"];
        AppSettings.ClientSecret = configuration.GetSection("Configuration")["ClientSecret"];
        AppSettings.AllowedScopes = configuration.GetSection("Configuration")["AllowedScopes"];
        AppSettings.Password = configuration.GetSection("Configuration")["Password"];
        AppSettings.Salt = configuration.GetSection("Configuration")["Salt"];

        services.AddHostedService<Worker>();
    })
    .Build();

host.Run();
