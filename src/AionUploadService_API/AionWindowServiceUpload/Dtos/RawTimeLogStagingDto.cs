﻿namespace AionWindowServiceUpload.Dtos
{
    public class AttendanceUploadsDto
    {
        public required string EmployeeCode { get; set; }

        public DateTime Date { get; set; }

        public string? TimeIn { get; set; }

        public string? LunchBreaktimeIn { get; set; }

        public string? LunchBreaktimeOut { get; set; }

        public string? PMBreaktimeIn { get; set; }

        public string? PMBreaktimeOut { get; set; }

        public string? TimeOut { get; set; }
    }

    public class RawTimeLogStagingDto
    {
        public required string SwipeCode { get; set; }

        public DateTime LogTime { get; set; }

        public string? InOut { get; set; }
    }

    public class FileMoveDto
    {
        public bool IsSuccessful { get; set; }

        public required string FilePath { get; set; }

        public string? UploadPath { get; set; }

        public string? ErrorPath { get; set; }

        public required string FileName { get; set; }
    }

    public class CreateAttendanceUploadsDto
    {
        public CreateAttendanceUploadsDto()
        {
            AttendanceUploads = new List<AttendanceUploadsDto>();
        }

        public List<AttendanceUploadsDto> AttendanceUploads { get; set; }
    }

    public class CreateRawTimeLogStagingDto
    {
        public CreateRawTimeLogStagingDto()
        {
            RawTimeLogStaging = new List<RawTimeLogStagingDto>();
        }

        public List<RawTimeLogStagingDto> RawTimeLogStaging { get; set; }

        public Guid UploadSessionId { get; set; }
    }
}
