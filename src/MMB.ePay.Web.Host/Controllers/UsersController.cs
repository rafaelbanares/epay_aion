﻿using Abp.AspNetCore.Mvc.Authorization;
using MMB.ePay.Authorization;
using MMB.ePay.Storage;
using Abp.BackgroundJobs;

namespace MMB.ePay.Web.Controllers
{
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_Users)]
    public class UsersController : UsersControllerBase
    {
        public UsersController(IBinaryObjectManager binaryObjectManager, IBackgroundJobManager backgroundJobManager)
            : base(binaryObjectManager, backgroundJobManager)
        {
        }
    }
}