#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["src/MMB.ePay.Web.Host/MMB.ePay.Web.Host.csproj", "src/MMB.ePay.Web.Host/"]
COPY ["src/MMB.ePay.Web.Core/MMB.ePay.Web.Core.csproj", "src/MMB.ePay.Web.Core/"]
COPY ["src/MMB.ePay.Application/MMB.ePay.Application.csproj", "src/MMB.ePay.Application/"]
COPY ["src/MMB.ePay.Application.Shared/MMB.ePay.Application.Shared.csproj", "src/MMB.ePay.Application.Shared/"]
COPY ["src/MMB.ePay.Core.Shared/MMB.ePay.Core.Shared.csproj", "src/MMB.ePay.Core.Shared/"]
COPY ["src/MMB.ePay.Core/MMB.ePay.Core.csproj", "src/MMB.ePay.Core/"]
COPY ["src/MMB.ePay.EntityFrameworkCore/MMB.ePay.EntityFrameworkCore.csproj", "src/MMB.ePay.EntityFrameworkCore/"]
COPY ["src/MMB.ePay.GraphQL/MMB.ePay.GraphQL.csproj", "src/MMB.ePay.GraphQL/"]
RUN dotnet restore "src/MMB.ePay.Web.Host/MMB.ePay.Web.Host.csproj"
COPY . .
WORKDIR "/src/src/MMB.ePay.Web.Host"
RUN dotnet build "MMB.ePay.Web.Host.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "MMB.ePay.Web.Host.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "MMB.ePay.Web.Host.dll"]
