﻿using Abp.AspNetCore.Mvc.Views;

namespace MMB.ePay.Web.Views
{
    public abstract class ePayRazorPage<TModel> : AbpRazorPage<TModel>
    {
        protected ePayRazorPage()
        {
            LocalizationSourceName = ePayConsts.LocalizationSourceName;
        }
    }
}
