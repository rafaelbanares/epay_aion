﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class WorkFlowAppService : ePayAppServiceBase, IWorkFlowAppService
    {
        private readonly IRepository<WorkFlow> _workFlowRepository;
        private readonly IRepository<Status> _statusRepository;

        public WorkFlowAppService(IRepository<WorkFlow> workFlowRepository,
            IRepository<Status> statusRepository)
        {
            _workFlowRepository = workFlowRepository;
            _statusRepository = statusRepository;
        }

        public async Task<List<SelectListItem>> GetWorkFlowStatusList(int statusId) =>
            await _workFlowRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId && e.StatusId == statusId
                    && e.Status.AccessLevel == 0 && e.NextStatus != null)
                .Select(e => new SelectListItem
                {
                    Value = e.NextStatus.ToString(),
                    Text = e.NextStatusNavigation.BeforeStatusActionText
                }).ToListAsync();

        [AbpAuthorize(AppPermissions.Pages_WorkFlow)]
        public async Task<PagedResultDto<GetWorkFlowForViewDto>> GetAll(GetAllWorkFlowInput input)
        {
            var status = await _statusRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new
                {
                    e.DisplayName,
                    e.Id
                }).ToListAsync();

            var nextStatus = await _workFlowRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new
                {
                    e.NextStatusNavigation.DisplayName,
                    e.StatusId
                }).ToListAsync();

            var workFlow = status.Join(nextStatus,
                status => status.Id,
                nextStatus => nextStatus.StatusId,
                (status, nextStatus) => new WorkFlowDto
                {
                    Status = status.DisplayName,
                    NextStatus = nextStatus.DisplayName,
                    Id = status.Id
                }).ToList();

            var modifiedWorkFlow = workFlow
                .Select(e =>
                {
                    e.NextStatus = string.IsNullOrEmpty(e.NextStatus) ? "Finalized" : e.NextStatus;
                    return e;
                }).ToList();

            var results = modifiedWorkFlow
                .GroupBy(e => e.Id)
                .Select(e => new GetWorkFlowForViewDto
                {
                    WorkFlow = new WorkFlowDto
                    {
                        Status = e.First().Status,
                        NextStatus = string.Join(", ", e.Select(f => f.NextStatus).ToList()),
                        Id = e.Key
                    }
                }).ToList();

            return new PagedResultDto<GetWorkFlowForViewDto>(results.Count, results);
        }

        [AbpAuthorize(AppPermissions.Pages_WorkFlow)]
        public async Task<GetWorkFlowForViewDto> GetWorkFlowForView(int id)
        {
            var status = await _statusRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => e.DisplayName)
                .FirstOrDefaultAsync();

            var nextStatus = await _workFlowRepository.GetAll()
                .Where(e => e.StatusId == id)
                .Select(e => e.NextStatusNavigation.DisplayName)
                .ToArrayAsync();

            var modifiedWorkFlow = nextStatus
                .Select(e =>
                {
                    e = string.IsNullOrEmpty(e) ? "Finalized" : e;
                    return e;
                }).ToList();

            return new GetWorkFlowForViewDto
            {
                WorkFlow = new WorkFlowDto
                {
                    Status = status,
                    NextStatus = string.Join(", ", nextStatus)
                }
            };
        }
    }
}