﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class FrequenciesAppService : ePayAppServiceBase, IFrequenciesAppService
    {
        private readonly IRepository<Frequencies> _frequenciesRepository;

        public FrequenciesAppService(IRepository<Frequencies> frequenciesRepository)
        {
            _frequenciesRepository = frequenciesRepository;

        }

        public async Task<IList<SelectListItem>> GetFrequencyList()
        {
            var frequencyList = await _frequenciesRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            frequencyList.Insert(0, new SelectListItem { Text = "Select", Value = "0" });

            return frequencyList;
        }

        public async Task<IList<SelectListItem>> GetFrequencyListForFilter()
        {
            return await _frequenciesRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName,
                    Selected = e.DisplayName == "Semi-Monthly"
                }).ToListAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_Frequencies)]
        public async Task<PagedResultDto<GetFrequenciesForViewDto>> GetAll(GetAllFrequenciesInput input)
        {
            var filteredFrequencies = _frequenciesRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.DisplayName.Contains(input.Filter) || e.Description.Contains(input.Filter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.DisplayNameFilter), e => e.DisplayName == input.DisplayNameFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description == input.DescriptionFilter);

            var pagedAndFilteredFrequencies = filteredFrequencies
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var frequencies = pagedAndFilteredFrequencies
                .Select(o => new
                {
                    o.DisplayName,
                    o.Description,
                    o.Id
                });

            var totalCount = await filteredFrequencies.CountAsync();
            var results = await frequencies
                .Select(o => new GetFrequenciesForViewDto
                {
                    Frequencies = new FrequenciesDto
                    {

                        DisplayName = o.DisplayName,
                        Description = o.Description,
                        Id = o.Id,
                    }
                }).ToListAsync();

            return new PagedResultDto<GetFrequenciesForViewDto>(
                totalCount,
                results
            );
        }

        public async Task<GetFrequenciesForViewDto> GetFrequenciesForView(int id)
        {
            var frequencies = await _frequenciesRepository.GetAsync(id);

            var output = new GetFrequenciesForViewDto { Frequencies = ObjectMapper.Map<FrequenciesDto>(frequencies) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Frequencies_Edit)]
        public async Task<GetFrequenciesForEditOutput> GetFrequenciesForEdit(EntityDto input)
        {
            var frequencies = await _frequenciesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetFrequenciesForEditOutput { Frequencies = ObjectMapper.Map<CreateOrEditFrequenciesDto>(frequencies) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditFrequenciesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Frequencies_Create)]
        protected virtual async Task Create(CreateOrEditFrequenciesDto input)
        {
            var frequencies = ObjectMapper.Map<Frequencies>(input);

            if (AbpSession.TenantId != null)
            {
                frequencies.TenantId = AbpSession.TenantId.Value;
            }

            await _frequenciesRepository.InsertAsync(frequencies);

        }

        [AbpAuthorize(AppPermissions.Pages_Frequencies_Edit)]
        protected virtual async Task Update(CreateOrEditFrequenciesDto input)
        {
            var frequencies = await _frequenciesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, frequencies);

        }

        [AbpAuthorize(AppPermissions.Pages_Frequencies_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _frequenciesRepository.DeleteAsync(input.Id);
        }

    }
}