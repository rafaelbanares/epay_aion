﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Timekeeping.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize(AppPermissions.Pages_EmployeeApprovers)]
    public class EmployeeApproversAppService : ePayAppServiceBase, IEmployeeApproversAppService
    {
        private readonly IRepository<EmployeeApprovers> _employeeApproversRepository;
        private readonly IRepository<ApproverOrders> _approverOrdersRepository;
        private readonly IRepository<Employees> _employeesRepository;

        public EmployeeApproversAppService(IRepository<EmployeeApprovers> employeeApproversRepository,
            IRepository<ApproverOrders> approverOrdersRepository,
            IRepository<Employees> employeesRepository)
        {
            _employeeApproversRepository = employeeApproversRepository;
            _approverOrdersRepository = approverOrdersRepository;
            _employeesRepository = employeesRepository;
        }

        public async Task<PagedResultDto<GetEmployeeApproversForViewDto>> GetAll(GetAllEmployeeApproversInput input)
        {
            var maxCount = await _approverOrdersRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .CountAsync();

            input.MaxResultCount *= maxCount;

            if (input.MaxResultCount == 0)
            {
                return new PagedResultDto<GetEmployeeApproversForViewDto>(0,
                    new List<GetEmployeeApproversForViewDto>()
                );
            }

            var filteredEmployeeApprovers = _employeeApproversRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e =>
                    e.Employees.EmployeeCode.ToUpper().Contains(input.Filter)
                    || (e.Employees.EmployeeCode + " | " + e.Employees.LastName + ", " + e.Employees.FirstName + " " + e.Employees.MiddleName)
                        .Contains(input.Filter));

            var pagedAndFilteredEmployeeApprovers = filteredEmployeeApprovers
                .Select(e => new
                {
                    Id = e.EmployeeId,
                    Employee = e.Employees.FullName,
                    Approver = e.Approvers.FullName,
                    Placement = (e.ApproverOrders.Level * 2) + (e.ApproverOrders.IsBackup ? 1 : 0) - 2
                })
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var approverOrderCount = await _approverOrdersRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .CountAsync();

            var employeeApprovers = (await pagedAndFilteredEmployeeApprovers.ToListAsync())
                .GroupBy(e => e.Id)
                .Select(e => new
                {
                    Id = e.Key,
                    e.First().Employee,
                    Approvers = e.Select(f =>
                        new Tuple<string, int>
                        (
                            f.Approver,
                            f.Placement
                        )).ToList(),
                }).ToList();

            var totalCount = (await filteredEmployeeApprovers.ToListAsync()).GroupBy(e => e.EmployeeId).Count();
            var results = employeeApprovers
                .Select(e => new GetEmployeeApproversForViewDto
                {
                    EmployeeApprovers = new EmployeeApproversDto
                    {
                        Id = e.Id,
                        Employee = e.Employee,
                        Approvers = Enumerable.Range(0, approverOrderCount)
                            .Select(f => e.Approvers.Where(g => g.Item2 == f).Select(g => g.Item1).FirstOrDefault())
                            .ToList()
                    }
                }).ToList();

            return new PagedResultDto<GetEmployeeApproversForViewDto>(
                totalCount,
                results
            );
        }

        public async Task<GetEmployeeApproversForViewDto> GetEmployeeApproversForView(int id)
        {
            var employeeApprovers = await _employeesRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new EmployeeApproversDto
                {
                    Employee = e.FullName,
                    ApproverList = e.EmployeeApproversEmployee
                        .Select(f => new Tuple<string, string>
                        (
                            f.ApproverOrders.DisplayName,
                            f.Approvers.FullName
                        )).ToList()
                }).FirstOrDefaultAsync();

            return new GetEmployeeApproversForViewDto
            {
                EmployeeApprovers = employeeApprovers
            };
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeeApprovers_Edit)]
        public async Task<GetEmployeeApproversForEditOutput> GetEmployeeApproversForEdit(EntityDto input)
        {
            var approverOrderCount = await _approverOrdersRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .CountAsync();

            var employeeApprovers = await _employeeApproversRepository.GetAll()
                .Where(e => e.EmployeeId == input.Id)
                .OrderBy(e => e.ApproverOrders.Level)
                    .ThenBy(e => e.ApproverOrders.IsBackup)
                .Select(e => new
                {
                    e.EmployeeId,
                    e.ApproverId,
                    Placement = (e.ApproverOrders.Level * 2) + (e.ApproverOrders.IsBackup ? 1 : 0) - 2,
                    EmployeeName = string.Format("{0} | {1}", e.Employees.EmployeeCode, e.Employees.FullName.ToUpper()),
                    ApproverName = string.Format("{0} | {1}", e.Approvers.EmployeeCode, e.Approvers.FullName.ToUpper())
                }).ToListAsync();

            return new GetEmployeeApproversForEditOutput
            {
                EmployeeApprovers = new CreateOrEditEmployeeApproversDto
                {
                    Id = input.Id,
                    EmployeeId = employeeApprovers.First().EmployeeId,
                    Employee = employeeApprovers.First().EmployeeName,
                    ApproverIdList = Enumerable.Range(0, approverOrderCount)
                        .Select(e =>
                            employeeApprovers.Where(f => f.Placement == e)
                            .Select(f => (int?)f.ApproverId).FirstOrDefault()
                        ).ToList(),
                    ApproverNameList = Enumerable.Range(0, approverOrderCount)
                        .Select(e =>
                            employeeApprovers.Where(f => f.Placement == e)
                            .Select(f => f.ApproverName).FirstOrDefault()
                        ).ToList()

                }
            };
        }

        public async Task CreateOrEdit(CreateOrEditEmployeeApproversDto input)
        {
            if (input.EmployeeId == null || input.EmployeeId == 0)
                throw new UserFriendlyException("Your request is not valid!", "The Employee field is required.");

            if (input.ApproverIdList.Any(e => e == input.EmployeeId))
                throw new UserFriendlyException("Your request is not valid!", "The employee cannot be their own approver.");

            if (input.ApproverIdList.Where(e => e != null).Count() != input.ApproverIdList.Where(e => e != null).Distinct().Count())
                throw new UserFriendlyException("Your request is not valid!", "The employee has duplicate approvers.");

            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeeApprovers_Create)]
        protected virtual async Task Create(CreateOrEditEmployeeApproversDto input)
        {
            if (await _employeeApproversRepository.GetAll().AnyAsync(e => e.EmployeeId == input.EmployeeId))
                throw new UserFriendlyException("Your request is not valid!", "The employee has an existsing record.");

            var approverOrders = await _approverOrdersRepository.GetAll()
                .OrderBy(e => e.Level)
                    .ThenBy(e => e.IsBackup)
                .Select(e => e.Id)
                .ToListAsync();

            for (var i = 0; input.ApproverIdList.Count > i; i++)
            {
                if (input.ApproverIdList[i] != null)
                {
                    var employeeApprovers = new EmployeeApprovers
                    {
                        EmployeeId = (int)input.EmployeeId,
                        ApproverId = (int)input.ApproverIdList[i],
                        ApproverOrderId = approverOrders[i]
                    };

                    if (AbpSession.TenantId != null)
                    {
                        employeeApprovers.TenantId = AbpSession.TenantId.Value;
                    }

                    await _employeeApproversRepository.InsertAsync(employeeApprovers);
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeeApprovers_Edit)]
        protected virtual async Task Update(CreateOrEditEmployeeApproversDto input)
        {
            var employeeApprovers = await _employeeApproversRepository.GetAll()
                .Where(e => e.EmployeeId == input.Id)
                .ToListAsync();

            foreach (var employeeApprover in employeeApprovers)
                await _employeeApproversRepository.DeleteAsync(employeeApprover);

            var approverOrders = await _approverOrdersRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .OrderBy(e => e.Level)
                    .ThenBy(e => e.IsBackup)
                .Select(e => new { e.Id, e.Level, e.IsBackup })
                .ToListAsync();

            for (var i = 0; input.ApproverIdList.Count > i; i++)
            {
                if (approverOrders[i].Level == 1 && !approverOrders[i].IsBackup && input.ApproverIdList[i] == null)
                    throw new UserFriendlyException("Your request is not valid!", "Level 1 Approvers are required.");

                if (input.ApproverIdList[i] != null)
                {
                    var insert = new EmployeeApprovers
                    {
                        TenantId = AbpSession.TenantId.Value,
                        EmployeeId = input.EmployeeId.Value, 
                        ApproverId = input.ApproverIdList[i].Value,
                        ApproverOrderId = approverOrders[i].Id
                    };

                    await _employeeApproversRepository.InsertAsync(insert);
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeeApprovers_Delete)]
        public async Task Delete(EntityDto input) =>
            await _employeeApproversRepository.DeleteAsync(input.Id);
    }
}