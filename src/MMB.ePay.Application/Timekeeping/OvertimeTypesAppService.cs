﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class OvertimeTypesAppService : ePayAppServiceBase, IOvertimeTypesAppService
    {
        private readonly IRepository<OvertimeTypes> _overtimeTypesRepository;

        public OvertimeTypesAppService(IRepository<OvertimeTypes> overtimeTypesRepository) =>
            _overtimeTypesRepository = overtimeTypesRepository;

        public async Task<IList<SelectListItem>> GetOvertimeTypeList()
        {
            var types = await _overtimeTypesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.ShortName
                }).ToListAsync();

            types.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return types;
        }

        public async Task<IList<SelectListItem>> GetOvertimeTypeOTCodeList(string otCode)
        {
            var otCodes = OvertimeTypesConsts.StandardOTCodes.ToList();

            var overtimeTypesOTCode = await _overtimeTypesRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(otCode), e => e.OTCode != otCode)
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new { e.OTCode, e.SubType })
                .ToListAsync();

            var otCodesList = overtimeTypesOTCode
                .GroupBy(e => e.OTCode)
                .Where(e => string.Join("", e.Select(f => f.SubType).OrderBy(f => f).ToList()) == "1234")
                .Select(e => e.Key)
                .ToList();

            var list = otCodes.Except(otCodesList)
                .Select(e => new SelectListItem { Value = e, Text = OvertimeTypesConsts.OTCodeDictionary[e] })
                .ToList();

            list.Insert(0, new SelectListItem { Value = string.Empty, Text = "Select" });

            return list;
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeTypes)]
        public async Task<PagedResultDto<GetOvertimeTypesForViewDto>> GetAll(GetAllOvertimeTypesInput input)
        {
            var filteredType = _overtimeTypesRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId);  

            var totalCount = await filteredType.CountAsync();
            var results = await filteredType
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input)
                .Select(e => new GetOvertimeTypesForViewDto
                {
                    OvertimeTypes = new OvertimeTypesDto
                    {
                        ShortName = e.ShortName,
                        Description = e.Description,
                        OTCode = e.OTCode,
                        Id = e.Id
                    }
                }).ToListAsync();

            return new PagedResultDto<GetOvertimeTypesForViewDto>(totalCount, results);
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeTypes)]
        public async Task<GetOvertimeTypesForViewDto> GetOvertimeTypesForView(int id)
        {
            var type = await _overtimeTypesRepository.GetAsync(id);
            var output = new GetOvertimeTypesForViewDto { OvertimeTypes = ObjectMapper.Map<OvertimeTypesDto>(type) };
            var displayFormat = output.OvertimeTypes.DisplayFormat;

            output.OvertimeTypes.DisplayFormat = 
                displayFormat == "s" ? "seconds" :
                displayFormat == "m" ? "minutes" :
                displayFormat == "h" ? "hours" : 
                string.Empty;

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeTypes_Edit)]
        public async Task<GetOvertimeTypesForEditOutput> GetOvertimeTypesForEdit(int id)
        {
            var type = await _overtimeTypesRepository.FirstOrDefaultAsync(id);

           return new GetOvertimeTypesForEditOutput { OvertimeTypes = ObjectMapper.Map<CreateOrEditOvertimeTypesDto>(type) };
        }

        public async Task CreateOrEdit(CreateOrEditOvertimeTypesDto input)
        {
            await Validate(input);

            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        private async Task Validate(CreateOrEditOvertimeTypesDto input)
        {
            var hasOvertimeType = await _overtimeTypesRepository.GetAll().AnyAsync(e =>
                e.TenantId == AbpSession.TenantId && e.OTCode == input.OTCode
                    && e.SubType == input.SubType && e.Id != input.Id);

            if (hasOvertimeType)
                throw new UserFriendlyException("Your request is not valid!", "An identical record already exists.");
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeTypes_Create)]
        private async Task Create(CreateOrEditOvertimeTypesDto input)
        {
            var type = ObjectMapper.Map<OvertimeTypes>(input);

            var subType = 
                input.SubType == 4 ? " ND2" :
                input.SubType == 3 ? " ND1" :
                input.SubType == 2 ? " > 8" : 
                string.Empty;

            type.ShortName += subType;

            await _overtimeTypesRepository.InsertAsync(type);
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeTypes_Edit)]
        private async Task Update(CreateOrEditOvertimeTypesDto input)
        {
            var type = await _overtimeTypesRepository.FirstOrDefaultAsync(input.Id.Value);
            ObjectMapper.Map(input, type);
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeTypes_Delete)]
        public async Task Delete(int id) =>
            await _overtimeTypesRepository.DeleteAsync(id);
    }
}