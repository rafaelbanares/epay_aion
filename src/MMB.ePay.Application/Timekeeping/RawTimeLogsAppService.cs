﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Timekeeping.Dtos;
using RestSharp;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
	[AbpAuthorize]
	public class RawTimeLogsAppService : ePayAppServiceBase, IRawTimeLogsAppService
	{
		private const int V = 0;
		private readonly IRepository<RawTimeLogs> _rawTimeLogsRepository;
		private readonly IRepository<Employees> _employeesRepository;
		private readonly IRepository<Attendances> _attendancesRepository;

		public RawTimeLogsAppService(IRepository<RawTimeLogs> rawTimeLogsRepository,
			IRepository<Employees> employeesRepository,
			IRepository<Attendances> attendancesRepository)
		{
			_rawTimeLogsRepository = rawTimeLogsRepository;
			_employeesRepository = employeesRepository;
			_attendancesRepository = attendancesRepository;
		}

		[AbpAuthorize(AppPermissions.Pages_RawTimeLogs)]
		public async Task<PagedResultDto<GetRawTimeLogsForViewDto>> GetAll(GetAllRawTimeLogsInput input)
		{
			var swipeCode = await _employeesRepository.GetAll()
				.Where(e => e.Id == input.EmployeeIdFilter)
				.Select(e => e.EmployeeTimeInfo.SwipeCode)
				.FirstOrDefaultAsync();

			var filteredRawTimeLogs = _rawTimeLogsRepository.GetAll()
				.WhereIf(input.DateFilter != null, e => e.LogTime.Date == input.DateFilter)
				.Where(e => e.TenantId == AbpSession.TenantId
					&& e.SwipeCode == swipeCode);

			var totalCount = await filteredRawTimeLogs.CountAsync();
			var results = await filteredRawTimeLogs
				.OrderBy(input.Sorting ?? "id desc")
				.PageBy(input)
				.Select(e => new GetRawTimeLogsForViewDto
				{
					RawTimeLogs = new RawTimeLogsDto
					{
						LogTime = e.LogTime,
						InOut = e.InOut,
						WorkFromHome = e.WorkFromHome,
						Device = e.Device,
						Latitude = e.Geolocation == null ? V : e.Geolocation.Y,
						Longitude = e.Geolocation == null ? V : e.Geolocation.X,
						Id = e.Id
					}
				}).ToListAsync();

			results.ForEach(e =>
			{
				e.RawTimeLogs.InOut = e.RawTimeLogs.InOut == "I" ? "IN" : "OUT";
				e.RawTimeLogs.Device = e.RawTimeLogs.Device == "M" ? "Mobile" : "PC";
			});

			return new PagedResultDto<GetRawTimeLogsForViewDto>(totalCount, results);
		}

		private static async Task<string> GetAddressFromPoint(double latitude, double longitude) /*XY*/
		{
			var nominatimUrl = $"https://nominatim.openstreetmap.org/reverse?format=json&lat={latitude}&lon={longitude}&zoom=18&addressdetails=1";

			var client = new RestClient(nominatimUrl);
			var request = new RestRequest();
			var response = await client.GetAsync(request);

			// Parse the JSON response to get the address
			dynamic jsonData = Newtonsoft.Json.JsonConvert.DeserializeObject(response.Content);
			string address = jsonData.display_name;

			return address;
		}

		[AbpAuthorize(AppPermissions.Pages_RawTimeLogs)]
		public async Task<GetRawTimeLogsForViewDto> GetRawTimeLogsForView(int id)
		{
			var rawTimeLogs = await _rawTimeLogsRepository.GetAll()
				.Where(e => e.Id == id)
				.Select(e => new RawTimeLogsDto
				{
					LogTime = e.LogTime,
					InOut = e.InOut == "I" ? "IN" : "OUT",
					WorkFromHome = e.WorkFromHome,
					Device = e.Device == "M" ? "Mobile" : "PC",
					Latitude = e.Geolocation.Y,
					Longitude = e.Geolocation.X
				}).FirstOrDefaultAsync();

			rawTimeLogs.Address = await GetAddressFromPoint(rawTimeLogs.Latitude, rawTimeLogs.Longitude);

			return new GetRawTimeLogsForViewDto
			{
				RawTimeLogs = rawTimeLogs
			};
		}

        [AbpAuthorize(AppPermissions.Pages_Attendances)]
        public async Task<GetRawTimeLogsForAttendancesDto> GetRawTimeLogsForAttendances(int id)
		{
			var attendances = await _attendancesRepository.GetAll()
				.Where(e => e.Id == id)
				.Select(e => new
				{
					e.Employees.EmployeeTimeInfo.SwipeCode,
					e.Date.Date
				}).FirstOrDefaultAsync();

			if (attendances == null)
			{
				return new GetRawTimeLogsForAttendancesDto();
			}

			var firstDate = attendances.Date.AddDays(-1);
			var lastDate = attendances.Date.AddDays(2);

			var rawTimeLogs = await _rawTimeLogsRepository.GetAll()
				.Where(e => e.SwipeCode == attendances.SwipeCode
					&& e.LogTime >= firstDate && e.LogTime <= lastDate)
				.Select(e => new RawTimeLogsDto
				{
					LogTime = e.LogTime,
					InOut = e.InOut == "I" ? "IN" : "OUT",
					WorkFromHome = e.WorkFromHome,
					Device = e.Device == "M" ? "Mobile" : "PC",
				})
				.OrderBy(e => e.LogTime)
				.ToListAsync();

			return new GetRawTimeLogsForAttendancesDto
			{
				RawTimeLogsList = rawTimeLogs
			};
		}

		[AbpAuthorize(AppPermissions.Pages_RawTimeLogs_Edit)]
		public async Task<GetRawTimeLogsForEditOutput> GetRawTimeLogsForEdit(int id)
		{
			var rawTimeLogs = await _rawTimeLogsRepository.FirstOrDefaultAsync(id);

			return new GetRawTimeLogsForEditOutput { RawTimeLogs = ObjectMapper.Map<CreateOrEditRawTimeLogsDto>(rawTimeLogs) };
		}

		public async Task CreateOrEdit(CreateOrEditRawTimeLogsDto input)
		{
			if (input.Id == null)
				await Create(input);
			else
				await Update(input);
		}

		[AbpAuthorize(AppPermissions.Pages_RawTimeLogs_Create)]
		protected virtual async Task Create(CreateOrEditRawTimeLogsDto input)
		{
			var rawTimeLogs = ObjectMapper.Map<RawTimeLogs>(input);
			await _rawTimeLogsRepository.InsertAsync(rawTimeLogs);
		}

		[AbpAuthorize(AppPermissions.Pages_RawTimeLogs_Edit)]
		protected virtual async Task Update(CreateOrEditRawTimeLogsDto input)
		{
			var rawTimeLogs = await _rawTimeLogsRepository.FirstOrDefaultAsync(input.Id.Value);
			ObjectMapper.Map(input, rawTimeLogs);
		}

		[AbpAuthorize(AppPermissions.Pages_RawTimeLogs_Delete)]
		public async Task Delete(int id) =>
			await _rawTimeLogsRepository.DeleteAsync(id);
	}
}