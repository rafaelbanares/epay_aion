﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class LeaveTypesAppService : ePayAppServiceBase, ILeaveTypesAppService
    {
        private readonly IRepository<LeaveTypes> _leaveTypesRepository;
        private readonly IRepository<LeaveEarningDetails> _leaveEarningDetailsRepository;
        private readonly IRepository<LeaveRequests> _leaveRequestsRepository;

        public LeaveTypesAppService(IRepository<LeaveTypes> leaveTypesRepository,
            IRepository<LeaveEarningDetails> leaveEarningDetailsRepository,
            IRepository<LeaveRequests> leaveRequestsRepository)
        {
            _leaveTypesRepository = leaveTypesRepository;
            _leaveEarningDetailsRepository = leaveEarningDetailsRepository;
            _leaveRequestsRepository = leaveRequestsRepository;
        }

        public async Task<IList<SelectListItem>> GetLeaveTypeList()
        {
            var types = await _leaveTypesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            types.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return types;
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveTypes)]
        public async Task<PagedResultDto<GetLeaveTypesForViewDto>> GetAll(GetAllLeaveTypesInput input)
        {
            var filteredTypes = _leaveTypesRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId);  

            var totalCount = await filteredTypes.CountAsync();
            var results = await filteredTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input)
                .Select(o => new GetLeaveTypesForViewDto
                {
                    LeaveTypes = new LeaveTypesDto
                    {
                        DisplayName = o.DisplayName,
                        Description = o.Description,
                        WithPay = o.WithPay,
                        AdvancedFilingInDays = o.AdvancedFilingInDays,
                        Id = o.Id,
                    }
                }).ToListAsync();

            return new PagedResultDto<GetLeaveTypesForViewDto>(totalCount, results);
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveTypes)]
        public async Task<GetLeaveTypesForViewDto> GetLeaveTypesForView(int id)
        {
            var type = await _leaveTypesRepository.GetAsync(id);

            return new GetLeaveTypesForViewDto { LeaveTypes = ObjectMapper.Map<LeaveTypesDto>(type) };
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveTypes_Edit)]
        public async Task<GetLeaveTypesForEditOutput> GetLeaveTypesForEdit(int id)
        {
            var type = await _leaveTypesRepository.FirstOrDefaultAsync(id);

            return new GetLeaveTypesForEditOutput { LeaveTypes = ObjectMapper.Map<CreateOrEditLeaveTypesDto>(type) };
        }

        public async Task CreateOrEdit(CreateOrEditLeaveTypesDto input)
        {
            await Validate(input);
            if (input.AdvancedFilingInDays == 0)
                input.AdvancedFilingInDays = null;

            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        private async Task Validate(CreateOrEditLeaveTypesDto input)
        {


            var hasLeaveType = await _leaveTypesRepository.GetAll().AnyAsync(e =>
                e.TenantId == AbpSession.TenantId && e.Id != input.Id
                && e.DisplayName == input.DisplayName);

            if (hasLeaveType)
                throw new UserFriendlyException("Your request is not valid!", "An identical record already exists.");
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveTypes_Create)]
        private async Task Create(CreateOrEditLeaveTypesDto input)
        {
            

            var type = ObjectMapper.Map<LeaveTypes>(input);
            await _leaveTypesRepository.InsertAsync(type);
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveTypes_Edit)]
        private async Task Update(CreateOrEditLeaveTypesDto input)
        {
            var type = await _leaveTypesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, type);
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveTypes_Delete)]
        public async Task Delete(int id)
        {
            if (await _leaveEarningDetailsRepository.GetAll().AnyAsync(e => e.LeaveTypeId == id))
                throw new UserFriendlyException("Your request is not valid!", "The leave type is already in use.");

            if (await _leaveRequestsRepository.GetAll().AnyAsync(e => e.LeaveTypeId == id))
                throw new UserFriendlyException("Your request is not valid!", "The leave type is already in use.");

            await _leaveTypesRepository.DeleteAsync(id);
        }

    }
}