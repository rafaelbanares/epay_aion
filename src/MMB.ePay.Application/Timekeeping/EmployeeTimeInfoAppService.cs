﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping.Dtos;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class EmployeeTimeInfoAppService : ePayAppServiceBase, IEmployeeTimeInfoAppService
    {
        private readonly IRepository<EmployeeTimeInfo> _employeeTimeInfoRepository;
        private readonly IRepository<AttendancePeriods> _attendancePeriodsRepository;
        private readonly IRepository<ShiftRequests> _shiftRequestsRepository;
        private readonly IRepository<ShiftTypeDetails> _shiftTypeDetailsRepository;

        public EmployeeTimeInfoAppService(IRepository<EmployeeTimeInfo> employeeTimeInfoRepository,
            IRepository<AttendancePeriods> attendancePeriodsRepository,
            IRepository<ShiftRequests> shiftRequestsRepository,
            IRepository<ShiftTypeDetails> shiftTypeDetailsRepository)
        {
            _employeeTimeInfoRepository = employeeTimeInfoRepository;
            _attendancePeriodsRepository = attendancePeriodsRepository;
            _shiftRequestsRepository = shiftRequestsRepository;
            _shiftTypeDetailsRepository = shiftTypeDetailsRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeeTimeInfo)]
        public async Task<PagedResultDto<GetEmployeeTimeInfoForViewDto>> GetAll(GetAllEmployeeTimeInfoInput input)
        {
            var filteredEmployeeTimeInfo = _employeeTimeInfoRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.RestDay.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.RestDayFilter), e => e.RestDay == input.RestDayFilter)
                        .WhereIf(input.OvertimeFilter.HasValue && input.OvertimeFilter > -1, e => (input.OvertimeFilter == 1 && e.Overtime) || (input.OvertimeFilter == 0 && !e.Overtime))
                        .WhereIf(input.UndertimeFilter.HasValue && input.UndertimeFilter > -1, e => (input.UndertimeFilter == 1 && e.Undertime) || (input.UndertimeFilter == 0 && !e.Undertime))
                        .WhereIf(input.TardyFilter.HasValue && input.TardyFilter > -1, e => (input.TardyFilter == 1 && e.Tardy) || (input.TardyFilter == 0 && !e.Tardy))
                        .WhereIf(input.NonSwiperFilter.HasValue && input.NonSwiperFilter > -1, e => (input.NonSwiperFilter == 1 && e.NonSwiper) || (input.NonSwiperFilter == 0 && !e.NonSwiper))
                        .WhereIf(input.MinEmployeeIdFilter != null, e => e.EmployeeId >= input.MinEmployeeIdFilter)
                        .WhereIf(input.MaxEmployeeIdFilter != null, e => e.EmployeeId <= input.MaxEmployeeIdFilter)
                        .WhereIf(input.MinShiftTypeIdFilter != null, e => e.ShiftTypeId >= input.MinShiftTypeIdFilter)
                        .WhereIf(input.MaxShiftTypeIdFilter != null, e => e.ShiftTypeId <= input.MaxShiftTypeIdFilter);

            var pagedAndFilteredEmployeeTimeInfo = filteredEmployeeTimeInfo
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var employeeTimeInfo = pagedAndFilteredEmployeeTimeInfo
                .Select(o => new
                {
                    o.RestDay,
                    o.Overtime,
                    o.Undertime,
                    o.Tardy,
                    o.NonSwiper,
                    o.EmployeeId,
                    o.FrequencyId,
                    o.ShiftTypeId,
                    o.Id
                });

            var totalCount = await filteredEmployeeTimeInfo.CountAsync();
            var results = await employeeTimeInfo
                .Select(o => new GetEmployeeTimeInfoForViewDto
                {
                    EmployeeTimeInfo = new EmployeeTimeInfoDto
                    {

                        RestDay = o.RestDay,
                        Overtime = o.Overtime,
                        Undertime = o.Undertime,
                        Tardy = o.Tardy,
                        NonSwiper = o.NonSwiper,
                        EmployeeId = o.EmployeeId,
                        FrequencyId = o.FrequencyId,
                        ShiftTypeId = o.ShiftTypeId,
                        Id = o.Id,
                    }
                }).ToListAsync();

            return new PagedResultDto<GetEmployeeTimeInfoForViewDto>(
                totalCount,
                results
            );
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeeTimeInfo)]
        public async Task<GetEmployeeTimeInfoForViewDto> GetEmployeeTimeInfoForView(int id)
        {
            var employeeTimeInfo = await _employeeTimeInfoRepository.GetAsync(id);

            var output = new GetEmployeeTimeInfoForViewDto { EmployeeTimeInfo = ObjectMapper.Map<EmployeeTimeInfoDto>(employeeTimeInfo) };

            return output;
        }

        public async Task<GetEmployeeTimeInfoForViewDto> GetEmployeeTimeInfoByUserId(long id)
        {
            var employeeTimeInfo = await _employeeTimeInfoRepository.GetAll()
                .Where(e => e.Employees.User.Id == id)
                .FirstOrDefaultAsync();

            var output = new GetEmployeeTimeInfoForViewDto { EmployeeTimeInfo = ObjectMapper.Map<EmployeeTimeInfoDto>(employeeTimeInfo) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeeTimeInfo_Edit)]
        public async Task<GetEmployeeTimeInfoForEditOutput> GetEmployeeTimeInfoForEdit(EntityDto input)
        {
            var employeeTimeInfo = await _employeeTimeInfoRepository.GetAll()
                .Where(e => e.EmployeeId == input.Id)
                .FirstOrDefaultAsync();

            if (employeeTimeInfo == null)
            {
                employeeTimeInfo = new EmployeeTimeInfo();
            }

            var output = new GetEmployeeTimeInfoForEditOutput { EmployeeTimeInfo = ObjectMapper.Map<CreateOrEditEmployeeTimeInfoDto>(employeeTimeInfo) };

            if (employeeTimeInfo.RestDay != null)
            {
                output.EmployeeTimeInfo.Monday = employeeTimeInfo.RestDay.Contains("1");
                output.EmployeeTimeInfo.Tuesday = employeeTimeInfo.RestDay.Contains("2");
                output.EmployeeTimeInfo.Wednesday = employeeTimeInfo.RestDay.Contains("3");
                output.EmployeeTimeInfo.Thursday = employeeTimeInfo.RestDay.Contains("4");
                output.EmployeeTimeInfo.Friday = employeeTimeInfo.RestDay.Contains("5");
                output.EmployeeTimeInfo.Saturday = employeeTimeInfo.RestDay.Contains("6");
                output.EmployeeTimeInfo.Sunday = employeeTimeInfo.RestDay.Contains("7");
            }

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditEmployeeTimeInfoDto input)
        {
            input.Id = await _employeeTimeInfoRepository.GetAll()
                .Where(e => e.EmployeeId == input.EmployeeId)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            if (input.LocationId == 0)
            {
                input.LocationId = null;
            }

            if (await _employeeTimeInfoRepository.GetAll().AnyAsync(e => 
                e.TenantId == AbpSession.TenantId 
                && e.SwipeCode == input.SwipeCode
                && e.Id != input.Id))
            {
                throw new UserFriendlyException("Your request is not valid!", "An identical record already exists.");
            }

            if (input.Id == 0)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected static void SelectedRestDayToRestDayCode(CreateOrEditEmployeeTimeInfoDto input)
        {
            input.RestDay = string.Empty;

            if (input.Monday)
                input.RestDay += "1";

            if (input.Tuesday)
                input.RestDay += "2";

            if (input.Wednesday)
                input.RestDay += "3";

            if (input.Thursday)
                input.RestDay += "4";

            if (input.Friday)
                input.RestDay += "5";

            if (input.Saturday)
                input.RestDay += "6";

            if (input.Sunday)
                input.RestDay += "7";
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeeTimeInfo_Create)]
        protected virtual async Task Create(CreateOrEditEmployeeTimeInfoDto input)
        {
            SelectedRestDayToRestDayCode(input);

            var employeeTimeInfo = ObjectMapper.Map<EmployeeTimeInfo>(input);

            if (AbpSession.TenantId != null)
            {
                employeeTimeInfo.TenantId = AbpSession.TenantId.Value;
            }

            await _employeeTimeInfoRepository.InsertAsync(employeeTimeInfo);

        }

        [AbpAuthorize(AppPermissions.Pages_EmployeeTimeInfo_Edit)]
        protected virtual async Task Update(CreateOrEditEmployeeTimeInfoDto input)
        {
            SelectedRestDayToRestDayCode(input);
            input.LocationId = input.LocationId == 0 ? null : input.LocationId;
            var employeeTimeInfo = await _employeeTimeInfoRepository.FirstOrDefaultAsync((int)input.Id);

            await ValidateShift(input, employeeTimeInfo.ShiftTypeId);

            ObjectMapper.Map(input, employeeTimeInfo);
        }

        private async Task ValidateShift(CreateOrEditEmployeeTimeInfoDto input, int baseShiftTypeId)
        {
            var period = await _attendancePeriodsRepository.GetAll()
                .Where(e => e.FrequencyId == input.FrequencyId && e.Posted == false)
                .Select(e => new { e.StartDate, e.EndDate })
                .OrderBy(e => e.StartDate)
                .FirstOrDefaultAsync();

            var shiftRequests = await _shiftRequestsRepository.GetAll()
                .Where(e => e.EmployeeId == input.EmployeeId && e.Status.DisplayName == "Approved")
                .Where(e => (e.ShiftStart <= period.EndDate && e.ShiftStart >= period.StartDate) ||
                    (e.ShiftEnd >= period.StartDate && e.ShiftEnd <= period.EndDate) ||
                    (e.ShiftStart <= period.StartDate && e.ShiftEnd >= period.EndDate))
                .OrderByDescending(e => e.CreationTime)
                .Select(e => new
                {
                    e.ShiftStart, 
                    e.ShiftEnd, 
                    ShiftTypeDetails = e.ShiftTypes.ShiftTypeDetails
                        .Select(f => new ValidShiftDto 
                        { 
                            TimeIn = f.TimeIn,
                            TimeOut = f.TimeOut,
                            StartOnPreviousDay = f.StartOnPreviousDay
                        }).First(),
                }).ToListAsync();

            var shiftTypeDetails = await _shiftTypeDetailsRepository.GetAll()
                .Where(e => e.ShiftTypeId == input.ShiftTypeId || e.ShiftTypeId == baseShiftTypeId)
                .Select(e => new ValidShiftDto 
                { 
                    ShiftTypeId = e.ShiftTypeId,
                    TimeIn = e.TimeIn, 
                    TimeOut = e.TimeOut,
                    StartOnPreviousDay = e.StartOnPreviousDay 
                }).ToListAsync();

            var firstLoop = true;
            var dateRange = Enumerable.Range(0, 3 + period.EndDate.Subtract(period.StartDate).Days)
                .Select(e => period.StartDate.AddDays(e - 1))
                .ToList();

            var validatedTimeInfo = dateRange.Select(date =>
            {
                var request = shiftRequests
                    .Where(e => date >= e.ShiftStart && date <= e.ShiftEnd)
                    .Select(e => e.ShiftTypeDetails)
                    .FirstOrDefault();

                if (request == null)
                {
                    if (firstLoop)
                        request = shiftTypeDetails.Where(e => e.ShiftTypeId == baseShiftTypeId).First();
                    else
                        request = shiftTypeDetails.Where(e => e.ShiftTypeId == input.ShiftTypeId).First();
                }

                request.Date = date;

                if (firstLoop)
                    firstLoop = false;

                return AdjustTime(request);
            }).ToList();

            for (var i = 0; i < validatedTimeInfo.Count - 1; i++)
            {
                var prevTimeInfo = validatedTimeInfo[i];
                var nextTimeInfo = validatedTimeInfo[i + 1];

                if (prevTimeInfo.StartDate <= nextTimeInfo.EndDate && prevTimeInfo.EndDate >= nextTimeInfo.StartDate)
                {
                    throw new UserFriendlyException(L("YourRequestIsNotValid"), "Shift Type is overlapping with previous shift.");
                }
            }
        }

        private static ValidateEmployeeTimeInfoDto AdjustTime(ValidShiftDto validShift)
        {
            var adjustedDate = validShift.StartOnPreviousDay ? validShift.Date.AddDays(-1) : validShift.Date;
            var startTime = DateTime.ParseExact(validShift.TimeIn, "HH:mm", null);
            var endTime = DateTime.ParseExact(validShift.TimeOut, "HH:mm", null);
            var startDate = adjustedDate.Date.Add(startTime.TimeOfDay);
            var endDate = adjustedDate.Date.Add(endTime.TimeOfDay);

            return new ValidateEmployeeTimeInfoDto
            {
                ActualDate = validShift.Date,
                StartDate = startDate,
                EndDate = endDate
            };
        }

        [AbpAuthorize(AppPermissions.Pages_EmployeeTimeInfo_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _employeeTimeInfoRepository.DeleteAsync(input.Id);
        }

    }
}