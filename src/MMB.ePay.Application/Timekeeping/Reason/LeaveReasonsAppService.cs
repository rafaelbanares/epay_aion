﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class LeaveReasonsAppService : ePayAppServiceBase, ILeaveReasonsAppService
    {
        private readonly IRepository<LeaveReasons> _leaveReasonsRepository;

        public LeaveReasonsAppService(IRepository<LeaveReasons> leaveReasonsRepository) =>
            _leaveReasonsRepository = leaveReasonsRepository;

        public async Task<IList<SelectListItem>> GetLeaveReasonList()
        {
            var leaveReasonList = await _leaveReasonsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            leaveReasonList.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return leaveReasonList;
        }
    }
}