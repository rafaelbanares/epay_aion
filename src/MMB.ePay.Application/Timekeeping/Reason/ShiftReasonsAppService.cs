﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class ShiftReasonsAppService : ePayAppServiceBase, IShiftReasonsAppService
    {
        private readonly IRepository<ShiftReasons> _shiftReasonsRepository;

        public ShiftReasonsAppService(IRepository<ShiftReasons> shiftReasonsRepository) =>
            _shiftReasonsRepository = shiftReasonsRepository;

        public async Task<IList<SelectListItem>> GetShiftReasonList()
        {
            var shiftReasonList = await _shiftReasonsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            shiftReasonList.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return shiftReasonList;
        }
    }
}