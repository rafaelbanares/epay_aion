﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class RestDayReasonsAppService : ePayAppServiceBase, IRestDayReasonsAppService
    {
        private readonly IRepository<RestDayReasons> _restDayReasonsRepository;

        public RestDayReasonsAppService(IRepository<RestDayReasons> restDayReasonsRepository) =>
            _restDayReasonsRepository = restDayReasonsRepository;

        public async Task<IList<SelectListItem>> GetRestDayReasonList()
        {
            var restDayReasonList = await _restDayReasonsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            restDayReasonList.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return restDayReasonList;
        }
    }
}