﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class OvertimeReasonsAppService : ePayAppServiceBase, IOvertimeReasonsAppService
    {
        private readonly IRepository<OvertimeReasons> _overtimeReasonsRepository;

        public OvertimeReasonsAppService(IRepository<OvertimeReasons> overtimeReasonsRepository) =>
            _overtimeReasonsRepository = overtimeReasonsRepository;

        public async Task<IList<SelectListItem>> GetOvertimeReasonList()
        {
            var overtimeReasonList = await _overtimeReasonsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            overtimeReasonList.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return overtimeReasonList;
        }
    }
}