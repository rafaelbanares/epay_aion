﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class ExcuseReasonsAppService : ePayAppServiceBase, IExcuseReasonsAppService
    {
        private readonly IRepository<ExcuseReasons> _excuseReasonsRepository;

        public ExcuseReasonsAppService(IRepository<ExcuseReasons> excuseReasonsRepository) =>
            _excuseReasonsRepository = excuseReasonsRepository;

        public async Task<IList<SelectListItem>> GetExcuseReasonList()
        {
            var excuseReasonList = await _excuseReasonsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            excuseReasonList.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return excuseReasonList;
        }
    }
}