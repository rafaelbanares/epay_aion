﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class AttendanceReasonsAppService : ePayAppServiceBase, IAttendanceReasonsAppService
    {
        private readonly IRepository<AttendanceReasons> _attendanceReasonsRepository;

        public AttendanceReasonsAppService(IRepository<AttendanceReasons> attendanceReasonsRepository) =>
            _attendanceReasonsRepository = attendanceReasonsRepository;

        public async Task<IList<SelectListItem>> GetAttendanceReasonList()
        {
            var attendanceReasonList = await _attendanceReasonsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            attendanceReasonList.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return attendanceReasonList;
        }
    }
}