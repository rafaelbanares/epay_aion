﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class OfficialBusinessReasonsAppService : ePayAppServiceBase, IOfficialBusinessReasonsAppService
    {
        private readonly IRepository<OfficialBusinessReasons> _officialBusinessReasonsRepository;

        public OfficialBusinessReasonsAppService(IRepository<OfficialBusinessReasons> officialBusinessReasonsRepository) =>
            _officialBusinessReasonsRepository = officialBusinessReasonsRepository;

        public async Task<IList<SelectListItem>> GetOfficialBusinessReasonList()
        {
            var obReasonList = await _officialBusinessReasonsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            obReasonList.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return obReasonList;
        }
    }
}