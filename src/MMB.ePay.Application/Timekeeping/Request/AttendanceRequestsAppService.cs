﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Processor;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class AttendanceRequestsAppService : ePayAppServiceBase, IAttendanceRequestsAppService
    {
        private readonly IRepository<AttendanceRequests> _attendanceRequestsRepository;
        private readonly IRepository<AttendanceApprovals> _attendanceApprovalsRepository;
        private readonly IRepository<Attendances> _attendancesRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<EmployeeApprovers> _employeeApproversRepository;
        private readonly IStatusAppService _statusAppService;
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;
        private readonly IProcessorAppService _processorAppService;
        private readonly IShiftTypesAppService _shiftTypesAppService;
        private readonly IWorkFlowAppService _workFlowAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public AttendanceRequestsAppService(
            IRepository<AttendanceRequests> attendanceRequestsRepository,
            IRepository<AttendanceApprovals> attendanceApprovalsRepository,
            IRepository<Attendances> attendancesRepository,
            IRepository<Employees> employeesRepository,
            IRepository<EmployeeApprovers> employeeApproversRepository,
            IStatusAppService statusAppService,
            IAttendancePeriodsAppService attendancePeriodsAppService,
            IProcessorAppService processorAppService,
            IShiftTypesAppService shiftTypesAppService,
            IWorkFlowAppService workFlowAppService,
            IApprovalsAppService approvalsAppService
            )
        {
            _attendanceRequestsRepository = attendanceRequestsRepository;
            _attendanceApprovalsRepository = attendanceApprovalsRepository;
            _attendancesRepository = attendancesRepository;
            _employeesRepository = employeesRepository;
            _employeeApproversRepository = employeeApproversRepository;
            _statusAppService = statusAppService;
            _attendancePeriodsAppService = attendancePeriodsAppService;
            _processorAppService = processorAppService;
            _shiftTypesAppService = shiftTypesAppService;
            _workFlowAppService = workFlowAppService;
            _approvalsAppService = approvalsAppService;
        }

        [AbpAuthorize(AppPermissions.Pages_AttendanceRequests)]
        public async Task<PagedResultDto<GetAttendanceRequestsForViewDto>> GetAll(GetAllAttendanceRequestsInput input)
        {
            var periodId = input.AttendancePeriodIdFilter.GetValueOrDefault();
            var requests = _attendanceRequestsRepository.GetAll().AsNoTracking()
                .WhereIf(input.StatusIdFilter > 0, e => e.StatusId == input.StatusIdFilter)
                .Where(e => e.Employees.User.Id == AbpSession.UserId);

            if (periodId > 0)
            {
                var period = await _attendancePeriodsAppService.GetDateRangeFromAttendancePeriod(periodId);

                if (period != null)
                    requests = requests.WhereIf(period != null, e => e.Date >= period.Start && e.Date <= period.End);
            }

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(null);
            var totalCount = await requests.CountAsync();

            var pendingResult = await requests
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input)
                .Select(e => new
                {
                    e.Date,
                    e.TimeIn,
                    e.BreaktimeIn,
                    e.BreaktimeOut,
                    e.PMBreaktimeIn,
                    e.PMBreaktimeOut,
                    e.TimeOut,
                    Reason = e.AttendanceReason.DisplayName,
                    e.Remarks,
                    Status = e.Status.DisplayName,
                    e.Status.IsEditable,
                    e.Id
                }).ToListAsync();

            var result = pendingResult.Select(e => new GetAttendanceRequestsForViewDto
            {
                AttendanceRequests = new AttendanceRequestsDto
                {
                    Date = e.Date,
                    TimeIn = e.TimeIn,
                    BreaktimeIn = e.BreaktimeIn,
                    BreaktimeOut = e.BreaktimeOut,
                    PMBreaktimeIn = e.PMBreaktimeIn,
                    PMBreaktimeOut = e.PMBreaktimeOut,
                    TimeOut = e.TimeOut,
                    Reason = e.Reason,
                    Remarks = e.Remarks,
                    Status = e.Status,
                    IsEditable = e.IsEditable && e.Date > postedDate,
                    Id = e.Id
                }
            }).ToList();

            return new PagedResultDto<GetAttendanceRequestsForViewDto>(totalCount, result);
        }

        [AbpAuthorize(AppPermissions.Pages_AttendanceRequests)]
        public async Task<GetAttendanceRequestsForViewDto> GetAttendanceRequestsForView(int id)
        {
            var request = await _attendanceRequestsRepository.GetAll().AsNoTracking()
                .Where(e => e.Id == id)
                .Select(e => new
                {
                    AttendanceDetails = new AttendanceDetailsDto
                    {
                        Employee = e.Employees.FullName,
                        Date = e.Date,
                        TimeIn = e.TimeIn,
                        BreaktimeIn = e.BreaktimeIn,
                        BreaktimeOut = e.BreaktimeOut,
                        PMBreaktimeIn = e.PMBreaktimeIn,
                        PMBreaktimeOut = e.PMBreaktimeOut,
                        TimeOut = e.TimeOut,
                        Reason = e.AttendanceReason.DisplayName,
                        DateFiled = e.CreationTime,
                        Status = e.Status.DisplayName,
                        Id = e.Id
                    },
                    e.Reason,
                    e.StatusId,
                    e.Employees.EmployeeTimeInfo.FrequencyId
                }).FirstOrDefaultAsync();

            if (request == null)
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "The record doesn't exist.");

            var detail = request.AttendanceDetails;

            if (detail.Reason == null)
                detail.Reason += request.Reason;

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(request.FrequencyId);

            if (detail.Date > postedDate)
                detail.StatusList =
                    await _workFlowAppService.GetWorkFlowStatusList(request.StatusId);

            return new GetAttendanceRequestsForViewDto { AttendanceDetails = detail };
        }

        [AbpAuthorize(AppPermissions.Pages_AttendanceRequests_Edit)]
        public async Task<GetAttendanceRequestsForEditOutput> GetAttendanceRequestsForEdit(int id)
        {
            var request = await _attendanceRequestsRepository.GetAll().AsNoTracking()
                .Where(e => e.Id == id)
                .Select(e => new CreateOrEditAttendanceRequestsDto
                {
                    Date = e.Date,
                    TimeIn = e.TimeIn,
                    BreaktimeIn = e.BreaktimeIn,
                    BreaktimeOut = e.BreaktimeOut,
                    PMBreaktimeIn = e.PMBreaktimeIn,
                    PMBreaktimeOut = e.PMBreaktimeOut,
                    TimeOut = e.TimeOut,
                    EarlyLogin = e.EarlyLogin,
                    AttendanceReasonId = e.AttendanceReasonId,
                    Remarks = e.Remarks,
                    Id = e.Id
                }).FirstOrDefaultAsync();

            if (request == null)
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "The record doesn't exist.");

            return new GetAttendanceRequestsForEditOutput
            {
                AttendanceRequests = ObjectMapper.Map<CreateOrEditAttendanceRequestsDto>(request)
            };
        }

        [AbpAuthorize(AppPermissions.Pages_AttendanceRequests_Create)]
        public async Task<GetAttendanceRequestsForEditOutput> GetAttendanceRequestsForCreate(int id) =>
            await _attendancesRepository.GetAll().AsNoTracking()
                .Where(e => e.Id == id)
                .Select(e => new GetAttendanceRequestsForEditOutput
                {
                    AttendanceRequests = new CreateOrEditAttendanceRequestsDto
                    {
                        Date = e.Date,
                        AttendanceId = e.Id
                    }
                }).FirstOrDefaultAsync();

        public async Task CreateOrEdit(CreateOrEditAttendanceRequestsDto input)
        {
            var newDate = input.Date.Value;
            var prevDay = await _shiftTypesAppService.GetStartOnPreviousDayByDate(input.Date.Value);

            if (prevDay)
                newDate = newDate.AddDays(-1);

            input.TimeIn = newDate + input.TimeIn?.TimeOfDay;
            input.BreaktimeIn = newDate + input.BreaktimeIn?.TimeOfDay;
            input.BreaktimeOut = newDate + input.BreaktimeOut?.TimeOfDay;
            input.PMBreaktimeIn = newDate + input.PMBreaktimeIn?.TimeOfDay;
            input.PMBreaktimeOut = newDate + input.PMBreaktimeOut?.TimeOfDay;
            input.TimeOut = newDate + input.TimeOut?.TimeOfDay;

            if (input.TimeIn > input.TimeOut)
                input.TimeOut = input.TimeOut.Value.AddDays(1);

            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        [AbpAuthorize(AppPermissions.Pages_AttendanceRequests_Create)]
        private async Task Create(CreateOrEditAttendanceRequestsDto input)
        {
            var request = ObjectMapper.Map<AttendanceRequests>(input);
            var initialStatus = await _statusAppService.GetInitialStatus();
            var employee = await _employeesRepository.GetAll().AsNoTracking()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new { e.Id, e.EmployeeTimeInfo.FrequencyId })
                .FirstOrDefaultAsync();

            request.EmployeeId = employee.Id;
            request.StatusId = initialStatus.Status.Id;

            await Validate(input, employee.Id, employee.FrequencyId);
            await _attendanceRequestsRepository.InsertAsync(request);
        }

        [AbpAuthorize(AppPermissions.Pages_AttendanceRequests_Edit)]
        private async Task Update(CreateOrEditAttendanceRequestsDto input)
        {
            var request = await _attendanceRequestsRepository.GetAll().AsNoTracking()
                .Where(e => e.Id == input.Id)
                .Select(e => new
                {
                    Main = e,
                    e.Employees.EmployeeTimeInfo.FrequencyId
                }).FirstOrDefaultAsync();

            await Validate(input, request.Main.EmployeeId, request.FrequencyId);
            ObjectMapper.Map(input, request.Main);
        }

        private async Task Validate(CreateOrEditAttendanceRequestsDto input, int employeeId, int frequencyId)
        {
            if (!await _attendancesRepository.GetAll().AsNoTracking().AnyAsync(e => e.EmployeeId == employeeId && e.Date == input.Date))
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "No record in Timesheet.");

            if (await _attendanceRequestsRepository.GetAll().AsNoTracking()
                .AnyAsync(e => e.EmployeeId == employeeId && e.Date == input.Date && e.Id != input.Id))
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "An identical record already exists.");

            if (input.Date <= await _attendancePeriodsAppService.GetLatestPostedDate(frequencyId))
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Can't file dates before posted date.");

            if (input.Date >= DateTime.Today)
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Can't file past current date.");

            if (input.TimeIn > input.TimeIn)
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Invalid range of time.");
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_AttendanceRequests_Edit)]
        public async Task UpdateStatus(UpdateAttendanceRequestsStatus input)
        {
            var status = await _statusAppService.GetStatusDisplayName(input.StatusId);

            if (status.StatusDisplayName is StatusDisplayNames.ForUpdate
                or StatusDisplayNames.Declined or StatusDisplayNames.Cancelled)
            {
                if (string.IsNullOrWhiteSpace(input.Reason))
                    throw new UserFriendlyException(L("YourRequestIsNotValid"), "The Remarks field is required.");
            }

            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
            var approver = await _employeesRepository.GetAll().AsNoTracking()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new ApproverDto
                {
                    Id = e.Id,
                    Name = e.FullName,
                    FrequencyId = e.EmployeeTimeInfo.FrequencyId
                }).FirstOrDefaultAsync();

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(approver.FrequencyId);
            var request = await _attendanceRequestsRepository.GetAll().AsNoTracking()
                .WhereIf(postedDate != DateTime.MinValue, e => e.Date > postedDate)
                .Where(e => e.Id == input.Id)
                .Where(e => (e.Status.AccessLevel == 0 && e.EmployeeId == approver.Id)
                    || (e.Status.AccessLevel == e.Employees.EmployeeApproversApprover
                        .Where(f => f.ApproverId == approver.Id).First().ApproverOrders.Level))
                .Select(e => new
                {
                    Main = e,
                    e.Employees.EmployeeTimeInfo.FrequencyId
                }).FirstOrDefaultAsync();

            if (request != null)
            {
                request.Main.StatusId = input.StatusId;
                request.Main.Reason = input.Reason;
                await _attendanceRequestsRepository.UpdateAsync(request.Main);

                await CreateApproval(input);
               
                if (status.StatusDisplayName != StatusDisplayNames.Reopened)
                    await SendApprovalDetails(request.Main, status, approver);

                CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
                if (status.StatusDisplayName == StatusDisplayNames.Cancelled)
                {
                    var periodId = await _attendancePeriodsAppService.GetFirstUnpostedPeriodId(request.FrequencyId);

                    await _processorAppService.Process(periodId, request.Main.EmployeeId);
                }
            }
            else
                throw new UserFriendlyException(L("YourRequestIsNotValid"));
        }

        private async Task CreateApproval(UpdateAttendanceRequestsStatus input) =>
            await _attendanceApprovalsRepository.InsertAsync(new AttendanceApprovals
            {
                TenantId = AbpSession.TenantId.Value,
                Remarks = input.Reason,
                AttendanceRequestId = input.Id,
                StatusId = input.StatusId,
            });

        private async Task SendApprovalDetails(AttendanceRequests request, GetStatusForDisplayNameDto status, ApproverDto approver)
        {
            string email;

            if (status.StatusDisplayName == StatusDisplayNames.Cancelled)
            {
                email = await _employeeApproversRepository.GetAll().AsNoTracking()
                    .Where(e => e.EmployeeId == approver.Id && !e.ApproverOrders.IsBackup)
                    .OrderByDescending(e => e.ApproverOrders.Level)
                    .Select(e => e.Approvers.Email)
                    .FirstOrDefaultAsync();
            } 
            else
            {
                email = await _employeeApproversRepository.GetAll().AsNoTracking()
                    .Where(e => e.EmployeeId == approver.Id
                        && e.ApproverOrders.Level == 1 && !e.ApproverOrders.IsBackup)
                    .Select(e => e.Approvers.Email)
                    .FirstOrDefaultAsync();
            }

            var dto = new EmailRequestDto
            {
                Request = L("Attendance"),
                FullName = approver.Name,
                ApprovalStatus = status.AfterStatusMessage,
                Remarks = request.Remarks,
                Reason = request.Reason,
                StartDate = request.Date,
                Recipient = email
            };

            await _approvalsAppService.SendApprovalDetails(dto);
        }
    }
}