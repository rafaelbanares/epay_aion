﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Processor;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class OfficialBusinessRequestsAppService : ePayAppServiceBase, IOfficialBusinessRequestsAppService
    {
        private readonly IApprovalsAppService _approvalsAppService;
        private readonly IRepository<OfficialBusinessRequests> _officialBusinessRequestsRepository;
        private readonly IRepository<OfficialBusinessRequestDetails> _officialBusinessRequestDetailsRepository;
        private readonly IRepository<OfficialBusinessApprovals> _officialBusinessApprovalsRepository;
        private readonly IRepository<LeaveRequestDetails> _leaveRequestDetailsRepository;
        private readonly IRepository<RestDayRequests> _restDayRequestsRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<EmployeeApprovers> _employeeApproversRepository;
        private readonly IStatusAppService _statusAppService;
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;
        private readonly IProcessorAppService _processorAppService;
        private readonly IWorkFlowAppService _workFlowAppService;

        public OfficialBusinessRequestsAppService(IApprovalsAppService approvalsAppService,
            IRepository<OfficialBusinessRequests> officialBusinessRequestsRepository,
            IRepository<OfficialBusinessRequestDetails> officialBusinessRequestDetailsRepository,
            IRepository<OfficialBusinessApprovals> officialBusinessApprovalsRepository,
            IRepository<RestDayRequests> restDayRequestsRepository,
            IRepository<LeaveRequestDetails> leaveRequestDetailsRepository,
            IRepository<Employees> employeesRepository,
            IRepository<EmployeeApprovers> employeeApproversRepository,
            IStatusAppService statusAppService,
            IAttendancePeriodsAppService attendancePeriodsAppService,
            IProcessorAppService processorAppService,
            IWorkFlowAppService workFlowAppService)
        {
            _approvalsAppService = approvalsAppService;
            _officialBusinessRequestsRepository = officialBusinessRequestsRepository;
            _officialBusinessRequestDetailsRepository = officialBusinessRequestDetailsRepository;
            _officialBusinessApprovalsRepository = officialBusinessApprovalsRepository;
            _leaveRequestDetailsRepository = leaveRequestDetailsRepository;
            _restDayRequestsRepository = restDayRequestsRepository;
            _employeesRepository = employeesRepository;
            _employeeApproversRepository = employeeApproversRepository;
            _statusAppService = statusAppService;
            _attendancePeriodsAppService = attendancePeriodsAppService;
            _processorAppService = processorAppService;
            _workFlowAppService = workFlowAppService;
        }

        [AbpAuthorize(AppPermissions.Pages_OfficialBusinessRequests)]
        public async Task<PagedResultDto<GetOfficialBusinessRequestsForViewDto>> GetAll(GetAllOfficialBusinessRequestsInput input)
        {
            var periodId = input.AttendancePeriodIdFilter.GetValueOrDefault();
            var requests = _officialBusinessRequestsRepository.GetAll()
                .WhereIf(input.StatusIdFilter > 0, e => e.StatusId == input.StatusIdFilter)
                .Where(e => e.Employees.User.Id == AbpSession.UserId);

            if (periodId > 0)
            {
                var period = await _attendancePeriodsAppService.GetDateRangeFromAttendancePeriod(periodId);

                if (period != null)
                    requests = requests.Where(e => e.OfficialBusinessRequestDetails.Any(f => f.OfficialBusinessDate >= period.Start)
                        && e.OfficialBusinessRequestDetails.Any(f => f.OfficialBusinessDate <= period.End));
            }

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(null);
            var totalCount = await requests.CountAsync();

            var pendingResults = await requests
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input)
                .Select(e => new
                {
                    StartDate = e.OfficialBusinessRequestDetails.Min(f => f.OfficialBusinessDate),
                    EndDate = e.OfficialBusinessRequestDetails.Max(f => f.OfficialBusinessDate),
                    Reason = e.OfficialBusinessReasons.DisplayName,
                    e.Remarks,
                    Status = e.Status.DisplayName,
                    e.Status.IsEditable,
                    e.Id
                }).ToListAsync();

            var results = pendingResults.Select(e => new GetOfficialBusinessRequestsForViewDto
            {
                OfficialBusinessRequests = new OfficialBusinessRequestsDto
                {
                    StartDate = e.StartDate,
                    EndDate = e.EndDate,
                    Reason = e.Reason,
                    Remarks = e.Remarks,
                    Status = e.Status,
                    IsEditable = e.IsEditable && e.StartDate > postedDate,
                    Id = e.Id
                }
            }).ToList();

            return new PagedResultDto<GetOfficialBusinessRequestsForViewDto>(totalCount, results);
        }

        [AbpAuthorize(AppPermissions.Pages_OfficialBusinessRequests)]
        public async Task<GetOfficialBusinessRequestsForViewDto> GetOfficialBusinessRequestsForView(int id)
        {
            var request = await _officialBusinessRequestsRepository.GetAll()
                .Where(e => e.Id == id
                    && e.Employees.User.Id == AbpSession.UserId)
                .Select(e => new
                {
                    e.Status.AccessLevel,
                    Employee = e.Employees.FullName,
                    StartDate = e.OfficialBusinessRequestDetails.Min(f => f.OfficialBusinessDate),
                    EndDate = e.OfficialBusinessRequestDetails.Max(f => f.OfficialBusinessDate),
                    Details = e.OfficialBusinessRequestDetails.Select(f => f.OfficialBusinessDate.ToString("MM/dd/yyyy")).ToList(),
                    Reason = e.OfficialBusinessReasons.DisplayName + (e.Reason == null ? string.Empty : "; " + e.Reason),
                    e.Remarks,
                    DateFiled = e.CreationTime,
                    Status = e.Status.DisplayName,
                    e.StatusId,
                    e.Employees.EmployeeTimeInfo.FrequencyId,
                    e.Id
                }).FirstOrDefaultAsync();

            var actualDates = string.Join(", ", request.Details);

            if (request.Details.Count == 2)
                if (request.Details[0] == request.Details[1])
                    actualDates = request.Details[0];

            var output = new GetOfficialBusinessRequestsForViewDto
            {
                OfficialBusinessDetails = new OfficialBusinessDetailsDto
                {
                    Employee = request.Employee,
                    StartDate = request.StartDate,
                    EndDate = request.EndDate,
                    ActualDates = actualDates,
                    Reason = request.Reason,
                    Remarks = request.Remarks,
                    DateFiled = request.DateFiled,
                    Status = request.Status,
                    Id = request.Id
                }
            };

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(request.FrequencyId);

            if (request.EndDate > postedDate)
                output.OfficialBusinessDetails.StatusList = await _workFlowAppService.GetWorkFlowStatusList(request.StatusId);

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_OfficialBusinessRequests_Edit)]
        public async Task<GetOfficialBusinessRequestsForEditOutput> GetOfficialBusinessRequestsForEdit(int id) =>
            await _officialBusinessRequestsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetOfficialBusinessRequestsForEditOutput
                {
                    OfficialBusinessRequests = new CreateOrEditOfficialBusinessRequestsDto
                    {
                        StartDate = e.OfficialBusinessRequestDetails.Min(f => f.OfficialBusinessDate),
                        EndDate = e.OfficialBusinessRequestDetails.Max(f => f.OfficialBusinessDate),
                        WholeDay = e.OfficialBusinessRequestDetails.Count == 1,
                        OfficialBusinessReasonId = e.OfficialBusinessReasonId,
                        Remarks = e.Remarks,
                        Id = e.Id
                    }
                }).FirstOrDefaultAsync();

        public async Task CreateOrEdit(CreateOrEditOfficialBusinessRequestsDto input)
        {
            if (input.StartTime.HasValue && input.EndDate.HasValue)
            {
                input.StartDate = input.StartDate?.Date + input.StartTime;
                input.EndDate = input.EndDate?.Date + input.EndTime;
            }

            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        private async Task Validate(CreateOrEditOfficialBusinessRequestsDto input, int frequencyId)
        {
            //Invalid date range
            if (input.StartDate > input.EndDate)
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Invalid date range.");

            //Before posted date
            if (input.StartDate <= await _attendancePeriodsAppService.GetLatestPostedDate(frequencyId))
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Can't file dates before posted date.");

            //Overlapping active Leave Request
            var declinedStatus = await _statusAppService
                .GetStatusByDisplayNames(StatusDisplayNames.Cancelled, StatusDisplayNames.Declined);
            var declinedStatusIds = declinedStatus.Select(e => e.Status.Id).ToArray();

            var hasLeaveRequest = await _leaveRequestDetailsRepository.GetAll()
                .Where(e => e.LeaveRequests.Employees.User.Id == AbpSession.UserId
                    && !declinedStatusIds.Contains(e.LeaveRequests.StatusId)
                    && e.LeaveDate >= input.StartDate && e.LeaveDate <= input.EndDate)
                .AnyAsync();

            if (hasLeaveRequest)
                throw new UserFriendlyException(L("YourRequestIsNotValid"),
                    "Your request is overlapping with an existing Leave request.");

            //Overlapping pending RD Request
            //valid only if at least 1 good day
            var declinedIds = (await _statusAppService
                .GetStatusByDisplayNames(StatusDisplayNames.Cancelled, StatusDisplayNames.Declined))
                .Select(e => e.Status.Id)
                .ToArray();

            var requests = await _restDayRequestsRepository.GetAll()
                .Where(e => e.Employees.User.Id == AbpSession.UserId
                    && input.StartDate <= e.RestDayEnd && input.EndDate >= e.RestDayStart
                    && !declinedIds.Contains(e.StatusId))
                .Select(e => new { e.RestDayStart, e.RestDayEnd, e.RestDayCode })
                .ToListAsync();

            if (requests.Count > 0)
            {
                var dayOfWeeks = Enumerable.Range(0, 1 + input.EndDate.Value.Subtract(input.StartDate.Value).Days)
                  .Select(offset => new
                  {
                      input.StartDate.Value.AddDays(offset).Date,
                      DayOfWeek = (int)input.StartDate.Value.AddDays(offset).DayOfWeek
                  }).ToList();

                var formattedDoWs = dayOfWeeks.Select(e => new
                {
                    e.Date,
                    DayOfWeek = e.DayOfWeek == 0 ? "7" : e.DayOfWeek.ToString()
                }).ToList();

                var hasRestDayRequest = formattedDoWs.Any(e => requests.Any(f => f.RestDayStart <= e.Date
                    && f.RestDayEnd >= e.Date
                    && f.RestDayCode.Contains(e.DayOfWeek)));

                if (hasRestDayRequest)
                    throw new UserFriendlyException(L("YourRequestIsNotValid"),
                        "Your request is overlapping with an existing Rest Day request.");
            }
        }

        [AbpAuthorize(AppPermissions.Pages_OfficialBusinessRequests_Create)]
        private async Task Create(CreateOrEditOfficialBusinessRequestsDto input)
        {
            var request = ObjectMapper.Map<OfficialBusinessRequests>(input);
            var employee = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new { e.Id, e.EmployeeTimeInfo.FrequencyId })
                .FirstOrDefaultAsync();

            request.EmployeeId = employee.Id;
            request.StatusId = (await _statusAppService.GetInitialStatus()).Status.Id;

            input.Id = await _officialBusinessRequestsRepository.InsertAndGetIdAsync(request);
            await CreateDetails(input, employee.Id, employee.FrequencyId);
        }

        [AbpAuthorize(AppPermissions.Pages_OfficialBusinessRequestDetails_Create)]
        private async Task CreateDetails(CreateOrEditOfficialBusinessRequestsDto input, int employeeId, int frequencyId)
        {
            var start = input.StartDate.Value;
            var end = input.EndDate.Value;
            var range = new List<DateTime>();

            if (start.Date == end.Date)
            {
                if (input.StartDate > input.EndDate)
                    input.EndDate = input.EndDate.Value.AddDays(1);

                if (start == end)
                    range.Add(start);
                else
                {
                    range.Add(start);
                    range.Add(end);
                }
            }
            else
                range = Enumerable.Range(0, 1 + end.Subtract(start).Days)
                    .Select(e => start.AddDays(e))
                    .ToList();

            var details = range
                .Select(e => new OfficialBusinessRequestDetails
                {
                    EmployeeId = employeeId,
                    OfficialBusinessRequestId = (int)input.Id,
                    OfficialBusinessDate = e,
                }).ToList();

            await Validate(input, frequencyId);

            //var holidays = await _holidaysRepository.GetAll()
            //    .Where(e => e.TenantId == AbpSession.TenantId
            //        && e.Date >= input.StartDate && e.Date <= input.EndDate)
            //    .Select(e => e.Date)
            //    .ToListAsync();

            //var restDay = await _employeesRepository.GetAll()
            //    .Where(e => e.User.Id == AbpSession.UserId)
            //    .Select(e => e.EmployeeTimeInfo.RestDay)
            //    .FirstOrDefaultAsync();

            var finalCount = 0;

            //var restDayDates = new List<string>();
            //var holidayDates = new List<string>();

            foreach (var detail in details)
            {
                //if (restDay != null)
                //{
                //    var dayOfWeek = (int)detail.OfficialBusinessDate.DayOfWeek;
                //    var formattedDoW = dayOfWeek == 0 ? "7" : dayOfWeek.ToString();

                //    if (restDay.Contains(formattedDoW))
                //        continue;
                //    else
                //        restDayDates.Add(detail.OfficialBusinessDate.ToString("MM/dd/yyyy"));
                //}

                //if (holidays.Any(e => e == detail.OfficialBusinessDate))
                //    continue;
                //else
                //    holidayDates.Add(detail.OfficialBusinessDate.ToString("MM/dd/yyyy"));

                if (await _officialBusinessRequestDetailsRepository.GetAll()
                    .AnyAsync(e => e.EmployeeId == employeeId
                        && e.OfficialBusinessDate == detail.OfficialBusinessDate
                        && e.OfficialBusinessRequestId != input.Id))
                    throw new UserFriendlyException(L("YourRequestIsNotValid"), "An identical record already exists.");

                finalCount += 1;

                await _officialBusinessRequestDetailsRepository.InsertAsync(detail);
            }

            if (finalCount == 0)
            {
                var errorMessage = new StringBuilder();
                errorMessage.Append("Your request has no valid day.");

                //if (restDayDates.Count > 0)
                //{
                //    errorMessage.AppendLine();
                //    errorMessage.Append("The following dates are Rest Days: ");
                //    errorMessage.Append(string.Join(", ", restDayDates));
                //}

                //if (holidayDates.Count > 0)
                //{
                //    errorMessage.AppendLine();
                //    errorMessage.Append("The following dates are Holidays: ");
                //    errorMessage.Append(string.Join(",", holidayDates));
                //}

                throw new UserFriendlyException(L("YourRequestIsNotValid"),
                    errorMessage.ToString());
            }
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_OfficialBusinessRequests_Edit)]
        public async Task UpdateStatus(UpdateOfficialBusinessRequestsStatus input)
        {
            var status = await _statusAppService.GetStatusDisplayName(input.StatusId);

            if (status.StatusDisplayName is StatusDisplayNames.ForUpdate
                or StatusDisplayNames.Declined or StatusDisplayNames.Cancelled)
            {
                if (string.IsNullOrWhiteSpace(input.Reason))
                    throw new UserFriendlyException(L("YourRequestIsNotValid"), "The Remarks field is required.");
            }

            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
            var approver = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new ApproverDto
                {
                    Id = e.Id,
                    Name = e.FullName,
                    FrequencyId = e.EmployeeTimeInfo.FrequencyId
                }).FirstOrDefaultAsync();

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(approver.FrequencyId);
            var request = await _officialBusinessRequestsRepository.GetAll()
                .Include(e => e.Employees)
                    .ThenInclude(e => e.EmployeeTimeInfo)
                .Include(e => e.OfficialBusinessRequestDetails)
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .WhereIf(postedDate != DateTime.MinValue, e => e.OfficialBusinessRequestDetails.Min(f => f.OfficialBusinessDate) > postedDate)
                .Where(e => e.Id == input.Id)
                .Where(e => (e.Status.AccessLevel == 0 && e.EmployeeId == approver.Id)
                    || (e.Status.AccessLevel == e.Employees.EmployeeApproversApprover
                        .Where(f => f.ApproverId == approver.Id).First().ApproverOrders.Level))
                .Select(e => new
                {
                    Main = e,
                    e.Employees.EmployeeTimeInfo.FrequencyId
                }).FirstOrDefaultAsync();

            if (request != null)
            {
                request.Main.StatusId = input.StatusId;
                request.Main.Reason = input.Reason;
                await _officialBusinessRequestsRepository.UpdateAsync(request.Main);

                await CreateApproval(input);

                if (status.StatusDisplayName is not StatusDisplayNames.Reopened)
                    await SendApprovalDetails(request.Main, status, approver);

                CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
                if (status.StatusDisplayName == StatusDisplayNames.Cancelled)
                {
                    var periodId = await _attendancePeriodsAppService.GetFirstUnpostedPeriodId(request.FrequencyId);

                    await _processorAppService.Process(periodId, request.Main.EmployeeId);
                }
            }
            else
                throw new UserFriendlyException(L("YourRequestIsNotValid"));
        }

        private async Task CreateApproval(UpdateOfficialBusinessRequestsStatus input)
        {
            var approvals = new OfficialBusinessApprovals
            {
                TenantId = AbpSession.TenantId.Value,
                Remarks = input.Reason,
                OfficialBusinessRequestId = input.Id,
                StatusId = input.StatusId,
            };

            await _officialBusinessApprovalsRepository.InsertAsync(approvals);
        }

        [AbpAuthorize(AppPermissions.Pages_OfficialBusinessRequests_Edit)]
        private async Task Update(CreateOrEditOfficialBusinessRequestsDto input)
        {
            var request = await _officialBusinessRequestsRepository.GetAll()
                .Include(e => e.Employees)
                    .ThenInclude(e => e.EmployeeTimeInfo)
                .Where(e => e.Id == input.Id)
                .FirstOrDefaultAsync();

            ObjectMapper.Map(input, request);

            await DeleteDetails((int)input.Id);
            await CreateDetails(input, request.EmployeeId, request.Employees.EmployeeTimeInfo.FrequencyId);
        }

        [AbpAuthorize(AppPermissions.Pages_OfficialBusinessRequestDetails_Delete)]
        private async Task DeleteDetails(int officialBusinessRequestId)
        {
            var obDetailIdList = await _officialBusinessRequestDetailsRepository.GetAll()
                .Where(e => e.OfficialBusinessRequestId == officialBusinessRequestId)
                .Select(e => e.Id)
                .ToListAsync();

            foreach (var id in obDetailIdList)
                await _officialBusinessRequestDetailsRepository.DeleteAsync(id);
        }

        private async Task SendApprovalDetails(OfficialBusinessRequests request, GetStatusForDisplayNameDto status, ApproverDto approver)
        {
            string email;

            if (status.StatusDisplayName == StatusDisplayNames.Cancelled)
            {
                email = await _employeeApproversRepository.GetAll()
                    .Where(e => e.EmployeeId == approver.Id && !e.ApproverOrders.IsBackup)
                    .OrderByDescending(e => e.ApproverOrders.Level)
                    .Select(e => e.Approvers.Email)
                    .FirstOrDefaultAsync();
            }
            else
            {
                email = await _employeeApproversRepository.GetAll()
                    .Where(e => e.EmployeeId == approver.Id
                        && e.ApproverOrders.Level == 1 && !e.ApproverOrders.IsBackup)
                    .Select(e => e.Approvers.Email)
                    .FirstOrDefaultAsync();
            }

            var dto = new EmailRequestDto
            {
                Request = L("OfficialBusiness"),
                FullName = approver.Name,
                ApprovalStatus = status.AfterStatusMessage,
                Remarks = request.Remarks,
                Reason = request.Reason,
                StartDate = request.OfficialBusinessRequestDetails.Min(f => f.OfficialBusinessDate),
                EndDate = request.OfficialBusinessRequestDetails.Max(f => f.OfficialBusinessDate),
                Recipient = email
            };

            await _approvalsAppService.SendApprovalDetails(dto);
        }
    }
}