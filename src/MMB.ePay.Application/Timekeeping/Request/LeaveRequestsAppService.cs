﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.HR;
using MMB.ePay.Processor;
using MMB.ePay.Processor.Dtos;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class LeaveRequestsAppService : ePayAppServiceBase, ILeaveRequestsAppService
    {
        private readonly IApprovalsAppService _approvalsAppService;
        private readonly IRepository<LeaveRequests> _leaveRequestsRepository;
        private readonly IRepository<LeaveRequestDetails> _leaveRequestDetailsRepository;
        private readonly IRepository<LeaveTypes> _leaveTypesRepository;
        private readonly IRepository<LeaveApprovals> _leaveApprovalsRepository;
        private readonly IRepository<LeaveEarningDetails> _leaveEarningDetailsRepository;
        private readonly IRepository<RestDayRequests> _restDayRequestsRepository;
        private readonly IRepository<Holidays> _holidaysRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<EmployeeApprovers> _employeeApproversRepository;
        private readonly IStatusAppService _statusAppService;
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;
        private readonly IProcessorAppService _processorAppService;
        private readonly IDbContextProvider<ePayDbContext> _dbContextProvider;
        private readonly IWorkFlowAppService _workFlowAppService;

        public LeaveRequestsAppService(IApprovalsAppService approvalsAppService,
            IRepository<LeaveRequests> leaveRequestsRepository,
            IRepository<LeaveRequestDetails> leaveRequestDetailsRepository,
            IRepository<LeaveTypes> leaveTypesRepository,
            IRepository<LeaveApprovals> leaveApprovalsRepository,
            IRepository<LeaveEarningDetails> leaveEarningDetailsRepository,
            IRepository<RestDayRequests> restDayRequestsRepository,
            IRepository<Holidays> holidaysRepository,
            IRepository<Employees> employeesRepository,
            IRepository<EmployeeApprovers> employeeApproversRepository,
            IStatusAppService statusAppService,
            IAttendancePeriodsAppService attendancePeriodsAppService,
            IProcessorAppService processorAppService,
            IDbContextProvider<ePayDbContext> dbContextProvider,
            IWorkFlowAppService workFlowAppService)
        {
            _approvalsAppService = approvalsAppService;
            _leaveRequestsRepository = leaveRequestsRepository;
            _leaveRequestDetailsRepository = leaveRequestDetailsRepository;
            _leaveTypesRepository = leaveTypesRepository;
            _leaveApprovalsRepository = leaveApprovalsRepository;
            _leaveEarningDetailsRepository = leaveEarningDetailsRepository;
            _restDayRequestsRepository = restDayRequestsRepository;
            _holidaysRepository = holidaysRepository;
            _employeesRepository = employeesRepository;
            _employeeApproversRepository = employeeApproversRepository;
            _statusAppService = statusAppService;
            _attendancePeriodsAppService = attendancePeriodsAppService;
            _processorAppService = processorAppService;
            _dbContextProvider = dbContextProvider;
            _workFlowAppService = workFlowAppService;
        }

        public async Task<int> GetPendingLeaveRequestCount(DateRange dateRange)
        {
            return await _leaveRequestsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.LeaveRequestDetails.Any(f => f.LeaveDate >= dateRange.Start && f.LeaveDate <= dateRange.End)
                    && !(e.Status.IsEditable == false && e.Status.AccessLevel == 0))
                .CountAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveRequests)]
        public async Task<PagedResultDto<GetLeaveRequestsForViewDto>> GetAll(GetAllLeaveRequestsInput input)
        {
            var periodId = input.AttendancePeriodIdFilter.GetValueOrDefault();
            var requests = _leaveRequestsRepository.GetAll().AsNoTracking()
                .WhereIf(input.StatusIdFilter > 0, e => e.StatusId == input.StatusIdFilter)
                .Where(e => e.Employees.User.Id == AbpSession.UserId);

            if (periodId > 0)
            {
                var period = await _attendancePeriodsAppService.GetDateRangeFromAttendancePeriod(periodId);

                if (period != null)
                    requests = requests.Where(e => e.LeaveRequestDetails
                        .Any(f => f.LeaveDate >= period.Start && f.LeaveDate <= period.End));
            }

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(null);
            var totalCount = await requests.CountAsync();

            var pendingResult = await requests
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input)
                .Select(e => new 
                {
                    LeaveType = e.LeaveTypes.DisplayName,
                    StartDate = e.LeaveRequestDetails.Min(f => f.LeaveDate),
                    EndDate = e.LeaveRequestDetails.Max(f => f.LeaveDate),
                    Reason = e.LeaveReasons.DisplayName,
                    e.Remarks,
                    Status = e.Status.DisplayName,
                    e.Status.IsEditable,
                    e.Id
                }).ToListAsync();


            var result = pendingResult.Select(e => new GetLeaveRequestsForViewDto
            {
                LeaveRequests = new LeaveRequestsDto
                {
                    LeaveType = e.LeaveType,
                    StartDate = e.StartDate,
                    EndDate = e.EndDate,
                    Reason = e.Reason,
                    Remarks = e.Remarks,
                    Status = e.Status,
                    IsEditable = e.IsEditable && e.StartDate > postedDate,
                    Id = e.Id
                }
            }).ToList();

            return new PagedResultDto<GetLeaveRequestsForViewDto>(totalCount, result);
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveRequests)]
        public async Task<GetLeaveRequestsForViewDto> GetLeaveRequestsForView(int id)
        {
            var request = await _leaveRequestsRepository.GetAll()
                .Where(e => e.Id == id && e.Employees.User.Id == AbpSession.UserId)
                .Select(e => new
                {
                    e.Status.AccessLevel,
                    Employee = e.Employees.FullName,
                    LeaveType = e.LeaveTypes.DisplayName,
                    StartDate = e.LeaveRequestDetails.Min(f => f.LeaveDate),
                    EndDate = e.LeaveRequestDetails.Max(f => f.LeaveDate),
                    Days = e.LeaveRequestDetails.Sum(f => f.Days),
                    ActualDates = string.Join(", ", e.LeaveRequestDetails.Select(f => f.LeaveDate.ToString("MM/dd/yyyy")).ToList()),
                    Reason = e.LeaveReasons.DisplayName + (e.Reason == null ? string.Empty : "; " + e.Reason),
                    e.Remarks,
                    DateFiled = e.CreationTime,
                    Status = e.Status.DisplayName,
                    e.StatusId,
                    e.Employees.EmployeeTimeInfo.FrequencyId,
                    e.Id
                }).FirstOrDefaultAsync();

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(request.FrequencyId);
            var output = new GetLeaveRequestsForViewDto
            {
                LeaveDetails = new LeaveDetailsDto
                {
                    Employee = request.Employee,
                    LeaveType = request.LeaveType,
                    StartDate = request.StartDate,
                    EndDate = request.EndDate,
                    Days = request.Days,
                    ActualDates = request.ActualDates,
                    Reason = request.Reason,
                    Remarks = request.Remarks,
                    DateFiled = request.DateFiled,
                    Status = request.Status,
                    Id = request.Id
                }
            };

            if (request.EndDate > postedDate)
                output.LeaveDetails.StatusList = await _workFlowAppService.GetWorkFlowStatusList(request.StatusId);

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveRequests_Edit)]
        public async Task<GetLeaveRequestsForEditOutput> GetLeaveRequestsForEdit(int id) =>
            await _leaveRequestsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetLeaveRequestsForEditOutput
                {
                    LeaveRequests = new CreateOrEditLeaveRequestsDto
                    {
                        LeaveTypeId = e.LeaveTypeId,
                        StartDate = e.LeaveRequestDetails.Min(f => f.LeaveDate),
                        EndDate = e.LeaveRequestDetails.Max(f => f.LeaveDate),
                        HalfDay = e.LeaveRequestDetails.Sum(f => f.Days) == (decimal)0.5,
                        FirstHalf = e.LeaveRequestDetails.First().FirstHalf,
                        LeaveReasonId = e.LeaveReasonId,
                        Remarks = e.Remarks,
                        Id = e.Id
                    }
                }).FirstOrDefaultAsync();

        public async Task CreateOrEdit(CreateOrEditLeaveRequestsDto input)
        {
            if (input.FirstHalf || input.HalfDay || input.EndDate == null)
                input.EndDate = input.StartDate;

            await Validate(input);

            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        private async Task<IList<DateTime>> GetValidDays(CreateOrEditLeaveRequestsDto input)
        {
            var declinedIds = (await _statusAppService
                .GetStatusByDisplayNames(StatusDisplayNames.Cancelled, StatusDisplayNames.Declined))
                .Select(e => e.Status.Id)
                .ToArray();

            //Rest Day Requests
            var requests = await _restDayRequestsRepository.GetAll()
                .Where(e => e.Employees.User.Id == AbpSession.UserId
                    && input.StartDate <= e.RestDayEnd && input.EndDate >= e.RestDayStart
                    && !declinedIds.Contains(e.StatusId))
                .Select(e => new { e.RestDayStart, e.RestDayEnd, e.RestDayCode })
                .ToListAsync();

            //RestDays in EmployeeTimeInfo
            var defaultRestDays = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => e.EmployeeTimeInfo.RestDay)
                .FirstOrDefaultAsync();

            //Holidays
            var holidays = await _holidaysRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.Date >= input.StartDate && e.Date <= input.EndDate)
                .Select(e => e.Date)
                .ToListAsync();

            var leaveDates = Enumerable.Range(0, 1 + input.EndDate.Value.Subtract(input.StartDate.Value).Days)
                  .Select(offset =>
                  {
                      var dayOfWeek = (int)input.StartDate.Value.AddDays(offset).DayOfWeek;

                      return new
                      {
                          input.StartDate.Value.AddDays(offset).Date,
                          DayOfWeek = dayOfWeek == 0 ? "7" : dayOfWeek.ToString()
                      };
                  }).ToList();

            var restDayDates = new List<string>();
            var holidayDates = new List<string>();

            var validDays = leaveDates
                .Select(e =>
                {
                    var restDayRequest = requests
                        .FirstOrDefault(f => f.RestDayStart <= e.Date && f.RestDayEnd >= e.Date);

                    var hasRestDay = restDayRequest != null ?
                        restDayRequest.RestDayCode.Contains(e.DayOfWeek) :
                        defaultRestDays.Contains(e.DayOfWeek);

                    var hasHoliday = holidays.Any(f => f == e.Date);

                    if (hasRestDay)
                    {
                        restDayDates.Add(e.Date.ToString("MM/dd/yyyy"));
                        return new DateTime();
                    }

                    if (hasHoliday)
                    {
                        holidayDates.Add(e.Date.ToString("MM/dd/yyyy"));
                        return new DateTime();
                    }

                    return e.Date;
                })
                .Where(e => e != DateTime.MinValue)
                .ToList();


            if (validDays.Count == 0)
            {
                var errorMessage = new StringBuilder();
                errorMessage.Append("Your request has no valid day.");

                if (restDayDates.Count > 0)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append("The following dates are Rest Days: ");
                    errorMessage.Append(string.Join(", ", restDayDates));
                }

                if (holidayDates.Count > 0)
                {
                    errorMessage.AppendLine();
                    errorMessage.Append("The following dates are Holidays: ");
                    errorMessage.Append(string.Join(",", holidayDates));
                }

                throw new UserFriendlyException(L("YourRequestIsNotValid"),
                    errorMessage.ToString());
            }

            return validDays;
        }

        private async Task Validate(CreateOrEditLeaveRequestsDto input)
        {
            //Invalid date range
            if (input.StartDate > input.EndDate)
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Invalid date range.");

            //Before posted date
            if (input.StartDate <= await _attendancePeriodsAppService.GetLatestPostedDate(null))
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Can't file dates before posted date.");

            //Same Date Filing on AdvancedFiling enabled
            if (await _leaveRequestsRepository.GetAll()
                .WhereIf(input.Id != null, e => e.Id != input.Id)
                .Where(e => e.Employees.User.Id == AbpSession.UserId
                    && e.LeaveTypes.AdvancedFilingInDays > 0
                    && e.LeaveRequestDetails.Any(f => input.StartDate <= f.LeaveDate && input.EndDate >= f.LeaveDate))
                .AnyAsync())
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Existing Leave is already applied on the selected date(s).");

            //Advanced Filing in Days
            var currentDate = DateTime.Today;
            var advancedFilingInDays = await _leaveTypesRepository.GetAll()
                .Where(e => e.Id == input.LeaveTypeId)
                .Select(e => e.AdvancedFilingInDays)
                .FirstOrDefaultAsync();

            if (advancedFilingInDays != null && advancedFilingInDays != 0)
            {
                var advancedDate = currentDate.AddDays(advancedFilingInDays ?? 0);

                if (input.StartDate < advancedDate)
                    throw new UserFriendlyException(L("YourRequestIsNotValid"), "Filing for past dates is not allowed; ensure submissions are made in advance.");
            }

            //Insufficient Balance
            var declinedIds = (await _statusAppService
                .GetStatusByDisplayNames(StatusDisplayNames.Cancelled, StatusDisplayNames.Declined))
                .Select(e => e.Status.Id)
                .ToArray();

            var year = input.StartDate.Value.Year;

            var requests = await _leaveRequestsRepository.GetAll()
                .WhereIf(input.Id != null, e => e.Id != input.Id)
                .Where(e => e.Employees.User.Id == AbpSession.UserId
                    && !declinedIds.Contains(e.StatusId)
                    && e.LeaveTypeId == input.LeaveTypeId
                    && e.LeaveRequestDetails.Any(f => f.LeaveDate.Year == year))
                .Select(e => e.LeaveRequestDetails.Sum(f => f.Days))
                .ToListAsync();

            var leaveEarnings = await _leaveEarningDetailsRepository.GetAll()
                .Where(e => e.LeaveEarnings.AppYear == year
                    && e.Employees.User.Id == AbpSession.UserId
                    && e.LeaveTypeId == input.LeaveTypeId)
                .Select(e => e.Earned)
                .ToListAsync();

            var leaveBalance = leaveEarnings.Sum() - requests.Sum();

            var leaveUsed = input.HalfDay ? (decimal)0.5 :
                (decimal)(input.EndDate - input.StartDate).Value.TotalDays + 1;

            if (leaveUsed > leaveBalance)
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Insufficient available balance.");
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveRequests_Create)]
        private async Task Create(CreateOrEditLeaveRequestsDto input)
        {
            var request = ObjectMapper.Map<LeaveRequests>(input);
            request.EmployeeId = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            request.StatusId = (await _statusAppService.GetInitialStatus()).Status.Id;

            input.Id = await _leaveRequestsRepository.InsertAndGetIdAsync(request);
            await CreateDetails(input, request.EmployeeId);
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveRequestDetails_Create)]
        private async Task CreateDetails(CreateOrEditLeaveRequestsDto input, int employeeId)
        {
            var validDays = await GetValidDays(input);
            var leaveRequestDetails = validDays
                .Select(e => new LeaveRequestDetails
                {
                    EmployeeId = employeeId,
                    LeaveRequestId = (int)input.Id,
                    LeaveDate = e.Date,
                    Days = input.HalfDay ? (decimal)0.5 : 1,
                    FirstHalf = input.FirstHalf
                }).ToList();

            var hasLeaveRequests = await _leaveRequestDetailsRepository.GetAll()
                .Where(e => e.Employees.User.Id == AbpSession.UserId
                    && validDays.Contains(e.LeaveDate)
                    && e.LeaveRequestId != input.Id)
                .AnyAsync();

            if (hasLeaveRequests)
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "An identical record already exists.");

            var dbContext = _dbContextProvider.GetDbContext();
            await dbContext.LeaveRequestDetails.AddRangeAsync(leaveRequestDetails);
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_LeaveRequests_Edit)]
        public async Task UpdateStatus(UpdateLeaveRequestsStatus input)
        {
            var status = await _statusAppService.GetStatusDisplayName(input.StatusId);

            if (status.StatusDisplayName is StatusDisplayNames.ForUpdate
                or StatusDisplayNames.Declined or StatusDisplayNames.Cancelled)
            {
                if (string.IsNullOrWhiteSpace(input.Reason))
                    throw new UserFriendlyException(L("YourRequestIsNotValid"), "The Remarks field is required.");
            }

            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
            var approver = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new ApproverDto
                {
                    Id = e.Id,
                    Name = e.FullName,
                    FrequencyId = e.EmployeeTimeInfo.FrequencyId
                }).FirstOrDefaultAsync();

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(approver.FrequencyId);
            var request = await _leaveRequestsRepository.GetAll()
                .Include(e => e.Employees)
                    .ThenInclude(e => e.EmployeeTimeInfo)
                .Include(e => e.LeaveRequestDetails)
                .WhereIf(postedDate != DateTime.MinValue, e => e.LeaveRequestDetails.Min(f => f.LeaveDate) > postedDate)
                .Where(e => e.Id == input.Id)
                .Where(e => (e.Status.AccessLevel == 0 && e.EmployeeId == approver.Id)
                    || (e.Status.AccessLevel == e.Employees.EmployeeApproversApprover
                        .Where(f => f.ApproverId == approver.Id).First().ApproverOrders.Level))
                .Select(e => new
                {
                    Main = e,
                    e.Employees.EmployeeTimeInfo.FrequencyId
                }).FirstOrDefaultAsync();

            if (request != null)
            {
                request.Main.StatusId = input.StatusId;
                request.Main.Reason = input.Reason;
                await _leaveRequestsRepository.UpdateAsync(request.Main);

                await CreateApproval(input);

                if (status.StatusDisplayName is not StatusDisplayNames.Reopened)
                    await SendApprovalDetails(request.Main, status, approver);

                CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
                if (status.StatusDisplayName == StatusDisplayNames.Cancelled)
                {
                    var periodId = await _attendancePeriodsAppService.GetFirstUnpostedPeriodId(request.FrequencyId);

                    await _processorAppService.Process(periodId, request.Main.EmployeeId);
                }
            }
            else
                throw new UserFriendlyException(L("YourRequestIsNotValid"));
        }

        private async Task CreateApproval(UpdateLeaveRequestsStatus input)
        {
            var approvals = new LeaveApprovals
            {
                TenantId = AbpSession.TenantId.Value,
                Remarks = input.Reason,
                LeaveRequestId = input.Id,
                StatusId = input.StatusId,
            };

            await _leaveApprovalsRepository.InsertAsync(approvals);
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveRequests_Edit)]
        private async Task Update(CreateOrEditLeaveRequestsDto input)
        {
            var leaveRequests = await _leaveRequestsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, leaveRequests);

            await DeleteDetails((int)input.Id);
            await CreateDetails(input, leaveRequests.EmployeeId);
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveRequestDetails_Delete)]
        private async Task DeleteDetails(int leaveRequestId)
        {
            var leaveRequestDetails = await _leaveRequestDetailsRepository.GetAll()
                .Where(e => e.LeaveRequestId == leaveRequestId)
                .ToListAsync();

            var dbContext = _dbContextProvider.GetDbContext();
            dbContext.LeaveRequestDetails.RemoveRange(leaveRequestDetails);
        }

        private async Task SendApprovalDetails(LeaveRequests request, GetStatusForDisplayNameDto status, ApproverDto approver)
        {
            string email;

            if (status.StatusDisplayName == StatusDisplayNames.Cancelled)
            {
                email = await _employeeApproversRepository.GetAll()
                    .Where(e => e.EmployeeId == approver.Id && !e.ApproverOrders.IsBackup)
                    .OrderByDescending(e => e.ApproverOrders.Level)
                    .Select(e => e.Approvers.Email)
                    .FirstOrDefaultAsync();
            }
            else
            {
                email = await _employeeApproversRepository.GetAll()
                    .Where(e => e.EmployeeId == approver.Id
                        && e.ApproverOrders.Level == 1 && !e.ApproverOrders.IsBackup)
                    .Select(e => e.Approvers.Email)
                    .FirstOrDefaultAsync();
            }

            var dto = new EmailRequestDto
            {
                Request = L("Leave"),
                FullName = approver.Name,
                ApprovalStatus = status.AfterStatusMessage,
                Remarks = request.Remarks,
                Reason = request.Reason,
                StartDate = request.LeaveRequestDetails.Min(f => f.LeaveDate),
                EndDate = request.LeaveRequestDetails.Max(f => f.LeaveDate),
                Recipient = email
            };

            await _approvalsAppService.SendApprovalDetails(dto);
        }
    }
}