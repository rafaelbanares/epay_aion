﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize(AppPermissions.Pages_OfficialBusinessRequestDetails)]
    public class OfficialBusinessRequestDetailsAppService : ePayAppServiceBase, IOfficialBusinessRequestDetailsAppService
    {
        private readonly IRepository<OfficialBusinessRequestDetails> _obRequestDetailsRepository;

        public OfficialBusinessRequestDetailsAppService(IRepository<OfficialBusinessRequestDetails> officialBusinessRequestDetailsRepository) =>
            _obRequestDetailsRepository = officialBusinessRequestDetailsRepository;

        public async Task<GetOfficialBusinessRequestDetailsForViewDto> GetOfficialBusinessRequestDetailsForView(int id)
        {
            var requestDetail = await _obRequestDetailsRepository.GetAsync(id);

            return new GetOfficialBusinessRequestDetailsForViewDto 
            { 
                OfficialBusinessRequestDetails = ObjectMapper.Map<OfficialBusinessRequestDetailsDto>(requestDetail) 
            };
        }

        [AbpAuthorize(AppPermissions.Pages_OfficialBusinessRequestDetails_Edit)]
        public async Task<GetOfficialBusinessRequestDetailsForEditOutput> GetOfficialBusinessRequestDetailsForEdit(EntityDto input)
        {
            var requestDetail = await _obRequestDetailsRepository.FirstOrDefaultAsync(input.Id);

            return new GetOfficialBusinessRequestDetailsForEditOutput 
            { 
                OfficialBusinessRequestDetails = ObjectMapper.Map<CreateOrEditOfficialBusinessRequestDetailsDto>(requestDetail) 
            };
        }

        public async Task CreateOrEdit(CreateOrEditOfficialBusinessRequestDetailsDto input)
        {
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        [AbpAuthorize(AppPermissions.Pages_OfficialBusinessRequestDetails_Create)]
        protected virtual async Task Create(CreateOrEditOfficialBusinessRequestDetailsDto input)
        {
            var requesttDetail = ObjectMapper.Map<OfficialBusinessRequestDetails>(input);
            await _obRequestDetailsRepository.InsertAsync(requesttDetail);
        }

        [AbpAuthorize(AppPermissions.Pages_OfficialBusinessRequestDetails_Edit)]
        protected virtual async Task Update(CreateOrEditOfficialBusinessRequestDetailsDto input)
        {
            var requestDetail = await _obRequestDetailsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, requestDetail);
        }

        [AbpAuthorize(AppPermissions.Pages_OfficialBusinessRequestDetails_Delete)]
        public async Task Delete(int id) =>
            await _obRequestDetailsRepository.DeleteAsync(id);
    }
}