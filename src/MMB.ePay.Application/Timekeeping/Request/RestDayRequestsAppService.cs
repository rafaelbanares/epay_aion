﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Processor;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class RestDayRequestsAppService : ePayAppServiceBase, IRestDayRequestsAppService
    {
        private readonly IApprovalsAppService _approvalsAppService;
        private readonly IRepository<RestDayRequests> _restDayRequestsRepository;
        private readonly IRepository<RestDayApprovals> _restDayApprovalsRepository;
        private readonly IRepository<LeaveRequestDetails> _leaveRequestDetailsRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<EmployeeApprovers> _employeeApproversRepository;
        private readonly IStatusAppService _statusAppService;
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;
        private readonly IProcessorAppService _processorAppService;
        private readonly IWorkFlowAppService _workFlowAppService;

        public RestDayRequestsAppService(IApprovalsAppService approvalsAppService,
            IRepository<RestDayRequests> restDayRequestsRepository,
            IRepository<RestDayApprovals> restDayApprovalsRepository,
            IRepository<LeaveRequestDetails> leaveRequestDetailsRepository,
            IRepository<Employees> employeesRepository,
            IRepository<EmployeeApprovers> employeeApproversRepository,
            IStatusAppService statusAppService,
            IAttendancePeriodsAppService attendancePeriodsAppService,
            IProcessorAppService processorAppService,
            IWorkFlowAppService workFlowAppService)
        {
            _approvalsAppService = approvalsAppService;
            _restDayRequestsRepository = restDayRequestsRepository;
            _restDayApprovalsRepository = restDayApprovalsRepository;
            _leaveRequestDetailsRepository = leaveRequestDetailsRepository;
            _employeesRepository = employeesRepository;
            _employeeApproversRepository = employeeApproversRepository;
            _statusAppService = statusAppService;
            _attendancePeriodsAppService = attendancePeriodsAppService;
            _processorAppService = processorAppService;
            _workFlowAppService = workFlowAppService;
        }

        [AbpAuthorize(AppPermissions.Pages_RestDayRequests)]
        public async Task<PagedResultDto<GetRestDayRequestsForViewDto>> GetAll(GetAllRestDayRequestsInput input)
        {
            var periodId = input.AttendancePeriodIdFilter.GetValueOrDefault();
            var requests = _restDayRequestsRepository.GetAll()
                .WhereIf(input.StatusIdFilter > 0, e => e.StatusId == input.StatusIdFilter)
                .Where(e => e.Employees.User.Id == AbpSession.UserId);

            if (periodId > 0)
            {
                var period = await _attendancePeriodsAppService.GetDateRangeFromAttendancePeriod(periodId);

                if (period != null)
                {
                    requests = requests.Where(e => (e.RestDayStart >= period.Start && e.RestDayStart <= period.End)
                        || (e.RestDayEnd >= period.Start && e.RestDayEnd <= period.End));
                }
            }

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(null);
            var totalCount = await requests.CountAsync();

            var pendingResults = await requests
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input)
                .Select(e => new 
                {
                    e.RestDayStart,
                    e.RestDayEnd,
                    e.RestDayCode,
                    Reason = e.RestDayReasons.DisplayName,
                    e.Remarks,
                    Status = e.Status.DisplayName,
                    e.Status.IsEditable,
                    e.Id
                }).ToListAsync();

            var results = pendingResults
                .Select(e => new GetRestDayRequestsForViewDto
                {
                    RestDayRequests = new RestDayRequestsDto
                    {
                        RestDayStart = e.RestDayStart,
                        RestDayEnd = e.RestDayEnd,
                        Reason = e.Reason,
                        Remarks = e.Remarks,
                        Status = e.Status,
                        IsEditable = e.IsEditable && e.RestDayStart > postedDate,
                        RestDays = string.Join(", ", e.RestDayCode.ToCharArray()
                            .Select(f => (int)char.GetNumericValue(f))
                            .Select(f => Enum.GetName(typeof(DayOfWeek), f == 7 ? 0 : f).Substring(0, 3))),
                        Id = e.Id
                    }
                }).ToList();

            return new PagedResultDto<GetRestDayRequestsForViewDto>(totalCount, results);
        }

        [AbpAuthorize(AppPermissions.Pages_RestDayRequests)]
        public async Task<GetRestDayRequestsForViewDto> GetRestDayRequestsForView(int id)
        {
            var request = await _restDayRequestsRepository.GetAll()
                .Where(e => e.Id == id)
                .Where(e => e.Employees.User.Id == AbpSession.UserId)
                .Select(e => new
                {
                    e.Status.AccessLevel,
                    Employee = e.Employees.FullName,
                    e.RestDayEnd,
                    e.RestDayStart,
                    RestDay = e.RestDayCode,
                    Reason = e.RestDayReasons.DisplayName + (e.Reason == null ? string.Empty : "; " + e.Reason),
                    e.Remarks,
                    DateFiled = e.CreationTime,
                    Status = e.Status.DisplayName,
                    e.StatusId,
                    e.Employees.EmployeeTimeInfo.FrequencyId,
                    e.Id
                }).FirstOrDefaultAsync();

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(request.FrequencyId);
            var output = new GetRestDayRequestsForViewDto
            {
                RestDayDetails = new RestDayDetailsDto
                {
                    Employee = request.Employee,
                    RestDayStart = request.RestDayStart,
                    RestDayEnd = request.RestDayEnd,
                    RestDay = string.Join(", ", request.RestDay.ToCharArray()
                        .Select(f => (int)char.GetNumericValue(f))
                        .Select(f => Enum.GetName(typeof(DayOfWeek), f == 7 ? 0 : f).Substring(0, 3))),
                    Reason = request.Reason,
                    Remarks = request.Remarks,
                    DateFiled = request.DateFiled,
                    Status = request.Status,
                    Id = request.Id
                }
            };

            if (request.RestDayEnd > postedDate)
                output.RestDayDetails.StatusList = await _workFlowAppService.GetWorkFlowStatusList(request.StatusId);

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_RestDayRequests_Edit)]
        public async Task<GetRestDayRequestsForEditOutput> GetRestDayRequestsForEdit(int id)
        {
            var request = await _restDayRequestsRepository.FirstOrDefaultAsync(id);
            var output = new GetRestDayRequestsForEditOutput { RestDayRequests = ObjectMapper.Map<CreateOrEditRestDayRequestsDto>(request) };
            var restDayCode = request.RestDayCode;
            var requestDto = output.RestDayRequests;

            requestDto.Monday = restDayCode.Contains("1");
            requestDto.Tuesday = restDayCode.Contains("2");
            requestDto.Wednesday = restDayCode.Contains("3");
            requestDto.Thursday = restDayCode.Contains("4");
            requestDto.Friday = restDayCode.Contains("5");
            requestDto.Saturday = restDayCode.Contains("6");
            requestDto.Sunday = restDayCode.Contains("7");

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditRestDayRequestsDto input)
        {
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        private async Task Validate(CreateOrEditRestDayRequestsDto input, int employeeId, int frequencyId)
        {
            if (input.RestDayStart > input.RestDayEnd)
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Invalid date range.");

            if (await _restDayRequestsRepository.GetAll()
                .AnyAsync(e => e.EmployeeId == employeeId && e.Id != input.Id
                    && e.RestDayStart == input.RestDayStart
                    && e.RestDayEnd == input.RestDayEnd))
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "An identical record already exists.");

            //Apply leave request validation
            var requests = await _leaveRequestDetailsRepository.GetAll()
                .Where(e => e.Employees.User.Id == AbpSession.UserId
                    & input.RestDayStart <= e.LeaveDate && input.RestDayEnd >= e.LeaveDate)
                .Select(e => new { e.LeaveDate, DayOfWeek = (int)e.LeaveDate.DayOfWeek })
                .ToListAsync();

            var formattedRequests = requests.Select(e => new
            {
                e.LeaveDate,
                DayOfWeek = e.DayOfWeek == 0 ? "7" : e.DayOfWeek.ToString()
            }).ToList();

            var hasLeaveRequests = formattedRequests.Any(e => input.RestDayCode.Contains(e.DayOfWeek));

            if (hasLeaveRequests)
                throw new UserFriendlyException(L("YourRequestIsNotValid"),
                    "Your request is overlapping with an existing Leave request.");

            if (input.RestDayStart <= await _attendancePeriodsAppService.GetLatestPostedDate(frequencyId))
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Can't file dates before posted date.");
        }

        private static void SelectedRestDayToRestDayCode(CreateOrEditRestDayRequestsDto input)
        {
            input.RestDayCode = string.Empty;

            if (input.Monday) input.RestDayCode += "1";
            if (input.Tuesday) input.RestDayCode += "2";
            if (input.Wednesday) input.RestDayCode += "3";
            if (input.Thursday) input.RestDayCode += "4";
            if (input.Friday) input.RestDayCode += "5";
            if (input.Saturday) input.RestDayCode += "6";
            if (input.Sunday) input.RestDayCode += "7";
        }

        [AbpAuthorize(AppPermissions.Pages_RestDayRequests_Create)]
        private async Task Create(CreateOrEditRestDayRequestsDto input)
        {
            SelectedRestDayToRestDayCode(input);

            var request = ObjectMapper.Map<RestDayRequests>(input);
            var employee = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new { e.Id, e.EmployeeTimeInfo.FrequencyId })
                .FirstOrDefaultAsync();

            request.EmployeeId = employee.Id;
            request.StatusId = (await _statusAppService.GetInitialStatus()).Status.Id;

            await Validate(input, employee.Id, employee.FrequencyId);
            await _restDayRequestsRepository.InsertAsync(request);
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_RestDayRequests_Edit)]
        public async Task UpdateStatus(UpdateRestDayRequestsStatus input)
        {
            var status = await _statusAppService.GetStatusDisplayName(input.StatusId);

            if (status.StatusDisplayName is StatusDisplayNames.ForUpdate
                or StatusDisplayNames.Declined or StatusDisplayNames.Cancelled)
            {
                if (string.IsNullOrWhiteSpace(input.Reason))
                    throw new UserFriendlyException(L("YourRequestIsNotValid"), "The Remarks field is required.");
            }

            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
            var approver = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new ApproverDto
                {
                    Id = e.Id,
                    Name = e.FullName,
                    FrequencyId = e.EmployeeTimeInfo.FrequencyId
                }).FirstOrDefaultAsync();

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(approver.FrequencyId);
            var request = await _restDayRequestsRepository.GetAll()
                .Include(e => e.Employees)
                    .ThenInclude(e => e.EmployeeTimeInfo)
                .WhereIf(postedDate != DateTime.MinValue, e => e.RestDayEnd > postedDate)
                .Where(e => e.Id == input.Id)
                .Where(e => (e.Status.AccessLevel == 0 && e.EmployeeId == approver.Id)
                    || (e.Status.AccessLevel == e.Employees.EmployeeApproversApprover
                        .Where(f => f.ApproverId == approver.Id).First().ApproverOrders.Level))
                .Select(e => new
                {
                    Main = e,
                    e.Employees.EmployeeTimeInfo.FrequencyId
                }).FirstOrDefaultAsync();

            if (request != null)
            {
                request.Main.StatusId = input.StatusId;
                request.Main.Reason = input.Reason;
                await _restDayRequestsRepository.UpdateAsync(request.Main);

                await CreateApproval(input);

                if (status.StatusDisplayName is not StatusDisplayNames.Cancelled or StatusDisplayNames.Reopened)
                    await SendApprovalDetails(request.Main, status, approver);

                await UnitOfWorkManager.Current.SaveChangesAsync();
                if (status.StatusDisplayName == StatusDisplayNames.Cancelled)
                {
                    var periodId = await _attendancePeriodsAppService.GetFirstUnpostedPeriodId(request.FrequencyId);

                    await _processorAppService.Process(periodId, request.Main.EmployeeId);
                }
            }
            else
                throw new UserFriendlyException(L("YourRequestIsNotValid"));
        }

        private async Task CreateApproval(UpdateRestDayRequestsStatus input)
        {
            var approvals = new RestDayApprovals
            {
                TenantId = AbpSession.TenantId.Value,
                Remarks = input.Reason,
                RestDayRequestId = input.Id,
                StatusId = input.StatusId,
            };

            await _restDayApprovalsRepository.InsertAsync(approvals);
        }

        [AbpAuthorize(AppPermissions.Pages_RestDayRequests_Edit)]
        private async Task Update(CreateOrEditRestDayRequestsDto input)
        {
            SelectedRestDayToRestDayCode(input);

            var request = await _restDayRequestsRepository.GetAll()
                .Include(e => e.Employees)
                    .ThenInclude(e => e.EmployeeTimeInfo)
                .Where(e => e.Id == input.Id)
                .FirstOrDefaultAsync();

            await Validate(input, request.EmployeeId, request.Employees.EmployeeTimeInfo.FrequencyId);
            ObjectMapper.Map(input, request);
        }

        private async Task SendApprovalDetails(RestDayRequests request, GetStatusForDisplayNameDto status, ApproverDto approver)
        {
            string email;

            if (status.StatusDisplayName == StatusDisplayNames.Cancelled)
            {
                email = await _employeeApproversRepository.GetAll()
                    .Where(e => e.EmployeeId == approver.Id && !e.ApproverOrders.IsBackup)
                    .OrderByDescending(e => e.ApproverOrders.Level)
                    .Select(e => e.Approvers.Email)
                    .FirstOrDefaultAsync();
            }
            else
            {
                email = await _employeeApproversRepository.GetAll()
                    .Where(e => e.EmployeeId == approver.Id
                        && e.ApproverOrders.Level == 1 && !e.ApproverOrders.IsBackup)
                    .Select(e => e.Approvers.Email)
                    .FirstOrDefaultAsync();
            }

            var dto = new EmailRequestDto
            {
                Request = L("RestDay"),
                FullName = approver.Name,
                ApprovalStatus = status.AfterStatusMessage,
                Remarks = request.Remarks,
                Reason = request.Reason,
                StartDate = request.RestDayStart,
                EndDate = request.RestDayEnd,
                Recipient = email
            };

            await _approvalsAppService.SendApprovalDetails(dto);
        }
    }
}