﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Processor;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class ShiftRequestsAppService : ePayAppServiceBase, IShiftRequestsAppService
    {
        private readonly IRepository<ShiftRequests> _shiftRequestsRepository;
        private readonly IRepository<ShiftApprovals> _shiftApprovalsRepository;
        private readonly IRepository<ShiftTypes> _shiftTypesRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<EmployeeApprovers> _employeeApproversRepository;
        private readonly IStatusAppService _statusAppService;
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;
        private readonly IProcessorAppService _processorAppService;
        private readonly IWorkFlowAppService _workFlowAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public ShiftRequestsAppService(
            IRepository<ShiftRequests> shiftRequestsRepository,
            IRepository<ShiftApprovals> shiftApprovalsRepository,
            IRepository<ShiftTypes> shiftTypesRepository,
            IRepository<Employees> employeesRepository,
            IRepository<EmployeeApprovers> employeeApproversRepository,
            IStatusAppService statusAppService,
            IAttendancePeriodsAppService attendancePeriodsAppService,
            IProcessorAppService processorAppService,
            IWorkFlowAppService workFlowAppService,
            IApprovalsAppService approvalsAppService
            )
        {
            _shiftRequestsRepository = shiftRequestsRepository;
            _shiftApprovalsRepository = shiftApprovalsRepository;
            _shiftTypesRepository = shiftTypesRepository;
            _employeesRepository = employeesRepository;
            _employeeApproversRepository = employeeApproversRepository;
            _statusAppService = statusAppService;
            _attendancePeriodsAppService = attendancePeriodsAppService;
            _processorAppService = processorAppService;
            _workFlowAppService = workFlowAppService;
            _approvalsAppService = approvalsAppService;
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftRequests)]
        public async Task<PagedResultDto<GetShiftRequestsForViewDto>> GetAll(GetAllShiftRequestsInput input)
        {
            var periodId = input.AttendancePeriodIdFilter.GetValueOrDefault();
            var requests = _shiftRequestsRepository.GetAll()
                .WhereIf(input.StatusIdFilter > 0, e => e.StatusId == input.StatusIdFilter)
                .Where(e => e.Employees.User.Id == AbpSession.UserId);

            if (periodId > 0)
            {
                var period = await _attendancePeriodsAppService.GetDateRangeFromAttendancePeriod(periodId);

                if (period != null)
                    requests = requests.Where(e => (e.ShiftStart >= period.Start && e.ShiftStart <= period.End)
                        || (e.ShiftEnd >= period.Start && e.ShiftEnd <= period.End));
            }

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(null);
            var totalCount = await requests.CountAsync();

            var pendingResult = await requests
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input)
                .Select(e => new 
                {
                    ShiftType = e.ShiftTypes.DisplayName,
                    e.ShiftStart,
                    e.ShiftEnd,
                    Reason = e.ShiftReasons.DisplayName,
                    e.Remarks,
                    Status = e.Status.DisplayName,
                    e.Status.IsEditable,
                    e.Id
                }).ToListAsync();

            var result = pendingResult
                .Select(e => new GetShiftRequestsForViewDto
                {
                    ShiftRequests = new ShiftRequestsDto
                    {
                        ShiftType = e.ShiftType,
                        ShiftStart = e.ShiftStart,
                        ShiftEnd = e.ShiftEnd,
                        Reason = e.Reason,
                        Remarks = e.Remarks,
                        Status = e.Status,
                        IsEditable = e.IsEditable && e.ShiftStart > postedDate,
                        Id = e.Id
                    }
                }).ToList();

            return new PagedResultDto<GetShiftRequestsForViewDto>(totalCount, result);
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftRequests)]
        public async Task<GetShiftRequestsForViewDto> GetShiftRequestsForView(int id)
        {
            var request = await _shiftRequestsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new
                {
                    e.Status.AccessLevel,
                    Employee = e.Employees.FullName,
                    ShiftType = e.ShiftTypes.DisplayName,
                    e.ShiftStart,
                    e.ShiftEnd,
                    Reason = e.ShiftReasons.DisplayName + (e.Reason == null ? string.Empty : "; " + e.Reason),
                    e.Remarks,
                    DateFiled = e.CreationTime,
                    Status = e.Status.DisplayName,
                    e.StatusId,
                    e.Employees.EmployeeTimeInfo.FrequencyId,
                    e.Id
                }).FirstOrDefaultAsync();

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(request.FrequencyId);
            var output = new GetShiftRequestsForViewDto
            {
                ShiftDetails = new ShiftDetailsDto
                {
                    Employee = request.Employee,
                    ShiftType = request.ShiftType,
                    StartDate = request.ShiftStart,
                    EndDate = request.ShiftEnd,
                    Reason = request.Reason,
                    Remarks = request.Remarks,
                    DateFiled = request.DateFiled,
                    Status = request.Status,
                    Id = request.Id
                }
            };

            if (request.ShiftEnd > postedDate)
                output.ShiftDetails.StatusList = await _workFlowAppService.GetWorkFlowStatusList(request.StatusId);

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftRequests_Edit)]
        public async Task<GetShiftRequestsForEditOutput> GetShiftRequestsForEdit(int id)
        {
            var request = await _shiftRequestsRepository.FirstOrDefaultAsync(id);
            var output = new GetShiftRequestsForEditOutput { ShiftRequests = ObjectMapper.Map<CreateOrEditShiftRequestsDto>(request) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditShiftRequestsDto input)
        {
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        private static DateRangeDto AdjustShift(ShiftDto shift)
        {
            var dateRange = new DateRangeDto
            {
                StartDate = shift.StartDate,
                EndDate = shift.EndDate
            };

            if (shift.StartDate > shift.EndDate)
                dateRange.EndDate = shift.EndDate.AddDays(1);

            return dateRange;
        }

        private async Task Validate(CreateOrEditShiftRequestsDto input, int employeeId, int frequencyId)
        {
            if (input.ShiftStart > input.ShiftEnd)
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Invalid date range.");

            if (await _shiftRequestsRepository.GetAll()
                .AnyAsync(e => e.TenantId == AbpSession.TenantId && e.Id != input.Id && e.EmployeeId == employeeId
                    && e.ShiftStart == input.ShiftStart && e.ShiftEnd == input.ShiftEnd))
            {
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "An identical record already exists.");
            };

            if (input.ShiftStart <= await _attendancePeriodsAppService.GetLatestPostedDate(frequencyId))
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Can't file dates before posted date.");

            var firstDate = (DateTime)input.ShiftStart;
            var lastDate = (DateTime)input.ShiftEnd;
            var prevDate = firstDate.AddDays(-1);
            var nextDate = lastDate.AddDays(1);

            var shiftTypes = await _shiftTypesRepository.GetAll()
                .Where(e => e.Id == input.ShiftTypeId)
                .Select(e => e.ShiftTypeDetails)
                .FirstOrDefaultAsync();

            var firstDOW = (int)firstDate.DayOfWeek;
            var firstShift = shiftTypes
                .Where(e => e.Days.Contains((firstDOW == 0 ? 7 : firstDOW).ToString()))
                .Select(e => new ShiftDto
                {
                    Date = e.StartOnPreviousDay ? firstDate.AddDays(-1) : firstDate,
                    TimeIn = e.TimeIn,
                    TimeOut = e.TimeOut
                }).First();

            var lastDOW = (int)lastDate.DayOfWeek;
            var lastShift = shiftTypes
                .Where(e => e.Days.Contains((lastDOW == 0 ? 7 : lastDOW).ToString()))
                .Select(e => new ShiftDto
                {
                    Date = e.StartOnPreviousDay ? lastDate.AddDays(-1) : lastDate,
                    TimeIn = e.TimeIn,
                    TimeOut = e.TimeOut
                }).First();

            var statusList = await _statusAppService.GetStatusByDisplayNames(StatusDisplayNames.Approved);
            var approvedId = statusList.First().Status.Id;

            var shiftRequests = await _shiftRequestsRepository.GetAll()
                .Where(e => e.EmployeeId == employeeId && 
                    e.StatusId == approvedId && 
                    ((e.ShiftStart <= prevDate && e.ShiftEnd >= prevDate) || 
                    (e.ShiftStart <= nextDate && e.ShiftEnd >= nextDate)))
                .Select(e => new
                {
                    e.ShiftStart,
                    e.ShiftEnd,
                    ShiftDetails = e.ShiftTypes.ShiftTypeDetails
                }).ToListAsync();

            var prevShiftRequest = shiftRequests.FirstOrDefault(e => e.ShiftStart <= prevDate && e.ShiftEnd >= prevDate)?.ShiftDetails;
            var nextShiftRequest = shiftRequests.FirstOrDefault(e => e.ShiftStart <= nextDate && e.ShiftEnd >= nextDate)?.ShiftDetails;

            var employeeShift = await _employeesRepository.GetAll()
                .Where(e => e.Id == employeeId)
                .Select(e => e.EmployeeTimeInfo.ShiftTypes.ShiftTypeDetails)
                .FirstOrDefaultAsync();

            if (prevShiftRequest == null)
                prevShiftRequest = employeeShift;

            if (nextShiftRequest == null)
                nextShiftRequest = employeeShift;

            var prevDOW = (int)prevDate.DayOfWeek;
            var prevShift = prevShiftRequest
                .Where(e => e.Days.Contains((prevDOW == 0 ? 7 : prevDOW).ToString()))
                .Select(e => new ShiftDto
                {
                    Date = e.StartOnPreviousDay ? prevDate.AddDays(-1) : prevDate,
                    TimeIn = e.TimeIn,
                    TimeOut = e.TimeOut
                }).First();

            var nextDOW = (int)prevDate.DayOfWeek;
            var nextShift = nextShiftRequest
                .Where(e => e.Days.Contains((nextDOW == 0 ? 7 : nextDOW).ToString()))
                .Select(e => new ShiftDto
                {
                    Date = e.StartOnPreviousDay ? nextDate.AddDays(-1) : nextDate,
                    TimeIn = e.TimeIn,
                    TimeOut = e.TimeOut
                })
                .First();

            var first = AdjustShift(firstShift);
            var last = AdjustShift(lastShift);
            var prev = AdjustShift(prevShift);
            var next = AdjustShift(nextShift);

            if (prev.EndDate > first.StartDate)
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Shift Type is overlapping with previous shift.");

            if (next.StartDate < last.EndDate)
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Shift Type is overlapping with next shift.");
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftRequests_Create)]
        private async Task Create(CreateOrEditShiftRequestsDto input)
        {
            var request = ObjectMapper.Map<ShiftRequests>(input);
            var employee = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new { e.Id, e.EmployeeTimeInfo.FrequencyId })
                .FirstOrDefaultAsync();

            request.EmployeeId = employee.Id;
            request.StatusId = (await _statusAppService.GetInitialStatus()).Status.Id;

            await Validate(input, employee.Id, employee.FrequencyId);
            await _shiftRequestsRepository.InsertAsync(request);
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_ShiftRequests_Edit)]
        public async Task UpdateStatus(UpdateShiftRequestsStatus input)
        {
            var status = await _statusAppService.GetStatusDisplayName(input.StatusId);

            if (status.StatusDisplayName is StatusDisplayNames.ForUpdate
                or StatusDisplayNames.Declined or StatusDisplayNames.Cancelled)
            {
                if (string.IsNullOrWhiteSpace(input.Reason))
                    throw new UserFriendlyException(L("YourRequestIsNotValid"), "The Remarks field is required.");
            }

            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
            var approver = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new ApproverDto
                {
                    Id = e.Id,
                    Name = e.FullName,
                    FrequencyId = e.EmployeeTimeInfo.FrequencyId
                }).FirstOrDefaultAsync();

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(approver.FrequencyId);
            var request = await _shiftRequestsRepository.GetAll()
                .Include(e => e.Employees)
                    .ThenInclude(e => e.EmployeeTimeInfo)
                .WhereIf(postedDate != DateTime.MinValue, e => e.ShiftStart > postedDate)
                .Where(e => e.Id == input.Id)
                .Where(e => (e.Status.AccessLevel == 0 && e.EmployeeId == approver.Id)
                    || (e.Status.AccessLevel == e.Employees.EmployeeApproversApprover
                        .Where(f => f.ApproverId == approver.Id).First().ApproverOrders.Level))
                .Select(e => new
                {
                    Main = e,
                    e.Employees.EmployeeTimeInfo.FrequencyId
                }).FirstOrDefaultAsync();

            if (request != null)
            {
                request.Main.StatusId = input.StatusId;
                request.Main.Reason = input.Reason;
                await _shiftRequestsRepository.UpdateAsync(request.Main);

                await CreateApproval(input);

                if (status.StatusDisplayName is not StatusDisplayNames.Reopened)
                    await SendApprovalDetails(request.Main, status, approver);

                await UnitOfWorkManager.Current.SaveChangesAsync();
                if (status.StatusDisplayName == StatusDisplayNames.Cancelled)
                {
                    var periodId = await _attendancePeriodsAppService.GetFirstUnpostedPeriodId(request.FrequencyId);

                    await _processorAppService.Process(periodId,request.Main.EmployeeId);
                }
            }
            else
                throw new UserFriendlyException(L("YourRequestIsNotValid"));
        }

        private async Task CreateApproval(UpdateShiftRequestsStatus input)
        {
            var approvals = new ShiftApprovals
            {
                TenantId = AbpSession.TenantId.Value,
                Remarks = input.Reason,
                ShiftRequestId = input.Id,
                StatusId = input.StatusId,
            };

            await _shiftApprovalsRepository.InsertAsync(approvals);
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftRequests_Edit)]
        private async Task Update(CreateOrEditShiftRequestsDto input)
        {
            var shiftRequests = await _shiftRequestsRepository.GetAll()
                .Include(e => e.Employees)
                    .ThenInclude(e => e.EmployeeTimeInfo)
                .Where(e => e.Id == input.Id)
                .FirstOrDefaultAsync();

            await Validate(input, shiftRequests.EmployeeId, shiftRequests.Employees.EmployeeTimeInfo.FrequencyId);
            ObjectMapper.Map(input, shiftRequests);
        }

        private async Task SendApprovalDetails(ShiftRequests request, GetStatusForDisplayNameDto status, ApproverDto approver)
        {
            string email;

            if (status.StatusDisplayName == StatusDisplayNames.Cancelled)
            {
                email = await _employeeApproversRepository.GetAll()
                    .Where(e => e.EmployeeId == approver.Id && !e.ApproverOrders.IsBackup)
                    .OrderByDescending(e => e.ApproverOrders.Level)
                    .Select(e => e.Approvers.Email)
                    .FirstOrDefaultAsync();
            }
            else
            {
                email = await _employeeApproversRepository.GetAll()
                    .Where(e => e.EmployeeId == approver.Id
                        && e.ApproverOrders.Level == 1 && !e.ApproverOrders.IsBackup)
                    .Select(e => e.Approvers.Email)
                    .FirstOrDefaultAsync();
            }

            var dto = new EmailRequestDto
            {
                Request = L("Shift"),
                FullName = approver.Name,
                ApprovalStatus = status.AfterStatusMessage,
                Remarks = request.Remarks,
                Reason = request.Reason,
                StartDate = request.ShiftStart,
                EndDate = request.ShiftEnd,
                Recipient = email
            };

            await _approvalsAppService.SendApprovalDetails(dto);
        }
    }
}