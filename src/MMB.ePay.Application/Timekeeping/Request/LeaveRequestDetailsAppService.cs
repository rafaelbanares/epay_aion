﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize(AppPermissions.Pages_LeaveRequestDetails)]
    public class LeaveRequestDetailsAppService : ePayAppServiceBase, ILeaveRequestDetailsAppService
    {
        private readonly IRepository<LeaveRequestDetails> _leaveRequestDetailsRepository;

        public LeaveRequestDetailsAppService(IRepository<LeaveRequestDetails> leaveRequestDetailsRepository) =>
            _leaveRequestDetailsRepository = leaveRequestDetailsRepository;

        public async Task<GetLeaveRequestDetailsForViewDto> GetLeaveRequestDetailsForView(int id)
        {
            var requestDetail = await _leaveRequestDetailsRepository.GetAsync(id);

            return new GetLeaveRequestDetailsForViewDto 
            { 
                LeaveRequestDetails = ObjectMapper.Map<LeaveRequestDetailsDto>(requestDetail) 
            };
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveRequestDetails_Edit)]
        public async Task<GetLeaveRequestDetailsForEditOutput> GetLeaveRequestDetailsForEdit(EntityDto input)
        {
            var requestDetail = await _leaveRequestDetailsRepository.FirstOrDefaultAsync(input.Id);

            return new GetLeaveRequestDetailsForEditOutput 
            { 
                LeaveRequestDetails = ObjectMapper.Map<CreateOrEditLeaveRequestDetailsDto>(requestDetail) 
            };
        }

        public async Task CreateOrEdit(CreateOrEditLeaveRequestDetailsDto input)
        {
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveRequestDetails_Create)]
        private async Task Create(CreateOrEditLeaveRequestDetailsDto input)
        {
            var requestDetail = ObjectMapper.Map<LeaveRequestDetails>(input);
            await _leaveRequestDetailsRepository.InsertAsync(requestDetail);
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveRequestDetails_Edit)]
        private async Task Update(CreateOrEditLeaveRequestDetailsDto input)
        {
            var requestDetail = await _leaveRequestDetailsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, requestDetail);
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveRequestDetails_Delete)]
        public async Task Delete(int id) =>
            await _leaveRequestDetailsRepository.DeleteAsync(id);
    }
}