﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Processor;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class ExcuseRequestsAppService : ePayAppServiceBase, IExcuseRequestsAppService
    {
        private readonly IApprovalsAppService _approvalsAppService;
        private readonly IRepository<ExcuseRequests> _excuseRequestsRepository;
        private readonly IRepository<ExcuseApprovals> _excuseApprovalsRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<EmployeeApprovers> _employeeApproversRepository;
        private readonly IStatusAppService _statusAppService;
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;
        private readonly IProcessorAppService _processorAppService;
        private readonly IWorkFlowAppService _workFlowAppService;

        public ExcuseRequestsAppService(IApprovalsAppService approvalsAppService,
            IRepository<ExcuseRequests> excuseRequestsRepository,
            IRepository<ExcuseApprovals> excuseApprovalsRepository,
            IRepository<Employees> employeesRepository,
            IRepository<EmployeeApprovers> employeeApproversRepository,
            IStatusAppService statusAppService,
            IAttendancePeriodsAppService attendancePeriodsAppService,
            IProcessorAppService processorAppService,
            IWorkFlowAppService workFlowAppService)
        {
            _approvalsAppService = approvalsAppService;
            _excuseRequestsRepository = excuseRequestsRepository;
            _excuseApprovalsRepository = excuseApprovalsRepository;
            _employeesRepository = employeesRepository;
            _employeeApproversRepository = employeeApproversRepository;
            _statusAppService = statusAppService;
            _attendancePeriodsAppService = attendancePeriodsAppService;
            _processorAppService = processorAppService;
            _workFlowAppService = workFlowAppService;
        }

        [AbpAuthorize(AppPermissions.Pages_ExcuseRequests)]
        public async Task<PagedResultDto<GetExcuseRequestsForViewDto>> GetAll(GetAllExcuseRequestsInput input)
        {
            var periodId = input.AttendancePeriodIdFilter.GetValueOrDefault();
            var requests = _excuseRequestsRepository.GetAll()
                .WhereIf(input.StatusIdFilter > 0, e => e.StatusId == input.StatusIdFilter)
                .Where(e => e.Employees.User.Id == AbpSession.UserId);

            if (periodId > 0)
            {
                var period = await _attendancePeriodsAppService.GetDateRangeFromAttendancePeriod(periodId);

                if (period != null)
                    requests = requests.WhereIf(period != null, e => e.ExcuseDate >= period.Start && e.ExcuseDate <= period.End);
            }

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(null);
            var totalCount = await requests.CountAsync();

            var pendingResult = await requests
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input)
                .Select(e => new
                {
                        e.ExcuseDate,
                        e.ExcuseStart,
                        e.ExcuseEnd,
                        Reason = e.ExcuseReasons.DisplayName,
                        e.Remarks,
                        Status = e.Status.DisplayName,
                        e.Status.IsEditable,
                        e.Id
                }).ToListAsync();

            var result = pendingResult.Select(e => new GetExcuseRequestsForViewDto
            {
                ExcuseRequests = new ExcuseRequestsDto
                {
                    ExcuseDate = e.ExcuseDate,
                    ExcuseStart = e.ExcuseStart,
                    ExcuseEnd = e.ExcuseEnd,
                    Reason = e.Reason,
                    Remarks = e.Remarks,
                    Status = e.Status,
                    IsEditable = e.IsEditable && e.ExcuseDate > postedDate,
                    Id = e.Id
                }
            }).ToList();

            return new PagedResultDto<GetExcuseRequestsForViewDto>(totalCount, result);
        }

        [AbpAuthorize(AppPermissions.Pages_ExcuseRequests)]
        public async Task<GetExcuseRequestsForViewDto> GetExcuseRequestsForView(int id)
        {
            var request = await _excuseRequestsRepository.GetAll()
                .Where(e => e.Id == id && e.Employees.User.Id == AbpSession.UserId)
                .Select(e => new
                {
                    e.Status.AccessLevel,
                    Employee = e.Employees.FullName,
                    e.ExcuseStart,
                    e.ExcuseEnd,
                    Reason = e.ExcuseReasons.DisplayName + (e.Reason == null ? string.Empty : "; " + e.Reason),
                    e.Remarks,
                    DateFiled = e.CreationTime,
                    Status = e.Status.DisplayName,
                    e.StatusId,
                    e.Employees.EmployeeTimeInfo.FrequencyId,
                    e.Id
                }).FirstOrDefaultAsync();

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(request.FrequencyId);
            var output = new GetExcuseRequestsForViewDto
            {
                ExcuseDetails = new ExcuseDetailsDto
                {
                    Employee = request.Employee,
                    ExcuseStart = request.ExcuseStart,
                    ExcuseEnd = request.ExcuseEnd,
                    Reason = request.Reason,
                    Remarks = request.Remarks,
                    DateFiled = request.DateFiled,
                    Status = request.Status,
                    Id = request.Id
                }
            };

            if (request.ExcuseEnd > postedDate)
                output.ExcuseDetails.StatusList = await _workFlowAppService.GetWorkFlowStatusList(request.StatusId);

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_ExcuseRequests_Edit)]
        public async Task<GetExcuseRequestsForEditOutput> GetExcuseRequestsForEdit(int id)
        {
            var excuseRequests = await _excuseRequestsRepository.FirstOrDefaultAsync(id);

            return new GetExcuseRequestsForEditOutput 
            { 
                ExcuseRequests = ObjectMapper.Map<CreateOrEditExcuseRequestsDto>(excuseRequests) 
            };
        }

        public async Task CreateOrEdit(CreateOrEditExcuseRequestsDto input)
        {
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        private async Task Validate(CreateOrEditExcuseRequestsDto input, int employeeId, int frequencyId)
        {
            if (input.ExcuseStart > input.ExcuseEnd)
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Invalid date range.");

            if (await _excuseRequestsRepository.GetAll()
                .AnyAsync(e => e.EmployeeId == employeeId && e.ExcuseDate == input.ExcuseDate && e.Id != input.Id))
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "An identical record already exists.");

            if (input.ExcuseDate <= await _attendancePeriodsAppService.GetLatestPostedDate(frequencyId))
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Can't file dates before posted date.");

            if (input.ExcuseDate >= DateTime.Today)
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Can't file past current date.");
        }

        [AbpAuthorize(AppPermissions.Pages_ExcuseRequests_Create)]
        private async Task Create(CreateOrEditExcuseRequestsDto input)
        {
            var request = ObjectMapper.Map<ExcuseRequests>(input);
            var employee = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new { e.Id, e.EmployeeTimeInfo.FrequencyId })
                .FirstOrDefaultAsync();

            request.EmployeeId = employee.Id;
            request.StatusId = (await _statusAppService.GetInitialStatus()).Status.Id;

            await Validate(input, employee.Id, employee.FrequencyId);
            await _excuseRequestsRepository.InsertAsync(request);
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_ExcuseRequests_Edit)]
        public async Task UpdateStatus(UpdateExcuseRequestsStatus input)
        {
            var status = await _statusAppService.GetStatusDisplayName(input.StatusId);

            if (status.StatusDisplayName is StatusDisplayNames.ForUpdate
                or StatusDisplayNames.Declined or StatusDisplayNames.Cancelled)
            {
                if (string.IsNullOrWhiteSpace(input.Reason))
                    throw new UserFriendlyException(L("YourRequestIsNotValid"), "The Remarks field is required.");
            }

            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
            var approver = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new ApproverDto
                {
                    Id = e.Id,
                    Name = e.FullName,
                    FrequencyId = e.EmployeeTimeInfo.FrequencyId
                }).FirstOrDefaultAsync();

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(approver.FrequencyId);
            var request = await _excuseRequestsRepository.GetAll()
                .Include(e => e.Employees)
                    .ThenInclude(e => e.EmployeeTimeInfo)
                .WhereIf(postedDate != DateTime.MinValue, e => e.ExcuseDate > postedDate)
                .Where(e => e.Id == input.Id)
                .Where(e => (e.Status.AccessLevel == 0 && e.EmployeeId == approver.Id)
                    || (e.Status.AccessLevel == e.Employees.EmployeeApproversApprover
                        .Where(f => f.ApproverId == approver.Id).First().ApproverOrders.Level))
                .Select(e => new
                {
                    Main = e,
                    e.Employees.EmployeeTimeInfo.FrequencyId
                }).FirstOrDefaultAsync();

            if (request != null)
            {
                request.Main.StatusId = input.StatusId;
                request.Main.Reason = input.Reason;
                await _excuseRequestsRepository.UpdateAsync(request.Main);

                await CreateApproval(input);

                if (status.StatusDisplayName is not StatusDisplayNames.Reopened)
                    await SendApprovalDetails(request.Main, status, approver);

                CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
                if (status.StatusDisplayName == StatusDisplayNames.Cancelled)
                {
                    var periodId = await _attendancePeriodsAppService.GetFirstUnpostedPeriodId(request.FrequencyId);

                    await _processorAppService.Process(periodId, request.Main.EmployeeId);
                }
            }
            else
                throw new UserFriendlyException(L("YourRequestIsNotValid"));
        }

        private async Task CreateApproval(UpdateExcuseRequestsStatus input)
        {
            var approval = new ExcuseApprovals
            {
                TenantId = AbpSession.TenantId.Value,
                Remarks = input.Reason,
                ExcuseRequestId = input.Id,
                StatusId = input.StatusId,
            };

            await _excuseApprovalsRepository.InsertAsync(approval);
        }

        [AbpAuthorize(AppPermissions.Pages_ExcuseRequests_Edit)]
        private async Task Update(CreateOrEditExcuseRequestsDto input)
        {
            var request = await _excuseRequestsRepository.GetAll()
                .Include(e => e.Employees)
                    .ThenInclude(e => e.EmployeeTimeInfo)
                .Where(e => e.Id == input.Id)
                .FirstOrDefaultAsync();

            await Validate(input, request.EmployeeId, request.Employees.EmployeeTimeInfo.FrequencyId);
            ObjectMapper.Map(input, request);
        }

        private async Task SendApprovalDetails(ExcuseRequests request, GetStatusForDisplayNameDto status, ApproverDto approver)
        {
            string email;

            if (status.StatusDisplayName == StatusDisplayNames.Cancelled)
            {
                email = await _employeeApproversRepository.GetAll()
                    .Where(e => e.EmployeeId == approver.Id && !e.ApproverOrders.IsBackup)
                    .OrderByDescending(e => e.ApproverOrders.Level)
                    .Select(e => e.Approvers.Email)
                    .FirstOrDefaultAsync();
            }
            else
            {
                email = await _employeeApproversRepository.GetAll()
                    .Where(e => e.EmployeeId == approver.Id
                        && e.ApproverOrders.Level == 1 && !e.ApproverOrders.IsBackup)
                    .Select(e => e.Approvers.Email)
                    .FirstOrDefaultAsync();
            }

            var dto = new EmailRequestDto
            {
                Request = L("Excuse"),
                FullName = approver.Name,
                ApprovalStatus = status.AfterStatusMessage,
                Remarks = request.Remarks,
                Reason = request.Reason,
                StartDate = request.ExcuseDate,
                Recipient = email
            };

            await _approvalsAppService.SendApprovalDetails(dto);
        }
    }
}