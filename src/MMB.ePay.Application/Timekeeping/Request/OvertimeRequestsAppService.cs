﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Processor;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using System;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class OvertimeRequestsAppService : ePayAppServiceBase, IOvertimeRequestsAppService
    {
        private readonly IApprovalsAppService _approvalsAppService;
        private readonly IRepository<OvertimeRequests> _overtimeRequestsRepository;
        private readonly IRepository<OvertimeApprovals> _overtimeApprovalsRepository;
        private readonly IRepository<LeaveRequestDetails> _leaveRequestDetailsRepository;
        private readonly IRepository<ShiftRequests> _shiftRequestsRepository;
        private readonly IRepository<EmployeeTimeInfo> _employeeTimeInfoRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<EmployeeApprovers> _employeeApproversRepository;
        private readonly IStatusAppService _statusAppService;
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;
        private readonly IProcessorAppService _processorAppService;
        private readonly IWorkFlowAppService _workFlowAppService;

        public OvertimeRequestsAppService(IApprovalsAppService approvalsAppService,
            IRepository<OvertimeRequests> overtimeRequestsRepository,
            IRepository<OvertimeApprovals> overtimeApprovalsRepository,
            IRepository<LeaveRequestDetails> leaveRequestDetailsRepository,
            IRepository<ShiftRequests> shiftRequestsRepository,
            IRepository<EmployeeTimeInfo> employeeTimeInfoRepository,
            IRepository<Employees> employeesRepository,
            IRepository<EmployeeApprovers> employeeApproversRepository,
            IStatusAppService statusAppService,
            IAttendancePeriodsAppService attendancePeriodsAppService,
            IProcessorAppService processorAppService,
            IWorkFlowAppService workFlowAppService)
        {
            _approvalsAppService = approvalsAppService;
            _overtimeRequestsRepository = overtimeRequestsRepository;
            _overtimeApprovalsRepository = overtimeApprovalsRepository;
            _leaveRequestDetailsRepository = leaveRequestDetailsRepository;
            _shiftRequestsRepository = shiftRequestsRepository;
            _employeeTimeInfoRepository = employeeTimeInfoRepository;
            _employeesRepository = employeesRepository;
            _employeeApproversRepository = employeeApproversRepository;
            _statusAppService = statusAppService;
            _attendancePeriodsAppService = attendancePeriodsAppService; ;
            _processorAppService = processorAppService;
            _workFlowAppService = workFlowAppService;
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeRequests)]
        public async Task<PagedResultDto<GetOvertimeRequestsForViewDto>> GetAll(GetAllOvertimeRequestsInput input)
        {
            var periodId = input.AttendancePeriodIdFilter.GetValueOrDefault();
            var requests = _overtimeRequestsRepository.GetAll()
                .WhereIf(input.StatusIdFilter > 0, e => e.StatusId == input.StatusIdFilter)
                .Where(e => e.Employees.User.Id == AbpSession.UserId);

            if (periodId > 0)
            {
                var period = await _attendancePeriodsAppService.GetDateRangeFromAttendancePeriod(periodId);

                if (period != null)
                    requests = requests.Where(e => e.OvertimeDate >= period.Start && e.OvertimeDate <= period.End);
            }

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(null);
            var totalCount = await requests.CountAsync();

            var pendingResult = await requests
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input)
                .Select(e => new
                {
                    e.OvertimeDate,
                    e.OvertimeStart,
                    e.OvertimeEnd,
                    Reason = e.OvertimeReasons.DisplayName,
                    e.Remarks,
                    Status = e.Status.DisplayName,
                    e.Status.IsEditable,
                    e.Id
                }).ToListAsync();

            var result = pendingResult.Select(e => new GetOvertimeRequestsForViewDto
            {
                OvertimeRequests = new OvertimeRequestsDto
                {
                    OvertimeDate = e.OvertimeDate,
                    OvertimeStart = e.OvertimeStart,
                    OvertimeEnd = e.OvertimeEnd,
                    Reason = e.Reason,
                    Remarks = e.Remarks,
                    Status = e.Status,
                    IsEditable = e.IsEditable && e.OvertimeDate > postedDate,
                    Id = e.Id
                }
            }).ToList();

            return new PagedResultDto<GetOvertimeRequestsForViewDto>(totalCount, result);
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeRequests)]
        public async Task<GetOvertimeRequestsForViewDto> GetOvertimeRequestsForView(int id)
        {
            var request = await _overtimeRequestsRepository.GetAll()
                .Where(e => e.Id == id)
                .Where(e => e.Employees.User.Id == AbpSession.UserId)
                .Select(e => new
                {
                    e.Status.AccessLevel,
                    Employee = e.Employees.FullName,
                    e.OvertimeDate,
                    e.OvertimeStart,
                    e.OvertimeEnd,
                    Reason = e.OvertimeReasons.DisplayName + (e.Reason == null ? string.Empty : "; " + e.Reason),
                    e.Remarks,
                    DateFiled = e.CreationTime,
                    Status = e.Status.DisplayName,
                    e.StatusId,
                    e.Employees.EmployeeTimeInfo.FrequencyId,
                    e.Id
                }).FirstOrDefaultAsync();

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(request.FrequencyId);
            var output = new GetOvertimeRequestsForViewDto
            {
                OvertimeDetails = new OvertimeDetailsDto
                {
                    Employee = request.Employee,
                    OvertimeDate = request.OvertimeDate,
                    OvertimeStart = request.OvertimeStart,
                    OvertimeEnd = request.OvertimeEnd,
                    Reason = request.Reason,
                    Remarks = request.Remarks,
                    DateFiled = request.DateFiled,
                    Status = request.Status,
                    Id = request.Id
                }
            };

            if (request.OvertimeEnd > postedDate)
                output.OvertimeDetails.StatusList = await _workFlowAppService.GetWorkFlowStatusList(request.StatusId);

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeRequests_Edit)]
        public async Task<GetOvertimeRequestsForEditOutput> GetOvertimeRequestsForEdit(int id)
        {
            var request = await _overtimeRequestsRepository.FirstOrDefaultAsync(id);

            return new GetOvertimeRequestsForEditOutput 
            { 
                OvertimeRequests = ObjectMapper.Map<CreateOrEditOvertimeRequestsDto>(request) 
            };
        }

        public async Task CreateOrEdit(CreateOrEditOvertimeRequestsDto input)
        {
            //Adjust Start/End DateTime
            if (input.OvertimeStart != null && input.OvertimeEnd != null)
            {
                input.OvertimeStart += input.StartTime;
                input.OvertimeEnd += input.EndTime;
            }
            else
            {
                input.OvertimeStart = input.OvertimeDate + input.StartTime;
                input.OvertimeEnd = input.OvertimeDate + input.EndTime;

                if (input.OvertimeStart > input.OvertimeEnd)
                    input.OvertimeEnd = input.OvertimeEnd.Value.AddDays(1);
            }

            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        private async Task Validate(CreateOrEditOvertimeRequestsDto input, int employeeId, int frequencyId)
        {
            if (input.OvertimeStart > input.OvertimeEnd)
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Invalid date range.");

            if (await _overtimeRequestsRepository.GetAll()
                .AnyAsync(e => e.EmployeeId == employeeId
                    && e.OvertimeDate == input.OvertimeDate
                    && e.Id != input.Id))
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "An identical record already exists.");

            var latestPostedDate = await _attendancePeriodsAppService.GetLatestPostedDate(frequencyId);

            if (input.OvertimeDate <= latestPostedDate)
                throw new UserFriendlyException(L("YourRequestIsNotValid"), "Can't file dates before posted date.");

            var dayOfWeek = (int)input.OvertimeDate.Value.DayOfWeek;
            var formattedDoW = dayOfWeek == 0 ? "7" : dayOfWeek.ToString();

            var declinedStatus = await _statusAppService
                .GetStatusByDisplayNames(StatusDisplayNames.Cancelled, StatusDisplayNames.Declined);
            var declinedStatusIds = declinedStatus.Select(e => e.Status.Id).ToArray();

            var hasLeaveRequest = await _leaveRequestDetailsRepository.GetAll()
                .Where(e => e.LeaveRequests.Employees.User.Id == AbpSession.UserId
                    && e.Days >= 1
                    && !declinedStatusIds.Contains(e.LeaveRequests.StatusId)
                    && e.LeaveDate == input.OvertimeDate)
                .AnyAsync();

            if (hasLeaveRequest)
                throw new UserFriendlyException(L("YourRequestIsNotValid"),
                    "Your request is overlapping with an existing Leave request.");

            var shiftType = await _shiftRequestsRepository.GetAll()
                .Include(e => e.ShiftTypes)
                .ThenInclude(e => e.ShiftTypeDetails)
                .Where(e => e.ShiftStart <= input.OvertimeDate && e.ShiftEnd >= input.OvertimeDate
                    && e.ShiftTypes.ShiftTypeDetails.First().Days.Contains(formattedDoW))
                .Select(e => e.ShiftTypes)
                .FirstOrDefaultAsync();

            if (shiftType == null)
            {
                shiftType = await _employeeTimeInfoRepository.GetAll()
                    .Include(e => e.ShiftTypes)
                    .ThenInclude(e => e.ShiftTypeDetails)
                    .Where(e => e.Employees.User.Id == AbpSession.UserId)
                    .Select(e => e.ShiftTypes)
                    .FirstOrDefaultAsync();
            }

            var totalOvertime = (input.OvertimeEnd - input.OvertimeStart).Value.TotalMinutes;
            if (shiftType.MinimumOvertime > totalOvertime)
                throw new UserFriendlyException(L("YourRequestIsNotValid"),
                    "Your request is below the minimum required overtime.");

            //Advanced OT
            if (input.OvertimeEnd < latestPostedDate)
            {
                var shiftTypeDetail = shiftType.ShiftTypeDetails.Where(e => e.Days.Contains(formattedDoW)).FirstOrDefault();
                var shiftEnd = DateTime.ParseExact(input.OvertimeDate.Value.ToString("yyyy/MM/dd") + " "
                    + shiftTypeDetail.TimeOut, @"yyyy/MM/dd HH:mm", CultureInfo.InvariantCulture);

                if (input.OvertimeStart < shiftEnd)
                    throw new UserFriendlyException(L("YourRequestIsNotValid"),
                        "Your request is not within the excess hours.");
            }
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeRequests_Create)]
        private async Task Create(CreateOrEditOvertimeRequestsDto input)
        {
            var request = ObjectMapper.Map<OvertimeRequests>(input);
            var employee = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new { e.Id, e.EmployeeTimeInfo.FrequencyId })
                .FirstOrDefaultAsync();

            request.EmployeeId = employee.Id;
            request.StatusId = (await _statusAppService.GetInitialStatus()).Status.Id;

            await Validate(input, employee.Id, employee.FrequencyId);
            await _overtimeRequestsRepository.InsertAsync(request);
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_OvertimeRequests_Edit)]
        public async Task UpdateStatus(UpdateOvertimeRequestsStatus input)
        {
            var status = await _statusAppService.GetStatusDisplayName(input.StatusId);

            if (status.StatusDisplayName is StatusDisplayNames.ForUpdate
                or StatusDisplayNames.Declined or StatusDisplayNames.Cancelled)
            {
                if (string.IsNullOrWhiteSpace(input.Reason))
                    throw new UserFriendlyException(L("YourRequestIsNotValid"), "The Remarks field is required.");
            }

            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
            var approver = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new ApproverDto
                {
                    Id = e.Id,
                    Name = e.FullName,
                    FrequencyId = e.EmployeeTimeInfo.FrequencyId
                }).FirstOrDefaultAsync();

            var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(approver.FrequencyId);
            var request = await _overtimeRequestsRepository.GetAll()
                .Include(e => e.Employees)
                    .ThenInclude(e => e.EmployeeTimeInfo)
                .WhereIf(postedDate != DateTime.MinValue, e => e.OvertimeDate > postedDate)
                .Where(e => e.Id == input.Id)
                .Where(e => (e.Status.AccessLevel == 0 && e.EmployeeId == approver.Id)
                    || (e.Status.AccessLevel == e.Employees.EmployeeApproversApprover
                        .Where(f => f.ApproverId == approver.Id).First().ApproverOrders.Level))
                .Select(e => new
                {
                    Main = e,
                    e.Employees.EmployeeTimeInfo.FrequencyId
                }).FirstOrDefaultAsync();

            if (request != null)
            {
                request.Main.StatusId = input.StatusId;
                request.Main.Reason = input.Reason;
                await _overtimeRequestsRepository.UpdateAsync(request.Main);

                await CreateApproval(input);

                if (status.StatusDisplayName is not StatusDisplayNames.Reopened)
                    await SendApprovalDetails(request.Main, status, approver);

                CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
                if (status.StatusDisplayName == StatusDisplayNames.Cancelled)
                {
                    var periodId = await _attendancePeriodsAppService.GetFirstUnpostedPeriodId(request.FrequencyId);

                    await _processorAppService.Process(periodId, request.Main.EmployeeId);
                }
            }
            else
                throw new UserFriendlyException(L("YourRequestIsNotValid"));
        }

        private async Task CreateApproval(UpdateOvertimeRequestsStatus input)
        {
            var approvals = new OvertimeApprovals
            {
                TenantId = AbpSession.TenantId.Value,
                Remarks = input.Reason,
                OvertimeRequestId = input.Id,
                StatusId = input.StatusId,
            };

            await _overtimeApprovalsRepository.InsertAsync(approvals);
        }

        [AbpAuthorize(AppPermissions.Pages_OvertimeRequests_Edit)]
        private async Task Update(CreateOrEditOvertimeRequestsDto input)
        {
            var request = await _overtimeRequestsRepository.GetAll()
                .Include(e => e.Employees)
                    .ThenInclude(e => e.EmployeeTimeInfo)
                .Where(e => e.Id == input.Id)
                .FirstOrDefaultAsync();

            await Validate(input, request.EmployeeId, request.Employees.EmployeeTimeInfo.FrequencyId);
            ObjectMapper.Map(input, request);
        }

        private async Task SendApprovalDetails(OvertimeRequests request, GetStatusForDisplayNameDto status, ApproverDto approver)
        {
            string email;

            if (status.StatusDisplayName == StatusDisplayNames.Cancelled)
            {
                email = await _employeeApproversRepository.GetAll()
                    .Where(e => e.EmployeeId == approver.Id && !e.ApproverOrders.IsBackup)
                    .OrderByDescending(e => e.ApproverOrders.Level)
                    .Select(e => e.Approvers.Email)
                    .FirstOrDefaultAsync();
            }
            else
            {
                email = await _employeeApproversRepository.GetAll()
                    .Where(e => e.EmployeeId == approver.Id
                        && e.ApproverOrders.Level == 1 && !e.ApproverOrders.IsBackup)
                    .Select(e => e.Approvers.Email)
                    .FirstOrDefaultAsync();
            }

            var dto = new EmailRequestDto
            {
                Request = L("Overtime"),
                FullName = approver.Name,
                ApprovalStatus = status.AfterStatusMessage,
                Remarks = request.Remarks,
                Reason = request.Reason,
                StartDate = request.OvertimeDate,
                Recipient = email
            };

            await _approvalsAppService.SendApprovalDetails(dto);
        }
    }
}