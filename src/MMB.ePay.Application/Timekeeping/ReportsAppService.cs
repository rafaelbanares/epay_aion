﻿using Abp.Application.Services;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using DevExpress.XtraReports.UI;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.MultiTenancy;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize(AppPermissions.Pages_Reports)]
    public class ReportsAppService : ePayAppServiceBase, IReportsAppService
    {
        private readonly IRepository<Reports> _reportsRepository;
        private readonly TenantManager _tenantManager;
        private readonly IRepository<Attendances> _attendancesRepository;
        private readonly IRepository<ProcessedTimesheets> _processedTimesheetsRepository;
        private readonly IRepository<ProcessedLeaves> _processedLeavesRepository;
        private readonly IRepository<ProcessedOvertimes> _processedOvertimesRepository;
        private readonly IRepository<PostedAttendances> _postedAttendancesRepository;
        private readonly IRepository<PostedTimesheets> _postedTimesheetsRepository;
        private readonly IRepository<PostedLeaves> _postedLeavesRepository;
        private readonly IRepository<PostedOvertimes> _postedOvertimesRepository;
        private readonly IRepository<LeaveTypes> _leaveTypesRepository;
        private readonly IRepository<OvertimeTypes> _overtimeTypesRepository;
        private readonly IRepository<AttendanceRequests> _attendanceRequestsRepository;
        private readonly IRepository<ExcuseRequests> _excuseRequestsRepository;
        private readonly IRepository<LeaveRequests> _leaveRequestsRepository;
        private readonly IRepository<OfficialBusinessRequests> _officialBussinessRequestsRepository;
        private readonly IRepository<OvertimeRequests> _overtimeRequestsRepository;
        private readonly IRepository<RestDayRequests> _restDayRequestsRepository;
        private readonly IRepository<ShiftRequests> _shiftRequestsRepository;
        private readonly IRepository<LeaveEarningDetails> _leaveEarningDetailsRepository;
        private readonly IRepository<RawTimeLogs> _rawTimeLogsRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IHelperAppService _helperAppService;
        private readonly IStatusAppService _statusManager;

        public ReportsAppService(
            IRepository<Reports> reportsRepository,
            TenantManager tenantManager,
            IRepository<Attendances> attendancesRepository,
            IRepository<ProcessedTimesheets> processedTimesheetsRepository,
            IRepository<ProcessedLeaves> processedLeavesRepository,
            IRepository<ProcessedOvertimes> processedOvertimesRepository,
            IRepository<PostedAttendances> postedAttendancesRepository,
            IRepository<PostedTimesheets> postedTimesheetsRepository,
            IRepository<PostedLeaves> postedLeavesRepository,
            IRepository<PostedOvertimes> postedOvertimesRepository,
            IRepository<LeaveTypes> leaveTypesRepository,
            IRepository<OvertimeTypes> overtimeTypesRepository,
            IRepository<AttendanceRequests> attendanceRequestsRepository,
            IRepository<ExcuseRequests> excuseRequestsRepository,
            IRepository<LeaveRequests> leaveRequestsRepository,
            IRepository<OfficialBusinessRequests> officialBussinessRequestsRepository,
            IRepository<OvertimeRequests> overtimeRequestsRepository,
            IRepository<RestDayRequests> restDayRequestsRepository,
            IRepository<ShiftRequests> shiftRequestsRepository,
            IRepository<LeaveEarningDetails> leaveEarningDetailsRepository,
            IRepository<RawTimeLogs> rawTimeLogsRepository,
            IRepository<Employees> employeesRepository,
            IHelperAppService helperAppService,
            IStatusAppService statusManager)
        {
            _reportsRepository = reportsRepository;
            _tenantManager = tenantManager;
            _attendancesRepository = attendancesRepository;
            _processedTimesheetsRepository = processedTimesheetsRepository;
            _processedLeavesRepository = processedLeavesRepository;
            _processedOvertimesRepository = processedOvertimesRepository;
            _postedAttendancesRepository = postedAttendancesRepository;
            _postedTimesheetsRepository = postedTimesheetsRepository;
            _postedLeavesRepository = postedLeavesRepository;
            _postedOvertimesRepository = postedOvertimesRepository;
            _leaveTypesRepository = leaveTypesRepository;
            _overtimeTypesRepository = overtimeTypesRepository;
            _attendanceRequestsRepository = attendanceRequestsRepository;
            _excuseRequestsRepository = excuseRequestsRepository;
            _leaveRequestsRepository = leaveRequestsRepository;
            _officialBussinessRequestsRepository = officialBussinessRequestsRepository;
            _overtimeRequestsRepository = overtimeRequestsRepository;
            _restDayRequestsRepository = restDayRequestsRepository;
            _shiftRequestsRepository = shiftRequestsRepository;
            _leaveEarningDetailsRepository = leaveEarningDetailsRepository;
            _rawTimeLogsRepository = rawTimeLogsRepository;
            _employeesRepository = employeesRepository;
            _helperAppService = helperAppService;
            _statusManager = statusManager;
        }

        public async Task<string> GetReportName(int id)
        {
            return await _reportsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => e.ControllerName)
                .FirstOrDefaultAsync();
        }

        public async Task<IList<SelectListItem>> GetReportTypeList()
        {
            var reports = await _reportsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId && e.Enabled)
                .OrderBy(e => e.DisplayName)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            reports.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return reports;
        }

        public async Task<GetReportNamesDto> GetControllerName(int id)
        {
            return await _reportsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetReportNamesDto
                {
                    ControllerName = e.ControllerName,
                    ExportFileName = e.ExportFileName,
                    StoredProcedure = e.StoredProcedure
                })
                .FirstOrDefaultAsync();
        }

        public async Task<GetReportsForViewDto> GetReportsForView(int id) =>
            await _reportsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetReportsForViewDto
                {
                    Reports = new ReportsDto
                    {
                        ReportId = e.Id,
                        Format = e.Format,
                        IsLandscape = e.IsLandscape,
                        Output = e.Output,
                        HasEmployeeFilter = e.HasEmployeeFilter,
                        HasPeriodFilter = e.HasPeriodFilter,
                        ControllerName = e.ControllerName
                    }
                }).FirstOrDefaultAsync();

        [RemoteService(false)]
        public async Task<GetReportsForActiveEmployees> GetActiveEmployees(ReportsDto dto)
        {
            var employees = await _postedAttendancesRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
                .WhereIf(dto.FrequencyFilterId > 0,
                    e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                .WhereIf(dto.MainOrganizationUnitId > 0,
                    e => e.Employees.User.OrganizationUnits
                        .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Where(e => e.Employees.EmployeeTimeInfo.Frequencies.AttendancePeriods
                    .Any(f => f.Id == dto.AttendancePeriodId))
                .Select(e => new ActiveEmployeesTable
                {
                    EmployeeId = e.Employees.EmployeeCode,
                    EmployeeName = e.Employees.FullName,
                    DateEmployed = e.Employees.DateEmployed,
                    DateTerminated = e.Employees.DateTerminated,
                    EmployeeStatus = e.Employees.EmployeeStatus
                }).Distinct().ToListAsync();

            //var employees = await _employeesRepository.GetAll()
            //    .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
            //    .WhereIf(dto.FrequencyFilterId > 0,
            //        e => e.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
            //    .WhereIf(dto.Rank != null, e => e.Rank == dto.Rank)
            //    .WhereIf(dto.MainOrganizationUnitId > 0,
            //        e => e.User.OrganizationUnits
            //            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
            //    .Where(e => e.TenantId == AbpSession.TenantId)
            //    .Where(e => e.EmployeeTimeInfo.Frequencies.AttendancePeriods
            //        .Any(f => f.Id == dto.AttendancePeriodId))
            //    .Where(e => e.PostedAttendances)

            //    //.Where(e => e.DateEmployed >= dto.MinDate)
            //    //.Where(e => e.DateTerminated <= dto.MaxDate || e.DateTerminated == null)
            //    .Select(e => new ActiveEmployeesTable
            //    {
            //        EmployeeId = e.EmployeeCode,
            //        EmployeeName = e.FullName,
            //        DateEmployed = e.DateEmployed,
            //        DateTerminated = e.DateTerminated,
            //        EmployeeStatus = e.EmployeeStatus
            //    }).ToListAsync();

            return new GetReportsForActiveEmployees
            {
                ActiveEmployees = employees
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForDailyAttendance> GetDailyAttendance(ReportsDto dto)
        {
            var dailyAttendances = new List<DailyAttendanceTable>();

            if (dto.PostFilterId)
            {
                var attendances = await _postedAttendancesRepository.GetAll()
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => e.TenantId == AbpSession.TenantId)
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.Date)
                    .Select(e => new
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        EmployeeName = e.Employees.FullName,
                        e.Date,
                        e.TimeIn,
                        e.TimeOut,
                    }).ToListAsync();

                var timesheets = await _postedTimesheetsRepository.GetAll()
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => e.TenantId == AbpSession.TenantId)
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.Date)
                    .Select(e => new
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        e.Date,
                        e.UtMins,
                        e.TardyMins,
                        e.LeaveDays,
                        e.Absence
                    }).ToListAsync();

                dailyAttendances = attendances.Join(timesheets,
                        att => new { att.EmployeeId, att.Date },
                        time => new { time.EmployeeId, time.Date },
                    (att, time) => new DailyAttendanceTable
                    {
                        EmployeeId = att.EmployeeId,
                        EmployeeName = att.EmployeeName,
                        Date = att.Date,
                        TimeIn = att.TimeIn,
                        TimeOut = att.TimeOut,
                        UTMins = time.UtMins,
                        TardyMins = time.TardyMins,
                        LeaveDays = time.LeaveDays,
                        Absence = time.Absence
                    }).OrderBy(e => e.EmployeeName).ThenBy(e => e.Date)
                    .ToList();
            }
            else
            {
                var attendances = await _attendancesRepository.GetAll()
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .Where(e => e.TenantId == AbpSession.TenantId)
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.Date)
                    .Select(e => new
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        EmployeeName = e.Employees.FullName,
                        e.Date,
                        e.TimeIn,
                        e.TimeOut,
                    }).ToListAsync();

                var timesheets = await _processedTimesheetsRepository.GetAll()
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .Where(e => e.TenantId == AbpSession.TenantId)
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.Date)
                    .Select(e => new
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        e.Date,
                        e.UtMins,
                        e.TardyMins,
                        e.LeaveDays,
                        e.Absence
                    }).ToListAsync();

                dailyAttendances = attendances.Join(timesheets,
                        att => new { att.EmployeeId, att.Date },
                        time => new { time.EmployeeId, time.Date },
                    (att, time) => new DailyAttendanceTable
                    {
                        EmployeeId = att.EmployeeId,
                        EmployeeName = att.EmployeeName,
                        Date = att.Date,
                        TimeIn = att.TimeIn,
                        TimeOut = att.TimeOut,
                        UTMins = time.UtMins,
                        TardyMins = time.TardyMins,
                        LeaveDays = time.LeaveDays,
                        Absence = time.Absence
                    }).OrderBy(e => e.EmployeeName).ThenBy(e => e.Date)
                    .ToList();
            }

            return new GetReportsForDailyAttendance
            {
                DailyAttendance = dailyAttendances
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForAttendanceWithBreaks> GetAttendanceWithBreaks(ReportsDto dto)
        {
            var attendanceWithBreaks = new List<AttendanceWithBreaksTable>();

            if (dto.PostFilterId)
            {
                attendanceWithBreaks = await _postedAttendancesRepository.GetAll()
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => e.TenantId == AbpSession.TenantId)
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.Date)
                    .Select(e => new AttendanceWithBreaksTable
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        EmployeeName = e.Employees.FullName,
                        Date = e.Date,
                        TimeIn = e.TimeIn,
                        TimeOut = e.TimeOut,
                        BreaktimeIn = e.BreaktimeIn,
                        BreaktimeOut = e.BreaktimeOut,
                        PMBreaktimeIn = e.PMBreaktimeIn,
                        PMBreaktimeOut = e.PMBreaktimeOut
                    }).ToListAsync();
            }
            else
            {
                attendanceWithBreaks = await _attendancesRepository.GetAll()
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .Where(e => e.TenantId == AbpSession.TenantId)
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.Date)
                    .Select(e => new AttendanceWithBreaksTable
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        EmployeeName = e.Employees.FullName,
                        Date = e.Date,
                        TimeIn = e.TimeIn,
                        TimeOut = e.TimeOut,
                        BreaktimeIn = e.BreaktimeIn,
                        BreaktimeOut = e.BreaktimeOut,
                        PMBreaktimeIn = e.PMBreaktimeIn,
                        PMBreaktimeOut = e.PMBreaktimeOut
                    }).ToListAsync();
            }

            return new GetReportsForAttendanceWithBreaks
            {
                AttendanceWithBreaks = attendanceWithBreaks
            };
        }

        private async Task<string> GetAddressFromPoint(double latitude, double longitude) /*XY*/
        {
            var nominatimUrl = $"https://nominatim.openstreetmap.org/reverse?format=json&lat={latitude}&lon={longitude}&zoom=18&addressdetails=1";

            var client = new RestClient(nominatimUrl);
            var request = new RestRequest();
            var response = await client.GetAsync(request);

            // Parse the JSON response to get the address
            dynamic jsonData = Newtonsoft.Json.JsonConvert.DeserializeObject(response.Content);
            string address = jsonData.display_name;

            return address;
        }

        [RemoteService(false)]
        public async Task<GetReportsForWorkFromHome> GetWorkFromHome(ReportsDto dto)
        {
            //M-Mobile | P-PC | B-Biometric
            var employees = await _employeesRepository.GetAll()
                .WhereIf(dto.EmployeeId > 0, e => e.Id == dto.EmployeeId)
                .WhereIf(dto.FrequencyFilterId > 0,
                    e => e.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                .WhereIf(dto.Rank != null, e => e.Rank == dto.Rank)
                .WhereIf(dto.MainOrganizationUnitId > 0,
                    e => e.User.OrganizationUnits
                        .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new
                {
                    e.EmployeeTimeInfo.SwipeCode,
                    e.EmployeeCode,
                    e.FullName
                }).ToListAsync();

            var rawTimeLogs = await _rawTimeLogsRepository.GetAll()
                .Where(e => dto.MinDate <= e.LogTime && dto.MaxDate >= e.LogTime)
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.WorkFromHome)
                .Select(e => new
                {
                    e.SwipeCode,
                    e.LogTime,
                    e.Device
                }).ToListAsync();

            var join = employees
                .Join(rawTimeLogs,
                    emp => emp.SwipeCode,
                    rtl => rtl.SwipeCode,
                    (emp, rtl) => new
                    {
                        emp.EmployeeCode,
                        emp.FullName,
                        emp.SwipeCode,
                        rtl.LogTime,
                        rtl.Device
                    })
                .Where(e => e.SwipeCode != null)
                .ToList();

            var workFromHome = join
                .Select(e => new WorkFromHomeTable
                {
                    EmployeeId = e.EmployeeCode,
                    EmployeeName = e.FullName,
                    Date = e.LogTime,
                    Time = e.LogTime,
                    Device = e.Device
                }).ToList();

            return new GetReportsForWorkFromHome
            {
                WorkFromHome = workFromHome
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForAttendanceSummary> GetAttendanceSummary(ReportsDto dto)
        {
            var attendanceSummaries = new List<AttendanceSummaryTable>();

            if (dto.PostFilterId)
            {
                var attendanceSummary = await _postedTimesheetsRepository.GetAll()
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => e.TenantId == AbpSession.TenantId)
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.Date)
                    .Select(e => new
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        EmployeeName = e.Employees.FullName,
                        RegHrs = (decimal)e.RegMins / 60,
                        e.RegNd1Mins,
                        e.RegNd2Mins,
                        e.UtMins,
                        e.TardyMins,
                        e.PaidHoliday,
                        e.Absence
                    }).ToListAsync();

                attendanceSummaries = attendanceSummary
                    .GroupBy(e => e.EmployeeId)
                    .Select(e => new AttendanceSummaryTable
                    {
                        EmployeeId = e.Key,
                        EmployeeName = e.First().EmployeeName,
                        RegDays = decimal.Round(e.Sum(f => f.RegHrs) / 8, 2),
                        RegNd1Mins = _helperAppService.GetMinuteFormat(e.Sum(f => f.RegNd1Mins)),
                        RegNd2Mins = _helperAppService.GetMinuteFormat(e.Sum(f => f.RegNd2Mins)),
                        UTMins = _helperAppService.GetMinuteFormat(e.Sum(f => f.UtMins)),
                        TardyMins = _helperAppService.GetMinuteFormat(e.Sum(f => f.TardyMins)),
                        PaidHoliday = decimal.Round(e.Sum(f => f.PaidHoliday), 2),
                        Absence = decimal.Round(e.Sum(f => f.Absence), 2)
                    }).ToList();
            }
            else
            {
                var attendanceSummary = await _processedTimesheetsRepository.GetAll()
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => e.TenantId == AbpSession.TenantId)
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.Date)
                    .Select(e => new
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        EmployeeName = e.Employees.FullName,
                        RegHrs = (decimal)e.RegMins / 60,
                        e.RegNd1Mins,
                        e.RegNd2Mins,
                        e.UtMins,
                        e.TardyMins,
                        e.PaidHoliday,
                        e.Absence
                    }).ToListAsync();

                attendanceSummaries = attendanceSummary
                    .GroupBy(e => e.EmployeeId)
                    .Select(e => new AttendanceSummaryTable
                    {
                        EmployeeId = e.Key,
                        EmployeeName = e.First().EmployeeName,
                        RegDays = decimal.Round(e.Sum(f => f.RegHrs) / 8, 2),
                        RegNd1Mins = _helperAppService.GetMinuteFormat(e.Sum(f => f.RegNd1Mins)),
                        RegNd2Mins = _helperAppService.GetMinuteFormat(e.Sum(f => f.RegNd2Mins)),
                        UTMins = _helperAppService.GetMinuteFormat(e.Sum(f => f.UtMins)),
                        TardyMins = _helperAppService.GetMinuteFormat(e.Sum(f => f.TardyMins)),
                        PaidHoliday = decimal.Round(e.Sum(f => f.PaidHoliday), 2),
                        Absence = decimal.Round(e.Sum(f => f.Absence), 2)
                    }).ToList();
            }

            return new GetReportsForAttendanceSummary
            {
                AttendanceSummary = attendanceSummaries
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForAttendanceSummaryPayroll> GetAttendanceSummaryPayroll(ReportsDto dto)
        {
            var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);
            var attendanceSummaryPayrolls = new List<AttendanceSummaryPayrollTable>();

            if (dto.PostFilterId)
            {
                var attendanceSummaryPayroll = await _postedTimesheetsRepository.GetAll()
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.Date)
                    .Select(e => new
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        e.Employees.LastName,
                        e.Employees.FirstName,
                        RegDays = ((decimal)e.RegMins / 60) / 8,
                        RegHrs = 0,
                        AbsDays = e.Absence,
                        AbsHrs = 0,
                        TardyHrs = (decimal)e.TardyMins / 60,
                        UTHrs = (decimal)e.UtMins / 60,
                        ND1Hrs = (decimal)e.RegNd1Mins / 60,
                        ND2Hrs = (decimal)e.RegNd2Mins / 60,
                        SLDays = 0,
                        VLDays = 0,
                        e.PaidHoliday
                    }).ToListAsync();

                attendanceSummaryPayrolls = attendanceSummaryPayroll
                    .GroupBy(e => e.EmployeeId)
                    .Select(e => new AttendanceSummaryPayrollTable
                    {
                        TenantId = tenant.TenancyName,
                        EmployeeId = e.Key,
                        LastName = e.First().LastName,
                        FirstName = e.First().FirstName,
                        Paycode = string.Empty,
                        Department = string.Empty,
                        RegDays = decimal.Round(e.Sum(f => f.RegDays), 2),
                        RegHrs = decimal.Round(e.Sum(f => f.RegHrs), 2),
                        AbsDays = decimal.Round(e.Sum(f => f.AbsDays), 2),
                        AbsHrs = decimal.Round(e.Sum(f => f.AbsHrs), 2),
                        TardyHrs = decimal.Round(e.Sum(f => f.TardyHrs), 2),
                        UTHrs = decimal.Round(e.Sum(f => f.UTHrs), 2),
                        ND1Hrs = decimal.Round(e.Sum(f => f.ND1Hrs), 2),
                        ND2Hrs = decimal.Round(e.Sum(f => f.ND2Hrs), 2),
                        SLDays = decimal.Round(e.Sum(f => f.SLDays), 2),
                        VLDays = decimal.Round(e.Sum(f => f.VLDays), 2),
                        PaidHoliday = decimal.Round(e.Sum(f => f.PaidHoliday), 2),
                    }).OrderBy(e => e.EmployeeId)
                    .ToList();
            }
            else
            {
                var attendanceSummaryPayroll = await _processedTimesheetsRepository.GetAll()
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.Date)
                    .Select(e => new
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        e.Employees.LastName,
                        e.Employees.FirstName,
                        RegDays = ((decimal)e.RegMins / 60) / 8,
                        RegHrs = 0,
                        AbsDays = e.Absence,
                        AbsHrs = 0,
                        TardyHrs = (decimal)e.TardyMins / 60,
                        UTHrs = (decimal)e.UtMins / 60,
                        ND1Hrs = (decimal)e.RegNd1Mins / 60,
                        ND2Hrs = (decimal)e.RegNd2Mins / 60,
                        SLDays = 0,
                        VLDays = 0,
                        e.PaidHoliday
                    }).ToListAsync();

                attendanceSummaryPayrolls = attendanceSummaryPayroll
                    .GroupBy(e => e.EmployeeId)
                    .Select(e => new AttendanceSummaryPayrollTable
                    {
                        TenantId = tenant.TenancyName,
                        EmployeeId = e.Key,
                        LastName = e.First().LastName,
                        FirstName = e.First().FirstName,
                        Paycode = string.Empty,
                        Department = string.Empty,
                        RegDays = decimal.Round(e.Sum(f => f.RegDays), 2),
                        RegHrs = decimal.Round(e.Sum(f => f.RegHrs), 2),
                        AbsDays = decimal.Round(e.Sum(f => f.AbsDays), 2),
                        AbsHrs = decimal.Round(e.Sum(f => f.AbsHrs), 2),
                        TardyHrs = decimal.Round(e.Sum(f => f.TardyHrs), 2),
                        UTHrs = decimal.Round(e.Sum(f => f.UTHrs), 2),
                        ND1Hrs = decimal.Round(e.Sum(f => f.ND1Hrs), 2),
                        ND2Hrs = decimal.Round(e.Sum(f => f.ND2Hrs), 2),
                        SLDays = decimal.Round(e.Sum(f => f.SLDays), 2),
                        VLDays = decimal.Round(e.Sum(f => f.VLDays), 2),
                        PaidHoliday = decimal.Round(e.Sum(f => f.PaidHoliday), 2),
                    }).OrderBy(e => e.EmployeeId)
                    .ToList();
            }

            return new GetReportsForAttendanceSummaryPayroll
            {
                AttendanceSummaryPayroll = attendanceSummaryPayrolls
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForAttendanceSummaryPayroll> GetAttendanceSummaryPayrollWithMinuteFormat(ReportsDto dto)
        {
            var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);
            var attendanceSummaryPayrolls = new List<AttendanceSummaryPayrollTable>();

            if (dto.PostFilterId)
            {
                var attendanceSummaryPayroll = await _postedTimesheetsRepository.GetAll()
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.Date)
                    .Select(e => new
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        e.Employees.LastName,
                        e.Employees.FirstName,
                        RegDays = ((decimal)e.RegMins / 60) / 8,
                        RegMins = 0,
                        AbsDays = e.Absence,
                        AbsMins = 0,
                        TardyMins = (decimal)e.TardyMins,
                        UtMins = (decimal)e.UtMins,
                        Nd1Mins = (decimal)e.RegNd1Mins,
                        Nd2Mins = (decimal)e.RegNd2Mins,
                        SLDays = 0,
                        VLDays = 0,
                        e.PaidHoliday
                    }).ToListAsync();

                attendanceSummaryPayrolls = attendanceSummaryPayroll
                    .GroupBy(e => e.EmployeeId)
                    .Select(e => new AttendanceSummaryPayrollTable
                    {
                        TenantId = tenant.TenancyName,
                        EmployeeId = e.Key,
                        LastName = e.First().LastName,
                        FirstName = e.First().FirstName,
                        Paycode = string.Empty,
                        Department = string.Empty,
                        RegDays = decimal.Round(e.Sum(f => f.RegDays), 2),
                        RegHrs = _helperAppService.GetMinuteFormat(e.Sum(f => f.RegMins)),
                        AbsDays = decimal.Round(e.Sum(f => f.AbsDays), 2),
                        AbsHrs = _helperAppService.GetMinuteFormat(e.Sum(f => f.AbsMins)),
                        TardyHrs = _helperAppService.GetMinuteFormat(e.Sum(f => f.TardyMins)),
                        UTHrs = _helperAppService.GetMinuteFormat(e.Sum(f => f.UtMins)),
                        ND1Hrs = _helperAppService.GetMinuteFormat(e.Sum(f => f.Nd1Mins)),
                        ND2Hrs = _helperAppService.GetMinuteFormat(e.Sum(f => f.Nd2Mins)),
                        SLDays = decimal.Round(e.Sum(f => f.SLDays), 2),
                        VLDays = decimal.Round(e.Sum(f => f.VLDays), 2),
                        PaidHoliday = decimal.Round(e.Sum(f => f.PaidHoliday), 2),
                    }).ToList();
            }
            else
            {
                var attendanceSummaryPayroll = await _processedTimesheetsRepository.GetAll()
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.Date)
                    .Select(e => new
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        e.Employees.LastName,
                        e.Employees.FirstName,
                        RegDays = ((decimal)e.RegMins / 60) / 8,
                        RegMins = 0,
                        AbsDays = e.Absence,
                        AbsMins = 0,
                        TardyMins = (decimal)e.TardyMins,
                        UtMins = (decimal)e.UtMins,
                        Nd1Mins = (decimal)e.RegNd1Mins,
                        Nd2Mins = (decimal)e.RegNd2Mins,
                        SLDays = 0,
                        VLDays = 0,
                        e.PaidHoliday
                    }).ToListAsync();

                attendanceSummaryPayrolls = attendanceSummaryPayroll
                    .GroupBy(e => e.EmployeeId)
                    .Select(e => new AttendanceSummaryPayrollTable
                    {
                        TenantId = tenant.TenancyName,
                        EmployeeId = e.Key,
                        LastName = e.First().LastName,
                        FirstName = e.First().FirstName,
                        Paycode = string.Empty,
                        Department = string.Empty,
                        RegDays = decimal.Round(e.Sum(f => f.RegDays), 2),
                        RegHrs = _helperAppService.GetMinuteFormat(e.Sum(f => f.RegMins)),
                        AbsDays = decimal.Round(e.Sum(f => f.AbsDays), 2),
                        AbsHrs = _helperAppService.GetMinuteFormat(e.Sum(f => f.AbsMins)),
                        TardyHrs = _helperAppService.GetMinuteFormat(e.Sum(f => f.TardyMins)),
                        UTHrs = _helperAppService.GetMinuteFormat(e.Sum(f => f.UtMins)),
                        ND1Hrs = _helperAppService.GetMinuteFormat(e.Sum(f => f.Nd1Mins)),
                        ND2Hrs = _helperAppService.GetMinuteFormat(e.Sum(f => f.Nd2Mins)),
                        SLDays = decimal.Round(e.Sum(f => f.SLDays), 2),
                        VLDays = decimal.Round(e.Sum(f => f.VLDays), 2),
                        PaidHoliday = decimal.Round(e.Sum(f => f.PaidHoliday), 2),
                    }).ToList();
            }

            return new GetReportsForAttendanceSummaryPayroll
            {
                AttendanceSummaryPayroll = attendanceSummaryPayrolls
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForLeaveBalance> GetLeaveBalance(ReportsDto dto)
        {
            var leaveEarningsQuery = await _leaveEarningDetailsRepository.GetAll()
                .WhereIf(dto.MinDate != null, e => e.LeaveEarnings.AppYear == dto.MinDate.Value.Year)
                .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                .WhereIf(dto.MainOrganizationUnitId > 0,
                    e => e.Employees.User.OrganizationUnits
                        .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new
                {
                    e.EmployeeId,
                    e.LeaveTypeId,
                    e.Earned
                }).ToListAsync();

            var leaveEarnings = leaveEarningsQuery
                .GroupBy(e => new { e.EmployeeId, e.LeaveTypeId })
                .Select(e => new
                {
                    e.First().EmployeeId,
                    e.First().LeaveTypeId,
                    Earned = e.Sum(f => f.Earned)
                }).ToList();

            var declinedIds = (await _statusManager
                .GetStatusByDisplayNames(StatusDisplayNames.Cancelled, StatusDisplayNames.Declined))
                .Select(e => e.Status.Id)
                .ToArray();

            var leaveRequestsQuery = await _leaveRequestsRepository.GetAll()
                .WhereIf(dto.MinDate != null && dto.MaxDate != null, e => e.LeaveRequestDetails
                    .Any(f => f.LeaveDate.Year == dto.MinDate.Value.Year && f.LeaveDate <= dto.MaxDate))
                .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                .WhereIf(dto.FrequencyFilterId > 0,
                    e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                .WhereIf(dto.MainOrganizationUnitId > 0,
                    e => e.Employees.User.OrganizationUnits
                        .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                .Where(e => e.TenantId == AbpSession.TenantId && !declinedIds.Contains(e.StatusId))
                .Select(e => new
                {
                    e.EmployeeId,
                    e.Employees.EmployeeCode,
                    EmployeeName = e.Employees.FullName,
                    e.LeaveTypeId,
                    LeaveType = e.LeaveTypes.DisplayName,
                    Taken = e.LeaveRequestDetails.Sum(f => f.Days)
                }).ToListAsync();

            var leaveRequests = leaveRequestsQuery
                .GroupBy(e => new { e.EmployeeId, e.LeaveTypeId })
                .Select(e => new
                {
                    e.First().EmployeeId,
                    e.First().EmployeeCode,
                    e.First().EmployeeName,
                    e.First().LeaveTypeId,
                    e.First().LeaveType,
                    Taken = e.Sum(f => f.Taken)
                }).ToList();

            var leaveBalance = leaveEarnings.Join(leaveRequests,
                earn => new { earn.EmployeeId, earn.LeaveTypeId },
                req => new { req.EmployeeId, req.LeaveTypeId },
                (earn, req) => new LeaveBalanceTable
                {
                    EmployeeId = req.EmployeeCode,
                    EmployeeName = req.EmployeeName,
                    LeaveType = req.LeaveType,
                    LeaveEarned = decimal.Round(earn.Earned, 2),
                    LeaveTaken = decimal.Round(req.Taken, 2)
                }).ToList();

            return new GetReportsForLeaveBalance
            {
                LeaveBalance = leaveBalance
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForLeaveSummary> GetLeaveSummary(ReportsDto dto)
        {
            var leaveSummaries = new List<LeaveSummaryTable>();

            if (dto.PostFilterId)
            {
                var leaves = await _postedLeavesRepository.GetAll()
                    .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.LeaveTypeId)
                    .Select(e => new
                    {
                        e.EmployeeId,
                        e.LeaveTypeId,
                        e.Employees.EmployeeCode,
                        e.Employees.FullName,
                        e.LeaveTypes.DisplayName,
                        e.Days,
                    }).ToListAsync();

                leaveSummaries = leaves
                    .GroupBy(e => e.EmployeeId)
                    .Select(e => new LeaveSummaryTable
                    {
                        EmployeeId = e.First().EmployeeCode,
                        EmployeeName = e.First().FullName,
                        TypeValues = e.Select(f => new Tuple<int, decimal>
                        (
                            f.LeaveTypeId,
                            e.Where(g => g.LeaveTypeId == f.LeaveTypeId).Sum(g => g.Days)
                        )).ToList(),
                    }).ToList();
            }
            else
            {
                var leaves = await _processedLeavesRepository.GetAll()
                    .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.LeaveTypeId)
                    .Select(e => new
                    {
                        e.EmployeeId,
                        e.LeaveTypeId,
                        e.Employees.EmployeeCode,
                        e.Employees.FullName,
                        e.LeaveTypes.DisplayName,
                        e.Days,
                    }).ToListAsync();

                leaveSummaries = leaves
                    .GroupBy(e => e.EmployeeId)
                    .Select(e => new LeaveSummaryTable
                    {
                        EmployeeId = e.First().EmployeeCode,
                        EmployeeName = e.First().FullName,
                        TypeValues = e.Select(f => new Tuple<int, decimal>
                        (
                            f.LeaveTypeId,
                            e.Where(g => g.LeaveTypeId == f.LeaveTypeId).Sum(g => g.Days)
                        )).ToList(),
                    }).ToList();
            }

            var leaveTypes = await _leaveTypesRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .Select(e => new Tuple<int, string>(e.Id, e.DisplayName))
                .ToListAsync();

            return new GetReportsForLeaveSummary
            {
                LeaveTypes = leaveTypes,
                LeaveSummary = leaveSummaries
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForLeaveSummaryPayroll> GetLeaveSummaryPayroll(ReportsDto dto)
        {
            var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);
            var leaveSummaryPayrolls = new List<LeaveSummaryPayrollTable>();

            if (dto.PostFilterId)
            {
                var leaveSummaryPayroll = await _postedLeavesRepository.GetAll()
                    .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.LeaveTypeId)
                    .Select(e => new
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        e.Employees.LastName,
                        e.Employees.FirstName,
                        e.LeaveTypeId,
                        LeaveType = e.LeaveTypes.DisplayName,
                        e.Days
                    }).ToListAsync();

                leaveSummaryPayrolls = leaveSummaryPayroll
                    .GroupBy(e => new { e.EmployeeId, e.LeaveTypeId })
                    .Select(e => new LeaveSummaryPayrollTable
                    {
                        TenantId = tenant.TenancyName,
                        EmployeeId = e.Key.EmployeeId,
                        LastName = e.First().LastName,
                        FirstName = e.First().FirstName,
                        Paycode = string.Empty,
                        Department = string.Empty,
                        TSCode = e.First().LeaveType,
                        TSDays = decimal.Round(e.Sum(f => f.Days), 2),
                        TSHrs = 0,
                        TSMins = 0
                    }).ToList();
            }
            else
            {
                var leaveSummaryPayroll = await _processedLeavesRepository.GetAll()
                    .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.LeaveTypeId)
                    .Select(e => new
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        e.Employees.LastName,
                        e.Employees.FirstName,
                        e.LeaveTypeId,
                        LeaveType = e.LeaveTypes.DisplayName,
                        e.Days
                    }).ToListAsync();

                leaveSummaryPayrolls = leaveSummaryPayroll
                    .GroupBy(e => new { e.EmployeeId, e.LeaveTypeId })
                    .Select(e => new LeaveSummaryPayrollTable
                    {
                        TenantId = tenant.TenancyName,
                        EmployeeId = e.Key.EmployeeId,
                        LastName = e.First().LastName,
                        FirstName = e.First().FirstName,
                        Paycode = string.Empty,
                        Department = string.Empty,
                        TSCode = e.First().LeaveType,
                        TSDays = decimal.Round(e.Sum(f => f.Days), 2),
                        TSHrs = 0,
                        TSMins = 0
                    }).ToList();
            }

            return new GetReportsForLeaveSummaryPayroll
            {
                LeaveSummaryPayroll = leaveSummaryPayrolls
            };
        }

        //private void CreateDynamicColumns(XtraReport report, List<string> overtimeTypes)
        //{
        //    XRTable table = new XRTable();
        //    table.BeginInit();
        //    table.WidthF = report.PageWidth - report.Margins.Left - report.Margins.Right;
        //    table.Borders = DevExpress.XtraPrinting.BorderSide.All;

        //    // Create header row
        //    XRTableRow headerRow = new XRTableRow();
        //    AddTableCell(headerRow, "Emp ID", true);
        //    AddTableCell(headerRow, "Emp Name", true);

        //    // Create detail row
        //    XRTableRow detailRow = new XRTableRow();
        //    AddTableCell(detailRow, "[EmployeeId]", false);
        //    AddTableCell(detailRow, "[EmployeeName]", false);

        //    foreach (var col in overtimeTypes)
        //    {
        //        AddTableCell(headerRow, col, true);
        //        AddTableCell(detailRow, $"[TypeValues.FirstOrDefault(x => x.Item1 == {col}).Item2]", false);
        //    }

        //    table.Rows.Add(headerRow);
        //    table.Rows.Add(detailRow);
        //    table.EndInit();

        //    report.Bands[BandKind.Detail].Controls.Add(table);
        //}

        //private void AddTableCell(XRTableRow row, string text, bool isHeader)
        //{
        //    XRTableCell cell = new XRTableCell
        //    {
        //        Text = isHeader ? text : null,
        //        ExpressionBindings = isHeader ? null : new ExpressionBindingCollection
        //{
        //    new ExpressionBinding("BeforePrint", "Text", text)
        //},
        //        Font = new System.Drawing.Font("Arial", 8.25F, isHeader ? System.Drawing.FontStyle.Bold : System.Drawing.FontStyle.Regular),
        //        Borders = DevExpress.XtraPrinting.BorderSide.All,
        //        TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft
        //    };
        //    row.Cells.Add(cell);
        //}

        [RemoteService(false)]
        public async Task<GetReportsForOvertimeSummary> GetOvertimeSummary(ReportsDto dto)
        {
            var overtimeTypes = new List<Tuple<int, string>>();
            var overtimeSummaries = new List<OvertimeSummaryTable>();

            if (dto.PostFilterId)
            {
                var overtimes = await _postedOvertimesRepository.GetAll()
                    .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.OvertimeTypeId)
                    .Select(e => new
                    {
                        e.EmployeeId,
                        e.OvertimeTypeId,
                        e.Employees.EmployeeCode,
                        e.Employees.FullName,
                        e.OvertimeTypes.ShortName,
                        e.Minutes,
                    }).ToListAsync();

                overtimeTypes = await _overtimeTypesRepository.GetAll()
                    .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                    .Where(e => overtimes.Select(f => f.OvertimeTypeId).Distinct().ToList().Contains(e.Id))
                    .Select(e => new Tuple<int, string>(e.Id, e.ShortName))
                    .ToListAsync();

                overtimeSummaries = overtimes
                    .GroupBy(e => e.EmployeeId)
                    .Select(e => new OvertimeSummaryTable
                    {
                        EmployeeId = e.First().EmployeeCode,
                        EmployeeName = e.First().FullName,
                        TypeValues = e.Select(f => new
                        {
                            f.OvertimeTypeId,
                            f.Minutes
                        })
                        .GroupBy(f => f.OvertimeTypeId)
                        .Select(f => new Tuple<int, decimal>
                        (
                            f.Key,
                            decimal.Round((decimal)f.Sum(g => g.Minutes) / 60, 2)
                        )).ToList()
                    }).ToList();
            }
            else
            {
                var overtimes = await _processedOvertimesRepository.GetAll()
                    .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.OvertimeTypeId)
                    .Select(e => new
                    {
                        e.EmployeeId,
                        e.OvertimeTypeId,
                        e.Employees.EmployeeCode,
                        e.Employees.FullName,
                        e.OvertimeTypes.ShortName,
                        e.Minutes
                    }).ToListAsync();

                overtimeTypes = await _overtimeTypesRepository.GetAll()
                    .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                    .Where(e => overtimes.Select(f => f.OvertimeTypeId).Distinct().ToList().Contains(e.Id))
                    .Select(e => new Tuple<int, string>(e.Id, e.ShortName))
                    .ToListAsync();

                overtimeSummaries = overtimes
                    .GroupBy(e => e.EmployeeId)
                    .Select(e => new OvertimeSummaryTable
                    {
                        EmployeeId = e.First().EmployeeCode,
                        EmployeeName = e.First().FullName,
                        TypeValues = e.Select(f => new
                        {
                            f.OvertimeTypeId,
                            f.Minutes
                        })
                        .GroupBy(f => f.OvertimeTypeId)
                        .Select(f => new Tuple<int, decimal>
                        (
                            f.Key,
                            decimal.Round((decimal)f.Sum(g => g.Minutes) / 60, 2)
                        )).ToList()
                    }).ToList();
            }

            return new GetReportsForOvertimeSummary
            {
                OvertimeTypes = overtimeTypes,
                OvertimeSummary = overtimeSummaries
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForOvertimeSummaryPayroll> GetOvertimeSummaryPayroll(ReportsDto dto)
        {
            var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);
            var overtimeTypes = new List<Tuple<int, string>>();
            var overtimeSummaryPayrolls = new List<OvertimeSummaryPayrollTable>();

            if (dto.PostFilterId)
            {
                var overtimesPayroll = await _postedOvertimesRepository.GetAll()
                    .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.OvertimeTypeId)
                    .Select(e => new
                    {
                        e.EmployeeId,
                        e.OvertimeTypeId,
                        e.Employees.EmployeeCode,
                        e.Employees.LastName,
                        e.Employees.FirstName,
                        e.OvertimeTypes.ShortName,
                        e.Minutes
                    }).ToListAsync();

                overtimeTypes = await _overtimeTypesRepository.GetAll()
                    .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                    .Where(e => overtimesPayroll.Select(f => f.OvertimeTypeId).Distinct().ToList().Contains(e.Id))
                    .Select(e => new Tuple<int, string>(e.Id, e.ShortName))
                    .ToListAsync();

                overtimeSummaryPayrolls = overtimesPayroll
                    .GroupBy(e => e.EmployeeId)
                    .Select(e => new OvertimeSummaryPayrollTable
                    {
                        TenantId = tenant.TenancyName,
                        EmployeeId = e.First().EmployeeCode,
                        LastName = e.First().LastName,
                        FirstName = e.First().FirstName,
                        Paycode = string.Empty,
                        Department = string.Empty,
                        TypeValues = e.Select(f => new Tuple<int, decimal>
                        (
                            f.OvertimeTypeId,
                            decimal.Round((decimal)f.Minutes / 60, 2)
                        )).ToList(),
                    }).ToList();
            }
            else
            {
                var overtimesPayroll = await _processedOvertimesRepository.GetAll()
                    .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.OvertimeTypeId)
                    .Select(e => new
                    {
                        e.EmployeeId,
                        e.OvertimeTypeId,
                        e.Employees.EmployeeCode,
                        e.Employees.LastName,
                        e.Employees.FirstName,
                        e.OvertimeTypes.ShortName,
                        e.Minutes
                    }).ToListAsync();

                overtimeTypes = await _overtimeTypesRepository.GetAll()
                    .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                    .Where(e => overtimesPayroll.Select(f => f.OvertimeTypeId).Distinct().ToList().Contains(e.Id))
                    .Select(e => new Tuple<int, string>(e.Id, e.ShortName))
                    .ToListAsync();

                overtimeSummaryPayrolls = overtimesPayroll
                    .GroupBy(e => e.EmployeeId)
                    .Select(e => new OvertimeSummaryPayrollTable
                    {
                        TenantId = tenant.TenancyName,
                        EmployeeId = e.First().EmployeeCode,
                        LastName = e.First().LastName,
                        FirstName = e.First().FirstName,
                        Paycode = string.Empty,
                        Department = string.Empty,
                        TypeValues = e.Select(f => new Tuple<int, decimal>
                        (
                            f.OvertimeTypeId,
                            decimal.Round((decimal)f.Minutes / 60, 2)
                        )).ToList(),
                    }).ToList();
            }

            return new GetReportsForOvertimeSummaryPayroll
            {
                OvertimeTypes = overtimeTypes,
                OvertimeSummaryPayroll = overtimeSummaryPayrolls
            };
        }

        [RemoteService(false)] 
        public async Task<GetReportsForOvertimeSummaryPayroll> GetOvertimeSummaryPayrollWithMinuteFormat(ReportsDto dto)
        {
            var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);
            var overtimeTypes = new List<Tuple<int, string>>();
            var overtimeSummaryPayrolls = new List<OvertimeSummaryPayrollTable>();

            if (dto.PostFilterId)
            {
                var overtimesPayroll = await _postedOvertimesRepository.GetAll()
                    .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.OvertimeTypeId)
                    .Select(e => new
                    {
                        e.EmployeeId,
                        e.OvertimeTypeId,
                        e.Employees.EmployeeCode,
                        e.Employees.LastName,
                        e.Employees.FirstName,
                        e.OvertimeTypes.ShortName,
                        e.Minutes
                    }).ToListAsync();

                overtimeTypes = await _overtimeTypesRepository.GetAll()
                    .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                    .Where(e => overtimesPayroll.Select(f => f.OvertimeTypeId).Distinct().ToList().Contains(e.Id))
                    .Select(e => new Tuple<int, string>(e.Id, e.ShortName))
                    .ToListAsync();

                overtimeSummaryPayrolls = overtimesPayroll
                    .GroupBy(e => e.EmployeeId)
                    .Select(e => new OvertimeSummaryPayrollTable
                    {
                        TenantId = tenant.TenancyName,
                        EmployeeId = e.First().EmployeeCode,
                        LastName = e.First().LastName,
                        FirstName = e.First().FirstName,
                        Paycode = string.Empty,
                        Department = string.Empty,
                        TypeValues = e.Select(f => new
                        {
                            f.OvertimeTypeId,
                            f.Minutes
                        })
                        .GroupBy(f => f.OvertimeTypeId)
                        .Select(f => new Tuple<int, decimal>
                        (
                            f.Key,
                            _helperAppService.GetMinuteFormat(f.Sum(g => g.Minutes))
                        )).ToList(),
                    }).ToList();
            }
            else
            {
                var overtimesPayroll = await _processedOvertimesRepository.GetAll()
                    .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                    .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                    .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                    .WhereIf(dto.MainOrganizationUnitId > 0,
                        e => e.Employees.User.OrganizationUnits
                            .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                    .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                    .OrderBy(e => e.EmployeeId)
                    .ThenBy(e => e.OvertimeTypeId)
                    .Select(e => new
                    {
                        e.EmployeeId,
                        e.OvertimeTypeId,
                        e.Employees.EmployeeCode,
                        e.Employees.LastName,
                        e.Employees.FirstName,
                        e.OvertimeTypes.ShortName,
                        e.Minutes
                    }).ToListAsync();

                overtimeTypes = await _overtimeTypesRepository.GetAll()
                    .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                    .Where(e => overtimesPayroll.Select(f => f.OvertimeTypeId).Distinct().ToList().Contains(e.Id))
                    .Select(e => new Tuple<int, string>(e.Id, e.ShortName))
                    .ToListAsync();

                overtimeSummaryPayrolls = overtimesPayroll
                    .GroupBy(e => e.EmployeeId)
                    .Select(e => new OvertimeSummaryPayrollTable
                    {
                        TenantId = tenant.TenancyName,
                        EmployeeId = e.First().EmployeeCode,
                        LastName = e.First().LastName,
                        FirstName = e.First().FirstName,
                        Paycode = string.Empty,
                        Department = string.Empty,
                        TypeValues = e.Select(f => new
                        {
                            f.OvertimeTypeId,
                            f.Minutes
                        })
                        .GroupBy(f => f.OvertimeTypeId)
                        .Select(f => new Tuple<int, decimal>
                        (
                            f.Key,
                            _helperAppService.GetMinuteFormat(f.Sum(g => g.Minutes))
                        )).ToList(),
                    }).ToList();
            }

            return new GetReportsForOvertimeSummaryPayroll
            {
                OvertimeTypes = overtimeTypes,
                OvertimeSummaryPayroll = overtimeSummaryPayrolls
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForAttendanceTransaction> GetAttendanceTransaction(ReportsDto dto)
        {
            var output = await _attendanceRequestsRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                .WhereIf(dto.MainOrganizationUnitId > 0,
                    e => e.Employees.User.OrganizationUnits
                        .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                .Where(e => dto.MinDate <= e.Date && dto.MaxDate >= e.Date)
                .Select(e => new AttendanceTransactionTable
                {
                    EmployeeId = e.Employees.EmployeeCode,
                    EmployeeName = e.Employees.FullName,
                    DateFiled = e.CreationTime,
                    Date = e.Date,
                    Reason = e.AttendanceReason.DisplayName,
                    Status = e.Status.DisplayName
                }).ToListAsync();

            return new GetReportsForAttendanceTransaction
            {
                AttendanceTransaction = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForExcuseTransaction> GetExcuseTransaction(ReportsDto dto)
        {
            var output = await _excuseRequestsRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                .WhereIf(dto.MainOrganizationUnitId > 0,
                    e => e.Employees.User.OrganizationUnits
                        .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                .Select(e => new ExcuseTransactionTable
                {
                    EmployeeId = e.Employees.EmployeeCode,
                    EmployeeName = e.Employees.FullName,
                    DateFiled = e.CreationTime,
                    Date = e.ExcuseDate,
                    Reason = e.ExcuseReasons.DisplayName,
                    Status = e.Status.DisplayName
                }).ToListAsync();

            return new GetReportsForExcuseTransaction
            {
                ExcuseTransaction = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForLeaveTransaction> GetLeaveTransaction(ReportsDto dto)
        {
            var output = await _leaveRequestsRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                .WhereIf(dto.FrequencyFilterId > 0,
                    e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                .WhereIf(dto.MainOrganizationUnitId > 0,
                    e => e.Employees.User.OrganizationUnits
                        .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                .Where(e => e.LeaveRequestDetails.Any(f => dto.MinDate <= f.LeaveDate && dto.MaxDate >= f.LeaveDate))
                .Select(e => new LeaveTransactionTable
                {
                    EmployeeId = e.Employees.EmployeeCode,
                    EmployeeName = e.Employees.FullName,
                    LeaveType = e.LeaveTypes.DisplayName,
                    DateFiled = e.CreationTime,
                    StartDate = e.LeaveRequestDetails.Min(f => f.LeaveDate),
                    EndDate = e.LeaveRequestDetails.Max(f => f.LeaveDate),
                    Days = e.LeaveRequestDetails.Sum(f => f.Days),
                    Reason = e.LeaveReasons.DisplayName,
                    Status = e.Status.DisplayName
                }).ToListAsync();

            return new GetReportsForLeaveTransaction
            {
                LeaveTransaction = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForOfficialBusinessTransaction> GetOfficialBusinessTransaction(ReportsDto dto)
        {
            var output = await _officialBussinessRequestsRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                .WhereIf(dto.MainOrganizationUnitId > 0,
                    e => e.Employees.User.OrganizationUnits
                        .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                .Where(e => e.OfficialBusinessRequestDetails.Any(f => dto.MinDate <= f.OfficialBusinessDate && dto.MaxDate >= f.OfficialBusinessDate))
                .Select(e => new OfficialBusinessTransactionTable
                {
                    EmployeeId = e.Employees.EmployeeCode,
                    EmployeeName = e.Employees.FullName,
                    DateFiled = e.CreationTime,
                    StartDate = e.OfficialBusinessRequestDetails.Min(f => f.OfficialBusinessDate),
                    EndDate = e.OfficialBusinessRequestDetails.Max(f => f.OfficialBusinessDate),
                    Reason = e.OfficialBusinessReasons.DisplayName,
                    Status = e.Status.DisplayName
                }).ToListAsync();

            return new GetReportsForOfficialBusinessTransaction
            {
                OfficialBusinessTransaction = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForOvertimeTransaction> GetOvertimeTransaction(ReportsDto dto)
        {
            var output = await _overtimeRequestsRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                    .WhereIf(dto.FrequencyFilterId > 0,
                        e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                .WhereIf(dto.MainOrganizationUnitId > 0,
                    e => e.Employees.User.OrganizationUnits
                        .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                .Where(e => dto.MinDate <= e.OvertimeStart && dto.MaxDate >= e.OvertimeEnd)
                .Select(e => new OvertimeTransactionTable
                {
                    EmployeeId = e.Employees.EmployeeCode,
                    EmployeeName = e.Employees.FullName,
                    DateFiled = e.CreationTime,
                    AttendanceDate = e.OvertimeDate,
                    OTStart = e.OvertimeStart,
                    OTEnd = e.OvertimeEnd,
                    Reason = e.OvertimeReasons.DisplayName,
                    Status = e.Status.DisplayName
                }).ToListAsync();

            return new GetReportsForOvertimeTransaction
            {
                OvertimeTransaction = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForRestDayTransaction> GetRestDayTransaction(ReportsDto dto)
        {
            var output = await _restDayRequestsRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                .WhereIf(dto.FrequencyFilterId > 0,
                    e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                .WhereIf(dto.MainOrganizationUnitId > 0,
                    e => e.Employees.User.OrganizationUnits
                        .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                .Where(e => dto.MinDate <= e.RestDayEnd && dto.MaxDate >= e.RestDayStart)
                .Select(e => new RestDayTransactionTable
                {
                    EmployeeId = e.Employees.EmployeeCode,
                    EmployeeName = e.Employees.FullName,
                    DateFiled = e.CreationTime,
                    StartDate = e.RestDayEnd,
                    EndDate = e.RestDayStart,
                    Reason = e.RestDayReasons.DisplayName,
                    Status = e.Status.DisplayName
                }).ToListAsync();

            return new GetReportsForRestDayTransaction
            {
                RestDayTransaction = output
            };
        }

        [RemoteService(false)]
        public async Task<GetReportsForShiftTransaction> GetShiftTransaction(ReportsDto dto)
        {
            var output = await _shiftRequestsRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .WhereIf(dto.EmployeeId > 0, e => e.EmployeeId == dto.EmployeeId)
                .WhereIf(dto.FrequencyFilterId > 0,
                    e => e.Employees.EmployeeTimeInfo.FrequencyId == dto.FrequencyFilterId)
                .WhereIf(dto.Rank != null, e => e.Employees.Rank == dto.Rank)
                .WhereIf(dto.MainOrganizationUnitId > 0,
                    e => e.Employees.User.OrganizationUnits
                        .Any(f => dto.OrgUnitIds.Contains(f.OrganizationUnitId)))
                .Where(e => dto.MinDate <= e.ShiftStart && dto.MaxDate >= e.ShiftEnd)
                .Select(e => new ShiftTransactionTable
                {
                    EmployeeId = e.Employees.EmployeeCode,
                    EmployeeName = e.Employees.FullName,
                    ShiftType = e.ShiftTypes.DisplayName,
                    DateFiled = e.CreationTime,
                    StartDate = e.ShiftStart,
                    EndDate = e.ShiftEnd,
                    Reason = e.ShiftReasons.DisplayName,
                    Status = e.Status.DisplayName
                }).ToListAsync();

            return new GetReportsForShiftTransaction
            {
                ShiftTransaction = output
            };
        }
    }
}
