﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.HR;
using MMB.ePay.Timekeeping.Dtos;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class WelcomeAppService : ePayAppServiceBase, IWelcomeAppService
    {
        private readonly IRepository<AttendancePeriods> _periodsRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<EmployeeTimeInfo> _employeeTimeInfoRepository;
        private readonly IRepository<Status> _statusRepository;
        private readonly IRepository<EmployeeApprovers> _employeeApproversRepository;
        private readonly IRepository<RawTimeLogs> _rawTimeLogsRepository;
        private readonly IRepository<TKSettings> _tkSettingsRepository;
        private readonly IRepository<ShiftRequests> _shiftRequestsRepository;
        private readonly IRepository<RestDayRequests> _restDayRequestsRepository;
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;

        public WelcomeAppService(
            IRepository<AttendancePeriods> periodsRepository,
            IRepository<Employees> employeesRepository,
            IRepository<EmployeeTimeInfo> employeeTimeInfoRepository,
            IRepository<Status> statusRepository,
            IRepository<EmployeeApprovers> employeeApproversRepository,
            IRepository<RawTimeLogs> rawTimeLogsRepository,
            IRepository<TKSettings> tkSettingsRepository,
            IRepository<ShiftRequests> shiftRequestsRepository,
            IRepository<RestDayRequests> restDayRequestsRepository,
            IAttendancePeriodsAppService attendancePeriodsAppService)
        {
            _periodsRepository = periodsRepository;
            _employeesRepository = employeesRepository;
            _employeeTimeInfoRepository = employeeTimeInfoRepository;
            _statusRepository = statusRepository;
            _employeeApproversRepository = employeeApproversRepository;
            _rawTimeLogsRepository = rawTimeLogsRepository;
            _tkSettingsRepository = tkSettingsRepository;
            _shiftRequestsRepository = shiftRequestsRepository;
            _restDayRequestsRepository = restDayRequestsRepository;
            _attendancePeriodsAppService = attendancePeriodsAppService;
        }

        public async Task<GetWelcomeForViewDto> GetWelcomeForView()
        {
            var currentDate = DateTime.Today;

            if (AbpSession.TenantId == null)
            {
                return new GetWelcomeForViewDto
                {
                    Welcome = new WelcomeDto
                    {
                        FullName = "Host Admin",
                        CurrentDate = currentDate.ToString("MM/dd/yyyy"),
                        TransactionPeriod = "N/A"
                    }
                };
            }

            var pendingIds = await _statusRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Where(e => e.AccessLevel == 0 && !e.WorkFlowStatus.Any(f => f.NextStatus == null))
                .Select(e => e.Id)
                .ToListAsync();

            var approvedId = await _statusRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId && e.DisplayName == "Approved")
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            var frequencyId = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => (int?)e.EmployeeTimeInfo.FrequencyId)
                .FirstOrDefaultAsync();

            var period = await _periodsRepository.GetAll()
                .WhereIf(frequencyId != null, e => e.FrequencyId == frequencyId)
                .Where(e => e.TenantId == AbpSession.TenantId && !e.Posted)
                .OrderBy(e => e.StartDate)
                .Select(e => new { e.StartDate, e.EndDate })
                .FirstOrDefaultAsync();

            var output = new GetWelcomeForViewDto();

            if (period == null)
            {
                output.Welcome = new WelcomeDto
                {
                    FullName = "Admin",
                    CurrentDate = currentDate.ToString("MM/dd/yyyy")
                };

                return output;
            }

            var employee = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new
                {
                    e.FullName,
                    e.Position,
                    Shift = e.EmployeeTimeInfo.ShiftTypes.DisplayName,
                    e.Email,
                    e.DateEmployed,
                    e.EmployeeTimeInfo.SwipeCode,
                    e.Id,
                    InvalidCount = e.Attendances
                        .Count(f => f.Date >= period.StartDate && f.Date <= period.EndDate && f.Invalid),
                    AttendanceCount = e.AttendanceRequests
                        .Count(f => f.Date >= period.StartDate && pendingIds.Contains(f.StatusId)),
                    ExcuseCount = e.ExcuseRequests
                        .Count(f => f.ExcuseDate >= period.StartDate && pendingIds.Contains(f.StatusId)),
                    LeaveCount = e.LeaveRequests
                        .Count(f => f.LeaveRequestDetails.Min(g => g.LeaveDate) >= period.StartDate 
                            && pendingIds.Contains(f.StatusId)),
                    OBCount = e.OfficialBusinessRequests
                        .Count(f => f.OfficialBusinessRequestDetails.Min(g => g.OfficialBusinessDate) >= period.StartDate 
                            && pendingIds.Contains(f.StatusId)),
                    OTCount = e.OvertimeRequests
                        .Count(f => f.OvertimeDate >= period.StartDate && pendingIds.Contains(f.StatusId)),
                    RestDayCount = e.RestDayRequests
                        .Count(f => f.RestDayEnd >= period.StartDate && pendingIds.Contains(f.StatusId)),
                    ShiftCount = e.ShiftRequests
                        .Count(f => f.ShiftStart >= period.StartDate && pendingIds.Contains(f.StatusId))
                }).FirstOrDefaultAsync();


            if (employee == null)
            {
                output.Welcome = new WelcomeDto
                {
                    FullName = "Admin",
                    CurrentDate = currentDate.ToString("MM/dd/yyyy"),
                    TransactionPeriod = $"{period.StartDate:MM/dd/yyyy} - {period.EndDate:MM/dd/yyyy}"
                };

                return output;
            }

            var dayOfWeek = ((int)DateTime.Now.DayOfWeek == 0 ? 7 : (int)DateTime.Now.DayOfWeek).ToString();
            var defaultShift = await _employeeTimeInfoRepository.GetAll()
                .Where(e => e.EmployeeId == employee.Id)
                .Select(e => new
                {
                    e.ShiftTypes.DisplayName,
                    RestDay = e.RestDay.ToCharArray(),
                    ShiftTypeDetails = e.ShiftTypes.ShiftTypeDetails.Where(f => f.Days.Contains(dayOfWeek))
                        .Select(f => new
                        {
                            f.TimeIn,
                            f.TimeOut
                        }).First()
                }).FirstOrDefaultAsync();

            var defaultRestDay = string.Empty;
            if (defaultShift != null)
            {
                defaultRestDay = string.Join(",", defaultShift.RestDay
                    .Select(e => Enum.GetName(typeof(DayOfWeek),
                        (int)char.GetNumericValue(e == '7' ? '0' : e)).Substring(0, 3)));
            }

            var currentShift = await _shiftRequestsRepository.GetAll()
                .Where(e => e.ShiftStart <= currentDate && e.ShiftEnd >= currentDate
                    && e.EmployeeId == employee.Id && e.StatusId == approvedId)
                .OrderByDescending(e => e.CreationTime)
                .Select(e => new
                {
                    e.ShiftTypes.DisplayName,
                    ShiftTypeDetails = e.ShiftTypes.ShiftTypeDetails.Where(f => f.Days.Contains(dayOfWeek))
                        .Select(f => new
                        {
                            f.TimeIn,
                            f.TimeOut
                        }).First()
                }).FirstOrDefaultAsync();

            var restDay = await _restDayRequestsRepository.GetAll()
                .Where(e => e.RestDayStart <= currentDate && e.RestDayEnd >= currentDate
                    && e.EmployeeId == employee.Id && e.StatusId == approvedId)
                .Select(e => e.RestDayCode)
                .FirstOrDefaultAsync();

            var currentRestDay = string.Empty;
            if (restDay != null)
            {
                currentRestDay = string.Join(",", restDay
                    .Select(e => Enum.GetName(typeof(DayOfWeek),
                        (int)char.GetNumericValue(e == '7' ? '0' : e)).Substring(0, 3)));
            }

            var employeeApprovers = await _employeeApproversRepository.GetAll()
                .Where(e => e.Employees.User.Id == AbpSession.UserId)
                .OrderBy(e => e.ApproverOrders.Level)
                    .ThenBy(e => e.ApproverOrders.IsBackup)
                .Select(e => new Tuple<string, string>
                    (
                        e.ApproverOrders.DisplayName,
                        e.Approvers.FullName)
                    )
                .ToListAsync();

            var minDate = await _attendancePeriodsAppService.GetMinUnpostedDate();

            var mainApprovers = await _employeeApproversRepository.GetAll()
                .Include(e => e.Employees)
                .Where(e => e.ApproverId == employee.Id && !e.ApproverOrders.IsBackup)
                .Select(e => new
                {
                    AttendanceCount = e.Employees.AttendanceRequests
                        .Count(f => f.Status.AccessLevel == e.ApproverOrders.Level 
                            && f.Date >= minDate),
                    LeaveCount = e.Employees.LeaveRequests
                        .Count(f => f.Status.AccessLevel == e.ApproverOrders.Level 
                            && f.LeaveRequestDetails.Any(g => g.LeaveDate >= minDate)),
                    OBCount = e.Employees.OfficialBusinessRequests
                        .Count(f => f.Status.AccessLevel == e.ApproverOrders.Level
                            && f.OfficialBusinessRequestDetails.Any(g => g.OfficialBusinessDate >= minDate)),
                    OTCount = e.Employees.OvertimeRequests
                        .Count(f => f.Status.AccessLevel == e.ApproverOrders.Level
                            && f.OvertimeDate >= minDate),
                    RestDayCount = e.Employees.RestDayRequests
                        .Count(f => f.Status.AccessLevel == e.ApproverOrders.Level
                            && f.RestDayStart >= minDate),
                    ShiftCount = e.Employees.ShiftRequests
                        .Count(f => f.Status.AccessLevel == e.ApproverOrders.Level
                            && f.ShiftStart >= minDate)
                }).ToListAsync();

            var backupApprovers = await _employeeApproversRepository.GetAll()
                .Include(e => e.Employees)
                .Where(e => e.ApproverId == employee.Id && e.ApproverOrders.IsBackup)
                .Select(e => new
                {
                    AttendanceCount = e.Employees.AttendanceRequests
                        .Count(f => f.Status.AccessLevel == e.ApproverOrders.Level 
                            && f.Date >= minDate),
                    LeaveCount = e.Employees.LeaveRequests
                        .Count(f => f.Status.AccessLevel == e.ApproverOrders.Level 
                            && f.LeaveRequestDetails.Any(g => g.LeaveDate >= minDate)),
                    OBCount = e.Employees.OfficialBusinessRequests
                        .Count(f => f.Status.AccessLevel == e.ApproverOrders.Level 
                            && f.OfficialBusinessRequestDetails.Any(g => g.OfficialBusinessDate >= minDate)),
                    OTCount = e.Employees.OvertimeRequests
                        .Count(f => f.Status.AccessLevel == e.ApproverOrders.Level 
                            && f.OvertimeDate >= minDate),
                    RestDayCount = e.Employees.RestDayRequests
                        .Count(f => f.Status.AccessLevel == e.ApproverOrders.Level 
                            && f.RestDayStart >= minDate),
                    ShiftCount = e.Employees.ShiftRequests
                        .Count(f => f.Status.AccessLevel == e.ApproverOrders.Level
                            && f.ShiftStart >= minDate)
                }).ToListAsync();

            var ioSetting = await _tkSettingsRepository.GetAll()
                .Where(e => e.Key == "RAWDATA_HAS_IO_INDICATOR")
                .Select(e => e.Value == "true")
                .FirstOrDefaultAsync();

            var timeEntries = await _rawTimeLogsRepository.GetAll()
                .Where(e => e.SwipeCode == employee.SwipeCode && e.LogTime.Date == currentDate)
                .OrderBy(e => e.LogTime)
                .Select(e => e.LogTime.ToString("hh:mm tt") + (ioSetting ? e.InOut : string.Empty))
                .ToListAsync();

            output.Welcome = new WelcomeDto
            {
                EmployeeApprovers = employeeApprovers,
                InvalidCount = employee.InvalidCount,
                ListTimeEntriesToday = timeEntries,
                IsMainApprover = mainApprovers.Count > 0,
                IsBackupApprover = backupApprovers.Count > 0,
                FullName = employee.FullName,
                Position = employee.Position,
                Shift = employee.Shift,
                Email = employee.Email,
                DateEmployed = employee.DateEmployed?.ToString("MM/dd/yyyy"),
                CurrentDate = currentDate.ToString("MM/dd/yyyy"),
                TransactionPeriod = $"{period.StartDate:MM/dd/yyyy} - {period.EndDate:MM/dd/yyyy}",
                AttendancePending = employee.AttendanceCount,
                LeavePending = employee.LeaveCount,
                OBPending = employee.OBCount,
                OTPending = employee.OTCount,
                RestDayPending = employee.RestDayCount,
                ShiftPending = employee.ShiftCount,
                AttendanceMain = mainApprovers.Sum(e => e.AttendanceCount),
                LeaveMain = mainApprovers.Sum(e => e.LeaveCount),
                OBMain = mainApprovers.Sum(e => e.OBCount),
                OTMain = mainApprovers.Sum(e => e.OTCount),
                RestDayMain = mainApprovers.Sum(e => e.RestDayCount),
                ShiftMain = mainApprovers.Sum(e => e.ShiftCount),
                AttendanceBackup = backupApprovers.Sum(e => e.AttendanceCount),
                LeaveBackup = backupApprovers.Sum(e => e.LeaveCount),
                OBBackup = backupApprovers.Sum(e => e.OBCount),
                OTBackup = backupApprovers.Sum(e => e.OTCount),
                RestDayBackup = backupApprovers.Sum(e => e.RestDayCount),
                ShiftBackup = backupApprovers.Sum(e => e.ShiftCount),
                DefaultShiftName = defaultShift?.DisplayName,
                DefaultTimeIn = defaultShift?.ShiftTypeDetails.TimeIn,
                DefaultTimeOut = defaultShift?.ShiftTypeDetails.TimeOut,
                DefaultRestDay = defaultRestDay,
                CurrentShiftName = currentShift?.DisplayName,
                CurrentTimeIn = currentShift?.ShiftTypeDetails.TimeIn,
                CurrentTimeOut = currentShift?.ShiftTypeDetails.TimeOut,
                CurrentRestDay = currentRestDay,
            };

            return output;
        }
    }
}