﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Processor.Dtos;
using MMB.ePay.Timekeeping.Dtos;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class AttendancePeriodsAppService : ePayAppServiceBase, IAttendancePeriodsAppService
    {
        private readonly IRepository<AttendancePeriods> _attendancePeriodsRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<EmployeeTimeInfo> _employeeTimeInfoRepository;

        public AttendancePeriodsAppService(IRepository<AttendancePeriods> attendancePeriodsRepository,
            IRepository<Employees> employeesRepository,
            IRepository<EmployeeTimeInfo> employeeTimeInfoRepository)
        {
            _attendancePeriodsRepository = attendancePeriodsRepository;
            _employeesRepository = employeesRepository;
            _employeeTimeInfoRepository = employeeTimeInfoRepository;
        }

        public async Task<bool> CheckUnpostedPeriodByStartDate(DateTime date) =>
             await _attendancePeriodsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && date >= e.StartDate && !e.Posted)
                .AnyAsync();

        public async Task<DateTime?> GetMinUnpostedDate() =>
            await _attendancePeriodsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId && e.Posted == false)
                .OrderBy(e => e.StartDate)
                .Select(e => (DateTime?)e.StartDate)
                .FirstOrDefaultAsync();

        public async Task<DateTime> GetLatestPostedDate(int? frequencyId)
        {
            if (frequencyId == null)
            {
                frequencyId = await _employeesRepository.GetAll()
                    .Where(e => e.User.Id == AbpSession.UserId)
                    .Select(e => e.EmployeeTimeInfo.FrequencyId)
                    .FirstOrDefaultAsync();
            }

            return await _attendancePeriodsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.FrequencyId == frequencyId
                    && e.Posted)
                .OrderByDescending(e => e.EndDate)
                .Select(e => e.EndDate)
                .FirstOrDefaultAsync();
        }

        public async Task<int> GetFirstUnpostedPeriodId(int frequencyId) =>
            await _attendancePeriodsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.FrequencyId == frequencyId
                    && !e.Posted)
                .OrderBy(e => e.EndDate)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

        public async Task<DateRange> GetUnpostedDateRangeFromFrequencyId(int frequencyId) =>
            await _attendancePeriodsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.FrequencyId == frequencyId
                    && !e.Posted)
                .OrderBy(e => e.EndDate)
                .Select(e => new DateRange
                {
                    Start = e.StartDate,
                    End = e.EndDate.Date + new TimeSpan(23, 59, 59)
                }).FirstOrDefaultAsync();

        public async Task<DateRange> GetDateRangeFromAttendancePeriod(int id) =>
            await _attendancePeriodsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.Id == id)
                .Select(e => new DateRange
                {
                    Start = e.StartDate,
                    End = e.EndDate.Date + new TimeSpan(23, 59, 59)
                }).FirstOrDefaultAsync();

        public async Task<IList<SelectListItem>> GetUnpostedAttendancePeriodList(int frequencyId)
        {
            var attendancePeriodList = await _attendancePeriodsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && !e.Posted
                    && e.FrequencyId == frequencyId)
                .OrderBy(e => e.StartDate)
                .Take(2)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = $"{e.StartDate:MMM d, yyyy} - {e.EndDate:MMM d, yyyy}"
                }).ToListAsync();

            attendancePeriodList.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return attendancePeriodList;
        }

        public async Task<IList<SelectListItem>> GetAttendancePeriodListByYearForTimesheet(int year, int? frequencyId, bool? isPosted)
        {
            if (frequencyId == null)
            {
                frequencyId = await _employeeTimeInfoRepository.GetAll()
                    .Where(e => e.Employees.User.Id == AbpSession.UserId)
                    .Select(e => e.FrequencyId)
                    .FirstOrDefaultAsync();
            }

            var query = _attendancePeriodsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.FrequencyId == frequencyId
                    && e.AppYear == year);

            var periodId = await query
                .Where(e => !e.Posted)
                .OrderBy(e => e.StartDate)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            var nextPeriod = await query
                .Where(e => !e.Posted
                    && e.Id != periodId)
                .OrderBy(e => e.StartDate)
                .Select(e => new { e.EndDate })
                .FirstOrDefaultAsync();

            var attendancePeriodList = await query
                .WhereIf(isPosted != null, e => e.Posted == isPosted)
                .WhereIf(nextPeriod != null, e => e.StartDate <= nextPeriod.EndDate)
                .OrderByDescending(e => e.StartDate)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = $"{e.StartDate:MMM d, yyyy} - {e.EndDate:MMM d, yyyy}",
                    Selected = e.Id == periodId
                }).ToListAsync();

            if (attendancePeriodList.Count == 0)
            {
                attendancePeriodList.Insert(0, new SelectListItem
                {
                    Value = "0",
                    Text = "Select"
                });
            }

            return attendancePeriodList;
        }

        public async Task<IList<SelectListItem>> GetAttendancePeriodListByEmployeeId(int year, int? employeeId, bool? isPosted)
        {
            var frequencyId = await _employeeTimeInfoRepository.GetAll()
                .WhereIf(employeeId != null, e => e.EmployeeId == employeeId)
                .WhereIf(employeeId == null, e => e.Employees.User.Id == AbpSession.UserId)
                .Select(e => e.FrequencyId)
                .FirstOrDefaultAsync(); ;

            var query = _attendancePeriodsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.FrequencyId == frequencyId
                    && e.AppYear == year);

            var periodId = await query
                .Where(e => !e.Posted)
                .OrderBy(e => e.StartDate)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            var attendancePeriodList = await query
                .WhereIf(isPosted != null, e => e.Posted == isPosted)
                .OrderByDescending(e => e.StartDate)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = $"{e.StartDate:MMM d, yyyy} - {e.EndDate:MMM d, yyyy}",
                    Selected = e.Id == periodId
                }).ToListAsync();

            if (attendancePeriodList.Count == 0)
            {
                attendancePeriodList.Insert(0, new SelectListItem
                {
                    Value = "0",
                    Text = "Select"
                });
            }

            return attendancePeriodList;
        }

        public async Task<IList<SelectListItem>> GetAttendancePeriodListByYear(int year, int? frequencyId, bool? isPosted)
        {
            if (frequencyId == null)
            {
                frequencyId = await _employeeTimeInfoRepository.GetAll()
                    .Where(e => e.Employees.User.Id == AbpSession.UserId)
                    .Select(e => e.FrequencyId)
                    .FirstOrDefaultAsync();
            }

            var query = _attendancePeriodsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.FrequencyId == frequencyId
                    && e.AppYear == year);

            var periodId = await query
                .Where(e => !e.Posted)
                .OrderBy(e => e.StartDate)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            var attendancePeriodList = await query
                .WhereIf(isPosted != null, e => e.Posted == isPosted)
                .OrderByDescending(e => e.StartDate)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = $"{e.StartDate:MMM d, yyyy} - {e.EndDate:MMM d, yyyy}",
                    Selected = e.Id == periodId
                }).ToListAsync();

            attendancePeriodList.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "Select"
            });

            //if (attendancePeriodList.Count == 0)
            //{
            //    attendancePeriodList.Insert(0, new SelectListItem
            //    {
            //        Value = "0",
            //        Text = "Select"
            //    });
            //}

            return attendancePeriodList;
        }

        public async Task<IList<SelectListItem>> GetYearListForFilter()
        {
            var currentYear = (short)DateTime.Now.Year;
            var appYears = await _attendancePeriodsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => e.AppYear)
                .Distinct()
                .ToListAsync();

            if (appYears.Count == 0)
                appYears.Add(currentYear);

            var output = appYears.Select(e => new SelectListItem
            {
                Value = e.ToString(),
                Text = e.ToString(),
                Selected = e == currentYear
            }).OrderByDescending(e => e.Text)
            .ToList();

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_AttendancePeriods)]
        public async Task<PagedResultDto<GetAttendancePeriodsForViewDto>> GetAll(GetAllAttendancePeriodsInput input)
        {
            var filteredAttendancePeriods = _attendancePeriodsRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .WhereIf(input.YearFilter > 0, e => e.AppYear == input.YearFilter)
                .WhereIf(input.FrequencyFilter > 0, e => e.FrequencyId == input.FrequencyFilter);

            var pagedAndFilteredAttendancePeriods = filteredAttendancePeriods
                .OrderBy(input.Sorting ?? "StartDate asc")
                .PageBy(input);

            var attendancePeriods = pagedAndFilteredAttendancePeriods
                .Select(o => new GetAttendancePeriodsForViewDto
                {
                    AttendancePeriods = new AttendancePeriodsDto
                    {
                        StartDate = o.StartDate,
                        EndDate = o.EndDate,
                        AppYear = o.AppYear,
                        AppMonth = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(o.AppMonth),
                        Posted = o.Posted,
                        Frequency = o.Frequencies.DisplayName,
                        Id = o.Id
                    }
                });

            var totalCount = await filteredAttendancePeriods.CountAsync();
            var results = await attendancePeriods.ToListAsync();

            return new PagedResultDto<GetAttendancePeriodsForViewDto>(
                totalCount,
                results
            );
        }

        public async Task<GetAttendancePeriodsForViewDto> GetAttendancePeriodsForView(int id)
        {
            var output = await _attendancePeriodsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetAttendancePeriodsForViewDto
                {
                    AttendancePeriods = new AttendancePeriodsDto
                    {
                        StartDate = e.StartDate,
                        EndDate = e.EndDate,
                        AppYear = e.AppYear,
                        AppMonth = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(e.AppMonth),
                        Posted = e.Posted,
                        Frequency = e.Frequencies.DisplayName
                    }
                }).FirstOrDefaultAsync();


            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_AttendancePeriods_Edit)]
        public async Task<GetAttendancePeriodsForEditOutput> GetAttendancePeriodsForEdit(EntityDto input)
        {
            var attendancePeriods = await _attendancePeriodsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetAttendancePeriodsForEditOutput { AttendancePeriods = ObjectMapper.Map<CreateOrEditAttendancePeriodsDto>(attendancePeriods) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditAttendancePeriodsDto input)
        {
            if (input.StartDate > input.EndDate)
                throw new UserFriendlyException("Your request is not valid!",
                    "Invalid date range.");

            var hasExistingPeriod = await CheckExistingPeriod(input);

            if (hasExistingPeriod)
                throw new UserFriendlyException("Your request is not valid!",
                    "Your request is overlapping with an existing period.");

            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        protected virtual async Task<bool> CheckExistingPeriod(CreateOrEditAttendancePeriodsDto input) =>
            await _attendancePeriodsRepository.GetAll()
                .WhereIf(input.Id != null, e => e.Id != input.Id)
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.FrequencyId == input.FrequencyId
                    && e.StartDate <= input.EndDate && e.EndDate >= input.StartDate)
                .AnyAsync();

        [AbpAuthorize(AppPermissions.Pages_AttendancePeriods_Create)]
        protected virtual async Task Create(CreateOrEditAttendancePeriodsDto input)
        {
            var attendancePeriods = ObjectMapper.Map<AttendancePeriods>(input);
            await _attendancePeriodsRepository.InsertAsync(attendancePeriods);
        }

        [AbpAuthorize(AppPermissions.Pages_AttendancePeriods_Edit)]
        protected virtual async Task Update(CreateOrEditAttendancePeriodsDto input)
        {
            var attendancePeriods = await _attendancePeriodsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, attendancePeriods);
        }

        [AbpAuthorize(AppPermissions.Pages_AttendancePeriods_Delete)]
        public async Task Delete(EntityDto input) =>
            await _attendancePeriodsRepository.DeleteAsync(input.Id);
    }
}