﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Hangfire;
using Hangfire.Storage;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize(AppPermissions.Pages_Administration_HangfireDashboard)]
    public class HangfireAppService : ePayAppServiceBase, IHangfireAppService
    {
        public PagedResultDto<GetHangfireForViewDto> GetAll()
        {
            var recurringJobs = JobStorage.Current.GetConnection().GetRecurringJobs();
            var results = recurringJobs
                .Select(e => new GetHangfireForViewDto
                {
                    Hangfire = new HangfireDto
                    {
                        DisplayName = e.Id,
                        Queue = e.Queue,
                        Cron = e.Cron,
                        NextExecution = e.NextExecution,
                        LastExecution = e.LastExecution,
                        Id = e.Id
                    }
                }).ToList();

            return new PagedResultDto<GetHangfireForViewDto>(
                recurringJobs.Count,
                results
            );
        }

        public GetHangfireForViewDto GetHangfireForView(string id) =>
            JobStorage.Current.GetConnection().GetRecurringJobs()
                .Where(e => e.Id == id)
                .Select(e => new GetHangfireForViewDto
                {
                    Hangfire = new HangfireDto
                    {
                        DisplayName = e.Id,
                        Queue = e.Queue,
                        Cron = e.Cron,
                        NextExecution = e.NextExecution,
                        LastExecution = e.LastExecution,
                        Id = e.Id
                    }
                }).FirstOrDefault();

        public void Delete(string id) =>
            RecurringJob.RemoveIfExists(id);
    }
}