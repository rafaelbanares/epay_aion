﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class ShiftTypesAppService : ePayAppServiceBase, IShiftTypesAppService
    {
        private readonly IRepository<ShiftTypes> _shiftTypesRepository;
        private readonly IRepository<ShiftTypeDetails> _shiftTypeDetailsRepository;
        private readonly IRepository<EmployeeTimeInfo> _employeeTimeInfoRepository;
        private readonly IRepository<ShiftRequests> _shiftRequestsRepository;
        private readonly List<char> availableDays = new() { '1', '2', '3', '4', '5', '6', '7' };

        public ShiftTypesAppService(IRepository<ShiftTypes> shiftTypesRepository,
            IRepository<ShiftTypeDetails> shiftTypeDetailsRepository,
            IRepository<EmployeeTimeInfo> employeeTimeInfoRepository,
            IRepository<ShiftRequests> shiftRequestsRepository)
        {
            _shiftTypesRepository = shiftTypesRepository;
            _shiftTypeDetailsRepository = shiftTypeDetailsRepository;
            _employeeTimeInfoRepository = employeeTimeInfoRepository;
            _shiftRequestsRepository = shiftRequestsRepository;
        }

        public async Task<bool> IsGraveyardShift(DateTime date)
        {
            var typeDetail = await GetShiftTypeDetail(date);
            if (typeDetail == null)
                return false;

            var timeIn = DateTime.ParseExact(typeDetail.TimeIn, "HH:mm", CultureInfo.InvariantCulture);
            var timeOut = DateTime.ParseExact(typeDetail.TimeOut, "HH:mm", CultureInfo.InvariantCulture);

            return (timeOut < timeIn);
        }

        public async Task<bool> GetStartOnPreviousDayByDate(DateTime date)
        {
            var typeDetail = await GetShiftTypeDetail(date);
            if (typeDetail == null)
                return false;

            return typeDetail.StartOnPreviousDay;
        }

        private async Task<ShiftTypeDetails> GetShiftTypeDetail(DateTime date)
        {
            var dayOfWeek = (int)date.DayOfWeek;
            var formattedDoW = (dayOfWeek == 0 ? 7 : dayOfWeek).ToString();

            var typeDetail = await _shiftRequestsRepository.GetAll()
                .Where(e => e.Employees.User.Id == AbpSession.UserId
                    && e.ShiftStart <= date && e.ShiftEnd >= date
                    && e.ShiftTypes.ShiftTypeDetails.Any(f => f.Days.Contains(formattedDoW)))
                .Select(e => e.ShiftTypes.ShiftTypeDetails.First())
                .FirstOrDefaultAsync();

            if (typeDetail == null)
            {
                typeDetail = await _employeeTimeInfoRepository.GetAll()
                    .Where(e => e.Employees.User.Id == AbpSession.UserId
                        && e.ShiftTypes.ShiftTypeDetails.Any(f => f.Days.Contains(formattedDoW)))
                    .Select(e => e.ShiftTypes.ShiftTypeDetails.First())
                    .FirstOrDefaultAsync();
            }

            return typeDetail;
        }

        public async Task<IList<SelectListItem>> GetShiftTypeList()
        {
            var types = await _shiftTypesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(o => new
                {
                    o.DisplayName,
                    Days = string.Join(string.Empty, o.ShiftTypeDetails.Select(f => f.Days).ToList()),
                    o.Id
                }).ToListAsync();

            var modifiedTypes = types
                .Where(e => availableDays.All(f => e.Days.Contains(f)))
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToList();

            modifiedTypes.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return modifiedTypes;
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftTypes)]
        public async Task<PagedResultDto<GetShiftTypesForViewDto>> GetAll(GetAllShiftTypesInput input)
        {
            var filteredTypes = _shiftTypesRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId);

            var totalCount = await filteredTypes.CountAsync();
            var types = await filteredTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input)
                .Select(e => new
                {
                    ShiftTypes = new ShiftTypesDto
                    {
                        DisplayName = e.DisplayName,
                        Description = e.Description,
                        Flexible1 = e.Flexible1,
                        GracePeriod1 = e.GracePeriod1,
                        MinimumOvertime = e.MinimumOvertime,
                        InUse = e.EmployeeTimeInfo.Any(),
                        Id = e.Id
                    },
                    Days = string.Join(string.Empty, e.ShiftTypeDetails.Select(f => f.Days).First())
                }).ToListAsync();

            types = types
                .Select(e => 
                {
                    e.ShiftTypes.RotateShiftWeekly = !availableDays.All(f => e.Days.Contains(f));
                    return e;
                }).ToList();

            var results = types
                .Select(e => new GetShiftTypesForViewDto
                {
                    ShiftTypes = e.ShiftTypes
                }).ToList();

            return new PagedResultDto<GetShiftTypesForViewDto>(totalCount, results);
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftTypes)]
        public async Task<GetShiftTypesForViewDto> GetShiftTypesForView(int id)
        {
            var type = await _shiftTypesRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new 
                {
                    ShiftTypes = new ShiftTypesDto
                    {
                        DisplayName = e.DisplayName,
                        Description = e.Description,
                        Flexible1 = e.Flexible1,
                        GracePeriod1 = e.GracePeriod1,
                        MinimumOvertime = e.MinimumOvertime,
                        Id = e.Id,
                        Details = e.ShiftTypeDetails 
                        .Select(f => new ShiftTypesDtoDetails
                        {
                            TimeIn = DateTime.MinValue.Add(TimeSpan.Parse(f.TimeIn)),
                            BreaktimeIn = DateTime.MinValue.Add(TimeSpan.Parse(f.BreaktimeIn)),
                            BreaktimeOut = DateTime.MinValue.Add(TimeSpan.Parse(f.BreaktimeOut)),
                            TimeOut = DateTime.MinValue.Add(TimeSpan.Parse(f.TimeOut))
                        }).ToList()
                    },
                    Days = string.Join(string.Empty, e.ShiftTypeDetails.Select(f => f.Days).ToList())
                }).FirstOrDefaultAsync();

            type.ShiftTypes.Status = availableDays.All(e => type.Days.Contains(e)) ? "Complete" : "Incomplete";
            type.ShiftTypes.RotateShiftWeekly = !availableDays.All(e => type.Days.Contains(e));

            return new GetShiftTypesForViewDto { ShiftTypes = type.ShiftTypes };
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftTypes_Edit)]
        public async Task<GetShiftTypesForEditOutput> GetShiftTypesForEdit(EntityDto input)
        {
            var type = await _shiftTypesRepository.GetAll()
                .Where(e => e.Id == input.Id)
                .Select(e => new
                {
                    Main = e,
                    Detail = e.ShiftTypeDetails.First()
                }).FirstOrDefaultAsync();

            var output = new GetShiftTypesForEditOutput { ShiftTypes = ObjectMapper.Map<CreateOrEditShiftTypesDto>(type.Main) };
            var typeDto = output.ShiftTypes;
            var typeDetail = type.Detail;

            typeDto.TimeIn = typeDetail.TimeIn;
            typeDto.BreaktimeIn = typeDetail.BreaktimeIn;
            typeDto.BreaktimeOut = typeDetail.BreaktimeOut;
            typeDto.TimeOut = typeDetail.TimeOut;
            typeDto.StartOnPreviousDay = typeDetail.StartOnPreviousDay;
            typeDto.Monday = typeDetail.Days.Contains("1");
            typeDto.Tuesday = typeDetail.Days.Contains("2");
            typeDto.Wednesday = typeDetail.Days.Contains("3");
            typeDto.Thursday = typeDetail.Days.Contains("4");
            typeDto.Friday = typeDetail.Days.Contains("5");
            typeDto.Saturday = typeDetail.Days.Contains("6");
            typeDto.Sunday = typeDetail.Days.Contains("7");

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditShiftTypesDto input)
        {
            await Validate(input);

            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftTypes_Create)]
        private async Task Create(CreateOrEditShiftTypesDto input)
        {
            var type = ObjectMapper.Map<ShiftTypes>(input);
            var timeIn = DateTime.MinValue.Add(TimeSpan.Parse(input.TimeIn));
            var timeOut = DateTime.MinValue.Add(TimeSpan.Parse(input.TimeOut));

            type.Description = $"{timeIn:hh:mm tt} - {timeOut:hh:mm tt}";

            var typeId = await _shiftTypesRepository.InsertAndGetIdAsync(type);
            var typeDetail = new ShiftTypeDetails
            {
                TenantId = type.TenantId,
                Days = GetDaysForShiftTypes(input),
                TimeIn = input.TimeIn,
                BreaktimeIn = input.BreaktimeIn,
                BreaktimeOut = input.BreaktimeOut,
                TimeOut = input.TimeOut,
                ShiftTypeId = typeId,
                StartOnPreviousDay = input.StartOnPreviousDay
            };

            await _shiftTypeDetailsRepository.InsertAsync(typeDetail);
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftTypes_Edit)]
        public async Task Edit(EditShiftTypesDto input)
        {
            var type = await _shiftTypesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, type);
        }

        private async Task Validate(CreateOrEditShiftTypesDto input)
        {
            var hasShiftType = await _shiftTypesRepository.GetAll()
                .AnyAsync(e => e.DisplayName == input.DisplayName && e.Id != input.Id);

            if (hasShiftType)
                throw new UserFriendlyException("Your request is not valid!", "An identical record already exists.");
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftTypes_Edit)]
        private async Task Update(CreateOrEditShiftTypesDto input)
        {
            var existInEmployees = await _employeeTimeInfoRepository.GetAll()
                .AnyAsync(e => e.ShiftTypeId == input.Id);

            var existInRequests = await _shiftRequestsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.ShiftTypeId == input.Id
                    && e.Status.DisplayName != "Cancelled" 
                    && e.Status.DisplayName != "Declined")
                .AnyAsync();

            if (existInEmployees && existInRequests)
                throw new UserFriendlyException("Your request is not valid!", "The shift type is already in use.");

            var type = await _shiftTypesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, type);

            var timeIn = DateTime.MinValue.Add(TimeSpan.Parse(input.TimeIn));
            var timeOut = DateTime.MinValue.Add(TimeSpan.Parse(input.TimeOut));
            type.Description = $"{timeIn:hh:mm tt} - {timeOut:hh:mm tt}";

            var detail = await _shiftTypeDetailsRepository.GetAll()
                .FirstOrDefaultAsync(e => e.ShiftTypeId == input.Id);

            detail.Days = GetDaysForShiftTypes(input);
            detail.TimeIn = input.TimeIn;
            detail.BreaktimeIn = input.BreaktimeIn;
            detail.BreaktimeOut = input.BreaktimeOut;
            detail.TimeOut = input.TimeOut;
            detail.StartOnPreviousDay = input.StartOnPreviousDay;
        }

        private static string GetDaysForShiftTypes(CreateOrEditShiftTypesDto input)
        {
            var days = string.Empty;

            if (input.Monday) days += "1";
            if (input.Tuesday) days += "2";
            if (input.Wednesday) days += "3";
            if (input.Thursday) days += "4";
            if (input.Friday) days += "5";
            if (input.Saturday) days += "6";
            if (input.Sunday) days += "7";

            return days;
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftTypes_Delete)]
        public async Task Delete(int id)
        {
            var hasRequest = await _shiftRequestsRepository.GetAll().AnyAsync(e => e.ShiftTypeId == id);

            if (hasRequest)
                throw new UserFriendlyException("Your request is not valid!", "The shift type is already in use.");

            var details = await _shiftTypeDetailsRepository.GetAll()
                .Where(e => e.ShiftTypeId == id)
                .Select(e => e.Id)
                .ToListAsync();
            
            foreach (var detail in details)
                await _shiftTypeDetailsRepository.DeleteAsync(detail);

            await _shiftTypesRepository.DeleteAsync(id);
        }
    }
}