﻿using Abp.Application.Services;
using Abp.Authorization;
using MMB.ePay.Timekeeping.Dtos;
using System;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    [RemoteService(false)]
    public class ApprovalsAppService : ePayAppServiceBase, IApprovalsAppService
    {
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;
        private readonly IFrequenciesAppService _frequenciesAppService;
        private readonly IHelperAppService _helperAppService;
        private readonly IStatusAppService _statusAppService;
        private readonly IEmailQueueAppService _emailQueueAppService;
        private readonly ITKSettingsAppService _tkSettingsAppService;

        public ApprovalsAppService(IAttendancePeriodsAppService attendancePeriodsAppService,
            IFrequenciesAppService frequenciesAppService,
            IHelperAppService helperAppService,
            IStatusAppService statusAppService,
            IEmailQueueAppService emailQueueAppService,
            ITKSettingsAppService tkSettingsAppService
            )
        {
            _attendancePeriodsAppService = attendancePeriodsAppService;
            _frequenciesAppService = frequenciesAppService;
            _helperAppService = helperAppService;
            _statusAppService = statusAppService;
            _emailQueueAppService = emailQueueAppService;
            _tkSettingsAppService = tkSettingsAppService;
        }

        public async Task<ApprovalsViewModel> GetApprovalFilters(bool type) =>
            new ApprovalsViewModel
            {
                ApproverTypeList = _helperAppService.GetApproverTypeList(),
                FrequencyList = await _frequenciesAppService.GetFrequencyList(),
                ApproverTypeId = type
            };

        public async Task<ApprovalsViewModel> GetAdminApprovalFilters() =>
            new ApprovalsViewModel
            {
                YearList = await _attendancePeriodsAppService.GetYearListForFilter(),
                FrequencyList = await _frequenciesAppService.GetFrequencyListForFilter(),
                ApproverTypeList = _helperAppService.GetApproverTypeList(),
                StatusTypeList = _helperAppService.GetStatusTypeList(),
                AttendancePeriodList = await _attendancePeriodsAppService
                    .GetAttendancePeriodListByYear(DateTime.Now.Year, null, null)
            };

        public async Task<RequestsViewModel> GetRequestsFilters()
        {
            var yearList = await _attendancePeriodsAppService.GetYearListForFilter();
            var selectedYear = int.Parse(yearList[0].Value);
            var attendancePeriodList = await _attendancePeriodsAppService
                    .GetAttendancePeriodListByYear(selectedYear, null, null);

            return new RequestsViewModel
            {
                YearList = yearList,
                StatusList = await _statusAppService.GetStatusListForFilter(),
                AttendancePeriodList = attendancePeriodList
            };
        }

        public async Task SendApprovalDetails(EmailRequestDto dto)
        {
            var emailEnabled = await _tkSettingsAppService.EnableEmailNotification();

            if (emailEnabled)
                await _emailQueueAppService.CreateOrEdit(new CreateOrEditEmailQueueDto
                {
                    TenantId = AbpSession.TenantId,
                    Subject = "Aion Approval Update",
                    Status = "P",
                    Recipient = dto.Recipient,
                    Request = dto.Request,
                    FullName = dto.FullName,
                    ApprovalStatus = dto.ApprovalStatus,
                    Remarks = dto.Remarks,
                    Reason = dto.Reason,
                    StartDate = dto.StartDate,
                    EndDate = dto.EndDate
                });
        }
    }
}