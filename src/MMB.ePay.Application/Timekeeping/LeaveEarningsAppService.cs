﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Storage;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class LeaveEarningsAppService : ePayAppServiceBase, ILeaveEarningsAppService
    {
        private readonly IRepository<LeaveEarnings> _leaveEarningsRepository;
        private readonly IRepository<LeaveEarningDetails> _leaveEarningDetailsRepository;
        private readonly IRepository<LeaveTypes> _leaveTypesRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<LeaveRequests> _leaveRequestsRepository;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IStatusAppService _statusManager;

        public LeaveEarningsAppService(IRepository<LeaveEarnings> leaveEarningsRepository,
            IRepository<LeaveEarningDetails> leaveEarningDetailsRepository,
            IRepository<LeaveTypes> leaveTypesRepository,
            IRepository<Employees> employeesRepository,
            IRepository<LeaveRequests> leaveRequestsRepository,
            ITempFileCacheManager tempFileCacheManager,
            IStatusAppService statusManager)
        {
            _leaveEarningsRepository = leaveEarningsRepository;
            _leaveEarningDetailsRepository = leaveEarningDetailsRepository;
            _leaveTypesRepository = leaveTypesRepository;
            _employeesRepository = employeesRepository;
            _leaveRequestsRepository = leaveRequestsRepository;
            _tempFileCacheManager = tempFileCacheManager;
            _statusManager = statusManager;
        }

        public async Task<IList<SelectListItem>> GetAppYearListForFilter()
        {
            var currentYear = (short)DateTime.Now.Year;
            var appYears = await _leaveEarningsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => e.AppYear)
                .Distinct()
                .ToListAsync();

            if (appYears.Count == 0)
                appYears.Add(currentYear);

            var output = appYears.OrderByDescending(e => e).Select(e => new SelectListItem
            {
                Value = e.ToString(),
                Text = e.ToString(),
                Selected = e == currentYear
            }).OrderByDescending(e => e.Value)
            .ToList();

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveEarnings)]
        public async Task<PagedResultDto<GetLeaveEarningsForViewDto>> GetAll(GetAllLeaveEarningsInput input)
        {
            var filteredLeaveEarnings = _leaveEarningsRepository.GetAll()
                .WhereIf(input.AppYearFilter > 0, e => e.AppYear == input.AppYearFilter)
                .WhereIf(input.AppMonthFilter > 0, e => e.AppMonth == input.AppMonthFilter)
                .Where(e => e.TenantId == AbpSession.TenantId);

            var totalCount = await filteredLeaveEarnings.CountAsync();
            var leaveEarnings = filteredLeaveEarnings
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input)
                .Select(o => new 
                {
                    o.Date,
                    o.AppYear,
                    o.AppMonth,
                    LeaveType = o.LeaveEarningDetails.First().LeaveTypes.DisplayName,
                    EmployeeCount = o.LeaveEarningDetails.Count,
                    TotalEarned = o.LeaveEarningDetails.Sum(f => f.Earned),
                    o.Remarks,
                    o.Id
                });

            var results = await leaveEarnings
                .Select(e => new GetLeaveEarningsForViewDto
                {
                    LeaveEarnings = new LeaveEarningsDto
                    {
                        Date = e.Date,
                        AppYear = e.AppYear,
                        AppMonth = e.AppMonth > 0 ? CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName((short)e.AppMonth) : string.Empty,
                        LeaveType = e.LeaveType,
                        EmployeeCount = e.EmployeeCount,
                        TotalEarned = e.TotalEarned,
                        Remarks = e.Remarks,
                        Id = e.Id
                    }
                }).ToListAsync();

            return new PagedResultDto<GetLeaveEarningsForViewDto>(totalCount, results);
        }

        public async Task<PagedResultDto<GetLeaveEarningDetailsForViewDto>> GetLeaveEarningDetails(GetAllLeaveEarningDetailsInput input)
        {
            var filteredDetails = _leaveEarningDetailsRepository.GetAll()
                .Where(e => e.LeaveEarningId == input.Id);

            var totalCount = await filteredDetails.CountAsync();
            var results = await filteredDetails
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input)
                .Select(e => new GetLeaveEarningDetailsForViewDto
                {
                    LeaveEarningDetails = new LeaveEarningDetailsDto
                    {
                        Employee = e.Employees.FullName,
                        Earned = e.Earned
                    }
                }).ToListAsync();

            return new PagedResultDto<GetLeaveEarningDetailsForViewDto>(totalCount, results);
        }

        public async Task<PagedResultDto<GetLeaveBalancesForViewDto>> GetLeaveBalances(GetAllLeaveBalancesInput input)
        {
            var filteredTypes = _leaveTypesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId);

            var status = await _statusManager
                .GetStatusByDisplayNames(StatusDisplayNames.Cancelled, StatusDisplayNames.Declined);

            var declinedIds = status.Select(e => e.Status.Id).ToArray();

            var leaveBalances = await filteredTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input)
                .Select(e => new LeaveBalancesDto
                {
                    LeaveType = e.DisplayName,
                    Earned = e.LeaveEarningDetails
                        .Where(f => f.LeaveEarnings.AppYear == input.AppYearFilter 
                            && f.Employees.User.Id == AbpSession.UserId)
                        .Sum(f => f.Earned),
                    Used = e.LeaveRequests
                        .Where(f => f.LeaveRequestDetails.Any(g => g.LeaveDate.Year == input.AppYearFilter) 
                            && f.Employees.User.Id == AbpSession.UserId && !declinedIds.Contains(f.StatusId))
                        .SelectMany(f => f.LeaveRequestDetails.Select(g => g.Days)).Sum(),
                    Id = e.Id
                }).ToListAsync();

            leaveBalances = leaveBalances
                .Select(e => { e.Balance = e.Earned - e.Used; return e; })
                .ToList();

            var totalCount = leaveBalances.Count;

            var results = leaveBalances.Select(e => new GetLeaveBalancesForViewDto
            {
                LeaveBalances = e
            }).ToList();

            return new PagedResultDto<GetLeaveBalancesForViewDto>(totalCount, results);
        }

        public async Task<PagedResultDto<GetAdminLeaveBalancesForViewDto>> GetAdminLeaveBalances(GetAdminLeaveBalancesInput input)
        {
            var filteredTypes = _leaveTypesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId);

            var pagedAndFilteredTypes = filteredTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var declinedIds = (await _statusManager
                .GetStatusByDisplayNames(StatusDisplayNames.Cancelled, StatusDisplayNames.Declined))
                .Select(e => e.Status.Id)
                .ToArray();

            var balances = new List<AdminLeaveBalancesDto>();

            if (input.EmployeeIdFilter == null)
            {
                balances = await pagedAndFilteredTypes
                    .Select(e => new AdminLeaveBalancesDto
                    {
                        LeaveType = e.DisplayName,
                        Earned = e.LeaveEarningDetails
                            .Where(f => f.LeaveEarnings.AppYear == input.AppYearFilter
                                && f.Employees.User.Id == AbpSession.UserId)
                            .Sum(f => f.Earned),
                        Used = e.LeaveRequests
                            .Where(f => f.LeaveRequestDetails.Any(g => g.LeaveDate.Year == input.AppYearFilter)
                                && f.Employees.User.Id == AbpSession.UserId && !declinedIds.Contains(f.StatusId))
                            .SelectMany(f => f.LeaveRequestDetails.Select(g => g.Days)).Sum(),
                        Id = e.Id
                    }).ToListAsync();
            }
            else
            {
                balances = await pagedAndFilteredTypes
                    .Select(e => new AdminLeaveBalancesDto
                    {
                        LeaveType = e.DisplayName,
                        Earned = e.LeaveEarningDetails
                            .Where(f => f.LeaveEarnings.AppYear == input.AppYearFilter
                                && f.EmployeeId == input.EmployeeIdFilter)
                            .Sum(f => f.Earned),
                        Used = e.LeaveRequests
                            .Where(f => f.LeaveRequestDetails.Any(g => g.LeaveDate.Year == input.AppYearFilter)
                                && f.EmployeeId == input.EmployeeIdFilter && !declinedIds.Contains(f.StatusId))
                            .SelectMany(f => f.LeaveRequestDetails.Select(g => g.Days)).Sum(),
                        Id = e.Id

                    }).ToListAsync();
            }

            var employee = await _employeesRepository.GetAll()
                .WhereIf(input.EmployeeIdFilter == null, e => e.User.Id == AbpSession.UserId)
                .WhereIf(input.EmployeeIdFilter != null, e => e.Id == input.EmployeeIdFilter)
                .Select(e => e.FullName)
                .FirstOrDefaultAsync();

            balances = balances
                .Select(e => { e.Balance = e.Earned - e.Used; e.Employee = employee; return e; })
                .ToList();

            var totalCount = balances.Count;

            var results = balances.Select(e => new GetAdminLeaveBalancesForViewDto
            {
                AdminLeaveBalances = e
            }).ToList();

            return new PagedResultDto<GetAdminLeaveBalancesForViewDto>(totalCount, results);
        }

        public async Task<GetLeaveEarningsForViewDto> GetLeaveEarningsForView(int id)
        {
            var leaveEarnings = await _leaveEarningsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new
                {
                    e.Date,
                    e.AppYear,
                    e.AppMonth,
                    e.Remarks,
                    LeaveType = e.LeaveEarningDetails.First().LeaveTypes.DisplayName,
                    e.Id
                }).FirstOrDefaultAsync();

            var output = new GetLeaveEarningsForViewDto
            {
                LeaveEarnings = new LeaveEarningsDto
                {
                    Date = leaveEarnings.Date,
                    AppYear = leaveEarnings.AppYear,
                    LeaveType = leaveEarnings.LeaveType,
                    AppMonth = leaveEarnings.AppMonth == null ? string.Empty :
                        CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName((int)leaveEarnings.AppMonth),
                    Remarks = leaveEarnings.Remarks,
                    Id = leaveEarnings.Id
                }
            };

            return output;
        }

        public async Task<GetLeaveBalancesForViewDto> GetLeaveBalancesForView(int id, int employeeId, int year)
        {
            var balances = new List<LeaveBalanceRequestsDto>();

            var earnings = await _leaveEarningDetailsRepository.GetAll()
                .WhereIf(employeeId > 0, e => e.EmployeeId == employeeId)
                .WhereIf(employeeId == 0, e => e.Employees.User.Id == AbpSession.UserId)
                .Where(e => e.LeaveEarnings.AppYear == year && e.LeaveTypeId == id)
                .Select(e => new LeaveBalanceRequestsDto
                {
                    Employee = e.Employees.FullName,
                    Date = e.LeaveEarnings.Date,
                    Earned = e.Earned,
                    Remarks = e.LeaveEarnings.Remarks
                }).ToListAsync();

            var declinedIds = (await _statusManager
                .GetStatusByDisplayNames(StatusDisplayNames.Cancelled, StatusDisplayNames.Declined))
                .Select(e => e.Status.Id)
                .ToArray();

            var requests = await _leaveRequestsRepository.GetAll()
                .WhereIf(employeeId > 0, e => e.EmployeeId == employeeId)
                .WhereIf(employeeId == 0, e => e.Employees.User.Id == AbpSession.UserId)
                .Where(e => e.LeaveRequestDetails.Any(f => f.LeaveDate.Year == year) && e.LeaveTypeId == id)
                .Where(e => !declinedIds.Contains(e.StatusId))
                .Select(e => new LeaveBalanceRequestsDto
                {
                    Employee = e.Employees.FullName,
                    Date = e.LeaveRequestDetails.First().LeaveDate,
                    Used = e.LeaveRequestDetails.Sum(f => f.Days),
                    Remarks = e.Remarks
                }).ToListAsync();

            balances.AddRange(earnings);
            balances.AddRange(requests);

            return new GetLeaveBalancesForViewDto
            {
                LeaveBalanceRequestList = balances.OrderBy(e => e.Date).ToList()
            };
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveEarnings_Edit)]
        public async Task<GetLeaveEarningsForEditOutput> GetLeaveEarningsForEdit(int id)
        {
            var earning = await _leaveEarningsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new
                {
                    e.LeaveEarningDetails.First().EmployeeId,
                    e.Date,
                    e.AppYear,
                    e.AppMonth,
                    e.Remarks,
                    EarningDetail = e.LeaveEarningDetails
                        .Select(f => new
                        {
                            f.EmployeeId,
                            f.Employees.FullName,
                            f.Employees.EmployeeCode,
                            f.Earned,
                            f.LeaveTypeId
                        }).First(),
                    DetailCount = e.LeaveEarningDetails.Count,
                    e.Id,
                }).FirstOrDefaultAsync();

            var output = new GetLeaveEarningsForEditOutput
            {
                LeaveEarnings = new CreateOrEditLeaveEarningsDto
                {
                    Date = earning.Date,
                    AppYear = earning.AppYear,
                    AppMonth = earning.AppMonth,
                    Remarks = earning.Remarks,
                    Earned = earning.EarningDetail.Earned,
                    LeaveTypeId = earning.EarningDetail.LeaveTypeId,
                    Employee = string.Format("{0} | {1}", earning.EarningDetail.EmployeeCode, earning.EarningDetail.FullName),
                    EmployeeId = earning.DetailCount == 1 ?
                        earning.EarningDetail.EmployeeId : null,
                    Id = earning.Id
                }
            };

            return output;
        }

        public async Task UpdateByFile(UploadLeaveEarningsDto input)
        {
            var currentRow = 0;

            try
            {
                var fileBytes = _tempFileCacheManager.GetFile(input.FileToken);

                if (fileBytes == null)
                {
                    throw new UserFriendlyException("Your request is not valid!", 
                        "There is no such file with the token: " + input.FileToken);
                }

                using var stream = new MemoryStream(fileBytes);
                var workbook = new XSSFWorkbook(stream);
                var worksheet = workbook.GetSheetAt(0);

                if (worksheet == null)
                {
                    throw new UserFriendlyException("Your request is not valid!",
                        "There is no such file with the token: " + input.FileToken);
                }

                var headerRow = worksheet.GetRow(0);
                var header = new string[]
                {
                    "Employee ID", "Earned", "Leave Type", "Date", "App Year", "App Month", "Remarks"
                };

                var firstRow = new string[]
                {
                    headerRow.GetCell(0).StringCellValue,
                    headerRow.GetCell(1).StringCellValue,
                    headerRow.GetCell(2).StringCellValue,
                    headerRow.GetCell(3).StringCellValue,
                    headerRow.GetCell(4).StringCellValue,
                    headerRow.GetCell(5).StringCellValue,
                    headerRow.GetCell(6).StringCellValue
                };

                if (!header.SequenceEqual(firstRow))
                {
                    throw new UserFriendlyException("Your request is not valid!", 
                        "File is not in the correct format.");
                }

                var rowCount = worksheet.LastRowNum;

                for (int row = 1; row <= rowCount; row++)
                {
                    currentRow = row;
                    var worksheetRow = worksheet.GetRow(row);

                    var file = new LeaveEarningsFile();

                    switch (worksheetRow.GetCell(0).CellType)
                    {
                        case CellType.Numeric:
                            file.EmployeeId = worksheetRow.GetCell(0).NumericCellValue.ToString();
                            break;
                        case CellType.String:
                            file.EmployeeId = worksheetRow.GetCell(0).StringCellValue;
                            break;
                    }

                    var employeeId = await _employeesRepository.GetAll()
                        .Where(e => e.TenantId == AbpSession.TenantId
                            && e.EmployeeCode == file.EmployeeId)
                        .Select(e => e.Id)
                        .FirstOrDefaultAsync();

                    if (employeeId == 0) continue;

                    switch (worksheetRow.GetCell(2).CellType)
                    {
                        case CellType.Numeric:
                            file.LeaveType = worksheetRow.GetCell(2).NumericCellValue.ToString();
                            break;
                        case CellType.String:
                            file.LeaveType = worksheetRow.GetCell(2).StringCellValue;
                            break;
                    }

                    switch (worksheetRow.GetCell(6).CellType)
                    {
                        case CellType.Numeric:
                            file.Remarks = worksheetRow.GetCell(6).NumericCellValue.ToString();
                            break;
                        case CellType.String:
                            file.Remarks = worksheetRow.GetCell(6).StringCellValue;
                            break;
                    }

                    switch (worksheetRow.GetCell(1).CellType)
                    {
                        case CellType.Numeric:
                            file.Earned = (decimal)worksheetRow.GetCell(1).NumericCellValue;
                            break;
                        case CellType.String:
                            file.Earned = decimal.Parse(worksheetRow.GetCell(1).StringCellValue);
                            break;
                    }

                    switch (worksheetRow.GetCell(3).CellType)
                    {
                        case CellType.Numeric:
                            file.Date = worksheetRow.GetCell(3).DateCellValue;
                            break;
                        case CellType.String:
                            file.Date = DateTime.Parse(worksheetRow.GetCell(3).StringCellValue);
                            break;
                    }

                    switch (worksheetRow.GetCell(4).CellType)
                    {
                        case CellType.Numeric:
                            file.AppYear = (short)worksheetRow.GetCell(4).NumericCellValue;
                            break;
                        case CellType.String:
                            file.AppYear = short.Parse(worksheetRow.GetCell(4).StringCellValue);
                            break;
                    }

                    switch (worksheetRow.GetCell(5).CellType)
                    {
                        case CellType.Numeric:
                            file.AppMonth = (short)worksheetRow.GetCell(5).NumericCellValue;
                            break;
                        case CellType.String:
                            file.AppMonth = short.Parse(worksheetRow.GetCell(5).StringCellValue);
                            break;
                    }

                    if (file.AppMonth > 12 || file.AppMonth < 1)
                    {
                        throw new UserFriendlyException("Your request is not valid!",
                            "Invalid App Month.");
                    }

                    var earning = new LeaveEarnings
                    {
                        TenantId = AbpSession.TenantId.Value,
                        Date = file.Date,
                        AppYear = file.AppYear,
                        AppMonth = Convert.ToInt16(file.AppMonth),
                        Remarks = file.Remarks
                    };

                    var leaveEarningId = await _leaveEarningsRepository.InsertAndGetIdAsync(earning);
                    var typeId = await _leaveTypesRepository.GetAll()
                        .Where(e => e.TenantId == AbpSession.TenantId && e.DisplayName == file.LeaveType)
                        .Select(e => e.Id)
                        .FirstOrDefaultAsync();

                    var details = new LeaveEarningDetails
                    {
                        TenantId = AbpSession.TenantId.Value,
                        Earned = file.Earned,
                        LeaveEarningId = leaveEarningId,
                        EmployeeId = employeeId,
                        LeaveTypeId = typeId
                    };

                    await _leaveEarningDetailsRepository.InsertAsync(details);
                }
            }
            catch (Exception ex)
            {
                if (currentRow > 0)
                    throw new UserFriendlyException("Row #" + currentRow + " is not valid!", ex.Message);

                throw new UserFriendlyException("Your request is not valid!", ex.Message);
            }
        }

        public async Task CreateOrEdit(CreateOrEditLeaveEarningsDto input)
        {
            if (input.Id == null)
            {
                if (input.EmployeeId == null)
                    await CreateMulti(input);
                else
                    await Create(input);
            }
            else
                await Update(input);
        }

        private async Task CreateMulti(CreateOrEditLeaveEarningsDto input)
        {
            var earning = ObjectMapper.Map<LeaveEarnings>(input);

            if (earning.AppMonth == 0)
                earning.AppMonth = null;

            var employees = await _employeesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId && e.EmploymentTypes.SysCode == "R")
                .Select(e => new { e.Id, e.DateRegularized })
                .ToListAsync();

            var oldEmployees = employees.Where(e => e.DateRegularized?.Year != DateTime.Now.Year).ToList();
            var newEmployees = employees.Where(e => e.DateRegularized?.Year == DateTime.Now.Year).ToList();

            if (oldEmployees.Count > 0)
            {
                var leaveEarningId = await _leaveEarningsRepository.InsertAndGetIdAsync(earning);

                foreach (var employee in oldEmployees)
                {
                    var leaveEarningDetail = new LeaveEarningDetails
                    {
                        LeaveEarningId = leaveEarningId,
                        Earned = (decimal)input.Earned,
                        EmployeeId = employee.Id,
                        LeaveTypeId = (int)input.LeaveTypeId
                    };

                    await _leaveEarningDetailsRepository.InsertAsync(leaveEarningDetail);
                }
            }

            foreach (var employee in newEmployees)
            {
                var monthRegularized = employee.DateRegularized.Value.Month;
                var currentMonth = DateTime.Now.Month;
                var monthRange = Enumerable.Range(currentMonth, monthRegularized - currentMonth + 1).ToList();

                if (earning.AppMonth == null)
                {
                    foreach (var month in monthRange)
                    {
                        await Create(new CreateOrEditLeaveEarningsDto
                        {
                            AppYear = (short)DateTime.Now.Year,
                            AppMonth = (short)month,
                            EmployeeId = employee.Id,
                            LeaveTypeId = input.LeaveTypeId,
                            Earned = (decimal)input.Earned,
                            Remarks = input.Remarks
                        });
                    }
                } 
                else if (monthRange.Contains((int)earning.AppMonth))
                {
                    await Create(new CreateOrEditLeaveEarningsDto
                    {
                        AppYear = (short)DateTime.Now.Year,
                        AppMonth = earning.AppMonth,
                        EmployeeId = employee.Id,
                        LeaveTypeId = input.LeaveTypeId,
                        Earned = (decimal)input.Earned,
                        Remarks = input.Remarks
                    });
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveEarnings_Create)]
        private async Task Create(CreateOrEditLeaveEarningsDto input)
        {
            var earning = ObjectMapper.Map<LeaveEarnings>(input);

            if (earning.AppMonth == 0)
                earning.AppMonth = null;

            var earningDetail = new LeaveEarningDetails
            {
                Earned = (decimal)input.Earned,
                EmployeeId = (int)input.EmployeeId,
                LeaveTypeId = (int)input.LeaveTypeId,
            };

            if (AbpSession.TenantId != null)
            {
                earning.TenantId = AbpSession.TenantId.Value;
                earningDetail.TenantId = AbpSession.TenantId.Value;
            }

            earningDetail.LeaveEarningId = await _leaveEarningsRepository.InsertAndGetIdAsync(earning);
            await _leaveEarningDetailsRepository.InsertAsync(earningDetail);
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveEarnings_Edit)]
        private async Task Update(CreateOrEditLeaveEarningsDto input)
        {
            var leaveEarnings = await _leaveEarningsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, leaveEarnings);

            var leaveEarningDetails = await _leaveEarningDetailsRepository.GetAll()
                .Where(e => e.LeaveEarningId == input.Id)
                .ToListAsync();

            leaveEarningDetails = leaveEarningDetails
                .Select(e =>
                {
                    e.Earned = (decimal)input.Earned;
                    e.LeaveTypeId = (int)input.LeaveTypeId;
                    return e;
                }).ToList();
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveEarnings_Delete)]
        public async Task Delete(int id) =>
            await _leaveEarningsRepository.DeleteAsync(id);
    }
}