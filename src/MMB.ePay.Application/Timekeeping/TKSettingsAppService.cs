﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Timekeeping.Dtos;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public class TKSettingsAppService : ePayAppServiceBase, ITKSettingsAppService
    {
        private readonly IRepository<TKSettings> _tkSettingsRepository;
        private readonly IRepository<GroupSettings> _groupSettingsRepository;

        public TKSettingsAppService(
            IRepository<TKSettings> tkSettingsRepository,
            IRepository<GroupSettings> groupSettingsRepository)
        {
            _tkSettingsRepository = tkSettingsRepository;
            _groupSettingsRepository = groupSettingsRepository;
        }

        public async Task<int> EmailDelay()
        {
            var value = await GetTkSettingValueByKey("EMAIL_DELAY");

            if (value == null)
                return 0;

            return int.Parse(value);
        }

        public async Task<string> Geolocation()
        {
            return await GetTkSettingValueByKey("GEOLOCATION");
        }

        public async Task<bool> EnableEmailNotification()
        {
            var value = await GetTkSettingValueByKey("ENABLE_EMAIL_NOTIFICATION");

            if (value == null)
                return false;

            return value.TrimEnd().ToLower() == "true";
        }

        public async Task<bool> MinuteFormat()
        {
            var value = await GetTkSettingValueByKey("MINUTE_FORMAT");

            if (value == null)
                return false;

            return value.TrimEnd().ToLower() == "true";
        }

        public async Task<bool> HasPMBreak()
        {
            var value = await GetTkSettingValueByKey("HAS_PM_BREAK");

            if (value == null)
                return false;

            return value.TrimEnd().ToLower() == "true";
        }

        private async Task<string> GetTkSettingValueByKey(string key) =>
            await _tkSettingsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId && e.Key == key)
                .Select(e => e.Value)
                .FirstOrDefaultAsync();

        [AbpAuthorize(AppPermissions.Pages_TKSettings_Edit)]
        public async Task<GetTKSettingsForEditOutput> GetTKSettingsForEdit() =>
            new GetTKSettingsForEditOutput
            {
                TKSettings = new TKSettingsDto
                {
                    SectionList = await _groupSettingsRepository.GetAll()
                        .Where(e => e.TenantId == AbpSession.TenantId)
                        .OrderBy(e => e.OrderNo)
                        .Select(e => new Tuple<string, string>(
                            e.Id.ToString(),
                            e.DisplayName
                        )).ToListAsync(),
                    SettingsList = await _tkSettingsRepository.GetAll()
                        .Where(e => e.TenantId == AbpSession.TenantId)
                        .OrderBy(e => e.DisplayOrder)
                        .Select(e => new Tuple<int, string, string, string, string>(
                            e.Id,
                            e.DataType,
                            e.Caption,
                            e.Value,
                            e.GroupSettings.Id.ToString()
                        )).ToListAsync()
                }
            };

        [AbpAuthorize(AppPermissions.Pages_TKSettings)]
        public async Task CreateOrEdit(CreateOrEditTKSettingsDto input)
        {
            var tkSettings = await _tkSettingsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .ToListAsync();

            for (var i = 0; i < input.IdList.Count; i++)
            {
                var tkSetting = tkSettings.Find(e => e.Id == input.IdList[i]);

                if (tkSetting != null)
                    tkSetting.Value = input.ValueList[i];
            }
        }
    }
}