﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping.Dtos;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class ShiftTypeDetailsAppService : ePayAppServiceBase, IShiftTypeDetailsAppService
    {
        private readonly IRepository<ShiftTypeDetails> _shiftTypeDetailsRepository;
        private readonly IRepository<ShiftTypes> _shiftTypesRepository;

        public ShiftTypeDetailsAppService(IRepository<ShiftTypeDetails> shiftTypeDetailsRepository,
            IRepository<ShiftTypes> shiftTypesRepository)
        {
            _shiftTypeDetailsRepository = shiftTypeDetailsRepository;
            _shiftTypesRepository = shiftTypesRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftTypeDetails)]
        public async Task<PagedResultDto<GetShiftTypeDetailsForViewDto>> GetAll(GetAllShiftTypeDetailsInput input)
        {
            var filteredTypeDetails = _shiftTypeDetailsRepository.GetAll()
                .Where(e => e.ShiftTypeId == input.ShiftTypeIdFilter);

            var totalCount = await filteredTypeDetails.CountAsync();
            var results = await filteredTypeDetails
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input)
                .Select(e => new GetShiftTypeDetailsForViewDto
                {
                    ShiftTypeDetails = new ShiftTypeDetailsDto
                    {
                        Days = e.Days,
                        TimeIn = e.TimeIn,
                        BreaktimeIn = e.BreaktimeIn,
                        BreaktimeOut = e.BreaktimeOut,
                        TimeOut = e.TimeOut,
                        ShiftTypeId = e.ShiftTypeId,
                        Id = e.Id
                    }
                }).ToListAsync();

            results = results
                .Select(e =>
                {
                    if (e.ShiftTypeDetails.Days != null)
                    {
                        e.ShiftTypeDetails.Days = string.Join(", ", e.ShiftTypeDetails.Days.ToCharArray()
                            .Select(f => (int)char.GetNumericValue(f))
                            .Select(f => Enum.GetName(typeof(DayOfWeek), f == 7 ? 0 : f).Substring(0, 3)));
                    }

                    return e;
                }).ToList();

            return new PagedResultDto<GetShiftTypeDetailsForViewDto> (totalCount, results);
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftTypeDetails)]
        public async Task<GetShiftTypeDetailsForViewDto> GetShiftTypeDetailsForView(int id)
        {
            var typeDetails = await _shiftTypeDetailsRepository.GetAsync(id);

            return new GetShiftTypeDetailsForViewDto 
            { 
                ShiftTypeDetails = ObjectMapper.Map<ShiftTypeDetailsDto>(typeDetails) 
            };
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftTypeDetails_Edit)]
        public async Task<GetShiftTypeDetailsForEditOutput> GetShiftTypeDetailsForEdit(EntityDto input)
        {
            var typeDetail = await _shiftTypeDetailsRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetShiftTypeDetailsForEditOutput 
            { 
                ShiftTypeDetails = ObjectMapper.Map<CreateOrEditShiftTypeDetailsDto>(typeDetail) 
            };

            var typeDetailDto = output.ShiftTypeDetails;
            var days = typeDetail.Days;

            typeDetailDto.Monday = days.Contains("1");
            typeDetailDto.Tuesday = days.Contains("2");
            typeDetailDto.Wednesday = days.Contains("3");
            typeDetailDto.Thursday = days.Contains("4");
            typeDetailDto.Friday = days.Contains("5");
            typeDetailDto.Saturday = days.Contains("6");
            typeDetailDto.Sunday = days.Contains("7");

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditShiftTypeDetailsDto input)
        {
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftTypeDetails_Create)]
        private async Task Create(CreateOrEditShiftTypeDetailsDto input)
        {
            var typeDetail = ObjectMapper.Map<ShiftTypeDetails>(input);
            typeDetail.Days = GetDaysForShiftTypes(input);

            await _shiftTypeDetailsRepository.InsertAsync(typeDetail);
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftTypeDetails_Edit)]
        private async Task Update(CreateOrEditShiftTypeDetailsDto input)
        {
            var typeDetail = await _shiftTypeDetailsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, typeDetail);

            typeDetail.Days = GetDaysForShiftTypes(input);
        }

        private static string GetDaysForShiftTypes(CreateOrEditShiftTypeDetailsDto input)
        {
            var days = string.Empty;

            if (input.Monday) days += "1";
            if (input.Tuesday) days += "2";
            if (input.Wednesday) days += "3";
            if (input.Thursday) days += "4";
            if (input.Friday) days += "5";
            if (input.Saturday) days += "6";
            if (input.Sunday) days += "7";

            return days;
        }

        [AbpAuthorize(AppPermissions.Pages_ShiftTypeDetails_Delete)]
        public async Task<bool> Delete(int id)
        {
            var typeDetail = await _shiftTypeDetailsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new { e.ShiftTypeId, 
                    HasMoreDetails = e.ShiftTypes.ShiftTypeDetails.Any(f => f.Id != id) })
                .FirstOrDefaultAsync();

            await _shiftTypeDetailsRepository.DeleteAsync(id);

            if (!typeDetail.HasMoreDetails)
                await _shiftTypesRepository.DeleteAsync(typeDetail.ShiftTypeId);

            return typeDetail.HasMoreDetails;
        }
    }
}