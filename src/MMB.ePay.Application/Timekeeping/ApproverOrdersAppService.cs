﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class ApproverOrdersAppService : ePayAppServiceBase, IApproverOrdersAppService
    {
        private readonly IRepository<ApproverOrders> _approverOrdersRepository;

        public ApproverOrdersAppService(IRepository<ApproverOrders> approverOrdersRepository) =>
            _approverOrdersRepository = approverOrdersRepository;
        
        public async Task<IList<string>> GetApproverNameList() =>
            await _approverOrdersRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .OrderBy(e => e.Level)
                .ThenBy(e => e.IsBackup)
                .Select(e => e.DisplayName)
                .ToListAsync();

        public async Task<IList<SelectListItem>> GetApproverOrderList() =>
            await _approverOrdersRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .OrderBy(e => e.Level)
                .ThenBy(e => e.IsBackup)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

        [AbpAuthorize(AppPermissions.Pages_ApproverOrders)]
        public async Task<PagedResultDto<GetApproverOrdersForViewDto>> GetAll(GetAllApproverOrdersInput input)
        {
            var filteredApproverOrders = _approverOrdersRepository.GetAll();

            var pagedAndFilteredApproverOrders = filteredApproverOrders
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var results = await pagedAndFilteredApproverOrders
                .Select(o => new GetApproverOrdersForViewDto
                {
                    ApproverOrders = new ApproverOrdersDto
                    {
                        DisplayName = o.DisplayName,
                        Level = o.Level,
                        IsBackup = o.IsBackup,
                        Id = o.Id,
                    }
                }).ToListAsync();

            var totalCount = await filteredApproverOrders.CountAsync();

            return new PagedResultDto<GetApproverOrdersForViewDto>(
                totalCount,
                results
            );
        }

        public async Task<GetApproverOrdersForViewDto> GetApproverOrdersForView(int id)
        {
            var approverOrders = await _approverOrdersRepository.GetAsync(id);
            var output = new GetApproverOrdersForViewDto { ApproverOrders = ObjectMapper.Map<ApproverOrdersDto>(approverOrders) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_ApproverOrders_Edit)]
        public async Task<GetApproverOrdersForEditOutput> GetApproverOrdersForEdit(int id)
        {
            var approverOrders = await _approverOrdersRepository.FirstOrDefaultAsync(id);
            var output = new GetApproverOrdersForEditOutput { ApproverOrders = ObjectMapper.Map<CreateOrEditApproverOrdersDto>(approverOrders) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditApproverOrdersDto input)
        {
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        [AbpAuthorize(AppPermissions.Pages_ApproverOrders_Create)]
        protected virtual async Task Create(CreateOrEditApproverOrdersDto input)
        {
            var approverOrders = ObjectMapper.Map<ApproverOrders>(input);
            await _approverOrdersRepository.InsertAsync(approverOrders);
        }

        [AbpAuthorize(AppPermissions.Pages_ApproverOrders_Edit)]
        protected virtual async Task Update(CreateOrEditApproverOrdersDto input)
        {
            var approverOrders = await _approverOrdersRepository.FirstOrDefaultAsync(input.Id.Value);
            ObjectMapper.Map(input, approverOrders);
        }

        [AbpAuthorize(AppPermissions.Pages_ApproverOrders_Delete)]
        public async Task Delete(EntityDto input) =>
            await _approverOrdersRepository.DeleteAsync(input.Id);
    }
}