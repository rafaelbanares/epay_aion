﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class HolidayTypesAppService : ePayAppServiceBase, IHolidayTypesAppService
    {
        private readonly IRepository<HolidayTypes> _holidayTypesRepository;

        public HolidayTypesAppService(IRepository<HolidayTypes> holidayTypesRepository) =>
            _holidayTypesRepository = holidayTypesRepository;

        public async Task<IList<SelectListItem>> GetHolidayTypeList()
        {
            var holidayTypeList = await _holidayTypesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            holidayTypeList.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return holidayTypeList;
        }

        [AbpAuthorize(AppPermissions.Pages_HolidayTypes)]
        public async Task<PagedResultDto<GetHolidayTypesForViewDto>> GetAll(GetAllHolidayTypesInput input)
        {
            var filteredHolidayTypes = _holidayTypesRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId);

            var pagedAndFilteredHolidayTypes = filteredHolidayTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var holidayTypes = pagedAndFilteredHolidayTypes
                .Select(o => new GetHolidayTypesForViewDto
                {
                    HolidayTypes = new HolidayTypesDto
                    {
                        DisplayName = o.DisplayName,
                        Description = o.Description,
                        HalfDayEnabled = o.HalfDayEnabled,
                        IsEditable = !o.Holidays.Any(),
                        Id = o.Id,
                    }
                });

            var totalCount = await filteredHolidayTypes.CountAsync();
            var results = await holidayTypes.ToListAsync();

            return new PagedResultDto<GetHolidayTypesForViewDto>(
                totalCount,
                results
            );
        }

        [AbpAuthorize(AppPermissions.Pages_HolidayTypes)]
        public async Task<GetHolidayTypesForViewDto> GetHolidayTypesForView(int id)
        {
            var holidayTypes = await _holidayTypesRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetHolidayTypesForViewDto
                {
                    HolidayTypes = new HolidayTypesDto
                    {
                        DisplayName = e.DisplayName,
                        Description = e.Description,
                        HolidayCode = e.HolidayCode,
                        HalfDayEnabled = e.HalfDayEnabled
                    }
                }).FirstOrDefaultAsync();

            return holidayTypes;
        }

        [AbpAuthorize(AppPermissions.Pages_HolidayTypes_Edit)]
        public async Task<GetHolidayTypesForEditOutput> GetHolidayTypesForEdit(EntityDto input)
        {
            var holidayTypes = await _holidayTypesRepository.FirstOrDefaultAsync(input.Id);

            return new GetHolidayTypesForEditOutput 
            { 
                HolidayTypes = ObjectMapper.Map<CreateOrEditHolidayTypesDto>(holidayTypes) 
            };
        }

        public async Task<bool> CheckHalfDayEnabled(int id) =>
            await _holidayTypesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId && e.Id == id)
                .Select(e => e.HalfDayEnabled)
                .FirstOrDefaultAsync();

        public async Task CreateOrEdit(CreateOrEditHolidayTypesDto input)
        {
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        protected virtual async Task Validate(CreateOrEditHolidayTypesDto input)
        {
            if (await _holidayTypesRepository.GetAll()
                .AnyAsync(e => e.TenantId == AbpSession.TenantId && e.DisplayName == input.DisplayName && e.Id != input.Id))
                throw new UserFriendlyException("Your request is not valid!", "An identical record already exists.");

            if (await _holidayTypesRepository.GetAll()
                .AnyAsync(e => e.TenantId == AbpSession.TenantId && e.HolidayCode == input.HolidayCode && e.Id != input.Id))
                throw new UserFriendlyException("Your request is not valid!", "An identical record already exists.");
        }

        [AbpAuthorize(AppPermissions.Pages_HolidayTypes_Create)]
        protected virtual async Task Create(CreateOrEditHolidayTypesDto input)
        {
            var holidayTypes = ObjectMapper.Map<HolidayTypes>(input);

            await Validate(input);
            await _holidayTypesRepository.InsertAsync(holidayTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_HolidayTypes_Edit)]
        protected virtual async Task Update(CreateOrEditHolidayTypesDto input)
        {
            var holidayTypes = await _holidayTypesRepository.FirstOrDefaultAsync((int)input.Id);

            await Validate(input);
            ObjectMapper.Map(input, holidayTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_HolidayTypes_Delete)]
        public async Task Delete(EntityDto input) =>
            await _holidayTypesRepository.DeleteAsync(input.Id);
    }
}