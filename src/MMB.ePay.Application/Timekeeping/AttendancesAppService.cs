﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.Linq.Extensions;
using Abp.UI;
using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.HR;
using MMB.ePay.MultiTenancy;
using MMB.ePay.Timekeeping.Dtos;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize(AppPermissions.Pages_Attendances)]
    public class AttendancesAppService : ePayAppServiceBase, IAttendancesAppService
    {
        private readonly TenantManager _tenantManager;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<Attendances> _attendancesRepository;
        private readonly IRepository<AttendanceUploads> _attendanceUploadsRepository;
        private readonly IRepository<ProcessedTimesheets> _processedTimesheetsRepository;
        private readonly IRepository<ProcessedOvertimes> _processedOvertimesRepository;
        private readonly IRepository<PostedAttendances> _postedAttendancesRepository;
        private readonly IRepository<PostedTimesheets> _postedTimesheetsRepository;
        private readonly IRepository<PostedOvertimes> _postedOvertimesRepository;
        private readonly ITKSettingsAppService _tKSettingsAppService;
        private readonly IAttendancePeriodsAppService _attendancePeriodsManager;
        private readonly IStoredProceduresAppService _storedProcedureManager;
        private readonly IHelperAppService _helperAppService;
        private readonly IDbContextProvider<ePayDbContext> _dbContextProvider;

        public AttendancesAppService(
            TenantManager tenantManager,
            IRepository<Employees> employeesRepository,
            IRepository<Attendances> attendancesRepository,
            IRepository<AttendanceUploads> attendanceUploadsRepository,
            IRepository<PostedAttendances> postedAttendancesRepository,
            IRepository<ProcessedTimesheets> processedTimesheetsRepository,
            IRepository<PostedTimesheets> postedTimesheetsRepository,
            IRepository<ProcessedOvertimes> processedOvertimesRepository,
            IRepository<PostedOvertimes> postedOvertimesRepository,
            ITKSettingsAppService tKSettingsAppService,
            IAttendancePeriodsAppService attendancePeriodsManager,
            IStoredProceduresAppService storedProcedureManager,
            IHelperAppService helperAppService,
            IDbContextProvider<ePayDbContext> dbContextProvider)
        {
            _tenantManager = tenantManager;

            _employeesRepository = employeesRepository;
            _attendancesRepository = attendancesRepository;
            _attendanceUploadsRepository = attendanceUploadsRepository;
            _processedTimesheetsRepository = processedTimesheetsRepository;
            _processedOvertimesRepository = processedOvertimesRepository;

            _postedAttendancesRepository = postedAttendancesRepository;
            _postedTimesheetsRepository = postedTimesheetsRepository;
            _postedOvertimesRepository = postedOvertimesRepository;

            _tKSettingsAppService = tKSettingsAppService;
            _attendancePeriodsManager = attendancePeriodsManager;
            _storedProcedureManager = storedProcedureManager;
            _helperAppService = helperAppService;
            _dbContextProvider = dbContextProvider;
        }

        public async Task<IList<InvalidAttendancesDto>> GetInvalidAttendanceList(int periodId)
        {
            var attendancePeriod = await _attendancePeriodsManager.GetDateRangeFromAttendancePeriod(periodId);

            var output = new List<InvalidAttendancesDto>();

            var invalidAttendances = await _attendancesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId && e.Invalid
                    && e.Date >= attendancePeriod.Start && e.Date <= attendancePeriod.End)
                .Select(e => new
                {
                    Employee = e.Employees.FullName,
                    e.Date,
                    e.TimeIn,
                    e.BreaktimeIn,
                    e.BreaktimeOut,
                    e.TimeOut
                }).ToListAsync();

            if (invalidAttendances.Count > 0)
            {
                output = invalidAttendances
                    .Select(e => new InvalidAttendancesDto
                    {
                        Employee = e.Employee,
                        Date = e.Date.ToString("MM/dd/yyyy"),
                        TimeIn = e.TimeIn?.ToString("hh:mm tt"),
                        BreaktimeIn = e.BreaktimeIn?.ToString("hh:mm tt"),
                        BreaktimeOut = e.BreaktimeOut?.ToString("hh:mm tt"),
                        TimeOut = e.TimeOut?.ToString("hh:mm tt")
                    }).OrderBy(e => e.Employee)
                    .ThenBy(e => e.Date)
                    .ToList();
            }

            return output;
        }

        //public IList<SelectListItem> GetBiometricList()
        //{

        //    return new List<SelectListItem>
        //    {
        //        new SelectListItem { Value = "MMB", Text = "MMB" },
        //        //new SelectListItem { Value = "Face 200SX", Text = "Face 200SX" },
        //        new SelectListItem { Value = "Face 400", Text = "Face 400" },
        //        new SelectListItem { Value = "Jason", Text = "Jason" },
        //        new SelectListItem { Value = "Taisho", Text = "Taisho" }
        //    };
        //}

        [AbpAuthorize(AppPermissions.Pages_Attendances_Upload)]
        public async Task<AttendancesUploadDto> UploadStaging(AttendancesUploadDto input)
        {
            var tenant = await _tenantManager.GetByIdAsync(AbpSession.TenantId.Value);

            return tenant.TenancyName switch
            {
                "MMB" => await UploadStagingDefault(input),
                //"Face 200SX" => await UploadFace200SX(input),
                "Tobys" => await UploadTobys(input),
                "Jason" => await UploadJason(input),
                "Taisho" => await UploadTaisho(input),
                "Benreys" => await UploadBenreys(input),
                "RTCC" => await UploadReadycon(input),
                _ => await UploadStagingDefault(input),
            };

            //DONT DELETE
            //var tenancyName = await _tenantRepository.GetAll()
            //    .Where(e => e.Id == AbpSession.TenantId)
            //    .Select(e => e.TenancyName)
            //    .FirstOrDefaultAsync();

            //return tenancyName switch
            //{
            //    "Default" => await UploadStagingTobys(input), //change this back
            //    "Tobys" => await UploadStagingTobys(input),
            //    _ => await UploadStagingDefault(input),
            //};
        }

        protected bool PreValidate(AttendancesUploadDto input, string fileType)
        {
            input.CustomError = string.Empty;

            if (fileType != input.FileType)
            {
                input.CustomError = L("Warn_FileType", fileType);
            }

            return input.CustomError == string.Empty;
        }

        protected async Task<bool> PostValidate(AttendancesUploadDto input)
        {
            var hasPeriod = await _attendancePeriodsManager
                .CheckUnpostedPeriodByStartDate(input.StartDate.Value);

            if (!hasPeriod)
                input.CustomError = "Invalid Upload. The attendance period has already been posted or doesn't exist.";

            return input.CustomError == string.Empty;
        }

        [UnitOfWork]
        public async Task UploadAttendanceUploads(CreateAttendanceUploadsDto input)
        {
            var employeeCodes = input.AttendanceUploads
                .Select(e => e.EmployeeCode)
                .Distinct()
                .ToList();

            var employees = await _employeesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId && employeeCodes.Contains(e.EmployeeCode))
                .Select(e => new
                {
                    e.Id,
                    e.EmployeeCode
                }).ToListAsync();

            var attendanceUploads = input.AttendanceUploads.Select(e => new AttendanceUploads
            {
                TenantId = AbpSession.TenantId.Value,
                EmployeeId = employees.Where(f => f.EmployeeCode == f.EmployeeCode).Select(f => f.Id).FirstOrDefault(),
                Date = e.Date,
                TimeIn = e.TimeIn,
                LunchBreaktimeIn = e.LunchBreaktimeIn,
                LunchBreaktimeOut = e.LunchBreaktimeOut,
                PMBreaktimeIn = e.PMBreaktimeIn,
                PMBreaktimeOut = e.PMBreaktimeOut,
                TimeOut = e.TimeOut
            }).Where(e => e.EmployeeId != 0)
            .ToList();

            if (attendanceUploads.Count > 0)
            {
                CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
                var dbContext = _dbContextProvider.GetDbContext();
                await dbContext.AttendanceUploads.AddRangeAsync(attendanceUploads);
                await UnitOfWorkManager.Current.SaveChangesAsync();
            }
        }

        [UnitOfWork]
        protected async Task<int> UploadAttendanceUploads(List<AttendanceUploads> attendanceUploads)
        {
            var maxDate = attendanceUploads.Max(e => e.Date);
            var minDate = attendanceUploads.Min(e => e.Date);

            var existingKeys = await _attendanceUploadsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId &&
                    e.Date >= minDate && e.Date <= maxDate)
                .Select (e => new { e.EmployeeId, e.Date })
                .ToListAsync();

            var toAdd = attendanceUploads
                .Where(e => !existingKeys.Contains(new { e.EmployeeId, e.Date }))
                .ToList();

            if (toAdd.Count > 0)
            {
                CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
                var dbContext = _dbContextProvider.GetDbContext();
                await dbContext.AttendanceUploads.AddRangeAsync(toAdd);
                await UnitOfWorkManager.Current.SaveChangesAsync();
            }

            var finalCount = toAdd.Count;

            var toUpdate = attendanceUploads
                .Where(e => existingKeys.Contains(new { e.EmployeeId, e.Date }))
                .ToList();

            foreach (var update in toUpdate)
            {
                var upload = await _attendanceUploadsRepository.GetAll()
                    .Where(e => e.TenantId == AbpSession.TenantId && 
                        e.EmployeeId == update.EmployeeId && e.Date == update.Date)
                    .FirstAsync();

                if (upload != null)
                {
                    var fieldsToUpdate = new Dictionary<string, string>
                    {
                        { nameof(upload.TimeIn), update.TimeIn },
                        { nameof(upload.TimeOut), update.TimeOut },
                        { nameof(upload.LunchBreaktimeIn), update.LunchBreaktimeIn },
                        { nameof(upload.LunchBreaktimeOut), update.LunchBreaktimeOut },
                        { nameof(upload.PMBreaktimeIn), update.PMBreaktimeIn },
                        { nameof(upload.PMBreaktimeOut), update.PMBreaktimeOut }
                    };

                    var hasUpdated = false;

                    foreach (var field in fieldsToUpdate)
                    {
                        var uploadProperty = upload.GetType().GetProperty(field.Key);
                        var uploadValue = uploadProperty.GetValue(upload) as string;

                        if (string.IsNullOrWhiteSpace(uploadValue) && !string.IsNullOrWhiteSpace(field.Value))
                        {
                            uploadProperty.SetValue(upload, field.Value);
                            hasUpdated = true;
                        }
                    }

                    if (hasUpdated)
                    {
                        finalCount++;
                        await _attendanceUploadsRepository.UpdateAsync(upload);
                    }
                }

            }


            return finalCount;
        }

        [UnitOfWork]
        public async Task UploadRawTimeLogStaging(CreateRawTimeLogStagingDto input)
        {
            var rawTimeLogs = input.RawTimeLogStaging.Select(e => new RawTimeLogStaging
            {
                TenantId = AbpSession.TenantId.Value,
                SwipeCode = e.SwipeCode,
                LogTime = e.LogTime,
                InOut = e.InOut,
                UploadSessionId = input.UploadSessionId,
                WorkFromHome = false,
                Device = "B"
            }).ToList();

            if (rawTimeLogs.Count > 0)
            {
                CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
                var dbContext = _dbContextProvider.GetDbContext();
                await dbContext.RawTimeLogStaging.AddRangeAsync(rawTimeLogs);
                await UnitOfWorkManager.Current.SaveChangesAsync();

                await _storedProcedureManager.ProcessRawTimeLogStaging(input.UploadSessionId);
                await UnitOfWorkManager.Current.SaveChangesAsync();
            }
        }

        [UnitOfWork]
        protected async Task<int> UploadRawTimeLogs(List<RawTimeLogStaging> rawTimeLogs)
        {
            if (rawTimeLogs.Count > 0)
            {
                CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
                var dbContext = _dbContextProvider.GetDbContext();
                await dbContext.RawTimeLogStaging.AddRangeAsync(rawTimeLogs);
                await UnitOfWorkManager.Current.SaveChangesAsync();

                var uploadSessionId = rawTimeLogs.First().UploadSessionId.Value;
                await _storedProcedureManager.ProcessRawTimeLogStaging(uploadSessionId);
                await UnitOfWorkManager.Current.SaveChangesAsync();
            }

            return rawTimeLogs.Count;
        }

        protected async Task<AttendancesUploadDto> UploadStagingDefault(AttendancesUploadDto input)
        {
            if (!PreValidate(input, ".csv"))
                return input;

            try
            {
                using var stream = new StreamReader(input.File.OpenReadStream());
                var config = new CsvConfiguration(CultureInfo.CreateSpecificCulture("en-US"))
                {
                    Delimiter = "	",
                    HasHeaderRecord = true,
                    MissingFieldFound = null
                };

                var csv = new CsvReader(stream, config);

                //var records = csv.GetRecords<UploadFileMMB>()
                //    .Select(e =>
                //    {
                //        e.UID = e.UID.Replace("000000000000", string.Empty);
                //        return e;
                //    }).ToList();

                var records = csv.GetRecords<UploadFileMMB>().ToList();

                var newGuid = Guid.NewGuid();

                input.Count = records.Count;
                input.UploadSessionId = newGuid;

                var rawTimeLogs = records.Select(e => new RawTimeLogStaging
                {
                    UploadSessionId = newGuid,
                    TenantId = AbpSession.TenantId.Value,
                    SwipeCode = e.UID,
                    LogTime = e.DateTime,
                    Device = "B",
                    InOut = "I",
                    WorkFromHome = false
                }).ToList();

                input.StartDate = rawTimeLogs.Min(f => f.LogTime);
                input.EndDate = rawTimeLogs.Max(f => f.LogTime);

                if (!await PostValidate(input))
                    return input;

                input.FinalCount = await UploadRawTimeLogs(rawTimeLogs);
                return input;
            }
            catch (Exception)
            {
                //input.CustomError = ex.Message;
                input.CustomError = "File is not in the correct format.";
                return input;
                //throw new UserFriendlyException("Your request is not valid!", ex.Message);
            }
        }

        protected async Task<AttendancesUploadDto> UploadTobys(AttendancesUploadDto input)
        {
            input.CustomError = string.Empty;

            if (input.FileType == ".xls")
            {
                return await UploadTobysFormat1(input);
            } 
            else if (input.FileType == ".xlsx")
            {
                return await UploadTobysFormat2(input);
            }
            else
            {
                input.CustomError = L("Warn_FileType", ".xls/.xlsx");
                return input;
            }
        }

        private async Task<AttendancesUploadDto> UploadTobysFormat1(AttendancesUploadDto input)
        {
            try
            {
                var workbook = new HSSFWorkbook(input.File.OpenReadStream());
                if (workbook.NumberOfSheets == 0)
                {
                    throw new UserFriendlyException("Your request is not valid!", "File is empty.");
                }

                var worksheet = workbook.GetSheetAt(0);
                var headerRow = worksheet.GetRow(0);
                var header = new string[]
                {
                    "UserID", "EmployeeCode", "Name", "Dept.", "Att_Time"
                };

                var firstRow = new string[]
                {
                    headerRow.GetCell(0).StringCellValue,
                    headerRow.GetCell(1).StringCellValue,
                    headerRow.GetCell(2).StringCellValue,
                    headerRow.GetCell(3).StringCellValue,
                    headerRow.GetCell(4).StringCellValue
                };

                if (!header.SequenceEqual(firstRow))
                {
                    throw new UserFriendlyException("Your request is not valid!",
                        "File is not in the correct format.");
                }

                var rowCount = worksheet.LastRowNum;
                var newGuid = Guid.NewGuid();
                var records = new List<UploadFileFace400>();
                var rawTimeLogs = new List<RawTimeLogStaging>();

                for (int row = 1; row <= rowCount; row++)
                {
                    var worksheetRow = worksheet.GetRow(row);

                    var record = new UploadFileFace400
                    {
                        UserID = worksheetRow.GetCell(0).StringCellValue,
                        Att_Time = DateTime.Parse(worksheetRow.GetCell(4).StringCellValue) //change to exact
                    };

                    records.Add(record);
                }

                input.Count = records.Count;
                input.UploadSessionId = newGuid;

                var groupedRecords = records
                    .Where(e => e.Att_Time != DateTime.MinValue)
                    .GroupBy(e => e.UserID)
                    .Select(e => new
                    {
                        SwipeCode = e.Key,
                        Dates = e.Select(f => f.Att_Time).ToList()
                    }).ToList();

                foreach (var record in groupedRecords)
                {

                    var minDate = record.Dates.Min();
                    var maxDate = record.Dates.Max();

                    if (input.StartDate == null || input.EndDate == null)
                    {
                        input.StartDate = minDate;
                        input.EndDate = maxDate;
                    }

                    if (input.StartDate > minDate)
                        input.StartDate = minDate;

                    if (input.EndDate < maxDate)
                        input.EndDate = maxDate;

                    rawTimeLogs.AddRange(
                        record.Dates.Select(e => new RawTimeLogStaging
                        {
                            UploadSessionId = newGuid,
                            TenantId = AbpSession.TenantId.Value,
                            SwipeCode = record.SwipeCode,
                            LogTime = e,
                            Device = "B",
                            InOut = "I",
                            WorkFromHome = false
                        }));
                };

                if (!await PostValidate(input))
                    return input;

                input.FinalCount = await UploadRawTimeLogs(rawTimeLogs);
                return input;
            }
            catch (Exception)
            {
                input.CustomError = "File is not in the correct format.";
                return input;
            }
        }

        private async Task<AttendancesUploadDto> UploadTobysFormat2(AttendancesUploadDto input)
        {
            try
            {
                var workbook = new XSSFWorkbook(input.File.OpenReadStream());
                if (workbook.NumberOfSheets == 0)
                {
                    throw new UserFriendlyException("Your request is not valid!", "File is empty.");
                }

                var worksheet = workbook.GetSheetAt(0);
                var headerRow = worksheet.GetRow(0);
                var header = new string[]
                {
                    "USER ID", "EMPLOYEE CODE", "NAME", "DEPT",	"DATE",	"TIME IN", "LUNCH OUT", "LUNCH IN",
                    "DURATION", "BREAK OUT", "BREAK IN", "DURATION", "TIME OUT", "DURATION"
                };

                var firstRow = new string[]
                {
                    headerRow.GetCell(0).StringCellValue.Trim(),
                    headerRow.GetCell(1).StringCellValue.Trim(),
                    headerRow.GetCell(2).StringCellValue.Trim(),
                    headerRow.GetCell(3).StringCellValue.Trim(),
                    headerRow.GetCell(4).StringCellValue.Trim(),
                    headerRow.GetCell(5).StringCellValue.Trim(),
                    headerRow.GetCell(6).StringCellValue.Trim(),
                    headerRow.GetCell(7).StringCellValue.Trim(),
                    headerRow.GetCell(8).StringCellValue.Trim(),
                    headerRow.GetCell(9).StringCellValue.Trim(),
                    headerRow.GetCell(10).StringCellValue.Trim(),
                    headerRow.GetCell(11).StringCellValue.Trim(),
                    headerRow.GetCell(12).StringCellValue.Trim(),
                    headerRow.GetCell(13).StringCellValue.Trim()
                };

                if (!header.SequenceEqual(firstRow))
                {
                    throw new UserFriendlyException("Your request is not valid!",
                        "File is not in the correct format.");
                }

                var rowCount = worksheet.LastRowNum;
                var newGuid = Guid.NewGuid();
                var records = new List<UploadFileTobys2>();

                for (int row = 1; row <= rowCount; row++)
                {
                    var worksheetRow = worksheet.GetRow(row);

                    var record = new UploadFileTobys2
                    {
                        EmployeeCode = worksheetRow.GetCell(1).StringCellValue,
                        Date = DateTime.Parse(worksheetRow.GetCell(4).StringCellValue), //change to exact
                        TimeIn = worksheetRow.GetCell(5).StringCellValue,
                        LunchBreaktimeIn = worksheetRow.GetCell(6).StringCellValue,
                        LunchBreaktimeOut = worksheetRow.GetCell(7).StringCellValue,
                        PMBreaktimeIn = worksheetRow.GetCell(9).StringCellValue,
                        PMBreaktimeOut = worksheetRow.GetCell(10).StringCellValue,
                        TimeOut = worksheetRow.GetCell(12).StringCellValue
                    };

                    records.Add(record);
                }

                input.Count = records.Count;
                input.UploadSessionId = newGuid;

                var employeeCodes = records.Select(x => x.EmployeeCode).Distinct().ToList();
                var employeeIds = await _employeesRepository.GetAll()
                    .Where(e => e.TenantId == AbpSession.TenantId
                        && employeeCodes.Any(f => f == e.EmployeeCode))
                    .Select(e => new
                    {
                        e.EmployeeCode,
                        e.Id
                    })
                    .ToListAsync();

                var attendanceUploads = records
                    .Select(e => new AttendanceUploads
                    {
                        TenantId = AbpSession.TenantId.Value,
                        EmployeeId = employeeIds
                            .Where(f => f.EmployeeCode == e.EmployeeCode)
                            .Select(f => f.Id)
                            .FirstOrDefault(),
                        Date = e.Date,
                        TimeIn = e.TimeIn,
                        LunchBreaktimeIn = e.LunchBreaktimeIn,
                        LunchBreaktimeOut = e.LunchBreaktimeOut,
                        PMBreaktimeIn = e.PMBreaktimeIn,
                        PMBreaktimeOut = e.PMBreaktimeOut, 
                        TimeOut = e.TimeOut
                    })
                    .Where(e => e.EmployeeId != 0)
                    .GroupBy(e => new { e.EmployeeId, e.Date })
                    .Select(g => g.First())
                    .ToList();

                //if (!await PostValidate(input))
                //    return input;

                input.FinalCount = await UploadAttendanceUploads(attendanceUploads);
                return input;
            }
            catch (Exception)
            {
                input.CustomError = "File is not in the correct format.";
                return input;
            }
        }

        protected async Task<AttendancesUploadDto> UploadReadycon(AttendancesUploadDto input)
        {
            if (!PreValidate(input, ".txt"))
                return input;

            var rawTimeLogs = new List<RawTimeLogStaging>();
            var newGuid = Guid.NewGuid();
            input.UploadSessionId = newGuid;

            try
            {
                using var stream = new StreamReader(input.File.OpenReadStream());

                var header = await stream.ReadLineAsync();
                if (string.IsNullOrWhiteSpace(header) || header.Length != 175)
                {
                    input.CustomError = "File is not in the correct format.";
                    return input;
                }

                int count = 0;
                while (stream.Peek() >= 0)
                {
                    var record = await stream.ReadLineAsync();
                    if (string.IsNullOrWhiteSpace(record) || record.Length != 175)
                        continue;

                    count++;

                    rawTimeLogs.Add(new RawTimeLogStaging
                    {
                        UploadSessionId = newGuid,
                        TenantId = AbpSession.TenantId.Value,
                        SwipeCode = record.Substring(56, 21).Trim(),
                        LogTime = DateTime.ParseExact(record.Substring(77, 22).Trim(), "M/d/yyyy h:mm:ss tt", CultureInfo.InvariantCulture),
                        InOut = record.Substring(99, 11).Trim() == "C/In" ? "I" : "O",
                        Device = "B"
                    });
                }

                if (!rawTimeLogs.Any())
                {
                    input.CustomError = "No valid records found.";
                    return input;
                }

                input.Count = count;
                input.StartDate = rawTimeLogs.Min(f => f.LogTime);
                input.EndDate = rawTimeLogs.Max(f => f.LogTime);

                if (!await PostValidate(input))
                    return input;

                input.FinalCount = await UploadRawTimeLogs(rawTimeLogs);
                return input;
            }
            catch (Exception)
            {
                input.CustomError = "File is not in the correct format.";
                return input;
            }
        }

        protected async Task<AttendancesUploadDto> UploadJason(AttendancesUploadDto input)
        {
            if (!PreValidate(input, ".txt"))
                return input;

            var rawTimeLogs = new List<RawTimeLogStaging>();
            var newGuid = Guid.NewGuid();
            var count = 0;

            input.UploadSessionId = newGuid;

            try
            {
                using var stream = new StreamReader(input.File.OpenReadStream());
                while (stream.Peek() >= 0)
                {
                    count++;

                    var record = await stream.ReadLineAsync();

                    if (record.Length > 42)
                    {
                        var swipeCode = record.Substring(0, 21);
                        var logTime = record.Substring(21, 19);
                        var inOut = record.Substring(41, 1);

                        rawTimeLogs.Add(new RawTimeLogStaging
                        {
                            UploadSessionId = newGuid,
                            TenantId = AbpSession.TenantId.Value,
                            SwipeCode = swipeCode.TrimEnd(),
                            LogTime = DateTime.ParseExact(logTime, "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture),
                            InOut = inOut == "1" ? "I" : "O",
                            Device = "B"
                        });
                    }
                }

                input.Count = count;
                input.StartDate = rawTimeLogs.Min(f => f.LogTime);
                input.EndDate = rawTimeLogs.Max(f => f.LogTime);

                if (!await PostValidate(input))
                    return input;

                input.FinalCount = await UploadRawTimeLogs(rawTimeLogs);
                return input;
            }
            catch (Exception)
            {
                //input.CustomError = ex.Message;
                input.CustomError = "File is not in the correct format.";
                return input;
                //throw new UserFriendlyException("Your request is not valid!", ex.Message);
            }
        }

        protected async Task<AttendancesUploadDto> UploadTaisho(AttendancesUploadDto input)
        {
            if (!PreValidate(input, ".txt"))
                return input;

            var rawTimeLogs = new List<RawTimeLogStaging>();
            var newGuid = Guid.NewGuid();
            var count = 0;

            input.UploadSessionId = newGuid;

            try
            {
                using var stream = new StreamReader(input.File.OpenReadStream());
                while (stream.Peek() >= 0)
                {
                    count++;

                    var record = await stream.ReadLineAsync();
                    var cols = record.Split("|");

                    if (cols.Length == 4)
                    {
                        var swipeCode = cols[0];
                        var logTime = string.Format("{0} {1}", cols[1], cols[2]);
                        var inOut = cols[3];

                        rawTimeLogs.Add(new RawTimeLogStaging
                        {
                            UploadSessionId = newGuid,
                            TenantId = AbpSession.TenantId.Value,
                            SwipeCode = swipeCode.TrimEnd(),
                            LogTime = DateTime.ParseExact(logTime, "yyyy-MM-dd HH:mm", CultureInfo.InvariantCulture),
                            InOut = inOut == "1" ? "I" : "O",
                            Device = "B"
                        });
                    }
                }

                input.Count = count;
                input.StartDate = rawTimeLogs.Min(f => f.LogTime);
                input.EndDate = rawTimeLogs.Max(f => f.LogTime);

                if (!await PostValidate(input))
                    return input;

                input.FinalCount = await UploadRawTimeLogs(rawTimeLogs);
                return input;
            }
            catch (Exception)
            {
                input.CustomError = "File is not in the correct format.";
                return input;
            }
        }

        protected async Task<AttendancesUploadDto> UploadBenreys(AttendancesUploadDto input)
        {
            if (!PreValidate(input, ".dat"))
                return input;

            var rawTimeLogs = new List<RawTimeLogStaging>();
            var newGuid = Guid.NewGuid();
            var count = 0;

            input.UploadSessionId = newGuid;

            try
            {
                using var stream = new StreamReader(input.File.OpenReadStream());
                while (stream.Peek() >= 0)
                {
                    count++;

                    var record = await stream.ReadLineAsync();
                    var cols = record.Split("\t");

                    if (cols.Length == 6)
                    {
                        var swipeCode = cols[0].Trim();
                        var logTime = cols[1];
                        var inOut = cols[3]; //0 = In / 1 = Out

                        rawTimeLogs.Add(new RawTimeLogStaging
                        {
                            UploadSessionId = newGuid,
                            TenantId = AbpSession.TenantId.Value,
                            SwipeCode = swipeCode.TrimEnd(),
                            LogTime = DateTime.ParseExact(logTime, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture),
                            InOut = inOut == "0" ? "I" : "O",
                            Device = "B"
                        });
                    }
                }

                input.Count = count;
                input.StartDate = rawTimeLogs.Min(f => f.LogTime);
                input.EndDate = rawTimeLogs.Max(f => f.LogTime);

                if (!await PostValidate(input))
                    return input;

                input.FinalCount = await UploadRawTimeLogs(rawTimeLogs);
                return input;
            }
            catch (Exception)
            {
                input.CustomError = "File is not in the correct format.";
                return input;
            }
        }

        public async Task<PagedResultDto<GetAttendancesForViewDto>> GetAll(GetAllAttendancesInput input)
        {
            var query = await _attendancePeriodsManager
                .GetAttendancePeriodsForView(input.AttendancePeriodIdFilter.Value);

            if (query == null)
                return new PagedResultDto<GetAttendancesForViewDto>(0, new List<GetAttendancesForViewDto>());

            var attendancePeriod = query?.AttendancePeriods;

            var totalCount = 0;
            var attendances = new List<PagedAndFilteredAttenancesDto>();
            var timesheets = new List<Tuple<DateTime, string, bool, bool>>();

            if (attendancePeriod.Posted)
            {
                var filteredAttendances = _postedAttendancesRepository.GetAll()
                    .WhereIf(attendancePeriod != null,
                        e => e.Date >= attendancePeriod.StartDate
                        && e.Date <= attendancePeriod.EndDate)
                    .WhereIf(input.EmployeeIdFilter == null, e => e.Employees.User.Id == AbpSession.UserId)
                    .WhereIf(input.EmployeeIdFilter != null, e => e.EmployeeId == input.EmployeeIdFilter);

                var pagedAndFilteredAttendances = filteredAttendances
                    .OrderBy(input.Sorting ?? "date asc");

                attendances = await pagedAndFilteredAttendances
                    .Select(o => new PagedAndFilteredAttenancesDto
                    {
                        EmployeeId = o.EmployeeId,
                        Date = o.Date,
                        DayOfWeek = o.Date.DayOfWeek,
                        Shift = o.ShiftTypes.DisplayName,
                        RestDayCode = o.RestDayCode,
                        TimeIn = o.TimeIn,
                        BreaktimeIn = o.BreaktimeIn,
                        BreaktimeOut = o.BreaktimeOut,
                        PMBreaktimeIn = o.PMBreaktimeIn,
                        PMBreaktimeOut = o.PMBreaktimeOut,
                        TimeOut = o.TimeOut,
                        IsPosted = true,
                        Invalid = false,
                        Id = o.Id
                    }).ToListAsync();
                
                totalCount = attendances.Count;

                timesheets = await _postedTimesheetsRepository.GetAll()
                    .WhereIf(input.EmployeeIdFilter == null, e => e.Employees.User.Id == AbpSession.UserId)
                    .WhereIf(input.EmployeeIdFilter != null, e => e.EmployeeId == input.EmployeeIdFilter)
                    .Where(e => attendancePeriod.StartDate <= e.Date && attendancePeriod.EndDate >= e.Date)
                    .Select(e => new Tuple<DateTime, string, bool, bool>
                    (
                        e.Date,
                        e.Remarks,
                        e.IsHoliday,
                        e.IsRestDay
                    )).ToListAsync();
            }
            else
            {
                var filteredAttendances = _attendancesRepository.GetAll()
                    .WhereIf(attendancePeriod != null,
                        e => e.Date >= attendancePeriod.StartDate
                        && e.Date <= attendancePeriod.EndDate)
                    .WhereIf(input.EmployeeIdFilter == null, e => e.Employees.User.Id == AbpSession.UserId)
                    .WhereIf(input.EmployeeIdFilter != null, e => e.EmployeeId == input.EmployeeIdFilter);

                var pagedAndFilteredAttendances = filteredAttendances
                    .OrderBy(input.Sorting ?? "date asc");

                attendances = await pagedAndFilteredAttendances
                    .Select(o => new PagedAndFilteredAttenancesDto
                    {
                        EmployeeId = o.EmployeeId,
                        Date = o.Date,
                        DayOfWeek = o.Date.DayOfWeek,
                        Shift = o.ShiftTypes.DisplayName,
                        RestDayCode = o.RestDayCode,
                        TimeIn = o.TimeIn,
                        BreaktimeIn = o.BreaktimeIn,
                        BreaktimeOut = o.BreaktimeOut,
                        PMBreaktimeIn = o.PMBreaktimeIn,
                        PMBreaktimeOut = o.PMBreaktimeOut,
                        TimeOut = o.TimeOut,
                        IsPosted = false,
                        Invalid = o.Invalid,
                        Id = o.Id
                    }).ToListAsync();

                
                totalCount = attendances.Count;

                timesheets = await _processedTimesheetsRepository.GetAll()
                    .WhereIf(input.EmployeeIdFilter == null, e => e.Employees.User.Id == AbpSession.UserId)
                    .WhereIf(input.EmployeeIdFilter != null, e => e.EmployeeId == input.EmployeeIdFilter)
                    .Where(e => attendancePeriod.StartDate <= e.Date && attendancePeriod.EndDate >= e.Date)
                    .Select(e => new Tuple<DateTime, string, bool, bool>
                    (
                        e.Date,
                        e.Remarks,
                        e.IsHoliday,
                        e.IsRestDay
                    )).ToListAsync();
            }

            var results = attendances
                .Select(e =>
                    {
                        var timesheet = timesheets
                            .Where(f => f.Item1 == e.Date)
                            .Select(f => new 
                            {
                                Remarks = f.Item2,
                                IsHoliday = f.Item3,
                                IsRestDay = f.Item4
                            }).FirstOrDefault();

                        return new GetAttendancesForViewDto
                        {
                            Attendances = new AttendancesDto
                            {
                                EmployeeId = e.EmployeeId,
                                Date = e.Date,
                                Day = e.DayOfWeek.ToString(),
                                Shift = e.Shift,
                                TimeIn = e.TimeIn,
                                BreaktimeIn = e.BreaktimeIn,
                                BreaktimeOut = e.BreaktimeOut,
                                PMBreaktimeIn = e.PMBreaktimeIn,
                                PMBreaktimeOut = e.PMBreaktimeOut,
                                TimeOut = e.TimeOut,
                                DayType =
                                    timesheet.IsHoliday && timesheet.IsRestDay ? "HAR" :
                                    timesheet.IsHoliday ? "HOL" :
                                    timesheet.IsRestDay ? "RES" : "REG",
                                Remarks = timesheet.Remarks,
                                IsPosted = e.IsPosted,
                                Id = e.Id
                            }
                        };
                    }
                ).OrderBy(e => e.Attendances.Date).ToList();

            return new PagedResultDto<GetAttendancesForViewDto>(
                totalCount,
                results
            );
        }

        public async Task<GetAttendancesForViewDto> GetAttendancesForView(int id, bool IsPosted)
        {
            var attendance = new AttendancesDto();
            var minuteFormat = await _tKSettingsAppService.MinuteFormat();

            if (IsPosted)
            {
                var postedAttendance = await _postedAttendancesRepository.GetAll()
                    .Select(o => new 
                    {
                        o.EmployeeId,
                        o.Date,
                        Day = o.Date.DayOfWeek.ToString(),
                        DayNumber = (int)o.Date.DayOfWeek == 0 ? "7" : ((int)o.Date.DayOfWeek).ToString(),
                        Shift = o.ShiftTypes.DisplayName,
                        ShiftDetails = o.ShiftTypes.ShiftTypeDetails.ToList(),
                        o.TimeIn,
                        o.BreaktimeIn,
                        o.BreaktimeOut,
                        o.PMBreaktimeIn,
                        o.PMBreaktimeOut,
                        o.TimeOut,
                        o.Remarks,
                        o.Id
                    }).SingleOrDefaultAsync(e => e.Id == id);

                var timesheet = await _postedTimesheetsRepository.GetAll()
                    .Where(e => e.TenantId == AbpSession.TenantId && e.EmployeeId == postedAttendance.EmployeeId
                        && e.Date == postedAttendance.Date)
                    .FirstOrDefaultAsync();

                var overtimeList = await _postedOvertimesRepository.GetAll()
                    .Where(e => e.TenantId == AbpSession.TenantId && e.EmployeeId == postedAttendance.EmployeeId
                        && e.Date == postedAttendance.Date)
                    .Select(e => new AttendanceOvertimeDto
                    {
                        OvertimeType = e.OvertimeTypes.ShortName,
                        Hours = minuteFormat ? _helperAppService.GetMinuteFormatString(e.Minutes)
                            : decimal.Round((decimal)e.Minutes / 60, 2).ToString()
                    }).ToListAsync();

                attendance.Id = postedAttendance.Id;
                attendance.EmployeeId = postedAttendance.EmployeeId;
                attendance.Date = postedAttendance.Date;
                attendance.Day = postedAttendance.Day;
                attendance.Shift = postedAttendance.Shift;
                attendance.TimeIn = postedAttendance.TimeIn;
                attendance.BreaktimeIn = postedAttendance.BreaktimeIn;
                attendance.BreaktimeOut = postedAttendance.BreaktimeOut;
                attendance.PMBreaktimeIn = postedAttendance.PMBreaktimeIn;
                attendance.PMBreaktimeOut = postedAttendance.PMBreaktimeIn;
                attendance.TimeOut = postedAttendance.TimeOut;
                attendance.Remarks = postedAttendance.Remarks;
                attendance.ActualShift = postedAttendance.ShiftDetails
                    .Where(e => e.Days.Contains(postedAttendance.DayNumber))
                    .Select(e => e.TimeIn + " - " + e.TimeOut)
                    .First();

                //Days
                attendance.Absence = (decimal)timesheet?.Absence;

                //Minutes
                attendance.Tardy = (decimal)timesheet?.TardyMins;
                attendance.Undertime = (decimal)timesheet?.UtMins;

                //Hours
                if (minuteFormat)
                {
                    attendance.Regular = _helperAppService.GetMinuteFormatString(timesheet.RegMins);
                    attendance.OfficialBusiness = _helperAppService.GetMinuteFormatString(timesheet.ObMins);
                    attendance.ND1 = _helperAppService.GetMinuteFormatString(timesheet.RegNd1Mins);
                    attendance.ND2 = _helperAppService.GetMinuteFormatString(timesheet.RegNd2Mins);
                }
                else
                {
                    attendance.Regular = decimal.Round((decimal)timesheet.RegMins / 60, 2).ToString();
                    attendance.OfficialBusiness = decimal.Round((decimal)timesheet.ObMins / 60, 2).ToString();
                    attendance.ND1 = decimal.Round((decimal)timesheet.RegNd1Mins / 60, 2).ToString();
                    attendance.ND2 = decimal.Round((decimal)timesheet.RegNd2Mins / 60, 2).ToString();
                }

                //OT List
                attendance.OvertimeList = overtimeList;
            }
            else
            {
                var processedAttendance = await _attendancesRepository.GetAll()
                    .Select(o => new 
                    {
                        o.EmployeeId,
                        o.Date,
                        Day = o.Date.DayOfWeek.ToString(),
                        DayNumber = (int)o.Date.DayOfWeek == 0 ? "7" : ((int)o.Date.DayOfWeek).ToString(),
                        Shift = o.ShiftTypes.DisplayName,
                        ShiftDetails = o.ShiftTypes.ShiftTypeDetails.ToList(),
                        o.TimeIn,
                        o.BreaktimeIn,
                        o.BreaktimeOut,
                        o.PMBreaktimeIn,
                        o.PMBreaktimeOut,
                        o.TimeOut,
                        o.Remarks,
                        o.Id
                    }).SingleOrDefaultAsync(e => e.Id == id);

                var timesheet = await _processedTimesheetsRepository.GetAll()
                    .Where(e => e.TenantId == AbpSession.TenantId
                        && e.EmployeeId == processedAttendance.EmployeeId
                        && e.Date == processedAttendance.Date)
                    .FirstOrDefaultAsync();

                var overtimeList = await _processedOvertimesRepository.GetAll()
                    .Where(e => e.TenantId == AbpSession.TenantId && e.EmployeeId == processedAttendance.EmployeeId
                        && e.Date == processedAttendance.Date)
                    .Select(e => new AttendanceOvertimeDto
                    {
                        OvertimeType = e.OvertimeTypes.ShortName,
                        Hours = _helperAppService.GetMinuteFormatString(e.Minutes)
                    }).ToListAsync();

                attendance.Id = processedAttendance.Id;
                attendance.EmployeeId = processedAttendance.EmployeeId;
                attendance.Date = processedAttendance.Date;
                attendance.Day = processedAttendance.Day;
                attendance.Shift = processedAttendance.Shift;
                attendance.TimeIn = processedAttendance.TimeIn;
                attendance.BreaktimeIn = processedAttendance.BreaktimeIn;
                attendance.BreaktimeOut = processedAttendance.BreaktimeOut;
                attendance.PMBreaktimeIn = processedAttendance.PMBreaktimeIn;
                attendance.PMBreaktimeOut = processedAttendance.PMBreaktimeOut;
                attendance.TimeOut = processedAttendance.TimeOut;
                attendance.Remarks = processedAttendance.Remarks;
                attendance.ActualShift = processedAttendance.ShiftDetails
                    .Where(e => e.Days.Contains(processedAttendance.DayNumber))
                    .Select(e => e.TimeIn + " - " + e.TimeOut)
                    .First();

                //Days
                attendance.Absence = (decimal)timesheet?.Absence;

                //Minutes
                attendance.Tardy = (decimal)timesheet?.TardyMins;
                attendance.Undertime = (decimal)timesheet?.UtMins;

                //Hours
                if (minuteFormat)
                {
                    attendance.Regular = _helperAppService.GetMinuteFormatString(timesheet.RegMins);
                    attendance.OfficialBusiness = _helperAppService.GetMinuteFormatString(timesheet.ObMins);
                    attendance.ND1 = _helperAppService.GetMinuteFormatString(timesheet.RegNd1Mins);
                    attendance.ND2 = _helperAppService.GetMinuteFormatString(timesheet.RegNd2Mins);
                }
                else
                {
                    attendance.Regular = decimal.Round((decimal)timesheet.RegMins / 60, 2).ToString();
                    attendance.OfficialBusiness = decimal.Round((decimal)timesheet.ObMins / 60, 2).ToString();
                    attendance.ND1 = decimal.Round((decimal)timesheet.RegNd1Mins / 60, 2).ToString();
                    attendance.ND2 = decimal.Round((decimal)timesheet.RegNd2Mins / 60, 2).ToString();
                }

                //OT List
                attendance.OvertimeList = overtimeList;
            }

            var output = new GetAttendancesForViewDto { Attendances = ObjectMapper.Map<AttendancesDto>(attendance) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Attendances_Edit)]
        public async Task<GetAttendancesForEditOutput> GetAttendancesForEdit(EntityDto input)
        {
            var attendances = await _attendancesRepository.FirstOrDefaultAsync(input.Id);

            return new GetAttendancesForEditOutput 
            { 
                Attendances = ObjectMapper.Map<CreateOrEditAttendancesDto>(attendances) 
            };
        }

        public async Task CreateOrEdit(CreateOrEditAttendancesDto input)
        {
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        [AbpAuthorize(AppPermissions.Pages_Attendances_Create)]
        protected virtual async Task Create(CreateOrEditAttendancesDto input)
        {
            var attendances = ObjectMapper.Map<Attendances>(input);
            await _attendancesRepository.InsertAsync(attendances);
        }

        [AbpAuthorize(AppPermissions.Pages_Attendances_Edit)]
        protected virtual async Task Update(CreateOrEditAttendancesDto input)
        {
            var attendances = await _attendancesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, attendances);
        }

        [AbpAuthorize(AppPermissions.Pages_Attendances_Delete)]
        public async Task Delete(EntityDto input) =>
            await _attendancesRepository.DeleteAsync(input.Id);
    }
}