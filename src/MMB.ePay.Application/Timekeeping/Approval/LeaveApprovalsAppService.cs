﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Processor;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class LeaveApprovalsAppService : ePayAppServiceBase, ILeaveApprovalsAppService
    {
        private readonly IRepository<LeaveApprovals> _leaveApprovalsRepository;
        private readonly IRepository<LeaveRequests> _leaveRequestsRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<EmployeeApprovers> _employeeApproversRepository;
        private readonly IRepository<WorkFlow> _workFlowRepository;
        private readonly IStatusAppService _statusAppService;
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;
        private readonly IProcessorAppService _processorAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public LeaveApprovalsAppService(
            IRepository<LeaveApprovals> leaveApprovalsRepository,
            IRepository<LeaveRequests> leaveRequestsRepository,
            IRepository<Employees> employeesRepository,
            IRepository<EmployeeApprovers> employeeApproversRepository,
            IRepository<WorkFlow> workFlowRepository,
            IStatusAppService statusAppService,
            IAttendancePeriodsAppService attendancePeriodsAppService,
            IProcessorAppService processorAppService,
            IApprovalsAppService approvalsAppService
            )
        {
            _leaveApprovalsRepository = leaveApprovalsRepository;
            _leaveRequestsRepository = leaveRequestsRepository;
            _employeesRepository = employeesRepository;
            _employeeApproversRepository = employeeApproversRepository;
            _workFlowRepository = workFlowRepository;
            _statusAppService = statusAppService;
            _attendancePeriodsAppService = attendancePeriodsAppService;
            _processorAppService = processorAppService;
            _approvalsAppService = approvalsAppService;
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveApprovals)]
        public async Task<PagedResultDto<GetLeaveApprovalsForViewDto>> GetAll(GetAllLeaveApprovalsInput input)
        {
            var approverId = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => (int?)e.Id)
                .FirstOrDefaultAsync();

            if (approverId == null)
                return new PagedResultDto<GetLeaveApprovalsForViewDto>(0, new List<GetLeaveApprovalsForViewDto>());

            var filteredApprovals = _leaveRequestsRepository.GetAll()
                .WhereIf(input.EmployeeIdFilter != null, e => e.EmployeeId == input.EmployeeIdFilter)
                .WhereIf(input.FrequencyIdFilter > 0,
                    e => e.Employees.EmployeeTimeInfo.FrequencyId == input.FrequencyIdFilter)
                .Where(e => e.Employees.EmployeeApproversEmployee.Any(f => f.ApproverId == approverId)
                    && e.Status.AccessLevel ==
                        e.Employees.EmployeeApproversEmployee
                            .Where(f => f.ApproverId == approverId).First().ApproverOrders.Level
                    && input.IsBackup ==
                        e.Employees.EmployeeApproversEmployee
                            .Where(f => f.ApproverId == approverId).First().ApproverOrders.IsBackup);

            if (input.FrequencyIdFilter > 0)
            {
                var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(input.FrequencyIdFilter);
                filteredApprovals = filteredApprovals
                    .Where(e => postedDate != DateTime.MinValue
                        && e.LeaveRequestDetails.Any(f => f.LeaveDate > postedDate));
            }
            else
            {
                var minDate = await _attendancePeriodsAppService.GetMinUnpostedDate();
                filteredApprovals = filteredApprovals
                    .WhereIf(minDate != null && input.FrequencyIdFilter == 0,
                        e => e.LeaveRequestDetails.Any(f => f.LeaveDate >= minDate));
            }

            return await GetPagedResult(filteredApprovals, input);
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveApprovals_Admin)]
        public async Task<PagedResultDto<GetLeaveApprovalsForViewDto>> GetAdminApprovals(GetAllLeaveApprovalsInput input)
        {
            var period = await _attendancePeriodsAppService
                .GetDateRangeFromAttendancePeriod(input.AttendancePeriodIdFilter.Value);

            if (period == null)
                return new PagedResultDto<GetLeaveApprovalsForViewDto>(0, new List<GetLeaveApprovalsForViewDto>());

            var filteredApprovals = _leaveRequestsRepository.GetAll()
                .WhereIf(input.EmployeeIdFilter != null, e => e.EmployeeId == input.EmployeeIdFilter)
                .WhereIf(input.FrequencyIdFilter > 0,
                    e => e.Employees.EmployeeTimeInfo.FrequencyId == input.FrequencyIdFilter)
                .WhereIf(input.StatusTypeIdFilter, e => e.Status.AccessLevel > 0)
                .WhereIf(!input.StatusTypeIdFilter, e => e.Status.AccessLevel == 0)
                .Where(e => e.LeaveRequestDetails.Any(f => f.LeaveDate >= period.Start && f.LeaveDate <= period.End));

            return await GetPagedResult(filteredApprovals, input);
        }

        public async Task<PagedResultDto<GetLeaveApprovalsForViewDto>> GetApprovalRequests(GetAllLeaveApprovalsInput input)
        {
            var period = await _attendancePeriodsAppService
                .GetDateRangeFromAttendancePeriod(input.AttendancePeriodIdFilter.Value);

            if (period == null)
                return new PagedResultDto<GetLeaveApprovalsForViewDto>(0, new List<GetLeaveApprovalsForViewDto>());

            var approverId = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => (int?)e.Id)
                .FirstOrDefaultAsync();

            if (approverId == null)
                return new PagedResultDto<GetLeaveApprovalsForViewDto>(0, new List<GetLeaveApprovalsForViewDto>());

            var filteredApprovals = _leaveRequestsRepository.GetAll()
                .WhereIf(input.EmployeeIdFilter != null, e => e.EmployeeId == input.EmployeeIdFilter)
                .Where(e => e.Employees.EmployeeApproversEmployee.Any(f => f.ApproverId == approverId)
                    && e.LeaveRequestDetails
                        .Any(f => f.LeaveDate >= period.Start && f.LeaveDate <= period.End)
                    && e.Status.AccessLevel !=
                        e.Employees.EmployeeApproversEmployee
                            .Where(f => f.ApproverId == approverId).First().ApproverOrders.Level
                    && input.IsBackup ==
                        e.Employees.EmployeeApproversEmployee
                            .Where(f => f.ApproverId == approverId).First().ApproverOrders.IsBackup);

            return await GetPagedResult(filteredApprovals, input);
        }

        private async Task<PagedResultDto<GetLeaveApprovalsForViewDto>> GetPagedResult(
            IQueryable<LeaveRequests> filteredApprovals,
            GetAllLeaveApprovalsInput input)
        {
            var totalCount = await filteredApprovals.CountAsync();
            var results = await filteredApprovals
                .Where(e => e.TenantId == AbpSession.TenantId)
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input)
                .Select(e => new GetLeaveApprovalsForViewDto
                {
                    LeaveApprovals = new LeaveApprovalsDto
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        Employee = e.Employees.FullName,
                        LeaveType = e.LeaveTypes.DisplayName,
                        StartDate = e.LeaveRequestDetails.Min(f => f.LeaveDate),
                        EndDate = e.LeaveRequestDetails.Max(f => f.LeaveDate),
                        Days = e.LeaveRequestDetails.Sum(f => f.Days),
                        Reason = e.LeaveReasons.DisplayName,
                        Remarks = e.Remarks,
                        Status = e.Status.DisplayName,
                        StatusId = e.StatusId,
                        Id = e.Id
                    }
                }).ToListAsync();

            return new PagedResultDto<GetLeaveApprovalsForViewDto>(totalCount, results);
        }

        public async Task<GetLeaveApprovalsForViewDto> GetLeaveApprovalsForView(int id)
        {
            var request = await _leaveRequestsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new
                {
                    LeaveDetails = new LeaveDetailsDto
                    {
                        Employee = e.Employees.FullName,
                        LeaveType = e.LeaveTypes.DisplayName,
                        StartDate = e.LeaveRequestDetails.Min(f => f.LeaveDate),
                        EndDate = e.LeaveRequestDetails.Max(f => f.LeaveDate),
                        Days = e.LeaveRequestDetails.Sum(f => f.Days),
                        ActualDates = string.Join(", ", e.LeaveRequestDetails.Select(f => f.LeaveDate.ToString("MM/dd/yyyy")).ToList()),
                        Reason = e.LeaveReasons.DisplayName,
                        Remarks = e.Remarks,
                        DateFiled = e.CreationTime,
                        Status = e.Status.DisplayName,
                        Id = e.Id
                    },
                    e.StatusId
                }).FirstOrDefaultAsync();

            request.LeaveDetails.StatusList = await _workFlowRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.StatusId == request.StatusId && e.Status.AccessLevel > 0)
                .Select(e => new SelectListItem
                {
                    Value = e.NextStatus.ToString(),
                    Text = e.NextStatusNavigation.BeforeStatusActionText
                }).ToListAsync();

            return new GetLeaveApprovalsForViewDto { LeaveDetails = request.LeaveDetails };
        }

        public async Task UpdateStatus(UpdateLeaveRequestsStatus input) =>
            await UpdateStatusOrAdminUpdateStatus(input, true);

        [AbpAuthorize(AppPermissions.Pages_LeaveApprovals_Admin)]
        public async Task AdminUpdateStatus(UpdateLeaveRequestsStatus input) =>
            await UpdateStatusOrAdminUpdateStatus(input, false);

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_LeaveApprovals_Edit)]
        private async Task UpdateStatusOrAdminUpdateStatus(UpdateLeaveRequestsStatus input, bool checkAccessLevel)
        {
            var status = await _statusAppService.GetStatusDisplayName(input.StatusId);

            if (status.StatusDisplayName is StatusDisplayNames.ForUpdate
                or StatusDisplayNames.Declined or StatusDisplayNames.Cancelled)
            {
                if (string.IsNullOrWhiteSpace(input.Reason))
                    throw new UserFriendlyException(L("YourRequestIsNotValid"), "The Remarks field is required.");
            }

            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
            var approver = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new { e.Id, e.FullName })
                .FirstOrDefaultAsync();

            var request = await GetRequestForApproval(input.Id, approver.Id);

            if (request != null)
            {
                request.StatusId = input.StatusId;
                request.Reason = input.Reason;

                await ChangeStatusForSingleApproval(status, request);
                await CreateApproval(input);

                if (status.StatusDisplayName is not StatusDisplayNames.Cancelled or StatusDisplayNames.Reopened)
                    await SendApprovalDetails(request, status, approver.FullName);

                if (status.StatusDisplayName == StatusDisplayNames.Approved)
                {
                    var periodId = await _attendancePeriodsAppService
                        .GetFirstUnpostedPeriodId(request.Employees.EmployeeTimeInfo.FrequencyId);

                    await _processorAppService.Process(periodId, request.EmployeeId);
                }
            }
            else
                throw new UserFriendlyException(L("YourRequestIsNotValid"));
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_LeaveApprovals_Create)]
        public async Task ApproveAll(ApproveAllLeaveRequests input)
        {
            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
            var approver = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new { e.Id, e.FullName }).FirstOrDefaultAsync();

            var frequencyIds = new List<int>();

            foreach (var id in input.Ids)
            {
                var request = await GetRequestForApproval(id, approver.Id);

                if (request != null)
                {
                    request.StatusId = await _workFlowRepository.GetAll()
                        .Where(e => e.StatusId == request.StatusId
                            && e.NextStatusNavigation.BeforeStatusActionText == "Approve")
                        .Select(e => e.NextStatusNavigation.Id)
                        .FirstOrDefaultAsync();

                    var status = await _statusAppService.GetStatusDisplayName(request.StatusId);
                    var approval = new UpdateLeaveRequestsStatus { Id = id, StatusId = request.StatusId };

                    await ChangeStatusForSingleApproval(status, request);
                    await CreateApproval(approval);

                    if (status.StatusDisplayName is not StatusDisplayNames.Cancelled or StatusDisplayNames.Reopened)
                        await SendApprovalDetails(request, status, approver.FullName);

                    if (status.StatusDisplayName == StatusDisplayNames.Approved)
                        if (!frequencyIds.Contains(request.Employees.EmployeeTimeInfo.FrequencyId))
                            frequencyIds.Add(request.Employees.EmployeeTimeInfo.FrequencyId);
                }
                else
                    throw new UserFriendlyException(L("YourRequestIsNotValid"));
            }

            foreach (var frequencyId in frequencyIds)
            {
                await UnitOfWorkManager.Current.SaveChangesAsync();

                var periodId = await _attendancePeriodsAppService.GetFirstUnpostedPeriodId(frequencyId);
                await _processorAppService.Process(periodId, null);
            }
        }

        private async Task<LeaveRequests> GetRequestForApproval(int id, int? approverId)
        {
            var request = _leaveRequestsRepository.GetAll()
                .Include(e => e.LeaveRequestDetails)
                .Include(e => e.Employees)
                    .ThenInclude(e => e.EmployeeTimeInfo)
                .Where(e => e.Id == id);

            if (approverId != null)
            {
                request = request
                    .Where(e => (e.Status.AccessLevel == 0 && e.EmployeeId == approverId)
                    || (e.Status.AccessLevel == e.Employees.EmployeeApproversEmployee
                        .Where(f => f.ApproverId == approverId).First().ApproverOrders.Level));
            }

            return await request.FirstOrDefaultAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveApprovals_Edit)]
        private async Task ChangeStatusForSingleApproval(GetStatusForDisplayNameDto status, LeaveRequests request)
        {
            if (status.StatusDisplayName == StatusDisplayNames.ForNextApproval)
            {
                var hasOverLevel1 = await _employeeApproversRepository.GetAll()
                    .AnyAsync(e => e.EmployeeId == request.EmployeeId && e.ApproverOrders.Level > 1);

                if (!hasOverLevel1)
                {
                    var approverStatus = (await _statusAppService.GetStatusByDisplayNames(StatusDisplayNames.Approved)).First();

                    request.StatusId = approverStatus.Status.Id;
                    status.StatusDisplayName = StatusDisplayNames.Approved;
                    status.AfterStatusMessage = approverStatus.Status.AfterStatusMessage;
                    status.StatusName = approverStatus.Status.DisplayName;
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveApprovals_Create)]
        private async Task CreateApproval(UpdateLeaveRequestsStatus input) =>
            await _leaveApprovalsRepository.InsertAsync(new LeaveApprovals
            {
                TenantId = AbpSession.TenantId.Value,
                Remarks = input.Reason,
                LeaveRequestId = input.Id,
                StatusId = input.StatusId,
            });

        private async Task SendApprovalDetails(LeaveRequests request, GetStatusForDisplayNameDto status, string approverName)
        {
            var dto = new EmailRequestDto
            {
                Request = L("Leave"),
                FullName = approverName,
                ApprovalStatus = status.AfterStatusMessage,
                Remarks = request.Remarks,
                Reason = request.Reason,
                StartDate = request.LeaveRequestDetails.Min(f => f.LeaveDate),
                EndDate = request.LeaveRequestDetails.Max(f => f.LeaveDate),
                Recipient = request.Employees.Email
            };

            await _approvalsAppService.SendApprovalDetails(dto);
        }
    }
}