﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Processor;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class AttendanceApprovalsAppService : ePayAppServiceBase, IAttendanceApprovalsAppService
    {
        private readonly IRepository<AttendanceApprovals> _attendanceApprovalsRepository;
        private readonly IRepository<AttendanceRequests> _attendanceRequestsRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<EmployeeApprovers> _employeeApproversRepository;
        private readonly IRepository<WorkFlow> _workFlowRepository;
        private readonly IStatusAppService _statusAppService;
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;
        private readonly IProcessorAppService _processorAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public AttendanceApprovalsAppService(
            IRepository<AttendanceApprovals> attendanceApprovalsRepository,
            IRepository<AttendanceRequests> attendanceRequestsRepository,
            IRepository<Employees> employeesRepository,
            IRepository<EmployeeApprovers> employeeApproversRepository,
            IRepository<WorkFlow> workFlowRepository,
            IStatusAppService statusAppService,
            IAttendancePeriodsAppService attendancePeriodsAppService,
            IProcessorAppService processorAppService,
            IApprovalsAppService approvalsAppService
            )
        {
            _attendanceApprovalsRepository = attendanceApprovalsRepository;
            _attendanceRequestsRepository = attendanceRequestsRepository;
            _employeesRepository = employeesRepository;
            _employeeApproversRepository = employeeApproversRepository;
            _workFlowRepository = workFlowRepository;
            _statusAppService = statusAppService;
            _attendancePeriodsAppService = attendancePeriodsAppService;
            _processorAppService = processorAppService;
            _approvalsAppService = approvalsAppService;
        }

        [AbpAuthorize(AppPermissions.Pages_AttendanceApprovals)]
        public async Task<PagedResultDto<GetAttendanceApprovalsForViewDto>> GetAll(GetAllAttendanceApprovalsInput input)
        {
            var approverId = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => (int?)e.Id)
                .FirstOrDefaultAsync();

            if (approverId == null)
                return new PagedResultDto<GetAttendanceApprovalsForViewDto>(0, new List<GetAttendanceApprovalsForViewDto>());

            var filteredApprovals = _attendanceRequestsRepository.GetAll()
                .WhereIf(input.EmployeeIdFilter != null, e => e.EmployeeId == input.EmployeeIdFilter)
                .WhereIf(input.FrequencyIdFilter > 0,
                    e => e.Employees.EmployeeTimeInfo.FrequencyId == input.FrequencyIdFilter)
                .Where(e => e.Employees.EmployeeApproversEmployee.Any(f => f.ApproverId == approverId)
                    && e.Status.AccessLevel ==
                        e.Employees.EmployeeApproversEmployee
                            .Where(f => f.ApproverId == approverId).First().ApproverOrders.Level
                    && input.IsBackup ==
                        e.Employees.EmployeeApproversEmployee
                            .Where(f => f.ApproverId == approverId).First().ApproverOrders.IsBackup);

            if (input.FrequencyIdFilter > 0)
            {
                var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(input.FrequencyIdFilter);
                filteredApprovals = filteredApprovals
                    .Where(e => postedDate != DateTime.MinValue && e.Date > postedDate);
            }
            else
            {
                var minDate = await _attendancePeriodsAppService.GetMinUnpostedDate();
                filteredApprovals = filteredApprovals
                    .WhereIf(minDate != null && input.FrequencyIdFilter == 0, e => e.Date >= minDate);
            }

            return await GetPagedResult(filteredApprovals, input);
        }

        [AbpAuthorize(AppPermissions.Pages_AttendanceApprovals_Admin)]
        public async Task<PagedResultDto<GetAttendanceApprovalsForViewDto>> GetAdminApprovals(GetAllAttendanceApprovalsInput input)
        {
            var period = await _attendancePeriodsAppService
                .GetDateRangeFromAttendancePeriod(input.AttendancePeriodIdFilter.Value);

            if (period == null)
                return new PagedResultDto<GetAttendanceApprovalsForViewDto>(0, new List<GetAttendanceApprovalsForViewDto>());

            var filteredApprovals = _attendanceRequestsRepository.GetAll()
                .WhereIf(input.EmployeeIdFilter != null, e => e.EmployeeId == input.EmployeeIdFilter)
                .WhereIf(input.FrequencyIdFilter > 0,
                    e => e.Employees.EmployeeTimeInfo.FrequencyId == input.FrequencyIdFilter)
                .WhereIf(input.StatusTypeIdFilter, e => e.Status.AccessLevel > 0)
                .WhereIf(!input.StatusTypeIdFilter, e => e.Status.AccessLevel == 0)
                .Where(e => e.Date >= period.Start && e.Date <= period.End);

            return await GetPagedResult(filteredApprovals, input);
        }

        [AbpAuthorize(AppPermissions.Pages_AttendanceApprovals)]
        public async Task<PagedResultDto<GetAttendanceApprovalsForViewDto>> GetApprovalRequests(GetAllAttendanceApprovalsInput input)
        {
            var period = await _attendancePeriodsAppService
                .GetDateRangeFromAttendancePeriod(input.AttendancePeriodIdFilter.Value);

            if (period == null)
                return new PagedResultDto<GetAttendanceApprovalsForViewDto>(0, new List<GetAttendanceApprovalsForViewDto>());

            var approverId = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => (int?)e.Id)
                .FirstOrDefaultAsync();

            if (approverId == null)
                return new PagedResultDto<GetAttendanceApprovalsForViewDto>(0, new List<GetAttendanceApprovalsForViewDto>());

            var filteredApprovals = _attendanceRequestsRepository.GetAll()
                .WhereIf(input.EmployeeIdFilter != null, e => e.EmployeeId == input.EmployeeIdFilter)
                .Where(e => e.Employees.EmployeeApproversEmployee.Any(f => f.ApproverId == approverId)
                    && e.Date >= period.Start && e.Date <= period.End
                    && e.Status.AccessLevel !=
                        e.Employees.EmployeeApproversEmployee
                            .Where(f => f.ApproverId == approverId).First().ApproverOrders.Level
                    && input.IsBackup ==
                        e.Employees.EmployeeApproversEmployee
                            .Where(f => f.ApproverId == approverId).First().ApproverOrders.IsBackup);

            return await GetPagedResult(filteredApprovals, input);
        }

        private async Task<PagedResultDto<GetAttendanceApprovalsForViewDto>> GetPagedResult(
            IQueryable<AttendanceRequests> filteredApprovals,
            GetAllAttendanceApprovalsInput input)
        {
            var totalCount = await filteredApprovals.CountAsync();
            var results = await filteredApprovals
                .Where(e => e.TenantId == AbpSession.TenantId)
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input)
                .Select(e => new GetAttendanceApprovalsForViewDto
                {
                    AttendanceApprovals = new AttendanceApprovalsDto
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        Employee = e.Employees.FullName,
                        Date = e.Date,
                        TimeIn = e.TimeIn,
                        BreaktimeIn = e.BreaktimeIn,
                        BreaktimeOut = e.BreaktimeOut,
                        PMBreaktimeIn = e.PMBreaktimeIn,
                        PMBreaktimeOut = e.PMBreaktimeOut,
                        TimeOut = e.TimeOut,
                        Reason = e.AttendanceReason.DisplayName,
                        Remarks = e.Remarks,
                        Status = e.Status.DisplayName,
                        StatusId = e.StatusId,
                        Id = e.Id
                    }
                }).ToListAsync();

            return new PagedResultDto<GetAttendanceApprovalsForViewDto>(totalCount, results);
        }

        [AbpAuthorize(AppPermissions.Pages_AttendanceApprovals)]
        public async Task<GetAttendanceApprovalsForViewDto> GetAttendanceApprovalsForView(int id)
        {
            var request = await _attendanceRequestsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new
                {
                    AttendanceDetails = new AttendanceDetailsDto
                    {
                        Employee = e.Employees.FullName,
                        Date = e.Date,
                        TimeIn = e.TimeIn,
                        TimeOut = e.TimeOut,
                        BreaktimeIn = e.BreaktimeIn,
                        BreaktimeOut = e.BreaktimeOut,
                        PMBreaktimeIn = e.PMBreaktimeIn,
                        PMBreaktimeOut = e.PMBreaktimeOut,
                        Remarks = e.Remarks,
                        Reason = e.AttendanceReason.DisplayName,
                        DateFiled = e.CreationTime,
                        Status = e.Status.DisplayName,
                        Id = e.Id
                    },
                    e.StatusId
                }).FirstOrDefaultAsync();

            request.AttendanceDetails.StatusList = await _workFlowRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.StatusId == request.StatusId && e.Status.AccessLevel > 0)
                .Select(e => new SelectListItem
                {
                    Value = e.NextStatus.ToString(),
                    Text = e.NextStatusNavigation.BeforeStatusActionText
                }).ToListAsync();

            return new GetAttendanceApprovalsForViewDto { AttendanceDetails = request.AttendanceDetails };
        }

        public async Task UpdateStatus(UpdateAttendanceRequestsStatus input) =>
            await UpdateStatusOrAdminUpdateStatus(input, true);

        [AbpAuthorize(AppPermissions.Pages_AttendanceApprovals_Admin)]
        public async Task AdminUpdateStatus(UpdateAttendanceRequestsStatus input) =>
            await UpdateStatusOrAdminUpdateStatus(input, false);

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_AttendanceApprovals_Edit)]
        private async Task UpdateStatusOrAdminUpdateStatus(UpdateAttendanceRequestsStatus input, bool checkAccessLevel)
        {
            var status = await _statusAppService.GetStatusDisplayName(input.StatusId);

            if (status.StatusDisplayName is StatusDisplayNames.ForUpdate
                or StatusDisplayNames.Declined or StatusDisplayNames.Cancelled)
            {
                if (string.IsNullOrWhiteSpace(input.Reason))
                    throw new UserFriendlyException(L("YourRequestIsNotValid"), "The Remarks field is required.");
            }

            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
            var approver = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new { e.Id, e.FullName })
                .FirstOrDefaultAsync();

            var request = await GetRequestForApproval(input.Id, checkAccessLevel ? approver.Id : null);

            if (request != null)
            {
                request.StatusId = input.StatusId;
                request.Reason = input.Reason;

                await ChangeStatusForSingleApproval(status, request);
                await CreateApproval(input);

                if (status.StatusDisplayName is not StatusDisplayNames.Cancelled or StatusDisplayNames.Reopened)
                    await SendApprovalDetails(request, status.AfterStatusMessage, approver.FullName);

                if (status.StatusDisplayName == StatusDisplayNames.Approved)
                {
                    var periodId = await _attendancePeriodsAppService
                        .GetFirstUnpostedPeriodId(request.Employees.EmployeeTimeInfo.FrequencyId);

                    await _processorAppService.Process(periodId, request.EmployeeId);
                }
            }
            else
                throw new UserFriendlyException(L("YourRequestIsNotValid"));
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_LeaveApprovals_Edit)]
        public async Task ApproveAll(ApproveAllAttendanceRequests input)
        {
            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
            var approver = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new { e.Id, e.FullName })
                .FirstOrDefaultAsync();

            var frequencyIds = new List<int>();

            foreach (var id in input.Ids)
            {
                var request = await GetRequestForApproval(id, approver.Id);

                if (request != null)
                {
                    request.StatusId = await _workFlowRepository.GetAll()
                        .Where(e => e.StatusId == request.StatusId
                            && e.NextStatusNavigation.BeforeStatusActionText == "Approve")
                        .Select(e => e.NextStatusNavigation.Id)
                        .FirstOrDefaultAsync();

                    var status = await _statusAppService.GetStatusDisplayName(request.StatusId);
                    var approval = new UpdateAttendanceRequestsStatus { Id = id, StatusId = request.StatusId };

                    await ChangeStatusForSingleApproval(status, request);
                    await CreateApproval(approval);

                    if (status.StatusDisplayName is not StatusDisplayNames.Cancelled or StatusDisplayNames.Reopened)
                        await SendApprovalDetails(request, status.AfterStatusMessage, approver.FullName);

                    if (status.StatusDisplayName == StatusDisplayNames.Approved)
                        if (!frequencyIds.Contains(request.Employees.EmployeeTimeInfo.FrequencyId))
                            frequencyIds.Add(request.Employees.EmployeeTimeInfo.FrequencyId);
                }
                else
                    throw new UserFriendlyException(L("YourRequestIsNotValid"));
            }

            foreach (var frequencyId in frequencyIds)
            {
                await UnitOfWorkManager.Current.SaveChangesAsync();

                var periodId = await _attendancePeriodsAppService.GetFirstUnpostedPeriodId(frequencyId);
                await _processorAppService.Process(periodId, null);
            }
        }

        private async Task<AttendanceRequests> GetRequestForApproval(int id, int? approverId)
        {
            var request = _attendanceRequestsRepository.GetAll()
                .Include(e => e.Employees)
                    .ThenInclude(e => e.EmployeeTimeInfo)
                .Where(e => e.Id == id);

            if (approverId != null)
            {
                request = request
                    .Where(e => (e.Status.AccessLevel == 0 && e.EmployeeId == approverId)
                    || (e.Status.AccessLevel == e.Employees.EmployeeApproversEmployee
                        .Where(f => f.ApproverId == approverId).First().ApproverOrders.Level));
            }

            return await request.FirstOrDefaultAsync();
        }

        private async Task ChangeStatusForSingleApproval(GetStatusForDisplayNameDto status, AttendanceRequests request)
        {
            if (status.StatusDisplayName == StatusDisplayNames.ForNextApproval)
            {
                var hasOverLevel1 = await _employeeApproversRepository.GetAll()
                    .AnyAsync(e => e.EmployeeId == request.EmployeeId && e.ApproverOrders.Level > 1);

                if (!hasOverLevel1)
                {
                    var approverStatus = (await _statusAppService.GetStatusByDisplayNames(StatusDisplayNames.Approved)).First();

                    request.StatusId = approverStatus.Status.Id;
                    status.StatusDisplayName = StatusDisplayNames.Approved;
                    status.AfterStatusMessage = approverStatus.Status.AfterStatusMessage;
                    status.StatusName = approverStatus.Status.DisplayName;
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_AttendanceApprovals_Create)]
        private async Task CreateApproval(UpdateAttendanceRequestsStatus input) =>
            await _attendanceApprovalsRepository.InsertAsync(new AttendanceApprovals
            {
                TenantId = AbpSession.TenantId.Value,
                Remarks = input.Reason,
                AttendanceRequestId = input.Id,
                StatusId = input.StatusId,
            });

        private async Task SendApprovalDetails(AttendanceRequests request, string approvalStatus, string approverName)
        {
            var dto = new EmailRequestDto
            {
                Request = L("Attendance"),
                FullName = approverName,
                ApprovalStatus = approvalStatus,
                Remarks = request.Remarks,
                Reason = request.Reason,
                StartDate = request.Date,
                Recipient = request.Employees.Email
            };

            await _approvalsAppService.SendApprovalDetails(dto);
        }
    }
}