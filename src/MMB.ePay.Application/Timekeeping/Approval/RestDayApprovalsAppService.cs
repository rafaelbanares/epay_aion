﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Processor;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize(AppPermissions.Pages_RestDayApprovals)]
    public class RestDayApprovalsAppService : ePayAppServiceBase, IRestDayApprovalsAppService
    {
        private readonly IRepository<RestDayApprovals> _restDayApprovalsRepository;
        private readonly IRepository<RestDayRequests> _restDayRequestsRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<EmployeeApprovers> _employeeApproversRepository;
        private readonly IRepository<WorkFlow> _workFlowRepository;
        private readonly IStatusAppService _statusAppService;
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;
        private readonly IProcessorAppService _processorAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public RestDayApprovalsAppService(
            IRepository<RestDayRequests> restDayRequestsRepository,
            IRepository<Employees> employeesRepository,
            IRepository<EmployeeApprovers> employeeApproversRepository,
            IRepository<RestDayApprovals> restDayApprovalsRepository,
            IRepository<WorkFlow> workFlowRepository,
            IStatusAppService statusAppService,
            IAttendancePeriodsAppService attendancePeriodsAppService,
            IProcessorAppService processorAppService,
            IApprovalsAppService approvalsAppService
            )
        {
            _restDayRequestsRepository = restDayRequestsRepository;
            _employeesRepository = employeesRepository;
            _employeeApproversRepository = employeeApproversRepository;
            _restDayApprovalsRepository = restDayApprovalsRepository;
            _workFlowRepository = workFlowRepository;
            _statusAppService = statusAppService;
            _attendancePeriodsAppService = attendancePeriodsAppService;
            _processorAppService = processorAppService;
            _approvalsAppService = approvalsAppService;
        }

        public async Task<PagedResultDto<GetRestDayApprovalsForViewDto>> GetAll(GetAllRestDayApprovalsInput input)
        {
            var approverId = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => (int?)e.Id)
                .FirstOrDefaultAsync();

            if (approverId == null)
                return new PagedResultDto<GetRestDayApprovalsForViewDto>(0, new List<GetRestDayApprovalsForViewDto>());

            var filteredApprovals = _restDayRequestsRepository.GetAll()
                .WhereIf(input.EmployeeIdFilter != null, e => e.EmployeeId == input.EmployeeIdFilter)
                .WhereIf(input.FrequencyIdFilter > 0,
                    e => e.Employees.EmployeeTimeInfo.FrequencyId == input.FrequencyIdFilter)
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.Employees.EmployeeApproversEmployee.Any(f => f.ApproverId == approverId)
                    && e.Status.AccessLevel ==
                        e.Employees.EmployeeApproversEmployee
                            .Where(f => f.ApproverId == approverId).First().ApproverOrders.Level
                    && input.IsBackup ==
                        e.Employees.EmployeeApproversEmployee
                            .Where(f => f.ApproverId == approverId).First().ApproverOrders.IsBackup);

            if (input.FrequencyIdFilter > 0)
            {
                var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(input.FrequencyIdFilter);
                filteredApprovals = filteredApprovals
                    .Where(e => postedDate != DateTime.MinValue && e.RestDayStart > postedDate);
            }
            else
            {
                var minDate = await _attendancePeriodsAppService.GetMinUnpostedDate();
                filteredApprovals = filteredApprovals
                    .WhereIf(minDate != null && input.FrequencyIdFilter == 0, e => e.RestDayStart >= minDate);
            }

            return await GetPagedResult(filteredApprovals, input);
        }

        [AbpAuthorize(AppPermissions.Pages_RestDayApprovals_Admin)]
        public async Task<PagedResultDto<GetRestDayApprovalsForViewDto>> GetAdminApprovals(GetAllRestDayApprovalsInput input)
        {
            var period = await _attendancePeriodsAppService
                .GetDateRangeFromAttendancePeriod(input.AttendancePeriodIdFilter.Value);

            if (period == null)
                return new PagedResultDto<GetRestDayApprovalsForViewDto>(0, new List<GetRestDayApprovalsForViewDto>());

            var filteredApprovals = _restDayRequestsRepository.GetAll()
                .WhereIf(input.EmployeeIdFilter != null, e => e.EmployeeId == input.EmployeeIdFilter)
                .WhereIf(input.FrequencyIdFilter > 0,
                    e => e.Employees.EmployeeTimeInfo.FrequencyId == input.FrequencyIdFilter)
                .WhereIf(input.StatusTypeIdFilter, e => e.Status.AccessLevel > 0)
                .WhereIf(!input.StatusTypeIdFilter, e => e.Status.AccessLevel == 0)
                .Where(e => (e.RestDayStart >= period.Start && e.RestDayStart <= period.End)
                    || (e.RestDayEnd >= period.Start && e.RestDayEnd <= period.End));

            return await GetPagedResult(filteredApprovals, input);
        }

        public async Task<PagedResultDto<GetRestDayApprovalsForViewDto>> GetApprovalRequests(GetAllRestDayApprovalsInput input)
        {
            var period = await _attendancePeriodsAppService
                .GetDateRangeFromAttendancePeriod(input.AttendancePeriodIdFilter.Value);

            if (period == null)
                return new PagedResultDto<GetRestDayApprovalsForViewDto>(0, new List<GetRestDayApprovalsForViewDto>());

            var approverId = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => (int?)e.Id)
                .FirstOrDefaultAsync();

            if (approverId == null)
                return new PagedResultDto<GetRestDayApprovalsForViewDto>(0, new List<GetRestDayApprovalsForViewDto>());

            var filteredApprovals = _restDayRequestsRepository.GetAll()
                .WhereIf(input.EmployeeIdFilter != null, e => e.EmployeeId == input.EmployeeIdFilter)
                .Where(e => (e.RestDayStart >= period.Start && e.RestDayStart <= period.End)
                    || (e.RestDayEnd >= period.Start && e.RestDayEnd <= period.End))
                .Where(e => e.Employees.EmployeeApproversEmployee.Any(f => f.ApproverId == approverId)
                    && e.Status.AccessLevel !=
                        e.Employees.EmployeeApproversEmployee
                            .Where(f => f.ApproverId == approverId).First().ApproverOrders.Level
                    && input.IsBackup ==
                        e.Employees.EmployeeApproversEmployee
                            .Where(f => f.ApproverId == approverId).First().ApproverOrders.IsBackup);

            return await GetPagedResult(filteredApprovals, input);
        }

        private async Task<PagedResultDto<GetRestDayApprovalsForViewDto>> GetPagedResult(
            IQueryable<RestDayRequests> filteredApprovals,
            GetAllRestDayApprovalsInput input)
        {
            var totalCount = await filteredApprovals.CountAsync();
            var results = await filteredApprovals
                .Where(e => e.TenantId == AbpSession.TenantId)
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input)
                .Select(e => new GetRestDayApprovalsForViewDto
                {
                    RestDayApprovals = new RestDayApprovalsDto
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        Employee = e.Employees.FullName,
                        RestDayStart = e.RestDayStart,
                        RestDayEnd = e.RestDayEnd,
                        Reason = e.RestDayReasons.DisplayName,
                        Remarks = e.Remarks,
                        Status = e.Status.DisplayName,
                        StatusId = e.StatusId,
                        Id = e.Id
                    }
                }).ToListAsync();

            results = results
                .Select(e =>
                {
                    if (e.RestDayApprovals.RestDays != null)
                    {
                        e.RestDayApprovals.RestDays = string.Join(", ", e.RestDayApprovals.RestDays.ToCharArray()
                            .Select(f => (int)char.GetNumericValue(f))
                            .Select(f => Enum.GetName(typeof(DayOfWeek), f == 7 ? 0 : f).Substring(0, 3)));
                    }

                    return e;
                }).ToList();

            return new PagedResultDto<GetRestDayApprovalsForViewDto>(totalCount, results);
        }

        public async Task<GetRestDayApprovalsForViewDto> GetRestDayApprovalsForView(int id)
        {
            var request = await _restDayRequestsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new
                {
                    e.Status.AccessLevel,
                    Employee = e.Employees.FullName,
                    e.RestDayEnd,
                    e.RestDayStart,
                    RestDay = e.RestDayCode,
                    Reason = e.RestDayReasons.DisplayName,
                    e.Remarks,
                    Status = e.Status.DisplayName,
                    DateFiled = e.CreationTime,
                    e.StatusId,
                    e.Id
                }).FirstOrDefaultAsync();

            var statusList = await _workFlowRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.StatusId == request.StatusId
                    && e.Status.AccessLevel > 0)
                .Select(e => new SelectListItem
                {
                    Value = e.NextStatus.ToString(),
                    Text = e.NextStatusNavigation.BeforeStatusActionText
                }).ToListAsync();

            var output = new GetRestDayApprovalsForViewDto
            {
                RestDayDetails = new RestDayDetailsDto
                {
                    Employee = request.Employee,
                    RestDayStart = request.RestDayStart,
                    RestDayEnd = request.RestDayEnd,
                    RestDay = string.Join(", ", request.RestDay.ToCharArray()
                        .Select(f => (int)char.GetNumericValue(f))
                        .Select(f => Enum.GetName(typeof(DayOfWeek), f == 7 ? 0 : f).Substring(0, 3))),
                    Reason = request.Reason,
                    Remarks = request.Remarks,
                    DateFiled = request.DateFiled,
                    Status = request.Status,
                    Id = request.Id,
                    StatusList = statusList
                }
            };

            return output;
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_RestDayApprovals_Edit)]
        public async Task UpdateStatus(UpdateRestDayRequestsStatus input) =>
            await UpdateStatusOrAdminUpdateStatus(input, true);

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_RestDayApprovals_Admin)]
        public async Task AdminUpdateStatus(UpdateRestDayRequestsStatus input) =>
            await UpdateStatusOrAdminUpdateStatus(input, false);

        private async Task UpdateStatusOrAdminUpdateStatus(UpdateRestDayRequestsStatus input, bool checkAccessLevel)
        {
            var status = await _statusAppService.GetStatusDisplayName(input.StatusId);

            if (status.StatusDisplayName is StatusDisplayNames.ForUpdate
                or StatusDisplayNames.Declined or StatusDisplayNames.Cancelled)
            {
                if (string.IsNullOrWhiteSpace(input.Reason))
                    throw new UserFriendlyException(L("YourRequestIsNotValid"), "The Remarks field is required.");
            }

            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
            var approver = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new { e.Id, e.FullName })
                .FirstOrDefaultAsync();

            var request = await GetRequestForApproval(input.Id, checkAccessLevel ? approver.Id : null);

            if (request != null)
            {
                request.StatusId = input.StatusId;
                request.Reason = input.Reason;

                await ChangeStatusForSingleApproval(status, request);
                await CreateApproval(input);

                if (status.StatusDisplayName is not StatusDisplayNames.Cancelled or StatusDisplayNames.Reopened)
                    await SendApprovalDetails(request, status, approver.FullName);

                if (status.StatusDisplayName == StatusDisplayNames.Approved)
                {
                    var periodId = await _attendancePeriodsAppService
                        .GetFirstUnpostedPeriodId(request.Employees.EmployeeTimeInfo.FrequencyId);

                    await _processorAppService.Process(periodId, request.EmployeeId);
                }
            }
            else
                throw new UserFriendlyException(L("YourRequestIsNotValid"));
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_RestDayApprovals_Edit)]
        public async Task ApproveAll(ApproveAllRestDayRequests input)
        {
            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
            var approver = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new { e.Id, e.FullName })
                .FirstOrDefaultAsync();

            var frequencyIds = new List<int>();

            foreach (var id in input.Ids)
            {
                var request = await GetRequestForApproval(id, approver.Id);

                if (request != null)
                {
                    request.StatusId = await _workFlowRepository.GetAll()
                        .Where(e => e.StatusId == request.StatusId
                            && e.NextStatusNavigation.BeforeStatusActionText == "Approve")
                        .Select(e => e.NextStatusNavigation.Id)
                        .FirstOrDefaultAsync();

                    var status = await _statusAppService.GetStatusDisplayName(request.StatusId);
                    var approval = new UpdateRestDayRequestsStatus { Id = id, StatusId = request.StatusId };

                    await ChangeStatusForSingleApproval(status, request);
                    await CreateApproval(approval);

                    if (status.StatusDisplayName is not StatusDisplayNames.Cancelled or StatusDisplayNames.Reopened)
                        await SendApprovalDetails(request, status, approver.FullName);

                    if (status.StatusDisplayName == StatusDisplayNames.Approved)
                        if (!frequencyIds.Contains(request.Employees.EmployeeTimeInfo.FrequencyId))
                            frequencyIds.Add(request.Employees.EmployeeTimeInfo.FrequencyId);
                }
                else
                    throw new UserFriendlyException(L("YourRequestIsNotValid"));
            }

            foreach (var frequencyId in frequencyIds)
            {
                await UnitOfWorkManager.Current.SaveChangesAsync();

                var periodId = await _attendancePeriodsAppService.GetFirstUnpostedPeriodId(frequencyId);
                await _processorAppService.Process(periodId, null);
            }
        }

        private async Task<RestDayRequests> GetRequestForApproval(int id, int? approverId)
        {
            var request = _restDayRequestsRepository.GetAll()
                .Include(e => e.Employees)
                    .ThenInclude(e => e.EmployeeTimeInfo)
                .Where(e => e.Id == id);

            if (approverId != null)
            {
                request = request
                    .Where(e => (e.Status.AccessLevel == 0 && e.EmployeeId == approverId)
                    || (e.Status.AccessLevel == e.Employees.EmployeeApproversEmployee
                        .Where(f => f.ApproverId == approverId).First().ApproverOrders.Level));
            }

            return await request.FirstOrDefaultAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_LeaveApprovals_Edit)]
        private async Task ChangeStatusForSingleApproval(GetStatusForDisplayNameDto status, RestDayRequests request)
        {
            if (status.StatusDisplayName == StatusDisplayNames.ForNextApproval)
            {
                var hasOverLevel1 = await _employeeApproversRepository.GetAll()
                    .AnyAsync(e => e.EmployeeId == request.EmployeeId && e.ApproverOrders.Level > 1);

                if (!hasOverLevel1)
                {
                    var approverStatus = (await _statusAppService.GetStatusByDisplayNames(StatusDisplayNames.Approved)).First();
                    request.StatusId = approverStatus.Status.Id;
                    status.StatusDisplayName = StatusDisplayNames.Approved;
                    status.AfterStatusMessage = approverStatus.Status.AfterStatusMessage;
                    status.StatusName = approverStatus.Status.DisplayName;
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_RestDayApprovals_Create)]
        private async Task CreateApproval(UpdateRestDayRequestsStatus input) =>
            await _restDayApprovalsRepository.InsertAsync(new RestDayApprovals
            {
                TenantId = AbpSession.TenantId.Value,
                Remarks = input.Reason,
                RestDayRequestId = input.Id,
                StatusId = input.StatusId,
            });

        private async Task SendApprovalDetails(RestDayRequests request, GetStatusForDisplayNameDto status, string approverName)
        {
            var dto = new EmailRequestDto
            {
                Request = L("RestDay"),
                FullName = approverName,
                ApprovalStatus = status.AfterStatusMessage,
                Remarks = request.Remarks,
                Reason = request.Reason,
                StartDate = request.RestDayStart,
                EndDate = request.RestDayEnd,
                Recipient = request.Employees.Email
            };

            await _approvalsAppService.SendApprovalDetails(dto);
        }
    }
}