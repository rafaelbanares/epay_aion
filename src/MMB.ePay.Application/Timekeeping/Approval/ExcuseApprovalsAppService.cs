﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Processor;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class ExcuseApprovalsAppService : ePayAppServiceBase, IExcuseApprovalsAppService
    {
        private readonly IRepository<ExcuseApprovals> _excuseApprovalsRepository;
        private readonly IRepository<ExcuseRequests> _excuseRequestsRepository;
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<EmployeeApprovers> _employeeApproversRepository;
        private readonly IRepository<WorkFlow> _workFlowRepository;
        private readonly IStatusAppService _statusAppService;
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;
        private readonly IProcessorAppService _processorAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public ExcuseApprovalsAppService(
            IRepository<ExcuseApprovals> excuseApprovalsRepository,
            IRepository<ExcuseRequests> excuseRequestsRepository,
            IRepository<Employees> employeesRepository,
            IRepository<EmployeeApprovers> employeeApproversRepository,
            IRepository<WorkFlow> workFlowRepository,
            IStatusAppService statusAppService,
            IAttendancePeriodsAppService attendancePeriodsAppService,
            IProcessorAppService processorAppService,
            IApprovalsAppService approvalsAppService
            )
        {
            _excuseApprovalsRepository = excuseApprovalsRepository;
            _excuseRequestsRepository = excuseRequestsRepository;
            _employeesRepository = employeesRepository;
            _employeeApproversRepository = employeeApproversRepository;
            _workFlowRepository = workFlowRepository;
            _statusAppService = statusAppService;
            _attendancePeriodsAppService = attendancePeriodsAppService;
            _processorAppService = processorAppService;
            _approvalsAppService = approvalsAppService;
        }

        public async Task<PagedResultDto<GetExcuseApprovalsForViewDto>> GetAll(GetAllExcuseApprovalsInput input)
        {
            var approverId = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => (int?)e.Id)
                .FirstOrDefaultAsync();

            if (approverId == null)
                return new PagedResultDto<GetExcuseApprovalsForViewDto>(0, new List<GetExcuseApprovalsForViewDto>());

            var filteredApprovals = _excuseRequestsRepository.GetAll()
                .WhereIf(input.EmployeeIdFilter != null, e => e.EmployeeId == input.EmployeeIdFilter)
                .WhereIf(input.FrequencyIdFilter > 0,
                    e => e.Employees.EmployeeTimeInfo.FrequencyId == input.FrequencyIdFilter)
                .Where(e => e.Employees.EmployeeApproversEmployee.Any(f => f.ApproverId == approverId)
                    && e.Status.AccessLevel ==
                        e.Employees.EmployeeApproversEmployee
                            .Where(f => f.ApproverId == approverId).First().ApproverOrders.Level
                    && input.IsBackup ==
                        e.Employees.EmployeeApproversEmployee
                            .Where(f => f.ApproverId == approverId).First().ApproverOrders.IsBackup);

            if (input.FrequencyIdFilter > 0)
            {
                var postedDate = await _attendancePeriodsAppService.GetLatestPostedDate(input.FrequencyIdFilter);
                filteredApprovals = filteredApprovals
                    .Where(e => postedDate != DateTime.MinValue && e.ExcuseDate > postedDate);
            }
            else
            {
                var minDate = await _attendancePeriodsAppService.GetMinUnpostedDate();
                filteredApprovals = filteredApprovals
                    .WhereIf(minDate != null && input.FrequencyIdFilter == 0,
                        e => e.ExcuseDate >= minDate);
            }

            return await GetPagedResult(filteredApprovals, input);
        }

        [AbpAuthorize(AppPermissions.Pages_ExcuseApprovals_Admin)]
        public async Task<PagedResultDto<GetExcuseApprovalsForViewDto>> GetAdminApprovals(GetAllExcuseApprovalsInput input)
        {
            var period = await _attendancePeriodsAppService
                .GetDateRangeFromAttendancePeriod(input.AttendancePeriodIdFilter.Value);

            if (period == null)
                return new PagedResultDto<GetExcuseApprovalsForViewDto>(0, new List<GetExcuseApprovalsForViewDto>());

            var filteredApprovals = _excuseRequestsRepository.GetAll()
                .WhereIf(input.EmployeeIdFilter != null, e => e.EmployeeId == input.EmployeeIdFilter)
                .WhereIf(input.FrequencyIdFilter > 0,
                    e => e.Employees.EmployeeTimeInfo.FrequencyId == input.FrequencyIdFilter)
                .WhereIf(input.StatusTypeIdFilter, e => e.Status.AccessLevel > 0)
                .WhereIf(!input.StatusTypeIdFilter, e => e.Status.AccessLevel == 0)
                .Where(e => e.ExcuseDate >= period.Start && e.ExcuseDate <= period.End);

            return await GetPagedResult(filteredApprovals, input);
        }

        public async Task<PagedResultDto<GetExcuseApprovalsForViewDto>> GetApprovalRequests(GetAllExcuseApprovalsInput input)
        {
            var period = await _attendancePeriodsAppService
                .GetDateRangeFromAttendancePeriod(input.AttendancePeriodIdFilter.Value);

            if (period == null)
                return new PagedResultDto<GetExcuseApprovalsForViewDto>(0, new List<GetExcuseApprovalsForViewDto>());

            var approverId = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => (int?)e.Id)
                .FirstOrDefaultAsync();

            if (approverId == null)
                return new PagedResultDto<GetExcuseApprovalsForViewDto>(0, new List<GetExcuseApprovalsForViewDto>());

            var filteredApprovals = _excuseRequestsRepository.GetAll()
                .WhereIf(input.EmployeeIdFilter != null, e => e.EmployeeId == input.EmployeeIdFilter)
                .Where(e => e.Employees.EmployeeApproversEmployee.Any(f => f.ApproverId == approverId)
                    && e.ExcuseDate >= period.Start && e.ExcuseDate <= period.End
                    && e.Status.AccessLevel !=
                        e.Employees.EmployeeApproversEmployee
                            .Where(f => f.ApproverId == approverId).First().ApproverOrders.Level
                    && input.IsBackup ==
                        e.Employees.EmployeeApproversEmployee
                            .Where(f => f.ApproverId == approverId).First().ApproverOrders.IsBackup);

            return await GetPagedResult(filteredApprovals, input);
        }

        private async Task<PagedResultDto<GetExcuseApprovalsForViewDto>> GetPagedResult(
            IQueryable<ExcuseRequests> filteredApprovals,
            GetAllExcuseApprovalsInput input)
        {
            var totalCount = await filteredApprovals.CountAsync();
            var results = await filteredApprovals
                .Where(e => e.TenantId == AbpSession.TenantId)
                .OrderBy(input.Sorting ?? "id desc")
                .PageBy(input)
                .Select(e => new GetExcuseApprovalsForViewDto
                {
                    ExcuseApprovals = new ExcuseApprovalsDto
                    {
                        EmployeeId = e.Employees.EmployeeCode,
                        Employee = e.Employees.FullName,
                        ExcuseDate = e.ExcuseDate,
                        ExcuseStart = e.ExcuseStart,
                        ExcuseEnd = e.ExcuseEnd,
                        Reason = e.ExcuseReasons.DisplayName,
                        Remarks = e.Remarks,
                        Status = e.Status.DisplayName,
                        StatusId = e.StatusId,
                        Id = e.Id
                    }
                }).ToListAsync();

            return new PagedResultDto<GetExcuseApprovalsForViewDto>(totalCount, results);
        }

        [AbpAuthorize(AppPermissions.Pages_ExcuseApprovals)]
        public async Task<GetExcuseApprovalsForViewDto> GetExcuseApprovalsForView(int id)
        {
            var request = await _excuseRequestsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new
                {
                    ExcuseDetails = new ExcuseDetailsDto
                    {
                        Employee = e.Employees.FullName,
                        ExcuseStart = e.ExcuseStart,
                        ExcuseEnd = e.ExcuseEnd,
                        Reason = e.ExcuseReasons.DisplayName,
                        Remarks = e.Remarks,
                        DateFiled = e.CreationTime,
                        Status = e.Status.DisplayName,
                        Id = e.Id
                    },
                    e.StatusId
                }).FirstOrDefaultAsync();

            request.ExcuseDetails.StatusList = await _workFlowRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.StatusId == request.StatusId && e.Status.AccessLevel > 0)
                .Select(e => new SelectListItem
                {
                    Value = e.NextStatus.ToString(),
                    Text = e.NextStatusNavigation.BeforeStatusActionText
                }).ToListAsync();

            return new GetExcuseApprovalsForViewDto { ExcuseDetails = request.ExcuseDetails };
        }

        public async Task UpdateStatus(UpdateExcuseRequestsStatus input) =>
            await UpdateStatusOrAdminUpdateStatus(input);

        [AbpAuthorize(AppPermissions.Pages_ExcuseApprovals_Admin)]
        public async Task AdminUpdateStatus(UpdateExcuseRequestsStatus input) =>
            await UpdateStatusOrAdminUpdateStatus(input);

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_ExcuseApprovals_Edit)]
        private async Task UpdateStatusOrAdminUpdateStatus(UpdateExcuseRequestsStatus input)
        {
            var status = await _statusAppService.GetStatusDisplayName(input.StatusId);

            if (status.StatusDisplayName is StatusDisplayNames.ForUpdate
                or StatusDisplayNames.Declined or StatusDisplayNames.Cancelled)
            {
                if (string.IsNullOrWhiteSpace(input.Reason))
                    throw new UserFriendlyException(L("YourRequestIsNotValid"), "The Remarks field is required.");
            }

            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
            var approver = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new { e.Id, e.FullName })
                .FirstOrDefaultAsync();

            var request = await GetRequestForApproval(input.Id, approver.Id);

            if (request != null)
            {
                request.StatusId = input.StatusId;
                request.Reason = input.Reason;

                await ChangeStatusForSingleApproval(status, request);
                await CreateApproval(input);

                if (status.StatusDisplayName is not StatusDisplayNames.Cancelled or StatusDisplayNames.Reopened)
                    await SendApprovalDetails(request, status.AfterStatusMessage, approver.FullName);

                if (status.StatusDisplayName == StatusDisplayNames.Approved)
                {
                    var periodId = await _attendancePeriodsAppService
                            .GetFirstUnpostedPeriodId(request.Employees.EmployeeTimeInfo.FrequencyId);

                    await _processorAppService.Process(periodId, request.EmployeeId);
                }
            }
            else
                throw new UserFriendlyException(L("YourRequestIsNotValid"));
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_ExcuseApprovals_Edit)]
        public async Task ApproveAll(ApproveAllExcuseRequests input)
        {
            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
            var approver = await _employeesRepository.GetAll()
                .Where(e => e.User.Id == AbpSession.UserId)
                .Select(e => new { e.Id, e.FullName }).FirstOrDefaultAsync();

            var frequencyIds = new List<int>();

            foreach (var id in input.Ids)
            {
                var request = await GetRequestForApproval(id, approver.Id);

                if (request != null)
                {
                    request.StatusId = await _workFlowRepository.GetAll()
                        .Where(e => e.StatusId == request.StatusId
                            && e.NextStatusNavigation.BeforeStatusActionText == "Approve")
                        .Select(e => e.NextStatusNavigation.Id)
                        .FirstOrDefaultAsync();

                    var status = await _statusAppService.GetStatusDisplayName(request.StatusId);
                    var approval = new UpdateExcuseRequestsStatus { Id = id, StatusId = request.StatusId };

                    await ChangeStatusForSingleApproval(status, request);
                    await CreateApproval(approval);

                    if (status.StatusDisplayName is not StatusDisplayNames.Cancelled or StatusDisplayNames.Reopened)
                        await SendApprovalDetails(request, status.AfterStatusMessage, approver.FullName);

                    if (status.StatusDisplayName == StatusDisplayNames.Approved)
                        if (!frequencyIds.Contains(request.Employees.EmployeeTimeInfo.FrequencyId))
                            frequencyIds.Add(request.Employees.EmployeeTimeInfo.FrequencyId);
                }
                else
                    throw new UserFriendlyException(L("YourRequestIsNotValid"));
            }

            foreach (var frequencyId in frequencyIds)
            {
                await UnitOfWorkManager.Current.SaveChangesAsync();

                var periodId = await _attendancePeriodsAppService.GetFirstUnpostedPeriodId(frequencyId);
                await _processorAppService.Process(periodId, null);
            }
        }

        private async Task<ExcuseRequests> GetRequestForApproval(int id, int? approverId)
        {
            var request = _excuseRequestsRepository.GetAll()
                .Include(e => e.Employees)
                    .ThenInclude(e => e.EmployeeTimeInfo)
                .Where(e => e.Id == id);

            if (approverId != null)
            {
                request = request
                    .Where(e => (e.Status.AccessLevel == 0 && e.EmployeeId == approverId)
                    || (e.Status.AccessLevel == e.Employees.EmployeeApproversEmployee
                        .Where(f => f.ApproverId == approverId).First().ApproverOrders.Level));
            }

            return await request.FirstOrDefaultAsync();
        }


        [AbpAuthorize(AppPermissions.Pages_LeaveApprovals_Edit)]
        protected async Task ChangeStatusForSingleApproval(GetStatusForDisplayNameDto status, ExcuseRequests request)
        {
            if (status.StatusDisplayName == StatusDisplayNames.ForNextApproval)
            {
                var hasOverLevel1 = await _employeeApproversRepository.GetAll()
                    .AnyAsync(e => e.EmployeeId == request.EmployeeId && e.ApproverOrders.Level > 1);

                if (!hasOverLevel1)
                {
                    var approverStatus = (await _statusAppService.GetStatusByDisplayNames(StatusDisplayNames.Approved)).First();
                    request.StatusId = approverStatus.Status.Id;
                    status.StatusDisplayName = StatusDisplayNames.Approved;
                    status.AfterStatusMessage = approverStatus.Status.AfterStatusMessage;
                    status.StatusName = approverStatus.Status.DisplayName;
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_ExcuseApprovals_Create)]
        protected async Task CreateApproval(UpdateExcuseRequestsStatus input)
        {
            var approvals = new ExcuseApprovals
            {
                TenantId = AbpSession.TenantId.Value,
                Remarks = input.Reason,
                ExcuseRequestId = input.Id,
                StatusId = input.StatusId,
            };

            await _excuseApprovalsRepository.InsertAsync(approvals);
        }

        private async Task SendApprovalDetails(ExcuseRequests request, string approvalStatus, string approverName)
        {
            var dto = new EmailRequestDto
            {
                Request = L("Excuse"),
                FullName = approverName,
                ApprovalStatus = approvalStatus,
                Remarks = request.Remarks,
                Reason = request.Reason,
                StartDate = request.ExcuseDate,
                Recipient = request.Employees.Email
            };

            await _approvalsAppService.SendApprovalDetails(dto);
        }
    }
}
