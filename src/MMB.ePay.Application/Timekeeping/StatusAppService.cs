﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class StatusAppService : ePayAppServiceBase, IStatusAppService
    {
        private readonly IRepository<Status> _statusRepository;

        public StatusAppService(IRepository<Status> statusRepository) =>
            _statusRepository = statusRepository;

        public async Task<IList<SelectListItem>> GetStatusListForFilter()
        {
            var status = await _statusRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.DisplayName
                }).ToListAsync();

            status.Insert(0, new SelectListItem { Value = "0", Text = "All" });

            return status;
        }

        public async Task<GetStatusForDisplayNameDto> GetStatusDisplayName(int id)
        {
            var status = await _statusRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetStatusForDisplayNameDto
                {
                    StatusName = e.DisplayName,
                    AfterStatusMessage = e.AfterStatusMessage
                }).FirstOrDefaultAsync();

            status.StatusDisplayName = status.StatusName switch
            {
                "New" => StatusDisplayNames.New,
                "For Approval" => StatusDisplayNames.ForApproval,
                "For Next Approval" => StatusDisplayNames.ForNextApproval,
                "Declined" => StatusDisplayNames.Declined,
                "Cancelled" => StatusDisplayNames.Cancelled,
                "Approved" => StatusDisplayNames.Approved,
                "Reopened" => StatusDisplayNames.Reopened,
                "For Update" => StatusDisplayNames.ForUpdate,
                _ => null,
            };

            return status;
        }

        public async Task<GetStatusForViewDto> GetInitialStatus()
        {
            var status = await _statusRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId 
                    && e.BeforeStatusActionText == null)
                .FirstOrDefaultAsync();

            var output = new GetStatusForViewDto { Status = ObjectMapper.Map<StatusDto>(status) };

            return output;
        }

        //FIX
        public async Task<IList<GetStatusForViewDto>> GetStatusByDisplayNames(params StatusDisplayNames[] names)
        {
            var stringNames = names.Select(e => e.ToString()).ToArray();

            var output = await _statusRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && stringNames.Contains(e.DisplayName))
                .Select(e => new GetStatusForViewDto
                {
                    Status = ObjectMapper.Map<StatusDto>(e)
                }).ToListAsync();

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Status_Edit)]
        public async Task<GetStatusForEditOutput> GetStatusForEdit(int id)
        {
            var status = await _statusRepository.FirstOrDefaultAsync(id);
            var output = new GetStatusForEditOutput { Status = ObjectMapper.Map<CreateOrEditStatusDto>(status) };

            return output;
        }
    }
}