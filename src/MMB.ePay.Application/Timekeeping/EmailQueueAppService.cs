﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public class EmailQueueAppService : ePayAppServiceBase, IEmailQueueAppService
    {
        private readonly IRepository<EmailQueue> _emailQueueRepository;
        private readonly IDbContextProvider<ePayDbContext> _dbContextProvider;

        public EmailQueueAppService(IRepository<EmailQueue> emailQueueRepository,
            IDbContextProvider<ePayDbContext> dbContextProvider)
        {
            _emailQueueRepository = emailQueueRepository;
            _dbContextProvider = dbContextProvider;
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Host_EmailQueue)]
        public async Task<DateTime?> GetOldestNewEmailQueue() =>
            await _emailQueueRepository.GetAll()
                .Where(e => e.Status == "P")
                .Select(e => e.StartDate)
                .OrderBy(e => e)
                .FirstOrDefaultAsync();

        [AbpAuthorize(AppPermissions.Pages_Administration_Host_EmailQueue)]
        public async Task<PagedResultDto<GetEmailQueueForViewDto>> GetAll(GetAllEmailQueueInput input)
        {
            var filteredEmailQueue = _emailQueueRepository.GetAll()
                .Where(e => e.TenantId == input.TenantFilter);

            var pagedAndFilteredEmailQueue = filteredEmailQueue
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var emailQueue = pagedAndFilteredEmailQueue
                .Select(e => new GetEmailQueueForViewDto
                {
                    EmailQueue = new EmailQueueDto
                    {
                        Request = e.Request,
                        FullName = e.FullName,
                        ApprovalStatus = e.ApprovalStatus,
                        Remarks = e.Remarks,
                        Reason = e.Reason,
                        StartDate = e.StartDate,
                        EndDate = e.EndDate,
                        Subject = e.Subject,
                        Recipient = e.Recipient,
                        Status = e.Status,
                        StatusError = e.StatusError,
                        Id = e.Id
                    }
                });

            var totalCount = await filteredEmailQueue.CountAsync();
            var results = await emailQueue.ToListAsync();

            results = results
                .Select(e =>
                {
                    var emailStatus = e.EmailQueue.Status;
                    e.EmailQueue.Status = emailStatus == "P" ? "Pending" :
                        emailStatus == "S" ? "Sent" : "Error";
                    return e;
                }).ToList();

            return new PagedResultDto<GetEmailQueueForViewDto>(
                totalCount,
                results
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Host_EmailQueue)]
        public async Task<GetEmailQueueForViewDto> GetEmailQueueForView(int id) =>
            await _emailQueueRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetEmailQueueForViewDto
                {
                    EmailQueue = new EmailQueueDto
                    {
                        Request = e.Request,
                        FullName = e.FullName,
                        ApprovalStatus = e.ApprovalStatus,
                        Remarks = e.Remarks,
                        Reason = e.Reason,
                        StartDate = e.StartDate,
                        EndDate = e.EndDate,
                        Subject = e.Subject,
                        Recipient = e.Recipient,
                        Status = e.Status,
                        StatusError = e.StatusError,
                        Id = e.Id
                    }
                }).FirstOrDefaultAsync();

        [UnitOfWork]
        public async Task<IList<EmailQueueDto>> GetPendingAsync()
        {
            CurrentUnitOfWork.SetTenantId(null);

            return await _emailQueueRepository.GetAll()
                .Where(e => e.Status == "P")
                .OrderBy(e => e.CreationTime)
                .Take(20)
                .Select(e => new EmailQueueDto
                {
                    Id = e.Id,
                    TenantId = e.TenantId,
                    Request = e.Request,
                    FullName = e.FullName,
                    ApprovalStatus = e.ApprovalStatus,
                    Remarks = e.Remarks,
                    Reason = e.Reason,
                    StartDate = e.StartDate,
                    EndDate = e.EndDate,
                    Subject = e.Subject,
                    Recipient = e.Recipient,
                    Status = e.Status
                }).ToListAsync();
        }

        [UnitOfWork]
        public async Task UpdateStatusAsync(UpdateStatusEmailQueueDto input)
        {
            CurrentUnitOfWork.SetTenantId(null);

            var emailQueue = await _emailQueueRepository.GetAll()
                .Where(e => e.Id == input.Id)
                .FirstOrDefaultAsync();

            if (string.IsNullOrWhiteSpace(input.StatusError))
            {
                emailQueue.Status = "S";
            }
            else
            {
                emailQueue.StatusError = input.StatusError;
                emailQueue.Status = "X";
            }
        }

        [UnitOfWork]
        public async Task CreateOrEdit(CreateOrEditEmailQueueDto input)
        {
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);

            await UnitOfWorkManager.Current.SaveChangesAsync();
            CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
        }

        [UnitOfWork]
        protected virtual async Task Create(CreateOrEditEmailQueueDto input)
        {
            CurrentUnitOfWork.SetTenantId(null);

            var emailQueue = ObjectMapper.Map<EmailQueue>(input);

            if (AbpSession.TenantId != null)
            {
                emailQueue.TenantId = AbpSession.TenantId.Value;
            }

            await _emailQueueRepository.InsertAsync(emailQueue);
        }

        [UnitOfWork]
        protected virtual async Task Update(CreateOrEditEmailQueueDto input)
        {
            CurrentUnitOfWork.SetTenantId(null);

            var emailQueue = await _emailQueueRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, emailQueue);
        }

        [UnitOfWork]
        public async Task DeleteAll(DeleteEmailQueueDto input)
        {
            CurrentUnitOfWork.SetTenantId(null);

            var emailQueue = await _emailQueueRepository.GetAll()
                .Where(e => e.Status == "P" 
                    && input.StartDate <= e.StartDate && input.EndDate >= e.StartDate)
                .ToListAsync();

            var dbContext = _dbContextProvider.GetDbContext();
            dbContext.EmailQueue.RemoveRange(emailQueue);
        }
    }
}