﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class HolidaysAppService : ePayAppServiceBase, IHolidaysAppService
    {
        private readonly IRepository<Holidays> _holidaysRepository;
        private readonly IAttendancePeriodsAppService _attendancePeriodsManager;

        public HolidaysAppService(IRepository<Holidays> holidaysRepository,
            IAttendancePeriodsAppService attendancePeriodsManager)
        {
            _attendancePeriodsManager = attendancePeriodsManager;
            _holidaysRepository = holidaysRepository;
        }

        public async Task<IList<SelectListItem>> GetYearListForFilter()
        {
            var currentYear = DateTime.Now.Year;
            var appYears = await _holidaysRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => e.Year)
                .Distinct()
                .ToListAsync();

            if (appYears.Count == 0)
                appYears.Add(currentYear);

            var output = appYears.Select(e => new SelectListItem
            {
                Value = e.ToString(),
                Text = e.ToString(),
                Selected = e == currentYear
            }).OrderByDescending(e => e.Value)
            .ToList();

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Holidays)]
        public async Task<PagedResultDto<GetHolidaysForViewDto>> GetAll(GetAllHolidaysInput input)
        {
            var filteredHolidays = _holidaysRepository.GetAll()
                .WhereIf(input.YearFilter > 0, e => e.Date.Year == input.YearFilter)
                .Where(e => e.TenantId == AbpSession.TenantId);

            var pagedAndFilteredHolidays = filteredHolidays
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var postedDate = await _attendancePeriodsManager.GetLatestPostedDate(null);
            var holidays = pagedAndFilteredHolidays
                .Select(o => new GetHolidaysForViewDto
                {
                    Holidays = new HolidaysDto
                    {
                        DisplayName = o.DisplayName,
                        Date = o.Date,
                        HolidayType = o.HolidayTypes.DisplayName,
                        HalfDay = o.HalfDay,
                        Location = o.Locations.Description,
                        IsEditable = o.Date > postedDate,
                        Id = o.Id,
                    }
                });

            var totalCount = await filteredHolidays.CountAsync();
            var results = await holidays.ToListAsync();

            return new PagedResultDto<GetHolidaysForViewDto>(
                totalCount,
                results
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Holidays)]
        public async Task<GetHolidaysForViewDto> GetHolidaysForView(int id) =>
            await _holidaysRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetHolidaysForViewDto
                {
                    Holidays = new HolidaysDto
                    {
                        DisplayName = e.DisplayName,
                        Date = e.Date,
                        HalfDay = e.HalfDay,
                        HolidayType = e.HolidayTypes.DisplayName,
                        Location = e.Locations.Description
                    }
                }).FirstOrDefaultAsync();

        [AbpAuthorize(AppPermissions.Pages_Holidays_Edit)]
        public async Task<GetHolidaysForEditOutput> GetHolidaysForEdit(EntityDto input)
        {
            var holidays = await _holidaysRepository.FirstOrDefaultAsync(input.Id);

            return new GetHolidaysForEditOutput 
            { 
                Holidays = ObjectMapper.Map<CreateOrEditHolidaysDto>(holidays) 
            };
        }

        public async Task CreateOrEdit(CreateOrEditHolidaysDto input)
        {
            input.LocationId = (input.LocationId == 0 ? null : input.LocationId);

            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        protected virtual async Task Validate(CreateOrEditHolidaysDto input)
        {
            if (await _holidaysRepository.GetAll()
                .AnyAsync(e => e.TenantId == AbpSession.TenantId && e.Date == input.Date && e.Id != input.Id))
                throw new UserFriendlyException("Your request is not valid!", "An identical record already exists.");

            if (await _holidaysRepository.GetAll()
                .AnyAsync(e => e.TenantId == AbpSession.TenantId
                    && e.Year == input.Date.Value.Year
                    && e.DisplayName == input.DisplayName
                    && e.Id != input.Id))
                throw new UserFriendlyException("Your request is not valid!", "An identical record already exists.");
        }

        [AbpAuthorize(AppPermissions.Pages_Holidays_Create)]
        protected virtual async Task Create(CreateOrEditHolidaysDto input)
        {
            var holidays = ObjectMapper.Map<Holidays>(input);

            await Validate(input);
            await _holidaysRepository.InsertAsync(holidays);
        }

        [AbpAuthorize(AppPermissions.Pages_Holidays_Edit)]
        protected virtual async Task Update(CreateOrEditHolidaysDto input)
        {
            var holidays = await _holidaysRepository.FirstOrDefaultAsync((int)input.Id);

            await Validate(input);
            ObjectMapper.Map(input, holidays);
        }

        [AbpAuthorize(AppPermissions.Pages_Holidays_Delete)]
        public async Task Delete(EntityDto input) =>
            await _holidaysRepository.DeleteAsync(input.Id);
    }
}