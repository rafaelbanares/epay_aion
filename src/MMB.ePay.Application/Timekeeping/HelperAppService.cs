﻿using Abp.Application.Services;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    [RemoteService(false)]
    public class HelperAppService : ePayAppServiceBase, IHelperAppService
    {
        public IList<SelectListItem> GetYearList()
        {
            var currentYear = DateTime.Now.Year;
            var nextYear = currentYear + 1;
            var minYear = currentYear - 3;

            var appYears = Enumerable.Range(minYear, nextYear - minYear + 1)
                .Select(e => new SelectListItem
                {
                    Value = e.ToString(),
                    Text = e.ToString(),
                    Selected = e == currentYear
                }).OrderByDescending(e => e.Text)
                .ToList();

            return appYears;
        }

        public IList<SelectListItem> GetApproverTypeList() =>
            new List<SelectListItem>
            {
                new SelectListItem { Text = "Main Approver", Value = false.ToString() },
                new SelectListItem { Text = "Backup Approver", Value = true.ToString() }
            };

        public IList<SelectListItem> GetSubTypeList() =>
            new List<SelectListItem>
            {
                new SelectListItem { Value = "1", Text = "First 8" },
                new SelectListItem { Value = "2", Text = "> 8" },
                new SelectListItem { Value = "3", Text = "ND1" },
                new SelectListItem { Value = "4", Text = "ND2" }
            };

        public IList<SelectListItem> GetCoveringPeriodList() =>
            new List<SelectListItem>
                {
                    new SelectListItem { Value = "P",  Text = "Periodic" },
                    new SelectListItem { Value = "M",  Text = "Monthly" }
                };

        public IList<SelectListItem> GetPostList() =>
            new List<SelectListItem>
                {
                    new SelectListItem { Value = "true",  Text = "Posted" },
                    new SelectListItem { Value = "false",  Text = "Unposted" }
                };

        public IList<SelectListItem> GetStatusTypeList() =>
            new List<SelectListItem>
            {
                new SelectListItem { Text = "Pending", Value = true.ToString(), },
                new SelectListItem { Text = "Other", Value = false.ToString() }
            };

        public IList<SelectListItem> GetRankList()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = string.Empty, Text = "Select" },
                new SelectListItem { Value = "R&F", Text = "Rank & File" },
                new SelectListItem { Value = "SUP", Text = "Supervisor" },
                new SelectListItem { Value = "MGR", Text = "Manager" },
                new SelectListItem { Value = "EXC", Text = "Executive" },
                new SelectListItem { Value = "CON", Text = "Consultant" }
            };
        }

        public IList<SelectListItem> GetEmployeeStatusList()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = "ACTIVE", Text = "ACTIVE" },
                new SelectListItem { Value = "RESIGNED", Text = "RESIGNED" },
                new SelectListItem { Value = "HOLD", Text = "HOLD" }
            };
        }

        public IList<SelectListItem> GetEmployeeStatusListForFilter()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Value = string.Empty, Text = "All" },
                new SelectListItem { Value = "ACTIVE", Text = "ACTIVE" },
                new SelectListItem { Value = "RESIGNED", Text = "RESIGNED" },
                new SelectListItem { Value = "HOLD", Text = "HOLD" }
            };
        }

        public IList<SelectListItem> GetMonthList()
        {
            var appMonths = Enumerable.Range(1, 12)
                .Select(e => new SelectListItem
                {
                    Value = e.ToString(),
                    Text = DateTimeFormatInfo.CurrentInfo.GetMonthName(e)
                }).ToList();

            appMonths.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return appMonths;
        }

        public IList<SelectListItem> GetMonthListForFilter()
        {
            var appMonths = Enumerable.Range(1, 12)
                .Select(e => new SelectListItem
                {
                    Value = e.ToString(),
                    Text = DateTimeFormatInfo.CurrentInfo.GetMonthName(e)
                }).ToList();

            appMonths.Insert(0, new SelectListItem { Value = "0", Text = "All" });

            return appMonths;
        }

        public string GetRankById(string id)
        {
            return id switch
            {
                "R&F" => "Rank & File",
                "SUP" => "Supervisor",
                "MGR" => "Manager",
                "EXC" => "Executive",
                "CON" => "Consultant",
                _ => string.Empty,
            };
        }

        public decimal GetMinuteFormat(decimal minutes)
        {
            int newHours = (int)(minutes / 60);
            var newMinutes = minutes % 60;

            return newHours + newMinutes / 100;
        }

        public string GetMinuteFormatString(decimal minutes)
        {
            int newHours = (int)(minutes / 60);
            var newMinutes = minutes % 60;
            var output = string.Empty;

            if (newHours > 0)
                output += newHours + " hour(s) ";

            if (newMinutes > 0)
                output += newMinutes + " minute(s)";

            return output;
        }
    }
}