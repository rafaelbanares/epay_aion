﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class LocationsAppService : ePayAppServiceBase, ILocationsAppService
    {
        private readonly IRepository<Locations> _locationsRepository;

        public LocationsAppService(IRepository<Locations> locationsRepository) =>
            _locationsRepository = locationsRepository;

        public async Task<IList<SelectListItem>> GetLocationList()
        {
            var locationList = await _locationsRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.Description
                }).ToListAsync();

            locationList.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return locationList;
        }

        [AbpAuthorize(AppPermissions.Pages_Locations)]
        public async Task<PagedResultDto<GetLocationsForViewDto>> GetAll(GetAllLocationsInput input)
        {
            var filteredLocations = _locationsRepository.GetAll();

            var totalCount = await filteredLocations.CountAsync();
            var results = await filteredLocations
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input)
                .Select(o => new GetLocationsForViewDto
                {
                    Locations = new LocationsDto
                    {
                        Description = o.Description,
                        Id = o.Id,
                    }
                }).ToListAsync();

            return new PagedResultDto<GetLocationsForViewDto>(totalCount, results);
        }

        [AbpAuthorize(AppPermissions.Pages_Locations)]
        public async Task<GetLocationsForViewDto> GetLocationsForView(int id) =>
            await _locationsRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetLocationsForViewDto
                {
                    Locations = new LocationsDto { Description = e.Description }
                }).FirstOrDefaultAsync();

        [AbpAuthorize(AppPermissions.Pages_Locations_Edit)]
        public async Task<GetLocationsForEditOutput> GetLocationsForEdit(int id)
        {
            var locations = await _locationsRepository.FirstOrDefaultAsync(id);

            return new GetLocationsForEditOutput 
            { 
                Locations = ObjectMapper.Map<CreateOrEditLocationsDto>(locations) 
            };
        }

        public async Task CreateOrEdit(CreateOrEditLocationsDto input)
        {
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        [AbpAuthorize(AppPermissions.Pages_Locations_Create)]
        private async Task Create(CreateOrEditLocationsDto input)
        {
            var locations = ObjectMapper.Map<Locations>(input);

            await Validate(input);
            await _locationsRepository.InsertAsync(locations);
        }

        [AbpAuthorize(AppPermissions.Pages_Locations_Edit)]
        private async Task Update(CreateOrEditLocationsDto input)
        {
            var locations = await _locationsRepository.FirstOrDefaultAsync((int)input.Id);

            await Validate(input);
            ObjectMapper.Map(input, locations);
        }

        private async Task Validate(CreateOrEditLocationsDto input)
        {
            if (await _locationsRepository.GetAll()
                .AnyAsync(e => e.TenantId == AbpSession.TenantId && e.Description == input.Description))
                    throw new UserFriendlyException("Your request is not valid!", "An identical record already exists.");
        }

        [AbpAuthorize(AppPermissions.Pages_Locations_Delete)]
        public async Task Delete(int id) =>
            await _locationsRepository.DeleteAsync(id);
    }
}