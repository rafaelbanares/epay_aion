﻿using Abp.Data;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.MultiTenancy;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping.Dtos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public class StoredProceduresAppService : ePayAppServiceBase, IStoredProceduresAppService
    {
        private readonly IActiveTransactionProvider _transactionProvider;
        private readonly IDbContextProvider<ePayDbContext> _dbContextProvider;

        public StoredProceduresAppService(
            IDbContextProvider<ePayDbContext> dbContextProvider,
            IActiveTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
            _dbContextProvider = dbContextProvider;
        }

        protected DbCommand CreateCommand(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            var command = _dbContextProvider.GetDbContext().Database.GetDbConnection().CreateCommand();

            command.CommandText = commandText;
            command.CommandType = commandType;
            command.Transaction = GetActiveTransaction();

            foreach (var parameter in parameters)
                command.Parameters.Add(parameter);

            return command;
        }

        protected async Task EnsureConnectionOpenAsync()
        {
            var connection = _dbContextProvider.GetDbContext().Database.GetDbConnection();

            if (connection.State != ConnectionState.Open)
                await connection.OpenAsync();
        }

        protected DbTransaction GetActiveTransaction() =>
            (DbTransaction)_transactionProvider.GetActiveTransaction(new ActiveTransactionProviderArgs
            {
                {"ContextType", typeof(ePayDbContext) },
                {"MultiTenancySide", MultiTenancySides.Tenant }
            });

        public async Task<List<string>> GetUserNames()
        {
            await EnsureConnectionOpenAsync();

            using var command = CreateCommand("GetUserNames", CommandType.StoredProcedure);
            using var dataReader = await command.ExecuteReaderAsync();
            var result = new List<string>();

            while (dataReader.Read())
                result.Add(dataReader["UserName"].ToString());

            return result;
        }

        [UnitOfWork]
        public async Task<int> InitializeProcSession(InitializeProcSessionDto input)
        {
            await EnsureConnectionOpenAsync();

            var parameters = new SqlParameter[]
            {
                new SqlParameter("AppSessionId", input.AppSessionId),
                new SqlParameter("AttendancePeriodId", input.AttendancePeriodId),
                new SqlParameter("TenantId", input.TenantId),
                new SqlParameter("EmployeeId", input.EmployeeId)
            };

            using var command = CreateCommand("tk.usp_InitializeProcSession", CommandType.StoredProcedure, parameters);
            using var dataReader = await command.ExecuteReaderAsync();
            int error = 0;

            while (dataReader.Read())
                error = (int)dataReader[0];

            return error;
        }

        [UnitOfWork]
        public async Task<int> PostAttendance(PostAttendanceDto input)
        {
            await EnsureConnectionOpenAsync();

            var parameters = new SqlParameter[]
            {
                new SqlParameter("AttendancePeriodId", input.AttendancePeriodId),
                new SqlParameter("TenantId", input.TenantId)
            };

            using var command = CreateCommand("tk.usp_PostAttendance", CommandType.StoredProcedure, parameters);
            var output = await command.ExecuteNonQueryAsync();

            return output;
        }

        [UnitOfWork]
        public async Task<Guid> GetProcessBatch(Guid appSessionId)
        {
            await EnsureConnectionOpenAsync();

            SqlParameter[] sqlParams = {
                new("@AppSessionId", SqlDbType.UniqueIdentifier) { Value = appSessionId, Direction = ParameterDirection.Input },
                new("@BatchId", SqlDbType.UniqueIdentifier) { Value = null, Direction = ParameterDirection.Output }
            };

            using var command = CreateCommand("[tk].usp_GetProcessBatch", CommandType.StoredProcedure, sqlParams);
            await command.ExecuteNonQueryAsync();

            if (command.Parameters["@BatchId"]?.Value == DBNull.Value)
                return Guid.Empty;
            else
                return (Guid)command.Parameters["@BatchId"].Value;
        }

        [UnitOfWork]
        public async Task<int> DeleteEmail()
        {
            await EnsureConnectionOpenAsync();

            using var command = CreateCommand("tk.usp_DeleteEmail", CommandType.StoredProcedure);
            using var dataReader = await command.ExecuteReaderAsync();
            int error = 0;

            while (dataReader.Read())
                error = (int)dataReader[0];

            return error;
        }

        [UnitOfWork]
        public async Task<int> DeleteAuditLogs()
        {
            await EnsureConnectionOpenAsync();

            using var command = CreateCommand("tk.usp_DeleteAuditLogs", CommandType.StoredProcedure);
            using var dataReader = await command.ExecuteReaderAsync();
            int error = 0;

            while (dataReader.Read())
                error = (int)dataReader[0];

            return error;
        }

        [UnitOfWork]
        public async Task<int> DeleteProcess(DeleteProcessDto input)
        {
            await EnsureConnectionOpenAsync();

            var parameters = new SqlParameter[]
            {
                new SqlParameter("AttendancePeriodId", input.AttendancePeriodId),
                new SqlParameter("TenantId", input.TenantId),
                new SqlParameter("EmployeeId", input.EmployeeId)
            };

            using var command = CreateCommand("tk.usp_DeleteProcess", CommandType.StoredProcedure, parameters);
            using var dataReader = await command.ExecuteReaderAsync();
            int error = 0;

            while (dataReader.Read())
                error = (int)dataReader[0];

            return error;
        }


        [UnitOfWork]
        public async Task ProcessRawTimeLogStaging(Guid uploadSessionId)
        {
            await EnsureConnectionOpenAsync();

            var parameters = new SqlParameter[]
            {
                new SqlParameter("UploadSessionId", uploadSessionId)
            };

            using var command = CreateCommand("tk.usp_ProcessRawTimeLogStaging", CommandType.StoredProcedure, parameters);
            using var dataReader = await command.ExecuteReaderAsync();
        }

        [UnitOfWork]
        public async Task ProcessSessionCleanup(Guid appSessionId)
        {
            await EnsureConnectionOpenAsync();

            var sql = "DELETE b " +
                        "FROM [tk].ProcSessions a INNER JOIN [tk].[ProcSessionBatches] b " +
                        "ON a.Id = b.PsId " +
                        "WHERE a.AppSessionId = @AppSessionId ";

            SqlParameter[] sqlParams = {
                new("@AppSessionId", SqlDbType.UniqueIdentifier) { Value = appSessionId, Direction = ParameterDirection.Input }
            };

            using var command = CreateCommand(sql, CommandType.Text, sqlParams);
            await command.ExecuteNonQueryAsync();            
        }
    }
}
