﻿using Abp.Data;
using Abp.EntityFrameworkCore;
using Abp.MultiTenancy;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public class StoredProcedureReportsAppService : ePayAppServiceBase, IStoredProcedureReportsAppService
    {
        private readonly IActiveTransactionProvider _transactionProvider;
        private readonly IDbContextProvider<ePayDbContext> _dbContextProvider;

        public StoredProcedureReportsAppService(
            IDbContextProvider<ePayDbContext> dbContextProvider,
            IActiveTransactionProvider transactionProvider)
        {
            _transactionProvider = transactionProvider;
            _dbContextProvider = dbContextProvider;
        }

        protected DbCommand CreateCommand(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            var command = _dbContextProvider.GetDbContext().Database.GetDbConnection().CreateCommand();

            command.CommandText = commandText;
            command.CommandType = commandType;
            command.Transaction = GetActiveTransaction();

            foreach (var parameter in parameters)
                command.Parameters.Add(parameter);

            return command;
        }

        protected async Task EnsureConnectionOpenAsync()
        {
            var connection = _dbContextProvider.GetDbContext().Database.GetDbConnection();

            if (connection.State != ConnectionState.Open)
                await connection.OpenAsync();
        }

        protected DbTransaction GetActiveTransaction() =>
            (DbTransaction)_transactionProvider.GetActiveTransaction(new ActiveTransactionProviderArgs
            {
                {"ContextType", typeof(ePayDbContext) },
                {"MultiTenancySide", MultiTenancySides.Tenant }
            });

        public async Task<GetReportsForAttendanceSummary> GetAttendanceSummary(ReportsDto dto)
        {
            await EnsureConnectionOpenAsync();

            var parameters = new SqlParameter[]
            {
                new("EmployeeId", dto.EmployeeId),
                new("Rank", dto.Rank),
                new("FrequencyFilterId", dto.FrequencyFilterId),
                new("MinDate", dto.MinDate),
                new("MaxDate", dto.MaxDate),
                new("PostFilterId", dto.PostFilterId)
            };

            using var command = CreateCommand("[tk]." + dto.StoredProcedure, CommandType.StoredProcedure, parameters);
            using var dataReader = await command.ExecuteReaderAsync();
            var attendanceSummaries = new List<AttendanceSummaryTable>();

            while (dataReader.Read())
            {
                var summary = new AttendanceSummaryTable
                {
                    EmployeeId = dataReader.GetString(dataReader.GetOrdinal("EmployeeId")),
                    EmployeeName = dataReader.GetString(dataReader.GetOrdinal("EmployeeName")),
                    RegDays = dataReader.GetDecimal(dataReader.GetOrdinal("RegDays")),
                    RegNd1Mins = dataReader.GetDecimal(dataReader.GetOrdinal("RegNd1Mins")),
                    RegNd2Mins = dataReader.GetDecimal(dataReader.GetOrdinal("RegNd2Mins")),
                    UTMins = dataReader.GetDecimal(dataReader.GetOrdinal("UTMins")),
                    TardyMins = dataReader.GetDecimal(dataReader.GetOrdinal("TardyMins")),
                    PaidHoliday = dataReader.GetDecimal(dataReader.GetOrdinal("PaidHoliday")),
                    Absence = dataReader.GetDecimal(dataReader.GetOrdinal("Absence"))
                };

                attendanceSummaries.Add(summary);
            }

            return new GetReportsForAttendanceSummary
            {
                AttendanceSummary = attendanceSummaries
            };
        }

        public async Task<GetReportsForAttendanceSummaryPayroll> GetAttendanceSummaryPayroll(ReportsDto dto)
        {
            await EnsureConnectionOpenAsync();

            var parameters = new SqlParameter[]
            {
                new("EmployeeId", dto.EmployeeId),
                new("Rank", dto.Rank),
                new("FrequencyFilterId", dto.FrequencyFilterId),
                new("MinDate", dto.MinDate),
                new("MaxDate", dto.MaxDate),
                new("PostFilterId", dto.PostFilterId)
            };

            using var command = CreateCommand("[tk]." + dto.StoredProcedure, CommandType.StoredProcedure, parameters);
            using var dataReader = await command.ExecuteReaderAsync();
            var attendanceSummaryPayrolls = new List<AttendanceSummaryPayrollTable>();

            while (dataReader.Read())
                attendanceSummaryPayrolls.Add(new AttendanceSummaryPayrollTable
                {
                    TenantId = dataReader.GetString(dataReader.GetOrdinal("TenantId")),
                    EmployeeId = dataReader.GetString(dataReader.GetOrdinal("EmployeeId")),
                    LastName = dataReader.GetString(dataReader.GetOrdinal("LastName")),
                    FirstName = dataReader.GetString(dataReader.GetOrdinal("FirstName")),
                    Paycode = dataReader.GetString(dataReader.GetOrdinal("Paycode")),
                    Department = dataReader.GetString(dataReader.GetOrdinal("Department")),
                    RegDays = dataReader.GetDecimal(dataReader.GetOrdinal("RegDays")),
                    RegHrs = dataReader.GetDecimal(dataReader.GetOrdinal("RegHrs")),
                    AbsDays = dataReader.GetDecimal(dataReader.GetOrdinal("AbsDays")),
                    AbsHrs = dataReader.GetDecimal(dataReader.GetOrdinal("AbsHrs")),
                    TardyHrs = dataReader.GetDecimal(dataReader.GetOrdinal("TardyHrs")),
                    UTHrs = dataReader.GetDecimal(dataReader.GetOrdinal("UTHrs")),
                    ND1Hrs = dataReader.GetDecimal(dataReader.GetOrdinal("ND1Hrs")),
                    ND2Hrs = dataReader.GetDecimal(dataReader.GetOrdinal("ND2Hrs")),
                    SLDays = dataReader.GetDecimal(dataReader.GetOrdinal("SLDays")),
                    VLDays = dataReader.GetDecimal(dataReader.GetOrdinal("VLDays")),
                    PaidHoliday = dataReader.GetDecimal(dataReader.GetOrdinal("PaidHoliday"))
                });

            return new GetReportsForAttendanceSummaryPayroll
            {
                AttendanceSummaryPayroll = attendanceSummaryPayrolls
            };
        }

        public async Task<GetReportsForOvertimeSummary> GetOvertimeSummary(ReportsDto dto)
        {
            await EnsureConnectionOpenAsync();

            var parameters = new SqlParameter[]
            {
                new("EmployeeId", dto.EmployeeId),
                new("Rank", dto.Rank),
                new("FrequencyFilterId", dto.FrequencyFilterId),
                new("MinDate", dto.MinDate),
                new("MaxDate", dto.MaxDate),
                new("PostFilterId", dto.PostFilterId)
            };

            using var command = CreateCommand("[tk]." + dto.StoredProcedure, CommandType.StoredProcedure, parameters);
            using var dataReader = await command.ExecuteReaderAsync();
            var attendanceSummaries = new List<OvertimeSummaryTable>();

            while (dataReader.Read())
            {
                attendanceSummaries.Add(new OvertimeSummaryTable
                {
                    EmployeeId = dataReader.GetString(dataReader.GetOrdinal("EmployeeId")),
                    EmployeeName = dataReader.GetString(dataReader.GetOrdinal("EmployeeName")),
                    RegularOT = dataReader.IsDBNull(dataReader.GetOrdinal("RegularOT")) ? 0m : 
                        dataReader.GetDecimal(dataReader.GetOrdinal("RegularOT")),
                    RegularOT8 = dataReader.IsDBNull(dataReader.GetOrdinal("RegularOT8")) ? 0m : 
                        dataReader.GetDecimal(dataReader.GetOrdinal("RegularOT8")),
                    RegularOTND1 = dataReader.IsDBNull(dataReader.GetOrdinal("RegularOTND1")) ? 0m : 
                        dataReader.GetDecimal(dataReader.GetOrdinal("RegularOTND1")),
                    RegularOTND2 = dataReader.IsDBNull(dataReader.GetOrdinal("RegularOTND2")) ? 0m : 
                        dataReader.GetDecimal(dataReader.GetOrdinal("RegularOTND2")),
                    RestDayOT = dataReader.IsDBNull(dataReader.GetOrdinal("RestDayOT")) ? 0m : 
                        dataReader.GetDecimal(dataReader.GetOrdinal("RestDayOT")),
                    RestDayOT8 = dataReader.IsDBNull(dataReader.GetOrdinal("RestDayOT8")) ? 0m : 
                        dataReader.GetDecimal(dataReader.GetOrdinal("RestDayOT8"))
                });
            }

            return new GetReportsForOvertimeSummary
            {
                OvertimeSummary = attendanceSummaries
            };
        }

        public async Task<GetReportsForOvertimeSummaryPayroll> GetOvertimeSummaryPayroll(ReportsDto dto)
        {
            await EnsureConnectionOpenAsync();

            var parameters = new SqlParameter[]
            {
                new("EmployeeId", dto.EmployeeId),
                new("Rank", dto.Rank),
                new("FrequencyFilterId", dto.FrequencyFilterId),
                new("MinDate", dto.MinDate),
                new("MaxDate", dto.MaxDate),
                new("PostFilterId", dto.PostFilterId)
            };

            using var command = CreateCommand("[tk]." + dto.StoredProcedure, CommandType.StoredProcedure, parameters);
            using var dataReader = await command.ExecuteReaderAsync();
            var overtimeSummaryPayrolls = new List<OvertimeSummaryPayrollTable>();

            while (dataReader.Read())
                overtimeSummaryPayrolls.Add(new OvertimeSummaryPayrollTable
                {
                    TenantId = dataReader.GetString(dataReader.GetOrdinal("TenantId")),
                    EmployeeId = dataReader.GetString(dataReader.GetOrdinal("EmployeeId")),
                    LastName = dataReader.GetString(dataReader.GetOrdinal("LastName")),
                    FirstName = dataReader.GetString(dataReader.GetOrdinal("FirstName")),
                    Paycode = dataReader.GetString(dataReader.GetOrdinal("Paycode")),
                    Department = dataReader.GetString(dataReader.GetOrdinal("Department")),
                    RegularOT = dataReader.GetDecimal(dataReader.GetOrdinal("RegularOT")),
                    RegularOT8 = dataReader.GetDecimal(dataReader.GetOrdinal("RegularOT8")),
                    RegularOTND1 = dataReader.GetDecimal(dataReader.GetOrdinal("RegularOTND1")),
                    RegularOTND2 = dataReader.GetDecimal(dataReader.GetOrdinal("RegularOTND2")),
                    RestDayOT = dataReader.GetDecimal(dataReader.GetOrdinal("RestDayOT")),
                    RestDayOT8 = dataReader.GetDecimal(dataReader.GetOrdinal("RestDayOT8"))
                });

            return new GetReportsForOvertimeSummaryPayroll
            {
                OvertimeSummaryPayroll = overtimeSummaryPayrolls
            };
        }

        public async Task<GetReportsForOvertimeTransaction> GetOvertimeTransaction(ReportsDto dto)
        {
            await EnsureConnectionOpenAsync();

            var parameters = new SqlParameter[]
            {
                new("EmployeeId", dto.EmployeeId),
                new("Rank", dto.Rank),
                new("FrequencyFilterId", dto.FrequencyFilterId),
                new("MinDate", dto.MinDate),
                new("MaxDate", dto.MaxDate),
            };

            using var command = CreateCommand("[tk]." + dto.StoredProcedure, CommandType.StoredProcedure, parameters);
            using var dataReader = await command.ExecuteReaderAsync();
            var overtimeTransactions = new List<OvertimeTransactionTable>();

            while (dataReader.Read())
                overtimeTransactions.Add(new OvertimeTransactionTable
                {
                    EmployeeId = dataReader.GetString(dataReader.GetOrdinal("EmployeeId")),
                    EmployeeName = dataReader.GetString(dataReader.GetOrdinal("EmployeeName")),
                    Reason = dataReader.GetString(dataReader.GetOrdinal("Reason")),
                    Status = dataReader.GetString(dataReader.GetOrdinal("Status")),
                    AttendanceDate = dataReader.GetDateTime(dataReader.GetOrdinal("AttendanceDate")),
                    DateFiled = dataReader.GetDateTime(dataReader.GetOrdinal("DateFiled")),
                    OTStart = dataReader.GetDateTime(dataReader.GetOrdinal("OTStart")),
                    OTEnd = dataReader.GetDateTime(dataReader.GetOrdinal("OTEnd"))
                });

            return new GetReportsForOvertimeTransaction
            {
                OvertimeTransaction = overtimeTransactions
            };
        }
    }
}
