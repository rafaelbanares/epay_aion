﻿using System;
using System.Collections.Generic;
using Tk.Domain;

namespace Tk
{
    public static class Core
    {
        public static IEmployeeShift GetProjectedShift(Attendance attend, IEmployeeShift employeeShift)
        {

            IEmployeeShift projected = new EmployeeShift
            {
                EmployeeId = employeeShift.EmployeeId,
                Date = employeeShift.Date,
                TimeIn = employeeShift.TimeIn,
                BreakTimeIn = employeeShift.BreakTimeIn,
                BreakTimeOut = employeeShift.BreakTimeOut,
                TimeOut = employeeShift.TimeOut,
                GraceMinutes = employeeShift.GraceMinutes,
                FlexibleMinutes = employeeShift.FlexibleMinutes
            };

            // noentry or invalid In/Out
            if (attend.NoEntry() || attend.InValidEntry())
                return projected;

            // adjust projectedShift on flexi
            if (attend.RawTimeIn > employeeShift.TimeIn)
            {
                // check late if within grace range
                var withinGrace = employeeShift.WithinGracePeriod((DateTime)attend.RawTimeIn);

                // apply flexi only if not covered by grace
                if (!withinGrace && employeeShift.IsFlexiShift() && employeeShift.WithinFlexiRange((DateTime)attend.RawTimeIn))
                    projected = employeeShift.CreateFlexShift((DateTime)attend.RawTimeIn);
            }

            // early login
            if (attend.EarlyLoginApproved && attend.RawTimeIn < employeeShift.TimeIn)
                projected = employeeShift.CreateFlexShift((DateTime)attend.RawTimeIn);

            return projected;
        }

        /// <summary>
        /// Returns payable entry for regular day
        /// </summary>
        /// <param name="trans"></param>
        /// <param name="projectedShift"></param>
        /// <param name="adtStatus"></param>
        /// <returns></returns>
        public static AttendanceBase GetPayableRegEntry(Attendance trans, IEmployeeShift projectedShift, AttendanceDateStatus adtStatus)
        {
            var payable = new AttendanceBase { EmployeeId = trans.EmployeeId, Date = trans.Date };

            // validity check
            if (trans.InValidEntry()) return payable;
            if (trans.NoEntry()) return payable;
            if (adtStatus.WholeDayAuthorizedOffWork()) return payable;


            if (trans.WithEntryInAM())
            {
                payable.TimeIn = Helper.Largest((DateTime)trans.TimeIn, projectedShift.TimeIn);
                payable.BreakTimeIn = trans.WholeDay() ? projectedShift.BreakTimeIn : Helper.Least((DateTime)trans.BreakTimeIn, projectedShift.BreakTimeIn);

                if (payable.TimeIn > payable.BreakTimeIn)
                {
                    payable.TimeIn = null;
                    payable.BreakTimeIn = null;
                }

                //apply grace period
                if (payable.WithEntryInAM() && projectedShift.HasGracePeriod())   //AM entry exists
                {
                    var tardy = TimeCalculator.ComputeTardy((DateTime)payable.TimeIn, projectedShift.TimeIn);

                    //adjust payable.TimeIn if within grace period
                    if (tardy > 0 && Math.Abs(tardy) <= Math.Abs(projectedShift.GraceMinutes))
                    {
                        payable.TimeIn = projectedShift.TimeIn;
                    }
                }
            }

            if (trans.WithEntryInPM())
            {
                payable.BreakTimeOut = trans.WholeDay() ? projectedShift.BreakTimeOut : Helper.Largest((DateTime)trans.BreakTimeOut, projectedShift.BreakTimeOut);
                payable.TimeOut = Helper.Least((DateTime)trans.TimeOut, projectedShift.TimeOut);

                if (payable.BreakTimeOut > payable.TimeOut)
                {
                    payable.BreakTimeOut = null;
                    payable.TimeOut = null;
                }

            }


            // remove payable if Leave / OB / Holiday / Restday 
            if (adtStatus.HalfdayAmAuthorizedOffWork())
            {
                payable.TimeIn = null;
                payable.BreakTimeIn = null;
            }
            if (adtStatus.HalfdayPmAuthorizedOffWork())
            {
                payable.BreakTimeOut = null;
                payable.TimeOut = null;
            }


            return payable;
        }

        /// <summary>
        /// Adjust payable based on OB
        /// </summary>
        /// <param name="payable"></param>
        /// <param name="projectedShift"></param>
        /// <param name="obRequest"></param>
        /// <param name="adtStatus"></param>
        /// <returns></returns>
        public static AttendanceBase GetAdjustedPayableRegEntry(AttendanceBase payable, IEmployeeShift projectedShift, OffBusinessRequest obRequest, AttendanceDateStatus adtStatus)
        {
            if (obRequest == null) return payable;

            var adjPayable = (AttendanceBase)payable.Clone();

            if (payable.NoEntry() && adtStatus.HasWholedayOBrequest)
            {
                adjPayable.TimeIn = projectedShift.TimeIn;
                adjPayable.BreakTimeIn = projectedShift.BreakTimeIn;
                adjPayable.BreakTimeOut = projectedShift.BreakTimeOut;
                adjPayable.TimeOut = projectedShift.TimeOut;
                return adjPayable;
            }

            //transform OB to 4-entry                
            var ob = RawTimeProcessor.TransformRawPairs(
                            new InOutPair
                            {
                                EmployeeId = payable.EmployeeId,
                                Date = payable.Date,
                                TimeIn = obRequest.ObStart,
                                TimeOut = obRequest.ObEnd
                            }, (EmployeeShift)projectedShift);

            var payableOB = GetPayableRegEntry(
                                new Attendance
                                {
                                    EmployeeId = payable.EmployeeId,
                                    Date = payable.Date,
                                    TimeIn = ob.TimeIn,
                                    BreakTimeIn = ob.BreakTimeIn,
                                    BreakTimeOut = ob.BreakTimeOut,
                                    TimeOut = ob.TimeOut
                                }, projectedShift, adtStatus);

            // adjust payable
            adjPayable.TimeIn = Helper.Least(payableOB.TimeIn, payable.TimeIn);
            adjPayable.BreakTimeIn = Helper.Largest(payableOB.BreakTimeIn, payable.BreakTimeIn);
            adjPayable.BreakTimeOut = Helper.Least(payableOB.BreakTimeOut, payable.BreakTimeOut);
            adjPayable.TimeOut = Helper.Largest(payableOB.TimeOut, payable.TimeOut);

            return adjPayable;
        }


        public static int GetOBused(AttendanceBase payable, AttendanceBase adjPayable)
        {
            int amOb = 0;
            if (adjPayable.WithEntryInAM())
            {
                amOb = payable.WithEntryInAM() ?
                    (int)((DateTime)payable.TimeIn - (DateTime)adjPayable.TimeIn).TotalMinutes +
                    (int)((DateTime)adjPayable.BreakTimeIn - (DateTime)payable.BreakTimeIn).TotalMinutes :
                    (int)((DateTime)adjPayable.BreakTimeIn - (DateTime)adjPayable.TimeIn).TotalMinutes;
            }

            int pmOb = 0;
            if (adjPayable.WithEntryInPM())
            {
                pmOb = payable.WithEntryInPM() ?
                    (int)((DateTime)adjPayable.TimeOut - (DateTime)payable.TimeOut).TotalMinutes +
                    (int)((DateTime)payable.BreakTimeOut - (DateTime)adjPayable.BreakTimeOut).TotalMinutes :
                    (int)((DateTime)adjPayable.TimeOut - (DateTime)adjPayable.BreakTimeOut).TotalMinutes;
            }

            return amOb + pmOb;
        }
    }


}
