﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Tk
{
    public static class Helper
    {
        public static int Dow(DateTime d)
        {
            int i = (int)d.DayOfWeek;
            return (i == 0 ? 7 : i);
        }

        public static DateTime ToDate(string s)
        {
            return DateTime.ParseExact(s, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
        }

        public static DateTime ToDateTime(string s)
        {
            return DateTime.ParseExact(s, "MM/dd/yyyy H:mm", System.Globalization.CultureInfo.InvariantCulture);
        }

        public static DateTime ToDateTime(DateTime date, string time)
        {
            return DateTime.ParseExact(date.ToString("MM/dd/yyyy") + " " + (time ?? "00:00"), "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
        }

        public static bool IsRestday(DateTime date)
        {
            //todo: change this to check the defined restday of employee
            return ("67").Contains(Dow(date).ToString());
        }

        /// <summary>
        /// Returns the smallest datetime
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public static DateTime Least(DateTime date1, DateTime date2)
        {
            return date1 < date2 ? date1 : date2;
        }

        /// <summary>
        /// Returns the smallest nullable datetime
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public static DateTime? Least(DateTime? date1, DateTime? date2)
        {
            if (date1 == null) return date2;
            if (date2 == null) return date1;
            return (DateTime)date1 < (DateTime)date2 ? date1 : date2;
        }

        /// <summary>
        /// Returns the smallest datetime
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <param name="date3"></param>
        /// <returns></returns>
        public static DateTime Least(DateTime date1, DateTime date2, DateTime date3)
        {
            var dt = Least(date1, date2);
            return dt < date3 ? dt : date3;
        }

        public static DateTime Least(DateTime date1, DateTime date2, DateTime date3, DateTime date4)
        {
            var dt = Least(date1, date2, date3);
            return dt < date4 ? dt : date4;
        }


        /// <summary>
        /// Returns the larger datetime
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public static DateTime Largest(DateTime date1, DateTime date2)
        {
            return date1 < date2 ? date2 : date1;
        }


        /// <summary>
        /// Returns the larger nullable datetime
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public static DateTime? Largest(DateTime? date1, DateTime? date2)
        {
            if (date1 == null) return date2;
            if (date2 == null) return date1;
            return date1 < date2 ? date2 : date1;
        }


        /// <summary>
        /// Returns the largest datetime
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <param name="date3"></param>
        /// <returns></returns>
        public static DateTime Largest(DateTime date1, DateTime date2, DateTime date3)
        {
            var dt = Largest(date1, date2);
            return dt < date3 ? date3 : dt;
        }


        public static IEnumerable<DateTime> GetDates(DateTime start, DateTime end)
        {
            if (start > end) return new List<DateTime>();

            return Enumerable.Range(0, 1 + end.Subtract(start).Days)
                                  .Select(offset => start.AddDays(offset));
        }
    }
}
