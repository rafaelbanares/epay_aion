﻿using Abp.UI;
using MMB.ePay.Processor.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using Tk.Domain;


namespace Tk
{

    public class RawTimeProcessor
    {


        private readonly List<ShiftType> _shiftTypes;
        private readonly List<ShiftTypeDetail> _shiftTypeDetails;
        private readonly DateRange _dateRange;
        private readonly IEnumerable<DateTime> _periodDates;


        public bool HasInOutIndicator { get; set; }


        public RawTimeProcessor(DateRange dateRange,
                                    List<ShiftType> shiftTypes,
                                    List<ShiftTypeDetail> shiftTypeDetails)
        {
            _dateRange = dateRange;
            _shiftTypes = shiftTypes;
            _shiftTypeDetails = shiftTypeDetails;
            _periodDates = Helper.GetDates(dateRange.Start, dateRange.End);

            //set defaults here
            HasInOutIndicator = true;
        }

        public List<Attendance> ProcessRaw(List<RawTimeLog> rawTimeLogs,
                                              List<Employee> employees,
                                              List<EmployeeShift> employeeShifts,
                                              List<AttendanceRequest> attendanceRequests,
                                              List<RestDayRequest> restDayRequests)
        {
            return ProcessRaw(rawTimeLogs, employees, employeeShifts, attendanceRequests, restDayRequests, null);
        }

        /// <summary>
        /// Process Raw Logs by batch
        /// </summary>
        /// <param name="rawTimeLogs"></param>
        /// <param name="attendanceUpload"></param>
        /// <param name="employees"></param>
        /// <param name="employeeShifts"></param>
        /// <param name="attendanceRequests"></param>
        /// <param name="restDayRequests"></param>
        /// <returns></returns>
        public List<Attendance> ProcessRaw(List<RawTimeLog> rawTimeLogs,
                                              List<Employee> employees,
                                              List<EmployeeShift> employeeShifts,
                                              List<AttendanceRequest> attendanceRequests,
                                              List<RestDayRequest> restDayRequests,
                                              List<AttendanceUpload> attendanceUploads)
        {
            var empShiftRange = GetEmployeesShiftRange(employees, employeeShifts);

            var ioPairs = GetInOutPairs(rawTimeLogs, employeeShifts, attendanceRequests, empShiftRange);

            return GetAttendance(employees, employeeShifts, restDayRequests, ioPairs, attendanceUploads, attendanceRequests);

        }




        //public List<Attendance> GetAttendance(List<Employee> employees,
        //                                         List<EmployeeShift> employeeShifts,
        //                                         List<RestDayRequest> restDayRequests,
        //                                         List<InOutPair> ioPairs)
        //{

        //    var qry = from a in employeeShifts
        //              join b in ioPairs on new { a.EmployeeId, a.Date } equals new { b.EmployeeId, b.Date }
        //              join e in employees on a.EmployeeId equals e.Id
        //              let xr =
        //                      (
        //                      restDayRequests
        //                            .Where(x => x.EmployeeId == a.EmployeeId && x.StartDate <= a.Date && x.EndDate >= a.Date)
        //                            .OrderByDescending(x => x.CreationTime)
        //                            .Select(x => x.RestDayCode)
        //                            .FirstOrDefault()
        //                      )
        //              let restdayCode = xr ?? e.RestDayCode ?? Constants.SystemDefaultRestDay

        //              let te = TransformRawPairs(b, a)

        //              select new Attendance()
        //              {
        //                  EmployeeId = a.EmployeeId,
        //                  Date = a.Date,
        //                  TimeIn = te.TimeIn.Truncate(),
        //                  BreakTimeIn = te.BreakTimeIn,
        //                  BreakTimeOut = te.BreakTimeOut,
        //                  TimeOut = te.TimeOut.Truncate(),
        //                  ShiftTypeId = a.ShiftTypeId,
        //                  RestDayCode = restdayCode,
        //                  Invalid = !ValidEntry(te.TimeIn, te.BreakTimeIn, te.BreakTimeOut, te.TimeOut)
        //              };


        //    return qry.ToList();
        //}

        public List<Attendance> GetAttendance(List<Employee> employees,
                                                 List<EmployeeShift> employeeShifts,
                                                 List<RestDayRequest> restDayRequests,
                                                 List<InOutPair> ioPairs,
                                                 List<AttendanceUpload> attendanceUploads,
                                                 List<AttendanceRequest> attendanceRequests)
        {
            bool hasAttendanceUpload = (attendanceUploads?.Count ?? 0) > 0;

            //create dictionaries for performance
            var employeesDict = employees.ToDictionary(e => e.Id);

            var ioPairsDict = ioPairs.ToDictionary(io => new { io.EmployeeId, io.Date });

            var restDayRequestsDict = restDayRequests
                              .GroupBy(r => r.EmployeeId)
                              .ToDictionary(g => g.Key, g => g.OrderByDescending(r => r.CreationTime).ToList());

            var attendanceUploadsDict = hasAttendanceUpload ? attendanceUploads.ToDictionary(au => new { au.EmployeeId, au.Date }) : null;


            var attendanceList = new List<Attendance>();

            foreach (var empShift in employeeShifts)
            {
                // Lookup employee details
                if (!employeesDict.TryGetValue(empShift.EmployeeId, out var employee))
                    continue;

                // Determine the rest day code
                var restDayCode = restDayRequestsDict.TryGetValue(empShift.EmployeeId, out var restDays) ?
                                  restDays.FirstOrDefault(rd => rd.StartDate <= empShift.Date && rd.EndDate >= empShift.Date)?.RestDayCode : null;

                restDayCode ??= employee.RestDayCode ?? Constants.SystemDefaultRestDay;

                var attendanceUpload = hasAttendanceUpload ? attendanceUploadsDict.GetValueOrDefault(new { empShift.EmployeeId, empShift.Date }) : null;

                if (attendanceUpload != null)
                {
                    //check attendance request
                    var atr = attendanceRequests
                               .Where(x => x.EmployeeId == empShift.EmployeeId && x.Date == empShift.Date)
                               .OrderByDescending(x => x.CreationTime)
                               .Select(x => new {x.TimeIn, x.BreaktimeIn, x.BreaktimeOut, x.PMBreaktimeIn, x.PMBreaktimeOut, x.TimeOut })
                               .FirstOrDefault();

                    var timeIn = atr?.TimeIn ?? attendanceUpload?.TimeIn;
                    var timeOut = atr?.TimeOut ?? attendanceUpload?.TimeOut;

                    var breakTimeIn = atr?.BreaktimeIn ?? attendanceUpload?.LunchBreaktimeIn;
                    var breakTimeOut = atr?.BreaktimeOut ?? attendanceUpload?.LunchBreaktimeOut;

                    var pmBreakTimeIn = atr?.PMBreaktimeIn ?? attendanceUpload?.PMBreaktimeIn;
                    var pmBreakTimeOut = atr?.PMBreaktimeOut ?? attendanceUpload?.PMBreaktimeOut;


                    var att = new Attendance
                    {
                        EmployeeId = empShift.EmployeeId,
                        Date = empShift.Date,
                        TimeIn = timeIn,
                        BreakTimeIn = breakTimeIn,
                        BreakTimeOut = breakTimeOut,
                        PMBreakTimeIn = pmBreakTimeIn,
                        PMBreakTimeOut = pmBreakTimeOut,
                        TimeOut = timeOut,
                        ShiftTypeId = empShift.ShiftTypeId,
                        RestDayCode = restDayCode,
                        Invalid = !ValidEntry(attendanceUpload.TimeIn, attendanceUpload.LunchBreaktimeIn, attendanceUpload.LunchBreaktimeOut, attendanceUpload.TimeOut)
                    };

                    attendanceList.Add(att);
                }
                else
                {
                    // Lookup in/out pair for the specific employee and date
                    if (!ioPairsDict.TryGetValue(new { empShift.EmployeeId, empShift.Date }, out var ioPair))
                        continue;

                    // Transform the raw pairs
                    var te = TransformRawPairs(ioPair, empShift);

                    // Create attendance record
                    var attendance = new Attendance
                    {
                        EmployeeId = empShift.EmployeeId,
                        Date = empShift.Date,
                        TimeIn = te.TimeIn.Truncate(),
                        BreakTimeIn = te.BreakTimeIn,
                        BreakTimeOut = te.BreakTimeOut,
                        TimeOut = te.TimeOut.Truncate(),
                        ShiftTypeId = empShift.ShiftTypeId,
                        RestDayCode = restDayCode,
                        Invalid = !ValidEntry(te.TimeIn, te.BreakTimeIn, te.BreakTimeOut, te.TimeOut)
                    };

                    attendanceList.Add(attendance);
                }
                
            }

            return attendanceList;
        }


        private bool ValidEntry(DateTime? timeIn, DateTime? breakTimeIn, DateTime? breakTimeOut, DateTime? timeOut)
        {
            return (timeIn.HasValue == breakTimeIn.HasValue) &&
                   (breakTimeOut.HasValue == timeOut.HasValue);
        }

        /// <summary>
        /// Transform the raw pairs (In/Out) into 4 entries including break In/Out
        /// </summary>
        /// <param name="ioPair"></param>
        /// <param name="empShift"></param>
        /// <returns></returns>
        public static TimeEntry TransformRawPairs(InOutPair ioPair, EmployeeShift empShift)
        {
            var te = new TimeEntry(null, null, null, null);

            if (ioPair.NoEntry())
                return te;

            if (ioPair.TimeIn.HasValue)
            {
                if (ioPair.TimeIn >= empShift.BreakTimeIn)
                    // half-day (PM Work)
                    te.BreakTimeOut = ioPair.TimeIn;
                else
                    te.TimeIn = ioPair.TimeIn;
            }

            if (ioPair.TimeOut.HasValue)
            {
                if (ioPair.TimeOut <= empShift.BreakTimeOut)
                    // half-day (AM Work)
                    te.BreakTimeIn = ioPair.TimeOut;
                else
                    te.TimeOut = ioPair.TimeOut;
            }

            //wholeday work: fill-in the break-time
            if (te.TimeIn.HasValue && te.TimeOut.HasValue)
            {
                te.BreakTimeIn = empShift.BreakTimeIn;
                te.BreakTimeOut = empShift.BreakTimeOut;
            }

            return te;
        }



        public List<InOutPair> GetInOutPairs(List<RawTimeLog> rawTimeLogs,
                                                List<EmployeeShift> employeeShifts,
                                                List<AttendanceRequest> attendanceRequests,
                                                List<EmployeeShiftEntryRange> employeesShiftEntryRange)
        {

            var io = from a in employeeShifts
                     join b in employeesShiftEntryRange
                     on new { a.EmployeeId, a.Date } equals new { b.EmployeeId, b.Date }
                     join d in _periodDates on a.Date equals d.Date
                     let raw_in =
                            (
                            rawTimeLogs
                               .Where(x => x.EmployeeId == a.EmployeeId &&
                                           x.LogTime >= b.ValidTimeIn.Start &&
                                           x.LogTime <= b.ValidTimeIn.End &&
                                           (!HasInOutIndicator || (HasInOutIndicator && x.InOut == "I")))
                               .OrderBy(x => x.LogTime)
                               .Select(x => new { x.LogTime })
                               .FirstOrDefault()
                            )
                     let raw_out =
                           (
                           rawTimeLogs
                               .Where(x => x.EmployeeId == a.EmployeeId &&
                                           x.LogTime >= b.ValidTimeOut.Start &&
                                           x.LogTime <= b.ValidTimeOut.End &&
                                           (!HasInOutIndicator || (HasInOutIndicator && x.InOut == "O")))
                               .OrderByDescending(x => x.LogTime)
                               .Select(x => new { x.LogTime })
                               .FirstOrDefault()
                           )
                     let atr =
                           (
                           attendanceRequests
                               .Where(x => x.EmployeeId == a.EmployeeId && x.Date == a.Date)
                               .OrderByDescending(x => x.CreationTime)
                               .Select(x => new { x.TimeIn, x.TimeOut })
                               .FirstOrDefault()
                           )
                     let timeIn = atr?.TimeIn ?? raw_in?.LogTime
                     let timeOut = atr?.TimeOut ?? raw_out?.LogTime

                     select new InOutPair()
                     {
                         EmployeeId = a.EmployeeId,
                         Date = a.Date,
                         TimeIn = timeIn,
                         TimeOut = (timeOut == timeIn) ? null : timeOut
                     };

            return io.ToList();
        }


        public List<EmployeeShiftEntryRange> GetEmployeesShiftRange(List<Employee> employees, List<EmployeeShift> employeeShifts)
        {
            var employeesShiftEntryRange = new List<EmployeeShiftEntryRange>();

            foreach (var e in employees)
            {
                var shiftRange = employeeShifts
                                    .Where(x => x.EmployeeId == e.Id)
                                    .OrderBy(x => x.Date)
                                    .Select(x => new EmployeeShiftEntryRange()
                                    {
                                        EmployeeId = x.EmployeeId,
                                        Date = x.Date,
                                        TimeIn = x.TimeIn,
                                        TimeOut = x.TimeOut,
                                        ValidTimeIn = new DateRange(DateTime.MinValue, DateTime.MinValue),
                                        ValidTimeOut = new DateRange(DateTime.MinValue, DateTime.MinValue)
                                    })
                                    .ToList();

                // 1st row 
                shiftRange[0].ValidTimeIn.Start = shiftRange[0].TimeIn.AddHours(-3);
                shiftRange[0].ValidTimeIn.End = shiftRange[0].TimeOut.AddMinutes(-1);
                shiftRange[0].ValidTimeOut.Start = shiftRange[0].TimeIn.AddMinutes(1);

                // set shift time in/out range
                for (int i = 1; i < shiftRange.Count; i++)
                {

                    var shiftToday = shiftRange[i];
                    var shiftYesterday = shiftRange[i - 1];

                    if (shiftToday.TimeIn < shiftYesterday.TimeOut)
                    {
                        var msg = string.Format("Employee: {0} {1}, has overlapping shift. \n" +
                                                "Date of overlap: {2} and {3} \n" +
                                                "Shift: {4} and {5}",
                                                e.LastName,
                                                e.FirstName,
                                                shiftYesterday.Date.ToString("MM/dd/yyyy"),
                                                shiftToday.Date.ToString("MM/dd/yyyy"),
                                                shiftYesterday.TimeIn.ToString("hh:mm tt") + " - " + shiftYesterday.TimeOut.ToString("hh:mm tt"),
                                                shiftToday.TimeIn.ToString("hh:mm tt") + " - " + shiftToday.TimeOut.ToString("hh:mm tt"));


                        throw new UserFriendlyException(msg);
                    }


                    ValidShiftRange vsr = GetValidShiftRange(shiftToday.TimeIn, shiftYesterday.TimeOut);

                    shiftRange[i].ValidTimeIn.Start = vsr.ValidTimeInToday;
                    shiftRange[i].ValidTimeIn.End = shiftToday.TimeOut.AddMinutes(-1);
                    shiftRange[i].ValidTimeOut.Start = shiftToday.TimeIn.AddMinutes(1);
                    shiftRange[i - 1].ValidTimeOut.End = vsr.ValidTimeOutYesterday;
                }
                // last row 
                shiftRange[^1].ValidTimeOut.End = shiftRange[^1].TimeOut.AddHours(8);

                employeesShiftEntryRange.AddRange(shiftRange);
            }

            return employeesShiftEntryRange;
        }

        /// <summary>
        /// Returns shifts for the entire period including 1 day before and after the period.
        /// </summary>
        /// <param name="employees"></param>
        /// <param name="shiftRequests"></param>
        /// <returns></returns>
        public List<EmployeeShift> GetEmployeeShifts(List<Employee> employees, List<ShiftRequest> shiftRequests)
        {

            //include previous and after dates
            var dates = Helper.GetDates(_dateRange.Start.AddDays(-1), _dateRange.End.AddDays(1));

            var q = from d in dates
                    from e in employees
                    let srq =
                             (
                             from x in shiftRequests
                             where x.EmployeeId == e.Id && x.StartDate <= d && x.EndDate >= d
                             orderby x.ApprovalDate descending
                             select new { x.ShiftTypeId }
                             ).FirstOrDefault()

                    let shiftTypeId = srq?.ShiftTypeId ?? e.ShiftTypeId

                    let empShift =
                             (
                             from a in _shiftTypes
                             join b in _shiftTypeDetails on a.Id equals b.ShiftTypeId
                             where a.Id == shiftTypeId && b.Days.Contains(Helper.Dow(d).ToString())
                             select new
                             {
                                 b.ShiftTypeId,
                                 b.TimeIn,
                                 b.BreakTimeIn,
                                 b.BreakTimeOut,
                                 b.TimeOut,
                                 b.CrossoverShiftStartsOnPrevDay,
                                 a.FlexiTime,
                                 a.MinimumOvertime,
                                 a.GraceTime
                             }
                            ).FirstOrDefault()

                    select new EmployeeShift(e.Id,
                                            d,
                                            shiftTypeId,
                                            empShift?.TimeIn,
                                            empShift?.BreakTimeIn,
                                            empShift?.BreakTimeOut,
                                            empShift?.TimeOut,
                                            empShift.FlexiTime,
                                            empShift.GraceTime,
                                            empShift.MinimumOvertime,
                                            empShift.CrossoverShiftStartsOnPrevDay);

            return q.ToList();
        }

        private ValidShiftRange GetValidShiftRange(DateTime shiftTimeInToday, DateTime shiftTimeOutYesterday)
        {
            
            int shiftGap = (int)(shiftTimeInToday - shiftTimeOutYesterday).TotalMinutes;

            if (shiftGap > 0)
            {
                // backtrack 50%(1-60mins), 25%(>60mins) (of gap between shiftIN today and shiftOUT yesterday)                                
                var back = (shiftGap <= 60) ? Math.Round(shiftGap * .5, 0) : Math.Round(shiftGap * .25, 0);
                var validTimeIn = shiftTimeInToday.AddMinutes(back * -1);

                return new ValidShiftRange
                {
                    ValidTimeInToday = validTimeIn,
                    ValidTimeOutYesterday = HasInOutIndicator ? validTimeIn : validTimeIn.AddMinutes(-1)
                };
            }
            else
            {
                //strait shift (yesterday & today)                
                return new ValidShiftRange
                {
                    ValidTimeOutYesterday = shiftTimeOutYesterday,
                    ValidTimeInToday = shiftTimeInToday
                };
            }
        }

        private class ValidShiftRange
        {
            public virtual DateTime ValidTimeInToday { get; set; }
            public virtual DateTime ValidTimeOutYesterday { get; set; }
        }
    }
}
