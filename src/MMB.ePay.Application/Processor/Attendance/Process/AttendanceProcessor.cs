﻿using System;
using System.Collections.Generic;
using Tk.Domain;
using System.Linq;

namespace Tk
{

    public class AttendanceProcessor
    {


        private readonly List<Holiday> _holidays;
        private readonly NightDifferential _nightDiffBusiness;

        public AttendanceProcessor(List<Holiday> holidays, NightDifferential nightDiffBusiness)
        {
            _holidays = holidays;
            _nightDiffBusiness = nightDiffBusiness;
        }






        public (List<Timesheet> Timesheets, List<ProcessedOvertime> ProcessedOvertimes)
            ProcessAttendance(List<Employee> employees,
                                List<Attendance> attendances,
                                List<EmployeeShift> employeeShifts,
                                List<LeaveRequest> leaveRequests,
                                List<OffBusinessRequest> obRequests,
                                List<OvertimeRequest> otRequests)
        {
            var timesheetList = new List<Timesheet>();
            var overtimeList = new List<ProcessedOvertime>();
            var otCalculator = new OvertimeCalculator(new NightDifferential());

            foreach (var e in employees)
            {
                var empAttend = attendances.Where(a => a.EmployeeId == e.Id).ToList();

                foreach (var att in empAttend)
                {
                    var es = employeeShifts.Find(s => s.EmployeeId == att.EmployeeId && s.Date == att.Date);

                    var lvReq = leaveRequests
                                    .Where(x => x.EmployeeId == att.EmployeeId && x.LeaveDate == att.Date)
                                    .FirstOrDefault();

                    var obReq = obRequests
                                    .Where(x => x.EmployeeId == att.EmployeeId && x.Date == att.Date)
                                    .FirstOrDefault();

                    var holiday = _holidays.Find(x => x.Date == att.Date);

                    var adtStatus = new AttendanceDateStatus(lvReq, holiday, obReq, att.IsRestDay(), es.CrossShiftStartsOnPrevDay, e.LocationId);

                    var projected = Core.GetProjectedShift(att, es);
                    var payable = Core.GetPayableRegEntry(att, projected, adtStatus);
                    var adjPayable = Core.GetAdjustedPayableRegEntry(payable, projected, obReq, adtStatus);
                    var timeCalc = new TimeCalculator(adtStatus, e, projected, adjPayable);
                    var absent = timeCalc.ComputeAbsence();
                    var tardy = timeCalc.ComputeTardy();
                    var ut = timeCalc.ComputeUT();
                    var regular = timeCalc.ComputeNetRegWork();
                    var regNd1 = _nightDiffBusiness.ComputeND1Regular(es.TimeIn.Date, adjPayable, adtStatus);
                    var regNd2 = _nightDiffBusiness.ComputeND2Regular(es.TimeIn.Date, adjPayable, adtStatus);

                    var obMins = adtStatus.OnWholedayLeave() ? 0 :
                                 adtStatus.HasOBrequest ? Core.GetOBused(payable, adjPayable) : 0;


                    //process overtime
                    var otcount = 0;
                    var otr = otRequests.Find(o => o.EmployeeId == att.EmployeeId && o.Date == att.Date);
                    if (otr != null)
                    {
                        var ot = otCalculator.GetOvertimeList(adtStatus, adjPayable, att, otr, holiday);
                        overtimeList.AddRange(ot);
                        otcount = ot.Count();
                    }

                    var tmsheet = new Timesheet
                    {
                        EmployeeId = att.EmployeeId,
                        Date = att.Date,
                        RegMins = regular,
                        Absence = absent,
                        TardyMins = tardy,
                        UtMins = ut,
                        PaidHoliday = 0,
                        RegNd1Mins = regNd1,
                        RegNd2Mins = regNd2,
                        ObMins = obMins,
                        LeaveDays = (lvReq?.Days ?? 0),
                        IsRestday = adtStatus.IsRestday,
                        IsHoliday = adtStatus.IsHoliday,
                        Invalid = att.InValidEntry()
                    };

                    //update remarks
                    tmsheet.Remarks = GetRemarks(tmsheet, adtStatus, otcount, lvReq);

                    //add to list
                    timesheetList.Add(tmsheet);
                }
            }

            return (timesheetList, overtimeList);
        }

        private string GetRemarks(Timesheet tmsheet, AttendanceDateStatus adtStatus, int otcount, LeaveRequest lvReq)
        {
            var remarks = new List<string>();

            if (tmsheet.Invalid)
                return "Invalid";

            if (tmsheet.LeaveDays > 0)
                remarks.Add(lvReq.LeaveDisplay);

            if (adtStatus.IsHoliday)
                remarks.Add(adtStatus.HolidayDescription);

            if (tmsheet.Absence == 1 && tmsheet.Date < DateTime.Today)
                remarks.Add("Absent");

            if (tmsheet.Absence == (decimal)0.5)
                remarks.Add("Half-Day");

            if (tmsheet.TardyMins > 0)
                remarks.Add("Tardy");

            if (tmsheet.UtMins > 0)
                remarks.Add("UT");

            if (tmsheet.ObMins > 0)
                remarks.Add("OB");

            if (otcount > 0)
                remarks.Add("OT");

            return string.Join(", ", remarks);
        }




    }

    public struct Tardy
    {
        public int TardyMinutes;
        public int GraceMinutesUsed;
    }
}
