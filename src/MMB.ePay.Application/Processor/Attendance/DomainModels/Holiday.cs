﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk.Domain
{

    public class Holiday
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public string Description { get; set; }

        public bool Halfday { get; set; }

        /// <summary>
        /// True if half-day holiday in the first half part
        /// </summary>
        public bool HolidayAM { get; set; }

        public string HolidayType { get; set; }

        public int? LocationId { get; set; }


        // public methods
        public bool WholeDayHoliday()
        {
            return !Halfday;
        }

        /// <summary>
        /// Half-day Holiday in the afternoon
        /// </summary>
        /// <returns></returns>
        public bool HolidayPM()
        {
            return !HolidayAM;
        }

        public bool IsLegal()
        {
            return HolidayType == HolidayConstants.Legal;
        }

        public bool IsSpecial()
        {
            return HolidayType == HolidayConstants.Special;
        }

        public bool IsLegalSpecial()
        {
            return HolidayType == HolidayConstants.LegalSpecial;
        }

        public bool IsDoubleLegal()
        {
            return HolidayType == HolidayConstants.DoubleLegal;
        }
    }




}
