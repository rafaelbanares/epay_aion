﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk.Domain
{
    public class LeaveRequest
    {
        //public int Id { get; set; }

        public int EmployeeId { get; set; }

        public int LeaveTypeId { get; set; }

        public DateTime LeaveDate { get; set; }

        public decimal Days { get; set; }

        /// <summary>
        /// True if half-day leave in the first-half of the shift (i.e. work in the second-half)
        /// Use this only if leave is half-day
        /// </summary>
        public bool HalfdayLeaveFirstHalf { get; set; }

        public string LeaveDisplay { get; set; }

        // Public Methods
        public bool WholedayLeave()
        {
            return (Days == 1M);
        }

        public bool HalfdayLeave()
        {
            return (Days == .5M);
        }
    }
}
