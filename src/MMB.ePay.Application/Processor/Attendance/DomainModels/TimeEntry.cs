﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk.Domain
{
    public class TimeEntry
    {
        public TimeEntry() { }

        public TimeEntry(DateTime? timeIn, DateTime? breakTimeIn, DateTime? breakTimeOut, DateTime? timeOut) 
        {
            TimeIn = timeIn;
            BreakTimeIn = breakTimeIn;
            BreakTimeOut = breakTimeOut;
            TimeOut = timeOut;
        }


        public DateTime? TimeIn { get; set; }
        public DateTime? BreakTimeIn { get; set; }
        public DateTime? BreakTimeOut { get; set; }
        public DateTime? TimeOut { get; set; }
    }
}
