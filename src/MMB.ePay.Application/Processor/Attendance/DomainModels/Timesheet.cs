﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk.Domain
{
    public class Timesheet
    {
        public int EmployeeId { get; set; }
        public DateTime Date { get; set; }
        public int RegMins { get; set; }
        public decimal Absence { get; set; }
        public int TardyMins { get; set; }
        public int UtMins { get; set; }
        public int RegNd1Mins { get; set; }
        public int RegNd2Mins { get; set; }
        public decimal PaidHoliday { get; set; }
        public decimal LeaveDays { get; set; }
        public decimal ObMins { get; set; }
        
        public bool Invalid { get; set; }

        public bool IsHoliday { get; set; }

        public bool IsRestday { get; set; }

        public string Remarks { get; set; }
    }
}
