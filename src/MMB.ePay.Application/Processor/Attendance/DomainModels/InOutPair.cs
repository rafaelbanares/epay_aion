﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk.Domain
{
    public class InOutPair
    {
 
        //--------- public properties ---------

        public int EmployeeId { get; set; }

        public DateTime Date { get; set; }

        public DateTime? TimeIn { get; set; }

        public DateTime? TimeOut { get; set; }

        

        //--------- public methods ---------

        public bool NoEntry()
        {
            return (TimeIn == null) && (TimeOut == null);
        }

        public bool ValidEntry()
        {
            //nts: NoEntry check is necessary here
            return ((TimeIn.HasValue == TimeOut.HasValue) && (NoEntry() || TimeIn <= TimeOut));
        }
    }
}
