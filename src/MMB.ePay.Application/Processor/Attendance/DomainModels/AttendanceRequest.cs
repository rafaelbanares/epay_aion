﻿using System;

namespace Tk.Domain
{
    public class AttendanceRequest
    {        
        public int EmployeeId { get; set; }
        
        public DateTime Date { get; set; }
        
        public DateTime? TimeIn { get; set; }

        public DateTime? BreaktimeIn { get; set; }

        public DateTime? BreaktimeOut { get; set; }

        public DateTime? PMBreaktimeIn { get; set; }

        public DateTime? PMBreaktimeOut { get; set; }

        public DateTime? TimeOut { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
