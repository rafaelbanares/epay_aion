﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk.Domain
{
    public class EmployeeShift : IEmployeeShift
    {
        public int EmployeeId { get; set; }

        public DateTime Date { get; set; }

        public DateTime TimeIn { get; set; }

        public DateTime BreakTimeIn { get; set; }

        public DateTime BreakTimeOut { get; set; }

        public DateTime TimeOut { get; set; }


        //--------- New Properties ---------

        public int ShiftTypeId { get; set; }

        /// <summary>
        /// Minimum overtime in minutes
        /// </summary>
        public int MinimumOvertime { get; set; }

        /// <summary>
        /// Flexible Time allowed in minutes
        /// </summary>
        public int FlexibleMinutes { get; set; }

        /// <summary>
        /// Grace period allowed in minutes
        /// </summary>
        public int GraceMinutes { get; set; }


        /// <summary>
        /// True if shift starts on previous day for crossover shifts
        /// </summary>
        public bool CrossShiftStartsOnPrevDay { get; set; }


        #region CTOR
        public EmployeeShift() { }

        public EmployeeShift(int employeeId,
                            DateTime date,
                            int shiftTypeId,
                            string shiftTimeIn,
                            string shiftBreakTimeIn,
                            string shiftBreakTimeOut,
                            string shiftTimeOut,
                            int flexiTime,
                            int graceTime,
                            int minimumOvertime,
                            bool crossoverShiftStartsOnPrevDay)
        {

            EmployeeId = employeeId;
            Date = date;
            ShiftTypeId = shiftTypeId;
            TimeIn = Helper.ToDateTime(date, shiftTimeIn);
            BreakTimeIn = Helper.ToDateTime(date, shiftBreakTimeIn);
            BreakTimeOut = Helper.ToDateTime(date, shiftBreakTimeOut);
            TimeOut = Helper.ToDateTime(date, shiftTimeOut);
            MinimumOvertime = minimumOvertime;
            FlexibleMinutes = flexiTime;
            GraceMinutes = graceTime;
            CrossShiftStartsOnPrevDay = crossoverShiftStartsOnPrevDay;

            if (IsCrossoverShift())
            {
                if (crossoverShiftStartsOnPrevDay)
                {
                    TimeIn = TimeIn.AddDays(-1);
                    BreakTimeIn = BreakTimeIn.AddDays(-1);
                    BreakTimeOut = BreakTimeOut.AddDays(-1);
                    TimeOut = TimeOut.AddDays(-1);
                }

                AdjustCrossoverShift();
            }

        }



        public EmployeeShift(int employeeId, DateTime date, ShiftType shiftType, ShiftTypeDetail shiftTypeDetail) :
            this(employeeId,
                 date,
                 shiftType.Id,
                 shiftTypeDetail.TimeIn,
                 shiftTypeDetail.BreakTimeIn,
                 shiftTypeDetail.BreakTimeOut,
                 shiftTypeDetail.TimeOut,
                 shiftType.FlexiTime,
                 shiftType.GraceTime,
                 shiftType.MinimumOvertime,
                 shiftTypeDetail.CrossoverShiftStartsOnPrevDay
                )
        { }






        private void AdjustCrossoverShift()
        {
            // fix crossovers
            if (TimeIn > BreakTimeIn)
                BreakTimeIn = BreakTimeIn.AddDays(1);

            if (BreakTimeIn > BreakTimeOut)
                BreakTimeOut = BreakTimeOut.AddDays(1);

            if (BreakTimeOut > TimeOut)
                TimeOut = TimeOut.AddDays(1);
        }
        #endregion


        public int FirstHalfTotalMinutes()
        {
            return (int)(BreakTimeIn - TimeIn).TotalMinutes;
        }

        public int LastHalfTotalMinutes()
        {
            return (int)(TimeOut - BreakTimeOut).TotalMinutes;
        }

        public int BreakTotalMinutes()
        {
            return (int)(BreakTimeOut - BreakTimeIn).TotalMinutes;
        }


        public IEmployeeShift CreateFlexShift(DateTime actualTimeIn)
        {
            var breakTimeIn = actualTimeIn.AddMinutes(FirstHalfTotalMinutes());
            var breakTimeOut = breakTimeIn.AddMinutes(BreakTotalMinutes());
            var timeOut = breakTimeOut.AddMinutes(LastHalfTotalMinutes());

            return new EmployeeShift
            {
                EmployeeId = this.EmployeeId,
                Date = this.Date,
                TimeIn = actualTimeIn,
                BreakTimeIn = breakTimeIn,
                BreakTimeOut = breakTimeOut,
                TimeOut = timeOut
            };
        }


        /// <summary>
        /// Returns true of shift is flexible
        /// </summary>
        /// <returns></returns>
        public bool IsFlexiShift()
        {
            return FlexibleMinutes > 0;
        }

        public bool WithinFlexiRange(DateTime actualTimeIn)
        {
            int trd = (int)(actualTimeIn - TimeIn).TotalMinutes;
            return (trd > 0 && Math.Abs(trd) <= Math.Abs(FlexibleMinutes));
        }


        public bool HasGracePeriod()
        {
            return GraceMinutes > 0;
        }

        public bool WithinGracePeriod(DateTime actualTimeIn)
        {
            int trd = (int)(actualTimeIn - TimeIn).TotalMinutes;
            return (trd > 0 && Math.Abs(trd) <= Math.Abs(GraceMinutes));
        }



        /// <summary>
        /// Returns true if shift is crossover
        /// </summary>
        /// <returns></returns>
        private bool IsCrossoverShift()
        {
            if (TimeIn > BreakTimeIn)
                return true;

            if (BreakTimeIn > BreakTimeOut)
                return true;

            if (BreakTimeOut > TimeOut)
                return true;

            return false;
        }






    }
}
