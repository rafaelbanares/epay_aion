﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk.Domain
{
    public class OffBusinessRequest
    {
        public int EmployeeId { get; set; }

        public DateTime Date { get; set; }

        public DateTime ObStart { get; set; }

        public DateTime ObEnd { get; set; }




        private bool ObStartHasNoTime()
        {
            return (ObStart.Hour == 0 && ObStart.Minute == 0 && ObStart.Second == 0);
        }
        private bool ObEndHasNoTime()
        {
            return (ObEnd.Hour == 0 && ObEnd.Minute == 0 && ObEnd.Second == 0);
        }


        // Public Methods
        //public bool WholedayOB()

        public bool ObEntriesHasNoTime()
        {
            return (ObStartHasNoTime() && ObEndHasNoTime());
        }

    }
}
