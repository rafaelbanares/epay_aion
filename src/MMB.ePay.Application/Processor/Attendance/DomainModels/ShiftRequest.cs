﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk.Domain
{
    
    public class ShiftRequest
    {
        public virtual int Id { get; set; }
        public virtual int EmployeeId { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }
        public virtual int ShiftTypeId { get; set; }
        //public virtual int StatusId { get; set; }
        public virtual DateTime ApprovalDate { get; set; }
    }
}
