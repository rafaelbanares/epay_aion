﻿using System;

namespace Tk.Domain
{
    public interface IAttendance
    {

        int EmployeeId { get; set; }

        DateTime Date { get; set; }

        DateTime? TimeIn { get; set; }

        DateTime? BreakTimeIn { get; set; }

        DateTime? BreakTimeOut { get; set; }

        DateTime? TimeOut { get; set; }


        /// <summary>
        /// Device Time-In or Requested Time-In if present
        /// </summary>        
        DateTime? RawTimeIn { get; }


        /// <summary>
        /// Device Time-Out or Requested Time-Out if present
        /// </summary>
        public DateTime? RawTimeOut { get; }


        public bool ValidEntry();

        public bool InValidEntry();

        public bool NoEntry();

        public bool WithEntry();

        public bool WholeDay();

        public bool NoEntryInAM();

        /// <summary>
        /// Returns true if with valid entry in AM
        /// </summary>
        /// <returns></returns>
        public bool WithEntryInAM();

        /// <summary>
        /// Returns true if no entry in PM (both In/Out)
        /// </summary>
        /// <returns></returns>
        public bool NoEntryInPM();

        /// <summary>
        /// Returns true if with valid entry in PM
        /// </summary>
        /// <returns></returns>
        public bool WithEntryInPM();

        public bool Halfday();

        public bool WorkedHalfdayInPM();

        public bool WorkedHalfdayInAM();
    }
}
