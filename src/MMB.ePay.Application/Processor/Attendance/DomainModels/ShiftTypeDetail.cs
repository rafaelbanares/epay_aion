﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk.Domain
{
    public class ShiftTypeDetail
    {
        public int Id { get; set; }

        public int ShiftTypeId { get; set; }

        public string Days { get; set; }

        public string TimeIn { get; set; }

        public string BreakTimeIn { get; set; }

        public string BreakTimeOut { get; set; }

        public string TimeOut { get; set; }

        /// <summary>
        /// True if shift starts on previous day for crossover shifts
        /// </summary>
        public bool CrossoverShiftStartsOnPrevDay { get; set; }
    }
}
