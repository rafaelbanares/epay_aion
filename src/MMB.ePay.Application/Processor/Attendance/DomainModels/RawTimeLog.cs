﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk.Domain
{
    public class RawTimeLog
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public DateTime LogTime { get; set; }
        public string InOut { get; set; }
    }
}
