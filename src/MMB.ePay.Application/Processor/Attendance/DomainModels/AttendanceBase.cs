﻿using System;

namespace Tk.Domain
{
    public class AttendanceBase : IAttendance, ICloneable
    {

        public int EmployeeId { get; set; }

        public DateTime Date { get; set; }
      
        public DateTime? TimeIn { get; set; }

        public DateTime? BreakTimeIn { get; set; }

        public DateTime? BreakTimeOut { get; set; }

        public DateTime? PMBreakTimeIn { get; set; }

        public DateTime? PMBreakTimeOut { get; set; }

        public DateTime? TimeOut { get; set; }


        /// <summary>
        /// Device Time-In or Requested Time-In if present
        /// </summary>        
        public DateTime? RawTimeIn
        {
            get { return TimeIn ?? BreakTimeOut; }
        }
       

        /// <summary>
        /// Device Time-Out or Requested Time-Out if present
        /// </summary>
        public DateTime? RawTimeOut
        {
            get { return TimeOut ?? BreakTimeIn; }
        }


        public bool ValidEntry()
        {
            return (TimeIn.HasValue == BreakTimeIn.HasValue) && 
                   (BreakTimeOut.HasValue == TimeOut.HasValue) && 
                   (PMBreakTimeIn.HasValue == PMBreakTimeOut.HasValue);
        }

        public bool InValidEntry()
        {
            return (!ValidEntry());
        }

        public bool NoEntry()
        {
            return (NoEntryInAM() && NoEntryInPM());
        }

        public bool WithEntry()
        {
            return !NoEntry();
        }

        public bool WholeDay()
        {
            return WithEntryInAM() && WithEntryInPM();
        }

        public bool NoEntryInAM()
        {            
            return (TimeIn == null && BreakTimeIn == null);
        }

        /// <summary>
        /// Returns true if with valid entry in AM
        /// </summary>
        /// <returns></returns>
        public bool WithEntryInAM()
        {
            return (TimeIn != null && BreakTimeIn != null);
        }

        /// <summary>
        /// Returns true if no entry in PM (both In/Out)
        /// </summary>
        /// <returns></returns>
        public bool NoEntryInPM()
        {            
            return (BreakTimeOut == null && TimeOut == null);
        }

        /// <summary>
        /// Returns true if with valid entry in PM
        /// </summary>
        /// <returns></returns>
        public bool WithEntryInPM()
        {
            return (BreakTimeOut != null && TimeOut != null);
        }

        public bool Halfday()
        {
            return WithEntryInAM() != WithEntryInPM();
        }

        public bool WorkedHalfdayInPM()
        {
            return NoEntryInAM() && WithEntryInPM();
        }

        public bool WorkedHalfdayInAM()
        {
            return NoEntryInPM() && WithEntryInAM();
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
