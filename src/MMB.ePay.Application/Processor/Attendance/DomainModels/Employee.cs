﻿using System;

namespace Tk.Domain
{
    public class Employee
    {
        public int Id { get; set; }
        public String LastName { get; set; }
        public String FirstName { get; set; }
        public int ShiftTypeId { get; set; }
        public string RestDayCode { get; set; }
        public bool OtExempt { get; set; }
        public bool UtExempt { get; set; }
        public bool TardyExempt { get; set; }
        public bool NonSwiper { get; set; }
        public int? LocationId { get; set; }
    }


}
