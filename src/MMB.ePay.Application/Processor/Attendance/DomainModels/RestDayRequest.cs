﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk.Domain
{
    public class RestDayRequest
    {
        public int EmployeeId { get; set; }
        public string RestDayCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreationTime { get; set; }

    }
}
