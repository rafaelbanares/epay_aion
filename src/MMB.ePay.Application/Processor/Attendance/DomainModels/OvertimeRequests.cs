﻿using System;

namespace Tk.Domain
{
    public class OvertimeRequest
    {        
        public int EmployeeId { get; set; }
        public DateTime Date { get; set; }
        public DateTime AuthorizedStart { get; set; }
        public DateTime AuthorizedEnd { get; set; }
    }
}
