﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk.Domain
{
    public class ProcessedOvertime
    {        
        public int EmployeeId { get; set; }
        
        public DateTime Date { get; set; }
        
        public string OvertimeCode { get; set; }

        public OvertimeSubTypes SubType { get; set; }

        public int OvertimeMinutes { get; set; }
        
    }
}
