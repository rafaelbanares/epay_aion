﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk.Domain
{
    public class ProcessedLeave
    {
        public int EmployeeId { get; set; }
        
        public DateTime Date { get; set; }
        
        public int LeaveTypeId { get; set; }

        public Decimal Days { get; set; }

    }
}
