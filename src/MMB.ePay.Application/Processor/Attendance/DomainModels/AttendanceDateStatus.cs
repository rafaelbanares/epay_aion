﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk.Domain
{
    public class AttendanceDateStatus
    {

        public bool IsRestday { get; private set; }

        public bool IsHoliday { get; private set; }
        public bool IsHalfdayHoliday { get; private set; }
        public bool IsHalfdayHolidayFirstHalf { get; private set; }


        public bool OnLeave { get; private set; }
        public bool OnHalfdayLeave { get; private set; }
        public bool OnHalfdayLeaveFirstHalf { get; private set; }

        public bool HasOBrequest { get; private set; }


        /// <summary>
        /// True if ObStart and ObEnd has no time part (date only)
        /// </summary>
        public bool HasWholedayOBrequest { get; private set; }


        /// <summary>
        /// Holiday Discription
        /// </summary>
        public string HolidayDescription { get; private set; }


        /// <summary>
        /// True if shift starts on previous day, othewise false
        /// </summary>
        public bool ShiftStartsPrevDay { get; private set; }



        /// <summary>
        /// True means it is restday(no leave/holiday/ob)
        /// False means Regular day(no leave/holiday/ob)
        /// (for unit test use only)
        /// </summary>
        /// <param name="isRestday"></param>
        public AttendanceDateStatus(bool isRestday) : this(null, null, null, isRestday, false)
        {
        }

        public AttendanceDateStatus(LeaveRequest leaveRequest, Holiday holiday, OffBusinessRequest obRequest, bool isRestday, bool shiftStartsPrevDay)
            : this(leaveRequest, holiday, obRequest, isRestday, shiftStartsPrevDay, null)
        {
        }

        /// <summary>
        /// Object containing date status whether holiday/restday or leave/ob
        /// This does not not check if date is the same as attendance date
        /// </summary>
        /// <param name="leaveRequest"></param>
        /// <param name="holiday"></param>
        /// <param name="obRequest"></param>
        /// <param name="isRestday"></param>
        public AttendanceDateStatus(LeaveRequest leaveRequest, Holiday holiday, OffBusinessRequest obRequest, bool isRestday, bool shiftStartsPrevDay, int? employeeLocationId)
        {

            IsRestday = isRestday;

            IsHoliday = holiday != null &&
                (holiday.LocationId == null || (holiday.LocationId != null && holiday.LocationId == employeeLocationId));
            IsHalfdayHoliday = IsHoliday && (holiday?.Halfday ?? false);
            IsHalfdayHolidayFirstHalf = IsHoliday && (holiday?.HolidayAM ?? false);
            HolidayDescription = holiday?.Description ?? string.Empty;

            OnLeave = leaveRequest != null;
            OnHalfdayLeave = leaveRequest?.HalfdayLeave() ?? false;
            OnHalfdayLeaveFirstHalf = leaveRequest?.HalfdayLeaveFirstHalf ?? false;

            HasOBrequest = obRequest != null;

            HasWholedayOBrequest = obRequest?.ObEntriesHasNoTime() ?? false;

            ShiftStartsPrevDay = shiftStartsPrevDay;
        }




        public bool IsRegularDay()
        {
            return (!IsRestday && !IsHoliday);
        }


        // Leave
        public bool OnWholedayLeave()
        {
            return (OnLeave && !OnHalfdayLeave);
        }

        public bool OnHalfdayLeaveAM()
        {
            return (OnLeave && OnHalfdayLeaveFirstHalf);
        }

        public bool OnHalfdayLeavePM()
        {
            return (OnLeave && !OnHalfdayLeaveFirstHalf);
        }


        // Holiday
        public bool IsWholedayHoliday()
        {
            return (IsHoliday && !IsHalfdayHoliday);
        }

        public bool IsHalfdayHolidayAM()
        {
            return IsHalfdayHoliday && IsHalfdayHolidayFirstHalf;
        }

        public bool IsHalfdayHolidayPM()
        {
            return IsHalfdayHoliday && !IsHalfdayHolidayFirstHalf;
        }







        /// <summary>
        /// True if no work whole day (restday, leave, holiday, OB)
        /// </summary>
        /// <returns></returns>
        public bool WholeDayAuthorizedOffWork()
        {
            return IsRestday ||
                   OnWholedayLeave() ||
                   IsWholedayHoliday() ||
                   HasWholedayOBrequest;
        }

        /// <summary>
        /// Returns true if whole day work (not restday/leave/ob/holiday)
        /// </summary>
        /// <returns></returns>
        public bool WholeDayWorkingHours()
        {
            return !WholeDayAuthorizedOffWork() && !HalfdayAuthorizedOffWork();
        }


        public bool HalfdayAuthorizedOffWork()
        {
            return HalfdayAmAuthorizedOffWork() || HalfdayPmAuthorizedOffWork();
        }


        public bool HalfdayAmAuthorizedOffWork()
        {
            //TODO: include OB
            return OnHalfdayLeaveAM() || IsHalfdayHolidayAM();
        }


        public bool HalfdayPmAuthorizedOffWork()
        {
            //TODO: include OB
            return OnHalfdayLeavePM() || IsHalfdayHolidayPM();
        }
    }
}
