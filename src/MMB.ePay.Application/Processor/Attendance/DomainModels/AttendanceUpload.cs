﻿using System;

namespace Tk.Domain
{
    public class AttendanceUpload
    {

        public int EmployeeId { get; set; }

        public DateTime Date { get; set; }

        public DateTime? TimeIn { get; set; }

        public DateTime? LunchBreaktimeIn { get; set; }

        public DateTime? LunchBreaktimeOut { get; set; }

        public DateTime? PMBreaktimeIn { get; set; }

        public DateTime? PMBreaktimeOut { get; set; }

        public DateTime? TimeOut { get; set; }
    }
    
}
