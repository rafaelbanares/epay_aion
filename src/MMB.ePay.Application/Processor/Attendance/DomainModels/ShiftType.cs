﻿namespace Tk.Domain
{


    public class ShiftType
    {
        public int Id { get; set; }

        public string Description { get; set; }
        
        /// <summary>
        /// Minimum overtime in minutes
        /// </summary>
        public int MinimumOvertime { get; set; }

        /// <summary>
        /// Flexible TimeIn allowed in minutes
        /// </summary>
        public int FlexiTime { get; set; }


        public int GraceTime { get; set; }


    }


    
}
