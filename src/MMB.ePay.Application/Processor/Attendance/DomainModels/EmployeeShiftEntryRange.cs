﻿using MMB.ePay.Processor.Dtos;
using System;

namespace Tk.Domain
{
    public class EmployeeShiftEntryRange
    {
        public int EmployeeId { get; set; }

        public DateTime Date { get; set; }

        public DateTime TimeIn { get; set; }

        public DateTime TimeOut { get; set; }

        public DateRange ValidTimeIn { get; set; }

        public DateRange ValidTimeOut { get; set; }
    }
}
