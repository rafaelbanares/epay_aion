﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk.Domain
{

    public interface IEmployeeShift
    {
        int EmployeeId { get; set; }

        DateTime Date { get; set; }

        DateTime TimeIn { get; set; }

        DateTime BreakTimeIn { get; set; }

        DateTime BreakTimeOut { get; set; }

        DateTime TimeOut { get; set; }

        int FlexibleMinutes { get; set; }

        int GraceMinutes { get; set; }

        int FirstHalfTotalMinutes();

        int LastHalfTotalMinutes();

        int BreakTotalMinutes();

        IEmployeeShift CreateFlexShift(DateTime actualTimeIn);

        bool WithinFlexiRange(DateTime actualTimeIn);

        bool IsFlexiShift();

        bool WithinGracePeriod(DateTime actualTimeIn);

        bool HasGracePeriod();
    }
     

}
