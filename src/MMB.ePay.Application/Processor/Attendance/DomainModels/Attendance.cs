﻿using System.Collections.Generic;
using System.Text;


namespace Tk.Domain
{
    public class Attendance : AttendanceBase
    {        
        public int ShiftTypeId { get; set; }

        public string RestDayCode { get; set; }

        public string Remarks { get; set; }

        public bool EarlyLoginApproved { get; set; }

        public bool IsRestDay()
        {            
            return RestDayCode.Contains(Date.Dow().ToString());
        }

        public bool Invalid { get; set; }
    }
}
