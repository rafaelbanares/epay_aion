﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk.Domain
{ 
     

    public class Overtime
    {
        public int EmployeeId { get; set; }
        public DateTime Date { get; set; }
        public int OvertimeTypeId { get; set; }
        public decimal Hours { get; set; }
        public int Minutes { get; set; }
    }

}
