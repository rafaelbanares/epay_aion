﻿using System;
using System.Collections.Generic;
using Tk.Domain;
using System.Linq;

namespace Tk
{

    public class OvertimeCalculator
    {
        private readonly NightDifferential _nightDiff;

        #region CTOR
        public OvertimeCalculator(NightDifferential ndiff)
        {
            MaxOvertimeWithoutBreak = 0;  //default to no break
            MinimumOvertime = 30;           //default to 30 mins

            _nightDiff = ndiff;
        }
        #endregion


        #region Notes
        // Payable Start of Overtime 
        //         -system will use actual ot_start or approved ot_start whichever is higher
        //==================================================================================
        //1. RESTDAY or WHOLEDAY HOLIDAY        (ok)
        //    ot starts:    attend.TimeIn   | author.TimeIn
        //    ot ends:      attend.Timeout  | author.TimeOut

        //2. HALFDAY HOLIDAY
        //  (a1)workStart_PM && holidayPM(ok)
        //    ot starts:    payable.BreakTimeOut | author.TimeIn
        //    ot ends:      attend.Timeout       | author.TimeOut
        //
        //  (a2)workStart_AM && holidayPM: regwork in AM, possible hol_ot in PM  (ok)
        //     Holiday_OT
        //          ot starts:   payable.breakTimeOut | author.TimeIn
        //          ot ends:    attend.Timeout        | author.TimeOut
        //
        //  (b1)workStart_PM && holidayAM: regwork in PM, ot may start at end of payable regwork  (ok)
        //    ot starts:    payable.TimeOut | author.TimeIn
        //    ot ends:      attend.Timeout  | author.TimeOut
        //
        //  (b2)workStart_AM && holidayAM: hol_ot in AM, regwork in PM, possible reg_ot after shift (ok)
        //     Holiday_OT
        //          ot starts:    attend.TimeIn       | author.TimeIn
        //          ot ends:      payable.BreakTimeIn | author.TimeOut
        //
        //     Regular_OT   (not yet implemented)
        //          ot starts:     payable.TimeOut
        //          ot ends:      attend.Timeout  | author.TimeOut
        //3. REGULAR DAY
        #endregion




        public List<ProcessedOvertime> GetOvertimeList(AttendanceDateStatus adtStatus, AttendanceBase payable, Attendance attendance, OvertimeRequest otr, Holiday holiday)
        {
            var processedOT = new List<ProcessedOvertime>();

            if (attendance.NoEntry()) return processedOT;
            if (attendance.InValidEntry()) return processedOT;
            if (otr == null || otr?.Date.Date != attendance.Date) return processedOT;

            // 1. Regular day
            if (adtStatus.IsRegularDay())
            {
                var hasOTb4shift = false;
                var hasOTafterShift = false;

                // Overtime before shift
                if (payable.WithEntryInAM() && otr.AuthorizedStart < payable.TimeIn)
                {
                    var amOT = GetOvertimeList(attendance.EmployeeId,
                                           attendance.Date,
                                           OvertimeConstants.Regular,
                                           (DateTime)attendance.TimeIn,
                                           (DateTime)payable.TimeIn,
                                           otr,
                                           adtStatus.ShiftStartsPrevDay);

                    hasOTb4shift = amOT.Count > 0;

                    processedOT.AddRange(amOT);
                }


                // Overtime after shift
                if (payable.WithEntryInPM())
                {
                    var pmOT = GetOvertimeList(attendance.EmployeeId,
                                            attendance.Date,
                                            OvertimeConstants.Regular,
                                            (DateTime)payable.TimeOut,
                                            (DateTime)attendance.RawTimeOut,
                                            otr,
                                            adtStatus.ShiftStartsPrevDay);

                    hasOTafterShift = pmOT.Count > 0;

                    processedOT.AddRange(pmOT);
                }


                //summarize if both have ot before and after shift                
                return (hasOTb4shift && hasOTafterShift) ? Summarize(processedOT) : processedOT;
            }


            // 2. restday or holiday

            //wholeday OT (possible to be both restday and wholeday holiday)
            if ((adtStatus.IsRestday || adtStatus.IsWholedayHoliday()) && !adtStatus.IsHalfdayHoliday)
            {
                var otcode = GetOTcode(adtStatus.IsRestday, holiday);

                processedOT = GetOvertimeList(attendance.EmployeeId,
                                        attendance.Date,
                                        otcode,
                                        (DateTime)attendance.RawTimeIn,
                                        (DateTime)attendance.RawTimeOut,
                                        otr,
                                        adtStatus.ShiftStartsPrevDay);
            }

            #region
            //halfday holiday OT
            if (adtStatus.IsHalfdayHoliday)
            {
                //throw new Exception("Halfday Holiday OT not yet implemented");                
                //var hdOt = GetHalfdayHolidayOvertime(adtStatus, payable, attendance, otr, holiday);
                //otlist.AddRange(hdOt);
            }
            #endregion



            return processedOT;

        }

        /// <summary>
        /// Summarize ot before shift and after shift. 
        /// </summary>
        /// <param name="otList"></param>
        /// <returns></returns>
        private List<ProcessedOvertime> Summarize(List<ProcessedOvertime> otList)
        {

            // summarize regOT (first8 and beyond8)
            var summarizedRegOT = (from e in otList
                                   where e.SubType == OvertimeSubTypes.First8 || e.SubType == OvertimeSubTypes.Beyond8
                                   group e by new { e.EmployeeId, e.Date, e.OvertimeCode } into g
                                   select new
                                   {
                                       EmployeeId = g.Key.EmployeeId,
                                       Date = g.Key.Date,
                                       OvertimeCode = g.Key.OvertimeCode,
                                       OvertimeMinutes = g.Sum(e => e.OvertimeMinutes)
                                   })
                        .ToList();

            // split back regOT (first8 and beyond8)
            var listRegOT = new List<ProcessedOvertime>();
            foreach (var o in summarizedRegOT)
            {
                var first8 = (o.OvertimeMinutes > 480) ? 480 : o.OvertimeMinutes;
                listRegOT.Add(new ProcessedOvertime
                {
                    EmployeeId = o.EmployeeId,
                    Date = o.Date,
                    OvertimeCode = o.OvertimeCode,
                    SubType = OvertimeSubTypes.First8,
                    OvertimeMinutes = first8
                });

                var beyond8 = (o.OvertimeMinutes > 480) ? o.OvertimeMinutes - 480 : 0;
                if (beyond8 > 0)
                {
                    listRegOT.Add(new ProcessedOvertime
                    {
                        EmployeeId = o.EmployeeId,
                        Date = o.Date,
                        OvertimeCode = o.OvertimeCode,
                        SubType = OvertimeSubTypes.Beyond8,
                        OvertimeMinutes = beyond8
                    });
                }
            }

            // ND1 and ND2 OT
            var ndOT = (from e in otList
                        where e.SubType != OvertimeSubTypes.First8 && e.SubType != OvertimeSubTypes.Beyond8
                        group e by new { e.EmployeeId, e.Date, e.OvertimeCode, e.SubType } into g
                        select new ProcessedOvertime
                        {
                            EmployeeId = g.Key.EmployeeId,
                            Date = g.Key.Date,
                            OvertimeCode = g.Key.OvertimeCode,
                            SubType = g.Key.SubType,
                            OvertimeMinutes = g.Sum(e => e.OvertimeMinutes)
                        })
                        .ToList();

            var summarizedOT = new List<ProcessedOvertime>();
            summarizedOT.AddRange(listRegOT);
            summarizedOT.AddRange(ndOT);

            return summarizedOT;
        }


        //----------------------------------

        /// <summary>
        /// 
        /// </summary>
        /// <param name="employeeId">Employee Id</param>
        /// <param name="attendDate">Date of attendance</param>
        /// <param name="otcode">Overtime code</param>
        /// <param name="otStarted">actual overtime started</param>
        /// <param name="otEnded">actual overtime ended</param>
        /// <returns></returns>
        private List<ProcessedOvertime> GetOvertimeList(int employeeId, DateTime attendDate, string otcode, DateTime otStarted, DateTime otEnded, OvertimeRequest otr, bool shiftStartsPrevDay)
        {
            var payableOTstart = Helper.Largest(otStarted, otr.AuthorizedStart);
            var payableOTend = Helper.Least(otEnded, otr.AuthorizedEnd);

            var totalOvertime = ComputeOT(payableOTstart, payableOTend);

            // apply minimum ot Policy
            if (totalOvertime < MinimumOvertime)
                return new List<ProcessedOvertime>();

            //1. first 8 hours overtime
            var first8 = (totalOvertime > 480) ? 480 : totalOvertime;

            var processedOvertime = new List<ProcessedOvertime>
            {
                new ProcessedOvertime
                {
                    EmployeeId = employeeId,
                    Date = attendDate,
                    OvertimeCode = otcode,
                    SubType = OvertimeSubTypes.First8,
                    OvertimeMinutes = first8
                }
            };

            //2. beyond 8 hours overtime
            var beyond8 = (totalOvertime > 480) ? totalOvertime - 480 : 0;

            if (beyond8 > 0)
            {
                processedOvertime.Add(new ProcessedOvertime
                {
                    EmployeeId = employeeId,
                    Date = attendDate,
                    OvertimeCode = otcode,
                    SubType = OvertimeSubTypes.Beyond8,
                    OvertimeMinutes = beyond8
                });
            }

            //3. ND1 overtime
            var nd1 = _nightDiff.ComputeND1Overtime(attendDate, payableOTstart, payableOTend, shiftStartsPrevDay);

            if (nd1 > 0)
            {
                processedOvertime.Add(new ProcessedOvertime
                {
                    EmployeeId = employeeId,
                    Date = attendDate,
                    OvertimeCode = otcode,
                    SubType = OvertimeSubTypes.Nd1,
                    OvertimeMinutes = nd1
                });
            }

            //4. ND2 overtime
            var nd2 = _nightDiff.ComputeND2Overtime(attendDate, payableOTstart, payableOTend, shiftStartsPrevDay);

            if (nd2 > 0)
            {
                processedOvertime.Add(new ProcessedOvertime
                {
                    EmployeeId = employeeId,
                    Date = attendDate,
                    OvertimeCode = otcode,
                    SubType = OvertimeSubTypes.Nd2,
                    OvertimeMinutes = nd2
                });
            }

            return processedOvertime;
        }



        public List<ProcessedOvertime> GetHalfdayHolidayOvertime(AttendanceDateStatus attDateInfo, AttendanceBase payable, Attendance attendance, OvertimeRequest otr, Holiday holiday)
        {
            var empty = new List<ProcessedOvertime>();
            if (!attDateInfo.IsHalfdayHoliday) return empty;


            // halfday holiday in afternoon
            if (holiday.HolidayPM())
            {
                if (attendance.WithEntryInPM())
                {
                    var payableOTstart = Helper.Largest((DateTime)payable.BreakTimeOut, otr.AuthorizedStart);
                    var payableOTend = Helper.Least((DateTime)attendance.TimeOut, otr.AuthorizedEnd);

                    return new List<ProcessedOvertime>
                    {
                        new ProcessedOvertime
                        {
                        EmployeeId = attendance.EmployeeId,
                        Date = attendance.Date,
                        OvertimeCode = GetOTcode(attDateInfo.IsRestday, holiday),
                        SubType = OvertimeSubTypes.First8,
                        OvertimeMinutes = ComputeOT(payableOTstart, payableOTend)
                        }
                    };
                }

                return empty;
            }

            // halfday holiday in morning
            if (holiday.HolidayAM)
            {
                // hday holidayAM && workStart_AM 
                //   hol_ot in AM, possible regwork in PM, possible reg ot after regwork 
                if (attendance.WithEntryInAM())
                {
                    //hol_ot in AM                        
                    var payableOTstart = Helper.Largest((DateTime)attendance.RawTimeIn, otr.AuthorizedStart);
                    var payableOTend = Helper.Least((DateTime)attendance.RawTimeOut,
                                                    (DateTime)payable.BreakTimeIn,
                                                    otr.AuthorizedEnd);

                    var hdOTlist = new List<ProcessedOvertime>
                    {
                        new ProcessedOvertime
                        {
                            EmployeeId = attendance.EmployeeId,
                            Date = attendance.Date,
                            OvertimeCode = GetOTcode(attDateInfo.IsRestday, holiday),
                            SubType = OvertimeSubTypes.First8,
                            OvertimeMinutes = ComputeOT(payableOTstart, payableOTend)
                        }
                    };

                    //If restday and worked continued on the 2nd half of shift
                    if (attDateInfo.IsRestday && attendance.RawTimeOut > otr.AuthorizedEnd)
                    {
                        var payOTstart = Helper.Largest((DateTime)payable.BreakTimeOut, otr.AuthorizedStart);
                        var payOTend = Helper.Least((DateTime)attendance.RawTimeOut, otr.AuthorizedEnd);

                        hdOTlist.Add(new ProcessedOvertime
                        {
                            EmployeeId = attendance.EmployeeId,
                            Date = attendance.Date,
                            OvertimeCode = OvertimeConstants.RestDay,
                            SubType = OvertimeSubTypes.First8,
                            OvertimeMinutes = ComputeOT(payOTstart, payOTend)
                        });
                    }

                    return hdOTlist;
                }

                //holidayAM && workStart_PM
                else
                {
                    //If restday and approved ot 
                    if ((DateTime)attendance.RawTimeOut > otr.AuthorizedEnd)
                    {
                        var payOTstart = Helper.Largest((DateTime)payable.BreakTimeOut, otr.AuthorizedStart);
                        var payOTend = Helper.Least((DateTime)attendance.TimeOut, otr.AuthorizedEnd);

                        return new List<ProcessedOvertime>
                        {
                            new ProcessedOvertime
                            {
                                EmployeeId = attendance.EmployeeId,
                                Date = attendance.Date,
                                OvertimeCode = OvertimeConstants.RestDay,
                                SubType = OvertimeSubTypes.First8,
                                OvertimeMinutes = ComputeOT(payOTstart, payOTend)
                            }
                        };
                    }
                }
            }

            return empty;
        }


        /// <summary>
        /// Returns overtime code based on this format:
        /// [HolidayType] + "RST"
        /// </summary>
        /// <param name="isRestday"></param>
        /// <param name="holiday"></param>
        /// <returns></returns>
        public string GetOTcode(bool isRestday, Holiday holiday)
        {
            if (!isRestday && holiday == null)
                return OvertimeConstants.Regular;

            var holiType = holiday?.HolidayType ?? string.Empty;
            var rst = isRestday ? OvertimeConstants.RestDay : string.Empty;
            var conj = holiType.IsFilled() && rst.IsFilled() ? "+" : string.Empty;

            return holiType + conj + rst;
        }





        /// <summary>
        /// Returns overtime in minutes
        /// </summary>
        /// <param name="payableOtStart"></param>
        /// <param name="payableOtEnd"></param>
        /// <param name="otr"></param>
        /// <returns></returns>
        public int ComputeOT(DateTime payableOtStart, DateTime payableOtEnd)
        {
            int totalOTmins = (int)(payableOtEnd - payableOtStart).TotalMinutes;

            //less 1hr break
            if (totalOTmins > MaxOvertimeWithoutBreak && MaxOvertimeWithoutBreak > 0)
            {
                totalOTmins -= 60;
                if (totalOTmins < MaxOvertimeWithoutBreak)
                    totalOTmins = MaxOvertimeWithoutBreak;
            }
            return totalOTmins;
        }




        /// <summary>
        /// Less 1 hr break if total overtime exceeds this value.
        /// Please enter value in minutes.
        /// </summary>
        public int MaxOvertimeWithoutBreak { get; set; }


        /// <summary>
        /// Minimum Overtime (in minutes)
        /// </summary>
        public int MinimumOvertime { get; set; }


    }
}
