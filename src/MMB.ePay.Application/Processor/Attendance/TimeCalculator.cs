﻿using System;
using System.Collections.Generic;
using Tk.Domain;

namespace Tk
{
    public class TimeCalculator
    {
        private readonly AttendanceBase _adjustedPayable;   //adjusted by ob
        private readonly AttendanceDateStatus _adtStatus;
        private readonly IEmployeeShift _projected;
        private readonly Employee _employee;


        public TimeCalculator(AttendanceDateStatus adtStatus, Employee employee, IEmployeeShift projected, AttendanceBase adjustedPayable)
        {
            _employee = employee;
            _projected = projected;
            _adjustedPayable = adjustedPayable;
            _adtStatus = adtStatus;
        }



        public static int ComputeTardy(DateTime payable_in, DateTime shift_in)
        {
            return (payable_in <= shift_in) ? 0 : (int)(payable_in - shift_in).TotalMinutes;
        }



        /// <summary>
        /// scenario: 11:30AM - 4:30PM  (shift: 8:30-5:30 flexi)
        /// halfday will be 10:00-2:00PM.
        /// returns minutes
        /// </summary>
        /// <param name="payable"></param>
        /// <param name="projected"></param>
        /// <returns></returns>
        public int ComputeTardy()
        {
            if (_employee.TardyExempt || _adtStatus.WholeDayAuthorizedOffWork() || _adjustedPayable.NoEntry())
                return 0;

            int trd = 0;
            if (_adjustedPayable.WithEntryInAM())   //AM entry exists
            {
                trd = ComputeTardy((DateTime)_adjustedPayable.TimeIn, _projected.TimeIn);
            }

            // This will handle tardy in the ff scenarios:
            // 1. OB in the morning but late TimeIn in PM
            // 2. half-day work in PM but late
            if (_adjustedPayable.WithEntryInPM())   //PM entry exists
                trd += ComputeTardy((DateTime)_adjustedPayable.BreakTimeOut, _projected.BreakTimeOut);

            return trd;
        }




        //Undertime
        private static int ComputeUT(DateTime payable_out, DateTime shift_out)
        {
            return (payable_out >= shift_out) ? 0 : (int)(shift_out - payable_out).TotalMinutes;
        }


        public int ComputeUT()
        {
            if (_employee.UtExempt || _adtStatus.WholeDayAuthorizedOffWork() || _adjustedPayable.NoEntry())
                return 0;

            int ut = 0;
            if (_adjustedPayable.WithEntryInPM())    //entry exist in PM 
                ut = ComputeUT((DateTime)_adjustedPayable.TimeOut, _projected.TimeOut);

            if (_adjustedPayable.WithEntryInAM())    //entry exist in AM 
                ut += ComputeUT((DateTime)_adjustedPayable.BreakTimeIn, _projected.BreakTimeIn);

            return ut;
        }




        /// <summary>
        /// Returns the net regular work in minutes
        /// 
        /// </summary>
        /// <param name="payable"></param>
        /// <param name="attDateState"></param>
        /// <returns></returns>
        public int ComputeNetRegWork()
        {
            if (_adtStatus.WholeDayAuthorizedOffWork() || _adjustedPayable.NoEntry())
                return 0;

            int regWork = 0;

            if (_adjustedPayable.WithEntryInAM())    //entry exist in AM 
                regWork += (int)((DateTime)_adjustedPayable.BreakTimeIn - (DateTime)_adjustedPayable.TimeIn).TotalMinutes;

            if (_adjustedPayable.WithEntryInPM())    //entry exist in PM 
                regWork += (int)((DateTime)_adjustedPayable.TimeOut - (DateTime)_adjustedPayable.BreakTimeOut).TotalMinutes;

            return regWork;
        }




        /// <summary>
        /// Returns absences in days
        /// 
        /// </summary>
        /// <param name="payable"></param>
        /// <param name="attDateState"></param>
        /// <returns></returns>
        public decimal ComputeAbsence()
        {
            if (_adjustedPayable == null) return 0;
            if (_adjustedPayable.WholeDay()) return 0;
            if (_employee.NonSwiper) return 0;

            //wholeday no work
            if (_adtStatus.WholeDayAuthorizedOffWork()) return 0;

            //wholeday working hours
            if (_adtStatus.WholeDayWorkingHours())
                return _adjustedPayable.NoEntry() ? 1 : _adjustedPayable.Halfday() ? .5M : 0;

            //halfday working hours
            if (_adtStatus.HalfdayAuthorizedOffWork())
            {
                if (_adjustedPayable.NoEntry()) return .5M;

                if (_adtStatus.HalfdayAmAuthorizedOffWork())
                    return _adjustedPayable.WorkedHalfdayInPM() ? 0 : .5M;

                if (_adtStatus.HalfdayPmAuthorizedOffWork())
                    return _adjustedPayable.WorkedHalfdayInAM() ? 0 : .5M;
            }

            return 0;
        }


        /// <summary>
        /// Computes paid holiday
        /// </summary>
        /// <param name="attDateState"></param>
        /// <returns></returns>
        public decimal ComputePaidHoliday()
        {
            //return _adtStatus.IsWholedayHoliday() ? 1 : _adtStatus.IsHalfdayHoliday ? .5M : 0;
            return 0;
        }






        // private methods
        //--------------------------------------------

        /// <summary>
        /// Get off work in days
        /// </summary>
        /// <param name="attDateState"></param>
        /// <returns></returns>
        private decimal GetAuthorizedHalfdayOffWork()
        {
            decimal offWork = 0;

            if (_adtStatus.OnHalfdayLeave)
                offWork += .5M;

            if (_adtStatus.IsHalfdayHoliday)
                offWork += .5M;

            return offWork;
        }

    }
}
