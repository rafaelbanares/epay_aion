﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk
{
    public class HolidayConstants
    {
        public const string Legal = "L";
        public const string Special = "S";
        public const string LegalSpecial = "LS";
        public const string DoubleLegal = "D";
        public const string Company = "C";
        public const string Regional = "R";
    }
}
