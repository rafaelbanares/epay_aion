﻿using System;
using System.Collections.Generic;
using System.Text;
using Tk.Domain;


namespace Tk
{

    public class NightDifferential
    {
        #region CTOR

        public NightDifferential()
        {
            // default nd1: 10pm-2am
            Nd1Range = new TimeRange { Start = new TimeSpan(22, 0, 0), End = new TimeSpan(2, 0, 0) };
            // default nd2: 2am-6am
            Nd2Range = new TimeRange { Start = new TimeSpan(2, 0, 0), End = new TimeSpan(6, 0, 0) };
        }

        public NightDifferential(TimeRange nd1Range, TimeRange nd2Range)
        {
            Nd1Range = nd1Range;
            Nd2Range = nd2Range;
        }

        #endregion

        public TimeRange Nd1Range { get; private set; }

        public TimeRange Nd2Range { get; private set; }



        /// <summary>
        /// Compute regular ND1. 
        /// </summary>
        /// <param name="attendanceDate">Attendance Date</param>
        /// <param name="payable"></param>
        /// <param name="adtStatus"></param>
        /// <param name="shiftStartsPrevDay"></param>
        /// <returns></returns>
        public int ComputeND1Regular(DateTime attendanceDate, AttendanceBase payable, AttendanceDateStatus adtStatus)
        {
            var nd1StartDate = adtStatus.ShiftStartsPrevDay ? attendanceDate.AddDays(-1) : attendanceDate;
            return ComputeNDRegular(GetND1Range(nd1StartDate), payable, adtStatus);
        }

        /// <summary>
        /// Compute regular ND2. 
        /// </summary>
        /// <param name="attendanceDate">Attendance Date</param>
        /// <param name="payable"></param>
        /// /// <param name="adtStatus"></param>
        /// <param name="shiftStartsPrevDay"></param>
        /// <returns></returns>
        public int ComputeND2Regular(DateTime attendanceDate, AttendanceBase payable, AttendanceDateStatus adtStatus)
        {
            var nd2StartDate = adtStatus.ShiftStartsPrevDay ? attendanceDate : attendanceDate.AddDays(1);
            return ComputeNDRegular(GetND2Range(nd2StartDate), payable, adtStatus);
        }


        /// <summary>
        /// Compute ND1 Overtime
        /// </summary>
        /// <param name="attendanceDate">Attendance Date</param>
        /// <param name="otStart"></param>
        /// <param name="otEnd"></param>
        /// <returns></returns>
        public int ComputeND1Overtime(DateTime attendanceDate, DateTime otStart, DateTime otEnd, bool shiftStartsPrevDay)
        {
            var nd1StartDate = shiftStartsPrevDay ? attendanceDate.AddDays(-1) : attendanceDate;
            var nd1 = ComputeND(GetND1Range(nd1StartDate), otStart, otEnd);

            //try prev day (just in case it is OT before shift)
            if (nd1 == 0)
                nd1 = ComputeND(GetND1Range(nd1StartDate.AddDays(-1)), otStart, otEnd);

            return nd1;
        }


        /// <summary>
        /// Compute ND2 Overtime
        /// </summary>
        /// <param name="attendanceDate">Attendance Date</param>
        /// <param name="otStart"></param>
        /// <param name="otEnd"></param>
        /// <returns></returns>
        public int ComputeND2Overtime(DateTime attendanceDate, DateTime otStart, DateTime otEnd, bool shiftStartsPrevDay)
        {
            var nd2StartDate = shiftStartsPrevDay ? attendanceDate : attendanceDate.AddDays(1);
            var nd2 = ComputeND(GetND2Range(nd2StartDate), otStart, otEnd);

            //try prev day (just in case it is OT before shift)
            if (nd2 == 0)
                nd2 = ComputeND(GetND2Range(nd2StartDate.AddDays(-1)), otStart, otEnd);

            return nd2;
        }





        //------------------------------------
        private int ComputeNDRegular(NightdiffRange ndrange, AttendanceBase payable, AttendanceDateStatus adtStatus)
        {
            if (payable.NoEntry() || payable.InValidEntry() || adtStatus.WholeDayAuthorizedOffWork()) return 0;

            return ComputeND(ndrange, (DateTime)payable.RawTimeIn, (DateTime)payable.RawTimeOut);
        }


        private NightdiffRange GetND1Range(DateTime nd1StartDate)
        {
            return GetNightDiffRange(nd1StartDate, Nd1Range);
        }

        private NightdiffRange GetND2Range(DateTime nd2StartDate)
        {
            return GetNightDiffRange(nd2StartDate, Nd2Range);
        }

        private int ComputeND(NightdiffRange ndRange, DateTime timeIn, DateTime timeOut)
        {
            if (timeIn == timeOut) return 0;

            DateTime actualNdStart = Helper.Largest(timeIn, ndRange.NdStart);
            DateTime actualNdEnd = Helper.Least(timeOut, ndRange.NdEnd);

            if (actualNdStart > actualNdEnd) return 0;

            return (int)(actualNdEnd - actualNdStart).TotalMinutes;
        }


        private NightdiffRange GetNightDiffRange(DateTime date, TimeRange ndTimeRange)
        {
            var nd = new NightdiffRange
            {
                NdStart = date.Date.Add(ndTimeRange.Start),
                NdEnd = date.Date.Add(ndTimeRange.End)
            };

            if (nd.NdEnd < nd.NdStart)
                nd.NdEnd += new TimeSpan(24, 0, 0);

            return nd;
        }



    }


}
