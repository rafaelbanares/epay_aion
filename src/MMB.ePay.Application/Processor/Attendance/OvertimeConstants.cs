﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk
{



    public class OvertimeConstants
    {
        public const string Regular = "REG";
        public const string RestDay = "RST";
        public const string Legal = HolidayConstants.Legal;
        public const string Special = HolidayConstants.Special;
        public const string SpecialRestday = HolidayConstants.Special + "+" + RestDay;
        public const string LegalRestday = HolidayConstants.Legal + "+" + RestDay;
        public const string LegalSpecial = HolidayConstants.LegalSpecial;
        public const string LegalSpecialRestday = HolidayConstants.LegalSpecial + "+" + RestDay;
        public const string DoubleLegal = HolidayConstants.DoubleLegal;
        public const string DoubleLegalRestday = HolidayConstants.DoubleLegal + "+" + RestDay;

    }

    public enum OvertimeSubTypes
    {
        First8 = 1,
        Beyond8 = 2,
        Nd1 = 3,
        Nd2 = 4
    }




}
