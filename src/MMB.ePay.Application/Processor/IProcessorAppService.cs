﻿using Abp.Application.Services;
using System.Threading.Tasks;

namespace MMB.ePay.Processor
{
    public interface IProcessorAppService : IApplicationService
    {
        Task AutomatedProcess(int tenantId);
        Task Process(int periodId, int? employeeId);
        Task Process(int tenantId, int periodId, int? employeeId);
        Task<bool> Post(int tenantId, int periodId, bool isForced);
    }
}