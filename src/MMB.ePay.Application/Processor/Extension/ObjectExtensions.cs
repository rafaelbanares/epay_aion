﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tk
{
 
    public static class ObjectExtensions
    {
     
        public static void ThrowIfNull<T>(this T targetObject, string message) where T:class
        {
            if (targetObject == null)
            {
                throw new NullReferenceException(message);
            }
        }
    }
}
