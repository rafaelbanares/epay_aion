﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using System.Xml;
using System.Web;
using System.Globalization;

namespace Tk
{
    
    public static class StringExtensions
    {
        static Dictionary<string, Regex> items = new Dictionary<string, Regex>();

        public static bool Like(this string item, string searchPattern)
        {
            return GetRegex("^" + searchPattern).IsMatch(item);
        }

        public static string Search(this string item, string searchPattern)
        {
            var match = GetRegex(searchPattern).Match(item);
            if (match.Success)
            {
                return item.Substring(match.Index, match.Length);
            }
            return null;
        }

        static Regex GetRegex(string searchPattern)
        {
            if (!items.Keys.Contains(searchPattern))
            {
                string regex = searchPattern
                    .Replace("\\", "\\\\")
                    .Replace(".", "\\.")
                    .Replace("{", "\\{")
                    .Replace("}", "\\}")
                    .Replace("[", "\\[")
                    .Replace("]", "\\]")
                    .Replace("+", "\\+")
                    .Replace("$", "\\$")
                    .Replace(" ", "\\s")
                    .Replace("#", "[0-9]")
                    .Replace("?", ".")
                    .Replace("*", "\\w*")
                    .Replace("%", ".*")
                    ;
                items[searchPattern] = new Regex(regex, RegexOptions.IgnoreCase);
            }
            return items[searchPattern];
        }

        public static XmlNode ReadXml(this string item, string sectionName)
        {
            XmlDocument document = new XmlDocument();
            document.LoadXml(item);
            XmlNode node = document.SelectSingleNode(sectionName);
            return node;
        }

        /// <summary>
        /// Removes all leading and trailing white-space characters from the current
        /// <c>System.String</c> object.
        /// </summary>
        /// <param name="item">A <c>System.String</c> reference.</param>
        /// <returns>
        /// The string that remains after all white-space characters are removed from
        /// the start and end of the current System.String object.
        /// </returns>
        public static string SafeTrim(this string item)
        {
            return item.IsNullOrEmpty() ? String.Empty : item.Trim();
        }

        public static string Default(this string item, string value)
        {
            return (string.IsNullOrEmpty(item)) ? value : item;
        }

        public static string Safe(this string item)
        {
            return item.Default(string.Empty);
        }
        /// <summary>
        /// Throws an exception if the string is null or empty.
        /// </summary>
        /// <param name="item">A <c>System.String</c> reference.</param>
        public static void CanNotBeEmpty(this string item)
        {
            if (String.IsNullOrEmpty(item)) throw new Exception("Can not be null or empty");
        }

        public static void CanNotContain(this string item, string value)
        {
            if (value.IsNullOrEmpty()) return;
            if (item.Contains(value)) throw new Exception("String can not contain '" + value + "'");
        }

        /// <summary>
        /// Appends the <paramref name="toAppend"/> string from the current
        /// <c>System.String</c> object only if necessary (the current string
        /// object doesn't end with the <paramref name="toAppend"/> text).
        /// </summary>
        /// <param name="item">A <c>System.String</c> reference.</param>
        /// <param name="toAppend">The string to append.</param>
        /// <returns></returns>
        public static string AppendInCase(this string item, string toAppend)
        {
            return (!item.EndsWith(toAppend)) ? item.Append(toAppend) : item;
        }

        /// <summary>
        /// Appends the <paramref name="stringToAppend"/> string from the current
        /// <c>System.String</c> object.
        /// </summary>
        /// <param name="item">A <c>System.String</c> reference.</param>
        /// <param name="stringToAppend">The string to append.</param>
        /// <returns></returns>
        public static string Append(this string item, string stringToAppend)
        {
            return !string.IsNullOrEmpty(stringToAppend)
                       ? ((item != null) ? item + stringToAppend : stringToAppend)
                       : item;
        }

        /// <summary>
        /// Prepends the <paramref name="toPrepend"/> string from the current
        /// <c>System.String</c> object only if necessary (the current string
        /// object doesn't start with the <paramref name="toPrepend"/> text).
        /// </summary>
        /// <param name="item">A <c>System.String</c> reference.</param>
        /// <param name="toPrepend">The string to prepend.</param>
        /// <returns></returns>
        public static string PrependInCase(this string item, string toPrepend)
        {
            return (!item.StartsWith(toPrepend)) ? item.Prepend(toPrepend) : item;
        }

        /// <summary>
        /// Prepends the <paramref name="stringToPrepend"/> string from the current
        /// <c>System.String</c> object.
        /// </summary>
        /// <param name="item">A <c>System.String</c> reference.</param>
        /// <param name="stringToPrepend">The string to prepend.</param>
        /// <returns></returns>
        public static string Prepend(this string item, string stringToPrepend)
        {
            return !string.IsNullOrEmpty(stringToPrepend)
                       ? ((item != null) ? stringToPrepend + item : stringToPrepend)
                       : item;
        }

        public static string RemoveEnd(this string item, string stringToRemove)
        {
            return
                !string.IsNullOrEmpty(stringToRemove)
                && !string.IsNullOrEmpty(item)
                && item.EndsWith(stringToRemove) ? item.Substring(0, item.Length - stringToRemove.Length) : item;
        }
        public static string RemoveStart(this string item, string stringToRemove)
        {
            return
                !string.IsNullOrEmpty(stringToRemove)
                && !string.IsNullOrEmpty(item)
                && item.StartsWith(stringToRemove) ? item.Substring(stringToRemove.Length) : item;
        }

        /// <summary>
        /// Removes the white spaces.
        /// </summary>
        /// <param name="s">A <c>System.String</c> reference.</param>
        /// <returns></returns>
        public static string RemoveWhiteSpaces(this string s)
        {
            return string.IsNullOrEmpty(s) ? s : Regex.Replace(s, @"\s+", "").Trim();
        }

        /// <summary>
        /// Throws an exception if the concreteFeatureType is null or empty.
        /// </summary>
        /// <param name="s">The concreteFeatureType.</param>
        public static void CanNotBeNullOrEmpty(this string s)
        {
            if (s.IsNullOrEmpty()) throw new Exception("Can not be null or empty");
        }

        ///// <summary>
        ///// Indicates whether the specified <c>System.String</c> object is null or an 
        ///// <c>System.String.Empty</c> string.
        ///// </summary>
        ///// <param name="item">A <c>System.String</c> reference.</param>
        ///// <returns>
        ///// <c>true</c> if the value parameter is null or an empty string (""); 
        ///// otherwise, <c>false</c>.
        ///// </returns>
        public static bool IsNullOrEmpty(this string item)
        {
            return string.IsNullOrWhiteSpace(item);
        }

        /// <summary>
        /// Indicates whether the specified <c>System.String</c> object is not null or not an 
        /// <c>System.String.Empty</c> string.
        /// </summary>
        /// <param name="item">A <c>System.String</c> reference.</param>
        /// <returns>
        /// <c>true</c> if the value parameter is not null or not an empty string (""); 
        /// otherwise, <c>false</c>.
        /// </returns>
        public static bool IsFilled(this string item)
        {
            return !string.IsNullOrWhiteSpace(item);
        }

        /// <summary>
        /// Returns the <paramref name="defaultValue"/> string if the current
        /// <c>System.String</c> object is null or empty.
        /// </summary>
        /// <param name="item">A <c>System.String</c> reference.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static string DefaultIfNull(this string item, string defaultValue)
        {
            return item.IsNullOrEmpty() ? defaultValue : item;
        }

        /// <summary>
        /// Converts the string representation of the name or numeric value of an
        /// enumerated constants to the equivalent enumerated object.
        /// </summary>
        /// <typeparam name="T">The type of the enumeration (<c>enum</c>).</typeparam>
        /// <param name="s">The <see cref="System.String"/> concreteFeatureType.</param>
        /// <returns></returns>
        public static T ToEnum<T>(this string s)
        {
            return (T)Enum.Parse(typeof(T), s);
        }

        /// <summary>
        /// Formats the current <c>System.String</c> object applying the Camel Case format.
        /// </summary>
        /// <param name="item">A <c>System.String</c> reference.</param>
        /// <returns></returns>
        public static string ToCamelCase(this string item)
        {
            return string.IsNullOrEmpty(item) ? item : string.Concat(item.Substring(0, 1).ToLowerInvariant(), item.Substring(1, item.Length));
        }

        /// <summary>
        /// Converts the current <c>System.String</c> object to a HTML string.
        /// </summary>
        /// <param name="item">A <c>System.String</c> reference.</param>
        /// <returns></returns>
        public static string ToHtml(this string item)
        {
            return !item.IsNullOrEmpty() ? item.Replace(">", "&gt;").Replace("<", "&lt;").Replace('"'.ToString(), "&#34;").Replace("'", "&#39;").Replace(Environment.NewLine, "<br />").Replace("\n", "<br />") : item;
        }

        /// <summary>
        /// Encodes a URL string.
        /// </summary>
        /// <param name="item">A <c>System.String</c> reference.</param>
        /// <returns></returns>
        public static string ToEncodedUrl(this string item)
        {
            return !item.IsNullOrEmpty() ? HttpUtility.UrlEncode(item) : item;
        }

        /// <summary>
        /// Decodes a URL string.
        /// </summary>
        /// <param name="item">A <c>System.String</c> reference.</param>
        /// <returns></returns>
        public static string ToDecodedUrl(this string item)
        {
            return !item.IsNullOrEmpty() ? HttpUtility.UrlDecode(item) : item;
        }

        /// <summary>
        /// Escapes special charecters.
        /// </summary>
        /// <param name="item">A <c>System.String</c> reference.</param>
        /// <returns></returns>
        public static string ToEscaped(this string item)
        {
            return !item.IsNullOrEmpty()
                       ? item.Replace('\\'.ToString(), "\\\\").Replace("'", "\\'").Replace('"'.ToString(), "&#34;")
                       : item;
        }

        /// <summary>
        /// Initializes a new concreteFeatureType of the <c>System.Guid</c> class using the value represented
        /// by the current <c>System.String</c> object.
        /// </summary>
        /// <param name="value">A <c>System.String</c> reference.</param>
        /// <returns></returns>
        public static Guid ToGuid(this string value)
        {
            value.CanNotBeEmpty();
            return new Guid(value.Trim());
        }

        /// <summary>
        /// Initializes a new concreteFeatureType of the <c>System.Guid</c> class using the value represented
        /// by the current <c>System.String</c> object.
        /// If the current <c>System.String</c> object is null or empty, the method returns <c>null</c>.
        /// </summary>
        /// <param name="value">A <c>System.String</c> reference.</param>
        /// <returns></returns>
        public static Guid? ToNGuid(this string value)
        {
            return value.IsNullOrEmpty() ? (Guid?)null : new Guid(value.Trim());
        }

        /// <summary>
        /// Returns a list of <c>System.Guid</c> objects builded from the current
        /// <c>System.String</c> object. The current <c>System.String</c> object
        /// must be a sequence of Guid separated by the comma character.
        /// </summary>
        /// <param name="value">A <c>System.String</c> reference.</param>
        /// <returns></returns>
        public static IEnumerable<Guid> ToGuidList(this string value)
        {
            return value.IsNullOrEmpty() ? new List<Guid>() : new List<Guid>(HttpUtility.UrlDecode(value).Split(',').Select(item => item.ToGuid()));
        }

        /// <summary>
        /// Converts the current string representation of a date 
        /// and time to its <c>System.DateTime</c> equivalent.
        /// </summary>
        /// <param name="value">A string containing a date and time to convert.</param>
        /// <returns>
        /// A <c>System.DateTime</c> equivalent to the date and time contained in 
        /// current <c>System.String</c> object.
        /// </returns>
        public static DateTime? ToNDate(this string value)
        {
            DateTime result;
            return DateTime.TryParse(value.SafeTrim(), out result) ? result : (DateTime?)null;
        }

        /// <summary>
        /// Converts the current string representation of a date
        /// and time to its <c>System.DateTime</c> equivalent.
        /// </summary>
        /// <param name="value">A string containing a date and time to convert.</param>
        /// <param name="format">The format.</param>
        /// <returns>
        /// A <c>System.DateTime</c> equivalent to the date and time contained in
        /// current <c>System.String</c> object.
        /// </returns>
        public static DateTime? ToNDate(this string value, string format)
        {
            DateTime result;
            return DateTime.TryParseExact(value.SafeTrim(), format, null, DateTimeStyles.None, out result) ? result : (DateTime?)null;
        }

        /// <summary>
        /// Converts the current string representation of a number 
        /// to its 32-bit signed integer equivalent.
        /// </summary>
        /// <param name="value">A string containing a number to convert.</param>
        /// <returns>
        /// A 32-bit signed integer equivalent to the number contained in the
        /// current <c>System.String</c> object.
        /// </returns>
        public static int ToInt(this string value)
        {
            value.CanNotBeEmpty();
            return int.Parse(value);
        }

        /// <summary>
        /// Converts the current string representation of a number 
        /// to its 32-bit signed integer equivalent.
        /// If the current object is null or empty or if the conversion failed, the
        /// <paramref name="defaultValue" />
        ///   integer is returned.
        /// </summary>
        /// <param name="value">A string containing a number to convert.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        /// A 32-bit signed integer equivalent to the number contained in the
        /// current <c>System.String</c> object or the <paramref name="defaultValue"/>
        /// integer if the conversion failed.
        /// </returns>
        public static int ToInt(this string value, int defaultValue)
        {
            if (value.IsNullOrEmpty()) return defaultValue;
            int result;
            return int.TryParse(value.Trim(), out result) ? result : defaultValue;
        }

        /// <summary>
        /// Converts the current string representation of a number 
        /// to its nullable 32-bit signed integer equivalent.
        /// If the current object is null or empty or if the conversion failed, the
        /// <paramref name="defaultValue"/> nullable integer is returned.
        /// </summary>
        /// <param name="value">A string containing a number to convert.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        /// A nullable 32-bit signed integer equivalent to the number contained in the
        /// current <c>System.String</c> object or the <paramref name="defaultValue"/>
        /// integer if the conversion failed.
        /// </returns>
        public static int? ToInt(this string value, int? defaultValue)
        {
            int result;
            return int.TryParse(value.Trim(), out result) ? result : defaultValue;
        }

        /// <summary>
        /// Converts the current string representation of a logical 
        /// value to its <c>System.Boolean</c> equivalent.
        /// If the current object is null or empty or if the conversion failed, the
        /// <paramref name="defaultValue"/> boolean is returned.
        /// </summary>
        /// <param name="value">A string containing the value to convert.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        /// <c>true</c> if value is equivalent to <c>System.Boolean.TrueString</c>, 
        /// <c>false</c> if value is equivalent to <c>System.Boolean.FalseString</c> or
        /// the <paramref name="defaultValue"/> value if the conversion failed.
        /// </returns>
        public static bool ToBoolean(this string value, bool defaultValue)
        {
            bool result;
            return bool.TryParse(value.Trim(), out result) ? result : defaultValue;
        }

        /// <summary>
        /// Converts the current string representation of a logical 
        /// value to its <c>System.Boolean</c> equivalent.
        /// </summary>
        /// <param name="value">A string containing the value to convert.</param>
        /// <c>true</c> if value is equivalent to <c>System.Boolean.TrueString</c>;
        /// otherwise <c>false</c>.
        public static bool ToBoolean(this string value)
        {
            value.CanNotBeEmpty();
            return bool.Parse(value);
        }

        /// <summary>
        /// Converts the current string representation of a number 
        /// to its <c>System.Decimal</c> equivalent.
        /// </summary>
        /// <param name="value">The string representation of the number to convert.</param>
        /// <returns>
        /// The <c>System.Decimal</c> number equivalent to the number contained in the
        /// current <c>System.String</c> object.
        /// </returns>
        public static decimal ToDecimal(this string value)
        {
            value.CanNotBeEmpty();
            return decimal.Parse(value, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Initializes a new concreteFeatureType of the <c>System.Decimal</c> class using the value represented
        /// by the current <c>System.String</c> object.
        /// If the current <c>System.String</c> object is null or empty, the method returns <c>null</c>.
        /// </summary>
        /// <param name="value">A <c>System.String</c> reference.</param>
        /// <returns></returns>
        public static decimal? ToNDecimal(this string value)
        {
            return value.IsNullOrEmpty() ? (decimal?)null : value.ToDecimal();
        }

        /// <summary>
        /// Initializes a new concreteFeatureType of the <c>System.Int</c> class using the value represented
        /// by the current <c>System.String</c> object.
        /// If the current <c>System.String</c> object is null or empty, the method returns <c>null</c>.
        /// </summary>
        /// <param name="value">A <c>System.String</c> reference.</param>
        /// <returns></returns>
        public static int? ToNInt(this string value)
        {
            return value.IsNullOrEmpty() ? (int?)null : value.ToInt();
        }

        /// <summary>
        /// Converts the current string representation of a 
        /// date and time to its <c>System.DateTime</c> equivalent.
        /// </summary>
        /// <param name="value">A string containing a date and time to convert.</param>
        /// <returns>
        /// A System.DateTime equivalent to the date and time contained in the current
        /// <c>System.String</c> object.
        /// </returns>
        public static DateTime ToDate(this string value)
        {
            return value.ToDate(false);
        }

        /// <summary>
        /// Converts the current string representation of a 
        /// date and time to its <c>System.DateTime</c> equivalent.
        /// </summary>
        /// <param name="value">A string containing a date and time to convert.</param>
        /// <param name="invariant">A boolean indicating if the conversion is culture invariant or not.</param>
        /// <returns>
        /// A System.DateTime equivalent to the date and time contained in the current
        /// <c>System.String</c> object.
        /// </returns>
        public static DateTime ToDate(this string value, bool invariant)
        {
            value.CanNotBeEmpty();
            return invariant ? DateTime.Parse(value, CultureInfo.InvariantCulture) : DateTime.Parse(value);
        }

        //public static string[] Split(this string value, string a)
        //{
        //    return Microsoft.VisualBasic.Strings.Split(value, a).Safe().Where(x => !string.IsNullOrWhiteSpace(x)).ToArray();
        //}

        public static string[] Split(this string value, char a)
        {
            return value.Split(new char[] { a });
        }

        /// <summary>
        /// Formats the string with the specified arguments (same as string.Format)
        /// </summary>
        /// <param name="format">The string to format</param>
        /// <param name="args">String.Format arguments</param>
        /// <returns></returns>
        public static string Format(this string format, params object[] args)
        {
            return string.Format(format, args);
        }

        public static string toCleanHtml(this string text)
        {
            // Replace the HTML entity &lt; with the '<' character
            text = text.Replace("&lt;", "<");

            // Replace the HTML entity &gt; with the '>' character
            text = text.Replace("&gt;", ">");

            // Replace the HTML entity &amp; with the '&' character
            text = text.Replace("&amp;", "&");

            // Replace the HTML entity &nbsp; with the ' ' character
            text = text.Replace("&nbsp;", " ");

            return text.Trim();
        }

        public static string StripHtml(this string text)
        {
            // Replace the HTML entity &lt; with the '<' character
            text = text.Replace("&lt;", "<");

            // Replace the HTML entity &gt; with the '>' character
            text = text.Replace("&gt;", ">");

            // Replace the HTML entity &amp; with the '&' character
            text = text.Replace("&amp;", "&");

            // Replace the HTML entity &nbsp; with the ' ' character
            text = text.Replace("&nbsp;", " ");

            // Replace any </p> tags with a newline
            text = text.Replace("</p>", Environment.NewLine);

            // Replace any <br> tags with a newline
            text = Regex.Replace(text, "<br.*>", Environment.NewLine);

            // Remove anything between <whatever> tags
            text = Regex.Replace(text, "<.+?>", "");

            return text.Trim();

        }

        /// <summary>
        /// Ellipsifies the given text
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="maxLength">Length of the max.</param>
        /// <returns></returns>
        public static string ToEllipse(this string text, int maxLength)
        {
            string end = "...";
            if (text.IsNullOrEmpty())
                return string.Empty;

            if (text.Length <= maxLength + end.Length)
                return text;

            return string.Concat(text.Substring(0, maxLength), end);
        }


        public static string TrimEnd(this string text, string toTrim)
        {
            if (toTrim.IsNullOrEmpty() || text.IsNullOrEmpty() || !text.EndsWith(toTrim)) return text;
            return text.Remove(text.Length - toTrim.Length, toTrim.Length);
        }

        public static string TrimStart(this string text, string toTrim)
        {
            if (toTrim.IsNullOrEmpty() || text.IsNullOrEmpty() || !text.StartsWith(toTrim)) return text;
            return text.Remove(0, toTrim.Length);
        }

        public static byte[] ToByteArray(this string value)
        {
            return System.Text.Encoding.UTF8.GetBytes(value);
        }

        public static List<string> ToList(this string value)
        {
            return new List<string>() { value };
        }
    }





    public static class DateExtensions
    {
        /// <summary>
        /// Returns DayOfWeek as integer value where 1 = Monday and 7 = Sunday
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static int Dow(this DateTime date)
        {
            int dayofweek = (int)date.DayOfWeek;
            if (dayofweek == 0) dayofweek = 7; // change to 7 if sunday
            return dayofweek;
        }

        /// <summary>
        /// Truncate the seconds in datetime and return
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime Truncate(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, 0, date.Kind);
        }

        /// <summary>
        /// Truncate the seconds in datetime? and return
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime? Truncate(this DateTime? date)
        {
            if (date.HasValue)
                return Truncate((DateTime)date);
            else
                return date;
        }
    }


}
