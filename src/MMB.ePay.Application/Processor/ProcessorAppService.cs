﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.HR;
using MMB.ePay.Processor.Dtos;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Tk;
using Tk.Domain;

namespace MMB.ePay.Processor
{
    public class ProcessorAppService : ePayAppServiceBase, IProcessorAppService
    {
        private readonly IRepository<Employees> _employeesRepository;
        private readonly IRepository<Status> _statusRepository;
        private readonly IRepository<TKSettings> _tkSettingRepository;
        private readonly IRepository<Holidays> _holidaysRepository;
        private readonly IRepository<AttendanceUploads> _attendanceUploadsRepository;
        private readonly IRepository<AttendanceRequests> _attendanceRequestsRepository;
        private readonly IRepository<LeaveRequestDetails> _leaveRequestDetailsRepository;
        private readonly IRepository<OfficialBusinessRequests> _officialBusinessRequestsRepository;
        private readonly IRepository<OvertimeRequests> _overtimeRequestsRepository;
        private readonly IRepository<RestDayRequests> _restDayRequestsRepository;
        private readonly IRepository<ShiftRequests> _shiftRequestsRepository;
        private readonly IRepository<ShiftTypes> _shiftTypesRepository;
        private readonly IRepository<ShiftTypeDetails> _shiftTypeDetailsRepository;
        private readonly IRepository<RawTimeLogs> _rawTimelogsRepository;
        private readonly IRepository<AttendancePeriods> _attendancePeriodsRepository;
        private readonly IRepository<Attendances> _attendancesRepository;
        private readonly IRepository<OvertimeTypes> _overtimeTypesRepository;
        private readonly IDbContextProvider<ePayDbContext> _dbContextProvider;
        private readonly IStoredProceduresAppService _storedProceduresAppService;

        public ProcessorAppService(IRepository<Employees> employeeRepository,
            IRepository<Status> statusRepository,
            IRepository<TKSettings> tkSettingsRepository,
            IRepository<Holidays> holidaysRepository,
            IRepository<AttendanceUploads> attendanceUploadsRepository,
            IRepository<AttendanceRequests> attendanceRequestsRepository,
            IRepository<LeaveRequestDetails> leaveRequestDetailsRepository,
            IRepository<OfficialBusinessRequests> officialBusinessRequestsRepository,
            IRepository<OvertimeRequests> overtimeRequestsRepository,
            IRepository<RestDayRequests> restDayRequestsRepository,
            IRepository<ShiftRequests> shiftRequestsRepository,
            IRepository<ShiftTypes> shiftTypesRepository,
            IRepository<ShiftTypeDetails> shiftTypeDetailsRepository,
            IRepository<RawTimeLogs> rawTimelogsRepository,
            IRepository<AttendancePeriods> attendancePeriodsRepository,

            IRepository<Attendances> attendancesRepository,
            IRepository<OvertimeTypes> overtimeTypesRepository,

            IDbContextProvider<ePayDbContext> dbContextProvider,
            IStoredProceduresAppService storedProceduresAppService)
        {
            _employeesRepository = employeeRepository;
            _statusRepository = statusRepository;
            _tkSettingRepository = tkSettingsRepository;
            _holidaysRepository = holidaysRepository;
            _attendanceUploadsRepository = attendanceUploadsRepository;
            _attendanceRequestsRepository = attendanceRequestsRepository;
            _leaveRequestDetailsRepository = leaveRequestDetailsRepository;
            _officialBusinessRequestsRepository = officialBusinessRequestsRepository;
            _overtimeRequestsRepository = overtimeRequestsRepository;
            _restDayRequestsRepository = restDayRequestsRepository;
            _shiftRequestsRepository = shiftRequestsRepository;
            _shiftTypesRepository = shiftTypesRepository;
            _shiftTypeDetailsRepository = shiftTypeDetailsRepository;
            _rawTimelogsRepository = rawTimelogsRepository;
            _attendancePeriodsRepository = attendancePeriodsRepository;

            _attendancesRepository = attendancesRepository;
            _overtimeTypesRepository = overtimeTypesRepository;

            _dbContextProvider = dbContextProvider;
            _storedProceduresAppService = storedProceduresAppService;
        }

        public async Task AutomatedProcess(int tenantId)
        {
            var periodId = await _attendancePeriodsRepository.GetAll()
                .Where(e => e.TenantId == tenantId && !e.Posted)
                .OrderBy(e => e.EndDate)
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            await Process(tenantId, periodId, null);
        }

        public async Task<bool> Post(int tenantId, int periodId, bool isForced)
        {
            if (periodId == 0)
                throw new NotImplementedException();

            var period = await _attendancePeriodsRepository.GetAll()
                .Where(e => e.Id == periodId)
                .Select(e => new
                {
                    e.StartDate,
                    e.EndDate
                }).FirstOrDefaultAsync();

            if (!isForced)
            {
                var hasInvalid = await _attendancesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.Date >= period.StartDate && e.Date <= period.EndDate
                    && e.Invalid
                ).AnyAsync();

                if (hasInvalid)
                    return false;
            }

            var postAttendanceDto = new PostAttendanceDto
            {
                AttendancePeriodId = periodId,
                TenantId = tenantId
            };

            await _storedProceduresAppService.PostAttendance(postAttendanceDto);

            return true;
        }

        public async Task Process(int periodId, int? employeeId) =>
            await Process(AbpSession.TenantId.Value, periodId, employeeId);


        [UnitOfWork]
        public async Task Process(int tenantId, int periodId, int? employeeId)
        {
            var deleteProcessDto = new DeleteProcessDto
            {
                AttendancePeriodId = periodId,
                TenantId = tenantId,
                EmployeeId = employeeId
            };

            CurrentUnitOfWork.SetTenantId(tenantId);
            await _storedProceduresAppService.DeleteProcess(deleteProcessDto);
            await UnitOfWorkManager.Current.SaveChangesAsync();

            var periodRange = await _attendancePeriodsRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.Id == periodId)
                .Select(e => new DateRange
                {
                    Start = e.StartDate,
                    End = e.EndDate
                }).FirstOrDefaultAsync();

            // processors
            var rawProcessor = new RawTimeProcessor(periodRange, await GetShiftTypes(tenantId), await GetShiftTypeDetails(tenantId))
            {
                HasInOutIndicator = false
            };

            var holidays = await GetHolidays(tenantId);
            string[] ndKeys =  { "ND1_START", "ND1_END", "ND2_START", "ND2_END" };

            var ranges = await _tkSettingRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && ndKeys.Contains(e.Key))
                .Select(e => new { e.Key, Value = TimeSpan.Parse(e.Value) })
                .ToListAsync();

            var nd1Start = ranges.Where(e => e.Key == ndKeys[0]).Select(e => e.Value).First();
            var nd1End = ranges.Where(e => e.Key == ndKeys[1]).Select(e => e.Value).First();
            var nd2Start = ranges.Where(e => e.Key == ndKeys[2]).Select(e => e.Value).First();
            var nd2End = ranges.Where(e => e.Key == ndKeys[3]).Select(e => e.Value).First();

            var attendanceProcessor = new AttendanceProcessor(holidays, 
                new NightDifferential(
                    new TimeRange { Start = nd1Start, End = nd1End },
                    new TimeRange { Start = nd2Start, End = nd2End }
                    )
                );

            // initialize processing session

            if (periodId == 0)
                throw new NotImplementedException();

            var initProcSessionDto = new InitializeProcSessionDto
            {
                AppSessionId = Guid.NewGuid(),
                AttendancePeriodId = periodId,
                EmployeeId = employeeId,
                TenantId = tenantId
            };

            await _storedProceduresAppService.InitializeProcSession(initProcSessionDto);
            var batchId = await _storedProceduresAppService.GetProcessBatch(initProcSessionDto.AppSessionId);

            var approvedId = await _statusRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.TenantId == AbpSession.TenantId
                    && e.DisplayName == StatusDisplayNames.Approved.ToString())
                .Select(e => e.Id)
                .FirstOrDefaultAsync();

            while (!batchId.Equals(Guid.Empty))
            {
                // query transactions by batch
                var employees = await GetEmployees(batchId);
                var shiftRequests = await GetShiftRequests(batchId, periodRange, approvedId);
                var rawTimeLogs = await GetRawTimeLogs(batchId, periodRange);
                var attendanceRequests = await GetAttendanceRequests(batchId, periodRange, approvedId);
                var restdayRequests = await GetRestDayRequests(batchId, periodRange, approvedId);

                var employeeShifts = rawProcessor.GetEmployeeShifts(employees, shiftRequests);

                var leaveRequests = await GetLeaveRequests(batchId, periodRange, approvedId);
                var obRequests = await GetOBrequests(batchId, periodRange, approvedId);
                var overtimeRequests = await GetOvertimeRequests(batchId, periodRange, approvedId);
                var attendanceUploads = await GetAttendanceUploads(batchId, periodRange);
                // process raw
                var attendances = rawProcessor.ProcessRaw(rawTimeLogs, employees, employeeShifts, attendanceRequests, restdayRequests, attendanceUploads);

                // process attendance
                var processedAttend = attendanceProcessor.ProcessAttendance(employees, attendances, employeeShifts, leaveRequests, obRequests, overtimeRequests);
                

                //save attendance/timesheet/ot here
                if (attendances.Count > 0)
                    await UpdateAttendance(tenantId, attendances);

                if (processedAttend.Timesheets.Count > 0)
                    await UpdateTimesheet(tenantId, processedAttend.Timesheets);

                if (processedAttend.ProcessedOvertimes.Count > 0)
                    await UpdateOvertime(tenantId, processedAttend.ProcessedOvertimes);

                if (leaveRequests.Count > 0)
                    await UpdateLeaves(tenantId, leaveRequests);

                batchId = await _storedProceduresAppService.GetProcessBatch(initProcSessionDto.AppSessionId);
            }

            // cleanup
            await _storedProceduresAppService.ProcessSessionCleanup(initProcSessionDto.AppSessionId);
        }

        protected async Task<List<Employee>> GetEmployees(Guid? batchId)
        {
            var output = await _employeesRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.ProcSessionBatches.Any(f => f.BatchId == batchId))
                .Select(e => new Employee
                {
                    Id = e.Id,
                    LastName = e.LastName,
                    FirstName = e.FirstName,
                    ShiftTypeId = e.EmployeeTimeInfo.ShiftTypeId,
                    RestDayCode = e.EmployeeTimeInfo.RestDay,
                    OtExempt = e.EmployeeTimeInfo.Overtime,
                    UtExempt = e.EmployeeTimeInfo.Undertime,
                    TardyExempt = e.EmployeeTimeInfo.Tardy,
                    NonSwiper = e.EmployeeTimeInfo.NonSwiper,
                    LocationId = e.EmployeeTimeInfo.LocationId
                }).ToListAsync();

            //if (output.Count == 0) 
            //{
            //    throw new NotImplementedException();
            //}

            return output;
        }

        protected async Task<List<Holiday>> GetHolidays(int tenantId)
        {
            var output = await _holidaysRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.TenantId == tenantId)
                .OrderBy(e => e.Date)
                .Select(e => new Holiday
                {
                    Id = e.Id,
                    Date = e.Date,
                    Description = e.DisplayName,
                    Halfday = e.HalfDay,
                    HolidayType = e.HolidayTypes.HolidayCode,
                    LocationId = e.LocationId
                }).ToListAsync();

            return output;
        }

        protected async Task<List<AttendanceRequest>> GetAttendanceRequests(Guid? batchId, DateRange period, int approvedId)
        {
            var output = await _attendanceRequestsRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.Employees.ProcSessionBatches.Any(f => f.BatchId == batchId) && e.StatusId == approvedId)
                .Where(e => e.Date <= period.End && e.Date >= period.Start)
                .Select(e => new AttendanceRequest
                {
                    EmployeeId = e.EmployeeId,
                    Date = e.Date,
                    TimeIn = e.TimeIn,
                    BreaktimeIn = e.BreaktimeIn,
                    BreaktimeOut = e.BreaktimeOut,
                    PMBreaktimeIn = e.PMBreaktimeIn,
                    PMBreaktimeOut = e.PMBreaktimeOut,
                    TimeOut = e.TimeOut,
                    CreationTime = e.CreationTime
                }).ToListAsync();

            return output;
        }


        protected async Task<List<LeaveRequest>> GetLeaveRequests(Guid? batchId, DateRange period, int approvedId)
        {
            return await _leaveRequestDetailsRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.LeaveRequests.Employees.ProcSessionBatches.Any(f => f.BatchId == batchId) 
                    && e.LeaveRequests.StatusId == approvedId)
                .Where(e => e.LeaveDate >= period.Start && e.LeaveDate <= period.End)
                .Select(e => new LeaveRequest {
                    EmployeeId = e.EmployeeId,
                    LeaveTypeId = e.LeaveRequests.LeaveTypeId,
                    LeaveDisplay = e.LeaveRequests.LeaveTypes.DisplayName,
                    LeaveDate = e.LeaveDate,
                    Days = e.Days, 
                    HalfdayLeaveFirstHalf = e.FirstHalf
                }).ToListAsync();
        }
        protected async Task<List<OvertimeRequest>> GetOvertimeRequests(Guid? batchId, DateRange period, int approvedId)
        {
            var output = await _overtimeRequestsRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.Employees.ProcSessionBatches.Any(f => f.BatchId == batchId) && e.StatusId == approvedId)
                .Where(e => e.OvertimeDate <= period.End && e.OvertimeDate >= period.Start)
                .Select(e => new OvertimeRequest
                {
                    EmployeeId = e.EmployeeId,
                    Date = e.OvertimeDate,
                    AuthorizedStart = e.OvertimeStart,
                    AuthorizedEnd = e.OvertimeEnd
                }).ToListAsync();

            return output;
        }

        protected async Task<List<OffBusinessRequest>> GetOBrequests(Guid? batchId, DateRange period, int approvedId)
        {
            var requests = await _officialBusinessRequestsRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.Employees.ProcSessionBatches.Any(f => f.BatchId == batchId) 
                    && e.StatusId == approvedId)
                .Where(e => e.OfficialBusinessRequestDetails
                    .Any(f => f.OfficialBusinessDate >= period.Start && f.OfficialBusinessDate <= period.End))
                .Select(e => new
                {
                    e.EmployeeId,
                    e.OfficialBusinessRequestDetails,
                }).ToListAsync();

            var details = requests
                .Select(e => new
                {
                    Details = e.OfficialBusinessRequestDetails
                        .Select(f => new OffBusinessRequest
                        {
                            EmployeeId = f.EmployeeId,
                            Date = f.OfficialBusinessDate.Date,
                            ObStart = e.OfficialBusinessRequestDetails.Min(g => g.OfficialBusinessDate),
                            ObEnd = e.OfficialBusinessRequestDetails.Max(g => g.OfficialBusinessDate)
                        }).ToList()
                }).ToList();

            var output = details.SelectMany(e => e.Details).ToList();

            return output;
        }

        protected async Task<List<ShiftRequest>> GetShiftRequests(Guid? batchId, DateRange period, int approvedId)
        {
            var output = await _shiftRequestsRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.Employees.ProcSessionBatches.Any(f => f.BatchId == batchId) && e.StatusId == approvedId)
                .Where(e => e.ShiftStart <= period.End && e.ShiftEnd >= period.Start)
                .Select(e => new ShiftRequest
                {
                    Id = e.Id,
                    EmployeeId = e.EmployeeId,
                    StartDate = e.ShiftStart,
                    EndDate = e.ShiftEnd,
                    ShiftTypeId = e.ShiftTypeId,
                    ApprovalDate = e.CreationTime
                }).ToListAsync();

            return output;
        }

        protected async Task<List<RestDayRequest>> GetRestDayRequests(Guid? batchId, DateRange period, int approvedId)
        {
            var output = await _restDayRequestsRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.Employees.ProcSessionBatches.Any(f => f.BatchId == batchId) && e.StatusId == approvedId)
                .Where(e => e.RestDayStart <= period.End && e.RestDayEnd >= period.Start)
                .Select(e => new RestDayRequest
                {
                    EmployeeId = e.EmployeeId,
                    RestDayCode = e.RestDayCode,
                    StartDate = e.RestDayStart,
                    EndDate = e.RestDayEnd,
                    CreationTime = e.CreationTime
                }).ToListAsync();

            return output;
        }

        protected async Task<List<ShiftType>> GetShiftTypes(int tenantId)
        {
            var output = await _shiftTypesRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.TenantId == tenantId)
                .Select(e => new ShiftType
                {
                    Id = e.Id,
                    Description = e.Description,
                    MinimumOvertime = e.MinimumOvertime,
                    FlexiTime = e.Flexible1,
                    GraceTime = e.GracePeriod1
                }).ToListAsync();

            if (output.Count == 0)
            {
                throw new NotImplementedException();
            }

            return output;
        }

        protected async Task<List<ShiftTypeDetail>> GetShiftTypeDetails(int tenantId)
        {
            var output = await _shiftTypeDetailsRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.TenantId == tenantId)
                .Select(e => new ShiftTypeDetail
                {
                    Id = e.Id,
                    ShiftTypeId = e.ShiftTypeId,
                    Days = e.Days,
                    TimeIn = e.TimeIn,
                    BreakTimeIn = e.BreaktimeIn,
                    BreakTimeOut = e.BreaktimeOut,
                    TimeOut = e.TimeOut,
                    CrossoverShiftStartsOnPrevDay = e.StartOnPreviousDay
                }).ToListAsync();

            return output;
        }

        protected async Task<List<RawTimeLog>> GetRawTimeLogs(Guid? batchId, DateRange period)
        {
            var employeeInfos = await _employeesRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.ProcSessionBatches.Any(f => f.BatchId == batchId))
                .Select(e => new { e.Id, e.EmployeeTimeInfo.SwipeCode })
                .ToListAsync();

            var start = period.Start.AddDays(-2);
            var end = period.End.AddDays(2);

            var rawTimeLogs = await _rawTimelogsRepository.GetAll()
                .Where(e => employeeInfos.Select(f => f.SwipeCode).Any(f => f == e.SwipeCode))
                .Where(e => e.LogTime >= start && e.LogTime <= end)
                .Select(e => new
                {
                    e.Id,
                    e.SwipeCode,
                    e.LogTime,
                    e.InOut
                }).ToListAsync();

            var output = rawTimeLogs
                .Join(employeeInfos,
                    raw => raw.SwipeCode,
                    info => info.SwipeCode,
                (raw, info) => new RawTimeLog
                {
                    Id = raw.Id,
                    EmployeeId = info.Id,
                    LogTime = raw.LogTime,
                    InOut = raw.InOut
                }).ToList();

            return output;
        }

        //protected async Task<List<AttendanceUpload>> GetAttendanceUploads(Guid? batchId, DateRange period)
        //{
        //    var output = await _attendanceUploadsRepository.GetAll()
        //        .AsNoTracking()
        //        .Where(e => e.Employees.ProcSessionBatches.Any(f => f.BatchId == batchId))
        //        .Where(e => e.Date <= period.End && e.Date >= period.Start)
        //        .Select(e => new
        //        {
        //            e.EmployeeId,
        //            e.Date,
        //            e.TimeIn,
        //            e.LunchBreaktimeIn,
        //            e.LunchBreaktimeOut,
        //            e.PMBreaktimeIn,
        //            e.PMBreaktimeOut,
        //            e.TimeOut
        //        })
        //        .ToListAsync();

        //    return output
        //        .Select(e => new AttendanceUpload
        //        {
        //            EmployeeId = e.EmployeeId,
        //            Date = e.Date,
        //            TimeIn = ConvertTimeStringToDateTime(e.TimeIn),
        //            LunchBreaktimeIn = ConvertTimeStringToDateTime(e.LunchBreaktimeIn),
        //            LunchBreaktimeOut = ConvertTimeStringToDateTime(e.LunchBreaktimeOut),
        //            PMBreaktimeIn = ConvertTimeStringToDateTime(e.PMBreaktimeIn),
        //            PMBreaktimeOut = ConvertTimeStringToDateTime(e.PMBreaktimeOut),
        //            TimeOut = ConvertTimeStringToDateTime(e.TimeOut),
        //        }).ToList();
        //}
        protected async Task<List<AttendanceUpload>> GetAttendanceUploads(Guid? batchId, DateRange period)
        {
            var output = await _attendanceUploadsRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.Employees.ProcSessionBatches.Any(f => f.BatchId == batchId))
                .Where(e => e.Date <= period.End && e.Date >= period.Start)
                .Select(e => new AttendanceUpload
                {
                    EmployeeId = e.EmployeeId,
                    Date = e.Date,
                    TimeIn = ConvertTimeStringToDateTime(e.Date, e.TimeIn),
                    LunchBreaktimeIn = ConvertTimeStringToDateTime(e.Date, e.LunchBreaktimeIn),
                    LunchBreaktimeOut = ConvertTimeStringToDateTime(e.Date, e.LunchBreaktimeOut),
                    PMBreaktimeIn = ConvertTimeStringToDateTime(e.Date, e.PMBreaktimeIn),
                    PMBreaktimeOut = ConvertTimeStringToDateTime(e.Date, e.PMBreaktimeOut),
                    TimeOut = ConvertTimeStringToDateTime(e.Date, e.TimeOut),
                }).ToListAsync();

            return output;            
        }

        private static DateTime? ConvertTimeStringToDateTime(DateTime date, string time)
        {
            if (string.IsNullOrEmpty(time))
                return null;

            var success = TimeSpan.TryParseExact(time, @"hh\:mm\:ss", CultureInfo.InvariantCulture, out TimeSpan timeSpan);

            if (!success)
                return null;

            return date + timeSpan;
        }

        protected async Task<AttendancePeriod> GetAttendancePeriod(int attendancePeriodId)
        {
            var period = await _attendancePeriodsRepository.GetAll()
                .AsNoTracking()
                .Where(e => e.Id == attendancePeriodId)
                .Select(e => new AttendancePeriod
                {
                    Id = e.Id,
                    StartDate = e.StartDate,
                    EndDate = e.EndDate
                }).FirstOrDefaultAsync();

            return period;
        }

        // Data Persistence
        protected async Task UpdateAttendance(int tenantId, List<Attendance> attendances)
        {
            var newAttendances = attendances
                .Select(e => new Attendances
                {
                    TenantId = tenantId,
                    EmployeeId = e.EmployeeId,
                    Date = e.Date,
                    RestDayCode = e.RestDayCode,
                    TimeIn = e.TimeIn,
                    BreaktimeIn = e.BreakTimeIn,
                    BreaktimeOut = e.BreakTimeOut,
                    PMBreaktimeIn = e.PMBreakTimeIn,
                    PMBreaktimeOut = e.PMBreakTimeOut,
                    TimeOut = e.TimeOut,
                    Remarks = e.Remarks,
                    ShiftTypeId = e.ShiftTypeId,
                    Invalid = e.Invalid
                }).ToList();

            var dbContext = _dbContextProvider.GetDbContext();
            await dbContext.Attendances.AddRangeAsync(newAttendances);
        }

        protected async Task UpdateTimesheet(int tenantId, List<Timesheet> timesheets)
        {
            var newTimesheets = timesheets.Select(e => new ProcessedTimesheets
            {
                TenantId = tenantId,
                EmployeeId = e.EmployeeId,
                Date = e.Date,
                RegMins = e.RegMins,
                Absence = e.Absence,
                TardyMins = e.TardyMins,
                UtMins = e.UtMins,
                RegNd1Mins = e.RegNd1Mins,
                RegNd2Mins = e.RegNd2Mins,
                PaidHoliday = e.PaidHoliday,
                LeaveDays = e.LeaveDays,
                ObMins = e.ObMins,
                IsHoliday = e.IsHoliday,
                IsRestDay = e.IsRestday,
                Remarks = e.Remarks
            }).ToList();

            var dbContext = _dbContextProvider.GetDbContext();
            await dbContext.ProcessedTimesheets.AddRangeAsync(newTimesheets);
        }

        protected async Task UpdateOvertime(int tenantId, List<ProcessedOvertime> overtimes)
        {
            var newOvertimes = overtimes.Select(e => new ProcessedOvertimes
            {
                TenantId = tenantId,
                EmployeeId = e.EmployeeId,
                Date = e.Date,
                OvertimeTypeId = _overtimeTypesRepository.GetAll()
                    .Where(f => f.OTCode == e.OvertimeCode && f.SubType == (short)e.SubType)
                    .Select(f => f.Id)
                    .FirstOrDefault(),
                Minutes = e.OvertimeMinutes
            }).ToList();

            var dbContext = _dbContextProvider.GetDbContext();
            await dbContext.ProcessedOvertimes.AddRangeAsync(newOvertimes);
        }

        protected async Task UpdateLeaves(int tenantId, List<LeaveRequest> leaves)
        {
            var newLeaves = leaves.Select(e => new ProcessedLeaves
            {
                TenantId = tenantId,
                EmployeeId = e.EmployeeId,
                Date = e.LeaveDate,
                LeaveTypeId = e.LeaveTypeId,
                Days = e.Days
            }).ToList();

            var dbContext = _dbContextProvider.GetDbContext();
            await dbContext.ProcessedLeaves.AddRangeAsync(newLeaves);
        }
    }
}
