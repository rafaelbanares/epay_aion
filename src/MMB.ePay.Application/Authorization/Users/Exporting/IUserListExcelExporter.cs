using System.Collections.Generic;
using MMB.ePay.Authorization.Users.Dto;
using MMB.ePay.Dto;

namespace MMB.ePay.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}