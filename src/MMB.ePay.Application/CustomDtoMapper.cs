﻿using Abp.Application.Editions;
using Abp.Application.Features;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.DynamicEntityProperties;
using Abp.EntityHistory;
using Abp.Localization;
using Abp.Notifications;
using Abp.Organizations;
using Abp.UI.Inputs;
using Abp.Webhooks;
using AutoMapper;
using MMB.ePay.Auditing.Dto;
using MMB.ePay.Authorization.Accounts.Dto;
using MMB.ePay.Authorization.Delegation;
using MMB.ePay.Authorization.Permissions.Dto;
using MMB.ePay.Authorization.Roles;
using MMB.ePay.Authorization.Roles.Dto;
using MMB.ePay.Authorization.Users;
using MMB.ePay.Authorization.Users.Delegation.Dto;
using MMB.ePay.Authorization.Users.Dto;
using MMB.ePay.Authorization.Users.Importing.Dto;
using MMB.ePay.Authorization.Users.Profile.Dto;
using MMB.ePay.Chat;
using MMB.ePay.Chat.Dto;
using MMB.ePay.DynamicEntityProperties.Dto;
using MMB.ePay.Editions;
using MMB.ePay.Editions.Dto;
using MMB.ePay.Friendships;
using MMB.ePay.Friendships.Cache;
using MMB.ePay.Friendships.Dto;
using MMB.ePay.HR;
using MMB.ePay.HR.Dtos;
using MMB.ePay.Localization.Dto;
using MMB.ePay.MultiTenancy;
using MMB.ePay.MultiTenancy.Dto;
using MMB.ePay.MultiTenancy.HostDashboard.Dto;
using MMB.ePay.MultiTenancy.Payments;
using MMB.ePay.MultiTenancy.Payments.Dto;
using MMB.ePay.Notifications.Dto;
using MMB.ePay.Organizations.Dto;
using MMB.ePay.Sessions.Dto;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.WebHooks.Dto;

namespace MMB.ePay
{
    internal static class CustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<CreateOrEditPostedAttendancesDto, PostedAttendances>().ReverseMap();
            configuration.CreateMap<PostedAttendancesDto, PostedAttendances>().ReverseMap();
            configuration.CreateMap<CreateOrEditCompaniesDto, Companies>().ReverseMap();
            configuration.CreateMap<CompaniesDto, Companies>().ReverseMap();
            configuration.CreateMap<CreateOrEditLocationsDto, Locations>().ReverseMap();
            configuration.CreateMap<LocationsDto, Locations>().ReverseMap();
            configuration.CreateMap<CreateOrEditHolidaysDto, Holidays>().ReverseMap();
            configuration.CreateMap<HolidaysDto, Holidays>().ReverseMap();
            configuration.CreateMap<CreateOrEditEmailQueueDto, EmailQueue>().ReverseMap();
            configuration.CreateMap<EmailQueueDto, EmailQueue>().ReverseMap();
            configuration.CreateMap<CreateOrEditHolidayTypesDto, HolidayTypes>().ReverseMap();
            configuration.CreateMap<HolidayTypesDto, HolidayTypes>().ReverseMap();
            configuration.CreateMap<CreateOrEditLeaveTypesDto, LeaveTypes>().ReverseMap();
            configuration.CreateMap<LeaveTypesDto, LeaveTypes>().ReverseMap();
            configuration.CreateMap<CreateOrEditApproverOrdersDto, ApproverOrders>().ReverseMap();
            configuration.CreateMap<ApproverOrdersDto, ApproverOrders>().ReverseMap();
            configuration.CreateMap<CreateOrEditAttendanceApprovalsDto, AttendanceApprovals>().ReverseMap();
            configuration.CreateMap<AttendanceApprovalsDto, AttendanceApprovals>().ReverseMap();
            configuration.CreateMap<CreateOrEditAttendancePeriodsDto, AttendancePeriods>().ReverseMap();
            configuration.CreateMap<AttendancePeriodsDto, AttendancePeriods>().ReverseMap();
            configuration.CreateMap<CreateOrEditAttendanceReasonsDto, AttendanceReasons>().ReverseMap();
            configuration.CreateMap<AttendanceReasonsDto, AttendanceReasons>().ReverseMap();
            configuration.CreateMap<CreateOrEditAttendanceRequestsDto, AttendanceRequests>().ReverseMap();
            configuration.CreateMap<AttendanceRequestsDto, AttendanceRequests>().ReverseMap();
            configuration.CreateMap<CreateOrEditAttendancesDto, Attendances>().ReverseMap();
            configuration.CreateMap<AttendancesDto, Attendances>().ReverseMap();
            configuration.CreateMap<CreateOrEditEmployeeApproversDto, EmployeeApprovers>().ReverseMap();
            configuration.CreateMap<EmployeeApproversDto, EmployeeApprovers>().ReverseMap();
            configuration.CreateMap<CreateOrEditEmployeeTimeInfoDto, EmployeeTimeInfo>().ReverseMap();
            configuration.CreateMap<EmployeeTimeInfoDto, EmployeeTimeInfo>().ReverseMap();
            configuration.CreateMap<CreateOrEditExcuseApprovalsDto, ExcuseApprovals>().ReverseMap();
            configuration.CreateMap<ExcuseApprovalsDto, ExcuseApprovals>().ReverseMap();
            configuration.CreateMap<CreateOrEditExcuseReasonsDto, ExcuseReasons>().ReverseMap();
            configuration.CreateMap<ExcuseReasonsDto, ExcuseReasons>().ReverseMap();
            configuration.CreateMap<CreateOrEditExcuseRequestsDto, ExcuseRequests>().ReverseMap();
            configuration.CreateMap<ExcuseRequestsDto, ExcuseRequests>().ReverseMap();
            configuration.CreateMap<CreateOrEditFrequenciesDto, Frequencies>().ReverseMap();
            configuration.CreateMap<FrequenciesDto, Frequencies>().ReverseMap();
            configuration.CreateMap<CreateOrEditLeaveApprovalsDto, LeaveApprovals>().ReverseMap();
            configuration.CreateMap<LeaveApprovalsDto, LeaveApprovals>().ReverseMap();
            configuration.CreateMap<CreateOrEditLeaveEarningsDto, LeaveEarnings>().ReverseMap();
            configuration.CreateMap<LeaveEarningsDto, LeaveEarnings>().ReverseMap();
            configuration.CreateMap<CreateOrEditLeaveReasonsDto, LeaveReasons>().ReverseMap();
            configuration.CreateMap<LeaveReasonsDto, LeaveReasons>().ReverseMap();
            configuration.CreateMap<CreateOrEditLeaveRequestDetailsDto, LeaveRequestDetails>().ReverseMap();
            configuration.CreateMap<LeaveRequestDetailsDto, LeaveRequestDetails>().ReverseMap();
            configuration.CreateMap<CreateOrEditLeaveRequestsDto, LeaveRequests>().ReverseMap();
            configuration.CreateMap<LeaveRequestsDto, LeaveRequests>().ReverseMap();
            configuration.CreateMap<CreateOrEditOfficialBusinessApprovalsDto, OfficialBusinessApprovals>().ReverseMap();
            configuration.CreateMap<OfficialBusinessApprovalsDto, OfficialBusinessApprovals>().ReverseMap();
            configuration.CreateMap<CreateOrEditOfficialBusinessReasonsDto, OfficialBusinessReasons>().ReverseMap();
            configuration.CreateMap<OfficialBusinessReasonsDto, OfficialBusinessReasons>().ReverseMap();
            configuration.CreateMap<CreateOrEditOfficialBusinessRequestDetailsDto, OfficialBusinessRequestDetails>().ReverseMap();
            configuration.CreateMap<OfficialBusinessRequestDetailsDto, OfficialBusinessRequestDetails>().ReverseMap();
            configuration.CreateMap<CreateOrEditOfficialBusinessRequestsDto, OfficialBusinessRequests>().ReverseMap();
            configuration.CreateMap<OfficialBusinessRequestsDto, OfficialBusinessRequests>().ReverseMap();
            configuration.CreateMap<CreateOrEditOvertimeApprovalsDto, OvertimeApprovals>().ReverseMap();
            configuration.CreateMap<OvertimeApprovalsDto, OvertimeApprovals>().ReverseMap();
            configuration.CreateMap<CreateOrEditOvertimeReasonsDto, OvertimeReasons>().ReverseMap();
            configuration.CreateMap<OvertimeReasonsDto, OvertimeReasons>().ReverseMap();
            configuration.CreateMap<CreateOrEditOvertimeRequestsDto, OvertimeRequests>().ReverseMap();
            configuration.CreateMap<OvertimeRequestsDto, OvertimeRequests>().ReverseMap();
            configuration.CreateMap<CreateOrEditOvertimeTypesDto, OvertimeTypes>().ReverseMap();
            configuration.CreateMap<OvertimeTypesDto, OvertimeTypes>().ReverseMap();
            configuration.CreateMap<CreateOrEditRawTimeLogsDto, RawTimeLogs>().ReverseMap();
            configuration.CreateMap<RawTimeLogsDto, RawTimeLogs>().ReverseMap();
            configuration.CreateMap<CreateOrEditRestDayApprovalsDto, RestDayApprovals>().ReverseMap();
            configuration.CreateMap<RestDayApprovalsDto, RestDayApprovals>().ReverseMap();
            configuration.CreateMap<CreateOrEditRestDayReasonsDto, RestDayReasons>().ReverseMap();
            configuration.CreateMap<RestDayReasonsDto, RestDayReasons>().ReverseMap();
            configuration.CreateMap<CreateOrEditRestDayRequestsDto, RestDayRequests>().ReverseMap();
            configuration.CreateMap<RestDayRequestsDto, RestDayRequests>().ReverseMap();
            configuration.CreateMap<CreateOrEditShiftApprovalsDto, ShiftApprovals>().ReverseMap();
            configuration.CreateMap<ShiftApprovalsDto, ShiftApprovals>().ReverseMap();
            configuration.CreateMap<CreateOrEditShiftReasonsDto, ShiftReasons>().ReverseMap();
            configuration.CreateMap<ShiftReasonsDto, ShiftReasons>().ReverseMap();
            configuration.CreateMap<CreateOrEditShiftRequestsDto, ShiftRequests>().ReverseMap();
            configuration.CreateMap<ShiftRequestsDto, ShiftRequests>().ReverseMap();
            configuration.CreateMap<CreateOrEditShiftTypeDetailsDto, ShiftTypeDetails>().ReverseMap();
            configuration.CreateMap<ShiftTypeDetailsDto, ShiftTypeDetails>().ReverseMap();
            configuration.CreateMap<CreateOrEditShiftTypesDto, ShiftTypes>().ReverseMap();
            configuration.CreateMap<EditShiftTypesDto, ShiftTypes>().ReverseMap();
            configuration.CreateMap<ShiftTypesDto, ShiftTypes>().ReverseMap();
            configuration.CreateMap<CreateOrEditStatusDto, Status>().ReverseMap();
            configuration.CreateMap<StatusDto, Status>().ReverseMap();
            configuration.CreateMap<CreateOrEditTKSettingsDto, TKSettings>().ReverseMap();
            configuration.CreateMap<TKSettingsDto, TKSettings>().ReverseMap();
            configuration.CreateMap<CreateOrEditWorkFlowDto, WorkFlow>().ReverseMap();
            configuration.CreateMap<WorkFlowDto, WorkFlow>().ReverseMap();
            configuration.CreateMap<CreateOrEditEmploymentTypesDto, EmploymentTypes>().ReverseMap();
            configuration.CreateMap<EmploymentTypesDto, EmploymentTypes>().ReverseMap();
            configuration.CreateMap<CreateOrEditGroupSettingsDto, GroupSettings>().ReverseMap();
            configuration.CreateMap<GroupSettingsDto, GroupSettings>().ReverseMap();
            configuration.CreateMap<CreateOrEditEmployeesDto, Employees>().ReverseMap();
            configuration.CreateMap<EmployeesDto, Employees>().ReverseMap();

            //Inputs
            configuration.CreateMap<CheckboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<SingleLineStringInputType, FeatureInputTypeDto>();
            configuration.CreateMap<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<IInputType, FeatureInputTypeDto>()
                .Include<CheckboxInputType, FeatureInputTypeDto>()
                .Include<SingleLineStringInputType, FeatureInputTypeDto>()
                .Include<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<ILocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>()
                .Include<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<LocalizableComboboxItem, LocalizableComboboxItemDto>();
            configuration.CreateMap<ILocalizableComboboxItem, LocalizableComboboxItemDto>()
                .Include<LocalizableComboboxItem, LocalizableComboboxItemDto>();

            //Chat
            configuration.CreateMap<ChatMessage, ChatMessageDto>();
            configuration.CreateMap<ChatMessage, ChatMessageExportDto>();

            //Feature
            configuration.CreateMap<FlatFeatureSelectDto, Feature>().ReverseMap();
            configuration.CreateMap<Feature, FlatFeatureDto>();

            //Role
            configuration.CreateMap<RoleEditDto, Role>().ReverseMap();
            configuration.CreateMap<Role, RoleListDto>();
            configuration.CreateMap<UserRole, UserListRoleDto>();

            //Edition
            configuration.CreateMap<EditionEditDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<EditionCreateDto, SubscribableEdition>();
            configuration.CreateMap<EditionSelectDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<Edition, EditionInfoDto>().Include<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<SubscribableEdition, EditionListDto>();
            configuration.CreateMap<Edition, EditionEditDto>();
            configuration.CreateMap<Edition, SubscribableEdition>();
            configuration.CreateMap<Edition, EditionSelectDto>();

            //Payment
            configuration.CreateMap<SubscriptionPaymentDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPaymentListDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPayment, SubscriptionPaymentInfoDto>();

            //Permission
            configuration.CreateMap<Permission, FlatPermissionDto>();
            configuration.CreateMap<Permission, FlatPermissionWithLevelDto>();

            //Language
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageListDto>();
            configuration.CreateMap<NotificationDefinition, NotificationSubscriptionWithDisplayNameDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>()
                .ForMember(ldto => ldto.IsEnabled, options => options.MapFrom(l => !l.IsDisabled));

            //Tenant
            configuration.CreateMap<Tenant, RecentTenant>();
            configuration.CreateMap<Tenant, TenantLoginInfoDto>();
            configuration.CreateMap<Tenant, TenantListDto>();
            configuration.CreateMap<TenantEditDto, Tenant>().ReverseMap();
            configuration.CreateMap<CurrentTenantInfoDto, Tenant>().ReverseMap();

            //User
            configuration.CreateMap<User, UserEditDto>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());
            configuration.CreateMap<User, UserLoginInfoDto>();
            configuration.CreateMap<User, UserListDto>();
            configuration.CreateMap<User, ChatUserDto>();
            configuration.CreateMap<User, OrganizationUnitUserListDto>();
            configuration.CreateMap<Role, OrganizationUnitRoleListDto>();
            configuration.CreateMap<CurrentUserProfileEditDto, User>().ReverseMap();
            configuration.CreateMap<UserLoginAttemptDto, UserLoginAttempt>().ReverseMap();
            configuration.CreateMap<ImportUserDto, User>();

            //AuditLog
            configuration.CreateMap<AuditLog, AuditLogListDto>();
            configuration.CreateMap<EntityChange, EntityChangeListDto>();
            configuration.CreateMap<EntityPropertyChange, EntityPropertyChangeDto>();

            //Friendship
            configuration.CreateMap<Friendship, FriendDto>();
            configuration.CreateMap<FriendCacheItem, FriendDto>();

            //OrganizationUnit
            configuration.CreateMap<OrganizationUnit, OrganizationUnitDto>();

            //Webhooks
            configuration.CreateMap<WebhookSubscription, GetAllSubscriptionsOutput>();
            configuration.CreateMap<WebhookSendAttempt, GetAllSendAttemptsOutput>()
                .ForMember(webhookSendAttemptListDto => webhookSendAttemptListDto.WebhookName,
                    options => options.MapFrom(l => l.WebhookEvent.WebhookName))
                .ForMember(webhookSendAttemptListDto => webhookSendAttemptListDto.Data,
                    options => options.MapFrom(l => l.WebhookEvent.Data));

            configuration.CreateMap<WebhookSendAttempt, GetAllSendAttemptsOfWebhookEventOutput>();

            configuration.CreateMap<DynamicProperty, DynamicPropertyDto>().ReverseMap();
            configuration.CreateMap<DynamicPropertyValue, DynamicPropertyValueDto>().ReverseMap();
            configuration.CreateMap<DynamicEntityProperty, DynamicEntityPropertyDto>()
                .ForMember(dto => dto.DynamicPropertyName,
                    options => options.MapFrom(entity => entity.DynamicProperty.DisplayName ?? entity.DynamicProperty.PropertyName));
            configuration.CreateMap<DynamicEntityPropertyDto, DynamicEntityProperty>();

            configuration.CreateMap<DynamicEntityPropertyValue, DynamicEntityPropertyValueDto>().ReverseMap();

            //User Delegations
            configuration.CreateMap<CreateUserDelegationDto, UserDelegation>();

            /* ADD YOUR OWN CUSTOM AUTOMAPPER MAPPINGS HERE */
        }
    }
}