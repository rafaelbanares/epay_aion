﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR.Dtos;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.HR
{
    [AbpAuthorize]
    public class EmploymentTypesAppService : ePayAppServiceBase, IEmploymentTypesAppService
    {
        private readonly IRepository<EmploymentTypes> _employmentTypesRepository;

        public EmploymentTypesAppService(IRepository<EmploymentTypes> employmentTypesRepository) =>
            _employmentTypesRepository = employmentTypesRepository;

        public async Task<IList<SelectListItem>> GetEmploymentTypeList()
        {
            var employmentTypeList = await _employmentTypesRepository.GetAll()
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.Description
                }).ToListAsync();

            employmentTypeList.Insert(0, new SelectListItem { Value = "0", Text = "Select" });

            return employmentTypeList;
        }

        public async Task<IList<SelectListItem>> GetEmploymentTypeListForFilter()
        {
            var employmentTypeList = await _employmentTypesRepository.GetAll()
                .Select(e => new SelectListItem
                {
                    Value = e.Id.ToString(),
                    Text = e.Description
                }).ToListAsync();

            employmentTypeList.Insert(0, new SelectListItem { Value = "0", Text = "All" });

            return employmentTypeList;
        }

        [AbpAuthorize(AppPermissions.Pages_EmploymentTypes)]
        public async Task<PagedResultDto<GetEmploymentTypesForViewDto>> GetAll(GetAllEmploymentTypesInput input)
        {
            var filteredEmploymentTypes = _employmentTypesRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Description.Contains(input.Filter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description == input.DescriptionFilter);

            var pagedAndFilteredEmploymentTypes = filteredEmploymentTypes
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var employmentTypes = pagedAndFilteredEmploymentTypes
                .Select(o => new GetEmploymentTypesForViewDto
                {
                    EmploymentTypes = new EmploymentTypesDto
                    {
                        Description = o.Description,
                        Id = o.Id.ToString()
                    }
                });

            var totalCount = await filteredEmploymentTypes.CountAsync();

            return new PagedResultDto<GetEmploymentTypesForViewDto>(
                totalCount,
                await employmentTypes.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_EmploymentTypes)]
        public async Task<GetEmploymentTypesForViewDto> GetEmploymentTypesForView(int id)
        {
            var employmentTypes = await _employmentTypesRepository.GetAsync(id);

            return new GetEmploymentTypesForViewDto 
            { 
                EmploymentTypes = ObjectMapper.Map<EmploymentTypesDto>(employmentTypes) 
            };
        }

        [AbpAuthorize(AppPermissions.Pages_EmploymentTypes_Edit)]
        public async Task<GetEmploymentTypesForEditOutput> GetEmploymentTypesForEdit(EntityDto input)
        {
            var employmentTypes = await _employmentTypesRepository.FirstOrDefaultAsync(input.Id);

            return new GetEmploymentTypesForEditOutput 
            { 
                EmploymentTypes = ObjectMapper.Map<CreateOrEditEmploymentTypesDto>(employmentTypes) 
            };
        }

        public async Task CreateOrEdit(CreateOrEditEmploymentTypesDto input)
        {
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        [AbpAuthorize(AppPermissions.Pages_EmploymentTypes_Create)]
        protected virtual async Task Create(CreateOrEditEmploymentTypesDto input)
        {
            var employmentTypes = ObjectMapper.Map<EmploymentTypes>(input);
            await _employmentTypesRepository.InsertAsync(employmentTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_EmploymentTypes_Edit)]
        protected virtual async Task Update(CreateOrEditEmploymentTypesDto input)
        {
            var employmentTypes = await _employmentTypesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, employmentTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_EmploymentTypes_Delete)]
        public async Task Delete(EntityDto input) =>
            await _employmentTypesRepository.DeleteAsync(input.Id);
    }
}