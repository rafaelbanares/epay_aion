﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR.Dtos;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.HR
{
    [AbpAuthorize]
    public class GroupSettingsAppService : ePayAppServiceBase, IGroupSettingsAppService
    {
        private readonly IRepository<GroupSettings> _groupSettingsRepository;

        public GroupSettingsAppService(IRepository<GroupSettings> groupSettingsRepository) =>
            _groupSettingsRepository = groupSettingsRepository;

        [AbpAuthorize(AppPermissions.Pages_GroupSettings)]
        public async Task<PagedResultDto<GetGroupSettingsForViewDto>> GetAll(GetAllGroupSettingsInput input)
        {
            var filteredGroupSettings = _groupSettingsRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.DisplayName.Contains(input.Filter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.DisplayNameFilter), e => e.DisplayName == input.DisplayNameFilter)
                .WhereIf(input.MinOrderNoFilter != null, e => e.OrderNo >= input.MinOrderNoFilter)
                .WhereIf(input.MaxOrderNoFilter != null, e => e.OrderNo <= input.MaxOrderNoFilter);

            var pagedAndFilteredGroupSettings = filteredGroupSettings
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var totalCount = await filteredGroupSettings.CountAsync();
            var groupSettings = pagedAndFilteredGroupSettings
                .Select(o => new GetGroupSettingsForViewDto
                {
                    GroupSettings = new GroupSettingsDto
                    {
                        DisplayName = o.DisplayName,
                        OrderNo = o.OrderNo,
                        Id = o.Id.ToString()
                    }
                });

            return new PagedResultDto<GetGroupSettingsForViewDto>(
                totalCount,
                await groupSettings.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_GroupSettings)]
        public async Task<GetGroupSettingsForViewDto> GetGroupSettingsForView(int id)
        {
            var groupSettings = await _groupSettingsRepository.GetAsync(id);

            return new GetGroupSettingsForViewDto
            {
                GroupSettings = ObjectMapper.Map<GroupSettingsDto>(groupSettings)
            };
        }

        [AbpAuthorize(AppPermissions.Pages_GroupSettings_Edit)]
        public async Task<GetGroupSettingsForEditOutput> GetGroupSettingsForEdit(EntityDto input)
        {
            var groupSettings = await _groupSettingsRepository.FirstOrDefaultAsync(input.Id);

            return new GetGroupSettingsForEditOutput
            {
                GroupSettings = ObjectMapper.Map<CreateOrEditGroupSettingsDto>(groupSettings)
            };
        }

        public async Task CreateOrEdit(CreateOrEditGroupSettingsDto input)
        {
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        [AbpAuthorize(AppPermissions.Pages_GroupSettings_Create)]
        protected virtual async Task Create(CreateOrEditGroupSettingsDto input)
        {
            var groupSettings = ObjectMapper.Map<GroupSettings>(input);
            await _groupSettingsRepository.InsertAsync(groupSettings);
        }

        [AbpAuthorize(AppPermissions.Pages_GroupSettings_Edit)]
        protected virtual async Task Update(CreateOrEditGroupSettingsDto input)
        {
            var groupSettings = await _groupSettingsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, groupSettings);
        }

        [AbpAuthorize(AppPermissions.Pages_GroupSettings_Delete)]
        public async Task Delete(EntityDto input) =>
            await _groupSettingsRepository.DeleteAsync(input.Id);
    }
}