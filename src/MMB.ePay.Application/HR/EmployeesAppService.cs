﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.HR
{
    [AbpAuthorize]
    public class EmployeesAppService : ePayAppServiceBase, IEmployeesAppService
    {
        private readonly IRepository<Employees> _employeesRepository;

        public EmployeesAppService(IRepository<Employees> employeesRepository) =>
            _employeesRepository = employeesRepository;

        public async Task<bool> CheckDateRegularized()
        {
            return await _employeesRepository.GetAll()
                .AnyAsync(e => e.TenantId == AbpSession.TenantId && e.DateRegularized == null);
        }

        public async Task<IList<SelectListItem>> GetEmployeeUserList()
        {
            var employeeList = await _employeesRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .Where(e => e.User != null)
                .Select(o => new SelectListItem
                {
                    Value = o.User.Id.ToString(),
                    Text = o.FullName
                }).ToListAsync();

            employeeList.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "Select"
            });

            return employeeList;
        }

        public async Task<IList<SelectListItem>> GetEmployeeNonUserList(long? id)
        {
            var employeeList = await _employeesRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .Where(e => e.User == null || e.User.Id == id)
                .OrderBy(e => e.LastName)
                .Select(o => new SelectListItem
                {
                    Value = o.Id.ToString(),
                    Text = o.FullName,
                    Selected = (o.Id == id)
                }).ToListAsync();

            employeeList.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "Select"
            });

            return employeeList;

        }

        public async Task<IList<SelectListItem>> GetEmployeeList()
        {
            var employees = await _employeesRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .Select(o => new SelectListItem
                {
                    Value = o.Id.ToString(),
                    Text = string.Format("{0} | {1}", o.EmployeeCode, o.FullName)
                }).ToListAsync();

            employees.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "Select"
            });

            return employees;
        }

        public async Task<IList<SelectListItem>> GetEmployeeFilterList()
        {
            var employees = await _employeesRepository.GetAll()
                .WhereIf(AbpSession.TenantId != null, e => e.TenantId == AbpSession.TenantId)
                .Select(o => new SelectListItem
                {
                    Value = o.Id.ToString(),
                    Text = o.FullName
                }).ToListAsync();

            employees.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = "All Employees"
            });

            return employees;
        }

        [AbpAuthorize(AppPermissions.Pages_Employees)]
        public async Task<PagedResultDto<GetEmployeesForViewDto>> GetAll(GetAllEmployeesInput input)
        {
            var employees = _employeesRepository.GetAll().AsNoTracking()
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e =>
                    (e.EmployeeCode + " | " + e.LastName + ", " + e.FirstName + " " + e.MiddleName).Contains(input.Filter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.EmployeeStatus == input.StatusFilter)
                .WhereIf(input.TypeFilter > 0, e => e.EmploymentTypeId == input.TypeFilter)
                .Where(e => e.TenantId == AbpSession.TenantId);

            var totalCount = await employees.CountAsync();
            var pendingResult = await employees
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input)
                .Select(e => new
                {
                    e.EmployeeCode,
                    e.FullName,
                    e.Position,
                    e.EmployeeTimeInfo.RestDay,
                    ShiftType = e.EmployeeTimeInfo.ShiftTypes.DisplayName,
                    EmploymentType = e.EmploymentTypes.Description,
                    e.EmployeeStatus,
                    e.DateEmployed,
                    e.Id
                }).ToListAsync();

            var result = pendingResult.Select(e => new GetEmployeesForViewDto
            {
                Employees = new EmployeesDto
                {
                    EmployeeId = e.EmployeeCode,
                    FullName = e.FullName,
                    Position = e.Position,
                    RestDay = e.RestDay == null ? string.Empty :
                        string.Join(", ", e.RestDay.ToCharArray()
                        .Select(f => (int)char.GetNumericValue(f))
                        .Select(f => Enum.GetName(typeof(DayOfWeek),
                            f == 7 ? 0 : f).Substring(0, 3))),
                    Shift = e.ShiftType,
                    Type = e.EmploymentType,
                    Status = e.EmployeeStatus,
                    DateEmployed = e.DateEmployed,
                    Id = e.Id
                }
            }).ToList();

            return new PagedResultDto<GetEmployeesForViewDto>(totalCount, result);
        }

        public async Task<GetEmployeesForViewDto> GetEmployeesForView(int id)
        {
            var employee = await _employeesRepository.GetAll()
                .Where(e => e.Id == id)
                .Include(e => e.EmployeeTimeInfo)
                    .ThenInclude(e => e.ShiftTypes)
                .Include(e => e.EmployeeTimeInfo)
                    .ThenInclude(e => e.Frequencies)
                .Include(e => e.EmployeeTimeInfo)
                    .ThenInclude(e => e.Locations)
                .Select(e => new
                {
                    e.EmployeeCode,
                    e.FullName,
                    e.Email,
                    e.Address,
                    e.Position,
                    e.DateOfBirth,
                    e.DateEmployed,
                    e.DateTerminated,
                    e.Gender,
                    e.EmployeeStatus,
                    EmploymentType = e.EmploymentTypes.Description,
                    e.Rank,
                    e.EmployeeTimeInfo
                }).FirstOrDefaultAsync();

            var employeeDto = new EmployeesDto
            {
                EmployeeId = employee.EmployeeCode,
                FullName = employee.FullName,
                Email = employee.Email,
                Address = employee.Address,
                Position = employee.Position,
                DateOfBirth = employee.DateOfBirth,
                DateEmployed = employee.DateEmployed,
                DateTerminated = employee.DateTerminated,
                Gender = employee.Gender == "M" ? "Male" : employee.Gender == "F" ? "Female" : string.Empty,
                EmployeeStatus = employee.EmployeeStatus,
                EmploymentType = employee.EmploymentType,
                Rank = GetRankName(employee.Rank),
                RestDay = "N/A",
                ShiftType = "N/A"
            };

            if (employee.EmployeeTimeInfo != null)
            {
                var employeeTimeInfo = employee.EmployeeTimeInfo;

                employeeDto.RestDay = string.Join(", ",
                    employee.EmployeeTimeInfo.RestDay.ToCharArray()
                        .Select(f => (int)char.GetNumericValue(f))
                        .Select(f => Enum.GetName(typeof(DayOfWeek), f == 7 ? 0 : f).Substring(0, 3)));

                employeeDto.Location = employeeTimeInfo.Locations?.Description;
                employeeDto.Frequency = employeeTimeInfo.Frequencies.DisplayName;
                employeeDto.ShiftType = employeeTimeInfo.ShiftTypes.DisplayName;
                employeeDto.Overtime = employeeTimeInfo.Overtime;
                employeeDto.Undertime = employeeTimeInfo.Undertime;
                employeeDto.Tardy = employeeTimeInfo.Tardy;
                employeeDto.NonSwiper = employeeTimeInfo.NonSwiper;
            }

            return new GetEmployeesForViewDto
            {
                Employees = employeeDto
            };
        }

        private static string GetRankName(string rankCode)
        {
            switch (rankCode)
            {
                case "R&F":
                    return "Rank & File";
                case "SUP":
                    return "Supervisor";
                case "MGR":
                    return "Manager";
                case "EXC":
                    return "Executive";
                default:
                    break;
            }

            return string.Empty;
        }

        [AbpAuthorize(AppPermissions.Pages_Employees_Edit)]
        public async Task<GetEmployeesForEditOutput> GetEmployeesForEdit(EntityDto input)
        {
            var employees = await _employeesRepository.FirstOrDefaultAsync(input.Id);

            return new GetEmployeesForEditOutput 
            { 
                Employees = ObjectMapper.Map<CreateOrEditEmployeesDto>(employees) 
            };
        }

        public async Task CreateOrEdit(CreateOrEditEmployeesDto input)
        {
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        [AbpAuthorize(AppPermissions.Pages_Employees_Create)]
        protected virtual async Task Create(CreateOrEditEmployeesDto input)
        {
            var employees = ObjectMapper.Map<Employees>(input);
            employees.EmployeeStatus = "ACTIVE";

            await _employeesRepository.InsertAsync(employees);
        }

        [AbpAuthorize(AppPermissions.Pages_Employees_Edit)]
        protected virtual async Task Update(CreateOrEditEmployeesDto input)
        {
            var employees = await _employeesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, employees);
        }

        [AbpAuthorize(AppPermissions.Pages_Employees_Delete)]
        public async Task Delete(EntityDto input) =>
            await _employeesRepository.DeleteAsync(input.Id);
    }
}