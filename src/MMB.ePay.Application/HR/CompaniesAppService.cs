﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.HR.Dtos;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    [AbpAuthorize]
    public class CompaniesAppService : ePayAppServiceBase, ICompaniesAppService
    {
        private readonly IRepository<Companies> _companiesRepository;

        public CompaniesAppService(IRepository<Companies> companiesRepository) =>
            _companiesRepository = companiesRepository;

        public async Task<Tuple<string, string, string>> GetOrgNames()
        {
            var company = await _companiesRepository.GetAll()
                .Where(e => e.TenantId == AbpSession.TenantId)
                .Select(e => new Tuple<string, string, string>
                    (
                        e.OrgUnitLevel1Name,
                        e.OrgUnitLevel2Name,
                        e.OrgUnitLevel3Name
                    )
                ).FirstOrDefaultAsync();

            return company;
        }

        [AbpAuthorize(AppPermissions.Pages_Companies)]
        public async Task<PagedResultDto<GetCompaniesForViewDto>> GetAll(GetAllCompaniesInput input)
        {
            var filteredCompanies = _companiesRepository.GetAll();

            var pagedAndFilteredCompanies = filteredCompanies
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input);

            var companies = pagedAndFilteredCompanies
                .Select(o => new GetCompaniesForViewDto
                {
                    Companies = new CompaniesDto
                    {
                        DisplayName = o.DisplayName,
                        Address1 = o.Address1,
                        Address2 = o.Address2,
                        OrgUnitLevel1Name = o.OrgUnitLevel1Name,
                        OrgUnitLevel2Name = o.OrgUnitLevel2Name,
                        OrgUnitLevel3Name = o.OrgUnitLevel3Name,
                        Id = o.Id
                    }
                });

            var totalCount = await filteredCompanies.CountAsync();
            var results = await companies.ToListAsync();

            return new PagedResultDto<GetCompaniesForViewDto>(
                totalCount,
                results
            );

        }

        [AbpAuthorize(AppPermissions.Pages_Companies)]
        public async Task<GetCompaniesForViewDto> GetCompaniesForView(int id) =>
            await _companiesRepository.GetAll()
                .Where(e => e.Id == id)
                .Select(e => new GetCompaniesForViewDto
                {
                    Companies = new CompaniesDto
                    {
                        DisplayName = e.DisplayName,
                        Address1 = e.Address1,
                        Address2 = e.Address2,
                        OrgUnitLevel1Name = e.OrgUnitLevel1Name,
                        OrgUnitLevel2Name = e.OrgUnitLevel2Name,
                        OrgUnitLevel3Name = e.OrgUnitLevel3Name
                    }
                }).FirstOrDefaultAsync();

        [AbpAuthorize(AppPermissions.Pages_Companies_Edit)]
        public async Task<GetCompaniesForEditOutput> GetCompaniesForEdit(EntityDto input)
        {
            var companies = await _companiesRepository.FirstOrDefaultAsync(input.Id);

            return new GetCompaniesForEditOutput 
            { 
                Companies = ObjectMapper.Map<CreateOrEditCompaniesDto>(companies) 
            };
        }

        public async Task CreateOrEdit(CreateOrEditCompaniesDto input)
        {
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        protected virtual async Task Validate(CreateOrEditCompaniesDto input)
        {
            if (await _companiesRepository.GetAll()
                .AnyAsync(e => e.DisplayName == input.DisplayName && e.Id != input.Id))
                throw new UserFriendlyException("Your request is not valid!", "An identical record already exists.");
        }

        [AbpAuthorize(AppPermissions.Pages_Companies_Create)]
        protected virtual async Task Create(CreateOrEditCompaniesDto input)
        {
            var companies = ObjectMapper.Map<Companies>(input);

            await Validate(input);
            await _companiesRepository.InsertAsync(companies);
        }

        [AbpAuthorize(AppPermissions.Pages_Companies_Edit)]
        protected virtual async Task Update(CreateOrEditCompaniesDto input)
        {
            var companies = await _companiesRepository.FirstOrDefaultAsync((int)input.Id);

            await Validate(input);
            ObjectMapper.Map(input, companies);
        }

        [AbpAuthorize(AppPermissions.Pages_Companies_Delete)]
        public async Task Delete(EntityDto input) =>
            await _companiesRepository.DeleteAsync(input.Id);
    }
}