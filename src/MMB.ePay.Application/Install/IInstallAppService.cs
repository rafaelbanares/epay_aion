﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MMB.ePay.Install.Dto;

namespace MMB.ePay.Install
{
    public interface IInstallAppService : IApplicationService
    {
        Task Setup(InstallDto input);

        AppSettingsJsonDto GetAppSettingsJson();

        CheckDatabaseOutput CheckDatabase();
    }
}