﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp;
using MMB.ePay.Dto;

namespace MMB.ePay.Gdpr
{
    public interface IUserCollectedDataProvider
    {
        Task<List<FileDto>> GetFiles(UserIdentifier user);
    }
}
