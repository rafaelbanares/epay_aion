﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;

namespace MMB.ePay.Core.Extensions
{
    /// <summary>
    /// Custom extensions for utility purposes
    /// </summary>
    public static class UtilityExtensions
    {
        public static IQueryable<T> WhereDynamic<T>(
            this IQueryable<T> sourceList, string query, string propertySearch = null)
        {

            if (string.IsNullOrEmpty(query))
            {
                return sourceList;
            }

            try
            {

                var properties = typeof(T).GetProperties()
                    .Where(x => x.Name == propertySearch || propertySearch == null);
                    //.Where(x => x.CanRead && x.CanWrite && !x.GetGetMethod().IsVirtual);

                //Expression
                sourceList = sourceList.Where(c =>
                    properties.Any(p => p.GetValue(c).ToString()
                        .Contains(query, StringComparison.InvariantCultureIgnoreCase)));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return sourceList;
        }

        public enum Order
        {
            Asc,
            Desc
        }

        public static IQueryable<T> OrderByDynamic<T>(
            this IQueryable<T> query,
            string orderByMember,
            Order direction)
        {
            var queryElementTypeParam = Expression.Parameter(typeof(T));

            var memberAccess = Expression.PropertyOrField(queryElementTypeParam, orderByMember);

            var keySelector = Expression.Lambda(memberAccess, queryElementTypeParam);

            var orderBy = Expression.Call(
                typeof(Queryable),
                direction == Order.Asc ? "OrderBy" : "OrderByDescending",
                new Type[] { typeof(T), memberAccess.Type },
                query.Expression,
                Expression.Quote(keySelector));

            return query.Provider.CreateQuery<T>(orderBy);
        }

        /// <summary>
        ///Checks if two date ranges overlap with each other
        /// </summary>
        public static bool DateRangesOverlap(DateTime startDate1, DateTime endDate1,
            DateTime startDate2, DateTime endDate2)
        {
            if ((startDate1 >= startDate2 && startDate1 <= endDate2)
                || (endDate1 >= startDate2 && endDate1 <= endDate2)
                || (startDate2 >= startDate1 && startDate2 <= endDate1)
                || (endDate2 >= startDate1 && endDate2 <= endDate1))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if date is between two dates
        /// </summary>
        public static bool Between(this DateTime mid, DateTime? left, DateTime? right)
        {
            if (left == null || right == null)
                return false;

            return (left <= mid && mid <= right);
        }

        /// <summary>
        /// Checks if an array of strings is within a string
        /// </summary>
        public static bool ContainsAny(this string haystack, params string[] needles)
        {
            foreach (string needle in needles)
            {
                if (haystack.Contains(needle))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if string is within an array of strings
        /// </summary>
        public static bool EqualsAny(this string haystack, params string[] needles)
        {
            foreach (string needle in needles)
            {
                if (haystack == needle)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if int is within an array of int
        /// </summary>
        public static bool EqualsAny(this int haystack, params int[] needles)
        {
            foreach (int needle in needles)
            {
                if (haystack == needle)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Convert day codes to list of string
        /// </summary>
        public static List<string> ToDayList(this string dayCodes)
        {
            var selectedDays = new List<string>();
            selectedDays.AddRange(dayCodes.Select(c => c.ToString()));

            return selectedDays;
        }

        /// <summary>
        /// Convert list of days to list of bool based on availability
        /// </summary>
        public static IList<SelectListItem> ToWeekList(this IList<string> dayList)
        {
            return Enum.GetValues(typeof(DayOfWeek)).Cast<DayOfWeek>().
                    Select(e => new SelectListItem
                    {
                        Text = e.ToString(),
                        Value = ((int)e).ToString(),
                        Selected = (dayList == null) ? false :
                        (dayList.Contains(((int)e).ToString()))
                    }).ToList();
        }

        /// <summary>
        /// Sets datetime to null if minvalue
        /// </summary>
        public static DateTime? IsNullable(this DateTime date)
        {
            if (date == DateTime.MinValue)
                return null;

            return date;
        }

        public static string GetPayrollFrequencyName(this string frequencyCodes)
        {
            var result = new List<string>();

            if (frequencyCodes.Contains("1"))
                result.Add("1st");

            if (frequencyCodes.Contains("2"))
                result.Add("2nd");

            if (frequencyCodes.Contains("3"))
                result.Add("3rd");

            if (frequencyCodes.Contains("4"))
                result.Add("4th");

            if (frequencyCodes.Contains("5"))
                result.Add("Last");

            return string.Join(", ", result);
        }

        /// <summary>
        /// Removes duplicate letters of a string
        /// </summary>
        public static string RemoveDuplicate(this string str)
        {
            string result = "";

            foreach (char c in str)
            {
                // See if character is in the result already.
                if (result.IndexOf(c) == -1)
                {
                    // Append to the result.
                    result += c;
                }
            }
            return result;
        }

        /// <summary>
        /// Count number of given DayOfWeek codes within a date range
        /// </summary>
        public static int CountDays(string dayCodes, DateTime startDate, DateTime endDate)
        {
            var totalDays = (endDate - startDate).TotalDays;
            var count = Math.Floor(totalDays / 7);
            var remainder = totalDays % 7;

            foreach (char code in dayCodes)
            {
                var sinceLastDay = endDate.DayOfWeek - (DayOfWeek)(code - '0');
                if (sinceLastDay < 0)
                    sinceLastDay += 7;

                if (remainder >= sinceLastDay)
                    count++;
            }

            return (int)count;
        }

        /// <summary>
        /// Joins a collection of string into one string
        /// </summary>
        public static string Join(this IEnumerable<string> strings, string separator = "")
        {
            return string.Join(separator, strings.ToArray());
        }

        public static string AddQueryString(this Dictionary<string, string> param)
        {
            if (param == null) return null;
            return string.Join("/", param.Values);

        }
        //public static int CountDays(DayOfWeek day, DateTime start, DateTime end)
        //{
        //    TimeSpan ts = end - start;                       // Total duration
        //    int count = (int)Math.Floor(ts.TotalDays / 7);   // Number of whole weeks
        //    int remainder = (int)(ts.TotalDays % 7);         // Number of remaining days
        //    int sinceLastDay = (int)(end.DayOfWeek - day);   // Number of days since last [day]
        //    if (sinceLastDay < 0) sinceLastDay += 7;         // Adjust for negative days since last [day]

        //    // If the days in excess of an even week are greater than or equal to the number days since the last [day], then count this one, too.
        //    if (remainder >= sinceLastDay) count++;

        //    return count;
        //}
    }

    public class EnsureOneElementAttribute : ValidationAttribute
    {
        /// <summary>
        /// Returns true if list has more than 1
        /// </summary>
        public override bool IsValid(object value)
        {
            var list = (IList)value;
            if (list != null)
            {
                return list.Count > 0;
            }
            return false;
        }
    }
}
