﻿using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Repositories;

namespace Abp.Auditing
{
    /// <summary>
    /// Implements <see cref="IAuditingStore"/> to save auditing informations to database.
    /// </summary>
    public class ePayAuditingStore : IAuditingStore, ITransientDependency
    {
        private readonly IRepository<AuditLog, long> _auditLogRepository;

        public ePayAuditingStore(IRepository<AuditLog, long> auditLogRepository)
        {
            _auditLogRepository = auditLogRepository;
        }

        private bool Validate(AuditInfo auditInfo)
        {
            if (auditInfo.MethodName == "Post")
            {
                return true;
            }

            if (auditInfo.MethodName == "Process")
            {
                return true;
            }

            if (auditInfo.MethodName == "Login" && auditInfo.Parameters != "{}")
            {
                return true;
            }

            if (auditInfo.MethodName == "Logout")
            {
                return true;
            }

            if (auditInfo.Exception != null)
            {
                return true;
            }

            return false;
        }

        public async Task SaveAsync(AuditInfo auditInfo)
        {
            if (Validate(auditInfo))
            {
                await _auditLogRepository.InsertAsync(AuditLog.CreateFromAuditInfo(auditInfo));
            }
        }

        public virtual void Save(AuditInfo auditInfo)
        {
            if (Validate(auditInfo))
            {
                _auditLogRepository.Insert(AuditLog.CreateFromAuditInfo(auditInfo));
            }
        }
    }
}