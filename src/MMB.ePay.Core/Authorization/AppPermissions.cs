﻿namespace MMB.ePay.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        public const string Pages_PostedAttendances = "Pages.PostedAttendances";
        public const string Pages_PostedAttendances_Create = "Pages.PostedAttendances.Create";
        public const string Pages_PostedAttendances_Edit = "Pages.PostedAttendances.Edit";
        public const string Pages_PostedAttendances_Delete = "Pages.PostedAttendances.Delete";

        public const string Pages_LeaveTypes = "Pages.LeaveTypes";
        public const string Pages_LeaveTypes_Create = "Pages.LeaveTypes.Create";
        public const string Pages_LeaveTypes_Edit = "Pages.LeaveTypes.Edit";
        public const string Pages_LeaveTypes_Delete = "Pages.LeaveTypes.Delete";

        public const string Pages_ApproverOrders = "Pages.ApproverOrders";
        public const string Pages_ApproverOrders_Create = "Pages.ApproverOrders.Create";
        public const string Pages_ApproverOrders_Edit = "Pages.ApproverOrders.Edit";
        public const string Pages_ApproverOrders_Delete = "Pages.ApproverOrders.Delete";

        public const string Pages_AttendanceApprovals = "Pages.AttendanceApprovals";
        public const string Pages_AttendanceApprovals_Create = "Pages.AttendanceApprovals.Create";
        public const string Pages_AttendanceApprovals_Edit = "Pages.AttendanceApprovals.Edit";
        public const string Pages_AttendanceApprovals_Admin = "Pages.AttendanceApprovals.Admin";

        public const string Pages_AttendancePeriods = "Pages.AttendancePeriods";
        public const string Pages_AttendancePeriods_Create = "Pages.AttendancePeriods.Create";
        public const string Pages_AttendancePeriods_Edit = "Pages.AttendancePeriods.Edit";
        public const string Pages_AttendancePeriods_Delete = "Pages.AttendancePeriods.Delete";

        public const string Pages_AttendanceReasons = "Pages.AttendanceReasons";
        public const string Pages_AttendanceReasons_Create = "Pages.AttendanceReasons.Create";
        public const string Pages_AttendanceReasons_Edit = "Pages.AttendanceReasons.Edit";
        public const string Pages_AttendanceReasons_Delete = "Pages.AttendanceReasons.Delete";

        public const string Pages_AttendanceRequests = "Pages.AttendanceRequests";
        public const string Pages_AttendanceRequests_Create = "Pages.AttendanceRequests.Create";
        public const string Pages_AttendanceRequests_Edit = "Pages.AttendanceRequests.Edit";

        public const string Pages_AdminAttendances = "Pages.AdminAttendances";

        public const string Pages_Attendances = "Pages.Attendances";
        public const string Pages_Attendances_Create = "Pages.Attendances.Create";
        public const string Pages_Attendances_Edit = "Pages.Attendances.Edit";
        public const string Pages_Attendances_Delete = "Pages.Attendances.Delete";
        public const string Pages_Attendances_Upload = "Pages.Attendances.Upload";
        public const string Pages_Attendances_Post = "Pages.Attendances.Post";
        public const string Pages_Attendances_Process = "Pages.Attendances.Process";
        public const string Pages_Attendances_Summarize = "Pages.Attendances.Summarize";

        public const string Pages_EmployeeApprovers = "Pages.EmployeeApprovers";
        public const string Pages_EmployeeApprovers_Create = "Pages.EmployeeApprovers.Create";
        public const string Pages_EmployeeApprovers_Edit = "Pages.EmployeeApprovers.Edit";
        public const string Pages_EmployeeApprovers_Delete = "Pages.EmployeeApprovers.Delete";

        public const string Pages_EmployeeTimeInfo = "Pages.EmployeeTimeInfo";
        public const string Pages_EmployeeTimeInfo_Create = "Pages.EmployeeTimeInfo.Create";
        public const string Pages_EmployeeTimeInfo_Edit = "Pages.EmployeeTimeInfo.Edit";
        public const string Pages_EmployeeTimeInfo_Delete = "Pages.EmployeeTimeInfo.Delete";

        public const string Pages_ExcuseApprovals = "Pages.ExcuseApprovals";
        public const string Pages_ExcuseApprovals_Create = "Pages.ExcuseApprovals.Create";
        public const string Pages_ExcuseApprovals_Edit = "Pages.ExcuseApprovals.Edit";
        public const string Pages_ExcuseApprovals_Admin = "Pages.ExcuseApprovals.Admin";

        public const string Pages_ExcuseReasons = "Pages.ExcuseReasons";
        public const string Pages_ExcuseReasons_Create = "Pages.ExcuseReasons.Create";
        public const string Pages_ExcuseReasons_Edit = "Pages.ExcuseReasons.Edit";
        public const string Pages_ExcuseReasons_Delete = "Pages.ExcuseReasons.Delete";

        public const string Pages_ExcuseRequests = "Pages.ExcuseRequests";
        public const string Pages_ExcuseRequests_Create = "Pages.ExcuseRequests.Create";
        public const string Pages_ExcuseRequests_Edit = "Pages.ExcuseRequests.Edit";

        public const string Pages_LeaveApprovals = "Pages.LeaveApprovals";
        public const string Pages_LeaveApprovals_Create = "Pages.LeaveApprovals.Create";
        public const string Pages_LeaveApprovals_Edit = "Pages.LeaveApprovals.Edit";
        public const string Pages_LeaveApprovals_Admin = "Pages.LeaveApprovals.Admin";

        public const string Pages_LeaveEarnings = "Pages.LeaveEarnings";
        public const string Pages_LeaveEarnings_Create = "Pages.LeaveEarnings.Create";
        public const string Pages_LeaveEarnings_Edit = "Pages.LeaveEarnings.Edit";
        public const string Pages_LeaveEarnings_Delete = "Pages.LeaveEarnings.Delete";

        public const string Pages_LeaveReasons = "Pages.LeaveReasons";
        public const string Pages_LeaveReasons_Create = "Pages.LeaveReasons.Create";
        public const string Pages_LeaveReasons_Edit = "Pages.LeaveReasons.Edit";
        public const string Pages_LeaveReasons_Delete = "Pages.LeaveReasons.Delete";

        public const string Pages_LeaveRequestDetails = "Pages.LeaveRequestDetails";
        public const string Pages_LeaveRequestDetails_Create = "Pages.LeaveRequestDetails.Create";
        public const string Pages_LeaveRequestDetails_Edit = "Pages.LeaveRequestDetails.Edit";
        public const string Pages_LeaveRequestDetails_Delete = "Pages.LeaveRequestDetails.Delete";

        public const string Pages_LeaveRequests = "Pages.LeaveRequests";
        public const string Pages_LeaveRequests_Create = "Pages.LeaveRequests.Create";
        public const string Pages_LeaveRequests_Edit = "Pages.LeaveRequests.Edit";

        public const string Pages_OfficialBusinessApprovals = "Pages.OfficialBusinessApprovals";
        public const string Pages_OfficialBusinessApprovals_Create = "Pages.OfficialBusinessApprovals.Create";
        public const string Pages_OfficialBusinessApprovals_Edit = "Pages.OfficialBusinessApprovals.Edit";
        public const string Pages_OfficialBusinessApprovals_Admin = "Pages.OfficialBusinessApprovals.Admin";

        public const string Pages_OfficialBusinessReasons = "Pages.OfficialBusinessReasons";
        public const string Pages_OfficialBusinessReasons_Create = "Pages.OfficialBusinessReasons.Create";
        public const string Pages_OfficialBusinessReasons_Edit = "Pages.OfficialBusinessReasons.Edit";
        public const string Pages_OfficialBusinessReasons_Delete = "Pages.OfficialBusinessReasons.Delete";

        public const string Pages_OfficialBusinessRequestDetails = "Pages.OfficialBusinessRequestDetails";
        public const string Pages_OfficialBusinessRequestDetails_Create = "Pages.OfficialBusinessRequestDetails.Create";
        public const string Pages_OfficialBusinessRequestDetails_Edit = "Pages.OfficialBusinessRequestDetails.Edit";
        public const string Pages_OfficialBusinessRequestDetails_Delete = "Pages.OfficialBusinessRequestDetails.Delete";

        public const string Pages_OfficialBusinessRequests = "Pages.OfficialBusinessRequests";
        public const string Pages_OfficialBusinessRequests_Create = "Pages.OfficialBusinessRequests.Create";
        public const string Pages_OfficialBusinessRequests_Edit = "Pages.OfficialBusinessRequests.Edit";

        public const string Pages_OvertimeApprovals = "Pages.OvertimeApprovals";
        public const string Pages_OvertimeApprovals_Create = "Pages.OvertimeApprovals.Create";
        public const string Pages_OvertimeApprovals_Edit = "Pages.OvertimeApprovals.Edit";
        public const string Pages_OvertimeApprovals_Admin = "Pages.OvertimeApprovals.Admin";

        public const string Pages_OvertimeReasons = "Pages.OvertimeReasons";
        public const string Pages_OvertimeReasons_Create = "Pages.OvertimeReasons.Create";
        public const string Pages_OvertimeReasons_Edit = "Pages.OvertimeReasons.Edit";
        public const string Pages_OvertimeReasons_Delete = "Pages.OvertimeReasons.Delete";

        public const string Pages_OvertimeRequests = "Pages.OvertimeRequests";
        public const string Pages_OvertimeRequests_Create = "Pages.OvertimeRequests.Create";
        public const string Pages_OvertimeRequests_Edit = "Pages.OvertimeRequests.Edit";

        public const string Pages_RawTimeLogs = "Pages.RawTimeLogs";
        public const string Pages_RawTimeLogs_Create = "Pages.RawTimeLogs.Create";
        public const string Pages_RawTimeLogs_Edit = "Pages.RawTimeLogs.Edit";
        public const string Pages_RawTimeLogs_Delete = "Pages.RawTimeLogs.Delete";

        public const string Pages_RestDayApprovals = "Pages.RestDayApprovals";
        public const string Pages_RestDayApprovals_Create = "Pages.RestDayApprovals.Create";
        public const string Pages_RestDayApprovals_Edit = "Pages.RestDayApprovals.Edit";
        public const string Pages_RestDayApprovals_Admin = "Pages.RestDayApprovals.Admin";

        public const string Pages_RestDayReasons = "Pages.RestDayReasons";
        public const string Pages_RestDayReasons_Create = "Pages.RestDayReasons.Create";
        public const string Pages_RestDayReasons_Edit = "Pages.RestDayReasons.Edit";
        public const string Pages_RestDayReasons_Delete = "Pages.RestDayReasons.Delete";

        public const string Pages_RestDayRequests = "Pages.RestDayRequests";
        public const string Pages_RestDayRequests_Create = "Pages.RestDayRequests.Create";
        public const string Pages_RestDayRequests_Edit = "Pages.RestDayRequests.Edit";

        public const string Pages_ShiftApprovals = "Pages.ShiftApprovals";
        public const string Pages_ShiftApprovals_Create = "Pages.ShiftApprovals.Create";
        public const string Pages_ShiftApprovals_Edit = "Pages.ShiftApprovals.Edit";
        public const string Pages_ShiftApprovals_Admin = "Pages.ShiftApprovals.Admin";

        public const string Pages_ShiftReasons = "Pages.ShiftReasons";
        public const string Pages_ShiftReasons_Create = "Pages.ShiftReasons.Create";
        public const string Pages_ShiftReasons_Edit = "Pages.ShiftReasons.Edit";
        public const string Pages_ShiftReasons_Delete = "Pages.ShiftReasons.Delete";

        public const string Pages_ShiftRequests = "Pages.ShiftRequests";
        public const string Pages_ShiftRequests_Create = "Pages.ShiftRequests.Create";
        public const string Pages_ShiftRequests_Edit = "Pages.ShiftRequests.Edit";

        public const string Pages_ShiftTypeDetails = "Pages.ShiftTypeDetails";
        public const string Pages_ShiftTypeDetails_Create = "Pages.ShiftTypeDetails.Create";
        public const string Pages_ShiftTypeDetails_Edit = "Pages.ShiftTypeDetails.Edit";
        public const string Pages_ShiftTypeDetails_Delete = "Pages.ShiftTypeDetails.Delete";

        public const string Pages_ShiftTypes = "Pages.ShiftTypes";
        public const string Pages_ShiftTypes_Create = "Pages.ShiftTypes.Create";
        public const string Pages_ShiftTypes_Edit = "Pages.ShiftTypes.Edit";
        public const string Pages_ShiftTypes_Delete = "Pages.ShiftTypes.Delete";

        public const string Pages_Status = "Pages.Status";
        public const string Pages_Status_Create = "Pages.Status.Create";
        public const string Pages_Status_Edit = "Pages.Status.Edit";
        public const string Pages_Status_Delete = "Pages.Status.Delete";

        public const string Pages_TKSettings = "Pages.TKSettings";
        public const string Pages_TKSettings_Create = "Pages.TKSettings.Create";
        public const string Pages_TKSettings_Edit = "Pages.TKSettings.Edit";
        public const string Pages_TKSettings_Delete = "Pages.TKSettings.Delete";

        public const string Pages_WorkFlow = "Pages.WorkFlow";
        public const string Pages_WorkFlow_Create = "Pages.WorkFlow.Create";
        public const string Pages_WorkFlow_Edit = "Pages.WorkFlow.Edit";
        public const string Pages_WorkFlow_Delete = "Pages.WorkFlow.Delete";

        public const string Pages_Locations = "Pages.Locations";
        public const string Pages_Locations_Create = "Pages.Locations.Create";
        public const string Pages_Locations_Edit = "Pages.Locations.Edit";
        public const string Pages_Locations_Delete = "Pages.Locations.Delete";

        public const string Pages_Holidays = "Pages.Holidays";
        public const string Pages_Holidays_Create = "Pages.Holidays.Create";
        public const string Pages_Holidays_Edit = "Pages.Holidays.Edit";
        public const string Pages_Holidays_Delete = "Pages.Holidays.Delete";

        public const string Pages_HolidayTypes = "Pages.HolidayTypes";
        public const string Pages_HolidayTypes_Create = "Pages.HolidayTypes.Create";
        public const string Pages_HolidayTypes_Edit = "Pages.HolidayTypes.Edit";
        public const string Pages_HolidayTypes_Delete = "Pages.HolidayTypes.Delete";

        public const string Pages_Reports = "Pages.Reports";
        public const string Pages_OvertimeFormats = "Pages.OvertimeFormats";

        public const string Pages_EmploymentTypes = "Pages.EmploymentTypes";
        public const string Pages_EmploymentTypes_Create = "Pages.EmploymentTypes.Create";
        public const string Pages_EmploymentTypes_Edit = "Pages.EmploymentTypes.Edit";
        public const string Pages_EmploymentTypes_Delete = "Pages.EmploymentTypes.Delete";

        public const string Pages_GroupSettings = "Pages.GroupSettings";
        public const string Pages_GroupSettings_Create = "Pages.GroupSettings.Create";
        public const string Pages_GroupSettings_Edit = "Pages.GroupSettings.Edit";
        public const string Pages_GroupSettings_Delete = "Pages.GroupSettings.Delete";

        public const string Pages_OvertimeTypes = "Pages.OvertimeTypes";
        public const string Pages_OvertimeTypes_Create = "Pages.OvertimeTypes.Create";
        public const string Pages_OvertimeTypes_Edit = "Pages.OvertimeTypes.Edit";
        public const string Pages_OvertimeTypes_Delete = "Pages.OvertimeTypes.Delete";

        public const string Pages_Process = "Pages.Process";
        public const string Pages_Process_Create = "Pages.Process.Create";
        public const string Pages_Process_Edit = "Pages.Process.Edit";
        public const string Pages_Process_Delete = "Pages.Process.Delete";

        public const string Pages_Frequencies = "Pages.Frequencies";
        public const string Pages_Frequencies_Create = "Pages.Frequencies.Create";
        public const string Pages_Frequencies_Edit = "Pages.Frequencies.Edit";
        public const string Pages_Frequencies_Delete = "Pages.Frequencies.Delete";

        public const string Pages_Companies = "Pages.Companies";
        public const string Pages_Companies_Create = "Pages.Companies.Create";
        public const string Pages_Companies_Edit = "Pages.Companies.Edit";
        public const string Pages_Companies_Delete = "Pages.Companies.Delete";

        public const string Pages_Employees = "Pages.Employees";
        public const string Pages_Employees_Create = "Pages.Employees.Create";
        public const string Pages_Employees_Edit = "Pages.Employees.Edit";
        public const string Pages_Employees_Delete = "Pages.Employees.Delete";

        public const string Pages_Processes = "Pages.Processes";

        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";

        public const string Pages_DemoUiComponents = "Pages.DemoUiComponents";
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";
        public const string Pages_Administration_Users_Unlock = "Pages.Administration.Users.Unlock";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";
        public const string Pages_Administration_OrganizationUnits_ManageRoles = "Pages.Administration.OrganizationUnits.ManageRoles";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";
        public const string Pages_Administration_Encryption = "Pages.Administration.Encryption";
        public const string Pages_Administration_SpeedTest = "Pages.Administration.SpeedTest";

        public const string Pages_Administration_UiCustomization = "Pages.Administration.UiCustomization";

        public const string Pages_Administration_WebhookSubscription = "Pages.Administration.WebhookSubscription";
        public const string Pages_Administration_WebhookSubscription_Create = "Pages.Administration.WebhookSubscription.Create";
        public const string Pages_Administration_WebhookSubscription_Edit = "Pages.Administration.WebhookSubscription.Edit";
        public const string Pages_Administration_WebhookSubscription_ChangeActivity = "Pages.Administration.WebhookSubscription.ChangeActivity";
        public const string Pages_Administration_WebhookSubscription_Detail = "Pages.Administration.WebhookSubscription.Detail";
        public const string Pages_Administration_Webhook_ListSendAttempts = "Pages.Administration.Webhook.ListSendAttempts";
        public const string Pages_Administration_Webhook_ResendWebhook = "Pages.Administration.Webhook.ResendWebhook";

        public const string Pages_Administration_DynamicProperties = "Pages.Administration.DynamicProperties";
        public const string Pages_Administration_DynamicProperties_Create = "Pages.Administration.DynamicProperties.Create";
        public const string Pages_Administration_DynamicProperties_Edit = "Pages.Administration.DynamicProperties.Edit";
        public const string Pages_Administration_DynamicProperties_Delete = "Pages.Administration.DynamicProperties.Delete";

        public const string Pages_Administration_DynamicPropertyValue = "Pages.Administration.DynamicPropertyValue";
        public const string Pages_Administration_DynamicPropertyValue_Create = "Pages.Administration.DynamicPropertyValue.Create";
        public const string Pages_Administration_DynamicPropertyValue_Edit = "Pages.Administration.DynamicPropertyValue.Edit";
        public const string Pages_Administration_DynamicPropertyValue_Delete = "Pages.Administration.DynamicPropertyValue.Delete";

        public const string Pages_Administration_DynamicEntityProperties = "Pages.Administration.DynamicEntityProperties";
        public const string Pages_Administration_DynamicEntityProperties_Create = "Pages.Administration.DynamicEntityProperties.Create";
        public const string Pages_Administration_DynamicEntityProperties_Edit = "Pages.Administration.DynamicEntityProperties.Edit";
        public const string Pages_Administration_DynamicEntityProperties_Delete = "Pages.Administration.DynamicEntityProperties.Delete";

        public const string Pages_Administration_DynamicEntityPropertyValue = "Pages.Administration.DynamicEntityPropertyValue";
        public const string Pages_Administration_DynamicEntityPropertyValue_Create = "Pages.Administration.DynamicEntityPropertyValue.Create";
        public const string Pages_Administration_DynamicEntityPropertyValue_Edit = "Pages.Administration.DynamicEntityPropertyValue.Edit";
        public const string Pages_Administration_DynamicEntityPropertyValue_Delete = "Pages.Administration.DynamicEntityPropertyValue.Delete";

        //TEST-SPECIFIC PERMISSIONS

        //public const string Pages_Tenant_ePay = "Pages.Tenant.ePay";
        //public const string Pages_Tenant_ePay_CreatePerson = "Pages.Tenant.ePay.CreatePerson";
        //public const string Pages_Tenant_ePay_DeletePerson = "Pages.Tenant.ePay.DeletePerson";

        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Administration_Tenant_SubscriptionManagement = "Pages.Administration.Tenant.SubscriptionManagement";

        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";
        public const string Pages_Editions_MoveTenantsToAnotherEdition = "Pages.Editions.MoveTenantsToAnotherEdition";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_EmailQueue = "Pages.Administration.Host.EmailQueue";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
        public const string Pages_Administration_Host_Dashboard = "Pages.Administration.Host.Dashboard";

    }
}