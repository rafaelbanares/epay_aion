﻿using System.Threading.Tasks;
using Abp.Authorization.Users;
using MMB.ePay.Authorization.Users;

namespace MMB.ePay.Authorization
{
    public static class UserManagerExtensions
    {
        public static async Task<User> GetAdminAsync(this UserManager userManager)
        {
            return await userManager.FindByNameAsync(AbpUserBase.AdminUserName);
        }
    }
}
