﻿namespace MMB.ePay.Authorization.Delegation
{
    public interface IUserDelegationConfiguration
    {
        bool IsEnabled { get; set; }
    }
}