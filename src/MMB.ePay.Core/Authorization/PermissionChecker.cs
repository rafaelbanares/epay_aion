﻿using Abp.Authorization;
using MMB.ePay.Authorization.Roles;
using MMB.ePay.Authorization.Users;

namespace MMB.ePay.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
