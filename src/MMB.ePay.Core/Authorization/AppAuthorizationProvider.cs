﻿using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace MMB.ePay.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        private readonly bool _isMultiTenancyEnabled;

        public AppAuthorizationProvider(bool isMultiTenancyEnabled)
        {
            _isMultiTenancyEnabled = isMultiTenancyEnabled;
        }

        public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
        {
            _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
        }

        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));

            var postedAttendances = pages.CreateChildPermission(AppPermissions.Pages_PostedAttendances, L("PostedAttendances"), multiTenancySides: MultiTenancySides.Tenant);
            postedAttendances.CreateChildPermission(AppPermissions.Pages_PostedAttendances_Create, L("CreateNewPostedAttendances"), multiTenancySides: MultiTenancySides.Tenant);
            postedAttendances.CreateChildPermission(AppPermissions.Pages_PostedAttendances_Edit, L("EditPostedAttendances"), multiTenancySides: MultiTenancySides.Tenant);
            postedAttendances.CreateChildPermission(AppPermissions.Pages_PostedAttendances_Delete, L("DeletePostedAttendances"), multiTenancySides: MultiTenancySides.Tenant);

            var locations = pages.CreateChildPermission(AppPermissions.Pages_Locations, L("Locations"), multiTenancySides: MultiTenancySides.Tenant);
            locations.CreateChildPermission(AppPermissions.Pages_Locations_Create, L("CreateNewLocations"), multiTenancySides: MultiTenancySides.Tenant);
            locations.CreateChildPermission(AppPermissions.Pages_Locations_Edit, L("EditLocations"), multiTenancySides: MultiTenancySides.Tenant);
            locations.CreateChildPermission(AppPermissions.Pages_Locations_Delete, L("DeleteLocations"), multiTenancySides: MultiTenancySides.Tenant);

            var holidays = pages.CreateChildPermission(AppPermissions.Pages_Holidays, L("Holidays"), multiTenancySides: MultiTenancySides.Tenant);
            holidays.CreateChildPermission(AppPermissions.Pages_Holidays_Create, L("CreateNewHolidays"), multiTenancySides: MultiTenancySides.Tenant);
            holidays.CreateChildPermission(AppPermissions.Pages_Holidays_Edit, L("EditHolidays"), multiTenancySides: MultiTenancySides.Tenant);
            holidays.CreateChildPermission(AppPermissions.Pages_Holidays_Delete, L("DeleteHolidays"), multiTenancySides: MultiTenancySides.Tenant);

            var holidayTypes = pages.CreateChildPermission(AppPermissions.Pages_HolidayTypes, L("HolidayTypes"), multiTenancySides: MultiTenancySides.Tenant);
            holidayTypes.CreateChildPermission(AppPermissions.Pages_HolidayTypes_Create, L("CreateNewHolidayTypes"), multiTenancySides: MultiTenancySides.Tenant);
            holidayTypes.CreateChildPermission(AppPermissions.Pages_HolidayTypes_Edit, L("EditHolidayTypes"), multiTenancySides: MultiTenancySides.Tenant);
            holidayTypes.CreateChildPermission(AppPermissions.Pages_HolidayTypes_Delete, L("DeleteHolidayTypes"), multiTenancySides: MultiTenancySides.Tenant);

            var leaveTypes = pages.CreateChildPermission(AppPermissions.Pages_LeaveTypes, L("LeaveTypes"), multiTenancySides: MultiTenancySides.Tenant);
            leaveTypes.CreateChildPermission(AppPermissions.Pages_LeaveTypes_Create, L("CreateNewLeaveTypes"), multiTenancySides: MultiTenancySides.Tenant);
            leaveTypes.CreateChildPermission(AppPermissions.Pages_LeaveTypes_Edit, L("EditLeaveTypes"), multiTenancySides: MultiTenancySides.Tenant);
            leaveTypes.CreateChildPermission(AppPermissions.Pages_LeaveTypes_Delete, L("DeleteLeaveTypes"), multiTenancySides: MultiTenancySides.Tenant);

            var approverOrders = pages.CreateChildPermission(AppPermissions.Pages_ApproverOrders, L("ApproverOrders"), multiTenancySides: MultiTenancySides.Tenant);
            approverOrders.CreateChildPermission(AppPermissions.Pages_ApproverOrders_Create, L("CreateNewApproverOrders"), multiTenancySides: MultiTenancySides.Tenant);
            approverOrders.CreateChildPermission(AppPermissions.Pages_ApproverOrders_Edit, L("EditApproverOrders"), multiTenancySides: MultiTenancySides.Tenant);
            approverOrders.CreateChildPermission(AppPermissions.Pages_ApproverOrders_Delete, L("DeleteApproverOrders"), multiTenancySides: MultiTenancySides.Tenant);

            var attendanceApprovals = pages.CreateChildPermission(AppPermissions.Pages_AttendanceApprovals, L("AttendanceApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            attendanceApprovals.CreateChildPermission(AppPermissions.Pages_AttendanceApprovals_Create, L("CreateNewAttendanceApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            attendanceApprovals.CreateChildPermission(AppPermissions.Pages_AttendanceApprovals_Edit, L("EditAttendanceApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            attendanceApprovals.CreateChildPermission(AppPermissions.Pages_AttendanceApprovals_Admin, L("AttendanceAdminApprovals"), multiTenancySides: MultiTenancySides.Tenant);

            var attendancePeriods = pages.CreateChildPermission(AppPermissions.Pages_AttendancePeriods, L("AttendancePeriods"), multiTenancySides: MultiTenancySides.Tenant);
            attendancePeriods.CreateChildPermission(AppPermissions.Pages_AttendancePeriods_Create, L("CreateNewAttendancePeriods"), multiTenancySides: MultiTenancySides.Tenant);
            attendancePeriods.CreateChildPermission(AppPermissions.Pages_AttendancePeriods_Edit, L("EditAttendancePeriods"), multiTenancySides: MultiTenancySides.Tenant);
            attendancePeriods.CreateChildPermission(AppPermissions.Pages_AttendancePeriods_Delete, L("DeleteAttendancePeriods"), multiTenancySides: MultiTenancySides.Tenant);

            var attendanceReasons = pages.CreateChildPermission(AppPermissions.Pages_AttendanceReasons, L("AttendanceReasons"), multiTenancySides: MultiTenancySides.Tenant);
            attendanceReasons.CreateChildPermission(AppPermissions.Pages_AttendanceReasons_Create, L("CreateNewAttendanceReasons"), multiTenancySides: MultiTenancySides.Tenant);
            attendanceReasons.CreateChildPermission(AppPermissions.Pages_AttendanceReasons_Edit, L("EditAttendanceReasons"), multiTenancySides: MultiTenancySides.Tenant);
            attendanceReasons.CreateChildPermission(AppPermissions.Pages_AttendanceReasons_Delete, L("DeleteAttendanceReasons"), multiTenancySides: MultiTenancySides.Tenant);

            var attendanceRequests = pages.CreateChildPermission(AppPermissions.Pages_AttendanceRequests, L("AttendanceRequests"), multiTenancySides: MultiTenancySides.Tenant);
            attendanceRequests.CreateChildPermission(AppPermissions.Pages_AttendanceRequests_Create, L("CreateNewAttendanceRequests"), multiTenancySides: MultiTenancySides.Tenant);
            attendanceRequests.CreateChildPermission(AppPermissions.Pages_AttendanceRequests_Edit, L("EditAttendanceRequests"), multiTenancySides: MultiTenancySides.Tenant);

            pages.CreateChildPermission(AppPermissions.Pages_AdminAttendances, L("AdminAttendances"), multiTenancySides: MultiTenancySides.Tenant);

            var attendances = pages.CreateChildPermission(AppPermissions.Pages_Attendances, L("Attendances"), multiTenancySides: MultiTenancySides.Tenant);
            attendances.CreateChildPermission(AppPermissions.Pages_Attendances_Create, L("CreateNewAttendances"), multiTenancySides: MultiTenancySides.Tenant);
            attendances.CreateChildPermission(AppPermissions.Pages_Attendances_Edit, L("EditAttendances"), multiTenancySides: MultiTenancySides.Tenant);
            attendances.CreateChildPermission(AppPermissions.Pages_Attendances_Delete, L("DeleteAttendances"), multiTenancySides: MultiTenancySides.Tenant);
            attendances.CreateChildPermission(AppPermissions.Pages_Attendances_Upload, L("UploadAttendances"), multiTenancySides: MultiTenancySides.Tenant);
            attendances.CreateChildPermission(AppPermissions.Pages_Attendances_Process, L("ProcessAttendances"), multiTenancySides: MultiTenancySides.Tenant);
            attendances.CreateChildPermission(AppPermissions.Pages_Attendances_Post, L("PostAttendances"), multiTenancySides: MultiTenancySides.Tenant);
            attendances.CreateChildPermission(AppPermissions.Pages_Attendances_Summarize, L("SummarizeAttendances"), multiTenancySides: MultiTenancySides.Tenant);

            var employeeApprovers = pages.CreateChildPermission(AppPermissions.Pages_EmployeeApprovers, L("EmployeeApprovers"), multiTenancySides: MultiTenancySides.Tenant);
            employeeApprovers.CreateChildPermission(AppPermissions.Pages_EmployeeApprovers_Create, L("CreateNewEmployeeApprovers"), multiTenancySides: MultiTenancySides.Tenant);
            employeeApprovers.CreateChildPermission(AppPermissions.Pages_EmployeeApprovers_Edit, L("EditEmployeeApprovers"), multiTenancySides: MultiTenancySides.Tenant);
            employeeApprovers.CreateChildPermission(AppPermissions.Pages_EmployeeApprovers_Delete, L("DeleteEmployeeApprovers"), multiTenancySides: MultiTenancySides.Tenant);

            var employeeTimeInfo = pages.CreateChildPermission(AppPermissions.Pages_EmployeeTimeInfo, L("EmployeeTimeInfo"), multiTenancySides: MultiTenancySides.Tenant);
            employeeTimeInfo.CreateChildPermission(AppPermissions.Pages_EmployeeTimeInfo_Create, L("CreateNewEmployeeTimeInfo"), multiTenancySides: MultiTenancySides.Tenant);
            employeeTimeInfo.CreateChildPermission(AppPermissions.Pages_EmployeeTimeInfo_Edit, L("EditEmployeeTimeInfo"), multiTenancySides: MultiTenancySides.Tenant);
            employeeTimeInfo.CreateChildPermission(AppPermissions.Pages_EmployeeTimeInfo_Delete, L("DeleteEmployeeTimeInfo"), multiTenancySides: MultiTenancySides.Tenant);

            var excuseApprovals = pages.CreateChildPermission(AppPermissions.Pages_ExcuseApprovals, L("ExcuseApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            excuseApprovals.CreateChildPermission(AppPermissions.Pages_ExcuseApprovals_Create, L("CreateNewExcuseApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            excuseApprovals.CreateChildPermission(AppPermissions.Pages_ExcuseApprovals_Edit, L("EditExcuseApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            excuseApprovals.CreateChildPermission(AppPermissions.Pages_ExcuseApprovals_Admin, L("ExcuseAdminApprovals"), multiTenancySides: MultiTenancySides.Tenant);

            var excuseReasons = pages.CreateChildPermission(AppPermissions.Pages_ExcuseReasons, L("ExcuseReasons"), multiTenancySides: MultiTenancySides.Tenant);
            excuseReasons.CreateChildPermission(AppPermissions.Pages_ExcuseReasons_Create, L("CreateNewExcuseReasons"), multiTenancySides: MultiTenancySides.Tenant);
            excuseReasons.CreateChildPermission(AppPermissions.Pages_ExcuseReasons_Edit, L("EditExcuseReasons"), multiTenancySides: MultiTenancySides.Tenant);
            excuseReasons.CreateChildPermission(AppPermissions.Pages_ExcuseReasons_Delete, L("DeleteExcuseReasons"), multiTenancySides: MultiTenancySides.Tenant);

            var excuseRequests = pages.CreateChildPermission(AppPermissions.Pages_ExcuseRequests, L("ExcuseRequests"), multiTenancySides: MultiTenancySides.Tenant);
            excuseRequests.CreateChildPermission(AppPermissions.Pages_ExcuseRequests_Create, L("CreateNewExcuseRequests"), multiTenancySides: MultiTenancySides.Tenant);
            excuseRequests.CreateChildPermission(AppPermissions.Pages_ExcuseRequests_Edit, L("EditExcuseRequests"), multiTenancySides: MultiTenancySides.Tenant);

            var frequencies = pages.CreateChildPermission(AppPermissions.Pages_Frequencies, L("Frequencies"), multiTenancySides: MultiTenancySides.Tenant);
            frequencies.CreateChildPermission(AppPermissions.Pages_Frequencies_Create, L("CreateNewFrequencies"), multiTenancySides: MultiTenancySides.Tenant);
            frequencies.CreateChildPermission(AppPermissions.Pages_Frequencies_Edit, L("EditFrequencies"), multiTenancySides: MultiTenancySides.Tenant);
            frequencies.CreateChildPermission(AppPermissions.Pages_Frequencies_Delete, L("DeleteFrequencies"), multiTenancySides: MultiTenancySides.Tenant);

            var leaveApprovals = pages.CreateChildPermission(AppPermissions.Pages_LeaveApprovals, L("LeaveApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            leaveApprovals.CreateChildPermission(AppPermissions.Pages_LeaveApprovals_Create, L("CreateNewLeaveApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            leaveApprovals.CreateChildPermission(AppPermissions.Pages_LeaveApprovals_Edit, L("EditLeaveApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            leaveApprovals.CreateChildPermission(AppPermissions.Pages_LeaveApprovals_Admin, L("LeaveAdminApprovals"), multiTenancySides: MultiTenancySides.Tenant);

            var leaveEarnings = pages.CreateChildPermission(AppPermissions.Pages_LeaveEarnings, L("LeaveEarnings"), multiTenancySides: MultiTenancySides.Tenant);
            leaveEarnings.CreateChildPermission(AppPermissions.Pages_LeaveEarnings_Create, L("CreateNewLeaveEarnings"), multiTenancySides: MultiTenancySides.Tenant);
            leaveEarnings.CreateChildPermission(AppPermissions.Pages_LeaveEarnings_Edit, L("EditLeaveEarnings"), multiTenancySides: MultiTenancySides.Tenant);
            leaveEarnings.CreateChildPermission(AppPermissions.Pages_LeaveEarnings_Delete, L("DeleteLeaveEarnings"), multiTenancySides: MultiTenancySides.Tenant);

            var leaveReasons = pages.CreateChildPermission(AppPermissions.Pages_LeaveReasons, L("LeaveReasons"), multiTenancySides: MultiTenancySides.Tenant);
            leaveReasons.CreateChildPermission(AppPermissions.Pages_LeaveReasons_Create, L("CreateNewLeaveReasons"), multiTenancySides: MultiTenancySides.Tenant);
            leaveReasons.CreateChildPermission(AppPermissions.Pages_LeaveReasons_Edit, L("EditLeaveReasons"), multiTenancySides: MultiTenancySides.Tenant);
            leaveReasons.CreateChildPermission(AppPermissions.Pages_LeaveReasons_Delete, L("DeleteLeaveReasons"), multiTenancySides: MultiTenancySides.Tenant);

            var leaveRequestDetails = pages.CreateChildPermission(AppPermissions.Pages_LeaveRequestDetails, L("LeaveRequestDetails"), multiTenancySides: MultiTenancySides.Tenant);
            leaveRequestDetails.CreateChildPermission(AppPermissions.Pages_LeaveRequestDetails_Create, L("CreateNewLeaveRequestDetails"), multiTenancySides: MultiTenancySides.Tenant);
            leaveRequestDetails.CreateChildPermission(AppPermissions.Pages_LeaveRequestDetails_Edit, L("EditLeaveRequestDetails"), multiTenancySides: MultiTenancySides.Tenant);
            leaveRequestDetails.CreateChildPermission(AppPermissions.Pages_LeaveRequestDetails_Delete, L("DeleteLeaveRequestDetails"), multiTenancySides: MultiTenancySides.Tenant);

            var leaveRequests = pages.CreateChildPermission(AppPermissions.Pages_LeaveRequests, L("LeaveRequests"), multiTenancySides: MultiTenancySides.Tenant);
            leaveRequests.CreateChildPermission(AppPermissions.Pages_LeaveRequests_Create, L("CreateNewLeaveRequests"), multiTenancySides: MultiTenancySides.Tenant);
            leaveRequests.CreateChildPermission(AppPermissions.Pages_LeaveRequests_Edit, L("EditLeaveRequests"), multiTenancySides: MultiTenancySides.Tenant);

            var officialBusinessApprovals = pages.CreateChildPermission(AppPermissions.Pages_OfficialBusinessApprovals, L("OfficialBusinessApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            officialBusinessApprovals.CreateChildPermission(AppPermissions.Pages_OfficialBusinessApprovals_Create, L("CreateNewOfficialBusinessApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            officialBusinessApprovals.CreateChildPermission(AppPermissions.Pages_OfficialBusinessApprovals_Edit, L("EditOfficialBusinessApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            officialBusinessApprovals.CreateChildPermission(AppPermissions.Pages_OfficialBusinessApprovals_Admin, L("OfficialBusinessAdminApprovals"), multiTenancySides: MultiTenancySides.Tenant);

            var officialBusinessReasons = pages.CreateChildPermission(AppPermissions.Pages_OfficialBusinessReasons, L("OfficialBusinessReasons"), multiTenancySides: MultiTenancySides.Tenant);
            officialBusinessReasons.CreateChildPermission(AppPermissions.Pages_OfficialBusinessReasons_Create, L("CreateNewOfficialBusinessReasons"), multiTenancySides: MultiTenancySides.Tenant);
            officialBusinessReasons.CreateChildPermission(AppPermissions.Pages_OfficialBusinessReasons_Edit, L("EditOfficialBusinessReasons"), multiTenancySides: MultiTenancySides.Tenant);
            officialBusinessReasons.CreateChildPermission(AppPermissions.Pages_OfficialBusinessReasons_Delete, L("DeleteOfficialBusinessReasons"), multiTenancySides: MultiTenancySides.Tenant);

            var officialBusinessRequestDetails = pages.CreateChildPermission(AppPermissions.Pages_OfficialBusinessRequestDetails, L("OfficialBusinessRequestDetails"), multiTenancySides: MultiTenancySides.Tenant);
            officialBusinessRequestDetails.CreateChildPermission(AppPermissions.Pages_OfficialBusinessRequestDetails_Create, L("CreateNewOfficialBusinessRequestDetails"), multiTenancySides: MultiTenancySides.Tenant);
            officialBusinessRequestDetails.CreateChildPermission(AppPermissions.Pages_OfficialBusinessRequestDetails_Edit, L("EditOfficialBusinessRequestDetails"), multiTenancySides: MultiTenancySides.Tenant);
            officialBusinessRequestDetails.CreateChildPermission(AppPermissions.Pages_OfficialBusinessRequestDetails_Delete, L("DeleteOfficialBusinessRequestDetails"), multiTenancySides: MultiTenancySides.Tenant);

            var officialBusinessRequests = pages.CreateChildPermission(AppPermissions.Pages_OfficialBusinessRequests, L("OfficialBusinessRequests"), multiTenancySides: MultiTenancySides.Tenant);
            officialBusinessRequests.CreateChildPermission(AppPermissions.Pages_OfficialBusinessRequests_Create, L("CreateNewOfficialBusinessRequests"), multiTenancySides: MultiTenancySides.Tenant);
            officialBusinessRequests.CreateChildPermission(AppPermissions.Pages_OfficialBusinessRequests_Edit, L("EditOfficialBusinessRequests"), multiTenancySides: MultiTenancySides.Tenant);

            var overtimeApprovals = pages.CreateChildPermission(AppPermissions.Pages_OvertimeApprovals, L("OvertimeApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            overtimeApprovals.CreateChildPermission(AppPermissions.Pages_OvertimeApprovals_Create, L("CreateNewOvertimeApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            overtimeApprovals.CreateChildPermission(AppPermissions.Pages_OvertimeApprovals_Edit, L("EditOvertimeApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            overtimeApprovals.CreateChildPermission(AppPermissions.Pages_OvertimeApprovals_Admin, L("OvertimeAdminApprovals"), multiTenancySides: MultiTenancySides.Tenant);

            var overtimeReasons = pages.CreateChildPermission(AppPermissions.Pages_OvertimeReasons, L("OvertimeReasons"), multiTenancySides: MultiTenancySides.Tenant);
            overtimeReasons.CreateChildPermission(AppPermissions.Pages_OvertimeReasons_Create, L("CreateNewOvertimeReasons"), multiTenancySides: MultiTenancySides.Tenant);
            overtimeReasons.CreateChildPermission(AppPermissions.Pages_OvertimeReasons_Edit, L("EditOvertimeReasons"), multiTenancySides: MultiTenancySides.Tenant);
            overtimeReasons.CreateChildPermission(AppPermissions.Pages_OvertimeReasons_Delete, L("DeleteOvertimeReasons"), multiTenancySides: MultiTenancySides.Tenant);

            var overtimeRequests = pages.CreateChildPermission(AppPermissions.Pages_OvertimeRequests, L("OvertimeRequests"), multiTenancySides: MultiTenancySides.Tenant);
            overtimeRequests.CreateChildPermission(AppPermissions.Pages_OvertimeRequests_Create, L("CreateNewOvertimeRequests"), multiTenancySides: MultiTenancySides.Tenant);
            overtimeRequests.CreateChildPermission(AppPermissions.Pages_OvertimeRequests_Edit, L("EditOvertimeRequests"), multiTenancySides: MultiTenancySides.Tenant);

            var rawTimeLogs = pages.CreateChildPermission(AppPermissions.Pages_RawTimeLogs, L("RawTimeLogs"), multiTenancySides: MultiTenancySides.Tenant);
            rawTimeLogs.CreateChildPermission(AppPermissions.Pages_RawTimeLogs_Create, L("CreateNewRawTimeLogs"), multiTenancySides: MultiTenancySides.Tenant);
            rawTimeLogs.CreateChildPermission(AppPermissions.Pages_RawTimeLogs_Edit, L("EditRawTimeLogs"), multiTenancySides: MultiTenancySides.Tenant);
            rawTimeLogs.CreateChildPermission(AppPermissions.Pages_RawTimeLogs_Delete, L("DeleteRawTimeLogs"), multiTenancySides: MultiTenancySides.Tenant);

            var restDayApprovals = pages.CreateChildPermission(AppPermissions.Pages_RestDayApprovals, L("RestDayApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            restDayApprovals.CreateChildPermission(AppPermissions.Pages_RestDayApprovals_Create, L("CreateNewRestDayApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            restDayApprovals.CreateChildPermission(AppPermissions.Pages_RestDayApprovals_Edit, L("EditRestDayApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            restDayApprovals.CreateChildPermission(AppPermissions.Pages_RestDayApprovals_Admin, L("RestDayAdminApprovals"), multiTenancySides: MultiTenancySides.Tenant);

            var restDayReasons = pages.CreateChildPermission(AppPermissions.Pages_RestDayReasons, L("RestDayReasons"), multiTenancySides: MultiTenancySides.Tenant);
            restDayReasons.CreateChildPermission(AppPermissions.Pages_RestDayReasons_Create, L("CreateNewRestDayReasons"), multiTenancySides: MultiTenancySides.Tenant);
            restDayReasons.CreateChildPermission(AppPermissions.Pages_RestDayReasons_Edit, L("EditRestDayReasons"), multiTenancySides: MultiTenancySides.Tenant);
            restDayReasons.CreateChildPermission(AppPermissions.Pages_RestDayReasons_Delete, L("DeleteRestDayReasons"), multiTenancySides: MultiTenancySides.Tenant);

            var restDayRequests = pages.CreateChildPermission(AppPermissions.Pages_RestDayRequests, L("RestDayRequests"), multiTenancySides: MultiTenancySides.Tenant);
            restDayRequests.CreateChildPermission(AppPermissions.Pages_RestDayRequests_Create, L("CreateNewRestDayRequests"), multiTenancySides: MultiTenancySides.Tenant);
            restDayRequests.CreateChildPermission(AppPermissions.Pages_RestDayRequests_Edit, L("EditRestDayRequests"), multiTenancySides: MultiTenancySides.Tenant);

            var shiftApprovals = pages.CreateChildPermission(AppPermissions.Pages_ShiftApprovals, L("ShiftApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            shiftApprovals.CreateChildPermission(AppPermissions.Pages_ShiftApprovals_Create, L("CreateNewShiftApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            shiftApprovals.CreateChildPermission(AppPermissions.Pages_ShiftApprovals_Edit, L("EditShiftApprovals"), multiTenancySides: MultiTenancySides.Tenant);
            shiftApprovals.CreateChildPermission(AppPermissions.Pages_ShiftApprovals_Admin, L("ShiftAdminApprovals"), multiTenancySides: MultiTenancySides.Tenant);

            var shiftReasons = pages.CreateChildPermission(AppPermissions.Pages_ShiftReasons, L("ShiftReasons"), multiTenancySides: MultiTenancySides.Tenant);
            shiftReasons.CreateChildPermission(AppPermissions.Pages_ShiftReasons_Create, L("CreateNewShiftReasons"), multiTenancySides: MultiTenancySides.Tenant);
            shiftReasons.CreateChildPermission(AppPermissions.Pages_ShiftReasons_Edit, L("EditShiftReasons"), multiTenancySides: MultiTenancySides.Tenant);
            shiftReasons.CreateChildPermission(AppPermissions.Pages_ShiftReasons_Delete, L("DeleteShiftReasons"), multiTenancySides: MultiTenancySides.Tenant);

            var shiftRequests = pages.CreateChildPermission(AppPermissions.Pages_ShiftRequests, L("ShiftRequests"), multiTenancySides: MultiTenancySides.Tenant);
            shiftRequests.CreateChildPermission(AppPermissions.Pages_ShiftRequests_Create, L("CreateNewShiftRequests"), multiTenancySides: MultiTenancySides.Tenant);
            shiftRequests.CreateChildPermission(AppPermissions.Pages_ShiftRequests_Edit, L("EditShiftRequests"), multiTenancySides: MultiTenancySides.Tenant);

            var shiftTypeDetails = pages.CreateChildPermission(AppPermissions.Pages_ShiftTypeDetails, L("ShiftTypeDetails"), multiTenancySides: MultiTenancySides.Tenant);
            shiftTypeDetails.CreateChildPermission(AppPermissions.Pages_ShiftTypeDetails_Create, L("CreateNewShiftTypeDetails"), multiTenancySides: MultiTenancySides.Tenant);
            shiftTypeDetails.CreateChildPermission(AppPermissions.Pages_ShiftTypeDetails_Edit, L("EditShiftTypeDetails"), multiTenancySides: MultiTenancySides.Tenant);
            shiftTypeDetails.CreateChildPermission(AppPermissions.Pages_ShiftTypeDetails_Delete, L("DeleteShiftTypeDetails"), multiTenancySides: MultiTenancySides.Tenant);

            var shiftTypes = pages.CreateChildPermission(AppPermissions.Pages_ShiftTypes, L("ShiftTypes"), multiTenancySides: MultiTenancySides.Tenant);
            shiftTypes.CreateChildPermission(AppPermissions.Pages_ShiftTypes_Create, L("CreateNewShiftTypes"), multiTenancySides: MultiTenancySides.Tenant);
            shiftTypes.CreateChildPermission(AppPermissions.Pages_ShiftTypes_Edit, L("EditShiftTypes"), multiTenancySides: MultiTenancySides.Tenant);
            shiftTypes.CreateChildPermission(AppPermissions.Pages_ShiftTypes_Delete, L("DeleteShiftTypes"), multiTenancySides: MultiTenancySides.Tenant);

            var overtimeTypes = pages.CreateChildPermission(AppPermissions.Pages_OvertimeTypes, L("OvertimeTypes"), multiTenancySides: MultiTenancySides.Tenant);
            overtimeTypes.CreateChildPermission(AppPermissions.Pages_OvertimeTypes_Create, L("CreateNewOvertimeTypes"), multiTenancySides: MultiTenancySides.Tenant);
            overtimeTypes.CreateChildPermission(AppPermissions.Pages_OvertimeTypes_Edit, L("EditOvertimeTypes"), multiTenancySides: MultiTenancySides.Tenant);
            overtimeTypes.CreateChildPermission(AppPermissions.Pages_OvertimeTypes_Delete, L("DeleteOvertimeTypes"), multiTenancySides: MultiTenancySides.Tenant);

            var status = pages.CreateChildPermission(AppPermissions.Pages_Status, L("Status"), multiTenancySides: MultiTenancySides.Tenant);
            status.CreateChildPermission(AppPermissions.Pages_Status_Create, L("CreateNewStatus"), multiTenancySides: MultiTenancySides.Tenant);
            status.CreateChildPermission(AppPermissions.Pages_Status_Edit, L("EditStatus"), multiTenancySides: MultiTenancySides.Tenant);
            status.CreateChildPermission(AppPermissions.Pages_Status_Delete, L("DeleteStatus"), multiTenancySides: MultiTenancySides.Tenant);

            var workFlow = pages.CreateChildPermission(AppPermissions.Pages_WorkFlow, L("WorkFlow"));
            workFlow.CreateChildPermission(AppPermissions.Pages_WorkFlow_Create, L("CreateNewWorkFlow"));
            workFlow.CreateChildPermission(AppPermissions.Pages_WorkFlow_Edit, L("EditWorkFlow"));
            workFlow.CreateChildPermission(AppPermissions.Pages_WorkFlow_Delete, L("DeleteWorkFlow"));

            //Reports
            pages.CreateChildPermission(AppPermissions.Pages_Reports, L("Reports"));

            //Hangfire
            pages.CreateChildPermission(AppPermissions.Pages_Processes, L("Processes"));

            var employmentTypes = pages.CreateChildPermission(AppPermissions.Pages_EmploymentTypes, L("EmploymentTypes"));
            employmentTypes.CreateChildPermission(AppPermissions.Pages_EmploymentTypes_Create, L("CreateNewEmploymentTypes"));
            employmentTypes.CreateChildPermission(AppPermissions.Pages_EmploymentTypes_Edit, L("EditEmploymentTypes"));
            employmentTypes.CreateChildPermission(AppPermissions.Pages_EmploymentTypes_Delete, L("DeleteEmploymentTypes"));

            var groupSettings = pages.CreateChildPermission(AppPermissions.Pages_GroupSettings, L("GroupSettings"));
            groupSettings.CreateChildPermission(AppPermissions.Pages_GroupSettings_Create, L("CreateNewGroupSettings"));
            groupSettings.CreateChildPermission(AppPermissions.Pages_GroupSettings_Edit, L("EditGroupSettings"));
            groupSettings.CreateChildPermission(AppPermissions.Pages_GroupSettings_Delete, L("DeleteGroupSettings"));

            var process = pages.CreateChildPermission(AppPermissions.Pages_Process, L("Process"));
            process.CreateChildPermission(AppPermissions.Pages_Process_Create, L("CreateNewProcess"));
            process.CreateChildPermission(AppPermissions.Pages_Process_Edit, L("EditProcess"));
            process.CreateChildPermission(AppPermissions.Pages_Process_Delete, L("DeleteProcess"));

            var companies = pages.CreateChildPermission(AppPermissions.Pages_Companies, L("Companies"));
            companies.CreateChildPermission(AppPermissions.Pages_Companies_Create, L("CreateNewCompanies"));
            companies.CreateChildPermission(AppPermissions.Pages_Companies_Edit, L("EditCompanies"));
            companies.CreateChildPermission(AppPermissions.Pages_Companies_Delete, L("DeleteCompanies"));

            var employees = pages.CreateChildPermission(AppPermissions.Pages_Employees, L("Employees"));
            employees.CreateChildPermission(AppPermissions.Pages_Employees_Create, L("CreateNewEmployees"));
            employees.CreateChildPermission(AppPermissions.Pages_Employees_Edit, L("EditEmployees"));
            employees.CreateChildPermission(AppPermissions.Pages_Employees_Delete, L("DeleteEmployees"));

            var tkSettings = pages.CreateChildPermission(AppPermissions.Pages_TKSettings, L("TKSettings"));
            tkSettings.CreateChildPermission(AppPermissions.Pages_TKSettings_Create, L("CreateNewTKSettings"));
            tkSettings.CreateChildPermission(AppPermissions.Pages_TKSettings_Edit, L("EditTKSettings"));
            tkSettings.CreateChildPermission(AppPermissions.Pages_TKSettings_Delete, L("DeleteTKSettings"));

            pages.CreateChildPermission(AppPermissions.Pages_DemoUiComponents, L("DemoUiComponents"));

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Unlock, L("Unlock"));

            var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

            var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles, L("ManagingRoles"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization, L("VisualSettings"));

            var webhooks = administration.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription, L("Webhooks"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Create, L("CreatingWebhooks"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Edit, L("EditingWebhooks"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_ChangeActivity, L("ChangingWebhookActivity"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Detail, L("DetailingSubscription"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_Webhook_ListSendAttempts, L("ListingSendAttempts"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_Webhook_ResendWebhook, L("ResendingWebhook"));

            var dynamicProperties = administration.CreateChildPermission(AppPermissions.Pages_Administration_DynamicProperties, L("DynamicProperties"));
            dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicProperties_Create, L("CreatingDynamicProperties"));
            dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicProperties_Edit, L("EditingDynamicProperties"));
            dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicProperties_Delete, L("DeletingDynamicProperties"));

            var dynamicPropertyValues = dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicPropertyValue, L("DynamicPropertyValue"));
            dynamicPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicPropertyValue_Create, L("CreatingDynamicPropertyValue"));
            dynamicPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicPropertyValue_Edit, L("EditingDynamicPropertyValue"));
            dynamicPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicPropertyValue_Delete, L("DeletingDynamicPropertyValue"));

            var dynamicEntityProperties = dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityProperties, L("DynamicEntityProperties"));
            dynamicEntityProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityProperties_Create, L("CreatingDynamicEntityProperties"));
            dynamicEntityProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityProperties_Edit, L("EditingDynamicEntityProperties"));
            dynamicEntityProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityProperties_Delete, L("DeletingDynamicEntityProperties"));

            var dynamicEntityPropertyValues = dynamicProperties.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityPropertyValue, L("EntityDynamicPropertyValue"));
            dynamicEntityPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityPropertyValue_Create, L("CreatingDynamicEntityPropertyValue"));
            dynamicEntityPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityPropertyValue_Edit, L("EditingDynamicEntityPropertyValue"));
            dynamicEntityPropertyValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicEntityPropertyValue_Delete, L("DeletingDynamicEntityPropertyValue"));

            //TENANT-SPECIFIC PERMISSIONS

            pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement, L("Subscription"), multiTenancySides: MultiTenancySides.Tenant);

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_MoveTenantsToAnotherEdition, L("MoveTenantsToAnotherEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_EmailQueue, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Encryption, L("Encryption"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_SpeedTest, L("SpeedTest"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Host);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, ePayConsts.LocalizationSourceName);
        }
    }
}