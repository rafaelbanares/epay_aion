﻿namespace MMB.ePay.Authorization.Roles
{
    public static class StaticRoleNames
    {
        public static class Host
        {
            public const string Admin = "Admin";
        }

        public static class Tenants
        {
            public const string Admin = "Admin";
            public const string User = "User";
            public const string ClientAdmin = "Client Admin";
            public const string Employee = "Employee";
            public const string Approver = "Approver";
            public const string AutoUploader = "Auto-Uploader";
        }
    }
}