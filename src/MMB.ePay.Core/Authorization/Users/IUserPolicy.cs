﻿using System.Threading.Tasks;
using Abp.Domain.Policies;

namespace MMB.ePay.Authorization.Users
{
    public interface IUserPolicy : IPolicy
    {
        Task CheckMaxUserCountAsync(int tenantId);
    }
}
