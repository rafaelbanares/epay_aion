﻿using Abp.Domain.Entities;
using System;


namespace MMB.ePay
{
    public class ProcessBatch : Entity
    {
        public Guid BatchId { get; set; }
    }
}
