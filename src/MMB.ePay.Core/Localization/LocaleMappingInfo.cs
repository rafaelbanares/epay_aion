﻿namespace MMB.ePay.Localization
{
    public class LocaleMappingInfo
    {
        public string From { get; set; }

        public string To { get; set; }
    }
}