﻿using System.Globalization;

namespace MMB.ePay.Localization
{
    public interface IApplicationCulturesProvider
    {
        CultureInfo[] GetAllCultures();
    }
}