﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("PostedLeaves")]
    public class PostedLeaves : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual int EmployeeId { get; set; }

        [Required]
        public virtual DateTime Date { get; set; }

        [Required]
        public virtual int LeaveTypeId { get; set; }

        public virtual decimal Days { get; set; }

        public virtual Employees Employees { get; set; }

        public virtual LeaveTypes LeaveTypes { get; set; }
    }
}
