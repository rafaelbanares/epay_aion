﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("ShiftApprovals")]
    public class ShiftApprovals : CreationAuditedEntity, IMayHaveTenant
    {
        public int? TenantId { get; set; }

        [StringLength(ShiftApprovalsConsts.MaxRemarksLength, MinimumLength = ShiftApprovalsConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        public virtual int ShiftRequestId { get; set; }

        public virtual int StatusId { get; set; }

        public virtual ShiftRequests ShiftRequests { get; set; }
        public virtual Status Status { get; set; }
    }
}