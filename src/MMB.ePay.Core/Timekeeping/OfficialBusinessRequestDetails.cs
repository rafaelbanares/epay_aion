﻿using Abp.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("OfficialBusinessRequestDetails")]
    public class OfficialBusinessRequestDetails : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual DateTime OfficialBusinessDate { get; set; }

        public virtual int OfficialBusinessRequestId { get; set; }

        public int EmployeeId { get; set; }

        public virtual OfficialBusinessRequests OfficialBusinessRequests { get; set; }
    }
}