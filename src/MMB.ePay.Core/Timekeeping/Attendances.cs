﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("Attendances")]
    public class Attendances : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual int EmployeeId { get; set; }

        [Required]
        public virtual DateTime Date { get; set; }

        [Required]
        [StringLength(AttendancesConsts.MaxRestDayCodeLength, MinimumLength = AttendancesConsts.MinRestDayCodeLength)]
        public virtual string RestDayCode { get; set; }

        public virtual DateTime? TimeIn { get; set; }

        public virtual DateTime? BreaktimeIn { get; set; }

        public virtual DateTime? BreaktimeOut { get; set; }

        public virtual DateTime? PMBreaktimeIn { get; set; }

        public virtual DateTime? PMBreaktimeOut { get; set; }

        public virtual DateTime? TimeOut { get; set; }

        [StringLength(AttendancesConsts.MaxRemarksLength, MinimumLength = AttendancesConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        [Required]
        public virtual int ShiftTypeId { get; set; }

        public bool Invalid { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual ShiftTypes ShiftTypes { get; set; }
    }
}