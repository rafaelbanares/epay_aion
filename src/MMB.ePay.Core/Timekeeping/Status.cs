﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("Status")]
    public class Status : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(StatusConsts.MaxDisplayNameLength, MinimumLength = StatusConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [StringLength(StatusConsts.MaxAfterStatusMessageLength, MinimumLength = StatusConsts.MinAfterStatusMessageLength)]
        public virtual string AfterStatusMessage { get; set; }

        [StringLength(StatusConsts.MaxBeforeStatusActionTextLength, MinimumLength = StatusConsts.MinBeforeStatusActionTextLength)]
        public virtual string BeforeStatusActionText { get; set; }

        public virtual bool IsEditable { get; set; }

        public virtual int? AccessLevel { get; set; }

        public virtual ICollection<AttendanceApprovals> AttendanceApprovals { get; set; }
        public virtual ICollection<AttendanceRequests> AttendanceRequests { get; set; }
        public virtual ICollection<ExcuseApprovals> ExcuseApprovals { get; set; }
        public virtual ICollection<ExcuseRequests> ExcuseRequests { get; set; }
        public virtual ICollection<LeaveApprovals> LeaveApprovals { get; set; }
        public virtual ICollection<LeaveRequests> LeaveRequests { get; set; }
        public virtual ICollection<OfficialBusinessApprovals> OfficialBusinessApprovals { get; set; }
        public virtual ICollection<OfficialBusinessRequests> OfficialBusinessRequests { get; set; }
        public virtual ICollection<OvertimeApprovals> OvertimeApprovals { get; set; }
        public virtual ICollection<OvertimeRequests> OvertimeRequests { get; set; }
        public virtual ICollection<RestDayApprovals> RestDayApprovals { get; set; }
        public virtual ICollection<RestDayRequests> RestDayRequests { get; set; }
        public virtual ICollection<ShiftApprovals> ShiftApprovals { get; set; }
        public virtual ICollection<ShiftRequests> ShiftRequests { get; set; }
        public virtual ICollection<WorkFlow> WorkFlowNextStatusNavigation { get; set; }
        public virtual ICollection<WorkFlow> WorkFlowStatus { get; set; }


    }
}