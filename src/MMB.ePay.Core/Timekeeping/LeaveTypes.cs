﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("LeaveTypes")]
    public class LeaveTypes : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(LeaveTypesConsts.MaxDisplayNameLength, MinimumLength = LeaveTypesConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(LeaveTypesConsts.MaxDescriptionLength, MinimumLength = LeaveTypesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual bool WithPay { get; set; }

        public virtual int? AdvancedFilingInDays { get; set; }

        public virtual ICollection<PostedLeaves> PostedLeaves { get; set; }
        public virtual ICollection<ProcessedLeaves> ProcessedLeaves { get; set; }
        public virtual ICollection<LeaveEarningDetails> LeaveEarningDetails { get; set; }
        public virtual ICollection<LeaveRequests> LeaveRequests { get; set; }

    }
}