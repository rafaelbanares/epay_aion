﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("ShiftReasons")]
    public class ShiftReasons : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(ShiftReasonsConsts.MaxDisplayNameLength, MinimumLength = ShiftReasonsConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [StringLength(ShiftReasonsConsts.MaxDescriptionLength, MinimumLength = ShiftReasonsConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual ICollection<ShiftRequests> ShiftRequests { get; set; }
    }
}