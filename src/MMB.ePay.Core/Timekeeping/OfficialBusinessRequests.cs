﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MMB.ePay.Timekeeping
{
    [Table("OfficialBusinessRequests")]
    public class OfficialBusinessRequests : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(OfficialBusinessRequestsConsts.MaxRemarksLength, MinimumLength = OfficialBusinessRequestsConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        [StringLength(OfficialBusinessRequestsConsts.MaxRemarksLength, MinimumLength = OfficialBusinessRequestsConsts.MinRemarksLength)]
        public virtual string Reason { get; set; }

        public virtual int EmployeeId { get; set; }

        public virtual int OfficialBusinessReasonId { get; set; }

        public virtual int StatusId { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual OfficialBusinessReasons OfficialBusinessReasons { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<OfficialBusinessApprovals> OfficialBusinessApprovals { get; set; }
        public virtual ICollection<OfficialBusinessRequestDetails> OfficialBusinessRequestDetails { get; set; }
    }
}