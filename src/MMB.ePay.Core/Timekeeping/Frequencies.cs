﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("Frequencies")]
    public class Frequencies : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(FrequenciesConsts.MaxDisplayNameLength, MinimumLength = FrequenciesConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [StringLength(FrequenciesConsts.MaxDescriptionLength, MinimumLength = FrequenciesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual ICollection<AttendancePeriods> AttendancePeriods { get; set; }
        public virtual ICollection<EmployeeTimeInfo> EmployeeTimeInfo { get; set; }
    }
}