﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("Reports")]
    public class Reports : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(ReportsConsts.MaxDisplayNameLength, MinimumLength = ReportsConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(ReportsConsts.MaxControllerNameLength, MinimumLength = ReportsConsts.MinControllerNameLength)]
        public virtual string ControllerName { get; set; }

        [StringLength(ReportsConsts.MaxStoredProcedureLength, MinimumLength = ReportsConsts.MinStoredProcedureLength)]
        public virtual string StoredProcedure { get; set; }

        public virtual bool HasEmployeeFilter { get; set; }

        public virtual bool HasPeriodFilter { get; set; }

        public virtual bool IsLandscape { get; set; }

        public virtual bool Enabled { get; set; }

        [StringLength(ReportsConsts.MaxFormatLength, MinimumLength = ReportsConsts.MinFormatLength)]
        public virtual string Format { get; set; }

        [StringLength(ReportsConsts.MaxOutputLength, MinimumLength = ReportsConsts.MinOutputLength)]
        public virtual string Output { get; set; }

        [StringLength(ReportsConsts.MaxExportFileNameLength, MinimumLength = ReportsConsts.MinExportFileNameLength)]
        public virtual string ExportFileName { get; set; }
    }
}