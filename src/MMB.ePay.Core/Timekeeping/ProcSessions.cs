﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("ProcSessions")]
    public class ProcSessions : Entity<int>, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual Guid AppSessionId { get; set; }

        public virtual int AttendancePeriodId { get; set; }
        //public int? PayrollNo { get; set; }

        public DateTime ProcessStart { get; set; }

        public DateTime? ProcessEnd { get; set; }

        public virtual AttendancePeriods AttendancePeriods { get; set; }

        public virtual ICollection<ProcSessionBatches> ProcSessionBatches { get; set; }
    }
}
