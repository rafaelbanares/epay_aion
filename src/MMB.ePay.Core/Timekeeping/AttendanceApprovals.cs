﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("AttendanceApprovals")]
    public class AttendanceApprovals : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [StringLength(AttendanceApprovalsConsts.MaxRemarksLength, MinimumLength = AttendanceApprovalsConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        public virtual int AttendanceRequestId { get; set; }

        public virtual int StatusId { get; set; }

        public virtual AttendanceRequests AttendanceRequest { get; set; }
        public virtual Status Status { get; set; }

    }
}