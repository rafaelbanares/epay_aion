﻿using Abp.Domain.Entities;
using MMB.ePay.HR;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("TKSettings")]
    public class TKSettings : Entity, IMayHaveTenant
    {
        public int? TenantId { get; set; }

        [Required]
        [StringLength(TKSettingsConsts.MaxKeyLength, MinimumLength = TKSettingsConsts.MinKeyLength)]
        public virtual string Key { get; set; }

        [Required]
        [StringLength(TKSettingsConsts.MaxValueLength, MinimumLength = TKSettingsConsts.MinValueLength)]
        public virtual string Value { get; set; }

        [Required]
        [StringLength(TKSettingsConsts.MaxDataTypeLength, MinimumLength = TKSettingsConsts.MinDataTypeLength)]
        public virtual string DataType { get; set; }

        [Required]
        [StringLength(TKSettingsConsts.MaxCaptionLength, MinimumLength = TKSettingsConsts.MinCaptionLength)]
        public virtual string Caption { get; set; }

        public virtual int? DisplayOrder { get; set; }

        [Required]
        public virtual int GroupSettingId { get; set; }

        public virtual GroupSettings GroupSettings { get; set; }
    }
}