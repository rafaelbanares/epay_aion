﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("HolidayTypes")]
    public class HolidayTypes : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(HolidayTypesConsts.MaxDisplayNameLength, MinimumLength = HolidayTypesConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(HolidayTypesConsts.MaxDescriptionLength, MinimumLength = HolidayTypesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        [Required]
        [StringLength(HolidayTypesConsts.MaxHolidayCodeLength, MinimumLength = HolidayTypesConsts.MinHolidayCodeLength)]
        public virtual string HolidayCode { get; set; }

        public virtual bool HalfDayEnabled { get; set; }

        public virtual ICollection<Holidays> Holidays { get; set; }
    }
}