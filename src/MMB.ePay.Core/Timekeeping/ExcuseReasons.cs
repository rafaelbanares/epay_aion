﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("ExcuseReasons")]
    public class ExcuseReasons : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(ExcuseReasonsConsts.MaxDisplayNameLength, MinimumLength = ExcuseReasonsConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [StringLength(ExcuseReasonsConsts.MaxDescriptionLength, MinimumLength = ExcuseReasonsConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual ICollection<ExcuseRequests> ExcuseRequests { get; set; }
    }
}