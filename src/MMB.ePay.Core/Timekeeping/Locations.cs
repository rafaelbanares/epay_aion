﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("Locations")]
    public class Locations : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(LocationsConsts.MaxDescriptionLength, MinimumLength = LocationsConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual ICollection<EmployeeTimeInfo> EmployeeTimeInfo { get; set; }
        public virtual ICollection<Holidays> Holidays { get; set; }

    }
}