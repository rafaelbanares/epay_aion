﻿using Abp.Domain.Entities;
using MMB.ePay.HR;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("LeaveEarningDetails")]
    public class LeaveEarningDetails : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual decimal Earned { get; set; }

        public virtual int LeaveEarningId { get; set; }

        public virtual int EmployeeId { get; set; }

        public virtual int LeaveTypeId { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual LeaveEarnings LeaveEarnings { get; set; }
        public virtual LeaveTypes LeaveTypes { get; set; }
    }
}