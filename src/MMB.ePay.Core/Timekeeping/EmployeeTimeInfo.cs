﻿using Abp.Domain.Entities;
using MMB.ePay.HR;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("EmployeeTimeInfo")]
    public class EmployeeTimeInfo : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [StringLength(EmployeeTimeInfoConsts.MaxRestDayLength, MinimumLength = EmployeeTimeInfoConsts.MinRestDayLength)]
        public virtual string RestDay { get; set; }

        [Required]
        public virtual bool Overtime { get; set; }

        [Required]
        public virtual bool Undertime { get; set; }

        [Required]
        public virtual bool Tardy { get; set; }

        [Required]
        public virtual bool NonSwiper { get; set; }

        [Required]
        [StringLength(EmployeeTimeInfoConsts.MaxSwipeCodeLength, MinimumLength = EmployeeTimeInfoConsts.MinSwipeCodeLength)]
        public virtual string SwipeCode { get; set; }

        public virtual int EmployeeId { get; set; }

        public virtual int ShiftTypeId { get; set; }

        public virtual int FrequencyId { get; set; }

        public virtual int? LocationId { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual ShiftTypes ShiftTypes { get; set; }
        public virtual Frequencies Frequencies { get; set; }
        public virtual Locations Locations { get; set; }
    }
}