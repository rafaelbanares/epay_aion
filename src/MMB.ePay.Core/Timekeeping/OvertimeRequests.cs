﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("OvertimeRequests")]
    public class OvertimeRequests : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        /// <summary>
        /// Actual date of attendance
        /// </summary>
        [Required]
        public virtual DateTime OvertimeDate { get; set; } 

        [Required]
        public virtual DateTime OvertimeStart { get; set; }

        [Required]
        public virtual DateTime OvertimeEnd { get; set; }

        [Required]
        [StringLength(OvertimeRequestsConsts.MaxRemarksLength, MinimumLength = OvertimeRequestsConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        [StringLength(OvertimeRequestsConsts.MaxRemarksLength, MinimumLength = OvertimeRequestsConsts.MinRemarksLength)]
        public virtual string Reason { get; set; }

        public virtual int EmployeeId { get; set; }

        public virtual int OvertimeReasonId { get; set; }

        public virtual int StatusId { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual OvertimeReasons OvertimeReasons { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<OvertimeApprovals> OvertimeApprovals { get; set; }
    }
}