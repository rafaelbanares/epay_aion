﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("AttendancePeriods")]
    public class AttendancePeriods : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual DateTime StartDate { get; set; }

        [Required]
        public virtual DateTime EndDate { get; set; }

        [Required]
        public virtual short AppYear { get; set; }

        [Required]
        public virtual short AppMonth { get; set; }

        [Required]
        public virtual bool Posted { get; set; }

        public virtual int FrequencyId { get; set; }

        public virtual Frequencies Frequencies { get; set; }

        public virtual ICollection<ProcSessions> ProcSessions { get; set; }
    }
}