﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.Authorization.Users;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("LeaveApprovals")]
    public class LeaveApprovals : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [StringLength(LeaveApprovalsConsts.MaxRemarksLength, MinimumLength = LeaveApprovalsConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        public virtual int LeaveRequestId { get; set; }

        public virtual int StatusId { get; set; }

        public virtual LeaveRequests LeaveRequests { get; set; }
        public virtual Status Status { get; set; }
    }
}