﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("ShiftTypeDetails")]
    public class ShiftTypeDetails : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(ShiftTypeDetailsConsts.MaxDaysLength, MinimumLength = ShiftTypeDetailsConsts.MinDaysLength)]
        public virtual string Days { get; set; }

        [Required]
        [StringLength(ShiftTypeDetailsConsts.MaxTimeInLength, MinimumLength = ShiftTypeDetailsConsts.MinTimeInLength)]
        public virtual string TimeIn { get; set; }

        [StringLength(ShiftTypeDetailsConsts.MaxBreaktimeInLength, MinimumLength = ShiftTypeDetailsConsts.MinBreaktimeInLength)]
        public virtual string BreaktimeIn { get; set; }

        [StringLength(ShiftTypeDetailsConsts.MaxBreaktimeOutLength, MinimumLength = ShiftTypeDetailsConsts.MinBreaktimeOutLength)]
        public virtual string BreaktimeOut { get; set; }

        [Required]
        [StringLength(ShiftTypeDetailsConsts.MaxTimeOutLength, MinimumLength = ShiftTypeDetailsConsts.MinTimeOutLength)]
        public virtual string TimeOut { get; set; }

        public virtual int ShiftTypeId { get; set; }

        public virtual bool StartOnPreviousDay { get; set; }

        public virtual ShiftTypes ShiftTypes { get; set; }
    }
}