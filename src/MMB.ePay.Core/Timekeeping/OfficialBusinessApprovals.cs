﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.Authorization.Users;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("OfficialBusinessApprovals")]
    public class OfficialBusinessApprovals : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [StringLength(OfficialBusinessApprovalsConsts.MaxRemarksLength, MinimumLength = OfficialBusinessApprovalsConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        public virtual int OfficialBusinessRequestId { get; set; }

        public virtual int StatusId { get; set; }

        public virtual OfficialBusinessRequests OfficialBusinessRequests { get; set; }
        public virtual Status Status { get; set; }
    }
}