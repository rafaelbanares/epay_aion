﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("AttendanceRequests")]
    public class AttendanceRequests : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual DateTime Date { get; set; }

        public virtual DateTime? TimeIn { get; set; }

        public virtual DateTime? BreaktimeIn { get; set; }

        public virtual DateTime? BreaktimeOut { get; set; }

        public virtual DateTime? PMBreaktimeIn { get; set; }

        public virtual DateTime? PMBreaktimeOut { get; set; }

        public virtual DateTime? TimeOut { get; set; }

        [Required]
        [StringLength(AttendanceRequestsConsts.MaxRemarksLength, MinimumLength = AttendanceRequestsConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        [StringLength(AttendanceRequestsConsts.MaxRemarksLength, MinimumLength = AttendanceRequestsConsts.MinRemarksLength)]
        public virtual string Reason { get; set; }

        public virtual int EmployeeId { get; set; }

        public virtual int AttendanceReasonId { get; set; }

        public bool EarlyLogin { get; set; }

        public virtual int StatusId { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual AttendanceReasons AttendanceReason { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<AttendanceApprovals> AttendanceApprovals { get; set; }

    }
}