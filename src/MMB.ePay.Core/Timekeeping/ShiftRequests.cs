﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("ShiftRequests")]
    public class ShiftRequests : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual DateTime ShiftStart { get; set; }

        [Required]
        public virtual DateTime ShiftEnd { get; set; }

        [Required]
        [StringLength(ShiftRequestsConsts.MaxRemarksLength, MinimumLength = ShiftRequestsConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        [StringLength(ShiftRequestsConsts.MaxRemarksLength, MinimumLength = ShiftRequestsConsts.MinRemarksLength)]
        public virtual string Reason { get; set; }

        public virtual int EmployeeId { get; set; }

        public virtual int ShiftTypeId { get; set; }

        public virtual int ShiftReasonId { get; set; }

        public virtual int StatusId { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual ShiftReasons ShiftReasons { get; set; }
        public virtual ShiftTypes ShiftTypes { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<ShiftApprovals> ShiftApprovals { get; set; }
    }
}