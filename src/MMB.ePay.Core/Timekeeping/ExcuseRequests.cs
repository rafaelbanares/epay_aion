﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("ExcuseRequests")]
    public class ExcuseRequests : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(ExcuseRequestsConsts.MaxRemarksLength, MinimumLength = ExcuseRequestsConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        [StringLength(ExcuseRequestsConsts.MaxRemarksLength, MinimumLength = ExcuseRequestsConsts.MinRemarksLength)]
        public virtual string Reason { get; set; }

        [Required]
        public virtual DateTime ExcuseDate { get; set; }

        [Required]
        public virtual DateTime ExcuseEnd { get; set; }

        [Required]
        public virtual DateTime ExcuseStart { get; set; }

        public virtual int EmployeeId { get; set; }

        public virtual int ExcuseReasonId { get; set; }

        public virtual int StatusId { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual ExcuseReasons ExcuseReasons { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<ExcuseApprovals> ExcuseApprovals { get; set; }
    }
}