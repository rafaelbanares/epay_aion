﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("PostedAttendances")]
    public class PostedAttendances : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual DateTime Date { get; set; }

        [Required]
        [StringLength(PostedAttendancesConsts.MaxRestDayCodeLength, MinimumLength = PostedAttendancesConsts.MinRestDayCodeLength)]
        public virtual string RestDayCode { get; set; }

        public virtual DateTime? TimeIn { get; set; }

        public virtual DateTime? BreaktimeIn { get; set; }

        public virtual DateTime? BreaktimeOut { get; set; }

        public virtual DateTime? PMBreaktimeIn { get; set; }

        public virtual DateTime? PMBreaktimeOut { get; set; }

        public virtual DateTime? TimeOut { get; set; }

        [StringLength(PostedAttendancesConsts.MaxRemarksLength, MinimumLength = PostedAttendancesConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        public virtual int EmployeeId { get; set; }

        public virtual int ShiftTypeId { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual ShiftTypes ShiftTypes { get; set; }
    }
}