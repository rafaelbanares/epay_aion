﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("WorkFlow")]
    public class WorkFlow : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual int? NextStatus { get; set; }

        public virtual int StatusId { get; set; }

        public virtual Status NextStatusNavigation { get; set; }

        public virtual Status Status { get; set; }
    }
}