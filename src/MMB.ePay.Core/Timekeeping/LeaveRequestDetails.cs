﻿using Abp.Domain.Entities;
using MMB.ePay.HR;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("LeaveRequestDetails")]
    public class LeaveRequestDetails : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual DateTime LeaveDate { get; set; }

        [Required]
        public virtual decimal Days { get; set; }

        [Required]
        public virtual bool FirstHalf { get; set; }

        public virtual int LeaveRequestId { get; set; }

        public virtual int EmployeeId { get; set; }

        public virtual LeaveRequests LeaveRequests { get; set; }
        public virtual Employees Employees { get; set; }
    }
}