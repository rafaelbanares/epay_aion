﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("LeaveReasons")]
    public class LeaveReasons : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(LeaveReasonsConsts.MaxDisplayNameLength, MinimumLength = LeaveReasonsConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [StringLength(LeaveReasonsConsts.MaxDescriptionLength, MinimumLength = LeaveReasonsConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual ICollection<LeaveRequests> LeaveRequests { get; set; }
    }
}