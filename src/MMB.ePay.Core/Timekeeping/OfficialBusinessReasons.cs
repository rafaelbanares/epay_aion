﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("OfficialBusinessReasons")]
    public class OfficialBusinessReasons : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(OfficialBusinessReasonsConsts.MaxDisplayNameLength, MinimumLength = OfficialBusinessReasonsConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [StringLength(OfficialBusinessReasonsConsts.MaxDescriptionLength, MinimumLength = OfficialBusinessReasonsConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual ICollection<OfficialBusinessRequests> OfficialBusinessRequests { get; set; }
    }
}