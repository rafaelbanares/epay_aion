﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("AttendanceUploads")]
    public class AttendanceUploads : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual int EmployeeId { get; set; }

        public virtual DateTime Date { get; set; }

        [StringLength(AttendanceUploadsConsts.MaxTimeInLength, MinimumLength = AttendanceUploadsConsts.MinTimeInLength)]
        public virtual string TimeIn { get; set; }

        [StringLength(AttendanceUploadsConsts.MaxLunchBreaktimeInLength, MinimumLength = AttendanceUploadsConsts.MinLunchBreaktimeInLength)]
        public virtual string LunchBreaktimeIn { get; set; }
        
        [StringLength(AttendanceUploadsConsts.MaxLunchBreaktimeOutLength, MinimumLength = AttendanceUploadsConsts.MinLunchBreaktimeOutLength)]
        public virtual string LunchBreaktimeOut { get; set; }
        
        [StringLength(AttendanceUploadsConsts.MaxPMBreaktimeInLength, MinimumLength = AttendanceUploadsConsts.MinPMBreaktimeInLength)]
        public virtual string PMBreaktimeIn { get; set; }
        
        [StringLength(AttendanceUploadsConsts.MaxPMBreaktimeOutLength, MinimumLength = AttendanceUploadsConsts.MinPMBreaktimeOutLength)]
        public virtual string PMBreaktimeOut { get; set; }

        [StringLength(AttendanceUploadsConsts.MaxTimeOutLength, MinimumLength = AttendanceUploadsConsts.MinTimeOutLength)]
        public virtual string TimeOut { get; set; }

        public virtual Employees Employees { get; set; }
    }
}