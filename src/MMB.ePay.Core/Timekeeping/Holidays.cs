﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("Holidays")]
    public class Holidays : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(HolidaysConsts.MaxDisplayNameLength, MinimumLength = HolidaysConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        public virtual DateTime Date { get; set; }

        [Required]
        public virtual bool HalfDay { get; set; }

        [Required]
        public virtual int Year { get; set; }

        [Required]
        public virtual int HolidayTypeId { get; set; }

         public virtual int? LocationId { get; set; }

        //[Required]
        //[StringLength(HolidaysConsts.MaxHolidayTypeLength, MinimumLength = HolidaysConsts.MinHolidayTypeLength)]
        //public virtual string HolidayType { get; set; }

        public virtual HolidayTypes HolidayTypes { get; set; }
        public virtual Locations Locations { get; set; }

    }
}