﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("PostedTimesheets")]
    public class PostedTimesheets : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual int EmployeeId { get; set; }

        [Required]
        public virtual DateTime Date { get; set; }

        public virtual int RegMins { get; set; }

        public virtual decimal Absence { get; set; }

        public virtual int TardyMins { get; set; }

        public virtual int UtMins { get; set; }

        //public virtual decimal TotalOtMins { get; set; }

        public virtual decimal ObMins { get; set; }

        public virtual int RegNd1Mins { get; set; }

        public virtual int RegNd2Mins { get; set; }

        public virtual decimal PaidHoliday { get; set; }

        public virtual decimal LeaveDays { get; set; }

        public virtual string Remarks { get; set; }

        public virtual bool IsHoliday { get; set; }

        public virtual bool IsRestDay { get; set; }

        public virtual Employees Employees { get; set; }
    }
}
