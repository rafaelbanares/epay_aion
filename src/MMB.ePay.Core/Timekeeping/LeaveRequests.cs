﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MMB.ePay.Timekeeping
{
    [Table("LeaveRequests")]
    public class LeaveRequests : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(LeaveRequestsConsts.MaxRemarksLength, MinimumLength = LeaveRequestsConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        [StringLength(LeaveRequestsConsts.MaxRemarksLength, MinimumLength = LeaveRequestsConsts.MinRemarksLength)]
        public virtual string Reason { get; set; }

        public virtual int EmployeeId { get; set; }

        public virtual int LeaveTypeId { get; set; }

        public virtual int LeaveReasonId { get; set; }

        public virtual int StatusId { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual LeaveReasons LeaveReasons { get; set; }
        public virtual LeaveTypes LeaveTypes { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<LeaveApprovals> LeaveApprovals { get; set; }
        public virtual ICollection<LeaveRequestDetails> LeaveRequestDetails { get; set; }
    }
}