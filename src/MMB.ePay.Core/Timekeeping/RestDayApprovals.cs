﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("RestDayApprovals")]
    public class RestDayApprovals : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [StringLength(RestDayApprovalsConsts.MaxRemarksLength, MinimumLength = RestDayApprovalsConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        public virtual int RestDayRequestId { get; set; }

        public virtual int StatusId { get; set; }

        public virtual RestDayRequests RestDayRequests { get; set; }
        public virtual Status Status { get; set; }
    }
}