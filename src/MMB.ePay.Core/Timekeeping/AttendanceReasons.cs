﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("AttendanceReasons")]
    public class AttendanceReasons : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(AttendanceReasonsConsts.MaxDisplayNameLength, MinimumLength = AttendanceReasonsConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [StringLength(AttendanceReasonsConsts.MaxDescriptionLength, MinimumLength = AttendanceReasonsConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual ICollection<AttendanceRequests> AttendanceRequests { get; set; }
    }
}