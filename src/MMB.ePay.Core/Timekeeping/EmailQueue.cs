﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("EmailQueue")]
    public class EmailQueue : CreationAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [Required]
        [StringLength(EmailQueueConsts.MaxSubjectLength, MinimumLength = EmailQueueConsts.MinSubjectLength)]
        public virtual string Subject { get; set; }

        [Required]
        [StringLength(EmailQueueConsts.MaxRequestLength, MinimumLength = EmailQueueConsts.MinRequestLength)]
        public virtual string Request { get; set; }

        [Required]
        [StringLength(EmailQueueConsts.MaxFullNameLength, MinimumLength = EmailQueueConsts.MinFullNameLength)]
        public virtual string FullName { get; set; }

        [Required]
        [StringLength(EmailQueueConsts.MaxApprovalStatusLength, MinimumLength = EmailQueueConsts.MinApprovalStatusLength)]
        public virtual string ApprovalStatus { get; set; }

        [Required]
        public virtual DateTime? StartDate { get; set; }

        public virtual DateTime? EndDate { get; set; }

        [Required]
        [StringLength(EmailQueueConsts.MaxRemarksLength, MinimumLength = EmailQueueConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        [StringLength(EmailQueueConsts.MaxReasonLength, MinimumLength = EmailQueueConsts.MinReasonLength)]
        public virtual string Reason { get; set; }

        [Required]
        [StringLength(EmailQueueConsts.MaxRecipientLength, MinimumLength = EmailQueueConsts.MinRecipientLength)]
        public virtual string Recipient { get; set; }

        [Required]
        [StringLength(EmailQueueConsts.MaxStatusLength, MinimumLength = EmailQueueConsts.MinStatusLength)]
        public virtual string Status { get; set; }

        [StringLength(EmailQueueConsts.MaxStatusErrorLength, MinimumLength = EmailQueueConsts.MinStatusErrorLength)]
        public virtual string StatusError { get; set; }


    }
}