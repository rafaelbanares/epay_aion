﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("ExcuseApprovals")]
    public class ExcuseApprovals : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [StringLength(ExcuseApprovalsConsts.MaxRemarksLength, MinimumLength = ExcuseApprovalsConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        public virtual int ExcuseRequestId { get; set; }

        public virtual int StatusId { get; set; }

        public virtual ExcuseRequests ExcuseRequests { get; set; }
        public virtual Status Status { get; set; }
    }
}