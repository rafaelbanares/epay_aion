﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("OvertimeTypes")]
    public class OvertimeTypes : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [StringLength(OvertimeTypesConsts.MaxShortNameLength, MinimumLength = OvertimeTypesConsts.MinShortNameLength)]
        public virtual string ShortName { get; set; }

        [StringLength(OvertimeTypesConsts.MaxDescriptionLength, MinimumLength = OvertimeTypesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        [StringLength(OvertimeTypesConsts.MaxOTCodeLength, MinimumLength = OvertimeTypesConsts.MinOTCodeLength)]
        public virtual string OTCode { get; set; }

        public virtual short SubType { get; set; }

        public virtual ICollection<PostedOvertimes> PostedOvertimes { get; set; }
        public virtual ICollection<ProcessedOvertimes> ProcessedOvertimes { get; set; }
    }
}
