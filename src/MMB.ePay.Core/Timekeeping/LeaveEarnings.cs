﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("LeaveEarnings")]
    public class LeaveEarnings : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual DateTime Date { get; set; }

        [Required]
        public virtual short AppYear { get; set; }

        public virtual DateTime? ValidFrom { get; set; }

        public virtual DateTime? ValidTo { get; set; }

        [Required]
        [StringLength(LeaveEarningsConsts.MaxRemarksLength, MinimumLength = LeaveEarningsConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        public virtual short? AppMonth { get; set; }

        public virtual ICollection<LeaveEarningDetails> LeaveEarningDetails { get; set; }
    }
}