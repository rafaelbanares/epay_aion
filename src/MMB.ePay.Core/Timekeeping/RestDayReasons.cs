﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("RestDayReasons")]
    public class RestDayReasons : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(RestDayReasonsConsts.MaxDisplayNameLength, MinimumLength = RestDayReasonsConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [StringLength(RestDayReasonsConsts.MaxDescriptionLength, MinimumLength = RestDayReasonsConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual ICollection<RestDayRequests> RestDayRequests { get; set; }
    }
}