﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using NetTopologySuite.Geometries;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("RawTimeLogs")]
    public class RawTimeLogs : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(RawTimeLogsConsts.MaxSwipeCodeLength, MinimumLength = RawTimeLogsConsts.MinSwipeCodeLength)]
        public virtual string SwipeCode { get; set; }

        [Required]
        public virtual DateTime LogTime { get; set; }

        [Required]
        [StringLength(RawTimeLogsConsts.MaxInOutLength, MinimumLength = RawTimeLogsConsts.MinInOutLength)]
        public virtual string InOut { get; set; }

        public bool WorkFromHome { get; set; }

        [StringLength(RawTimeLogsConsts.MaxDeviceLength, MinimumLength = RawTimeLogsConsts.MinDeviceLength)]
        public string Device { get; set; }

        public Point Geolocation { get; set; }
    }
}