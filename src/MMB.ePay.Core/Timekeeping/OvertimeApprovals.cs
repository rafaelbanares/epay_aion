﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("OvertimeApprovals")]
    public class OvertimeApprovals : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [StringLength(OvertimeApprovalsConsts.MaxRemarksLength, MinimumLength = OvertimeApprovalsConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        public virtual int OvertimeRequestId { get; set; }

        public virtual int StatusId { get; set; }

        public virtual OvertimeRequests OvertimeRequests { get; set; }
        public virtual Status Status { get; set; }
    }
}