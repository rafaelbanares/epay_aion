﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("ShiftTypes")]
    public class ShiftTypes : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(ShiftTypesConsts.MaxDisplayNameLength, MinimumLength = ShiftTypesConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        [StringLength(ShiftTypesConsts.MaxDescriptionLength, MinimumLength = ShiftTypesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        [Required]
        public virtual short Flexible1 { get; set; }

        [Required]
        public virtual short GracePeriod1 { get; set; }

        [Required]
        public virtual short MinimumOvertime { get; set; }

        //public virtual bool RotateShiftWeekly { get; set; }

        public virtual ICollection<Attendances> Attendances { get; set; }
        public virtual ICollection<PostedAttendances> PostedAttendances { get; set; }
        public virtual ICollection<EmployeeTimeInfo> EmployeeTimeInfo { get; set; }
        public virtual ICollection<ShiftRequests> ShiftRequests { get; set; }
        public virtual ICollection<ShiftTypeDetails> ShiftTypeDetails { get; set; }

    }
}