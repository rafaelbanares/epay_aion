﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("ApproverOrders")]
    public class ApproverOrders : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(ApproverOrdersConsts.MaxDisplayNameLength, MinimumLength = ApproverOrdersConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        public virtual int Level { get; set; }

        [Required]
        public virtual bool IsBackup { get; set; }

        public virtual ICollection<EmployeeApprovers> EmployeeApprovers { get; set; }
    }
}