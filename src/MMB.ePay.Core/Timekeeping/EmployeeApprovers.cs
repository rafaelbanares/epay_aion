﻿using Abp.Domain.Entities;
using MMB.ePay.HR;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("EmployeeApprovers")]
    public class EmployeeApprovers : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public virtual int EmployeeId { get; set; }

        public virtual int ApproverId { get; set; }

        public virtual int ApproverOrderId { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual Employees Approvers { get; set; }
        public virtual ApproverOrders ApproverOrders { get; set; }
    }
}