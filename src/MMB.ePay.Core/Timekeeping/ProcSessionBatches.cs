﻿using Abp.Domain.Entities;
using MMB.ePay.HR;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("ProcSessionBatches")]
    public class ProcSessionBatches : Entity<int>
    {
        public virtual int PsId { get; set; }

        public virtual int EmployeeId { get; set; }

        public virtual Guid? BatchId { get; set; }

        public virtual Employees Employees { get; set; }

        public virtual ProcSessions ProcSessions { get; set; }
    }
}
