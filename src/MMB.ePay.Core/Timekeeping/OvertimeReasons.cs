﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("OvertimeReasons")]
    public class OvertimeReasons : Entity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(OvertimeReasonsConsts.MaxDisplayNameLength, MinimumLength = OvertimeReasonsConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [StringLength(OvertimeReasonsConsts.MaxDescriptionLength, MinimumLength = OvertimeReasonsConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual ICollection<OvertimeRequests> OvertimeRequests { get; set; }
    }
}