﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.HR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.Timekeeping
{
    [Table("RestDayRequests")]
    public class RestDayRequests : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(RestDayRequestsConsts.MaxRestDayCodeLength, MinimumLength = RestDayRequestsConsts.MinRestDayCodeLength)]
        public virtual string RestDayCode { get; set; }

        [Required]
        [StringLength(RestDayRequestsConsts.MaxRemarksLength, MinimumLength = RestDayRequestsConsts.MinRemarksLength)]
        public virtual string Remarks { get; set; }

        [StringLength(RestDayRequestsConsts.MaxRemarksLength, MinimumLength = RestDayRequestsConsts.MinRemarksLength)]
        public virtual string Reason { get; set; }

        [Required]
        public virtual DateTime RestDayStart { get; set; }

        [Required]
        public virtual DateTime RestDayEnd { get; set; }

        public virtual int EmployeeId { get; set; }

        public virtual int RestDayReasonId { get; set; }

        public virtual int StatusId { get; set; }

        public virtual Employees Employees { get; set; }
        public virtual RestDayReasons RestDayReasons { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<RestDayApprovals> RestDayApprovals { get; set; }

    }
}