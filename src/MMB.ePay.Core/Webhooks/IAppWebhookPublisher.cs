﻿using System.Threading.Tasks;
using MMB.ePay.Authorization.Users;

namespace MMB.ePay.WebHooks
{
    public interface IAppWebhookPublisher
    {
        Task PublishTestWebhook();
    }
}
