﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.HR
{
    [Table("EmploymentTypes")]
    public class EmploymentTypes : CreationAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(EmploymentTypesConsts.MaxSysCodeLength, MinimumLength = EmploymentTypesConsts.MinSysCodeLength)]
        public string SysCode { get; set; }

        [Required]
        [StringLength(EmploymentTypesConsts.MaxDescriptionLength, MinimumLength = EmploymentTypesConsts.MinDescriptionLength)]
        public virtual string Description { get; set; }

        public virtual ICollection<Employees> Employees { get; set; }

    }
}