﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.Authorization.Users;
using MMB.ePay.MultiTenancy;
using MMB.ePay.Timekeeping;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.HR
{
    [Table("Employees")]
    public class Employees : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(EmployeesConsts.MaxEmployeeCodeLength, MinimumLength = EmployeesConsts.MinEmployeeCodeLength)]
        public virtual string EmployeeCode { get; set; }

        [Required]
        [StringLength(EmployeesConsts.MaxLastNameLength, MinimumLength = EmployeesConsts.MinLastNameLength)]
        public virtual string LastName { get; set; }

        [Required]
        [StringLength(EmployeesConsts.MaxFirstNameLength, MinimumLength = EmployeesConsts.MinFirstNameLength)]
        public virtual string FirstName { get; set; }

        [StringLength(EmployeesConsts.MaxMiddleNameLength, MinimumLength = EmployeesConsts.MinMiddleNameLength)]
        public virtual string MiddleName { get; set; }

        [StringLength(EmployeesConsts.MaxEmailLength, MinimumLength = EmployeesConsts.MinEmailLength)]
        public virtual string Email { get; set; }

        [StringLength(EmployeesConsts.MaxAddressLength, MinimumLength = EmployeesConsts.MinAddressLength)]
        public virtual string Address { get; set; }

        [StringLength(EmployeesConsts.MaxPositionLength, MinimumLength = EmployeesConsts.MinPositionLength)]
        public virtual string Position { get; set; }

        public virtual DateTime? DateOfBirth { get; set; }

        public virtual DateTime? DateEmployed { get; set; }

        public virtual DateTime? DateTerminated { get; set; }

        public virtual DateTime? DateRegularized { get; set; }

        [StringLength(EmployeesConsts.MaxGenderLength, MinimumLength = EmployeesConsts.MinGenderLength)]
        public virtual string Gender { get; set; }

        [Required]
        [StringLength(EmployeesConsts.MaxEmployeeStatusLength, MinimumLength = EmployeesConsts.MinEmployeeStatusLength)]
        public virtual string EmployeeStatus { get; set; }

        [Required]
        public virtual int EmploymentTypeId { get; set; }

        public virtual string Rank { get; set; }

        public virtual string FullName
        {
            get { return string.Format("{0}, {1} {2}", LastName, FirstName, MiddleName).TrimEnd(); }
        }

        public virtual User User { get; set; }
        //public virtual Tenant Tenant { get; set; }
        public virtual EmploymentTypes EmploymentTypes { get; set; }

        public virtual EmployeeTimeInfo EmployeeTimeInfo { get; set; }

        public virtual ICollection<Attendances> Attendances { get; set; }
        public virtual ICollection<AttendanceUploads> AttendanceUploads { get; set; }
        public virtual ICollection<AttendanceRequests> AttendanceRequests { get; set; }
        public virtual ICollection<PostedAttendances> PostedAttendances { get; set; }
        public virtual ICollection<EmployeeApprovers> EmployeeApproversApprover { get; set; }
        public virtual ICollection<EmployeeApprovers> EmployeeApproversEmployee { get; set; }
        public virtual ICollection<ExcuseRequests> ExcuseRequests { get; set; }
        public virtual ICollection<LeaveEarningDetails> LeaveEarningDetails { get; set; }
        public virtual ICollection<LeaveRequests> LeaveRequests { get; set; }
        public virtual ICollection<LeaveRequestDetails> LeaveRequestDetails { get; set; }
        public virtual ICollection<OfficialBusinessRequests> OfficialBusinessRequests { get; set; }
        public virtual ICollection<OvertimeRequests> OvertimeRequests { get; set; }
        public virtual ICollection<PostedLeaves> PostedLeaves { get; set; }
        public virtual ICollection<PostedTimesheets> PostedTimesheets { get; set; }
        public virtual ICollection<PostedOvertimes> PostedOvertimes { get; set; }
        public virtual ICollection<ProcessedLeaves> ProcessedLeaves { get; set; }
        public virtual ICollection<ProcessedTimesheets> ProcessedTimesheets { get; set; }
        public virtual ICollection<ProcessedOvertimes> ProcessedOvertimes { get; set; }
        public virtual ICollection<ProcSessionBatches> ProcSessionBatches { get; set; }
        public virtual ICollection<RestDayRequests> RestDayRequests { get; set; }
        public virtual ICollection<ShiftRequests> ShiftRequests { get; set; }
    }
}