﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.HR
{
    [Table("Companies")]
    public class Companies : AuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        [StringLength(CompaniesConsts.MaxDisplayNameLength, MinimumLength = CompaniesConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [StringLength(CompaniesConsts.MaxAddress1Length, MinimumLength = CompaniesConsts.MinAddress1Length)]
        public virtual string Address1 { get; set; }

        [StringLength(CompaniesConsts.MaxAddress2Length, MinimumLength = CompaniesConsts.MinAddress2Length)]
        public virtual string Address2 { get; set; }

        [StringLength(CompaniesConsts.MaxOrgUnitLevel1NameLength, MinimumLength = CompaniesConsts.MinOrgUnitLevel1NameLength)]
        public virtual string OrgUnitLevel1Name { get; set; }

        [StringLength(CompaniesConsts.MaxOrgUnitLevel2NameLength, MinimumLength = CompaniesConsts.MinOrgUnitLevel2NameLength)]
        public virtual string OrgUnitLevel2Name { get; set; }

        [StringLength(CompaniesConsts.MaxOrgUnitLevel3NameLength, MinimumLength = CompaniesConsts.MinOrgUnitLevel3NameLength)]
        public virtual string OrgUnitLevel3Name { get; set; }
    }
}
