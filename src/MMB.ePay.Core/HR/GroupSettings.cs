﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using MMB.ePay.Timekeeping;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MMB.ePay.HR
{
    [Table("GroupSettings")]
    public class GroupSettings : CreationAuditedEntity, IMayHaveTenant
    {
        public int? TenantId { get; set; }

        [Required]
        [StringLength(GroupSettingsConsts.MaxSysCodeLength, MinimumLength = GroupSettingsConsts.MinSysCodeLength)]
        public virtual string SysCode { get; set; }

        [Required]
        [StringLength(GroupSettingsConsts.MaxDisplayNameLength, MinimumLength = GroupSettingsConsts.MinDisplayNameLength)]
        public virtual string DisplayName { get; set; }

        [Required]
        public virtual short OrderNo { get; set; }

        public virtual ICollection<TKSettings> TKSettings { get; set; }
    }
}