﻿using Abp.Events.Bus;

namespace MMB.ePay.MultiTenancy
{
    public class RecurringPaymentsEnabledEventData : EventData
    {
        public int TenantId { get; set; }
    }
}