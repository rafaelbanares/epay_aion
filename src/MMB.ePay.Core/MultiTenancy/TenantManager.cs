﻿using Abp;
using Abp.Application.Features;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.IdentityFramework;
using Abp.Localization;
using Abp.MultiTenancy;
using Abp.Notifications;
using Abp.Runtime.Security;
using Abp.Runtime.Session;
using Abp.UI;
using Microsoft.AspNetCore.Identity;
using MMB.ePay.Authorization.Roles;
using MMB.ePay.Authorization.Users;
using MMB.ePay.Editions;
using MMB.ePay.HR;
using MMB.ePay.MultiTenancy.Demo;
using MMB.ePay.MultiTenancy.Payments;
using MMB.ePay.Notifications;
using MMB.ePay.Timekeeping;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace MMB.ePay.MultiTenancy
{
    /// <summary>
    /// Tenant manager.
    /// </summary>
    public class TenantManager : AbpTenantManager<Tenant, User>
    {
        public IAbpSession AbpSession { get; set; }

        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly RoleManager _roleManager;
        private readonly PermissionManager _permissionManager;
        private readonly UserManager _userManager;
        private readonly IUserEmailer _userEmailer;
        private readonly TenantDemoDataBuilder _demoDataBuilder;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IAppNotifier _appNotifier;
        private readonly IAbpZeroDbMigrator _abpZeroDbMigrator;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IRepository<SubscribableEdition> _subscribableEditionRepository;

        private readonly IRepository<AttendanceReasons> _attendanceReasonsRepository;
        private readonly IRepository<ExcuseReasons> _excuseReasonsRepository;
        private readonly IRepository<LeaveReasons> _leaveReasonsRepository;
        private readonly IRepository<OfficialBusinessReasons> _officialBusinessReasonsRepository;
        private readonly IRepository<OvertimeReasons> _overtimeReasonsRepository;
        private readonly IRepository<RestDayReasons> _restDayReasonsRepository;
        private readonly IRepository<ShiftReasons> _shiftReasonsRepository;

        private readonly IRepository<EmploymentTypes> _employmentypesRepository;
        private readonly IRepository<HolidayTypes> _holidayTypesRepository;
        private readonly IRepository<LeaveTypes> _leaveTypesRepository;
        private readonly IRepository<OvertimeTypes> _overtimeTypesRepository;
        private readonly IRepository<ShiftTypes> _shiftTypesRepository;
        private readonly IRepository<ShiftTypeDetails> _shiftTypeDetailsRepository;

        private readonly IRepository<ApproverOrders> _approverOrdersRepository;
        private readonly IRepository<GroupSettings> _groupSettingsRepository;
        private readonly IRepository<TKSettings> _tkSettingsRepository;
        private readonly IRepository<Frequencies> _frequenciesRepository;
        private readonly IRepository<Reports> _reportsRepository;
        private readonly IRepository<Status> _statusRepository;
        private readonly IRepository<WorkFlow> _workFlowRepository;

        public TenantManager(
            IRepository<Tenant> tenantRepository,
            IRepository<TenantFeatureSetting, long> tenantFeatureRepository,
            EditionManager editionManager,
            IUnitOfWorkManager unitOfWorkManager,
            RoleManager roleManager,
            PermissionManager permissionManager,
            IUserEmailer userEmailer,
            TenantDemoDataBuilder demoDataBuilder,
            UserManager userManager,
            INotificationSubscriptionManager notificationSubscriptionManager,
            IAppNotifier appNotifier,
            IAbpZeroFeatureValueStore featureValueStore,
            IAbpZeroDbMigrator abpZeroDbMigrator,
            IPasswordHasher<User> passwordHasher,
            IRepository<SubscribableEdition> subscribableEditionRepository,

            IRepository<AttendanceReasons> attenandanceReasonsRepository,
            IRepository<ExcuseReasons> excuseReasonsRepository,
            IRepository<LeaveReasons> leaveReasonsRepository,
            IRepository<OfficialBusinessReasons> officialBusinessReasonsRepository,
            IRepository<OvertimeReasons> overtimeReasonsRepository,
            IRepository<RestDayReasons> restDayReasonsRepository,
            IRepository<ShiftReasons> shiftReasonsRepository,

            IRepository<EmploymentTypes> employmentTypesRepository,
            IRepository<HolidayTypes> holidayTypesRepository,
            IRepository<LeaveTypes> leaveTypesRepository,
            IRepository<OvertimeTypes> overtimeTypesRepository,
            IRepository<ShiftTypes> shiftTypesRepository,
            IRepository<ShiftTypeDetails> shiftTypeDetailsRepository,

            IRepository<ApproverOrders> approverOrdersRepository,
            IRepository<GroupSettings> groupSettingsRepository,
            IRepository<TKSettings> tkSettingsRepository,
            IRepository<Frequencies> frequenciesRepository,
            IRepository<Reports> reportsRepository,
            IRepository<Status> statusRepository,
            IRepository<WorkFlow> workFlowRepository

            ) : base(
                tenantRepository,
                tenantFeatureRepository,
                editionManager,
                featureValueStore
            )
        {
            AbpSession = NullAbpSession.Instance;

            _unitOfWorkManager = unitOfWorkManager;
            _roleManager = roleManager;
            _permissionManager = permissionManager;
            _userEmailer = userEmailer;
            _demoDataBuilder = demoDataBuilder;
            _userManager = userManager;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _appNotifier = appNotifier;
            _abpZeroDbMigrator = abpZeroDbMigrator;
            _passwordHasher = passwordHasher;
            _subscribableEditionRepository = subscribableEditionRepository;

            _attendanceReasonsRepository = attenandanceReasonsRepository;
            _excuseReasonsRepository = excuseReasonsRepository;
            _leaveReasonsRepository = leaveReasonsRepository;
            _officialBusinessReasonsRepository = officialBusinessReasonsRepository;
            _overtimeReasonsRepository = overtimeReasonsRepository;
            _restDayReasonsRepository = restDayReasonsRepository;
            _shiftReasonsRepository = shiftReasonsRepository;

            _employmentypesRepository = employmentTypesRepository;
            _holidayTypesRepository = holidayTypesRepository;
            _leaveTypesRepository = leaveTypesRepository;
            _overtimeTypesRepository = overtimeTypesRepository;
            _shiftTypesRepository = shiftTypesRepository;
            _shiftTypeDetailsRepository = shiftTypeDetailsRepository;

            _approverOrdersRepository = approverOrdersRepository;
            _groupSettingsRepository = groupSettingsRepository;
            _tkSettingsRepository = tkSettingsRepository;
            _frequenciesRepository = frequenciesRepository;
            _reportsRepository = reportsRepository;
            _statusRepository = statusRepository;
            _workFlowRepository = workFlowRepository;
        }

        protected virtual async Task CreateDefaultReasons()
        {
            var reasons = new List<string> { "Activities", "Holidays", "OTHERS" };

            foreach (var reason in reasons)
            {
                await _attendanceReasonsRepository.InsertAsync(new AttendanceReasons { DisplayName = reason });
                await _excuseReasonsRepository.InsertAsync(new ExcuseReasons { DisplayName = reason });
                await _leaveReasonsRepository.InsertAsync(new LeaveReasons { DisplayName = reason });
                await _officialBusinessReasonsRepository.InsertAsync(new OfficialBusinessReasons { DisplayName = reason });
                await _overtimeReasonsRepository.InsertAsync(new OvertimeReasons { DisplayName = reason });
                await _restDayReasonsRepository.InsertAsync(new RestDayReasons { DisplayName = reason });
                await _shiftReasonsRepository.InsertAsync(new ShiftReasons { DisplayName = reason });
            }

            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        protected virtual async Task CreateDefaultTypes()
        {
            var employmentTypes = new List<EmploymentTypes>
            {
                new() { SysCode = "C", Description = "Contractual" },
                new() { SysCode = "P", Description = "Probationary" },
                new() { SysCode = "R", Description = "Regular" },
                new() { SysCode = "S", Description = "Consultant" }
            };

            foreach (var employmentType in employmentTypes)
            {
                await _employmentypesRepository.InsertAsync(employmentType);
            }

            var holidayTypes = new List<HolidayTypes>
            {
                new() { DisplayName = "Legal", HolidayCode = "L", HalfDayEnabled = false },
                new() { DisplayName = "Special", HolidayCode = "S", HalfDayEnabled = false },
                new() { DisplayName = "Legal & Special", HolidayCode = "LS", HalfDayEnabled = false },
                new() { DisplayName = "Double Legal", HolidayCode = "D", HalfDayEnabled = false },
                new() { DisplayName = "Company Holiday", HolidayCode = "C", HalfDayEnabled = true },
                new() { DisplayName = "Regional Holiday", HolidayCode = "R", HalfDayEnabled = false }
            };

            foreach (var holidayType in holidayTypes)
            {
                holidayType.Description = holidayType.DisplayName;
                await _holidayTypesRepository.InsertAsync(holidayType);
            }

            var leaveTypes = new List<LeaveTypes>
            {
                new() { DisplayName = "SL", Description = "Sick Leave", WithPay = true },
                new() { DisplayName = "VL", Description = "Vacation Leave", WithPay = true },
                new() { DisplayName = "EL", Description = "Emergency Leave", WithPay = true },
                new() { DisplayName = "ML", Description = "Maternity Leave", WithPay = true },
                new() { DisplayName = "PL", Description = "Paternity Leave", WithPay = true }
            };

            foreach (var leaveType in leaveTypes)
            {
                await _leaveTypesRepository.InsertAsync(leaveType);
            }

            var otTypeValues = new List<Tuple<string, string>>
            {
                new ( "Double Legal Holiday", "D" ),
                new ( "Legal Holiday", "L" ),
                new ( "Legal Rest Day", "L+RST" ),
                new ( "Legal Special", "LS" ),
                new ( "Regular OT", "REG" ),
                new ( "Rest Day OT", "RST" ),
                new ( "Special Holiday", "S" ),
                new ( "Special Rest Day", "S+RST" )
            };

            foreach (var value in otTypeValues)
            {
                for (short subType = 1; subType <= 4; subType++)
                {
                    var shortName = value.Item1;

                    switch (subType)
                    {
                        case 2:
                            shortName += " > 8";
                            break;
                        case 3:
                            shortName += " ND1";
                            break;
                        case 4:
                            shortName += " ND2";
                            break;
                    }

                    var overtimeType = new OvertimeTypes
                    {
                        ShortName = shortName,
                        Description = shortName,
                        OTCode = value.Item2,
                        SubType = subType
                    };

                    await _overtimeTypesRepository.InsertAsync(overtimeType);
                }
            };

            var shift9Am = new ShiftTypes
            {
                DisplayName = "09AM-06PM",
                Description = "09:00 AM - 06:00 PM",
                Flexible1 = 0,
                GracePeriod1 = 20,
                MinimumOvertime = 30
            };
            await _shiftTypesRepository.InsertAsync(shift9Am);

            var Shift10Am = new ShiftTypes
            {
                DisplayName = "10AM-06PM",
                Description = "10:00 AM - 07:00 PM",
                Flexible1 = 0,
                GracePeriod1 = 20,
                MinimumOvertime = 30
            };
            await _shiftTypesRepository.InsertAsync(Shift10Am);

            await _unitOfWorkManager.Current.SaveChangesAsync();

            var shiftDetail9Am = new ShiftTypeDetails
            {
                Days = "1234567",
                TimeIn = "09:00",
                BreaktimeIn = "12:00",
                BreaktimeOut = "13:00",
                TimeOut = "18:00",
                ShiftTypeId = shift9Am.Id,
                StartOnPreviousDay = false
            };
            await _shiftTypeDetailsRepository.InsertAsync(shiftDetail9Am);

            var shiftDetail10Am = new ShiftTypeDetails
            {
                Days = "1234567",
                TimeIn = "10:00",
                BreaktimeIn = "13:00",
                BreaktimeOut = "14:00",
                TimeOut = "19:00",
                ShiftTypeId = Shift10Am.Id,
                StartOnPreviousDay = false
            };
            await _shiftTypeDetailsRepository.InsertAsync(shiftDetail10Am);

            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        protected virtual async Task CreateDefaultOther(int tenantId)
        {
            //ApproverOrders
            var approverOrders = new List<ApproverOrders>
            {
                new() { DisplayName = "Approver 1", Level = 1, IsBackup = false },
                new() { DisplayName = "Backup Approver 1", Level = 1, IsBackup = true },
                new() { DisplayName = "Approver 2", Level = 2, IsBackup = false },
                new() { DisplayName = "Backup Approver 2", Level = 2, IsBackup = true }
            };

            foreach (var approverOrder in approverOrders)
            {
                await _approverOrdersRepository.InsertAsync(approverOrder);
                await _unitOfWorkManager.Current.SaveChangesAsync();
            }

            //GroupSettings
            await _groupSettingsRepository.InsertAsync(new GroupSettings
            {
                TenantId = tenantId,
                SysCode = "ORG",
                DisplayName = "Org. Group Settings",
                OrderNo = 1
            });

            var groupSetting = new GroupSettings
            {
                TenantId = tenantId,
                SysCode = "MISC",
                DisplayName = "Miscellaneous",
                OrderNo = 2
            };

            await _groupSettingsRepository.InsertAsync(groupSetting);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            //TKSettings
            var tkSettings = new List<TKSettings>
            {
                new TKSettings
                {
                    Key = "DATE_DISPLAY_FORMAT",
                    Value = "MM/dd/yyyy",
                    DataType = "string",
                    Caption = "Date Display Format",
                    DisplayOrder = 1,
                    GroupSettingId = groupSetting.Id
                },
                new TKSettings
                {
                    Key = "ENABLE_EMAIL_NOTIFICATION",
                    Value = "false",
                    DataType = "bool",
                    Caption = "Enable Email Notification",
                    DisplayOrder = 2,
                    GroupSettingId = groupSetting.Id
                },
                new TKSettings
                {
                    Key = "MAX_PAGE_SIZE",
                    Value = "10",
                    DataType = "int",
                    Caption = "Maximum Page Size",
                    DisplayOrder = 3,
                    GroupSettingId = groupSetting.Id
                },
                new TKSettings
                {
                    Key = "MULTI_SHIFT",
                    Value = "true",
                    DataType = "bool",
                    Caption = "Multipe Shift for the Week",
                    DisplayOrder = 4,
                    GroupSettingId = groupSetting.Id
                },
                new TKSettings
                {
                    Key = "RAWDATA_HAS_IO_INDICATOR",
                    Value = "true",
                    DataType = "bool",
                    Caption = "Raw Data has IO Indicator",
                    DisplayOrder = 5,
                    GroupSettingId = groupSetting.Id
                },
                new TKSettings
                {
                    Key = "ND1_START",
                    Value = "22:00",
                    DataType = "time",
                    Caption = "ND1 Start",
                    DisplayOrder = 6,
                    GroupSettingId = groupSetting.Id
                },
                new TKSettings
                {
                    Key = "ND1_END",
                    Value = "06:00",
                    DataType = "time",
                    Caption = "ND1 End",
                    DisplayOrder = 7,
                    GroupSettingId = groupSetting.Id
                },
                new TKSettings
                {
                    Key = "ND2_START",
                    Value = "06:00",
                    DataType = "time",
                    Caption = "ND2 Start",
                    DisplayOrder = 8,
                    GroupSettingId = groupSetting.Id
                },
                new TKSettings
                {
                    Key = "ND2_END",
                    Value = "06:00",
                    DataType = "time",
                    Caption = "ND2 End",
                    DisplayOrder = 9,
                    GroupSettingId = groupSetting.Id
                }
            };

            foreach (var tkSetting in tkSettings)
            {
                tkSetting.TenantId = tenantId;
                await _tkSettingsRepository.InsertAsync(tkSetting);
            };

            //Frequencies
            var frequencies = new List<string> { "Monthly", "Semi-Monthly", "Weekly" };

            foreach (var frequency in frequencies)
            {
                await _frequenciesRepository.InsertAsync(new Frequencies
                {
                    DisplayName = frequency,
                    Description = frequency
                });
            }

            //Reports
            var reports = new List<Reports>
            {
                new()
                { 
                    DisplayName = "Attendance Transaction", 
                    ControllerName = "AttendanceTransaction", 
                    IsLandscape = false, 
                    Output = "P"
                },
                new() 
                { 
                    DisplayName = "Excuse Transaction", 
                    ControllerName = "ExcuseTransaction", 
                    IsLandscape = false, 
                    Output = "P" 
                },
                new() 
                {
                    DisplayName = "Leave Transaction", 
                    ControllerName = "LeaveTransaction", 
                    IsLandscape = false, 
                    Output = "P" 
                },
                new() 
                { 
                    DisplayName = "Official Business Transaction", 
                    ControllerName = "OfficialBusinessTransaction", 
                    IsLandscape = false, 
                    Output = "P" 
                },
                new() 
                { 
                    DisplayName = "Overtime Transaction", 
                    ControllerName = "OvertimeTransaction",
                    IsLandscape = false,
                    Output = "P" ,
                    StoredProcedure = "usp_OvertimeTransaction"
                },
                new() 
                { 
                    DisplayName = "Rest Day Transaction", 
                    ControllerName = "RestDayTransaction",
                    IsLandscape = false, 
                    Output = "P"
                },
                new() 
                { 
                    DisplayName = "Shift Transaction", 
                    ControllerName = "ShiftTransaction", 
                    IsLandscape = false, 
                    Output = "P" 
                },
                new() 
                { 
                    DisplayName = "Attendance Summary",
                    ControllerName = "AttendanceSummary", 
                    IsLandscape = true,
                    Output = "P",
                    StoredProcedure = "usp_AttendanceSummary"
                },
                new() 
                { 
                    DisplayName = "Attendance Summary (Vertical Format)", 
                    ControllerName = "AttendanceSummary2", 
                    IsLandscape = false, 
                    Output = "P",
                    StoredProcedure = "usp_AttendanceSummary2"
                },
                new() 
                { 
                    DisplayName = "Leave Summary",
                    ControllerName = "LeaveSummary",
                    IsLandscape = false,
                    Output = "P" 
                },
                new() 
                { 
                    DisplayName = "Overtime Summary",
                    ControllerName = "OvertimeSummary",
                    IsLandscape = false, 
                    Output = "P",
                    StoredProcedure = "usp_OvertimeSummary"
                },
                new() 
                { 
                    DisplayName = "Daily Attendance", 
                    ControllerName = "DailyAttendance",
                    IsLandscape = true, 
                    Output = "P"
                },
                new() 
                { 
                    DisplayName = "Leave Balance",
                    ControllerName = "LeaveBalance", 
                    IsLandscape = false, 
                    Output = "P" 
                },
                new() 
                { 
                    DisplayName = "Work From Home",
                    ControllerName = "WorkFromHome", 
                    IsLandscape = false, 
                    Output = "P" 
                },
                new() 
                { 
                    DisplayName = "Attendance Summary (Payroll)",
                    ControllerName = "AttendanceSummaryPayroll", 
                    IsLandscape = true, 
                    Output = "E",
                    StoredProcedure = "usp_AttendanceSummaryPayroll"
                },
                new() 
                { 
                    DisplayName = "Leave Summary (Payroll)",
                    ControllerName = "LeaveSummaryPayroll", 
                    IsLandscape = true, 
                    Output = "E" 
                },
                new() 
                { 
                    DisplayName = "Overtime Summary (Payroll)",
                    ControllerName = "OvertimeSummaryPayroll", 
                    IsLandscape = true, 
                    Output = "E" 
                },
            };

            foreach (var report in reports)
            {
                report.HasEmployeeFilter = true;
                report.HasPeriodFilter = true;
                report.Enabled = true;
                report.Format = "Letter";
                report.ExportFileName = report.DisplayName.Replace(" ", "_");

                await _reportsRepository.InsertAsync(report);
            }

            //Status
            var statusNew = new Status
            {
                DisplayName = "New",
                AfterStatusMessage = "Created",
                BeforeStatusActionText = null,
                IsEditable = true,
                AccessLevel = 0
            };
            await _statusRepository.InsertAsync(statusNew);

            var statusForApproval = new Status
            {
                DisplayName = "For Approval",
                AfterStatusMessage = "Sent For Approval",
                BeforeStatusActionText = "Send for Approval",
                IsEditable = false,
                AccessLevel = 1
            };
            await _statusRepository.InsertAsync(statusForApproval);

            var statusForNextApproval = new Status
            {
                DisplayName = "For Next Approval",
                AfterStatusMessage = "Pre-Approved",
                BeforeStatusActionText = "Approve",
                IsEditable = false,
                AccessLevel = 2
            };
            await _statusRepository.InsertAsync(statusForNextApproval);

            var statusDeclined = new Status
            {
                DisplayName = "Declined",
                AfterStatusMessage = "Declined",
                BeforeStatusActionText = "Decline",
                IsEditable = false,
                AccessLevel = 0
            };
            await _statusRepository.InsertAsync(statusDeclined);

            var statusCancelled = new Status
            {
                DisplayName = "Cancelled",
                AfterStatusMessage = "Cancelled",
                BeforeStatusActionText = "Cancel Request",
                IsEditable = false,
                AccessLevel = 0
            };
            await _statusRepository.InsertAsync(statusCancelled);

            var statusApproved = new Status
            {
                DisplayName = "Approved",
                AfterStatusMessage = "Approved",
                BeforeStatusActionText = "Approve",
                IsEditable = false,
                AccessLevel = 0
            };
            await _statusRepository.InsertAsync(statusApproved);

            var statusReopened = new Status
            {
                DisplayName = "Reopened",
                AfterStatusMessage = "Reopened",
                BeforeStatusActionText = "Reopen",
                IsEditable = true,
                AccessLevel = 0
            };
            await _statusRepository.InsertAsync(statusReopened);

            var statusForUpdate = new Status
            {
                DisplayName = "For Update",
                AfterStatusMessage = "Changed Status",
                BeforeStatusActionText = "For Update",
                IsEditable = true,
                AccessLevel = 0
            };
            await _statusRepository.InsertAsync(statusForUpdate);

            await _unitOfWorkManager.Current.SaveChangesAsync();

            //WorkFlow
            var workFlows = new List<WorkFlow>
            {
                new() { NextStatus = statusCancelled.Id, StatusId = statusNew.Id },
                new() { NextStatus = statusForApproval.Id, StatusId = statusNew.Id },
                new() { NextStatus = statusDeclined.Id, StatusId = statusForApproval.Id },
                new() { NextStatus = statusForNextApproval.Id, StatusId = statusForApproval.Id },
                new() { NextStatus = statusForUpdate.Id, StatusId = statusForApproval.Id },
                new() { NextStatus = statusDeclined.Id, StatusId = statusForNextApproval.Id },
                new() { NextStatus = statusApproved.Id, StatusId = statusForNextApproval.Id },
                new() { NextStatus = statusForUpdate.Id, StatusId = statusForNextApproval.Id },
                new() { NextStatus = null, StatusId = statusDeclined.Id },
                new() { NextStatus = statusReopened.Id, StatusId = statusDeclined.Id },
                new() { NextStatus = statusReopened.Id, StatusId = statusCancelled.Id },
                new() { NextStatus = null, StatusId = statusCancelled.Id },
                new() { NextStatus = null, StatusId = statusApproved.Id },
                new() { NextStatus = statusCancelled.Id, StatusId = statusApproved.Id },
                new() { NextStatus = statusForApproval.Id, StatusId = statusReopened.Id },
                new() { NextStatus = statusCancelled.Id, StatusId = statusReopened.Id },
                new() { NextStatus = statusCancelled.Id, StatusId = statusForUpdate.Id },
                new() { NextStatus = statusForApproval.Id, StatusId = statusForUpdate.Id },
            };

            foreach (var workFlow in workFlows)
            {
                await _workFlowRepository.InsertAsync(workFlow);
            }

            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        public async Task<int> CreateWithAdminUserAsync(
            string tenancyName,
            string name,
            string adminPassword,
            string adminEmailAddress,
            string connectionString,
            bool isActive,
            int? editionId,
            bool shouldChangePasswordOnNextLogin,
            bool sendActivationEmail,
            DateTime? subscriptionEndDate,
            bool isInTrialPeriod,
            string emailActivationLink)
        {
            int newTenantId;
            long newAdminId;

            await CheckEditionAsync(editionId, isInTrialPeriod);

            if (isInTrialPeriod && !subscriptionEndDate.HasValue)
            {
                throw new UserFriendlyException(LocalizationManager.GetString(ePayConsts.LocalizationSourceName, "TrialWithoutEndDateErrorMessage"));
            }

            using (var uow = _unitOfWorkManager.Begin(TransactionScopeOption.RequiresNew))
            {
                //Create tenant
                var tenant = new Tenant(tenancyName, name)
                {
                    IsActive = isActive,
                    EditionId = editionId,
                    SubscriptionEndDateUtc = subscriptionEndDate?.ToUniversalTime(),
                    IsInTrialPeriod = isInTrialPeriod,
                    ConnectionString = connectionString.IsNullOrWhiteSpace() ? null : SimpleStringCipher.Instance.Encrypt(connectionString)
                };

                await CreateAsync(tenant);
                await _unitOfWorkManager.Current.SaveChangesAsync(); //To get new tenant's id.

                //Create tenant database
                _abpZeroDbMigrator.CreateOrMigrateForTenant(tenant);

                //We are working entities of new tenant, so changing tenant filter
                using (_unitOfWorkManager.Current.SetTenantId(tenant.Id))
                {
                    //Create static roles for new tenant
                    CheckErrors(await _roleManager.CreateStaticRoles(tenant.Id));
                    await _unitOfWorkManager.Current.SaveChangesAsync(); //To get static role ids

                    //grant all permissions to admin role
                    var adminRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.Admin);
                    await _roleManager.GrantAllPermissionsAsync(adminRole);

                    //User role should be default
                    var userRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.User);
                    userRole.IsDefault = true;
                    CheckErrors(await _roleManager.UpdateAsync(userRole));

                    //Create admin user for the tenant
                    var adminUser = User.CreateTenantAdminUser(tenant.Id, adminEmailAddress);
                    adminUser.ShouldChangePasswordOnNextLogin = shouldChangePasswordOnNextLogin;
                    adminUser.IsActive = true;

                    await SetTenantRoles();

                    if (adminPassword.IsNullOrEmpty())
                    {
                        adminPassword = await _userManager.CreateRandomPassword();
                    }
                    else
                    {
                        await _userManager.InitializeOptionsAsync(AbpSession.TenantId);
                        foreach (var validator in _userManager.PasswordValidators)
                        {
                            CheckErrors(await validator.ValidateAsync(_userManager, adminUser, adminPassword));
                        }

                    }

                    adminUser.Password = _passwordHasher.HashPassword(adminUser, adminPassword);

                    CheckErrors(await _userManager.CreateAsync(adminUser));
                    await _unitOfWorkManager.Current.SaveChangesAsync(); //To get admin user's id

                    //Assign admin user to admin role!
                    CheckErrors(await _userManager.AddToRoleAsync(adminUser, adminRole.Name));

                    //Notifications
                    await _appNotifier.WelcomeToTheApplicationAsync(adminUser);

                    //Send activation email
                    if (sendActivationEmail)
                    {
                        adminUser.SetNewEmailConfirmationCode();
                        await _userEmailer.SendEmailActivationLinkAsync(adminUser, emailActivationLink, adminPassword);
                    }

                    await _unitOfWorkManager.Current.SaveChangesAsync();

                    await CreateDefaultTypes();
                    await CreateDefaultOther(tenant.Id);
                    await CreateDefaultReasons();

                    await _demoDataBuilder.BuildForAsync(tenant);

                    newTenantId = tenant.Id;
                    newAdminId = adminUser.Id;
                }

                await uow.CompleteAsync();
            }

            //Used a second UOW since UOW above sets some permissions and _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync needs these permissions to be saved.
            using (var uow = _unitOfWorkManager.Begin(TransactionScopeOption.RequiresNew))
            {
                using (_unitOfWorkManager.Current.SetTenantId(newTenantId))
                {
                    await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(new UserIdentifier(newTenantId, newAdminId));
                    await _unitOfWorkManager.Current.SaveChangesAsync();
                    await uow.CompleteAsync();
                }
            }

            return newTenantId;
        }

        private async Task SetTenantRoles()
        {
            //User role should be default
            var employeeRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.Employee);
            employeeRole.IsDefault = true;
            CheckErrors(await _roleManager.UpdateAsync(employeeRole));

            var employeePermissions = new List<string>
            {
                "Pages",
                "Pages.Tenant.Dashboard"
            };
            var employeeOtherPermissions = new List<string>
            {
                "AttendanceRequests",
                "Attendances",
                "LeaveRequests",
                "LeaveRequestDetails",
                "OfficialBusinessRequests",
                "OfficialBusinessRequestDetails",
                "OvertimeRequests",
                "RestDayRequests",
                "ShiftRequests",
                "ShiftTypeDetails",
            };
            foreach (var permission in employeeOtherPermissions)
            {
                employeePermissions.Add("Pages." + permission);
                employeePermissions.Add("Pages." + permission + ".Create");
                employeePermissions.Add("Pages." + permission + ".Delete");
                employeePermissions.Add("Pages." + permission + ".Edit");
            }

            await SetRoleByNames(employeeRole.Id, employeePermissions);

            var approverRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.Approver);
            var approverPermissions = new List<string>
            {
                "Pages",
                "Pages.Administration",
                "Pages.DemoUiComponents",
                "Pages.Processes",
                "Pages.Tenant.Dashboard"
            };
            var approverOtherPermissions = new List<string>
            {
                "AttendanceApprovals",
                "AttendanceReasons",
                "AttendanceRequests",
                "Attendances",
                "EmployeeTimeInfo",
                "EmploymentTypes",
                "ExcuseApprovals",
                "ExcuseReasons",
                "ExcuseRequests",
                "Frequencies",
                "LeaveApprovals",
                "LeaveReasons",
                "LeaveRequestDetails",
                "LeaveRequests",
                "OfficialBusinessApprovals",
                "OfficialBusinessReasons",
                "OfficialBusinessRequestDetails",
                "OfficialBusinessRequests",
                "OvertimeApprovals",
                "OvertimeReasons",
                "OvertimeRequests",
                "PostedAttendances",
                "Process",
                "RawTimeLogs",
                "RestDayApprovals",
                "RestDayReasons",
                "RestDayRequests",
                "ShiftApprovals",
                "ShiftReasons",
                "ShiftRequests",
                "ShiftTypeDetails",
                "Status",
                "WorkFlow"
            };
            foreach (var permission in approverOtherPermissions)
            {
                approverPermissions.Add("Pages." + permission);
                approverPermissions.Add("Pages." + permission + ".Create");
                approverPermissions.Add("Pages." + permission + ".Delete");
                approverPermissions.Add("Pages." + permission + ".Edit");
            }

            await SetRoleByNames(approverRole.Id, approverPermissions);

            var autoUploaderRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.AutoUploader);
            var autoUploaderPermissions = new List<string>
            {
                "Pages",
                "Pages.Attendances",
                "Pages.Attendances.Upload"
            };

            await SetRoleByNames(autoUploaderRole.Id, approverPermissions);

            var clientAdminRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.AutoUploader);
            var clientAdminPermissions = new List<string>
            {
                "Pages",
                "Pages.AdminAttendances",
                "Pages.Administration",
                "Pages.Administration.OrganizationUnits",
                "Pages.Administration.OrganizationUnits.ManageMembers",
                "Pages.Administration.OrganizationUnits.ManageOrganizationTree",
                "Pages.Administration.OrganizationUnits.ManageRoles",
                "Pages.Administration.Users",
                "Pages.Administration.Users.ChangePermissions",
                "Pages.Administration.Users.Create",
                "Pages.Administration.Users.Delete",
                "Pages.Administration.Users.Edit",
                "Pages.Administration.Users.Impersonation",
                "Pages.Administration.Users.Unlock",
                "Pages.Attendances",
                "Pages.Attendances.Create",
                "Pages.Attendances.Delete",
                "Pages.Attendances.Edit",
                "Pages.Attendances.Post",
                "Pages.Attendances.Process",
                "Pages.Attendances.Summarize",
                "Pages.Attendances.Upload",
                "Pages.Reports",
                "Pages.Processes",
                "Pages.DemoUiComponents",
                "Pages.Tenant.Dashboard"
            };
            var clientAdminOtherPermissions = new List<string>
            {
                "Pages.Administration.Roles",
                "Pages.ApproverOrders",
                "Pages.AttendanceApprovals",
                "Pages.AttendancePeriods",
                "Pages.AttendanceReasons",
                "Pages.AttendanceRequests",
                "Pages.EmployeeApprovers",
                "Pages.Employees",
                "Pages.EmployeeTimeInfo",
                "Pages.EmploymentTypes",
                "Pages.ExcuseApprovals",
                "Pages.ExcuseReasons",
                "Pages.ExcuseRequests",
                "Pages.Frequencies",
                "Pages.GroupSettings",
                "Pages.Holidays",
                "Pages.HolidayTypes",
                "Pages.LeaveApprovals",
                "Pages.LeaveReasons",
                "Pages.LeaveRequestDetails",
                "Pages.LeaveRequests",
                "Pages.LeaveTypes",
                "Pages.OfficialBusinessApprovals",
                "Pages.OfficialBusinessReasons",
                "Pages.OfficialBusinessRequestDetails",
                "Pages.OfficialBusinessRequests",
                "Pages.OvertimeApprovals",
                "Pages.OvertimeReasons",
                "Pages.OvertimeRequests",
                "Pages.OvertimeTypes",
                "Pages.PostedAttendances",
                "Pages.Process",
                "Pages.RawTimeLogs",
                "Pages.RestDayApprovals",
                "Pages.RestDayReasons",
                "Pages.RestDayRequests",
                "Pages.ShiftApprovals",
                "Pages.ShiftReasons",
                "Pages.ShiftRequests",
                "Pages.ShiftTypeDetails",
                "Pages.ShiftTypes",
                "Pages.Status"
            };
            foreach (var permission in clientAdminOtherPermissions)
            {
                clientAdminPermissions.Add("Pages." + permission);
                clientAdminPermissions.Add("Pages." + permission + ".Create");
                clientAdminPermissions.Add("Pages." + permission + ".Delete");
                clientAdminPermissions.Add("Pages." + permission + ".Edit");
            }

            await SetRoleByNames(clientAdminRole.Id, approverPermissions);

        }
        private async Task SetRoleByNames(int roleId, List<string> names)
        {
            var permissions = new List<Permission>();
            foreach (var name in names)
            {
                var permission = _permissionManager.GetPermissionOrNull(name);

                if (permission != null)
                    permissions.Add(permission);
            }

            await _roleManager.SetGrantedPermissionsAsync(roleId, permissions);
        }

        public async Task CheckEditionAsync(int? editionId, bool isInTrialPeriod)
        {
            if (!editionId.HasValue || !isInTrialPeriod)
            {
                return;
            }

            var edition = await _subscribableEditionRepository.GetAsync(editionId.Value);
            if (!edition.IsFree)
            {
                return;
            }

            var error = LocalizationManager.GetSource(ePayConsts.LocalizationSourceName).GetString("FreeEditionsCannotHaveTrialVersions");
            throw new UserFriendlyException(error);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public static decimal GetUpgradePrice(SubscribableEdition currentEdition, SubscribableEdition targetEdition, int totalRemainingHourCount, PaymentPeriodType paymentPeriodType)
        {
            int numberOfHoursPerDay = 24;

            var totalRemainingDayCount = totalRemainingHourCount / numberOfHoursPerDay;
            var unusedPeriodCount = totalRemainingDayCount / (int)paymentPeriodType;
            var unusedHoursCount = totalRemainingHourCount % ((int)paymentPeriodType * numberOfHoursPerDay);

            decimal currentEditionPriceForUnusedPeriod = 0;
            decimal targetEditionPriceForUnusedPeriod = 0;

            var currentEditionPrice = currentEdition.GetPaymentAmount(paymentPeriodType);
            var targetEditionPrice = targetEdition.GetPaymentAmount(paymentPeriodType);

            if (currentEditionPrice > 0)
            {
                currentEditionPriceForUnusedPeriod = currentEditionPrice * unusedPeriodCount;
                currentEditionPriceForUnusedPeriod += (currentEditionPrice / (int)paymentPeriodType) / numberOfHoursPerDay * unusedHoursCount;
            }

            if (targetEditionPrice > 0)
            {
                targetEditionPriceForUnusedPeriod = targetEditionPrice * unusedPeriodCount;
                targetEditionPriceForUnusedPeriod += (targetEditionPrice / (int)paymentPeriodType) / numberOfHoursPerDay * unusedHoursCount;
            }

            return targetEditionPriceForUnusedPeriod - currentEditionPriceForUnusedPeriod;
        }

        public async Task<Tenant> UpdateTenantAsync(int tenantId, bool isActive, bool? isInTrialPeriod, PaymentPeriodType? paymentPeriodType, int editionId, EditionPaymentType editionPaymentType)
        {
            var tenant = await FindByIdAsync(tenantId);

            tenant.IsActive = isActive;

            if (isInTrialPeriod.HasValue)
            {
                tenant.IsInTrialPeriod = isInTrialPeriod.Value;
            }

            tenant.EditionId = editionId;

            if (paymentPeriodType.HasValue)
            {
                tenant.UpdateSubscriptionDateForPayment(paymentPeriodType.Value, editionPaymentType);
            }

            return tenant;
        }

        public async Task<EndSubscriptionResult> EndSubscriptionAsync(Tenant tenant, SubscribableEdition edition, DateTime nowUtc)
        {
            if (tenant.EditionId == null || tenant.HasUnlimitedTimeSubscription())
            {
                throw new Exception($"Can not end tenant {tenant.TenancyName} subscription for {edition.DisplayName} tenant has unlimited time subscription!");
            }

            Debug.Assert(tenant.SubscriptionEndDateUtc != null, "tenant.SubscriptionEndDateUtc != null");

            var subscriptionEndDateUtc = tenant.SubscriptionEndDateUtc.Value;
            if (!tenant.IsInTrialPeriod)
            {
                subscriptionEndDateUtc = tenant.SubscriptionEndDateUtc.Value.AddDays(edition.WaitingDayAfterExpire ?? 0);
            }

            if (subscriptionEndDateUtc >= nowUtc)
            {
                throw new Exception($"Can not end tenant {tenant.TenancyName} subscription for {edition.DisplayName} since subscription has not expired yet!");
            }

            if (!tenant.IsInTrialPeriod && edition.ExpiringEditionId.HasValue)
            {
                tenant.EditionId = edition.ExpiringEditionId.Value;
                tenant.SubscriptionEndDateUtc = null;

                await UpdateAsync(tenant);

                return EndSubscriptionResult.AssignedToAnotherEdition;
            }

            tenant.IsActive = false;
            tenant.IsInTrialPeriod = false;

            await UpdateAsync(tenant);

            return EndSubscriptionResult.TenantSetInActive;
        }

        public override Task UpdateAsync(Tenant tenant)
        {
            if (tenant.IsInTrialPeriod && !tenant.SubscriptionEndDateUtc.HasValue)
            {
                throw new UserFriendlyException(LocalizationManager.GetString(ePayConsts.LocalizationSourceName, "TrialWithoutEndDateErrorMessage"));
            }

            return base.UpdateAsync(tenant);
        }
    }
}
