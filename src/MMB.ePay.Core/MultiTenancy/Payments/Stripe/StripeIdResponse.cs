namespace MMB.ePay.MultiTenancy.Payments.Stripe
{
    public class StripeIdResponse
    {
        public string Id { get; set; }
    }
}