﻿using Abp.Domain.Repositories;
using System;
using System.Threading.Tasks;

namespace MMB.ePay
{
    public interface IProcessRepository : IRepository<ProcessBatch, int>
    {
        /// <summary>
        /// Initialize session for processing all employees
        /// </summary>
        /// <param name="processSessionId"></param>
        /// <param name="tenantId"></param>
        Task InitializeProcessSession(Guid processSessionId, int tenantId);

        /// <summary>
        /// Initialize session for processing single employee
        /// </summary>
        /// <param name="processSessionId"></param>
        /// <param name="tenantId"></param>
        /// <param name="employeeId">Employee id to process</param>
        Task InitializeProcessSession(Guid processSessionId, int tenantId, int employeeId);

        Task<Guid?> GetProcessBatch(Guid processSessionId);

         
    }
}
