﻿using Abp.Domain.Services;

namespace MMB.ePay
{
    public abstract class ePayDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected ePayDomainServiceBase()
        {
            LocalizationSourceName = ePayConsts.LocalizationSourceName;
        }
    }
}
