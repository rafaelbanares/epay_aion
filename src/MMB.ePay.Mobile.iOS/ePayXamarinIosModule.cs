﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace MMB.ePay
{
    [DependsOn(typeof(ePayXamarinSharedModule))]
    public class ePayXamarinIosModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ePayXamarinIosModule).GetAssembly());
        }
    }
}