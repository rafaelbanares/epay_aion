﻿namespace MMB.ePay.HR
{
    public class GroupSettingsConsts
    {
        public const int MinSysCodeLength = 0;
        public const int MaxSysCodeLength = 20;

        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

    }
}