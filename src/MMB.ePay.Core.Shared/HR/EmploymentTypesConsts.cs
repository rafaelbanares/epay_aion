﻿namespace MMB.ePay.HR
{
    public class EmploymentTypesConsts
    {
        public const int MinSysCodeLength = 0;
        public const int MaxSysCodeLength = 20;

        public const int MinDescriptionLength = 0;
        public const int MaxDescriptionLength = 100;
    }
}