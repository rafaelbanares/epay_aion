﻿namespace MMB.ePay.HR
{
    public class EmployeesConsts
    {

        public const int MinEmployeeCodeLength = 0;
        public const int MaxEmployeeCodeLength = 15;

        public const int MinLastNameLength = 0;
        public const int MaxLastNameLength = 150;

        public const int MinFirstNameLength = 0;
        public const int MaxFirstNameLength = 150;

        public const int MinMiddleNameLength = 0;
        public const int MaxMiddleNameLength = 150;

        public const int MinEmailLength = 0;
        public const int MaxEmailLength = 256;

        public const int MinAddressLength = 0;
        public const int MaxAddressLength = 500;

        public const int MinPositionLength = 0;
        public const int MaxPositionLength = 50;

        public const int MinGenderLength = 0;
        public const int MaxGenderLength = 1;

        public const int MinEmployeeStatusLength = 0;
        public const int MaxEmployeeStatusLength = 8;

        public const int MinRankLength = 0;
        public const int MaxRankLength = 3;
    }
}