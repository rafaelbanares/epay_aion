﻿namespace MMB.ePay.HR
{
    public class CompaniesConsts
    {
        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

        public const int MinAddress1Length = 0;
        public const int MaxAddress1Length = 50;

        public const int MinAddress2Length = 0;
        public const int MaxAddress2Length = 50;

        public const int MinOrgUnitLevel1NameLength = 0;
        public const int MaxOrgUnitLevel1NameLength = 100;

        public const int MinOrgUnitLevel2NameLength = 0;
        public const int MaxOrgUnitLevel2NameLength = 100;

        public const int MinOrgUnitLevel3NameLength = 0;
        public const int MaxOrgUnitLevel3NameLength = 100;
    }
}
