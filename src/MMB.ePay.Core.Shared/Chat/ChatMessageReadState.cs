﻿namespace MMB.ePay.Chat
{
    public enum ChatMessageReadState
    {
        Unread = 1,

        Read = 2
    }
}