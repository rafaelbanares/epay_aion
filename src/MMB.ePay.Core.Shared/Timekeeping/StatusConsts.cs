﻿namespace MMB.ePay.Timekeeping
{
    public class StatusConsts
    {
        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

        public const int MinAfterStatusMessageLength = 0;
        public const int MaxAfterStatusMessageLength = 100;

        public const int MinBeforeStatusActionTextLength = 0;
        public const int MaxBeforeStatusActionTextLength = 100;
    }
}