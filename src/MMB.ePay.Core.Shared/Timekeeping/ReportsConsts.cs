﻿namespace MMB.ePay.Timekeeping
{
    public class ReportsConsts
    {

        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

        public const int MinControllerNameLength = 0;
        public const int MaxControllerNameLength = 50;

        public const int MinStoredProcedureLength = 0;
        public const int MaxStoredProcedureLength = 50;

        public const int MinFormatLength = 0;
        public const int MaxFormatLength = 10;

        public const int MinOutputLength = 0;
        public const int MaxOutputLength = 1;

        public const int MinExportFileNameLength = 0;
        public const int MaxExportFileNameLength = 50;
    }
}