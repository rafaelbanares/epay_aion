﻿namespace MMB.ePay.Timekeeping
{
    public class PostedAttendancesConsts
    {

        public const int MinRestDayCodeLength = 0;
        public const int MaxRestDayCodeLength = 7;

        public const int MinRemarksLength = 0;
        public const int MaxRemarksLength = 100;

        //public const int MinTagsLength = 0;
        //public const int MaxTagsLength = 50;

    }
}