﻿namespace MMB.ePay.Timekeeping
{
    public class RestDayApprovalsConsts
    {

        public const int MinRemarksLength = 0;
        public const int MaxRemarksLength = 200;

    }
}