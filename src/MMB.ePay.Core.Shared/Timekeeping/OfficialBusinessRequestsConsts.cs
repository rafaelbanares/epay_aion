﻿namespace MMB.ePay.Timekeeping
{
    public class OfficialBusinessRequestsConsts
    {

        public const int MinRemarksLength = 0;
        public const int MaxRemarksLength = 500;

    }
}