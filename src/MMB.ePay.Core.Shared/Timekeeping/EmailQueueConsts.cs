﻿namespace MMB.ePay.Timekeeping
{
    public class EmailQueueConsts
    {
        public const int MinSubjectLength = 0;
        public const int MaxSubjectLength = 100;

        public const int MinRequestLength = 0;
        public const int MaxRequestLength = 20;

        public const int MinRemarksLength = 0;
        public const int MaxRemarksLength = 200;

        public const int MinReasonLength = 0;
        public const int MaxReasonLength = 200;

        public const int MinFullNameLength = 0;
        public const int MaxFullNameLength = 450;

        public const int MinApprovalStatusLength = 0;
        public const int MaxApprovalStatusLength = 50;

        public const int MinRecipientLength = 0;
        public const int MaxRecipientLength = 256;

        public const int MinStatusLength = 0;
        public const int MaxStatusLength = 1;

        public const int MinStatusErrorLength = 0;
        public const int MaxStatusErrorLength = 500;
    }
}