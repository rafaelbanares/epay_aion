﻿namespace MMB.ePay.Timekeeping
{
    public class AttendanceUploadsConsts
    {
        public const int MinTimeInLength = 0;
        public const int MaxTimeInLength = 10;

        public const int MinTimeOutLength = 0;
        public const int MaxTimeOutLength = 10;

        public const int MinLunchBreaktimeInLength = 0;
        public const int MaxLunchBreaktimeInLength = 10;

        public const int MinLunchBreaktimeOutLength = 0;
        public const int MaxLunchBreaktimeOutLength = 10;

        public const int MinPMBreaktimeInLength = 0;
        public const int MaxPMBreaktimeInLength = 10;

        public const int MinPMBreaktimeOutLength = 0;
        public const int MaxPMBreaktimeOutLength = 10;
    }
}