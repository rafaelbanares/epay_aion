﻿namespace MMB.ePay.Timekeeping
{
    public class HolidayTypesConsts
    {

        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

        public const int MinDescriptionLength = 0;
        public const int MaxDescriptionLength = 100;

        public const int MinHolidayCodeLength = 0;
        public const int MaxHolidayCodeLength = 3;

        public static readonly string[] StandardHolidayCodes = { "L", "s", "LS", "D", "C", "R" };

    }
}