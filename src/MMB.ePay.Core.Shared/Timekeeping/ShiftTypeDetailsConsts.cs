﻿namespace MMB.ePay.Timekeeping
{
    public class ShiftTypeDetailsConsts
    {

        public const int MinDaysLength = 0;
        public const int MaxDaysLength = 7;

        public const int MinTimeInLength = 0;
        public const int MaxTimeInLength = 5;

        public const int MinBreaktimeInLength = 0;
        public const int MaxBreaktimeInLength = 5;

        public const int MinBreaktimeOutLength = 0;
        public const int MaxBreaktimeOutLength = 5;

        public const int MinTimeOutLength = 0;
        public const int MaxTimeOutLength = 5;

    }
}