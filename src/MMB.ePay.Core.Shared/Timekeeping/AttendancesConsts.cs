﻿namespace MMB.ePay.Timekeeping
{
    public class AttendancesConsts
    {

        public const int MinRestDayCodeLength = 0;
        public const int MaxRestDayCodeLength = 7;

        //public const int MinDayTypeLength = 1;
        //public const int MaxDayTypeLength = 3;

        public const int MinRemarksLength = 0;
        public const int MaxRemarksLength = 100;

        public const int MinTagsLength = 0;
        public const int MaxTagsLength = 50;

    }
}