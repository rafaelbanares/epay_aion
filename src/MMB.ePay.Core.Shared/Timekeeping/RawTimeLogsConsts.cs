﻿namespace MMB.ePay.Timekeeping
{
    public class RawTimeLogsConsts
    {

        public const int MinSwipeCodeLength = 0;
        public const int MaxSwipeCodeLength = 20;

        public const int MinInOutLength = 0;
        public const int MaxInOutLength = 1;

        public const int MinDeviceLength = 0;
        public const int MaxDeviceLength = 1;
    }
}