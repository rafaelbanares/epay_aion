﻿namespace MMB.ePay.Timekeeping
{
    public class ExcuseRequestsConsts
    {

        public const int MinRemarksLength = 0;
        public const int MaxRemarksLength = 200;

    }
}