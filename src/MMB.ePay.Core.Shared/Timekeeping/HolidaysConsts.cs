﻿namespace MMB.ePay.Timekeeping
{
    public class HolidaysConsts
    {

        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;
    }
}