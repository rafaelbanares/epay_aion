﻿namespace MMB.ePay.Timekeeping
{
    public class LocationsConsts
    {

        public const int MinDescriptionLength = 0;
        public const int MaxDescriptionLength = 100;
    }
}