﻿namespace MMB.ePay.Timekeeping
{
    public class OfficialBusinessApprovalsConsts
    {

        public const int MinRemarksLength = 0;
        public const int MaxRemarksLength = 100;

    }
}