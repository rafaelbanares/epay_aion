﻿using System.Collections.Generic;

namespace MMB.ePay.Timekeeping
{
    public class OvertimeTypesConsts
    {
        public const int MinShortNameLength = 0;
        public const int MaxShortNameLength = 50;

        public const int MinDescriptionLength = 0;
        public const int MaxDescriptionLength = 100;

        public const int MinOTCodeLength = 0;
        public const int MaxOTCodeLength = 10;

        public readonly static string[] StandardOTCodes = { "LGL", "LGL+RST", "LGL+SPL", "LGL2", "REG", "RST", "SPL", "SPL+RST" };

        public readonly static Dictionary<string, string> OTCodeDictionary = new Dictionary<string, string>
        {
            { "LGL", "Legal" },
            { "LGL+RST", "Legal Rest Day" },
            { "LGL+SPL", "Legal Special Day" },
            { "LGL2", "Double Legal" },
            { "REG", "Regular" },
            { "RST", "Rest Day" },
            { "SPL", "Special" },
            { "SPL+RST", "Special Rest Day" },
        };
    }
}
