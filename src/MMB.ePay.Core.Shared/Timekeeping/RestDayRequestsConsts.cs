﻿namespace MMB.ePay.Timekeeping
{
    public class RestDayRequestsConsts
    {

        public const int MinRestDayCodeLength = 0;
        public const int MaxRestDayCodeLength = 7;

        public const int MinRemarksLength = 0;
        public const int MaxRemarksLength = 200;

    }
}