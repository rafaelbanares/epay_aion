﻿namespace MMB.ePay.Timekeeping
{
    public class ExcuseApprovalsConsts
    {

        public const int MinRemarksLength = 0;
        public const int MaxRemarksLength = 200;

    }
}