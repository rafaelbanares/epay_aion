﻿namespace MMB.ePay.Timekeeping
{
    public class EmployeeTimeInfoConsts
    {

        public const int MinRestDayLength = 0;
        public const int MaxRestDayLength = 7;

        public const int MinSwipeCodeLength = 0;
        public const int MaxSwipeCodeLength = 20;
    }
}