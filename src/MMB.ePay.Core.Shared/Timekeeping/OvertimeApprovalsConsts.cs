﻿namespace MMB.ePay.Timekeeping
{
    public class OvertimeApprovalsConsts
    {

        public const int MinRemarksLength = 0;
        public const int MaxRemarksLength = 200;

    }
}