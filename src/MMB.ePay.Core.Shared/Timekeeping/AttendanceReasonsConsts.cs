﻿namespace MMB.ePay.Timekeeping
{
    public class AttendanceReasonsConsts
    {

        public const int MinDisplayNameLength = 0;
        public const int MaxDisplayNameLength = 50;

        public const int MinDescriptionLength = 0;
        public const int MaxDescriptionLength = 100;

    }
}