﻿namespace MMB.ePay.MultiTenancy.Payments
{
    public abstract class CreatePaymentResponse
    {
        public abstract string GetId();
    }
}