﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace MMB.ePay
{
    [DependsOn(typeof(ePayClientModule), typeof(AbpAutoMapperModule))]
    public class ePayXamarinSharedModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Localization.IsEnabled = false;
            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ePayXamarinSharedModule).GetAssembly());
        }
    }
}