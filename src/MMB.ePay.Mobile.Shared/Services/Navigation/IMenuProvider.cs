using System.Collections.Generic;
using MvvmHelpers;
using MMB.ePay.Models.NavigationMenu;

namespace MMB.ePay.Services.Navigation
{
    public interface IMenuProvider
    {
        ObservableRangeCollection<NavigationMenuItem> GetAuthorizedMenuItems(Dictionary<string, string> grantedPermissions);
    }
}