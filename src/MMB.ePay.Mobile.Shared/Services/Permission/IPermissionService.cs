﻿namespace MMB.ePay.Services.Permission
{
    public interface IPermissionService
    {
        bool HasPermission(string key);
    }
}