﻿using Abp.Application.Services;
using MMB.ePay.Dto;
using MMB.ePay.Logging.Dto;

namespace MMB.ePay.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}
