﻿using System.Collections.Generic;

namespace MMB.ePay.Logging.Dto
{
    public class GetLatestWebLogsOutput
    {
        public List<string> LatestWebLogLines { get; set; }
    }
}
