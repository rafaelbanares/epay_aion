﻿using System;

namespace MMB.ePay.Processor.Dtos
{
    public class DateRange
    {
        public DateRange() { }

        public DateRange(DateTime start, DateTime end)
        {
            if (start > end) throw new Exception("Invalid date range.");

            Start = start;
            End = end;
        }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }
    }
}
