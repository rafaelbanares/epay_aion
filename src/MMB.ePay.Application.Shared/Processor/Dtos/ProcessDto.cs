﻿using System;

namespace MMB.ePay.Processor.Dtos
{
    //public class ProcessDto
    //{
    //    public int TenantId { get; set; }

    //    public int? EmployeeId { get; set; }

    //    public DateTime Start { get; set; }

    //    public DateTime End { get; set; }
    //}

    public class ProcessDto
    {
        public Guid ProcSessionId { get; set; }

        public int TenantId { get; set; }

        public int? EmployeeId { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }
    }

    //public class InitializeProcSessionDto
    //{
    //    public Guid AppSessionId { get; set; }
    //    //public int AttendancePeriodId { get; set; }
    //    public int TenantId { get; set; }
    //    public int? EmployeeId { get; set; }
    //}
}
