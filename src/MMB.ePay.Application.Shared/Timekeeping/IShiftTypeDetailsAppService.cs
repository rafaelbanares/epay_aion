﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IShiftTypeDetailsAppService : IApplicationService
    {
        Task<PagedResultDto<GetShiftTypeDetailsForViewDto>> GetAll(GetAllShiftTypeDetailsInput input);

        Task<GetShiftTypeDetailsForViewDto> GetShiftTypeDetailsForView(int id);

        Task<GetShiftTypeDetailsForEditOutput> GetShiftTypeDetailsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditShiftTypeDetailsDto input);

        Task<bool> Delete(int id);

    }
}