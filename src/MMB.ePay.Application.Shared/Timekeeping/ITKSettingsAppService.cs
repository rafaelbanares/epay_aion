﻿using Abp.Application.Services;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface ITKSettingsAppService : IApplicationService
    {
        Task<int> EmailDelay();

        Task<string> Geolocation();

        Task<bool> EnableEmailNotification();

        Task<bool> MinuteFormat();

        Task<bool> HasPMBreak();

        Task<GetTKSettingsForEditOutput> GetTKSettingsForEdit();

        Task CreateOrEdit(CreateOrEditTKSettingsDto input);
    }
}