﻿using Abp.Application.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IOfficialBusinessReasonsAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetOfficialBusinessReasonList();
    }
}