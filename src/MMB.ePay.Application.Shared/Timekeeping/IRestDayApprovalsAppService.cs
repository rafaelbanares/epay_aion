﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IRestDayApprovalsAppService : IApplicationService
    {
        Task<PagedResultDto<GetRestDayApprovalsForViewDto>> GetAll(GetAllRestDayApprovalsInput input);

        Task<PagedResultDto<GetRestDayApprovalsForViewDto>> GetAdminApprovals(GetAllRestDayApprovalsInput input);

        Task<PagedResultDto<GetRestDayApprovalsForViewDto>> GetApprovalRequests(GetAllRestDayApprovalsInput input);

        Task<GetRestDayApprovalsForViewDto> GetRestDayApprovalsForView(int id);


        Task ApproveAll(ApproveAllRestDayRequests input);

        Task UpdateStatus(UpdateRestDayRequestsStatus input);

        Task AdminUpdateStatus(UpdateRestDayRequestsStatus input);
    }
}