﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IAttendanceRequestsAppService : IApplicationService
    {
        Task<PagedResultDto<GetAttendanceRequestsForViewDto>> GetAll(GetAllAttendanceRequestsInput input);
        Task<GetAttendanceRequestsForViewDto> GetAttendanceRequestsForView(int id);
        Task<GetAttendanceRequestsForEditOutput> GetAttendanceRequestsForCreate(int id);
        Task<GetAttendanceRequestsForEditOutput> GetAttendanceRequestsForEdit(int id);
        Task CreateOrEdit(CreateOrEditAttendanceRequestsDto input);
        Task UpdateStatus(UpdateAttendanceRequestsStatus input);
    }
}