﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface ILeaveEarningsAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetAppYearListForFilter();

        Task<PagedResultDto<GetLeaveEarningsForViewDto>> GetAll(GetAllLeaveEarningsInput input);

        Task<PagedResultDto<GetLeaveEarningDetailsForViewDto>> GetLeaveEarningDetails(GetAllLeaveEarningDetailsInput input);

        Task<PagedResultDto<GetLeaveBalancesForViewDto>> GetLeaveBalances(GetAllLeaveBalancesInput input);

        Task<PagedResultDto<GetAdminLeaveBalancesForViewDto>> GetAdminLeaveBalances(GetAdminLeaveBalancesInput input);

        Task<GetLeaveEarningsForViewDto> GetLeaveEarningsForView(int id);

        Task<GetLeaveBalancesForViewDto> GetLeaveBalancesForView(int id, int employeeId, int year);

        Task<GetLeaveEarningsForEditOutput> GetLeaveEarningsForEdit(int id);

        Task UpdateByFile(UploadLeaveEarningsDto input);

        Task CreateOrEdit(CreateOrEditLeaveEarningsDto input);

        Task Delete(int id);
    }
}