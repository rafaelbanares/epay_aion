﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IOvertimeRequestsAppService : IApplicationService
    {
        Task<PagedResultDto<GetOvertimeRequestsForViewDto>> GetAll(GetAllOvertimeRequestsInput input);

        Task<GetOvertimeRequestsForViewDto> GetOvertimeRequestsForView(int id);

        Task<GetOvertimeRequestsForEditOutput> GetOvertimeRequestsForEdit(int id);

        Task CreateOrEdit(CreateOrEditOvertimeRequestsDto input);

        Task UpdateStatus(UpdateOvertimeRequestsStatus input);
    }
}