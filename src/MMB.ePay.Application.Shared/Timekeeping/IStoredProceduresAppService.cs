﻿using Abp.Application.Services;
using MMB.ePay.Timekeeping.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IStoredProceduresAppService : IApplicationService
    {
        Task<List<string>> GetUserNames();
        Task<int> InitializeProcSession(InitializeProcSessionDto input);
        Task<int> PostAttendance(PostAttendanceDto input);
        Task<Guid> GetProcessBatch(Guid appSessionId);
        Task<int> DeleteEmail();
        Task<int> DeleteAuditLogs();
        Task<int> DeleteProcess(DeleteProcessDto input);
        Task ProcessRawTimeLogStaging(Guid uploadSessionId);
        Task ProcessSessionCleanup(Guid appSessionId);
    }
}
