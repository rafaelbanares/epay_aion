﻿namespace MMB.ePay.Timekeeping.Enums
{
    public enum StatusDisplayNames
    {
        New,
        ForApproval,
        ForNextApproval,
        Declined,
        Cancelled,
        Approved,
        Reopened,
        ForUpdate
        //SystemApproved
    }
}
