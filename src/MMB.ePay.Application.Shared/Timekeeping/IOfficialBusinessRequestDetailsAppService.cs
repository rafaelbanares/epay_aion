﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IOfficialBusinessRequestDetailsAppService : IApplicationService
    {
        Task<GetOfficialBusinessRequestDetailsForViewDto> GetOfficialBusinessRequestDetailsForView(int id);
        Task<GetOfficialBusinessRequestDetailsForEditOutput> GetOfficialBusinessRequestDetailsForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditOfficialBusinessRequestDetailsDto input);
        Task Delete(int id);
    }
}