﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IRestDayRequestsAppService : IApplicationService
    {
        Task<PagedResultDto<GetRestDayRequestsForViewDto>> GetAll(GetAllRestDayRequestsInput input);

        Task<GetRestDayRequestsForViewDto> GetRestDayRequestsForView(int id);

        Task<GetRestDayRequestsForEditOutput> GetRestDayRequestsForEdit(int id);

        Task CreateOrEdit(CreateOrEditRestDayRequestsDto input);

        Task UpdateStatus(UpdateRestDayRequestsStatus input);
    }
}