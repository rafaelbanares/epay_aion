﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IEmailQueueAppService : IApplicationService
    {
        Task<DateTime?> GetOldestNewEmailQueue();
        Task<PagedResultDto<GetEmailQueueForViewDto>> GetAll(GetAllEmailQueueInput input);
        Task<GetEmailQueueForViewDto> GetEmailQueueForView(int id);
        Task<IList<EmailQueueDto>> GetPendingAsync();
        Task UpdateStatusAsync(UpdateStatusEmailQueueDto input);
        Task CreateOrEdit(CreateOrEditEmailQueueDto input);
        Task DeleteAll(DeleteEmailQueueDto input);
    }
}