﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface ILeaveApprovalsAppService : IApplicationService
    {
        Task<PagedResultDto<GetLeaveApprovalsForViewDto>> GetAll(GetAllLeaveApprovalsInput input);
        Task<PagedResultDto<GetLeaveApprovalsForViewDto>> GetAdminApprovals(GetAllLeaveApprovalsInput input);
        Task<PagedResultDto<GetLeaveApprovalsForViewDto>> GetApprovalRequests(GetAllLeaveApprovalsInput input);
        Task<GetLeaveApprovalsForViewDto> GetLeaveApprovalsForView(int id);
        Task ApproveAll(ApproveAllLeaveRequests input);
        Task UpdateStatus(UpdateLeaveRequestsStatus input);
        Task AdminUpdateStatus(UpdateLeaveRequestsStatus input);
    }
}