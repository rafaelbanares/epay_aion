﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface ILeaveRequestDetailsAppService : IApplicationService
    {
        Task<GetLeaveRequestDetailsForViewDto> GetLeaveRequestDetailsForView(int id);
        Task<GetLeaveRequestDetailsForEditOutput> GetLeaveRequestDetailsForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditLeaveRequestDetailsDto input);
        Task Delete(int id);
    }
}