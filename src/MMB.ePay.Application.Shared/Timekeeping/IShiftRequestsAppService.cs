﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IShiftRequestsAppService : IApplicationService
    {
        Task<PagedResultDto<GetShiftRequestsForViewDto>> GetAll(GetAllShiftRequestsInput input);

        Task<GetShiftRequestsForViewDto> GetShiftRequestsForView(int id);

        Task<GetShiftRequestsForEditOutput> GetShiftRequestsForEdit(int id);

        Task CreateOrEdit(CreateOrEditShiftRequestsDto input);

        Task UpdateStatus(UpdateShiftRequestsStatus input);
    }
}