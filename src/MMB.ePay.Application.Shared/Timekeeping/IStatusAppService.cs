﻿using Abp.Application.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Timekeeping.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IStatusAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetStatusListForFilter();
        Task<GetStatusForDisplayNameDto> GetStatusDisplayName(int id);
        Task<GetStatusForViewDto> GetInitialStatus();
        Task<IList<GetStatusForViewDto>> GetStatusByDisplayNames(params StatusDisplayNames[] names);
        Task<GetStatusForEditOutput> GetStatusForEdit(int id);
    }
}