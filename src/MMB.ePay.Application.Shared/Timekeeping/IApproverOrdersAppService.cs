﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IApproverOrdersAppService : IApplicationService
    {
        Task<IList<string>> GetApproverNameList();
        Task<IList<SelectListItem>> GetApproverOrderList();
        Task<PagedResultDto<GetApproverOrdersForViewDto>> GetAll(GetAllApproverOrdersInput input);
        Task<GetApproverOrdersForViewDto> GetApproverOrdersForView(int id);
        Task<GetApproverOrdersForEditOutput> GetApproverOrdersForEdit(int id);
        Task CreateOrEdit(CreateOrEditApproverOrdersDto input);
        Task Delete(EntityDto input);
    }
}