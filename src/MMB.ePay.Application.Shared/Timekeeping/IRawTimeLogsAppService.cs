﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IRawTimeLogsAppService : IApplicationService
    {
        Task<PagedResultDto<GetRawTimeLogsForViewDto>> GetAll(GetAllRawTimeLogsInput input);
        Task<GetRawTimeLogsForViewDto> GetRawTimeLogsForView(int id);
        Task<GetRawTimeLogsForAttendancesDto> GetRawTimeLogsForAttendances(int id);
        Task<GetRawTimeLogsForEditOutput> GetRawTimeLogsForEdit(int id);
        Task CreateOrEdit(CreateOrEditRawTimeLogsDto input);
        Task Delete(int id);
    }
}