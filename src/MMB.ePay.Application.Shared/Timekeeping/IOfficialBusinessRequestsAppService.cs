﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IOfficialBusinessRequestsAppService : IApplicationService
    {
        Task<PagedResultDto<GetOfficialBusinessRequestsForViewDto>> GetAll(GetAllOfficialBusinessRequestsInput input);

        Task<GetOfficialBusinessRequestsForViewDto> GetOfficialBusinessRequestsForView(int id);

        Task<GetOfficialBusinessRequestsForEditOutput> GetOfficialBusinessRequestsForEdit(int id);

        Task CreateOrEdit(CreateOrEditOfficialBusinessRequestsDto input);

        Task UpdateStatus(UpdateOfficialBusinessRequestsStatus input);
    }
}