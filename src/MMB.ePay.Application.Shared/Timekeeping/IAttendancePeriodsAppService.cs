﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Processor.Dtos;
using MMB.ePay.Timekeeping.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IAttendancePeriodsAppService : IApplicationService
    {
        Task<bool> CheckUnpostedPeriodByStartDate(DateTime start);

        Task<DateTime?> GetMinUnpostedDate();

        Task<DateTime> GetLatestPostedDate(int? frequencyId);

        Task<int> GetFirstUnpostedPeriodId(int frequencyId);

        Task<DateRange> GetUnpostedDateRangeFromFrequencyId(int frequencyId);

        Task<DateRange> GetDateRangeFromAttendancePeriod(int id);

        Task<IList<SelectListItem>> GetAttendancePeriodListByYearForTimesheet(int year, int? frequencyId, bool? isPosted);

        Task<IList<SelectListItem>> GetUnpostedAttendancePeriodList(int frequencyId);

        Task<IList<SelectListItem>> GetAttendancePeriodListByEmployeeId(int year, int? employeeId, bool? isPosted);

        Task<IList<SelectListItem>> GetAttendancePeriodListByYear(int year, int? frequencyId, bool? isPosted);

        Task<IList<SelectListItem>> GetYearListForFilter();

        Task<PagedResultDto<GetAttendancePeriodsForViewDto>> GetAll(GetAllAttendancePeriodsInput input);

        Task<GetAttendancePeriodsForViewDto> GetAttendancePeriodsForView(int id);

        Task<GetAttendancePeriodsForEditOutput> GetAttendancePeriodsForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditAttendancePeriodsDto input);

        Task Delete(EntityDto input);

    }
}