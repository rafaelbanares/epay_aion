﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IAttendancesAppService : IApplicationService
    {
        Task<IList<InvalidAttendancesDto>> GetInvalidAttendanceList(int periodId);

        //IList<SelectListItem> GetBiometricList();

        Task UploadRawTimeLogStaging(CreateRawTimeLogStagingDto input);

        Task<AttendancesUploadDto> UploadStaging(AttendancesUploadDto input);

        Task<PagedResultDto<GetAttendancesForViewDto>> GetAll(GetAllAttendancesInput input);

        Task<GetAttendancesForViewDto> GetAttendancesForView(int id, bool isPosted);

        Task<GetAttendancesForEditOutput> GetAttendancesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditAttendancesDto input);

        Task Delete(EntityDto input);

    }
}