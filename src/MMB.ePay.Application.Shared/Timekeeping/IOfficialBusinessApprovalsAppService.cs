﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IOfficialBusinessApprovalsAppService : IApplicationService
    {
        Task<PagedResultDto<GetOfficialBusinessApprovalsForViewDto>> GetAll(GetAllOfficialBusinessApprovalsInput input);
        Task<PagedResultDto<GetOfficialBusinessApprovalsForViewDto>> GetAdminApprovals(GetAllOfficialBusinessApprovalsInput input);
        Task<PagedResultDto<GetOfficialBusinessApprovalsForViewDto>> GetApprovalRequests(GetAllOfficialBusinessApprovalsInput input);
        Task<GetOfficialBusinessApprovalsForViewDto> GetOfficialBusinessApprovalsForView(int id);
        Task ApproveAll(ApproveAllOfficialBusinessRequests input);
        Task UpdateStatus(UpdateOfficialBusinessRequestsStatus input);
        Task AdminUpdateStatus(UpdateOfficialBusinessRequestsStatus input);
    }
}