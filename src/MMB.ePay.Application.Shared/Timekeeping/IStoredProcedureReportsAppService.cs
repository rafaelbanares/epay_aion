﻿using Abp.Application.Services;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IStoredProcedureReportsAppService : IApplicationService
    {
        Task<GetReportsForAttendanceSummary> GetAttendanceSummary(ReportsDto dto);

        Task<GetReportsForAttendanceSummaryPayroll> GetAttendanceSummaryPayroll(ReportsDto dto);

        Task<GetReportsForOvertimeSummary> GetOvertimeSummary(ReportsDto dto);

        Task<GetReportsForOvertimeSummaryPayroll> GetOvertimeSummaryPayroll(ReportsDto dto);

        Task<GetReportsForOvertimeTransaction> GetOvertimeTransaction(ReportsDto dto);
    }
}
