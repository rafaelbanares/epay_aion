﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface ILeaveTypesAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetLeaveTypeList();
        Task<PagedResultDto<GetLeaveTypesForViewDto>> GetAll(GetAllLeaveTypesInput input);
        Task<GetLeaveTypesForViewDto> GetLeaveTypesForView(int id);
        Task<GetLeaveTypesForEditOutput> GetLeaveTypesForEdit(int id);
        Task CreateOrEdit(CreateOrEditLeaveTypesDto input);
        Task Delete(int id);
    }
}