﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IOvertimeApprovalsAppService : IApplicationService
    {
        Task<PagedResultDto<GetOvertimeApprovalsForViewDto>> GetAll(GetAllOvertimeApprovalsInput input);
        Task<PagedResultDto<GetOvertimeApprovalsForViewDto>> GetAdminApprovals(GetAllOvertimeApprovalsInput input);
        Task<PagedResultDto<GetOvertimeApprovalsForViewDto>> GetApprovalRequests(GetAllOvertimeApprovalsInput input);
        Task<GetOvertimeApprovalsForViewDto> GetOvertimeApprovalsForView(int id);
        Task ApproveAll(ApproveAllOvertimeRequests input);
        Task UpdateStatus(UpdateOvertimeRequestsStatus input);
        Task AdminUpdateStatus(UpdateOvertimeRequestsStatus input);
    }
}