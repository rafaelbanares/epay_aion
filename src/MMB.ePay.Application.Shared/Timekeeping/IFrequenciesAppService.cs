﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IFrequenciesAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetFrequencyList();

        Task<IList<SelectListItem>> GetFrequencyListForFilter();

        Task<PagedResultDto<GetFrequenciesForViewDto>> GetAll(GetAllFrequenciesInput input);

        Task<GetFrequenciesForViewDto> GetFrequenciesForView(int id);

        Task<GetFrequenciesForEditOutput> GetFrequenciesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditFrequenciesDto input);

        Task Delete(EntityDto input);

    }
}