﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IExcuseApprovalsAppService : IApplicationService
    {
        Task<PagedResultDto<GetExcuseApprovalsForViewDto>> GetAll(GetAllExcuseApprovalsInput input);
        Task<PagedResultDto<GetExcuseApprovalsForViewDto>> GetAdminApprovals(GetAllExcuseApprovalsInput input);
        Task<PagedResultDto<GetExcuseApprovalsForViewDto>> GetApprovalRequests(GetAllExcuseApprovalsInput input);
        Task<GetExcuseApprovalsForViewDto> GetExcuseApprovalsForView(int id);
        Task ApproveAll(ApproveAllExcuseRequests input);
        Task UpdateStatus(UpdateExcuseRequestsStatus input);
        Task AdminUpdateStatus(UpdateExcuseRequestsStatus input);
    }
}