﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IHolidayTypesAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetHolidayTypeList();

        Task<PagedResultDto<GetHolidayTypesForViewDto>> GetAll(GetAllHolidayTypesInput input);

        Task<GetHolidayTypesForViewDto> GetHolidayTypesForView(int id);

        Task<GetHolidayTypesForEditOutput> GetHolidayTypesForEdit(EntityDto input);

        Task<bool> CheckHalfDayEnabled(int id);

        Task CreateOrEdit(CreateOrEditHolidayTypesDto input);

        Task Delete(EntityDto input);
    }
}