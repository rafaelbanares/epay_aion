﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IShiftTypesAppService : IApplicationService
    {
        Task<bool> IsGraveyardShift(DateTime date);
        Task<bool> GetStartOnPreviousDayByDate(DateTime date);
        Task<IList<SelectListItem>> GetShiftTypeList();
        Task<PagedResultDto<GetShiftTypesForViewDto>> GetAll(GetAllShiftTypesInput input);
        Task<GetShiftTypesForViewDto> GetShiftTypesForView(int id);
        Task<GetShiftTypesForEditOutput> GetShiftTypesForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditShiftTypesDto input);
        Task Edit(EditShiftTypesDto input);
        Task Delete(int id);
    }
}