﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Processor.Dtos;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface ILeaveRequestsAppService : IApplicationService
    {
        Task<int> GetPendingLeaveRequestCount(DateRange dateRange);

		Task<PagedResultDto<GetLeaveRequestsForViewDto>> GetAll(GetAllLeaveRequestsInput input);

        Task<GetLeaveRequestsForViewDto> GetLeaveRequestsForView(int id);

        Task<GetLeaveRequestsForEditOutput> GetLeaveRequestsForEdit(int id);

        Task CreateOrEdit(CreateOrEditLeaveRequestsDto input);

        Task UpdateStatus(UpdateLeaveRequestsStatus input);
    }
}