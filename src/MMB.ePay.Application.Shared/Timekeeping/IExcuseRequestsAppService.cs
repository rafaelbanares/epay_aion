﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IExcuseRequestsAppService : IApplicationService
    {
        Task<PagedResultDto<GetExcuseRequestsForViewDto>> GetAll(GetAllExcuseRequestsInput input);

        Task<GetExcuseRequestsForViewDto> GetExcuseRequestsForView(int id);

        Task<GetExcuseRequestsForEditOutput> GetExcuseRequestsForEdit(int id);

        Task CreateOrEdit(CreateOrEditExcuseRequestsDto input);

        Task UpdateStatus(UpdateExcuseRequestsStatus input);
    }
}