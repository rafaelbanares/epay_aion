﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IHangfireAppService : IApplicationService
    {
        PagedResultDto<GetHangfireForViewDto> GetAll();

        GetHangfireForViewDto GetHangfireForView(string id);

        void Delete(string id);
    }
}