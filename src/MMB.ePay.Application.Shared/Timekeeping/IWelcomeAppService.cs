﻿using Abp.Application.Services;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IWelcomeAppService : IApplicationService
    {
        Task<GetWelcomeForViewDto> GetWelcomeForView();
    }
}