﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface ILocationsAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetLocationList();

        Task<PagedResultDto<GetLocationsForViewDto>> GetAll(GetAllLocationsInput input);

        Task<GetLocationsForViewDto> GetLocationsForView(int id);

        Task<GetLocationsForEditOutput> GetLocationsForEdit(int id);

        Task CreateOrEdit(CreateOrEditLocationsDto input);

        Task Delete(int id);

    }
}
