﻿using Abp.Application.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IWorkFlowAppService : IApplicationService
    {
        Task<List<SelectListItem>> GetWorkFlowStatusList(int statusId);
        Task<GetWorkFlowForViewDto> GetWorkFlowForView(int id);
    }
}