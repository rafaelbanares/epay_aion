﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllEmailQueueInput : PagedAndSortedResultRequestDto
    {
        public int? TenantFilter { get; set; }
    }
}