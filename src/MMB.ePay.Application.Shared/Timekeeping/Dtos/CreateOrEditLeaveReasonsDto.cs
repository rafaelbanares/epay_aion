﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditLeaveReasonsDto : EntityDto<int?>
    {

        [Required]
        [StringLength(LeaveReasonsConsts.MaxDisplayNameLength, MinimumLength = LeaveReasonsConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [StringLength(LeaveReasonsConsts.MaxDescriptionLength, MinimumLength = LeaveReasonsConsts.MinDescriptionLength)]
        public string Description { get; set; }

    }
}