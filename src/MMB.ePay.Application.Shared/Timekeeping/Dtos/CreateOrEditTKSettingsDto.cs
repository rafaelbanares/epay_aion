﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditTKSettingsDto : EntityDto<int?>
    {
        public CreateOrEditTKSettingsDto()
        {
            IdList = new List<int>();

            ValueList = new List<string>();
        }

        public IList<int> IdList { get; set; }

        public IList<string> ValueList { get; set; }

        //[Required]
        //[StringLength(TKSettingsConsts.MaxKeyLength, MinimumLength = TKSettingsConsts.MinKeyLength)]
        //public string Key { get; set; }

        //[Required]
        //[StringLength(TKSettingsConsts.MaxValueLength, MinimumLength = TKSettingsConsts.MinValueLength)]
        //public string Value { get; set; }

        //[Required]
        //[StringLength(TKSettingsConsts.MaxDataTypeLength, MinimumLength = TKSettingsConsts.MinDataTypeLength)]
        //public string DataType { get; set; }

        //[Required]
        //[StringLength(TKSettingsConsts.MaxCaptionLength, MinimumLength = TKSettingsConsts.MinCaptionLength)]
        //public string Caption { get; set; }

        //[StringLength(TKSettingsConsts.MaxDescriptionLength, MinimumLength = TKSettingsConsts.MinDescriptionLength)]
        //public string Description { get; set; }

        //public int? DisplayOrder { get; set; }

        //[Required]
        //[StringLength(TKSettingsConsts.MaxGroupCodeLength, MinimumLength = TKSettingsConsts.MinGroupCodeLength)]
        //public string GroupCode { get; set; }

    }
}