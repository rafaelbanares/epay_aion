﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllLeaveRequestDetailsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public DateTime? MaxLeaveDateFilter { get; set; }
        public DateTime? MinLeaveDateFilter { get; set; }

        public decimal? MaxDaysFilter { get; set; }
        public decimal? MinDaysFilter { get; set; }

        public int? FirstHalfFilter { get; set; }

        public int? MaxLeaveRequestIdFilter { get; set; }
        public int? MinLeaveRequestIdFilter { get; set; }

    }
}