﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class ExcuseRequestsDto : EntityDto
    {
        public string Remarks { get; set; }

        public DateTime ExcuseDate { get; set; }

        public DateTime ExcuseEnd { get; set; }

        public DateTime ExcuseStart { get; set; }

        public int ExcuseReasonId { get; set; }

        public int StatusId { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }

        public bool IsEditable { get; set; }
    }

    public class ExcuseDetailsDto : EntityDto
    {
        public ExcuseDetailsDto()
        {
            StatusList = new List<SelectListItem>();
        }

        public string Employee { get; set; }
        public DateTime? ExcuseStart { get; set; }
        public DateTime? ExcuseEnd { get; set; }
        public string Reason { get; set; }
        public string Remarks { get; set; }
        public DateTime DateFiled { get; set; }
        public string Status { get; set; }

        public IList<SelectListItem> StatusList { get; set; }
    }
}