﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditAttendancesDto : EntityDto<int?>
    {

        [Required]
        public DateTime Date { get; set; }

        [Required]
        [StringLength(AttendancesConsts.MaxRestDayCodeLength, MinimumLength = AttendancesConsts.MinRestDayCodeLength)]
        public string RestDayCode { get; set; }

        public DateTime? TimeIn { get; set; }

        public DateTime? BreaktimeIn { get; set; }

        public DateTime? BreaktimeOut { get; set; }

        public DateTime? TimeOut { get; set; }

        [Required]
        public decimal Leave_days { get; set; }

        [Required]
        public int Reg_mins { get; set; }

        [Required]
        public int Regnd1_mins { get; set; }

        [Required]
        public int Regnd2_mins { get; set; }

        [Required]
        public int Tardy_mins { get; set; }

        [Required]
        public int UT_mins { get; set; }

        [Required]
        public decimal Absent_days { get; set; }

        [Required]
        public int Excess_mins { get; set; }

        [StringLength(AttendancesConsts.MaxRemarksLength, MinimumLength = AttendancesConsts.MinRemarksLength)]
        public string Remarks { get; set; }

        [StringLength(AttendancesConsts.MaxTagsLength, MinimumLength = AttendancesConsts.MinTagsLength)]
        public string Tags { get; set; }

        public int EmployeeId { get; set; }

        public int ShiftTypeId { get; set; }

    }
}