﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllEmployeeApproversInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}