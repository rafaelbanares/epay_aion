﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetShiftReasonsForViewDto
    {
        public ShiftReasonsDto ShiftReasons { get; set; }

    }
}