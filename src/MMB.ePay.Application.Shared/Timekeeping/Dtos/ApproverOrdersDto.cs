﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class ApproverOrdersDto : EntityDto
    {
        public short OrderNo { get; set; }

        public string DisplayName { get; set; }

        public int Level { get; set; }

        public bool IsBackup { get; set; }

    }
}