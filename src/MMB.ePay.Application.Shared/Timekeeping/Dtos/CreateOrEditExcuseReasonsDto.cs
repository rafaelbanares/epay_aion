﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditExcuseReasonsDto : EntityDto<int?>
    {

        [Required]
        [StringLength(ExcuseReasonsConsts.MaxDisplayNameLength, MinimumLength = ExcuseReasonsConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [StringLength(ExcuseReasonsConsts.MaxDescriptionLength, MinimumLength = ExcuseReasonsConsts.MinDescriptionLength)]
        public string Description { get; set; }

    }
}