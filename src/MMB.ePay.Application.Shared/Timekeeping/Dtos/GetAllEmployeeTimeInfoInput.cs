﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllEmployeeTimeInfoInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string RestDayFilter { get; set; }

        public int? OvertimeFilter { get; set; }

        public int? UndertimeFilter { get; set; }

        public int? TardyFilter { get; set; }

        public int? NonSwiperFilter { get; set; }

        public int? MaxEmployeeIdFilter { get; set; }
        public int? MinEmployeeIdFilter { get; set; }

        public int? MaxShiftTypeIdFilter { get; set; }
        public int? MinShiftTypeIdFilter { get; set; }

    }
}