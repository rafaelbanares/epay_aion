﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllRestDayApprovalsInput : PagedAndSortedResultRequestDto
    {
        public int? AttendancePeriodIdFilter { get; set; }
        public int FrequencyIdFilter { get; set; }
        public int? EmployeeIdFilter { get; set; }
        public bool IsBackup { get; set; }
        public bool StatusTypeIdFilter { get; set; }
    }
}