﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetHangfireForViewDto
    {
        public HangfireDto Hangfire { get; set; }
    }
}
