﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetWelcomeForViewDto
    {
        public WelcomeDto Welcome { get; set; }
    }
}
