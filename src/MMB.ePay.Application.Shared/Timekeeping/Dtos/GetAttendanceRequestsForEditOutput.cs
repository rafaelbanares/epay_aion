﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAttendanceRequestsForEditOutput
    {
        public CreateOrEditAttendanceRequestsDto AttendanceRequests { get; set; }
    }
}