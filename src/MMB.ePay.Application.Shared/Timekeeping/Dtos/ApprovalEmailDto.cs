﻿using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class EmailRequestDto
    {
        public string Recipient { get; set; }
        public string FullName { get; set; }
        public string Request { get; set; }
        public string Reason { get; set; }
        public string Remarks { get; set; }
        public string Email { get; set; }
        public string ApprovalStatus { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }

    public class ApproverDto
    {
        public int Id { get; set; }
        public int FrequencyId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
