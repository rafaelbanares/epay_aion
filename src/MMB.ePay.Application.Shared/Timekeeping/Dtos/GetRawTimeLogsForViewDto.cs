﻿using System.Collections.Generic;

namespace MMB.ePay.Timekeeping.Dtos
{
	public class GetRawTimeLogsForViewDto
	{
		public RawTimeLogsDto RawTimeLogs { get; set; }
	}

	public class GetRawTimeLogsForAttendancesDto
	{
		public GetRawTimeLogsForAttendancesDto()
		{
			RawTimeLogsList = new List<RawTimeLogsDto>();
		}

		public IList<RawTimeLogsDto> RawTimeLogsList { get; set; }
	}
}