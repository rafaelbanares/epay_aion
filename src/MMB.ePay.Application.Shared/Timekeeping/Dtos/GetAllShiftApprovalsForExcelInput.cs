﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllShiftApprovalsForExcelInput
    {
        public string Filter { get; set; }

        public string RemarksFilter { get; set; }

        public int? MaxActedByFilter { get; set; }
        public int? MinActedByFilter { get; set; }

        public int? MaxShiftRequestIdFilter { get; set; }
        public int? MinShiftRequestIdFilter { get; set; }

        public int? MaxStatusIdFilter { get; set; }
        public int? MinStatusIdFilter { get; set; }

    }
}