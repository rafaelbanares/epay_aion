﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllOfficialBusinessRequestDetailsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public DateTime? MaxOfficialBusinessDateFilter { get; set; }
        public DateTime? MinOfficialBusinessDateFilter { get; set; }

        public int? MaxOfficialBusinessRequestIdFilter { get; set; }
        public int? MinOfficialBusinessRequestIdFilter { get; set; }

    }
}