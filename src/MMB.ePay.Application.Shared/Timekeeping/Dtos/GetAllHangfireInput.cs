﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllHangfireInput : PagedAndSortedResultRequestDto
    {
        public DateTime? MinDateFilter { get; set; }

        public DateTime? MaxDateFilter { get; set; }
    }
}
