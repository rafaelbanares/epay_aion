﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditShiftApprovalsDto : EntityDto<int?>
    {

        [Required]
        [StringLength(ShiftApprovalsConsts.MaxRemarksLength, MinimumLength = ShiftApprovalsConsts.MinRemarksLength)]
        public string Remarks { get; set; }

        [Required]
        public int ActedBy { get; set; }

        public int ShiftRequestId { get; set; }

        public int StatusId { get; set; }

    }
}