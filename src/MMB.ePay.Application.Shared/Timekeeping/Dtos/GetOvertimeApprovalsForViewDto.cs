﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetOvertimeApprovalsForViewDto
    {
        public OvertimeApprovalsDto OvertimeApprovals { get; set; }

        public OvertimeDetailsDto OvertimeDetails { get; set; }
    }
}