﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditRawTimeLogsDto : EntityDto<int?>
    {

        [Required]
        [StringLength(RawTimeLogsConsts.MaxSwipeCodeLength, MinimumLength = RawTimeLogsConsts.MinSwipeCodeLength)]
        public string SwipeCode { get; set; }

        [Required]
        public DateTime LogTime { get; set; }

        [Required]
        [StringLength(RawTimeLogsConsts.MaxInOutLength, MinimumLength = RawTimeLogsConsts.MinInOutLength)]
        public string InOut { get; set; }

    }
}