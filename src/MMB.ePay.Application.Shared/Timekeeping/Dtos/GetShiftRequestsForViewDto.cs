﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetShiftRequestsForViewDto
    {
        public ShiftRequestsDto ShiftRequests { get; set; }

        public ShiftDetailsDto ShiftDetails { get; set; }
    }
}