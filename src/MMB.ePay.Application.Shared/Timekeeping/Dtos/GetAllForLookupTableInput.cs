﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}