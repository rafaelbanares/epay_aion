﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditAttendancePeriodsDto : EntityDto<int?>
    {

        [Required]
        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }

        [Required]
        [DisplayName("End Date")]
        public DateTime? EndDate { get; set; }

        [Required]
        [Range(1, short.MaxValue, ErrorMessage = "The Year field is required.")]
        [DisplayName("Year")]
        public short? AppYear { get; set; }

        [Required]
        [Range(1, short.MaxValue, ErrorMessage = "The Month field is required.")]
        [DisplayName("Month")]
        public short? AppMonth { get; set; }

        [DisplayName("Posted")]
        public bool Posted { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Frequency field is required.")]
        [DisplayName("Frequency")]
        public int? FrequencyId { get; set; }

    }
}