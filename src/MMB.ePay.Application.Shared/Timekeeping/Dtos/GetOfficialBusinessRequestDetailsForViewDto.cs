﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetOfficialBusinessRequestDetailsForViewDto
    {
        public OfficialBusinessRequestDetailsDto OfficialBusinessRequestDetails { get; set; }

    }
}