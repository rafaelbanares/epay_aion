﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditShiftTypesDto : EntityDto<int?>
    {

        [Required]
        [StringLength(ShiftTypesConsts.MaxDisplayNameLength, MinimumLength = ShiftTypesConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [StringLength(ShiftTypesConsts.MaxDescriptionLength, MinimumLength = ShiftTypesConsts.MinDescriptionLength)]
        public string Description { get; set; }

        [Required]
        public short? Flexible1 { get; set; }

        [Required]
        public short? GracePeriod1 { get; set; }

        [Required]
        public short? MinimumOvertime { get; set; }

        [Required]
        [StringLength(ShiftTypeDetailsConsts.MaxTimeInLength, MinimumLength = ShiftTypeDetailsConsts.MinTimeInLength)]
        public string TimeIn { get; set; }

        [StringLength(ShiftTypeDetailsConsts.MaxBreaktimeInLength, MinimumLength = ShiftTypeDetailsConsts.MinBreaktimeInLength)]
        public string BreaktimeIn { get; set; }

        [Required]
        [StringLength(ShiftTypeDetailsConsts.MaxBreaktimeOutLength, MinimumLength = ShiftTypeDetailsConsts.MinBreaktimeOutLength)]
        public string BreaktimeOut { get; set; }

        [Required]
        [StringLength(ShiftTypeDetailsConsts.MaxTimeOutLength, MinimumLength = ShiftTypeDetailsConsts.MinTimeOutLength)]
        public string TimeOut { get; set; }

        //public bool RotateShiftWeekly { get; set; }

        public bool StartOnPreviousDay { get; set; }

        public bool Sunday { get; set; }

        public bool Monday { get; set; }

        public bool Tuesday { get; set; }

        public bool Wednesday { get; set; }

        public bool Thursday { get; set; }

        public bool Friday { get; set; }

        public bool Saturday { get; set; }
    }

    public class EditShiftTypesDto : EntityDto<int?>
    {
        [Required]
        [StringLength(ShiftTypesConsts.MaxDisplayNameLength, MinimumLength = ShiftTypesConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        //[Required]
        //[StringLength(ShiftTypesConsts.MaxDescriptionLength, MinimumLength = ShiftTypesConsts.MinDescriptionLength)]
        public string Description { get; set; }

        [Required]
        public short? Flexible1 { get; set; }

        [Required]
        public short? GracePeriod1 { get; set; }

        [Required]
        public short? MinimumOvertime { get; set; }

        //public bool RotateShiftWeekly { get; set; }
    }
}