﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditAttendanceRequestsDto : EntityDto<int?>
    {
        public int? AttendanceId { get; set; }

        [Required]
        [DisplayName("Date")]
        public DateTime? Date { get; set; }

        [DisplayName("Time IN")]
        public DateTime? TimeIn { get; set; }

        public DateTime? BreaktimeIn { get; set; }

        public DateTime? BreaktimeOut { get; set; }

        public DateTime? PMBreaktimeIn { get; set; }

        public DateTime? PMBreaktimeOut { get; set; }

        [DisplayName("Time OUT")]
        public DateTime? TimeOut { get; set; }

        [AttendanceRequestsValidation(
            TimeIn = "TimeIn",
            BreaktimeIn = "BreaktimeIn",
            BreaktimeOut = "BreaktimeOut",
            PMBreaktimeIn = "PMBreaktimeIn",
            PMBreaktimeOut = "PMBreaktimeOut",
            TimeOut = "TimeOut")]

        [DisplayName("Early Login")]
        public bool EarlyLogin { get; set; }

        [Required]
        [StringLength(AttendanceRequestsConsts.MaxRemarksLength, MinimumLength = AttendanceRequestsConsts.MinRemarksLength)]
        [DisplayName("Remarks")]
        public string Remarks { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Attendance Reason field is required.")]
        [DisplayName("Attendance Reason")]
        public int? AttendanceReasonId { get; set; }
    }

    public class UpdateAttendanceRequestsStatus
    {
        public int Id { get; set; }

        public int StatusId { get; set; }

        public string Reason { get; set; }
    }

    public class AttendanceRequestsValidation : ValidationAttribute
    {
        public string TimeIn { get; set; }
        public string BreaktimeIn { get; set; }
        public string BreaktimeOut { get; set; }
        public string PMBreaktimeIn { get; set; }
        public string PMBreaktimeOut { get; set; }
        public string TimeOut { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var timeInProperty = validationContext.ObjectType.GetProperty(TimeIn);
            var breaktimeInProperty = validationContext.ObjectType.GetProperty(BreaktimeIn);
            var breaktimeOutProperty = validationContext.ObjectType.GetProperty(BreaktimeOut);
            var pMBreaktimeInProperty = validationContext.ObjectType.GetProperty(PMBreaktimeIn);
            var pMBreaktimeOutProperty = validationContext.ObjectType.GetProperty(PMBreaktimeOut);
            var timeOutProperty = validationContext.ObjectType.GetProperty(TimeOut);

            var timeIn = (DateTime?)timeInProperty.GetValue(validationContext.ObjectInstance, null);
            var breaktimeIn = (DateTime?)breaktimeInProperty.GetValue(validationContext.ObjectInstance, null);
            var breaktimeOut = (DateTime?)breaktimeOutProperty.GetValue(validationContext.ObjectInstance, null);
            var pMBreaktimeIn = (DateTime?)pMBreaktimeInProperty.GetValue(validationContext.ObjectInstance, null);
            var pMBreaktimeOut = (DateTime?)pMBreaktimeOutProperty.GetValue(validationContext.ObjectInstance, null);
            var timeOut = (DateTime?)timeOutProperty.GetValue(validationContext.ObjectInstance, null);
            var earlyLogin = (bool)value;

            if (!earlyLogin)
            {
                if (timeIn == null && breaktimeIn == null && breaktimeOut == null && 
                    pMBreaktimeIn == null && pMBreaktimeOut == null && timeOut == null)
                {
                    return new ValidationResult("The Time In/Time Out fields are required.");
                }
            }

            return ValidationResult.Success;
        }
    }

    public class ApproveAllAttendanceRequests
    {
        public int[] Ids { get; set; }
    }
}