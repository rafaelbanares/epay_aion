﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class ApprovalsViewModel
    {
        public ApprovalsViewModel()
        {
            YearList = new List<SelectListItem>();
            FrequencyList = new List<SelectListItem>();
            AttendancePeriodList = new List<SelectListItem>();
            ApproverTypeList = new List<SelectListItem>();
            StatusTypeList = new List<SelectListItem>();
        }

        public bool ApproverTypeId { get; set; }

        public IList<SelectListItem> YearList { get; set; }
        public IList<SelectListItem> FrequencyList { get; set; }
        public IList<SelectListItem> AttendancePeriodList { get; set; }
        public IList<SelectListItem> ApproverTypeList { get; set; }
        public IList<SelectListItem> StatusTypeList { get; set; }
    }

    public class RemarksViewModel
    {
        public int Id { get; set; }
        public int StatusId { get; set; }
        public string Status { get; set; }
    }

    public class RequestsViewModel
    {
        public RequestsViewModel()
        {
            YearList = new List<SelectListItem>();
            StatusList = new List<SelectListItem>();
            AttendancePeriodList = new List<SelectListItem>();
        }

        public bool HasPMBreak { get; set; }

        public IList<SelectListItem> YearList { get; set; }
        public IList<SelectListItem> StatusList { get; set; }
        public IList<SelectListItem> AttendancePeriodList { get; set; }
    }
}
