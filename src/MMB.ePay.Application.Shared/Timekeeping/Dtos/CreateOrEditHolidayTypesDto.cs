﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditHolidayTypesDto : EntityDto<int?>
    {

        [Required]
        [StringLength(HolidayTypesConsts.MaxDisplayNameLength, MinimumLength = HolidayTypesConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(HolidayTypesConsts.MaxDescriptionLength, MinimumLength = HolidayTypesConsts.MinDescriptionLength)]
        public string Description { get; set; }

        [Required]
        [StringLength(HolidayTypesConsts.MaxHolidayCodeLength, MinimumLength = HolidayTypesConsts.MinHolidayCodeLength)]
        public string HolidayCode { get; set; }

        public bool HalfDayEnabled { get; set; }
    }
}