﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditOfficialBusinessRequestDetailsDto : EntityDto<int?>
    {

        [Required]
        public DateTime OfficialBusinessDate { get; set; }

        [Required]
        public decimal Days { get; set; }

        public int OfficialBusinessRequestId { get; set; }

    }
}