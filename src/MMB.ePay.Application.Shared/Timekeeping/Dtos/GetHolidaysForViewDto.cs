﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetHolidaysForViewDto
    {
        public HolidaysDto Holidays { get; set; }

    }
}