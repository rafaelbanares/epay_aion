﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetOfficialBusinessApprovalsForViewDto
    {
        public OfficialBusinessApprovalsDto OfficialBusinessApprovals { get; set; }

        public OfficialBusinessDetailsDto OfficialBusinessDetails { get; set; }
    }
}