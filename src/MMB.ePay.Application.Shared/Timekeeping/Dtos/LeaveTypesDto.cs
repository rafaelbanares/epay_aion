﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class LeaveTypesDto : EntityDto
    {
        public string DisplayName { get; set; }

        public string Description { get; set; }

        public bool WithPay { get; set; }

        public int? AdvancedFilingInDays { get; set; }
    }
}