﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditLocationsDto : EntityDto<int?>
    {

        [Required]
        [StringLength(LocationsConsts.MaxDescriptionLength, MinimumLength = LocationsConsts.MinDescriptionLength)]
        public string Description { get; set; }
    }
}