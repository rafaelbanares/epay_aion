﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllHolidaysInput : PagedAndSortedResultRequestDto
    {
        public int? YearFilter { get; set; }
    }
}