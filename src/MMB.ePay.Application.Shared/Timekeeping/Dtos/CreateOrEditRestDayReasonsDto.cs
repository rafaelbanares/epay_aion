﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditRestDayReasonsDto : EntityDto<int?>
    {

        [Required]
        [StringLength(RestDayReasonsConsts.MaxDisplayNameLength, MinimumLength = RestDayReasonsConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [StringLength(RestDayReasonsConsts.MaxDescriptionLength, MinimumLength = RestDayReasonsConsts.MinDescriptionLength)]
        public string Description { get; set; }

    }
}