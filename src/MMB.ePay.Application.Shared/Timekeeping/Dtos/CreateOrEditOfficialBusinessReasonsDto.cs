﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditOfficialBusinessReasonsDto : EntityDto<int?>
    {

        [Required]
        [StringLength(OfficialBusinessReasonsConsts.MaxDisplayNameLength, MinimumLength = OfficialBusinessReasonsConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [StringLength(OfficialBusinessReasonsConsts.MaxDescriptionLength, MinimumLength = OfficialBusinessReasonsConsts.MinDescriptionLength)]
        public string Description { get; set; }

    }
}