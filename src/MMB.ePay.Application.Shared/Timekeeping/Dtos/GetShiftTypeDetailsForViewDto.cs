﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetShiftTypeDetailsForViewDto
    {
        public ShiftTypesDto ShiftTypes { get; set; }

        public ShiftTypeDetailsDto ShiftTypeDetails { get; set; }

    }
}