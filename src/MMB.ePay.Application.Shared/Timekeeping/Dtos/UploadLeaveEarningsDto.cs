﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class UploadLeaveEarningsDto
    {
        [MaxLength(400)]
        public string FileToken { get; set; }
    }

    public class LeaveEarningsFile
    {
        public string EmployeeId { get; set; }

        public decimal Earned { get; set; }

        public string LeaveType { get; set; }

        public DateTime Date { get; set; }

        public short AppYear { get; set; }

        public short AppMonth { get; set; }

        public string Remarks { get; set; }
    }
}