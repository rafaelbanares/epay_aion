﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetExcuseApprovalsForViewDto
    {
        public ExcuseApprovalsDto ExcuseApprovals { get; set; }

        public ExcuseDetailsDto ExcuseDetails { get; set; }
    }
}