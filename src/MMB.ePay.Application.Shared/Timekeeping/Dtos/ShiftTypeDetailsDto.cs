﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class ShiftTypeDetailsDto : EntityDto
    {
        public string Days { get; set; }

        public string TimeIn { get; set; }

        public string BreaktimeIn { get; set; }

        public string BreaktimeOut { get; set; }

        public string TimeOut { get; set; }

        public int ShiftTypeId { get; set; }

    }
}