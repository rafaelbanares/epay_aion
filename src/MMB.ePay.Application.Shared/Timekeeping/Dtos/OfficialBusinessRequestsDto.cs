﻿using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class OfficialBusinessRequestsDto : EntityDto
    {
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Reason { get; set; }

        public string Remarks { get; set; }

        public string Status { get; set; }

        public int OfficialBusinessReasonId { get; set; }

        public int StatusId { get; set; }

        public bool IsEditable { get; set; }

    }

    public class OfficialBusinessDetailsDto : EntityDto
    {
        public OfficialBusinessDetailsDto()
        {
            StatusList = new List<SelectListItem>();
        }

        public string Employee { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string ActualDates { get; set; }
        public string Reason { get; set; }
        public string Remarks { get; set; }
        public DateTime DateFiled { get; set; }
        public string Status { get; set; }

        public IList<SelectListItem> StatusList { get; set; }
    }
}