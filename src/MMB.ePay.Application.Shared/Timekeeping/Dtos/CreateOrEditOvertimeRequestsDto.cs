﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditOvertimeRequestsDto : EntityDto<int?>
    {
        [Required]
        [DisplayName("Overtime Date")]
        public DateTime? OvertimeDate { get; set; }

        //[Required]
        //[DisplayName("Start Time")]
        public TimeSpan? StartTime { get; set; }

        //[Required]
        //[DisplayName("End Time")]
        public TimeSpan? EndTime { get; set; }

        //[Required]
        //[BeforeEndDate(EndDatePropertyName = "OvertimeEnd")]
        //[DisplayName("Overtime Start")]
        public DateTime? OvertimeStart { get; set; }

        //[Required]
        //[DisplayName("Overtime End")]
        public DateTime? OvertimeEnd { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Overtime Reason field is required.")]
        [DisplayName("Overtime Reason")]
        public int? OvertimeReasonId { get; set; }

        [Required]
        [StringLength(OvertimeRequestsConsts.MaxRemarksLength, MinimumLength = OvertimeRequestsConsts.MinRemarksLength)]
        [DisplayName("Remarks")]
        public string Remarks { get; set; }
    }

    public class UpdateOvertimeRequestsStatus
    {
        public int Id { get; set; }

        public int StatusId { get; set; }

        public string Reason { get; set; }
    }

    public class ApproveAllOvertimeRequests
    {
        public int[] Ids { get; set; }
    }
}