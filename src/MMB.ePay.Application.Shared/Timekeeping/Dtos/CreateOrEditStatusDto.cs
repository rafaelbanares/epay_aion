﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditStatusDto : EntityDto<int?>
    {
        [Required]
        [StringLength(StatusConsts.MaxDisplayNameLength, MinimumLength = StatusConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [StringLength(StatusConsts.MaxAfterStatusMessageLength, MinimumLength = StatusConsts.MinAfterStatusMessageLength)]
        public string AfterStatusMessage { get; set; }

        [StringLength(StatusConsts.MaxBeforeStatusActionTextLength, MinimumLength = StatusConsts.MinBeforeStatusActionTextLength)]
        public string BeforeStatusActionText { get; set; }
    }
}