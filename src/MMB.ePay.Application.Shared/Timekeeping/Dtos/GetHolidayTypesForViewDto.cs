﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetHolidayTypesForViewDto
    {
        public HolidayTypesDto HolidayTypes { get; set; }

    }
}