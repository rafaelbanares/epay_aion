﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetReportsForViewDto
    {
        public ReportsDto Reports { get; set; }
    }

    public class GetReportNamesDto
    {
        public string ControllerName { get; set; }
        public string ExportFileName { get; set; }
        public string StoredProcedure { get; set; }
    }

    public class PayrollRegisterHeaderReportViewModel
    {
        public bool IsPosted { get; set; }
        public string CompanyName { get; set; }
        public string PayrollPeriod { get; set; }
        public string CutoffPeriod { get; set; }
    }

    public class GetReportsForActiveEmployees
    {
        public IList<ActiveEmployeesTable> ActiveEmployees { get; set; }
    }

    public class ActiveEmployeesTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Date Employed")]
        public DateTime? DateEmployed { get; set; }

        [DisplayName("Date Terminated")]
        public DateTime? DateTerminated { get; set; }

        [DisplayName("Status")]
        public string EmployeeStatus { get; set; }
    }

    public class GetReportsForDailyAttendance
    {
        public IList<DailyAttendanceTable> DailyAttendance { get; set; }
    }

    public class DailyAttendanceTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [DisplayName("Time In")]
        [DisplayFormat(DataFormatString = "{0:HH:mm ss}", ApplyFormatInEditMode = true)]
        public DateTime? TimeIn { get; set; }

        [DisplayName("Time Out")]
        [DisplayFormat(DataFormatString = "{0:HH:mm ss}", ApplyFormatInEditMode = true)]
        public DateTime? TimeOut { get; set; }

        [DisplayName("UT")]
        public int UTMins { get; set; }

        [DisplayName("Tardy")]
        public int TardyMins { get; set; }

        [DisplayName("Leave")]
        public decimal LeaveDays { get; set; }

        [DisplayName("Absence")]
        public decimal Absence { get; set; }
    }

    public class GetReportsForAttendanceWithBreaks
    {
        public IList<AttendanceWithBreaksTable> AttendanceWithBreaks { get; set; }
    }

    public class AttendanceWithBreaksTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [DisplayName("Time IN")]
        [DisplayFormat(DataFormatString = "{0:HH:mm ss}", ApplyFormatInEditMode = true)]
        public DateTime? TimeIn { get; set; }

        [DisplayName("Lunch IN")]
        [DisplayFormat(DataFormatString = "{0:HH:mm ss}", ApplyFormatInEditMode = true)]
        public DateTime? BreaktimeIn { get; set; }

        [DisplayName("Lunch OUT")]
        [DisplayFormat(DataFormatString = "{0:HH:mm ss}", ApplyFormatInEditMode = true)]
        public DateTime? BreaktimeOut { get; set; }

        [DisplayName("Break IN")]
        [DisplayFormat(DataFormatString = "{0:HH:mm ss}", ApplyFormatInEditMode = true)]
        public DateTime? PMBreaktimeIn { get; set; }

        [DisplayName("Break OUT")]
        [DisplayFormat(DataFormatString = "{0:HH:mm ss}", ApplyFormatInEditMode = true)]
        public DateTime? PMBreaktimeOut { get; set; }

        [DisplayName("Time OUT")]
        [DisplayFormat(DataFormatString = "{0:HH:mm ss}", ApplyFormatInEditMode = true)]
        public DateTime? TimeOut { get; set; }
    }

    public class GetReportsForRawTimeGeolocation
    {
        public IList<RawTimeGeolocationTable> RawTimeGeolocation { get; set; }
    }

    public class RawTimeGeolocationTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [DisplayName("Time")]
        [DisplayFormat(DataFormatString = "{0:HH:mm ss}", ApplyFormatInEditMode = true)]
        public DateTime? Time { get; set; }

        [DisplayName("Location")]
        public string Location { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }

    public class GetReportsForWorkFromHome
    {
        public IList<WorkFromHomeTable> WorkFromHome{ get; set; }
    }

    public class WorkFromHomeTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [DisplayName("Time")]
        [DisplayFormat(DataFormatString = "{0:HH:mm ss}", ApplyFormatInEditMode = true)]
        public DateTime? Time { get; set; }

        [DisplayName("Device")]
        public string Device { get; set; }
    }

    public class GetReportsForAttendanceSummary
    { 
        public IList<AttendanceSummaryTable> AttendanceSummary { get; set; }
    }

    public class AttendanceSummaryTable
    {
        public string EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        public decimal RegDays { get; set; }

        public decimal RegNd1Mins { get; set; }

        public decimal RegNd2Mins { get; set; }

        public decimal UTMins { get; set; }

        public decimal TardyMins { get; set; }

        public decimal PaidHoliday { get; set; }

        public decimal Absence { get; set; }

        //public IList<Tuple<int, decimal, string>> TypeValues { get; set; }
    }

    public class GetReportsForAttendanceSummaryPayroll
    {
        public IList<AttendanceSummaryPayrollTable> AttendanceSummaryPayroll { get; set; }
    }

    public class AttendanceSummaryPayrollTable
    {
        [DisplayName("COMPANY ID")]
        public string TenantId { get; set; }

        [DisplayName("EMPLOYEE NO")]
        public string EmployeeId { get; set; }

        [DisplayName("LASTNAME")]
        public string LastName { get; set; }

        [DisplayName("FIRSTNAME")]
        public string FirstName { get; set; }

        [DisplayName("PAYCODE")]
        public string Paycode { get; set; }

        [DisplayName("DEPARTMENT")]
        public string Department { get; set; }

        [DisplayName("REGDAYS")]
        public decimal RegDays { get; set; }

        [DisplayName("REGHRS")]
        public decimal RegHrs { get; set; }

        [DisplayName("ABSENT DAYS")]
        public decimal AbsDays { get; set; }

        [DisplayName("ABSENT HRS")]
        public decimal AbsHrs { get; set; }

        [DisplayName("TARDY")]
        public decimal TardyHrs { get; set; }

        [DisplayName("UT")]
        public decimal UTHrs { get; set; }

        [DisplayName("ND1")]
        public decimal ND1Hrs { get; set; }

        [DisplayName("ND2")]
        public decimal ND2Hrs { get; set; }

        [DisplayName("SLDAYS")]
        public decimal SLDays { get; set; }

        [DisplayName("VLDAYS")]
        public decimal VLDays { get; set; }

        [DisplayName("PAIDHOLIDAYS")]
        public decimal PaidHoliday { get; set; }
    }

    public class GetReportsForLeaveBalance
    {
        public IList<LeaveBalanceTable> LeaveBalance { get; set; }
    }

    public class LeaveBalanceTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Leave Type")]
        public string LeaveType { get; set; }

        [DisplayName("Leave Earned")]
        public decimal LeaveEarned { get; set; }

        [DisplayName("Leave Taken")]
        public decimal LeaveTaken { get; set; }

        [DisplayName("Current Balance")]
        public decimal CurrentBalance
        {
            get { return LeaveEarned - LeaveTaken; }
        }
    }

    public class GetReportsForLeaveSummary
    {
        public IList<Tuple<int, string>> LeaveTypes { get; set; }

        public IList<LeaveSummaryTable> LeaveSummary { get; set; }
    }

    public class LeaveSummaryTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        public IList<Tuple<int, decimal>> TypeValues { get; set; }
    }

    public class GetReportsForLeaveSummaryPayroll
    {
        public IList<LeaveSummaryPayrollTable> LeaveSummaryPayroll { get; set; }
    }

    public class LeaveSummaryPayrollTable
    {
        [DisplayName("COMPANY ID")]
        public string TenantId { get; set; }

        [DisplayName("EMPLOYEE NO")]
        public string EmployeeId { get; set; }

        [DisplayName("LASTNAME")]
        public string LastName { get; set; }

        [DisplayName("FIRSTNAME")]
        public string FirstName { get; set; }

        [DisplayName("PAYCODE")]
        public string Paycode { get; set; }

        [DisplayName("DEPARTMENT")]
        public string Department { get; set; }

        [DisplayName("TS CODE")]
        public string TSCode { get; set; }

        [DisplayName("TS DAYS")]
        public decimal TSDays { get; set; }

        [DisplayName("TS HRS")]
        public decimal TSHrs { get; set; }

        [DisplayName("TS MINS")]
        public decimal TSMins { get; set; }

    }

    public class GetReportsForOvertimeSummary
    {
        public IList<Tuple<int, string>> OvertimeTypes { get; set; }

        public IList<OvertimeSummaryTable> OvertimeSummary { get; set; }
    }

    public class OvertimeSummaryTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        public IList<Tuple<int, decimal>> TypeValues { get; set; }

        public decimal RegularOT { get; set; }
        public decimal RegularOT8 { get; set; }
        public decimal RegularOTND1 { get; set; }
        public decimal RegularOTND2 { get; set; }
        public decimal RestDayOT { get; set; }
        public decimal RestDayOT8 { get; set; }
    }

    public class GetReportsForOvertimeSummaryPayroll
    {
        public IList<Tuple<int, string>> OvertimeTypes { get; set; }

        public IList<OvertimeSummaryPayrollTable> OvertimeSummaryPayroll { get; set; }
    }

    public class OvertimeSummaryPayrollTable
    {
        [DisplayName("COMPANY ID")]
        public string TenantId { get; set; }

        [DisplayName("EMPLOYEE NO")]
        public string EmployeeId { get; set; }

        [DisplayName("LASTNAME")]
        public string LastName { get; set; }

        [DisplayName("FIRSTNAME")]
        public string FirstName { get; set; }

        [DisplayName("PAYCODE")]
        public string Paycode { get; set; }

        [DisplayName("DEPARTMENT")]
        public string Department { get; set; }

        public IList<Tuple<int, decimal>> TypeValues { get; set; }

        public decimal RegularOT { get; set; }
        public decimal RegularOT8 { get; set; }
        public decimal RegularOTND1 { get; set; }
        public decimal RegularOTND2 { get; set; }
        public decimal RestDayOT { get; set; }
        public decimal RestDayOT8 { get; set; }
    }

    public class GetReportsForAttendanceTransaction
    {
        public IList<AttendanceTransactionTable> AttendanceTransaction { get; set; }
    }

    public class AttendanceTransactionTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Date Filed")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateFiled { get; set; }

        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }
    }

    public class GetReportsForExcuseTransaction
    {
        public IList<ExcuseTransactionTable> ExcuseTransaction { get; set; }
    }

    public class ExcuseTransactionTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Date Filed")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateFiled { get; set; }

        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }
    }

    public class GetReportsForLeaveTransaction
    {
        public IList<LeaveTransactionTable> LeaveTransaction { get; set; }
    }

    public class LeaveTransactionTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Type")]
        public string LeaveType { get; set; }

        [DisplayName("Date Filed")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateFiled { get; set; }

        [DisplayName("Start Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [DisplayName("End Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }

        public decimal Days { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }
    }

    public class GetReportsForOfficialBusinessTransaction
    {
        public IList<OfficialBusinessTransactionTable> OfficialBusinessTransaction { get; set; }
    }

    public class OfficialBusinessTransactionTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Date Filed")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateFiled { get; set; }

        [DisplayName("Start Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [DisplayName("End Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }
    }

    public class GetReportsForOvertimeTransaction
    {
        public IList<OvertimeTransactionTable> OvertimeTransaction { get; set; }
    }

    public class OvertimeTransactionTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Attendance Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime AttendanceDate { get; set; }

        [DisplayName("Date Filed")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateFiled { get; set; }

        [DisplayName("OT Start")]
        [DisplayFormat(DataFormatString = "{0:hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime OTStart { get; set; }

        [DisplayName("OT End")]
        [DisplayFormat(DataFormatString = "{0:hh:mm tt}", ApplyFormatInEditMode = true)]
        public DateTime OTEnd { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }
    }

    public class GetReportsForRestDayTransaction
    {
        public IList<RestDayTransactionTable> RestDayTransaction { get; set; }
    }

    public class RestDayTransactionTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Date Filed")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateFiled { get; set; }

        [DisplayName("Start Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [DisplayName("End Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }
    }

    public class GetReportsForShiftTransaction
    {
        public IList<ShiftTransactionTable> ShiftTransaction { get; set; }
    }

    public class ShiftTransactionTable
    {
        [DisplayName("Emp ID")]
        public string EmployeeId { get; set; }

        [DisplayName("Emp Name")]
        public string EmployeeName { get; set; }

        [DisplayName("Type")]
        public string ShiftType { get; set; }

        [DisplayName("Date Filed")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DateFiled { get; set; }

        [DisplayName("Start Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [DisplayName("End Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }
    }



    //public class GetReportsForPayslip
    //{
    //    public GetReportsForPayslip()
    //    {
    //        Earnings = new List<Tuple<string, string, decimal>>();

    //        Deductions = new List<Tuple<string, decimal>>();

    //        YTDSummaries = new List<Tuple<string, decimal>>();

    //        LoanBalances = new List<Tuple<string, decimal>>();

    //        LeaveBalances = new List<Tuple<string, decimal>>();
    //    }

    //    public string CompanyName { get; set; }

    //    public string PayrollPeriod { get; set; }

    //    public string CutoffPeriod { get; set; }

    //    public string EmployeeCode { get; set; }

    //    public string EmployeeName { get; set; }

    //    [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime PayDate { get; set; }

    //    public string Department { get; set; }

    //    public string Designation { get; set; }

    //    [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
    //    public DateTime? DateOfJoining { get; set; }

    //    public IList<Tuple<string, string, decimal>> Earnings { get; set; }

    //    public IList<Tuple<string, decimal>> Deductions { get; set; }

    //    public IList<Tuple<string, decimal>> YTDSummaries { get; set; }

    //    public IList<Tuple<string, decimal>> LoanBalances { get; set; }

    //    public IList<Tuple<string, decimal>> LeaveBalances { get; set; }

    //    public decimal? TotalEarnings { get; set; }

    //    public decimal? TotalDeductions { get; set; }

    //    public decimal? NetPay { get; set; }
    //}
}
