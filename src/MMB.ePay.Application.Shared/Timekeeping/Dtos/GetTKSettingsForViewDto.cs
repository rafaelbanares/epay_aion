﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetTKSettingsForViewDto
    {
        public TKSettingsDto TKSettings { get; set; }

    }
}