﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class ExcuseApprovalsDto : EntityDto
    {
        public string Remarks { get; set; }
        public DateTime ExcuseDate { get; set; }
        public DateTime ExcuseEnd { get; set; }
        public DateTime ExcuseStart { get; set; }
        public string EmployeeId { get; set; }
        public int ExcuseReasonId { get; set; }
        public string Employee { get; set; }
        public string Reason { get; set; }
        public string Status { get; set; }
        public int StatusId { get; set; }
    }
}