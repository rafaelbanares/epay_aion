﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using static MMB.ePay.Application.Shared.CustomValidations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditExcuseRequestsDto : EntityDto<int?>
    {

        [Required]
        [DisplayName("Remarks")]
        [StringLength(ExcuseRequestsConsts.MaxRemarksLength, MinimumLength = ExcuseRequestsConsts.MinRemarksLength)]
        public string Remarks { get; set; }

        [Required]
        [DisplayName("Excuse Date")]
        public DateTime? ExcuseDate { get; set; }

        [Required]
        [DisplayName("Excuse Start")]
        [BeforeEndDate(EndDatePropertyName = "ExcuseEnd")]
        public DateTime? ExcuseStart { get; set; }

        [Required]
        [DisplayName("Excuse End")]
        public DateTime? ExcuseEnd { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Excuse Reason field is required.")]
        [DisplayName("Excuse Reason")]
        public int? ExcuseReasonId { get; set; }
    }

    public class UpdateExcuseRequestsStatus
    {
        public int Id { get; set; }

        public int StatusId { get; set; }

        public string Reason { get; set; }
    }

    public class ApproveAllExcuseRequests
    {
        public int[] Ids { get; set; }
    }
}