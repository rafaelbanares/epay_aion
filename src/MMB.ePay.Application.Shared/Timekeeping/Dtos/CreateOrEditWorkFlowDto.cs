﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditWorkFlowDto : EntityDto<int?>
    {

        public int? NextStatus { get; set; }

        public int PageTypeId { get; set; }

        public int StatusId { get; set; }

    }
}