﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditOvertimeReasonsDto : EntityDto<int?>
    {

        [Required]
        [StringLength(OvertimeReasonsConsts.MaxDisplayNameLength, MinimumLength = OvertimeReasonsConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [StringLength(OvertimeReasonsConsts.MaxDescriptionLength, MinimumLength = OvertimeReasonsConsts.MinDescriptionLength)]
        public string Description { get; set; }

    }
}