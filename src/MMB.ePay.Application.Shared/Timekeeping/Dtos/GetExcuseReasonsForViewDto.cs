﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetExcuseReasonsForViewDto
    {
        public ExcuseReasonsDto ExcuseReasons { get; set; }

    }
}