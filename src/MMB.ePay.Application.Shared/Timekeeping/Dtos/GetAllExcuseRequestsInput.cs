﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllExcuseRequestsInput : PagedAndSortedResultRequestDto
    {
        public int? AttendancePeriodIdFilter { get; set; }
        public int? StatusIdFilter { get; set; }
    }
}