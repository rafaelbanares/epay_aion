﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllStatusInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string StatusCodeFilter { get; set; }

        public string DisplayNameFilter { get; set; }

        public string AfterStatusMessageFilter { get; set; }

        public string BeforeStatusActionTextFilter { get; set; }

        public string BeforeStatusActionNameFilter { get; set; }

    }
}