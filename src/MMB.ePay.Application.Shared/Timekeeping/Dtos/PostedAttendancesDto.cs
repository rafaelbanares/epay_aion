﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class PostedAttendancesDto : EntityDto
    {
        public DateTime Date { get; set; }

        public string RestDayCode { get; set; }

        public DateTime? TimeIn { get; set; }

        public DateTime? BreaktimeIn { get; set; }

        public DateTime? BreaktimeOut { get; set; }

        public DateTime? TimeOut { get; set; }

        //public decimal Leave_days { get; set; }

        //public int Reg_mins { get; set; }

        //public int Regnd1_mins { get; set; }

        //public int Regnd2_mins { get; set; }

        //public int Tardy_mins { get; set; }

        //public int UT_mins { get; set; }

        //public decimal Absent_days { get; set; }

        //public int Excess_mins { get; set; }

        public string Remarks { get; set; }

        //public string Tags { get; set; }

        public int EmployeeId { get; set; }

        public int ShiftTypeId { get; set; }

        //public int LeaveTypeId { get; set; }

    }
}