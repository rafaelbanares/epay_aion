﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetLeaveTypesForViewDto
    {
        public LeaveTypesDto LeaveTypes { get; set; }

    }
}