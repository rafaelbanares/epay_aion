﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class AttendanceApprovalsDto : EntityDto
    {
        public string EmployeeId { get; set; }
        public string Employee { get; set; }
        public DateTime Date { get; set; }
        public DateTime? TimeIn { get; set; }
        public DateTime? BreaktimeIn { get; set; }
        public DateTime? BreaktimeOut { get; set; }
        public DateTime? PMBreaktimeIn { get; set; }
        public DateTime? PMBreaktimeOut { get; set; }
        public DateTime? TimeOut { get; set; }
        public string Reason { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; }
        public int? AttendanceId { get; set; }
        public int? AttendanceReasonId { get; set; }
        public int StatusId { get; set; }
    }
}