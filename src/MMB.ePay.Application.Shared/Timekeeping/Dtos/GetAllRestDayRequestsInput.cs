﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllRestDayRequestsInput : PagedAndSortedResultRequestDto
    {
        public int? AttendancePeriodIdFilter { get; set; }
        public int? StatusIdFilter { get; set; }
    }
}