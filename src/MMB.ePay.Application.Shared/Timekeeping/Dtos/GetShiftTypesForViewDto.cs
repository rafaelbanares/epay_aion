﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetShiftTypesForViewDto
    {
        public ShiftTypesDto ShiftTypes { get; set; }
    }
}