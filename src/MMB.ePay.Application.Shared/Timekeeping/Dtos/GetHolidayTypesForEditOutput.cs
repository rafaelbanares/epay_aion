﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetHolidayTypesForEditOutput
    {
        public CreateOrEditHolidayTypesDto HolidayTypes { get; set; }

    }
}