﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetOvertimeTypesForViewDto
    {
        public OvertimeTypesDto OvertimeTypes { get; set; }

    }
}