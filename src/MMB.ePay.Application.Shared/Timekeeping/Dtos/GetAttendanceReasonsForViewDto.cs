﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAttendanceReasonsForViewDto
    {
        public AttendanceReasonsDto AttendanceReasons { get; set; }

    }
}