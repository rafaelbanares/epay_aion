﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class OvertimeApprovalsDto : EntityDto
    {
        public string EmployeeId { get; set; }

        public string Employee { get; set; }

        public DateTime OvertimeDate { get; set; }

        public DateTime OvertimeStart { get; set; }

        public DateTime OvertimeEnd { get; set; }

        public string Reason { get; set; }

        public string Remarks { get; set; }

        public string Status { get; set; }

        public int OvertimeReasonId { get; set; }

        public int StatusId { get; set; }
    }
}