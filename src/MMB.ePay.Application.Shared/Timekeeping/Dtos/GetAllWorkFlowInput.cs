﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllWorkFlowInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public int? MaxNextStatusFilter { get; set; }
        public int? MinNextStatusFilter { get; set; }

        public int? MaxPageTypeIdFilter { get; set; }
        public int? MinPageTypeIdFilter { get; set; }

        public int? MaxStatusIdFilter { get; set; }
        public int? MinStatusIdFilter { get; set; }

    }
}