﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditRestDayApprovalsDto : EntityDto<int?>
    {

        [Required]
        public int ActedBy { get; set; }

        [Required]
        [StringLength(RestDayApprovalsConsts.MaxRemarksLength, MinimumLength = RestDayApprovalsConsts.MinRemarksLength)]
        public string Remarks { get; set; }

        public int RestDayRequestId { get; set; }

        public int StatusId { get; set; }

    }
}