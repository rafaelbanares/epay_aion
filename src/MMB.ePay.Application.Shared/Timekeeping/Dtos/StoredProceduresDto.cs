﻿using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class StoredProceduresDto
    {

    }

    public class TestDto
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
    }

    public class InitializeProcSessionDto
    {
        public Guid AppSessionId { get; set; }
        public int AttendancePeriodId { get; set; }
        public int TenantId { get; set; }
        public int? EmployeeId { get; set; }
    }

    public class PostAttendanceDto
    {
        public int AttendancePeriodId { get; set; }
        public int TenantId { get; set; }
    }

    public class GetProcessBatchDto
    {
        public Guid AppSessionId { get; set; }
    }

    public class DeleteProcessDto
    {
        public int AttendancePeriodId { get; set; }
        public int TenantId { get; set; }
        public int? EmployeeId { get; set; }
    }
}
