﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditLeaveEarningsDto : EntityDto<int?>
    {

        [Required]
        [DisplayName("Date")]
        public DateTime? Date { get; set; }

        //[DisplayName("Valid From")]
        //public DateTime? ValidFrom { get; set; }

        //[DisplayName("Valid To")]
        //public DateTime? ValidTo { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Year field is required.")]
        [DisplayName("Year")]
        public short? AppYear { get; set; }

        [DisplayName("Month")]
        public short? AppMonth { get; set; }

        //[Required(ErrorMessage = "The Employee field is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "The Employee field is required.")]
        [DisplayName("Employee")]
        public int? EmployeeId { get; set; }

        public string Employee { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Leave Type field is required.")]
        [DisplayName("Leave Type")]
        public int? LeaveTypeId { get; set; }

        [Required]
        //[Range(1, int.MaxValue, ErrorMessage = "The Earned field is required.")]
        [DisplayName("Earned")]
        public decimal? Earned { get; set; }


        [Required]
        [StringLength(LeaveEarningsConsts.MaxRemarksLength, MinimumLength = LeaveEarningsConsts.MinRemarksLength)]
        [DisplayName("Remarks")]
        public string Remarks { get; set; }
    }
}