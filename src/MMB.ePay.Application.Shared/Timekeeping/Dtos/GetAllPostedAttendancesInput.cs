﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllPostedAttendancesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public DateTime? MaxDateFilter { get; set; }
        public DateTime? MinDateFilter { get; set; }

        public string RestDayCodeFilter { get; set; }

        public DateTime? MaxTimeInFilter { get; set; }
        public DateTime? MinTimeInFilter { get; set; }

        public DateTime? MaxBreaktimeInFilter { get; set; }
        public DateTime? MinBreaktimeInFilter { get; set; }

        public DateTime? MaxBreaktimeOutFilter { get; set; }
        public DateTime? MinBreaktimeOutFilter { get; set; }

        public DateTime? MaxTimeOutFilter { get; set; }
        public DateTime? MinTimeOutFilter { get; set; }

        public decimal? MaxLeave_daysFilter { get; set; }
        public decimal? MinLeave_daysFilter { get; set; }

        public int? MaxReg_minsFilter { get; set; }
        public int? MinReg_minsFilter { get; set; }

        public int? MaxRegnd1_minsFilter { get; set; }
        public int? MinRegnd1_minsFilter { get; set; }

        public int? MaxRegnd2_minsFilter { get; set; }
        public int? MinRegnd2_minsFilter { get; set; }

        public int? MaxTardy_minsFilter { get; set; }
        public int? MinTardy_minsFilter { get; set; }

        public int? MaxUT_minsFilter { get; set; }
        public int? MinUT_minsFilter { get; set; }

        public decimal? MaxAbsent_daysFilter { get; set; }
        public decimal? MinAbsent_daysFilter { get; set; }

        public int? MaxExcess_minsFilter { get; set; }
        public int? MinExcess_minsFilter { get; set; }

        public string RemarksFilter { get; set; }

        public string TagsFilter { get; set; }

        public int? MaxShiftTypeIdFilter { get; set; }
        public int? MinShiftTypeIdFilter { get; set; }

        public int? MaxLeaveTypeIdFilter { get; set; }
        public int? MinLeaveTypeIdFilter { get; set; }

    }
}