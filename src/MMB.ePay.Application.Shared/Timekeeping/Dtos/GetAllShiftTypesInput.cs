﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllShiftTypesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string DisplayNameFilter { get; set; }

        public string DescriptionFilter { get; set; }

        public short? MaxFlexible1Filter { get; set; }
        public short? MinFlexible1Filter { get; set; }

        public short? MaxGracePeriod1Filter { get; set; }
        public short? MinGracePeriod1Filter { get; set; }

        public short? MaxMinimumOvertimeFilter { get; set; }
        public short? MinMinimumOvertimeFilter { get; set; }

    }
}