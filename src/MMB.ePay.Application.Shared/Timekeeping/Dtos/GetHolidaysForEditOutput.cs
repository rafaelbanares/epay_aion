﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetHolidaysForEditOutput
    {
        public CreateOrEditHolidaysDto Holidays { get; set; }

    }
}