﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetLeaveRequestsForViewDto
    {
        public LeaveRequestsDto LeaveRequests { get; set; }

        public LeaveDetailsDto LeaveDetails { get; set; }
    }

    public class GetLeaveRequestsForBalanceDto
    {
        public int LeaveTypeId { get; set; }

        public string LeaveType { get; set; }

        public decimal TotalDays { get; set; }
    }

    public class GetAdminLeaveRequestsForBalanceDto
    {
        public int EmployeeId { get; set; }

        public int LeaveTypeId { get; set; }

        public string Employee { get; set; }

        public string LeaveType { get; set; }

        public decimal TotalDays { get; set; }
    }
}