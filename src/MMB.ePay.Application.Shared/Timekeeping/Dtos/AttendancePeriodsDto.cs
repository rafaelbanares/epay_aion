﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class AttendancePeriodsDto : EntityDto
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public short AppYear { get; set; }

        public string AppMonth { get; set; }

        public bool Posted { get; set; }

        public string Frequency { get; set; }

        public bool IsEditable { get; set; }
    }
}