﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class LocationsDto : EntityDto
    {
        public string Description { get; set; }

        public bool IsEditable { get; set; }
    }
}