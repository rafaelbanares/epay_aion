﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditLeaveApprovalsDto : EntityDto<int?>
    {

        [Required]
        public int ActedBy { get; set; }

        [Required]
        [StringLength(LeaveApprovalsConsts.MaxRemarksLength, MinimumLength = LeaveApprovalsConsts.MinRemarksLength)]
        public string Remarks { get; set; }

        public int LeaveRequestId { get; set; }

        public int StatusId { get; set; }

    }
}