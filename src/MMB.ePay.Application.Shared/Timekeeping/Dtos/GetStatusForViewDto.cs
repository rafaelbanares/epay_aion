﻿using MMB.ePay.Timekeeping.Enums;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetStatusForViewDto
    {
        public StatusDto Status { get; set; }
    }

    public class GetStatusForDisplayNameDto
    {
        public StatusDisplayNames? StatusDisplayName { get; set; }
        public string AfterStatusMessage { get; set; }
        public string StatusName { get; set; }

    }
}