﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetPostedAttendancesForViewDto
    {
        public PostedAttendancesDto PostedAttendances { get; set; }

    }
}