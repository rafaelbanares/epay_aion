﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditEmployeeApproversDto : EntityDto<int?>
    {
        public CreateOrEditEmployeeApproversDto()
        {
            ApproverIdList = new List<int?>();
            ApproverNameList = new List<string>();
        }

        public int? EmployeeId { get; set; }

        public string Employee { get; set; }

        public List<int?> ApproverIdList { get; set; }

        public List<string> ApproverNameList { get; set; }
    }
}