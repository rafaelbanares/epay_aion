﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using static MMB.ePay.Application.Shared.CustomValidations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditLeaveRequestsDto : EntityDto<int?>
    {
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Leave Type field is required.")]
        [DisplayName("Leave Type")]
        public int? LeaveTypeId { get; set; }

        [Required]
        [DisplayName("Start Date")]
        [BeforeEndDate(EndDatePropertyName = "EndDate")]
        public DateTime? StartDate { get; set; }

        [DisplayName("End Date")]
        public DateTime? EndDate { get; set; }

        [DisplayName("Half Day")]
        public bool HalfDay { get; set; }

        [DisplayName("First Half")]
        public bool FirstHalf { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Leave Reason field is required.")]
        [DisplayName("Leave Reason")]
        public int? LeaveReasonId { get; set; }

        [Required]
        [StringLength(LeaveRequestsConsts.MaxRemarksLength, MinimumLength = LeaveRequestsConsts.MinRemarksLength)]
        [DisplayName("Remarks")]
        public string Remarks { get; set; }
    }

    public class UpdateLeaveRequestsStatus
    {
        public int Id { get; set; }

        public int StatusId { get; set; }

        public string Reason { get; set; }
    }

    public class ApproveAllLeaveRequests
    {
        public int[] Ids { get; set; }
    }
}