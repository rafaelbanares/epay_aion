﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class HolidaysDto : EntityDto
    {
        public string DisplayName { get; set; }

        public DateTime Date { get; set; }

        public bool HalfDay { get; set; }

        public string HolidayType { get; set; }

        public string Location { get; set; }

        public bool IsEditable { get; set; }
    }
}