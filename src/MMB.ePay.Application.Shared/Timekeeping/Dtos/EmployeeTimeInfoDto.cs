﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class EmployeeTimeInfoDto : EntityDto
    {
        public string RestDay { get; set; }

        public bool Overtime { get; set; }

        public bool Undertime { get; set; }

        public bool Tardy { get; set; }

        public bool NonSwiper { get; set; }

        public int EmployeeId { get; set; }

        public int FrequencyId { get; set; }

        public int ShiftTypeId { get; set; }
    }
}