﻿using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAttendancesForViewDto
    {
        public AttendancesDto Attendances { get; set; }
    }

    //public class GetAttendancesForRawEntriesDto
    //{
    //    public AttendancesDto Attendances { get; set; }
    //}

    public class GetAttendancesForUploadDto
    {
        public GetAttendancesForUploadDto()
        {
            AttendancesUpload = new AttendancesUploadDto();
        }

        public AttendancesUploadDto AttendancesUpload { get; set; }
    }

    public class PagedAndFilteredAttenancesDto
    {
        public int EmployeeId { get; set; }
        public DateTime Date { get; set; }
        public DayOfWeek DayOfWeek { get; set; }
        public string Shift { get; set; }
        public string RestDayCode { get; set; }
        public DateTime? TimeIn { get; set; }
        public DateTime? BreaktimeIn { get; set; }
        public DateTime? BreaktimeOut { get; set; }
        public DateTime? PMBreaktimeIn { get; set; }
        public DateTime? PMBreaktimeOut { get; set; }
        public DateTime? TimeOut { get; set; }
        public bool IsPosted { get; set; }
        public bool Invalid { get; set; }
        public int Id { get; set; }
    }
}