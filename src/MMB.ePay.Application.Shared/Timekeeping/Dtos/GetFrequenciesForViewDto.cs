﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetFrequenciesForViewDto
    {
        public FrequenciesDto Frequencies { get; set; }

    }
}