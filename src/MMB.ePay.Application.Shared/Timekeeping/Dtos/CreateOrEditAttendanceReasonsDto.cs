﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditAttendanceReasonsDto : EntityDto<int?>
    {

        [Required]
        [StringLength(AttendanceReasonsConsts.MaxDisplayNameLength, MinimumLength = AttendanceReasonsConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [StringLength(AttendanceReasonsConsts.MaxDescriptionLength, MinimumLength = AttendanceReasonsConsts.MinDescriptionLength)]
        public string Description { get; set; }

    }
}