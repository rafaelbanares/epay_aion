﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class ReportsDto
    {
        public ReportsDto()
        {
            ReportList = new List<SelectListItem>();
            OrganizationUnitList = new List<SelectListItem>();
            YearList = new List<SelectListItem>();
            MonthList = new List<SelectListItem>();
            PostList = new List<SelectListItem>();
            AttendancePeriodList = new List<SelectListItem>();
        }

        [Range(1, int.MaxValue, ErrorMessage = "The Report field is required.")]
        public int ReportId { get; set; }
        
        public int EmployeeId { get; set; }
        
        public int AttendancePeriodId { get; set; }

        public int FrequencyFilterId { get; set; }

        public string Rank { get; set; }

        public long OrganizationUnitId1 { get; set; }

        public long OrganizationUnitId2 { get; set; }

        public long OrganizationUnitId3 { get; set; }

        public virtual long MainOrganizationUnitId
        {
            get { return OrganizationUnitId3 > 0 ? OrganizationUnitId3 : OrganizationUnitId2 > 0 ? OrganizationUnitId2 : OrganizationUnitId1 > 0 ? OrganizationUnitId1 : 0; }
        }

        public List<long> OrgUnitIds { get; set; }

        public DateTime? MinDate { get; set; }
        
        public DateTime? MaxDate { get; set; }

        public string ControllerName { get; set; }

        public string Output { get; set; }

        public string ExportFileName { get; set; }

        public string StoredProcedure { get; set; }

        public bool IsLandscape { get; set; }

        public bool PostFilterId { get; set; }

        public string Format { get; set; }

        public bool HasEmployeeFilter { get; set; }
        
        public bool HasPeriodFilter { get; set; }

        public string OrgUnitLevel1Name { get; set; }
        public string OrgUnitLevel2Name { get; set; }
        public string OrgUnitLevel3Name { get; set; }

        public IList<SelectListItem> ReportList { get; set; }
        public IList<SelectListItem> RankList { get; set; }
        public IList<SelectListItem> OrganizationUnitList { get; set; }
        public IList<SelectListItem> YearList { get; set; }
        public IList<SelectListItem> MonthList { get; set; }
        public IList<SelectListItem> CoveringPeriodList { get; set; }
        public IList<SelectListItem> PostList { get; set; }
        public IList<SelectListItem> FrequencyList { get; set; }
        public IList<SelectListItem> AttendancePeriodList { get; set; }
    }

    public class ReportHeaderDto
    {
        public string ReportName { get; set; }
        //public string HeaderName { get; set; }
        public string Period { get; set; }
        public string Rank { get; set; }
    }
}
