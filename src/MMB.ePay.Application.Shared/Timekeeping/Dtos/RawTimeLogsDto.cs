﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class RawTimeLogsDto : EntityDto
    {
        public string SwipeCode { get; set; }

        public DateTime LogTime { get; set; }

        public string InOut { get; set; }

        public string Device { get; set; }

        public bool WorkFromHome { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string Address { get; set; }
    }
}