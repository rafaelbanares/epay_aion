﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditExcuseApprovalsDto : EntityDto<int?>
    {

        [Required]
        public int ActedBy { get; set; }

        [Required]
        [StringLength(ExcuseApprovalsConsts.MaxRemarksLength, MinimumLength = ExcuseApprovalsConsts.MinRemarksLength)]
        public string Remarks { get; set; }

        public int ExcuseRequestId { get; set; }

        public int StatusId { get; set; }

    }
}