﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetStatusForEditOutput
    {
        public CreateOrEditStatusDto Status { get; set; }

    }
}