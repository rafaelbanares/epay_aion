﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class LeaveEarningsDto : EntityDto
    {
        public string Employee { get; set; }

        public DateTime Date { get; set; }

        public int AppYear { get; set; }

        public string AppMonth { get; set; }

        public string LeaveType { get; set; }

        public int EmployeeCount { get; set; }

        public decimal TotalEarned { get; set; }

        public string Remarks { get; set; }
    }

    public class LeaveBalancesDto : EntityDto
    {
        public string LeaveType { get; set; }

        public decimal Earned { get; set; }

        public decimal Used { get; set; }

        public decimal Balance { get; set; }
    }

    public class AdminLeaveBalancesDto : EntityDto
    {
        public string Employee { get; set; }

        public string LeaveType { get; set; }

        public decimal Earned { get; set; }

        public decimal Used { get; set; }

        public decimal Balance { get; set; }
    }

    public class LeaveBalanceRequestsDto
    {
        public string Employee { get; set; }
        public DateTime Date { get; set; }
        public decimal? Earned { get; set; }
        public decimal? Used { get; set; }
        public string Remarks { get; set; }
    }

    public class LeaveEarningDetailsDto
    {
        public string Employee { get; set; }

        public decimal Earned { get; set; }
    }
}