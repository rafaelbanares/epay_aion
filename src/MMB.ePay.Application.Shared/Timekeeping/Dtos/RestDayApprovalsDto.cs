﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class RestDayApprovalsDto : EntityDto
    {
        public string RestDayCode { get; set; }

        public string Remarks { get; set; }

        public int RestDayReasonId { get; set; }

        public int StatusId { get; set; }

        public string EmployeeId { get; set; }

        public string Employee { get; set; }

        public DateTime RestDayDate { get; set; }

        public DateTime RestDayStart { get; set; }

        public DateTime RestDayEnd { get; set; }

        public string RestDays { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }

    }
}