﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class LeaveRequestsDto : EntityDto
    {
        public string Remarks { get; set; }

        public int LeaveTypeId { get; set; }

        public int LeaveReasonId { get; set; }

        public int StatusId { get; set; }

        public string LeaveType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }

        public bool IsEditable { get; set; }
    }

    public class LeaveDetailsDto : EntityDto
    {
        public LeaveDetailsDto()
        {
            StatusList = new List<SelectListItem>();
        }

        public string Employee { get; set; }
        public string LeaveType { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal Days { get; set; }
        public string ActualDates { get; set; }
        public string Reason { get; set; }
        public string Remarks { get; set; }
        public DateTime DateFiled { get; set; }
        public string Status { get; set; }

        public IList<SelectListItem> StatusList { get; set; }
    }
}