﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditOfficialBusinessApprovalsDto : EntityDto<int?>
    {

        [Required]
        public int ActedBy { get; set; }

        [Required]
        [StringLength(OfficialBusinessApprovalsConsts.MaxRemarksLength, MinimumLength = OfficialBusinessApprovalsConsts.MinRemarksLength)]
        public string Remarks { get; set; }

        public int OfficialBusinessRequestId { get; set; }

        public int StatusId { get; set; }

    }
}