﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class ShiftReasonsDto : EntityDto
    {
        public string DisplayName { get; set; }

        public string Description { get; set; }

    }
}