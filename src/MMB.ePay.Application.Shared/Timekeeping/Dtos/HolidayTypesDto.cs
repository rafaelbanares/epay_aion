﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class HolidayTypesDto : EntityDto
    {
        public string DisplayName { get; set; }

        public string Description { get; set; }

        public string HolidayCode { get; set; }

        public bool HalfDayEnabled { get; set; }

        public bool IsEditable { get; set; }
    }
}