﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class WorkFlowDto : EntityDto
    {
        public string Status { get; set; }
        public string NextStatus { get; set; }
    }
}