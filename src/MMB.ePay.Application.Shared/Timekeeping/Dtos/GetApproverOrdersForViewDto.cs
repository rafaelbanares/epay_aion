﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetApproverOrdersForViewDto
    {
        public ApproverOrdersDto ApproverOrders { get; set; }

    }
}