﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditEmailQueueDto : EntityDto<int?>
    {
        public int? TenantId { get; set; }

        [Required]
        public string Request { get; set; }

        [Required]
        public string FullName { get; set; }

        [Required]
        public string ApprovalStatus { get; set; }

        [Required]
        public string Remarks { get; set; }

        public string Reason { get; set; }

        [Required]
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required]
        public string Recipient { get; set; }

        [Required]
        public string Status { get; set; }

        public string StatusError { get; set; }
    }

    public class UpdateStatusEmailQueueDto
    {
        public int Id { get; set; }
        public int? TenantId { get; set; }
        public string StatusError { get; set; }
    }

    public class DeleteEmailQueueDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}