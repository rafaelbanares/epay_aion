﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetLocationsForEditOutput
    {
        public CreateOrEditLocationsDto Locations { get; set; }

    }
}