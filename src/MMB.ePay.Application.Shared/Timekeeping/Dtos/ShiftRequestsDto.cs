﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class ShiftRequestsDto : EntityDto
    {
        public string ShiftType { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }

        public DateTime ShiftStart { get; set; }

        public DateTime ShiftEnd { get; set; }

        public string Remarks { get; set; }

        public int ShiftTypeId { get; set; }

        public int ShiftReasonId { get; set; }

        public int StatusId { get; set; }

        public bool IsEditable { get; set; }

    }

    public class ShiftDetailsDto : EntityDto
    {
        public ShiftDetailsDto()
        {
            StatusList = new List<SelectListItem>();
        }

        public string Employee { get; set; }
        public string ShiftType { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Reason { get; set; }
        public string Remarks { get; set; }
        public DateTime DateFiled { get; set; }
        public string Status { get; set; }

        public IList<SelectListItem> StatusList { get; set; }
    }
}