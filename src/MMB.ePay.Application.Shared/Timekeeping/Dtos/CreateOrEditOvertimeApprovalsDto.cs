﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditOvertimeApprovalsDto : EntityDto<int?>
    {

        [Required]
        public int ActedBy { get; set; }

        [Required]
        [StringLength(OvertimeApprovalsConsts.MaxRemarksLength, MinimumLength = OvertimeApprovalsConsts.MinRemarksLength)]
        public string Remarks { get; set; }

        public int OvertimeRequestId { get; set; }

        public int StatusId { get; set; }

    }
}