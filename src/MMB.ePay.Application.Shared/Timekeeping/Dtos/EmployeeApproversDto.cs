﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class EmployeeApproversDto : EntityDto
    {
        public EmployeeApproversDto()
        {
            Approvers = new List<string>();
            ApproverList = new List<Tuple<string, string>>();
        }

        public string Employee { get; set; }

        public IList<string> Approvers { get; set; }

        public IList<Tuple<string, string>> ApproverList { get; set; }

        public short OrderNo { get; set; }

        public int EmployeeId { get; set; }

        public int ApproverId { get; set; }

        public int PageId { get; set; }
    }
}