﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetOvertimeRequestsForViewDto
    {
        public OvertimeRequestsDto OvertimeRequests { get; set; }

        public OvertimeDetailsDto OvertimeDetails { get; set; }
    }
}