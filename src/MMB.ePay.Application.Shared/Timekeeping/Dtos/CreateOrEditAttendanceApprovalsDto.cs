﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditAttendanceApprovalsDto : EntityDto<int?>
    {

        [Required]
        public int ActedBy { get; set; }

        [Required]
        [StringLength(AttendanceApprovalsConsts.MaxRemarksLength, MinimumLength = AttendanceApprovalsConsts.MinRemarksLength)]
        public string Remarks { get; set; }

        public int AttendanceRequestId { get; set; }

        public int StatusId { get; set; }

    }
}