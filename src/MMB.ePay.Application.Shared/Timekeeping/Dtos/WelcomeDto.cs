﻿using System;
using System.Collections.Generic;

namespace MMB.ePay.Timekeeping.Dtos
{
    

    public class WelcomeDto
    {
        public WelcomeDto()
        {
            //ListNotification = new List<Tuple<string, DateTime, string>> ();

            ListTimeEntriesToday = new List<string>();

            EmployeeApprovers = new List<Tuple<string, string>>();
        }

        public bool IsMainApprover { get; set; }

        public bool IsBackupApprover { get; set; }

        public string FullName { get; set; }

        public string Position { get; set; }

        public string Shift { get; set; }

        public string Email { get; set; }

        public string DateEmployed { get; set; }

        public string TransactionPeriod { get; set; }

        public string CurrentDate { get; set; }

        public int AttendancePending { get; set; }

        public int AttendanceCompleted { get; set; }

        public int AttendanceMain { get; set; }

        public int AttendanceBackup { get; set; }

        public int ExcusePending { get; set; }

        public int ExcuseCompleted { get; set; }

        public int ExcuseMain { get; set; }

        public int ExcuseBackup { get; set; }

        public int LeavePending { get; set; }

        public int LeaveCompleted { get; set; }

        public int LeaveMain { get; set; }

        public int LeaveBackup { get; set; }

        public int OBPending { get; set; }

        public int OBCompleted { get; set; }

        public int OBMain { get; set; }

        public int OBBackup { get; set; }

        public int OTPending { get; set; }

        public int OTCompleted { get; set; }

        public int OTMain { get; set; }

        public int OTBackup { get; set; }

        public int RestDayPending { get; set; }

        public int RestDayCompleted { get; set; }

        public int RestDayMain { get; set; }

        public int RestDayBackup { get; set; }

        public int ShiftPending { get; set; }

        public int ShiftCompleted { get; set; }

        public int ShiftMain { get; set; }

        public int ShiftBackup { get; set; }

        //public IList<Tuple<string, DateTime, string>> ListNotification { get; set; }

        public int InvalidCount { get; set; }

        //public int AbsentCount { get; set; }

        public string DefaultShiftName { get; set; }

        public string DefaultTimeIn { get; set; }

        public string DefaultTimeOut { get; set; }

        public string DefaultRestDay { get; set; }

        public string CurrentShiftName { get; set; }

        public string CurrentTimeIn { get; set; }

        public string CurrentTimeOut { get; set; }

        public string CurrentRestDay { get; set; }

        public IList<string> ListTimeEntriesToday { get; set; }

        public IList<Tuple<string, string>> EmployeeApprovers { get; set; }
    }
}
