﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetRestDayApprovalsForViewDto
    {
        public RestDayApprovalsDto RestDayApprovals { get; set; }
        public RestDayDetailsDto RestDayDetails { get; set; }
    }
}