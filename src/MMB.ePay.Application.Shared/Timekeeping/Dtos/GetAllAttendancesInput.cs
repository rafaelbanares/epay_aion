﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllAttendancesInput : PagedAndSortedResultRequestDto
    {
        public int? AttendancePeriodIdFilter { get; set; }
        public int? EmployeeIdFilter { get; set; }
    }
}