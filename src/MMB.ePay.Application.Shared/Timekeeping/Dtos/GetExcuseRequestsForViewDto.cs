﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetExcuseRequestsForViewDto
    {
        public ExcuseRequestsDto ExcuseRequests { get; set; }

        public ExcuseDetailsDto ExcuseDetails { get; set; }
    }
}