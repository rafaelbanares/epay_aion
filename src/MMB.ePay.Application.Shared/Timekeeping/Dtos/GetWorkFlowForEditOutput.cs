﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetWorkFlowForEditOutput
    {
        public CreateOrEditWorkFlowDto WorkFlow { get; set; }

    }
}