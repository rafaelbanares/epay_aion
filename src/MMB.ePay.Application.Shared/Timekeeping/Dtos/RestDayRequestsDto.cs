﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class RestDayRequestsDto : EntityDto
    {
        public string RestDayCode { get; set; }

        public string Remarks { get; set; }

        public int RestDayReasonId { get; set; }

        public int StatusId { get; set; }

        public DateTime RestDayStart { get; set; }

        public DateTime RestDayEnd { get; set; }

        public string RestDays { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }

        public bool IsEditable { get; set; }
    }

    public class RestDayDetailsDto : EntityDto
    {
        public RestDayDetailsDto()
        {
            StatusList = new List<SelectListItem>();
        }

        public string Employee { get; set; }
        public DateTime? RestDayStart { get; set; }
        public DateTime? RestDayEnd { get; set; }
        public string RestDay { get; set; }
        public string Reason { get; set; }
        public string Remarks { get; set; }
        public DateTime DateFiled { get; set; }
        public string Status { get; set; }

        public IList<SelectListItem> StatusList { get; set; }
    }
}