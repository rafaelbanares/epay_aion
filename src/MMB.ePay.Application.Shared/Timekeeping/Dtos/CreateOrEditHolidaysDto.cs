﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditHolidaysDto : EntityDto<int?>
    {

        [Required]
        [StringLength(HolidaysConsts.MaxDisplayNameLength, MinimumLength = HolidaysConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        public DateTime? Date { get; set; }

        [Required]
        public bool HalfDay { get; set; }

        [Required]
        //[StringLength(HolidaysConsts.MaxHolidayTypeLength, MinimumLength = HolidaysConsts.MinHolidayTypeLength)]
        public int HolidayTypeId { get; set; }

        public int? LocationId { get; set; }

        public int Year
        {
            get { return Date.Value.Year; }
        }

    }
}