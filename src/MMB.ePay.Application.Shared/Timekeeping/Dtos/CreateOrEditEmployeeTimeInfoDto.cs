﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditEmployeeTimeInfoDto : EntityDto<int?>
    {
        [StringLength(EmployeeTimeInfoConsts.MaxSwipeCodeLength, MinimumLength = EmployeeTimeInfoConsts.MinSwipeCodeLength)]
        public string SwipeCode { get; set; }

        [StringLength(EmployeeTimeInfoConsts.MaxRestDayLength, MinimumLength = EmployeeTimeInfoConsts.MinRestDayLength)]
        public string RestDay { get; set; }

        public bool Sunday { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }

        [Required]
        public bool Overtime { get; set; }

        [Required]
        public bool Undertime { get; set; }

        [Required]
        public bool Tardy { get; set; }

        [Required]
        public bool NonSwiper { get; set; }

        public int EmployeeId { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Shift Type field is required.")]
        [DisplayName("Shift Type")]
        public int? ShiftTypeId { get; set; }

        [DisplayName("Location")]
        public int? LocationId { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Frequency field is required.")]
        [DisplayName("Frequency")]
        public int? FrequencyId { get; set; }
    }

    public class ValidateEmployeeTimeInfoDto
    {
        public DateTime ActualDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class ValidShiftDto
    {
        public int ShiftTypeId { get; set; }
        public DateTime Date { get; set; }
        public string TimeIn { get; set; }
        public string TimeOut { get; set; }
        public bool StartOnPreviousDay { get; set; }
    }
}