﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetOfficialBusinessRequestsForViewDto
    {
        public OfficialBusinessRequestsDto OfficialBusinessRequests { get; set; }

        public OfficialBusinessDetailsDto OfficialBusinessDetails { get; set; }
    }
}