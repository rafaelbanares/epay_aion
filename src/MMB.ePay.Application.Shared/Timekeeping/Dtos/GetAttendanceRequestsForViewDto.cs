﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAttendanceRequestsForViewDto
    {
        public AttendanceRequestsDto AttendanceRequests { get; set; }

        public AttendanceDetailsDto AttendanceDetails { get; set; }
    }
}