﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class OvertimeRequestsDto : EntityDto
    {
        public DateTime OvertimeDate { get; set; }

        public DateTime OvertimeStart { get; set; }

        public DateTime OvertimeEnd { get; set; }

        public string Reason { get; set; }

        public string Remarks { get; set; }

        public string Status { get; set; }

        public int OvertimeReasonId { get; set; }

        public int StatusId { get; set; }

        public bool IsEditable { get; set; }

    }

    public class OvertimeDetailsDto : EntityDto
    {
        public OvertimeDetailsDto()
        {
            StatusList = new List<SelectListItem>();
        }

        public string Employee { get; set; }
        public DateTime OvertimeDate { get; set; }
        public DateTime? OvertimeStart { get; set; }
        public DateTime? OvertimeEnd { get; set; }
        public string Reason { get; set; }
        public string Remarks { get; set; }
        public DateTime DateFiled { get; set; }
        public string Status { get; set; }

        public IList<SelectListItem> StatusList { get; set; }
    }
}