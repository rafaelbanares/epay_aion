﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditFrequenciesDto : EntityDto<int?>
    {

        [Required]
        [StringLength(FrequenciesConsts.MaxDisplayNameLength, MinimumLength = FrequenciesConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [StringLength(FrequenciesConsts.MaxDescriptionLength, MinimumLength = FrequenciesConsts.MinDescriptionLength)]
        public string Description { get; set; }

    }
}