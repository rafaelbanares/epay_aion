﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditShiftTypeDetailsDto : EntityDto<int?>
    {
        [Required]
        [StringLength(ShiftTypeDetailsConsts.MaxTimeInLength, MinimumLength = ShiftTypeDetailsConsts.MinTimeInLength)]
        public string TimeIn { get; set; }

        [StringLength(ShiftTypeDetailsConsts.MaxBreaktimeInLength, MinimumLength = ShiftTypeDetailsConsts.MinBreaktimeInLength)]
        public string BreaktimeIn { get; set; }

        [StringLength(ShiftTypeDetailsConsts.MaxBreaktimeOutLength, MinimumLength = ShiftTypeDetailsConsts.MinBreaktimeOutLength)]
        public string BreaktimeOut { get; set; }

        [Required]
        [StringLength(ShiftTypeDetailsConsts.MaxTimeOutLength, MinimumLength = ShiftTypeDetailsConsts.MinTimeOutLength)]
        public string TimeOut { get; set; }

        public int ShiftTypeId { get; set; }

        public bool StartOnPreviousDay { get; set; }

        public bool Sunday { get; set; }

        public bool Monday { get; set; }

        public bool Tuesday { get; set; }

        public bool Wednesday { get; set; }

        public bool Thursday { get; set; }

        public bool Friday { get; set; }

        public bool Saturday { get; set; }

    }
}