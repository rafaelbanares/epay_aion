﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetEmployeeTimeInfoForViewDto
    {
        public EmployeeTimeInfoDto EmployeeTimeInfo { get; set; }

    }
}