﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class OvertimeTypesDto : EntityDto
    {
        public string ShortName { get; set; }

        public string Description { get; set; }

        public string OTCode { get; set; }

        public string DisplayFormat { get; set; }

    }
}