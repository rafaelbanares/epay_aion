﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetEmailQueueForViewDto
    {
        public EmailQueueDto EmailQueue { get; set; }
    }
}