﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class OfficialBusinessRequestDetailsDto : EntityDto
    {
        public DateTime OfficialBusinessDate { get; set; }

        public int OfficialBusinessRequestId { get; set; }

    }
}