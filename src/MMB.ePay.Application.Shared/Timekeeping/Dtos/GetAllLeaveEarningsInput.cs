﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllLeaveEarningsInput : PagedAndSortedResultRequestDto
    {
        public short? AppYearFilter { get; set; }

        public short? AppMonthFilter { get; set; }
    }

    public class GetAllLeaveBalancesInput : PagedAndSortedResultRequestDto
    {
        public short? AppYearFilter { get; set; }
    }

    public class GetAdminLeaveBalancesInput : PagedAndSortedResultRequestDto
    {
        public short? AppYearFilter { get; set; }
        public int? EmployeeIdFilter { get; set; }
    }

    public class GetAllLeaveEarningDetailsInput : PagedAndSortedResultRequestDto
    {
        public int Id { get; set; }
    }
}