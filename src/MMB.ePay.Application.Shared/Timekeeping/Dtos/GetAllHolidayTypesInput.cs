﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllHolidayTypesInput : PagedAndSortedResultRequestDto
    {
        public int? YearFilter { get; set; }
    }
}