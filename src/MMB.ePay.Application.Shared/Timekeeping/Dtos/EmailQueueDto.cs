﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class EmailQueueDto : EntityDto
    {
        public int? TenantId { get; set; }
        public string Request { get; set; }
        public string FullName { get; set; }
        public string ApprovalStatus { get; set; }
        public string Remarks { get; set; }
        public string Reason { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Subject { get; set; }
        public string Recipient { get; set; }
        public string Status { get; set; }
        public string StatusError { get; set; }
    }
}