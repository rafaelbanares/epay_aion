﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class ShiftTypesDto : EntityDto
    {
        public string DisplayName { get; set; }

        public string Description { get; set; }

        public string Status { get; set; }

        public short Flexible1 { get; set; }

        public short GracePeriod1 { get; set; }

        public short MinimumOvertime { get; set; }

        public bool RotateShiftWeekly { get; set; }

        //public bool StartOnPreviousDay { get; set; }

        //public DateTime? TimeIn { get; set; }

        //public DateTime? BreaktimeIn { get; set; }

        //public DateTime? BreaktimeOut { get; set; }

        //public DateTime? TimeOut { get; set; }

        public IList<ShiftTypesDtoDetails> Details { get; set; }

        public bool InUse { get; set; }
    }

    public class ShiftTypesDtoDetails
    {
        public DateTime? TimeIn { get; set; }

        public DateTime? BreaktimeIn { get; set; }

        public DateTime? BreaktimeOut { get; set; }

        public DateTime? TimeOut { get; set; }
    }
}