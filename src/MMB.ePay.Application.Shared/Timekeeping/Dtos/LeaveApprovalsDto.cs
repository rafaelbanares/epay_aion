﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class LeaveApprovalsDto : EntityDto
    {
        public string Remarks { get; set; }
        public string EmployeeId { get; set; }
        public int LeaveTypeId { get; set; }
        public int LeaveReasonId { get; set; }
        public string Employee { get; set; }
        public string LeaveType { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal Days { get; set; }
        public string Reason { get; set; }
        public string Status { get; set; }
        public bool HasApproval { get; set; }
        public int? StatusId { get; set; }
    }
}