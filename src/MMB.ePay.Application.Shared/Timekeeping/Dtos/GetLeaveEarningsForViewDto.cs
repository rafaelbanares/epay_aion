﻿using System.Collections.Generic;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetLeaveEarningsForViewDto
    {
        public LeaveEarningsDto LeaveEarnings { get; set; }
    }

    public class GetLeaveBalancesForViewDto
    {
        public LeaveBalancesDto LeaveBalances { get; set; }

        public IList<LeaveBalanceRequestsDto> LeaveBalanceRequestList { get; set; }
    }

    public class GetAdminLeaveBalancesForViewDto
    {
        public AdminLeaveBalancesDto AdminLeaveBalances { get; set; }

        public IList<LeaveBalanceRequestsDto> LeaveBalanceRequestList { get; set; }
    }


    public class GetLeaveEarningDetailsForViewDto
    {
        public LeaveEarningDetailsDto LeaveEarningDetails { get; set; }
    }
}