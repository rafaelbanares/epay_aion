﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetShiftApprovalsForViewDto
    {
        public ShiftApprovalsDto ShiftApprovals { get; set; }
        public ShiftDetailsDto ShiftDetails { get; set; }

    }
}