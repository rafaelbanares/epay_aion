﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetOfficialBusinessReasonsForViewDto
    {
        public OfficialBusinessReasonsDto OfficialBusinessReasons { get; set; }

    }
}