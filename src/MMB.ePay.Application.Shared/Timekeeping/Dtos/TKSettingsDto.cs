﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class TKSettingsDto : EntityDto
    {
        public TKSettingsDto()
        {
            SectionList = new List<Tuple<string, string>>();

            SettingsList = new List<Tuple<int, string, string, string, string>>();
        }

        public IList<Tuple<string, string>> SectionList { get; set; }

        public IList<Tuple<int, string, string, string, string>> SettingsList { get; set; }
    }
}