﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditOvertimeTypesDto : EntityDto<int?>
    {

        [Required]
        [StringLength(OvertimeTypesConsts.MaxShortNameLength, MinimumLength = OvertimeTypesConsts.MinShortNameLength)]
        public string ShortName { get; set; }

        [Required]
        [StringLength(OvertimeTypesConsts.MaxDescriptionLength, MinimumLength = OvertimeTypesConsts.MinDescriptionLength)]
        public string Description { get; set; }

        public short SubType { get; set; }

        [Required]
        [StringLength(OvertimeTypesConsts.MaxOTCodeLength, MinimumLength = OvertimeTypesConsts.MinOTCodeLength)]
        public string OTCode { get; set; }
    }
}