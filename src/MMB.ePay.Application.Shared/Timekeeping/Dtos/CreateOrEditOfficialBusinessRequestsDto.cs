﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using static MMB.ePay.Application.Shared.CustomValidations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditOfficialBusinessRequestsDto : EntityDto<int?>
    {
        [Required]
        [DisplayName("Start Date")]
        [BeforeEndDate(EndDatePropertyName = "EndDate")]
        public DateTime? StartDate { get; set; }

        [Required]
        [DisplayName("End Date")]
        public DateTime? EndDate { get; set; }

        public bool WholeDay { get; set; }

        public TimeSpan? StartTime { get; set; }

        public TimeSpan? EndTime { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Official Business Reason field is required.")]
        [DisplayName("Official Business Reason")]
        public int? OfficialBusinessReasonId { get; set; }


        [Required]
        [StringLength(OfficialBusinessRequestsConsts.MaxRemarksLength, MinimumLength = OfficialBusinessRequestsConsts.MinRemarksLength)]
        [DisplayName("Remarks")]
        public string Remarks { get; set; }
    }

    public class UpdateOfficialBusinessRequestsStatus
    {
        public int Id { get; set; }

        public int StatusId { get; set; }

        public string Reason { get; set; }
    }

    public class ApproveAllOfficialBusinessRequests
    {
        public int[] Ids { get; set; }
    }
}