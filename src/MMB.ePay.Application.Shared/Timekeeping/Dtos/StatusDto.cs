﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class StatusDto : EntityDto
    {
        public string StatusCode { get; set; }

        public string DisplayName { get; set; }

        public string AfterStatusMessage { get; set; }

        public string BeforeStatusActionText { get; set; }
    }
}