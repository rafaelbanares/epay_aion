﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditShiftReasonsDto : EntityDto<int?>
    {

        [Required]
        [StringLength(ShiftReasonsConsts.MaxDisplayNameLength, MinimumLength = ShiftReasonsConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [StringLength(ShiftReasonsConsts.MaxDescriptionLength, MinimumLength = ShiftReasonsConsts.MinDescriptionLength)]
        public string Description { get; set; }

    }
}