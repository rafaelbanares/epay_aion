﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditLeaveTypesDto : EntityDto<int?>
    {

        [Required]
        [StringLength(LeaveTypesConsts.MaxDisplayNameLength, MinimumLength = LeaveTypesConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        [StringLength(LeaveTypesConsts.MaxDescriptionLength, MinimumLength = LeaveTypesConsts.MinDescriptionLength)]
        public string Description { get; set; }

        public bool WithPay { get; set; }

        public int? AdvancedFilingInDays { get; set; }
    }
}