﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditLeaveRequestDetailsDto : EntityDto<int?>
    {

        [Required]
        public DateTime LeaveDate { get; set; }

        [Required]
        public decimal Days { get; set; }

        [Required]
        public bool FirstHalf { get; set; }

        public int LeaveRequestId { get; set; }

    }
}