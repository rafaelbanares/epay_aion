﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using static MMB.ePay.Application.Shared.CustomValidations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditRestDayRequestsDto : EntityDto<int?>
    {

        public string RestDayCode { get; set; }

        [Required]
        [StringLength(RestDayRequestsConsts.MaxRemarksLength, MinimumLength = RestDayRequestsConsts.MinRemarksLength)]
        [DisplayName("Remarks")]
        public string Remarks { get; set; }

        [Required]
        [DisplayName("StartDate")]
        [BeforeEndDate(EndDatePropertyName = "RestDayEnd")]
        public DateTime? RestDayStart { get; set; }

        [Required]
        [DisplayName("EndDate")]
        public DateTime? RestDayEnd { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Rest Day Reason field is required.")]
        [DisplayName("Rest Day Reason")]
        public int? RestDayReasonId { get; set; }

        [DisplayName("Sunday")]
        public bool Sunday { get; set; }

        [DisplayName("Monday")]
        public bool Monday { get; set; }

        [DisplayName("Tuesday")]
        public bool Tuesday { get; set; }

        [DisplayName("Wednesday")]
        public bool Wednesday { get; set; }

        [DisplayName("Thursday")]
        public bool Thursday { get; set; }

        [DisplayName("Friday")]
        public bool Friday { get; set; }

        [DisplayName("Saturday")]
        public bool Saturday { get; set; }

    }

    public class UpdateRestDayRequestsStatus
    {
        public int Id { get; set; }

        public int StatusId { get; set; }

        public string Reason { get; set; }
    }

    public class ApproveAllRestDayRequests
    {
        public int[] Ids { get; set; }
    }
}