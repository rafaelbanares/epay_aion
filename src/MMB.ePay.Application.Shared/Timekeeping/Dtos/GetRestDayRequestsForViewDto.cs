﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetRestDayRequestsForViewDto
    {
        public RestDayRequestsDto RestDayRequests { get; set; }

        public RestDayDetailsDto RestDayDetails { get; set; }
    }
}