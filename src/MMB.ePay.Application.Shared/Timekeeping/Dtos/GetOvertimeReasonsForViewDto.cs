﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetOvertimeReasonsForViewDto
    {
        public OvertimeReasonsDto OvertimeReasons { get; set; }

    }
}