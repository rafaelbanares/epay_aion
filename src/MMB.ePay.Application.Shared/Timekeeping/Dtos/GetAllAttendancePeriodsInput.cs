﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllAttendancePeriodsInput : PagedAndSortedResultRequestDto
    {
        public int? FrequencyFilter { get; set; }

        public int? YearFilter { get; set; }
    }
}