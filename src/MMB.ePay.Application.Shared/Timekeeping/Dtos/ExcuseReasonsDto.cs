﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class ExcuseReasonsDto : EntityDto
    {
        public string DisplayName { get; set; }

        public string Description { get; set; }

    }
}