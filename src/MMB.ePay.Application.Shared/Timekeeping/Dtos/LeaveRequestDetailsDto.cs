﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class LeaveRequestDetailsDto : EntityDto
    {
        public DateTime LeaveDate { get; set; }

        public decimal Days { get; set; }

        public bool FirstHalf { get; set; }

        public int LeaveRequestId { get; set; }

    }
}