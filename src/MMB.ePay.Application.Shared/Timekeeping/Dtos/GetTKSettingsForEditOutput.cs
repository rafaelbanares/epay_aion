﻿using System.Collections.Generic;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetTKSettingsForEditOutput
    {
        public TKSettingsDto TKSettings { get; set; }

    }
}