﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllApproverOrdersInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public short? MaxOrderNoFilter { get; set; }
        public short? MinOrderNoFilter { get; set; }

        public string DisplayNameFilter { get; set; }

        public int? MaxLevelFilter { get; set; }
        public int? MinLevelFilter { get; set; }

        public int? IsBackupFilter { get; set; }

    }
}