﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetLeaveApprovalsForViewDto
    {
        public LeaveApprovalsDto LeaveApprovals { get; set; }

        public LeaveDetailsDto LeaveDetails { get; set; }
    }
}