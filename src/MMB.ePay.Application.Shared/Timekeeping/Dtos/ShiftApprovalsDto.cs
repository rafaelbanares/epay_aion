﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class ShiftApprovalsDto : EntityDto
    {
        public string EmployeeId { get; set; }

        public string Employee { get; set; }

        public string ShiftType { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }

        public DateTime ShiftStart { get; set; }

        public DateTime ShiftEnd { get; set; }

        public string Remarks { get; set; }

        public int ShiftTypeId { get; set; }

        public int ShiftReasonId { get; set; }

        public int StatusId { get; set; }
    }
}