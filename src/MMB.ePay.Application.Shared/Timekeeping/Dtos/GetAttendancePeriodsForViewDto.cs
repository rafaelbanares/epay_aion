﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAttendancePeriodsForViewDto
    {
        public AttendancePeriodsDto AttendancePeriods { get; set; }

    }
}