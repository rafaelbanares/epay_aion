﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllLeaveRequestsInput : PagedAndSortedResultRequestDto
    {
        public int? AttendancePeriodIdFilter { get; set; }
        public int? StatusIdFilter { get; set; }
    }

    public class GetLeaveBalancesInput
    {
        public int? EmployeeId { get; set; }
        public short AppYear { get; set; }
        public int? LeaveTypeId { get; set; }
    }
}