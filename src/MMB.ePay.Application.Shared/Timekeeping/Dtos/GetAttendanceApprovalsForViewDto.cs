﻿namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAttendanceApprovalsForViewDto
    {
        public AttendanceApprovalsDto AttendanceApprovals { get; set; }

        public AttendanceDetailsDto AttendanceDetails { get; set; }
    }
}