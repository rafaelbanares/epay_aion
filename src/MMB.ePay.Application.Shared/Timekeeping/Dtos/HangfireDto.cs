﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class HangfireDto : EntityDto<string>
    {
        public string DisplayName { get; set; }

        public string Queue { get; set; }

        public string Cron { get; set; }

        public DateTime? NextExecution { get; set; }

        public DateTime? LastExecution { get; set; }
    }
}
