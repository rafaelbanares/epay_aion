﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class OvertimeReasonsDto : EntityDto
    {
        public string DisplayName { get; set; }

        public string Description { get; set; }

    }
}