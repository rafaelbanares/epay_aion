﻿using System;
using System.Collections.Generic;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetEmployeeApproversForViewDto
    {
        public EmployeeApproversDto EmployeeApprovers { get; set; }
    }
}