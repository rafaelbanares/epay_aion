﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditApproverOrdersDto : EntityDto<int?>
    {

        [Required]
        [StringLength(ApproverOrdersConsts.MaxDisplayNameLength, MinimumLength = ApproverOrdersConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Level field is required.")]
        public int? Level { get; set; }

        public bool IsBackup { get; set; }
    }
}