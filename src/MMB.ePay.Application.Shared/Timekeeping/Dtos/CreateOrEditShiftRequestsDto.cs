﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using static MMB.ePay.Application.Shared.CustomValidations;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class CreateOrEditShiftRequestsDto : EntityDto<int?>
    {

        [Required]
        [DisplayName("Shift Start")]
        [BeforeEndDate(EndDatePropertyName = "ShiftEnd")]
        public DateTime? ShiftStart { get; set; }

        [Required]
        [DisplayName("Shift End")]
        public DateTime? ShiftEnd { get; set; }

        [Required]
        [StringLength(ShiftRequestsConsts.MaxRemarksLength, MinimumLength = ShiftRequestsConsts.MinRemarksLength)]
        [DisplayName("Remarks")]
        public string Remarks { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Shift Type field is required.")]
        [DisplayName("Shift Type")]
        public int? ShiftTypeId { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The Shift Reason field is required.")]
        [DisplayName("Shift Reason")]
        public int ? ShiftReasonId { get; set; }
    }

    public class UpdateShiftRequestsStatus
    {
        public int Id { get; set; }

        public int StatusId { get; set; }

        public string Reason { get; set; }
    }

    public class ShiftDto
    {
        public DateTime Date { get; set; }

        public string TimeIn { get; set; }

        public string TimeOut { get; set; }

        public virtual DateTime StartDate
        {
            get { return DateTime.Parse(Date.ToShortDateString() + " " + TimeIn); }
        }

        public virtual DateTime EndDate
        {
            get { return DateTime.Parse(Date.ToShortDateString() + " " + TimeOut); }
        }
    }

    public class DateRangeDto
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class ApproveAllShiftRequests
    {
        public int[] Ids { get; set; }
    }
}