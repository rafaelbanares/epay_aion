﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllShiftTypeDetailsInput : PagedAndSortedResultRequestDto
    {
        public int? ShiftTypeIdFilter { get; set; }
    }
}