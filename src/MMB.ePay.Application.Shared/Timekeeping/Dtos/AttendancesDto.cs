﻿using Abp.Application.Services.Dto;
using CsvHelper.Configuration.Attributes;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;

namespace MMB.ePay.Timekeeping.Dtos
{
	public class AttendancesDto : EntityDto
    {
        public AttendancesDto()
        {
            OvertimeList = new List<AttendanceOvertimeDto>();
        }

        public DateTime Date { get; set; }

        public int EmployeeId { get; set; }

        public string Day { get; set; }

        public string Shift { get; set; }

        public string ActualShift { get; set; }

        public DateTime? TimeIn { get; set; }

        public DateTime? BreaktimeIn { get; set; }

        public DateTime? BreaktimeOut { get; set; }

        public DateTime? PMBreaktimeIn { get; set; }

        public DateTime? PMBreaktimeOut { get; set; }

        public DateTime? TimeOut { get; set; }

        public string DayType { get; set; }

        public string Remarks { get; set; }

        public decimal Absence { get; set; }

        public decimal Tardy { get; set; }

        public decimal Undertime { get; set; }

        //public decimal Regular { get; set; }

        //public decimal OfficialBusiness { get; set; }

        //public decimal ND1 { get; set; }

        //public decimal ND2 { get; set; }

        public string Regular { get; set; }

        public string OfficialBusiness { get; set; }

        public string ND1 { get; set; }

        public string ND2 { get; set; }

        public decimal ExcessHours { get; set; }

        public bool IsPosted { get; set; }

        public List<AttendanceOvertimeDto> OvertimeList { get; set; }

    }

    public class AttendanceOvertimeDto
    {
        public string OvertimeType { get; set; }

        //public decimal Hours { get; set; }
        public string Hours { get; set; }
    }
    public class AttendanceUploadsDto
    {
        public string EmployeeCode { get; set; }

        public DateTime Date { get; set; }

        public string TimeIn { get; set; }

        public string LunchBreaktimeIn { get; set; }

        public string LunchBreaktimeOut { get; set; }

        public string PMBreaktimeIn { get; set; }

        public string PMBreaktimeOut { get; set; }

        public string TimeOut { get; set; }
    }

    public class CreateAttendanceUploadsDto
    {
        public List<AttendanceUploadsDto> AttendanceUploads { get; set; }
    }

    public class RawTimeLogStagingDto
    {
        public string SwipeCode { get; set; }

        public DateTime LogTime { get; set; }

        public string InOut { get; set; }
    }

    public class CreateRawTimeLogStagingDto
    {
        public List<RawTimeLogStagingDto> RawTimeLogStaging { get; set; }

        public Guid UploadSessionId { get; set; }
    }

    public class AttendancesUploadDto
    {
        public AttendancesUploadDto()
        {
            Count = -1;
        }

        public int FrequencyId { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public IFormFile File { get; set; }

        public string FileType => Path.GetExtension(File?.FileName);

        public int Count { get; set; }

        public int FinalCount { get; set; }

        //public string Biometric { get; set; }

        //[Required]
        //[DisplayName("Frequency")]
        //public int? FrequencyId { get; set; }

        public string CustomError { get; set; }

        public Guid UploadSessionId { get; set; }

	}

    public class InvalidAttendancesDto
    {
		public string Employee { get; set; }

		public string Date { get; set; }

        public string TimeIn { get; set; }

        public string BreaktimeIn { get; set; }

        public string BreaktimeOut { get; set; }

        public string TimeOut { get; set; }
    }

    public class UploadFileFace200SX
    {
        [Index(0)]
        public string NonDelimiter { get; set; }
    }

    public class UploadFileFace400
    {
        [Index(0)]
        public string UserID { get; set; }

        [Index(1)]
        public string EmployeeCode { get; set; }

        [Index(2)]
        public string Name { get; set; }

        [Index(3)]
        public string Dept { get; set; }

        [Index(4)]
        public DateTime Att_Time { get; set; }
    }

    public class UploadFileTobys2
    {
        public string EmployeeCode { get; set; }

        public DateTime Date { get; set; }

        public string TimeIn { get; set; }

        public string LunchBreaktimeIn { get; set; }

        public string LunchBreaktimeOut { get; set; }

        public string PMBreaktimeIn { get; set; }

        public string PMBreaktimeOut { get; set; }

        public string TimeOut { get; set; }

    }

    public class UploadFileMMB
    {
        [Index(0)]
        public string No { get; set; }

        [Index(1)]
        public string DN { get; set; }

        [Index(2)]
        public string UID { get; set; }

        [Index(3)]
        public string Name { get; set; }

        [Index(4)]
        public string Status { get; set; }

        [Index(5)]
        public string Action { get; set; }

        [Index(6)]
        public string APB { get; set; }

        [Index(7)]
        public string JobCode { get; set; }

        [Index(8)]
        public DateTime DateTime { get; set; }
    }
}