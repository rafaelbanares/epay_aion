﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.Timekeeping.Dtos
{
    public class GetAllRawTimeLogsInput : PagedAndSortedResultRequestDto
    {
        public int? EmployeeIdFilter { get; set; }

        public DateTime? DateFilter { get; set; }
    }
}