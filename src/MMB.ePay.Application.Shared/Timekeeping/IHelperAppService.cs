﻿using Abp.Application.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace MMB.ePay.Timekeeping
{
    public interface IHelperAppService : IApplicationService
    {
        IList<SelectListItem> GetYearList();
        IList<SelectListItem> GetApproverTypeList();
        IList<SelectListItem> GetSubTypeList();
        IList<SelectListItem> GetCoveringPeriodList();
        IList<SelectListItem> GetPostList();
        IList<SelectListItem> GetStatusTypeList();
        IList<SelectListItem> GetRankList();
        IList<SelectListItem> GetEmployeeStatusList();
        IList<SelectListItem> GetEmployeeStatusListForFilter();
        IList<SelectListItem> GetMonthList();
        IList<SelectListItem> GetMonthListForFilter();
        string GetRankById(string id);
        decimal GetMinuteFormat(decimal minutes);
        string GetMinuteFormatString(decimal minutes);
    }
}