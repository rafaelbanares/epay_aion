﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IAttendanceApprovalsAppService : IApplicationService
    {
        Task<PagedResultDto<GetAttendanceApprovalsForViewDto>> GetAll(GetAllAttendanceApprovalsInput input);
        Task<PagedResultDto<GetAttendanceApprovalsForViewDto>> GetAdminApprovals(GetAllAttendanceApprovalsInput input);
        Task<PagedResultDto<GetAttendanceApprovalsForViewDto>> GetApprovalRequests(GetAllAttendanceApprovalsInput input);
        Task<GetAttendanceApprovalsForViewDto> GetAttendanceApprovalsForView(int id);
        Task ApproveAll(ApproveAllAttendanceRequests input);
        Task UpdateStatus(UpdateAttendanceRequestsStatus input);
        Task AdminUpdateStatus(UpdateAttendanceRequestsStatus input);
    }
}