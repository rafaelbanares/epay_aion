﻿using Abp.Application.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IReportsAppService : IApplicationService
    {
        Task<string> GetReportName(int id);

        Task<GetReportNamesDto> GetControllerName(int id);

        Task<IList<SelectListItem>> GetReportTypeList();

        Task<GetReportsForViewDto> GetReportsForView(int id);

        Task<GetReportsForActiveEmployees> GetActiveEmployees(ReportsDto dto);

        Task<GetReportsForDailyAttendance> GetDailyAttendance(ReportsDto dto);

        Task<GetReportsForAttendanceWithBreaks> GetAttendanceWithBreaks(ReportsDto dto);

        Task<GetReportsForWorkFromHome> GetWorkFromHome(ReportsDto dto);

        Task<GetReportsForAttendanceSummary> GetAttendanceSummary(ReportsDto dto);

        Task<GetReportsForAttendanceSummaryPayroll> GetAttendanceSummaryPayroll(ReportsDto dto);

        Task<GetReportsForAttendanceSummaryPayroll> GetAttendanceSummaryPayrollWithMinuteFormat(ReportsDto dto);

        Task<GetReportsForLeaveBalance> GetLeaveBalance(ReportsDto dto);

        Task<GetReportsForLeaveSummary> GetLeaveSummary(ReportsDto dto);

        Task<GetReportsForLeaveSummaryPayroll> GetLeaveSummaryPayroll(ReportsDto dto);

        Task<GetReportsForOvertimeSummary> GetOvertimeSummary(ReportsDto dto);

        Task<GetReportsForOvertimeSummaryPayroll> GetOvertimeSummaryPayroll(ReportsDto dto);

        Task<GetReportsForOvertimeSummaryPayroll> GetOvertimeSummaryPayrollWithMinuteFormat(ReportsDto dto);

        Task<GetReportsForAttendanceTransaction> GetAttendanceTransaction(ReportsDto dto);

        Task<GetReportsForExcuseTransaction> GetExcuseTransaction(ReportsDto dto);

        Task<GetReportsForLeaveTransaction> GetLeaveTransaction(ReportsDto dto);

        Task<GetReportsForOfficialBusinessTransaction> GetOfficialBusinessTransaction(ReportsDto dto);

        Task<GetReportsForOvertimeTransaction> GetOvertimeTransaction(ReportsDto dto);

        Task<GetReportsForRestDayTransaction> GetRestDayTransaction(ReportsDto dto);

        Task<GetReportsForShiftTransaction> GetShiftTransaction(ReportsDto dto);

        //Task<GetReportsForPayslip> GetPayslip(int employeeId, int payrollNo);
    }
}
