﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
	public interface IEmployeeApproversAppService : IApplicationService
    {
        Task<PagedResultDto<GetEmployeeApproversForViewDto>> GetAll(GetAllEmployeeApproversInput input);

        Task<GetEmployeeApproversForViewDto> GetEmployeeApproversForView(int id);

        Task<GetEmployeeApproversForEditOutput> GetEmployeeApproversForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditEmployeeApproversDto input);

        Task Delete(EntityDto input);

    }
}