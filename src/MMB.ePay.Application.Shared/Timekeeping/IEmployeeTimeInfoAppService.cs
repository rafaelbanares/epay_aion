﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IEmployeeTimeInfoAppService : IApplicationService
    {
        Task<PagedResultDto<GetEmployeeTimeInfoForViewDto>> GetAll(GetAllEmployeeTimeInfoInput input);

        Task<GetEmployeeTimeInfoForViewDto> GetEmployeeTimeInfoForView(int id);

        Task<GetEmployeeTimeInfoForViewDto> GetEmployeeTimeInfoByUserId(long id);

        Task<GetEmployeeTimeInfoForEditOutput> GetEmployeeTimeInfoForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditEmployeeTimeInfoDto input);

        Task Delete(EntityDto input);
    }
}