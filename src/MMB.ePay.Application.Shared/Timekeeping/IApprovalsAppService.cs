﻿using Abp.Application.Services;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IApprovalsAppService : IApplicationService
    {
        Task<ApprovalsViewModel> GetApprovalFilters(bool type);
        Task<ApprovalsViewModel> GetAdminApprovalFilters();
        Task<RequestsViewModel> GetRequestsFilters();
        Task SendApprovalDetails(EmailRequestDto dto);
    }
}