﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping.Dtos;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IShiftApprovalsAppService : IApplicationService
    {
        Task<PagedResultDto<GetShiftApprovalsForViewDto>> GetAll(GetAllShiftApprovalsInput input);
        Task<PagedResultDto<GetShiftApprovalsForViewDto>> GetAdminApprovals(GetAllShiftApprovalsInput input);
        Task<PagedResultDto<GetShiftApprovalsForViewDto>> GetApprovalRequests(GetAllShiftApprovalsInput input);
        Task<GetShiftApprovalsForViewDto> GetShiftApprovalsForView(int id);
        Task ApproveAll(ApproveAllShiftRequests input);
        Task UpdateStatus(UpdateShiftRequestsStatus input);
        Task AdminUpdateStatus(UpdateShiftRequestsStatus input);
    }
}