﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IHolidaysAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetYearListForFilter();

        Task<PagedResultDto<GetHolidaysForViewDto>> GetAll(GetAllHolidaysInput input);

        Task<GetHolidaysForViewDto> GetHolidaysForView(int id);

        Task<GetHolidaysForEditOutput> GetHolidaysForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditHolidaysDto input);

        Task Delete(EntityDto input);

    }
}