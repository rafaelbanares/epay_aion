﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface IOvertimeTypesAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetOvertimeTypeList();
        Task<IList<SelectListItem>> GetOvertimeTypeOTCodeList(string otCode);
        Task<PagedResultDto<GetOvertimeTypesForViewDto>> GetAll(GetAllOvertimeTypesInput input);
        Task<GetOvertimeTypesForViewDto> GetOvertimeTypesForView(int id);
        Task<GetOvertimeTypesForEditOutput> GetOvertimeTypesForEdit(int id);
        Task CreateOrEdit(CreateOrEditOvertimeTypesDto input);
        Task Delete(int id);
    }
}