﻿--RUN FOR ALL TENANTS

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tk].[usp_UnPostAttendance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [tk].[usp_UnPostAttendance] AS' 
END
GO


ALTER PROCEDURE [tk].[usp_UnPostAttendance]
(
	@AttendancePeriodId int,
	@TenantId int
)
AS
BEGIN
	SET NOCOUNT ON	
	SET XACT_ABORT ON

	BEGIN TRY		
		DECLARE @StartDate DATE
		DECLARE @EndDate DATE
		DECLARE @frequencyId int
		DECLARE @Posted bit
		
 
		SELECT 
			@StartDate = a.StartDate,
			@EndDate = a.EndDate,
			@frequencyId = FrequencyId,
			@Posted = Posted
		FROM tk.AttendancePeriods a
		WHERE Id = @AttendancePeriodId
			AND TenantId = @TenantId;

		
		--Check if there are newer posted period within the same frequency
		DECLARE @newerPostedPeriodCnt int ;
		select @newerPostedPeriodCnt = count(*) 
		From tk.AttendancePeriods 
		where 
			TenantId = @TenantId AND 
			StartDate > @EndDate AND
			FrequencyId = @frequencyId AND
			Posted = 1 ;

		declare @PostedAttendances_DeletedRows int = 0;
		declare @PostedLeaves_DeletedRows int = 0;
		declare @PostedOvertimes_DeletedRows int = 0;
		declare @PostedTimesheets_DeletedRows int = 0;

		-- Allow unpost only if already posted and there is no newer posted period
		IF @Posted = 1 AND @newerPostedPeriodCnt = 0
		BEGIN
			BEGIN TRANSACTION    

 			--unpost
			UPDATE tk.AttendancePeriods SET Posted = 0
			where id = @AttendancePeriodId and TenantId = @TenantId ;

			-- delete posted data
			DELETE tk.PostedAttendances
			WHERE TenantId = @TenantId 
				AND [Date] >= @StartDate 
				AND [Date] <= @EndDate ;
			
			SET @PostedAttendances_DeletedRows = @@ROWCOUNT ;


			DELETE tk.PostedLeaves
			WHERE TenantId = @TenantId 
				AND [Date] >= @StartDate 
				AND [Date] <= @EndDate ;

			SET @PostedLeaves_DeletedRows = @@ROWCOUNT ;


			DELETE tk.PostedOvertimes
			WHERE TenantId = @TenantId 
				AND [Date] >= @StartDate 
				AND [Date] <= @EndDate ;

			SET @PostedOvertimes_DeletedRows = @@ROWCOUNT ;


			DELETE tk.PostedTimesheets
			WHERE TenantId = @TenantId 
				AND [Date] >= @StartDate 
				AND [Date] <= @EndDate ;

			SET @PostedTimesheets_DeletedRows = @@ROWCOUNT ;


			COMMIT TRANSACTION;

			SELECT 
				CAST(1 as BIT) as [Success], 
				@Posted as [Posted],
				@newerPostedPeriodCnt as [newerPostedPeriodCnt],
				@PostedAttendances_DeletedRows as [PostedAttendances_DeletedRows],
				@PostedLeaves_DeletedRows as PostedLeaves_DeletedRows,
				@PostedOvertimes_DeletedRows as [PostedOvertimes_DeletedRows],
				@PostedTimesheets_DeletedRows as [PostedTimesheets_DeletedRows] ;
		END
		ELSE
		BEGIN
			SELECT 
				CAST(0 as BIT) as [Success], 
				@Posted as [Posted],
				@newerPostedPeriodCnt as [newerPostedPeriodCnt],
				@PostedAttendances_DeletedRows as [PostedAttendances_DeletedRows],
				@PostedLeaves_DeletedRows as PostedLeaves_DeletedRows,
				@PostedOvertimes_DeletedRows as [PostedOvertimes_DeletedRows],
				@PostedTimesheets_DeletedRows as [PostedTimesheets_DeletedRows] ;

		END

	END TRY

	BEGIN CATCH
		IF @@trancount > 0 ROLLBACK TRANSACTION
		EXEC dbo.usp_error_handler_sp
		RETURN 55555
	END CATCH
	
END


