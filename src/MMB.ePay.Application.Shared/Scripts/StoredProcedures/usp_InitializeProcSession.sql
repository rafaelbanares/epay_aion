﻿--RUN FOR ALL TENANTS

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tk].[usp_InitializeProcSession]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [tk].[usp_InitializeProcSession] AS' 
END
GO


-- Initialize ProcSession and insert active, regular employees for batch processing
-- Parameters:
--   @AppSessionId: guid unique session id from the calling application
--   @payrollNo: payroll# to process
--   @tenantId: tenant id to process
ALTER PROCEDURE [tk].[usp_InitializeProcSession]
(
	@AppSessionId	uniqueidentifier,
	@AttendancePeriodId int,
	@TenantId int,
	@EmployeeId int = null
)
AS
BEGIN	 
	SET NOCOUNT ON	
	SET XACT_ABORT ON

	DECLARE @Err int
	DECLARE @PsId int
	DECLARE @frequencyId int
 
	BEGIN TRANSACTION

	SELECT 
		@frequencyId = FrequencyId
	FROM AttendancePeriods a
	WHERE Id = @AttendancePeriodId
		AND TenantId = @TenantId;

	INSERT INTO [tk].[ProcSessions]([TenantId],[AppSessionId],[AttendancePeriodId],[ProcessStart])
	VALUES(@TenantId, @AppSessionId, @AttendancePeriodId, GETDATE());
	
	SET @PsId = scope_identity();

	INSERT INTO [tk].[ProcSessionBatches]([PsId],[EmployeeId])
	SELECT @PsId as [ProcSessionId], 
			e.Id as [EmployeeId]
	FROM hr.Employees e
	INNER JOIN tk.EmployeeTimeInfo eti
		ON e.Id = eti.EmployeeId
		AND e.TenantId = eti.TenantId
	WHERE 
		e.TenantId = @TenantId AND
		e.EmployeeStatus = 'ACTIVE' AND 		
		eti.FrequencyId = @FrequencyId AND
		(e.Id = @EmployeeId OR @EmployeeId IS NULL) ;
		

	COMMIT TRANSACTION

	SET @Err = @@Error

	SELECT @Err
END


GO