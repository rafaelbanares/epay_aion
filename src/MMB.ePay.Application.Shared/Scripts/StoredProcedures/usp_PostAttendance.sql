--RUN FOR ALL TENANTS

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tk].[usp_PostAttendance]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [tk].[usp_PostAttendance] AS' 
END
GO


ALTER PROCEDURE [tk].[usp_PostAttendance]
(
	@AttendancePeriodId int,
	@TenantId int
)
AS
BEGIN
	SET NOCOUNT ON	
	SET XACT_ABORT ON

	BEGIN TRY		
		DECLARE @StartDate DATE
		DECLARE @EndDate DATE
		DECLARE @frequencyId int
		DECLARE @Posted bit
		DECLARE @StatusId INT
 
		SELECT 
			@StartDate = a.StartDate,
			@EndDate = a.EndDate,
			@frequencyId = FrequencyId,
			@Posted = Posted
		FROM tk.AttendancePeriods a
		WHERE Id = @AttendancePeriodId
			AND TenantId = @TenantId;

		SELECT
			@StatusId = Id
		FROM tk.[Status]
		WHERE TenantId = @TenantId
			AND DisplayName = 'Cancelled';

		IF @Posted = 0
		BEGIN
			BEGIN TRANSACTION    

			UPDATE a
			SET a.StatusId = @StatusId
			FROM
				tk.LeaveRequests a
				INNER JOIN tk.LeaveRequestDetails b
				ON a.Id = b.LeaveRequestId
				INNER JOIN tk.[Status] c
				ON a.StatusId = c.Id
			WHERE
				a.TenantId = @TenantId
				AND b.LeaveDate BETWEEN @StartDate AND @EndDate
				AND NOT (c.IsEditable = 0 AND c.AccessLevel = 0);

			INSERT INTO tk.PostedAttendances
				(TenantId, EmployeeId, [Date], RestDayCode, TimeIn, BreaktimeIn, BreaktimeOut, PMBreaktimeIn, PMBreaktimeOut, [TimeOut], Remarks, ShiftTypeId, CreationTime, CreatorUserId)
			SELECT
				a.TenantId, a.EmployeeId, [Date], RestDayCode, TimeIn, BreaktimeIn, BreaktimeOut, PMBreaktimeIn, PMBreaktimeOut, [TimeOut], Remarks, a.ShiftTypeId, CreationTime, CreatorUserId
			FROM tk.Attendances a INNER JOIN [tk].[EmployeeTimeInfo] e
			ON a.EmployeeId = e.EmployeeId and
				a.TenantId = e.TenantId
			WHERE a.TenantId = @TenantId 
				AND a.[Date] >= @StartDate 
				AND a.[Date] <= @EndDate
				AND e.FrequencyId = @frequencyId ;


			INSERT INTO tk.PostedLeaves
				(TenantId, EmployeeId, [Date], LeaveTypeId, [Days], CreationTime, CreatorUserId)
			SELECT
				a.TenantId, a.EmployeeId, [Date], LeaveTypeId, [Days], CreationTime, CreatorUserId
			FROM tk.ProcessedLeaves a INNER JOIN [tk].[EmployeeTimeInfo] e
			ON a.EmployeeId = e.EmployeeId and
				a.TenantId = e.TenantId
			WHERE a.TenantId = @TenantId 
				AND a.[Date] >= @StartDate 
				AND a.[Date] <= @EndDate
				AND e.FrequencyId = @frequencyId ;


			INSERT INTO tk.PostedOvertimes
				(TenantId, EmployeeId, [Date], OvertimeTypeId, [Minutes], CreationTime, CreatorUserId)
			SELECT
				a.TenantId, a.EmployeeId, [Date], OvertimeTypeId, [Minutes], CreationTime, CreatorUserId
			FROM tk.ProcessedOvertimes a INNER JOIN [tk].[EmployeeTimeInfo] e
			ON a.EmployeeId = e.EmployeeId and
				a.TenantId = e.TenantId
			WHERE a.TenantId = @TenantId 
				AND a.[Date] >= @StartDate 
				AND a.[Date] <= @EndDate
				AND e.FrequencyId = @frequencyId ;


			INSERT INTO tk.PostedTimesheets
				(TenantId, EmployeeId, [Date], RegMins, Absence, TardyMins, UtMins, RegNd1Mins, RegNd2Mins, PaidHoliday, LeaveDays, ObMins, IsRestDay, IsHoliday, Remarks, CreationTime, CreatorUserId)
			SELECT
				a.TenantId, a.EmployeeId, [Date], RegMins, Absence, TardyMins, UtMins, RegNd1Mins, RegNd2Mins, PaidHoliday, LeaveDays, ObMins, IsRestDay, IsHoliday, Remarks, CreationTime, CreatorUserId
			FROM tk.ProcessedTimesheets a INNER JOIN [tk].[EmployeeTimeInfo] e
			ON a.EmployeeId = e.EmployeeId and
				a.TenantId = e.TenantId
			WHERE a.TenantId = @TenantId 
				AND a.[Date] >= @StartDate 
				AND a.[Date] <= @EndDate
				AND e.FrequencyId = @frequencyId ;


			EXEC [tk].[usp_DeleteProcess]
					@AttendancePeriodId = @AttendancePeriodId,
					@TenantId = @TenantId ;


			UPDATE tk.AttendancePeriods
			SET Posted = 1
			WHERE Id = @AttendancePeriodId
				AND TenantId = @TenantId;


			COMMIT TRANSACTION;
		END

	END TRY

	BEGIN CATCH
		IF @@trancount > 0 ROLLBACK TRANSACTION
		EXEC dbo.usp_error_handler_sp
		RETURN 55555
	END CATCH
	
END


GO