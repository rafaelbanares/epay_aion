--RUN FOR ALL TENANTS

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_error_handler_sp]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_error_handler_sp] AS' 
END
GO

ALTER PROCEDURE [dbo].[usp_error_handler_sp] AS
BEGIN 
	DECLARE @errmsg   nvarchar(2048),
			@severity tinyint,
			@state    tinyint,
			@errno    int,
			@proc     sysname,
			@lineno   int
           
	SELECT @errmsg = error_message(), @severity = error_severity(),   -- 10
		   @state  = error_state(), @errno = error_number(),
		   @proc   = error_procedure(), @lineno = error_line()
       
	IF @errmsg NOT LIKE '***%'                                        -- 11  
	BEGIN
	   SELECT @errmsg = '*** ' + coalesce(quotename(@proc), '<dynamic SQL>') +
						', ' + ltrim(str(@lineno)) + '. Errno ' +
						ltrim(str(@errno)) + ': ' + @errmsg
	   RAISERROR(@errmsg, @severity, @state)
	END
	ELSE
	   RAISERROR(@errmsg, @severity, @state)
END

GO