--RUN FOR ALL TENANTS

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tk].[usp_DeleteEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [tk].[usp_DeleteEmail] AS' 
END
GO

ALTER PROCEDURE [tk].[usp_DeleteEmail]
AS
BEGIN
	SET NOCOUNT ON	

	BEGIN TRY
		
		BEGIN TRANSACTION

		--Delete records for the all tenants
		DELETE FROM AionDb.tk.EmailQueue 
		WHERE CreationTime < DATEADD(MONTH, -1, GETDATE())
			AND [Status] IN ('X', 'S');

		COMMIT TRANSACTION
		
	END TRY

	BEGIN CATCH
		IF @@trancount > 0 ROLLBACK TRANSACTION
		EXEC dbo.usp_error_handler_sp
		RETURN 55555
	END CATCH
	
END

