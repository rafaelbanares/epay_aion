--RUN FOR ALL TENANTS

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tk].[usp_ProcessRawTimeLogStaging]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [tk].[usp_ProcessRawTimeLogStaging] AS' 
END
GO

-- Transfer tk.RawTimeLogStaging to tk.RawTimeLogs without duplicates
ALTER PROCEDURE [tk].[usp_ProcessRawTimeLogStaging]
(
	@UploadSessionId UNIQUEIDENTIFIER
)
AS
BEGIN	 
	SET NOCOUNT ON	
	BEGIN TRANSACTION

		INSERT INTO tk.RawTimeLogs
		(
			TenantId, 
			SwipeCode, 
			LogTime, 
			InOut, 
			WorkFromHome, 
			Device, 
			CreationTime, 
			CreatorUserId
		)
		SELECT
			TenantId,
			SwipeCode,
			LogTime,
			InOut,
			MIN(CASE WorkFromHome WHEN 1 THEN 1 ELSE 0 END),
			MIN(Device),
			MIN(CreationTime),
			MIN(CreatorUserId)
		FROM 
			tk.RawTimeLogStaging x
		WHERE
			UploadSessionId = @UploadSessionId
			AND NOT EXISTS(
				SELECT TenantId, Swipecode, LogTime, InOut
				FROM tk.RawTimeLogs y
				WHERE x.TenantId = y.TenantId
					AND x.SwipeCode = y.SwipeCode
					AND x.LogTime = y.LogTime
					AND x.InOut = y.InOut
			)
		GROUP BY 
			TenantId, SwipeCode, LogTime, InOut;

		DELETE FROM tk.RawTimeLogStaging
		WHERE UploadSessionId = @UploadSessionId;

	COMMIT TRANSACTION
END




