﻿--RUN FOR ALL TENANTS

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tk].[usp_DeleteProcess]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [tk].[usp_DeleteProcess] AS' 
END
GO

ALTER PROCEDURE [tk].[usp_DeleteProcess]
(
	@AttendancePeriodId int,
	@TenantId int,
	@EmployeeId int = NULL
)
AS
BEGIN
	SET NOCOUNT ON	
	SET XACT_ABORT ON

	BEGIN TRY
		

		DECLARE @StartDate DATE
		DECLARE @EndDate   DATE
		DECLARE @frequencyId int
		
 
		SELECT 
			@StartDate = a.StartDate,
			@EndDate = a.EndDate,
			@frequencyId = FrequencyId
		FROM [tk].AttendancePeriods a
		WHERE Id = @AttendancePeriodId
			AND TenantId = @TenantId;

		
		BEGIN TRANSACTION    


		DELETE [tk].[Attendances]
		FROM [tk].[Attendances] a INNER JOIN [tk].[EmployeeTimeInfo] e
		ON a.EmployeeId = e.EmployeeId and
			a.TenantId = e.TenantId
		WHERE 
			a.[TenantId] = @TenantId AND 
			(a.[Date] >= @startDate AND a.[Date] <= @endDate) AND
			(@EmployeeId IS NULL OR a.EmployeeId = @EmployeeId) AND 
			e.FrequencyId = @frequencyId ;


		DELETE [tk].[ProcessedLeaves]
		FROM [tk].[ProcessedLeaves] a INNER JOIN [tk].[EmployeeTimeInfo] e
		ON a.EmployeeId = e.EmployeeId and
			a.TenantId = e.TenantId
		WHERE 
			a.[TenantId] = @TenantId AND 
			(a.[Date] >= @startDate AND a.[Date] <= @endDate) AND
			(@EmployeeId IS NULL OR a.EmployeeId = @EmployeeId) AND 
			e.FrequencyId = @frequencyId ;


		DELETE [tk].[ProcessedOvertimes] 
		FROM [tk].[ProcessedOvertimes] a INNER JOIN [tk].[EmployeeTimeInfo] e
		ON a.EmployeeId = e.EmployeeId and
			a.TenantId = e.TenantId
		WHERE 
			a.[TenantId] = @TenantId AND 
			(a.[Date] >= @startDate AND a.[Date] <= @endDate) AND
			(@EmployeeId IS NULL OR a.EmployeeId = @EmployeeId) AND 
			e.FrequencyId = @frequencyId ;


		DELETE [tk].[ProcessedTimesheets]
		FROM [tk].[ProcessedTimesheets] a INNER JOIN [tk].[EmployeeTimeInfo] e
		ON a.EmployeeId = e.EmployeeId and
			a.TenantId = e.TenantId
		WHERE 
			a.[TenantId] = @TenantId AND 
			(a.[Date] >= @startDate AND a.[Date] <= @endDate) AND
			(@EmployeeId IS NULL OR a.EmployeeId = @EmployeeId) AND 
			e.FrequencyId = @frequencyId ;

		COMMIT TRANSACTION
		

	END TRY

	BEGIN CATCH
		IF @@trancount > 0 ROLLBACK TRANSACTION
		EXEC dbo.usp_error_handler_sp
		RETURN 55555
	END CATCH
	
END

GO