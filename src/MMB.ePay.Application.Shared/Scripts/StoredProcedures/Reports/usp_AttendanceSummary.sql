--RUN FOR ALL TENANTS

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tk].[usp_AttendanceSummary]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [tk].[usp_AttendanceSummary] AS' 
END
GO

ALTER PROCEDURE [tk].[usp_AttendanceSummary]
(
    @EmployeeId INT = NULL,
    @Rank VARCHAR(MAX) = NULL, 
    @FrequencyFilterId INT = NULL,
    @MinDate DATETIME = NULL,
    @MaxDate DATETIME = NULL,
    @PostFilterId BIT = NULL
)
AS
BEGIN
    SET NOCOUNT ON;    
    SET XACT_ABORT ON;

    DECLARE @SQLQuery NVARCHAR(MAX);

    DECLARE @TableName NVARCHAR(50) = CASE WHEN @PostFilterId = 1 THEN 'tk.PostedTimesheets' ELSE 'tk.ProcessedTimesheets' END;

    SET @SQLQuery = N'
    WITH FilteredTimesheets AS (
        SELECT  
            e.EmployeeCode AS EmployeeId,
            e.LastName + '', '' + e.FirstName + '' '' + ISNULL(e.MiddleName, '''') AS EmployeeName,
            CAST(t.RegMins AS DECIMAL(18, 2)) / 60 AS RegHrs,
            CAST(t.RegNd1Mins AS DECIMAL(18, 2)) AS RegNd1Mins,
            CAST(t.RegNd2Mins AS DECIMAL(18, 2)) AS RegNd2Mins,
            CAST(t.UtMins AS DECIMAL(18, 2)) AS UtMins,
            CAST(t.TardyMins AS DECIMAL(18, 2)) AS TardyMins,
            CAST(t.PaidHoliday AS DECIMAL(18, 2)) AS PaidHoliday,
            CAST(t.Absence AS DECIMAL(18, 2)) AS Absence
        FROM ' + @TableName + N' t
        INNER JOIN hr.Employees e ON t.EmployeeId = e.Id
        INNER JOIN tk.EmployeeTimeInfo ti ON e.Id = ti.EmployeeId
        INNER JOIN dbo.AbpUsers au ON e.Id = au.EmployeeId
        WHERE (@EmployeeId = 0 OR t.EmployeeId = @EmployeeId)
			AND (@Rank IS NULL OR e.Rank = @Rank)
			AND (@FrequencyFilterId IS NULL OR ti.FrequencyId = @FrequencyFilterId)
			AND t.Date BETWEEN @MinDate AND @MaxDate
    )
    SELECT 
        EmployeeId,
        EmployeeName,
        ROUND(SUM(RegHrs) / 8, 2) AS RegDays,
        SUM(RegNd1Mins) AS RegNd1Mins,
        SUM(RegNd2Mins) AS RegNd2Mins,
        SUM(UtMins) AS UtMins,
        SUM(TardyMins) AS TardyMins,
        ROUND(SUM(PaidHoliday), 2) AS PaidHoliday,
        ROUND(SUM(Absence), 2) AS Absence
    FROM FilteredTimesheets
    GROUP BY EmployeeId, EmployeeName
    ORDER BY EmployeeId;
    ';

    EXEC sp_executesql @SQLQuery,
        N'@EmployeeId INT, @Rank VARCHAR(MAX), @FrequencyFilterId INT, @MinDate DATETIME, @MaxDate DATETIME',
        @EmployeeId, @Rank, @FrequencyFilterId, @MinDate, @MaxDate;
END
GO
