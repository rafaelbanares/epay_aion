--RUN FOR ALL TENANTS

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tk].[usp_OvertimeSummary]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [tk].[usp_OvertimeSummary] AS' 
END
GO

ALTER PROCEDURE [tk].[usp_OvertimeSummary]
(
    @EmployeeId INT = NULL,
    @Rank VARCHAR(MAX) = NULL, 
    @FrequencyFilterId INT = NULL,
    @MinDate DATETIME = NULL,
    @MaxDate DATETIME = NULL,
    @PostFilterId BIT = NULL
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DECLARE @SQLQuery NVARCHAR(MAX);

    DECLARE @TableName NVARCHAR(50) = CASE WHEN @PostFilterId = 1 THEN 'tk.PostedOvertimes' ELSE 'tk.ProcessedOvertimes' END;

	SET @SQLQuery = N'
	SELECT 
		EmployeeId, 
		EmployeeName, 
		CAST([Regular OT] AS DECIMAL(18, 2)) AS RegularOT, 
		CAST([Regular OT > 8] AS DECIMAL(18, 2)) AS RegularOT8, 
		CAST([Regular OT ND1] AS DECIMAL(18, 2)) AS RegularOTND1,
		CAST([Regular OT ND2] AS DECIMAL(18, 2)) AS RegularOTND2,
		CAST([Rest Day OT] AS DECIMAL(18, 2)) AS RestDayOT,
		CAST([Rest Day OT > 8] AS DECIMAL(18, 2)) AS RestDayOT8
	FROM (
		SELECT 
			e.EmployeeCode AS EmployeeId,
			e.LastName + '', '' + e.FirstName + '' '' + ISNULL(e.MiddleName, '''') AS EmployeeName,
			ot.ShortName,
			po.[Minutes]
		FROM ' + @TableName + N' po
		INNER JOIN hr.Employees e ON po.EmployeeId = e.Id
		INNER JOIN tk.EmployeeTimeInfo ti ON ti.EmployeeId = e.Id
		INNER JOIN tk.OvertimeTypes ot ON po.OvertimeTypeId = ot.Id
		WHERE (@EmployeeId = 0 OR po.EmployeeId = @EmployeeId)
			  AND (@FrequencyFilterId IS NULL OR ti.FrequencyId = @FrequencyFilterId)
			  AND (@Rank IS NULL OR e.Rank = @Rank)
			  AND (po.[Date] BETWEEN @MinDate AND @MaxDate)
	) AS SourceTable
	PIVOT (
		SUM([Minutes]) FOR ShortName IN 
		(
			[Regular OT], 
			[Regular OT > 8], 
			[Regular OT ND1],
			[Regular OT ND2],
			[Rest Day OT],
			[Rest Day OT > 8]
		)
	) AS PivotTable;
	';

	EXEC sp_executesql @SQLQuery,
        N'@EmployeeId INT, @Rank VARCHAR(MAX), @FrequencyFilterId INT, @MinDate DATETIME, @MaxDate DATETIME',
        @EmployeeId, @Rank, @FrequencyFilterId, @MinDate, @MaxDate;
END
GO
