--RUN FOR ALL TENANTS

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tk].[usp_AttendanceSummaryPayroll]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [tk].[usp_AttendanceSummaryPayroll] AS' 
END
GO

ALTER PROCEDURE [tk].[usp_AttendanceSummaryPayroll]
(
    @EmployeeId INT = NULL,
    @Rank VARCHAR(MAX) = NULL, 
    @FrequencyFilterId INT = NULL,
    @MinDate DATETIME = NULL,
    @MaxDate DATETIME = NULL,
    @PostFilterId BIT = NULL
)
AS
BEGIN
    SET NOCOUNT ON;    
    SET XACT_ABORT ON;

    DECLARE @SQL NVARCHAR(MAX);

    DECLARE @TableName NVARCHAR(50) = CASE WHEN @PostFilterId = 1 THEN 'tk.PostedTimesheets' ELSE 'tk.ProcessedTimesheets' END;


    SET @SQL = N'
    WITH FilteredTimesheets AS (
        SELECT 
			at.TenancyName AS TenantId,
            e.EmployeeCode AS EmployeeId,
            e.LastName,
            e.FirstName,
            CAST(ROUND((CAST(t.RegMins AS DECIMAL(18,2)) / 60) / 8, 2) AS DECIMAL(18,2)) AS RegDays,
			CAST(0 AS DECIMAL(18,2)) AS RegHrs,
			CAST(ROUND(t.Absence, 2) AS DECIMAL(18,2)) AS AbsDays,
			CAST(0 AS DECIMAL(18,2)) AS AbsHrs,
			CAST(ROUND(CAST(t.TardyMins AS DECIMAL(18,2)) / 60, 2) AS DECIMAL(18,2)) AS TardyHrs,
			CAST(ROUND(CAST(t.UtMins AS DECIMAL(18,2)) / 60, 2) AS DECIMAL(18,2)) AS UTHrs,
			CAST(ROUND(CAST(t.RegNd1Mins AS DECIMAL(18,2)) / 60, 2) AS DECIMAL(18,2)) AS ND1Hrs,
			CAST(ROUND(CAST(t.RegNd2Mins AS DECIMAL(18,2)) / 60, 2) AS DECIMAL(18,2)) AS ND2Hrs,
			CAST(0 AS DECIMAL(18,2)) AS SLDays,
			CAST(0 AS DECIMAL(18,2)) AS VLDays,
			CAST(ROUND(CAST(t.PaidHoliday AS DECIMAL(18,2)), 2) AS DECIMAL(18,2)) AS PaidHoliday
        FROM ' + @TableName + ' t
        INNER JOIN hr.Employees e ON t.EmployeeId = e.Id
		INNER JOIN AbpUsers a ON a.EmployeeId = e.Id
		INNER JOIN AionDb.dbo.AbpTenants at ON a.TenantId = at.Id
        INNER JOIN tk.EmployeeTimeInfo ti ON e.Id = ti.EmployeeId
        WHERE (@EmployeeId = 0 OR t.EmployeeId = @EmployeeId)
          AND (@FrequencyFilterId IS NULL OR ti.FrequencyId = @FrequencyFilterId)
          AND (@Rank IS NULL OR e.Rank = @Rank)
          AND t.Date BETWEEN @MinDate AND @MaxDate
    )
    SELECT 
        TenantId,
        EmployeeId,
        LastName,
        FirstName,
        '''' AS Paycode,
        '''' AS Department,
        ROUND(SUM(RegDays), 2) AS RegDays,
        ROUND(SUM(RegHrs), 2) AS RegHrs,
        ROUND(SUM(AbsDays), 2) AS AbsDays,
        ROUND(SUM(AbsHrs), 2) AS AbsHrs,
        ROUND(SUM(TardyHrs), 2) AS TardyHrs,
        ROUND(SUM(UTHrs), 2) AS UTHrs,
        ROUND(SUM(ND1Hrs), 2) AS ND1Hrs,
        ROUND(SUM(ND2Hrs), 2) AS ND2Hrs,
        ROUND(SUM(SLDays), 2) AS SLDays,
        ROUND(SUM(VLDays), 2) AS VLDays,
        ROUND(SUM(PaidHoliday), 2) AS PaidHoliday
    FROM FilteredTimesheets
    GROUP BY TenantId, EmployeeId, LastName, FirstName
    ORDER BY EmployeeId;
    ';

    -- Execute the dynamic SQL
    EXEC sp_executesql @SQL, 
        N'@EmployeeId INT, @Rank VARCHAR(MAX), @FrequencyFilterId INT, @MinDate DATETIME, @MaxDate DATETIME',
        @EmployeeId, @Rank, @FrequencyFilterId, @MinDate, @MaxDate;
END
GO
