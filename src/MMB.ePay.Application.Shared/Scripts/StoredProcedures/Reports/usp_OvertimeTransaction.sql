--RUN FOR ALL TENANTS

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tk].[Usp_OvertimeTransaction]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [tk].[Usp_OvertimeTransaction] AS' 
END
GO

ALTER PROCEDURE [tk].[Usp_OvertimeTransaction]
(
    @EmployeeId INT = NULL,
    @Rank VARCHAR(MAX) = NULL, 
    @FrequencyFilterId INT = NULL,
    @MinDate DATETIME = NULL,
    @MaxDate DATETIME = NULL,
    @PostFilterId BIT = NULL
)
AS
BEGIN
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

	DECLARE @SQLQuery NVARCHAR(MAX);

	SELECT 
        e.EmployeeCode AS EmployeeId,
        e.LastName + ', ' + e.FirstName + ' ' + ISNULL(e.MiddleName, '') AS EmployeeName,
        o.CreationTime AS DateFiled,
        o.OvertimeDate AS AttendanceDate,
        o.OvertimeStart AS OTStart,
        o.OvertimeEnd AS OTEnd,
        r.DisplayName AS Reason,
        s.DisplayName AS Status
    FROM tk.OvertimeRequests o
    INNER JOIN hr.Employees e ON o.EmployeeId = e.Id
    INNER JOIN tk.OvertimeReasons r ON o.OvertimeReasonId = r.Id
    INNER JOIN tk.[Status] s ON o.StatusId = s.Id
    INNER JOIN tk.EmployeeTimeInfo ti ON e.Id = ti.EmployeeId
    WHERE (@EmployeeId = 0 OR o.EmployeeId = @EmployeeId)
      AND (@FrequencyFilterId IS NULL OR ti.FrequencyId = @FrequencyFilterId)
      AND (@Rank IS NULL OR e.Rank = @Rank)
      AND (o.OvertimeStart BETWEEN @MinDate AND @MaxDate)
      AND (o.OvertimeEnd BETWEEN @MinDate AND @MaxDate)
    ORDER BY e.EmployeeCode;

END
GO
