--RUN FOR ALL TENANTS

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[tk].[usp_GetProcessBatch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [tk].[usp_GetProcessBatch] AS' 
END
GO


-- This will assign 10 employees in a batch and return the batchId
-- Returns null if no employees to process
ALTER PROCEDURE [tk].[usp_GetProcessBatch]
(
	@AppSessionId	uniqueidentifier,
	@BatchId uniqueidentifier OUTPUT
)
AS
BEGIN	 
	SET NOCOUNT ON	
	SET XACT_ABORT ON

	DECLARE @updated INT
	

	SET @BatchId = NEWID()

	UPDATE TOP (10) a set BatchId = @BatchId
	FROM [tk].ProcSessionBatches a
	WHERE 
		a.PsId = (select Id from [tk].ProcSessions where AppSessionId = @AppSessionId) and 
		a.BatchId IS NULL
	
	SELECT @updated = @@ROWCOUNT

	--return the BatchId
	IF @updated = 0
		SET @BatchId = NULL


	RETURN @updated
END

GO