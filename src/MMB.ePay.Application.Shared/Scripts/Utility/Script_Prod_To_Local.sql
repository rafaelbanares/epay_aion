--TO-DO--
--
--1) Backup existing DBs
--2) Download DB Prod
--3) Update script with encrypted connection strings for each tenant
	--* Login in Aion Host
	--* Go to ../App/Administration/Encryption page
	--* Enter connection strings and encryt for each tenant
	--* Paste the encrypted connection string BELOW
--4) Restore DB
--5) Run Script
--6) Login in to Host DB and check ../App/Administration/Tenants page if each tenant has the correct connection string
--7) Login as each tenant if connections are OK

----SET CONNECTION STRING
UPDATE AionDb.dbo.AbpTenants
SET ConnectionString = '' --PASTE ENCRYPTED CONNECTION STRING HERE
WHERE TenancyName = 'Jason';

UPDATE AionDb.dbo.AbpTenants
SET ConnectionString = '' --PASTE ENCRYPTED CONNECTION STRING HERE
WHERE TenancyName = 'MMB';

UPDATE AionDb.dbo.AbpTenants
SET ConnectionString = '' --PASTE ENCRYPTED CONNECTION STRING HERE
WHERE TenancyName = 'Caritas';

UPDATE AionDb.dbo.AbpTenants
SET ConnectionString = '' --PASTE ENCRYPTED CONNECTION STRING HERE
WHERE TenancyName = 'Taisho';

UPDATE AionDb.dbo.AbpTenants
SET ConnectionString = '' --PASTE ENCRYPTED CONNECTION STRING HERE
WHERE TenancyName = 'AgencyVA';

UPDATE AionDb.dbo.AbpTenants
SET ConnectionString = '' --PASTE ENCRYPTED CONNECTION STRING HERE
WHERE TenancyName = 'Benreys';