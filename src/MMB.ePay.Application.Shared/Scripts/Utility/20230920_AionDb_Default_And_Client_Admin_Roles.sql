DECLARE @TenantId INT = 2;
DECLARE @TenancyName VARCHAR(MAX) = 'abc';

-- Create Client Admin
IF NOT EXISTS (SELECT 1 FROM AbpRoles WHERE DisplayName = 'Client Admin' AND TenantId = @TenantId)
BEGIN
	INSERT INTO AbpRoles
	(
		TenantId, 
		IsDeleted, 
		[Name], 
		DisplayName, 
		IsStatic, 
		IsDefault, 
		NormalizedName, 
		ConcurrencyStamp, 
		CreationTime
	)
	VALUES
	(
		@TenantId, 
		0, 
		'Client Admin',
		'Client Admin',
		0, 
		0, 
		'CLIENT ADMIN',
		'f0ed9b75-70be-419f-9a2a-a8ff30992d18',
		GETDATE()
	);
END

-- Get the IDs for Admin and Client Admin roles
DECLARE @AdminRoleId INT = (SELECT Id FROM AbpRoles WHERE DisplayName = 'Admin' AND TenantId = @TenantId);
DECLARE @ClientAdminRoleId INT = (SELECT Id FROM AbpRoles WHERE DisplayName = 'Client Admin' AND TenantId = @TenantId);

-- Use a common table expression (CTE) to generate a list of admin user IDs
WITH AdminUserIds AS (
    SELECT DISTINCT x.UserId
    FROM AbpUserRoles x
    LEFT JOIN AbpUsers y 
		ON x.UserId = y.Id
    WHERE x.TenantId = @TenantId
        AND x.RoleId = @AdminRoleId
        AND y.EmployeeId IS NOT NULL
)

UPDATE ur
SET RoleId = @ClientAdminRoleId
FROM AbpUserRoles ur
JOIN AdminUserIds a 
	ON ur.UserId = a.UserId
WHERE ur.TenantId = @TenantId;

DELETE FROM AbpPermissions WHERE RoleId = @ClientAdminRoleId;
INSERT INTO AbpPermissions
	(TenantId, [Name], IsGranted, Discriminator, RoleId, CreationTime)
VALUES
	(@TenantId, 'Pages', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OfficialBusinessApprovals', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OfficialBusinessApprovals.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OfficialBusinessApprovals.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OfficialBusinessApprovals.Admin', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OfficialBusinessReasons', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OfficialBusinessReasons.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OfficialBusinessReasons.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OfficialBusinessReasons.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OfficialBusinessRequestDetails', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OfficialBusinessRequestDetails.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OfficialBusinessRequestDetails.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OfficialBusinessRequestDetails.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OfficialBusinessRequests', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OfficialBusinessRequests.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OfficialBusinessRequests.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OvertimeApprovals', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OvertimeApprovals.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OvertimeApprovals.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OvertimeApprovals.Admin', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OvertimeReasons', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OvertimeReasons.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OvertimeReasons.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OvertimeReasons.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Locations.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OvertimeRequests', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Locations.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Locations', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveApprovals', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveApprovals.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveApprovals.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveApprovals.Admin', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveEarnings', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveEarnings.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveEarnings.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveEarnings.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveReasons', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveReasons.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveReasons.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveReasons.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveRequestDetails', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveRequestDetails.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveRequestDetails.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveRequestDetails.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveRequests', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveRequests.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveRequests.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveTypes', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveTypes.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveTypes.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.LeaveTypes.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Locations.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Holidays.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OvertimeRequests.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.PostedAttendances', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftApprovals.Admin', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftReasons', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftReasons.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftReasons.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftReasons.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftRequests', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftRequests.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftRequests.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftTypeDetails', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftTypeDetails.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftTypeDetails.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftTypeDetails.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftTypes', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftTypes.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftTypes.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftTypes.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Status', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Status.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Status.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Status.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.TKSettings', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.TKSettings.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.TKSettings.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftApprovals.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.OvertimeRequests.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftApprovals.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.RestDayRequests.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.PostedAttendances.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.PostedAttendances.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.PostedAttendances.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Process', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Process.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Process.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Process.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Processes', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Reports', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.RestDayApprovals', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.RestDayApprovals.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.RestDayApprovals.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.RestDayApprovals.Admin', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.RestDayReasons', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.RestDayReasons.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.RestDayReasons.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.RestDayReasons.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.RestDayRequests', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.RestDayRequests.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ShiftApprovals', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Holidays.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Holidays.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Holidays', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.Tenant.SubscriptionManagement', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.Users', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.Users.ChangePermissions', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.Users.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.Users.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.Users.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.Users.Impersonation', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.Users.Unlock', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.WebhookSubscription', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.WebhookSubscription.ChangeActivity', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.WebhookSubscription.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.WebhookSubscription.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.Webhook.ListSendAttempts', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.Webhook.ResendWebhook', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.WebhookSubscription.Detail', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.AttendanceApprovals', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.AttendanceApprovals.Admin', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.OrganizationUnits.ManageRoles', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.DynamicProperties', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.DynamicProperties.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.DynamicProperties.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.DynamicEntityProperties', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.DynamicEntityProperties.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.DynamicEntityProperties.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.DynamicEntityProperties.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.DynamicPropertyValue', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.DynamicPropertyValue.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.DynamicPropertyValue.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.DynamicPropertyValue.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.DynamicProperties.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.DynamicEntityPropertyValue', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.DynamicEntityPropertyValue.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.DynamicEntityPropertyValue.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.DynamicEntityPropertyValue.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.Languages', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.Languages.ChangeTexts', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.OrganizationUnits', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.OrganizationUnits.ManageMembers', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Administration.OrganizationUnits.ManageOrganizationTree', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.AttendanceApprovals.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.AttendanceApprovals.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.AttendancePeriods', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.EmployeeTimeInfo', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.EmployeeTimeInfo.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.EmployeeTimeInfo.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.EmployeeTimeInfo.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Employees', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Employees.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Employees.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Employees.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.EmploymentTypes', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.EmploymentTypes.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.EmploymentTypes.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.EmploymentTypes.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ExcuseRequests', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ExcuseRequests.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.ExcuseRequests.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Frequencies', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Frequencies.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Frequencies.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Frequencies.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.GroupSettings', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.GroupSettings.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.GroupSettings.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.GroupSettings.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.EmployeeApprovers.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.EmployeeApprovers.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.EmployeeApprovers.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.EmployeeApprovers', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.AttendancePeriods.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.AttendancePeriods.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.AttendancePeriods.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.AttendanceReasons', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.AttendanceReasons.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.AttendanceReasons.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.AttendanceReasons.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.AttendanceRequests', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.AttendanceRequests.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.AttendanceRequests.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Attendances', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.TKSettings.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Attendances.Create', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Attendances.Edit', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Attendances.Post', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Attendances.Process', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Attendances.Summarize', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Attendances.Upload', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Tenant.Dashboard', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.DemoUiComponents', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.Attendances.Delete', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE()),
	(@TenantId, 'Pages.AdminAttendances', 1, 'RolePermissionSetting', @ClientAdminRoleId, GETDATE());

-- Create Default Admin
IF NOT EXISTS 
(
	SELECT 1 
	FROM AbpUsers x
	LEFT JOIN AbpUserRoles y
		ON x.Id = y.UserId
	WHERE x.TenantId = @TenantId
		AND x.EmployeeId IS NULL
		AND x.UserName = 'admin'
		AND y.RoleId = @AdminRoleId
)
BEGIN
	INSERT INTO AbpUsers
	(
		ShouldChangePasswordOnNextLogin, 
		CreationTime, 
		IsDeleted, 
		UserName, 
		TenantId, 
		EmailAddress, 
		Name, 
		Surname, 
		Password, 
		AccessFailedCount, 
		IsLockoutEnabled, 
		IsPhoneNumberConfirmed, 
		SecurityStamp, 
		IsTwoFactorEnabled, 
		IsEmailConfirmed, 
		IsActive, 
		NormalizedUserName,
		NormalizedEmailAddress,
		ConcurrencyStamp
	)
	VALUES
	(
		0,
		GETDATE(),
		0,	
		'admin',
		@TenantId,
		'admin@' + @TenancyName + '.com',
		'admin',
		'admin',
		'AQAAAAEAACcQAAAAEMdugfKXDp4cbpmipiucxQuJZT9HnzBPJs8H9kozdCU0hwTOQBQMsN20ipI287iwoA==',
		0,
		1,
		0,
		'AQAAAAEAACcQAAAAEMdugfKXDp4cbpmipiucxQuJZT9HnzBPJs8H9kozdCU0hwTOQBQMsN20ipI287iwoA==',
		1,
		0,
		1,
		'ADMIN',
		'ADMIN@' + UPPER(@TenancyName) + '.COM',
		'aeeb53fb-84c3-4b35-aa04-baf1db30bbb1'
	);
END
