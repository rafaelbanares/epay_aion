--SET PASSWORD --(DO NOT RUN THIS IN PROD)
DECLARE @Password VARCHAR(MAX) = 'AQAAAAEAACcQAAAAELAy12JI/PHxJen98075QbVtkUqL3StEcMP+dWpT/KM2l8+OqKOiFKxpzh2CKPzQhg==';
DECLARE @ConcurrencyStamp VARCHAR(MAX) = 'cb6eb01f-ee5c-468f-9756-68244c362a3a';
DECLARE @SecurityStamp VARCHAR(MAX) = 'PPMBOJNMQHNMJREVFACNLB3F5JKUWBPJ';

UPDATE AionDb.dbo.AbpUsers
SET [Password] = @Password,
ConcurrencyStamp = @ConcurrencyStamp,
SecurityStamp = @SecurityStamp;

UPDATE AionDb_Jason.dbo.AbpUsers
SET [Password] = @Password,
ConcurrencyStamp = @ConcurrencyStamp,
SecurityStamp = @SecurityStamp;

UPDATE AionDb_MMB.dbo.AbpUsers
SET [Password] = @Password,
ConcurrencyStamp = @ConcurrencyStamp,
SecurityStamp = @SecurityStamp;

UPDATE AionDb_Caritas.dbo.AbpUsers
SET [Password] = @Password,
ConcurrencyStamp = @ConcurrencyStamp,
SecurityStamp = @SecurityStamp;

UPDATE AionDb_Taisho.dbo.AbpUsers
SET [Password] = @Password,
ConcurrencyStamp = @ConcurrencyStamp,
SecurityStamp = @SecurityStamp;

UPDATE AionDb_AgencyVA.dbo.AbpUsers
SET [Password] = @Password,
ConcurrencyStamp = @ConcurrencyStamp,
SecurityStamp = @SecurityStamp;

UPDATE AionDb_BMC.dbo.AbpUsers
SET [Password] = @Password,
ConcurrencyStamp = @ConcurrencyStamp,
SecurityStamp = @SecurityStamp;

--DISABLE EMAIL NOTIF --(DO NOT RUN THIS IN PROD)
UPDATE AionDb.tk.TKSettings
SET [Value] = 'false'
WHERE [Key] = 'ENABLE_EMAIL_NOTIFICATION';

UPDATE AionDb_Jason.tk.TKSettings
SET [Value] = 'false'
WHERE [Key] = 'ENABLE_EMAIL_NOTIFICATION';

UPDATE AionDb_MMB.tk.TKSettings
SET [Value] = 'false'
WHERE [Key] = 'ENABLE_EMAIL_NOTIFICATION';

UPDATE AionDb_Caritas.tk.TKSettings
SET [Value] = 'false'
WHERE [Key] = 'ENABLE_EMAIL_NOTIFICATION';

UPDATE AionDb_Taisho.tk.TKSettings
SET [Value] = 'false'
WHERE [Key] = 'ENABLE_EMAIL_NOTIFICATION';

UPDATE AionDb_AgencyVA.tk.TKSettings
SET [Value] = 'false'
WHERE [Key] = 'ENABLE_EMAIL_NOTIFICATION';

UPDATE AionDb_BMC.tk.TKSettings
SET [Value] = 'false'
WHERE [Key] = 'ENABLE_EMAIL_NOTIFICATION';

