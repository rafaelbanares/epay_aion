USE AionDb_RTCC; --Change to TenantDb
GO

SET IDENTITY_INSERT AbpTenants ON;

DECLARE @TenancyName VARCHAR(MAX) = 'RTCC'; --Change to Tenant's TenancyName

IF NOT EXISTS (SELECT 1 FROM AionDb.dbo.AbpTenants WHERE TenancyName = @TenancyName)
BEGIN
    DECLARE @TenantId INT = (SELECT Id FROM AionDb.dbo.AbpTenants WHERE TenancyName = @TenancyName);

    IF @TenantId IS NOT NULL
    BEGIN
        INSERT INTO AbpTenants 
			(Id, IsInTrialPeriod, SubscriptionPaymentType, IsDeleted, TenancyName, [Name], IsActive, EditionId)
        SELECT 
			Id, IsInTrialPeriod, SubscriptionPaymentType, IsDeleted, TenancyName, [Name], IsActive, EditionId
        FROM AionDb.dbo.AbpTenants
        WHERE Id = @TenantId;
    END
END

SET IDENTITY_INSERT AbpTenants OFF;