USE AionDb
GO

DECLARE @TenantId INT = 3;
DECLARE @UserId INT;
DECLARE @RoleId INT;

IF NOT EXISTS (SELECT 1 FROM dbo.AbpUsers WHERE TenantId = @TenantId AND UserName = 'admin-upload')
BEGIN
	INSERT INTO dbo.AbpUsers
	(
		ShouldChangePasswordOnNextLogin, 
		CreationTime, 
		IsDeleted, 
		UserName, 
		TenantId, 
		EmailAddress, 
		[Name], 
		Surname, 
		[Password], 
		AccessFailedCount, 
		IsLockoutEnabled, 
		IsPhoneNumberConfirmed, 
		SecurityStamp, 
		IsTwoFactorEnabled, 
		IsEmailConfirmed, 
		IsActive, 
		NormalizedUserName, 
		NormalizedEmailAddress, 
		ConcurrencyStamp
	)
	VALUES
	(
		0,
		GETDATE(),
		0, 
		'admin-upload', 
		@TenantId, 
		'admin-upload@tobys.com', 
		'admin-upload', 
		'admin-upload', 
		'AQAAAAEAACcQAAAAEMdugfKXDp4cbpmipiucxQuJZT9HnzBPJs8H9kozdCU0hwTOQBQMsN20ipI287iwoA==',
		0,
		1,
		0,
		'AQAAAAEAACcQAAAAEMdugfKXDp4cbpmipiucxQuJZT9HnzBPJs8H9kozdCU0hwTOQBQMsN20ipI287iwoA==',
		0,
		0,
		1,
		'ADMIN-UPLOAD',
		'ADMIN-UPLOAD@TOBYS.COM',
		'aeeb53fb-84c3-4b35-aa04-baf1db30bbb1'
	);

	SET @UserId = SCOPE_IDENTITY();
END
ELSE
BEGIN
	SET @UserId = (SELECT TOP 1 Id FROM dbo.AbpUsers WHERE TenantId = @TenantId AND UserName = 'admin-upload' );
END;

IF NOT EXISTS (SELECT 1 FROM dbo.AbpRoles WHERE TenantId = @TenantId AND DisplayName = 'Auto-Uploader')
BEGIN
	INSERT INTO dbo.AbpRoles
	(
		CreationTime, 
		IsDeleted, 
		TenantId, 
		[Name], 
		DisplayName, 
		IsStatic, 
		IsDefault, 
		NormalizedName, 
		ConcurrencyStamp
	)
	VALUES
	(
		GETDATE(),
		0,
		@TenantId,
		'20f0296255774484a4f6b06b60878a27',
		'Auto-Uploader',
		0,
		0,
		'20F0296255774484A4F6B06B60878A27',
		'638e7454-bc31-43de-996d-a9a040e11b78'
	);

	SET @RoleId = SCOPE_IDENTITY();

	INSERT INTO dbo.AbpPermissions
		(TenantId, [Name], IsGranted, Discriminator, RoleId, CreationTime)
	VALUES
		(@TenantId, 'Pages.Attendances.Upload', 1, 'RolePermissionSetting', @RoleId, GETDATE()),
		(@TenantId, 'Pages.Attendances',		1, 'RolePermissionSetting', @RoleId, GETDATE()),
		(@TenantId, 'Pages',					1, 'RolePermissionSetting', @RoleId, GETDATE());
END
ELSE
BEGIN
	SET @RoleId = (SELECT TOP 1 Id FROM dbo.AbpRoles WHERE TenantId = @TenantId AND DisplayName = 'Auto-Uploader');
END;

IF NOT EXISTS (SELECT 1 FROM dbo.AbpUserRoles WHERE TenantId = @TenantId AND UserId = @UserId AND RoleId = @RoleId)
BEGIN
	INSERT INTO dbo.AbpUserRoles
		(TenantId, UserId, RoleId, CreationTime)
	VALUES
		(@TenantId, @UserId, @RoleId, GETDATE());
END;