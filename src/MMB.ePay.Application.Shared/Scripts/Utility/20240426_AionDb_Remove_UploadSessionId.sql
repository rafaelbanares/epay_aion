--Apply to all tenants

IF EXISTS(SELECT 1 FROM sys.columns 
          WHERE Name = N'UploadSessionId'
          AND Object_ID = Object_ID(N'tk.RawTimeLogs'))
BEGIN
    ALTER TABLE tk.RawTimeLogs
	DROP COLUMN UploadSessionId

	SELECT * FROM tk.RawTimeLogs
END