--RUN FOR ALL TENANTS

IF OBJECT_ID('[dbo].[zzTenantsConString]', 'U') IS NOT NULL
    DROP TABLE [dbo].[zzTenantsConString];
GO

IF COL_LENGTH('[hr].[Employees]', 'DateRegularized') IS NULL
    ALTER TABLE [hr].[Employees] ADD [DateRegularized] [date] NULL;
GO

IF EXISTS (SELECT 1 FROM sys.columns WHERE object_id = OBJECT_ID('[tk].[RawTimeLogs]') AND name = 'LogTime' AND is_nullable = 1)
BEGIN
    IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = 'IX_RawTimeLogs_SwipeCode_LogTime_InOut_TenantId' AND object_id = OBJECT_ID('[tk].[RawTimeLogs]'))
    BEGIN
        DROP INDEX IX_RawTimeLogs_SwipeCode_LogTime_InOut_TenantId ON [tk].[RawTimeLogs];
    END

    EXEC('ALTER TABLE [tk].[RawTimeLogs] ALTER COLUMN [LogTime] DATETIME2(7) NOT NULL');

    CREATE INDEX IX_RawTimeLogs_SwipeCode_LogTime_InOut_TenantId 
    ON [tk].[RawTimeLogs] ([SwipeCode], [LogTime], [InOut], [TenantId]);
END
GO

IF EXISTS (SELECT 1 FROM sys.columns WHERE object_id = OBJECT_ID('[tk].[RawTimeLogStaging]') AND name = 'LogTime' AND is_nullable = 1)
    ALTER TABLE [tk].[RawTimeLogStaging] ALTER COLUMN [LogTime] [datetime2](7) NOT NULL;
GO

IF EXISTS (SELECT 1 FROM sys.objects WHERE type = 'PK' AND name = 'PK_RawTimeLogStaging')
BEGIN
    ALTER TABLE [tk].[RawTimeLogStaging] DROP CONSTRAINT [PK_RawTimeLogStaging];
END
GO
ALTER TABLE [tk].[RawTimeLogStaging] ADD CONSTRAINT [PK_RawTimeLogStaging] PRIMARY KEY CLUSTERED ([Id] ASC);
GO

DELETE FROM [tk].[Reports];

IF COL_LENGTH('[tk].[Reports]', 'StoredProcedure') IS NULL
    ALTER TABLE [tk].[Reports] ADD [StoredProcedure] [varchar](50) NULL;
GO
IF COL_LENGTH('[tk].[Reports]', 'ExportFileName') IS NULL
    ALTER TABLE [tk].[Reports] ADD [ExportFileName] [varchar](50) NULL;
GO

IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE name = 'FK_User_Employees')
    ALTER TABLE [dbo].[AbpUsers] DROP CONSTRAINT [FK_User_Employees];
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE name = 'FK_AbpUsers_Employees_EmployeeId')
	ALTER TABLE [dbo].[AbpUsers] ADD CONSTRAINT [FK_AbpUsers_Employees_EmployeeId] FOREIGN KEY ([EmployeeId]) REFERENCES [hr].[Employees] ([Id]);
GO

IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE name = 'FK_Employees_EmploymentTypes')
    ALTER TABLE [hr].[Employees] DROP CONSTRAINT [FK_Employees_EmploymentTypes];
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE name = 'FK_Employees_EmploymentTypes_EmploymentTypeId')
	ALTER TABLE [hr].[Employees] ADD CONSTRAINT [FK_Employees_EmploymentTypes_EmploymentTypeId] FOREIGN KEY ([EmploymentTypeId]) REFERENCES [hr].[EmploymentTypes] ([Id]);
GO

IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE name = 'FK_LeaveRequestDetails_Employees_EmployeeId')
    ALTER TABLE [tk].[LeaveRequestDetails] ADD CONSTRAINT [FK_LeaveRequestDetails_Employees_EmployeeId] FOREIGN KEY ([EmployeeId]) REFERENCES [hr].[Employees] ([Id]);
GO

IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE name = 'FK_ProcSessions_AttendancePeriods_AttendancePeriodId')
BEGIN
	DELETE FROM [tk].[ProcSessions] WHERE AttendancePeriodId NOT IN (SELECT Id FROM [tk].AttendancePeriods)
    ALTER TABLE [tk].[ProcSessions] ADD CONSTRAINT [FK_ProcSessions_AttendancePeriods_AttendancePeriodId] FOREIGN KEY ([AttendancePeriodId]) REFERENCES [tk].[AttendancePeriods] ([Id]);
END
GO

IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE name = 'FK_GroupSettings_TKSettings')
    ALTER TABLE [tk].[TKSettings] DROP CONSTRAINT [FK_GroupSettings_TKSettings];
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE name = 'FK_TKSettings_GroupSettings_GroupSettingId')
	ALTER TABLE [tk].[TKSettings] ADD CONSTRAINT [FK_TKSettings_GroupSettings_GroupSettingId] FOREIGN KEY ([GroupSettingId]) REFERENCES [hr].[GroupSettings] ([Id]);
GO

DECLARE @sql NVARCHAR(MAX);
SET @sql = '';

SELECT @sql = @sql + 'ALTER TABLE ' + OBJECT_SCHEMA_NAME(parent_object_id) + '.' + OBJECT_NAME(parent_object_id) + ' DROP CONSTRAINT ' + name + ';\n'
FROM sys.default_constraints
WHERE name IN (
    'DF_Attendances_Invalid',
    'DF_EmailQueue_ApprovalStatus',
    'DF_EmailQueue_FullName',
    'DF_EmailQueue_Remarks',
    'DF_EmailQueue_Request',
    'DF_EmailQueue_StartDate',
    'DF_EmployeeTimeInfo_FrequencyId',
    'DF_PostedTimesheets_IsRestDay',
    'DF_PostedTimesheets_IsHoliday',
    'DF_ProcessedTimesheets_IsRestDay',
    'DF_ProcessedTimesheets_IsHoliday',
    'DF_ProcSessions_TenantId');

EXEC sp_executesql @sql;
GO

DECLARE @sql NVARCHAR(MAX);

SELECT @sql = STRING_AGG('ALTER TABLE ' + QUOTENAME(s.name) + '.' + QUOTENAME(t.name) + 
                         ' DROP CONSTRAINT ' + QUOTENAME(dc.name) + ';', ' ')
FROM sys.default_constraints dc
JOIN sys.tables t ON dc.parent_object_id = t.object_id
JOIN sys.schemas s ON t.schema_id = s.schema_id
WHERE (s.name = 'tk' AND t.name = 'Attendances' AND dc.parent_column_id = COLUMNPROPERTY(t.object_id, 'Invalid', 'ColumnId'))
   OR (s.name = 'tk' AND t.name = 'EmailQueue' AND dc.parent_column_id = COLUMNPROPERTY(t.object_id, 'ApprovalStatus', 'ColumnId'))
   OR (s.name = 'tk' AND t.name = 'EmailQueue' AND dc.parent_column_id = COLUMNPROPERTY(t.object_id, 'FullName', 'ColumnId'))
   OR (s.name = 'tk' AND t.name = 'EmailQueue' AND dc.parent_column_id = COLUMNPROPERTY(t.object_id, 'Remarks', 'ColumnId'))
   OR (s.name = 'tk' AND t.name = 'EmailQueue' AND dc.parent_column_id = COLUMNPROPERTY(t.object_id, 'Request', 'ColumnId'))
   OR (s.name = 'tk' AND t.name = 'EmailQueue' AND dc.parent_column_id = COLUMNPROPERTY(t.object_id, 'StartDate', 'ColumnId'))
   OR (s.name = 'tk' AND t.name = 'EmployeeTimeInfo' AND dc.parent_column_id = COLUMNPROPERTY(t.object_id, 'FrequencyId', 'ColumnId'))
   OR (s.name = 'tk' AND t.name = 'PostedTimesheets' AND dc.parent_column_id = COLUMNPROPERTY(t.object_id, 'IsRestDay', 'ColumnId'))
   OR (s.name = 'tk' AND t.name = 'PostedTimesheets' AND dc.parent_column_id = COLUMNPROPERTY(t.object_id, 'IsHoliday', 'ColumnId'))
   OR (s.name = 'tk' AND t.name = 'ProcessedTimesheets' AND dc.parent_column_id = COLUMNPROPERTY(t.object_id, 'IsRestDay', 'ColumnId'))
   OR (s.name = 'tk' AND t.name = 'ProcessedTimesheets' AND dc.parent_column_id = COLUMNPROPERTY(t.object_id, 'IsHoliday', 'ColumnId'))
   OR (s.name = 'tk' AND t.name = 'ProcSessions' AND dc.parent_column_id = COLUMNPROPERTY(t.object_id, 'TenantId', 'ColumnId'));

IF @sql IS NOT NULL
    EXEC sp_executesql @sql;
GO

IF NOT EXISTS (SELECT 1 FROM dbo.AbpTenants WHERE [Name] = 'Readycon')
	DBCC CHECKIDENT ('dbo.AbpTenants', RESEED, 9);