DECLARE @TenancyName VARCHAR(MAX) = 'RTCC';

DECLARE @TenantId INT = (SELECT Id FROM AionDb.dbo.AbpTenants WHERE TenancyName = @TenancyName);
DECLARE @CreationTime DATETIME = GETDATE();

DECLARE @MaxCount INT = (SELECT COUNT(1) FROM tk.Reports);
DBCC CHECKIDENT ('tk.Reports', RESEED, @MaxCount);

INSERT INTO tk.Reports
([TenantId], [DisplayName], [ControllerName], [StoredProcedure], [HasEmployeeFilter], [HasPeriodFilter], [IsLandscape], [Enabled], [Format], [Output], [ExportFileName], [CreationTime])
VALUES
(@TenantId, 'Attendance Transaction', 'AttendanceTransaction', null, 1, 1, 0, 1, 'Letter', 'P', null, @CreationTime),
(@TenantId, 'Excuse Transaction', 'ExcuseTransaction', null, 1, 1, 0, 1, 'Letter', 'P', null, @CreationTime),
(@TenantId, 'Leave Transaction', 'LeaveTransaction', null, 1, 1, 0, 1, 'Letter', 'P', null, @CreationTime),
(@TenantId, 'Official Business Transaction', 'OfficialBusinessTransaction', null, 1, 1, 0, 1, 'Letter', 'P', null, @CreationTime),
(@TenantId, 'Overtime Transaction', 'OvertimeTransaction', 'usp_OvertimeTransaction', 1, 1, 0, 1, 'Letter', 'P', null, @CreationTime),
(@TenantId, 'Rest Day Transaction', 'RestDayTransaction', null, 1, 1, 0, 1, 'Letter', 'P', null, @CreationTime),
(@TenantId, 'Shift Transaction', 'ShiftTransaction', null, 1, 1, 0, 1, 'Letter', 'P', null, @CreationTime),
(@TenantId, 'Attendance Summary', 'AttendanceSummary', 'usp_AttendanceSummary', 1, 1, 0, 1, 'Letter', 'P', null, @CreationTime),
(@TenantId, 'Attendance Summary (Vertical Format)', 'AttendanceSummary2', 'usp_AttendanceSummary2', 1, 1, 0, 1, 'Letter', 'P', null, @CreationTime),
(@TenantId, 'Leave Summary', 'LeaveSummary', null, 1, 1, 0, 1, 'Letter', 'P', null, @CreationTime),
(@TenantId, 'Overtime Summary', 'OvertimeSummary', 'usp_OvertimeSummary', 1, 1, 0, 1, 'Letter', 'P', null, @CreationTime),
(@TenantId, 'Daily Attendance', 'DailyAttendance', null, 1, 1, 0, 1, 'Letter', 'P', null, @CreationTime),
(@TenantId, 'Leave Balance', 'LeaveBalance', null, 1, 1, 0, 1, 'Letter', 'P', null, @CreationTime),
(@TenantId, 'Work From Home', 'WorkFromHome', null, 1, 1, 0, 1, 'Letter', 'P', null, @CreationTime),
(@TenantId, 'Attendance Summary (Payroll)', 'AttendanceSummaryPayroll', 'usp_AttendanceSummaryPayroll', 1, 1, 1, 1, 'Letter', 'E', null, @CreationTime),
(@TenantId, 'Leave Summary (Payroll)', 'LeaveSummaryPayroll', null, 1, 1, 1, 1, 'Letter', 'E', null, @CreationTime),
(@TenantId, 'Overtime Summary (Payroll)', 'OvertimeSummaryPayroll', null, 1, 1, 1, 1, 'Letter', 'E', null, @CreationTime)

select 
	Id, TenancyName, ConnectionString
from AionDb.dbo.AbpTenants;

select * from tk.Reports
where TenantId = @TenantId;