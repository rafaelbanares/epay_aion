ALTER TABLE AionDb.tk.Reports
ADD [StoredProcedure] [varchar](50) NULL;

ALTER TABLE AionDb_AgencyVA.tk.Reports
ADD [StoredProcedure] [varchar](50) NULL;

ALTER TABLE AionDb_BMC.tk.Reports
ADD [StoredProcedure] [varchar](50) NULL;

ALTER TABLE AionDb_Caritas.tk.Reports
ADD [StoredProcedure] [varchar](50) NULL;

ALTER TABLE AionDb_Jason.tk.Reports
ADD [StoredProcedure] [varchar](50) NULL;

ALTER TABLE AionDb_MMB.tk.Reports
ADD [StoredProcedure] [varchar](50) NULL;

ALTER TABLE AionDb_Taisho.tk.Reports
ADD [StoredProcedure] [varchar](50) NULL;