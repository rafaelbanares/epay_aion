IF EXISTS(SELECT 1 FROM sys.columns WHERE [Name] = 'DisplayFormat' AND object_id = OBJECT_ID('tk.OvertimeTypes'))
BEGIN
	ALTER TABLE tk.OvertimeTypes
	DROP COLUMN DisplayFormat;
END