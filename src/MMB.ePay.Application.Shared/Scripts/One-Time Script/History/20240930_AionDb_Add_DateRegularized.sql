ALTER TABLE AionDb.hr.Employees
ADD [DateRegularized] [date] NULL;

ALTER TABLE AionDb_AgencyVA.hr.Employees
ADD [DateRegularized] [date] NULL;

ALTER TABLE AionDb_BMC.hr.Employees
ADD [DateRegularized] [date] NULL;

ALTER TABLE AionDb_Caritas.hr.Employees
ADD [DateRegularized] [date] NULL;

ALTER TABLE AionDb_Jason.hr.Employees
ADD [DateRegularized] [date] NULL;

ALTER TABLE AionDb_MMB.hr.Employees
ADD [DateRegularized] [date] NULL;

ALTER TABLE AionDb_Taisho.hr.Employees
ADD [DateRegularized] [date] NULL;