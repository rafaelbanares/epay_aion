--AionDb
USE AionDb
GO
IF NOT EXISTS (
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'Reports'
    AND COLUMN_NAME = 'ExportFileName'
)
BEGIN
	ALTER TABLE [tk].[Reports]
    ADD [ExportFileName] [varchar](50) NULL;
END
GO
UPDATE tk.Reports 
SET ExportFileName = REPLACE(DisplayName, ' ', '_');

--AionDb_AgencyVA
USE AionDb_AgencyVA
GO
IF NOT EXISTS (
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'Reports'
    AND COLUMN_NAME = 'ExportFileName'
)
BEGIN
	ALTER TABLE tk.Reports
    ADD [ExportFileName] [varchar](50) NULL;
END
GO
UPDATE tk.Reports 
SET ExportFileName = REPLACE(DisplayName, ' ', '_');

--AionDb_BMC
USE AionDb_BMC
GO
IF NOT EXISTS (
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'Reports'
    AND COLUMN_NAME = 'ExportFileName'
)
BEGIN
	ALTER TABLE [tk].[Reports]
    ADD ExportFileName varchar(50) NULL;
END
GO
UPDATE tk.Reports 
SET ExportFileName = REPLACE(DisplayName, ' ', '_');

--AionDb_Caritas
USE AionDb_Caritas
GO
IF NOT EXISTS (
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'Reports'
    AND COLUMN_NAME = 'ExportFileName'
)
BEGIN
	ALTER TABLE [tk].[Reports]
    ADD ExportFileName varchar(50) NULL;
END
GO
UPDATE tk.Reports 
SET ExportFileName = REPLACE(DisplayName, ' ', '_');

--AionDb_Jason
USE AionDb_Jason
GO
IF NOT EXISTS (
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'Reports'
    AND COLUMN_NAME = 'ExportFileName'
)
BEGIN
	ALTER TABLE [tk].[Reports]
    ADD ExportFileName varchar(50) NULL;
END
GO
UPDATE tk.Reports 
SET ExportFileName = REPLACE(DisplayName, ' ', '_');

--AionDb_MMB
USE AionDb_MMB
GO
IF NOT EXISTS (
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'Reports'
    AND COLUMN_NAME = 'ExportFileName'
)
BEGIN
	ALTER TABLE [tk].[Reports]
    ADD ExportFileName varchar(50) NULL;
END
GO
UPDATE tk.Reports 
SET ExportFileName = REPLACE(DisplayName, ' ', '_');

--AionDb_Taisho
USE AionDb_Taisho
GO
IF NOT EXISTS (
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'Reports'
    AND COLUMN_NAME = 'ExportFileName'
)
BEGIN
	ALTER TABLE [tk].[Reports]
    ADD ExportFileName varchar(50) NULL;
END
GO
UPDATE tk.Reports 
SET ExportFileName = REPLACE(DisplayName, ' ', '_');