/****** Object:  Table [tk].[AttendanceUploads]    Script Date: 14/12/2023 4:12:39 pm ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [tk].[AttendanceUploads](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TenantId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[Date] [date] NOT NULL,
	[TimeIn] [varchar](10) NULL,
	[LunchBreaktimeIn] [varchar](10) NULL,
	[LunchBreaktimeOut] [varchar](10) NULL,
	[PMBreaktimeIn] [varchar](10) NULL,
	[PMBreaktimeOut] [varchar](10) NULL,
	[TimeOut] [varchar](10) NULL,
	[CreationTime] [datetime2](7) NOT NULL,
	[CreatorUserId] [bigint] NULL,
 CONSTRAINT [PK_AttendanceUploads] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [tk].[AttendanceUploads]  WITH CHECK ADD  CONSTRAINT [FK_AttendanceUploads_Employees_EmployeeId] FOREIGN KEY([EmployeeId])
REFERENCES [hr].[Employees] ([Id])
GO

ALTER TABLE [tk].[AttendanceUploads] CHECK CONSTRAINT [FK_AttendanceUploads_Employees_EmployeeId]
GO


