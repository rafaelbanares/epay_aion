USE AionDb
GO
UPDATE tk.Reports
SET ControllerName = 'AttendanceSummaryPayrollWithMinuteFormat'
WHERE TenantId = 3 AND DisplayName = 'Attendance Summary (Payroll)';

UPDATE tk.Reports
SET ControllerName = 'OvertimeSummaryPayrollWithMinuteFormat'
WHERE TenantId = 3 AND DisplayName = 'Overtime Summary (Payroll)';