DECLARE @TenantId INT = 0; --Set tenant id
DECLARE @GroupSettingId INT = 
	(
		SELECT TOP 1 Id 
		FROM hr.GroupSettings 
		WHERE TenantId = @TenantId 
			AND SysCode = 'MISC'
	);
DECLARE @DisplayOrder INT = 1 + 
	(
		SELECT MAX(DisplayOrder) 
		FROM tk.TKSettings
		WHERE TenantId = @TenantId
	);

IF NOT EXISTS(SELECT 1 FROM tk.TKSettings WHERE TenantId = @TenantId AND [Key] = 'MINUTE_FORMAT')
BEGIN
	INSERT INTO tk.TKSettings
		(TenantId, [Key], [Value], DataType, Caption, DisplayOrder, GroupSettingId)
	VALUES
		(@TenantId, 'MINUTE_FORMAT', 'true', 'bool', 'Enable Minute Format', @DisplayOrder, @GroupSettingId)
END;

