DECLARE @TenantId INT = 0 --set tenantid
DECLARE @UserId INT = (SELECT Id FROM AbpUsers WHERE TenantId = @TenantId AND UserName = 'admin')
DECLARE @RoleId INT = (SELECT Id FROM AbpRoles WHERE TenantId = @TenantId AND [Name] = 'admin')

IF NOT EXISTS(SELECT 1 FROM AbpUserRoles WHERE UserId = @UserId)
BEGIN
	
	INSERT INTO AbpUserRoles
	(TenantId, UserId, RoleId, CreationTime)
	VALUES
	(@TenantId, @UserId, @RoleId, GETDATE())

END
