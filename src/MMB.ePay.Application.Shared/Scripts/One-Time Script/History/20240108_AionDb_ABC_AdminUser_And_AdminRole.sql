USE AionDb
GO

DECLARE @TenantId	INT = (SELECT Id from AbpTenants WHERE TenancyName = 'ABC');
DECLARE @RoleId		INT = (SELECT Id FROM AbpRoles WHERE TenantId = @TenantId AND [Name] = 'Admin');

INSERT INTO AbpUsers
(
	ShouldChangePasswordOnNextLogin, 
	CreationTime, 
	IsDeleted, 
	UserName, 
	TenantId, 
	EmailAddress, 
	[Name], 
	Surname, 
	[Password], 
	AccessFailedCount, 
	IsLockoutEnabled, 
	IsPhoneNumberConfirmed, 
	SecurityStamp, 
	IsTwoFactorEnabled, 
	IsEmailConfirmed, 
	IsActive, 
	NormalizedUserName, 
	NormalizedEmailAddress, 
	ConcurrencyStamp
)
VALUES
(
	0,
	GETDATE(), 
	0, 
	'admin', 
	@TenantId, 
	'admin@abc.com',
	'admin',
	'admin',
	'AQAAAAEAACcQAAAAEBzgy4qgenXFpUwgB4cag82TTJZqNE20HdSxaVGY6p0BuJYZ2a/mVweqCZ/VrkOTnA==', 
	0,
	1,
	0, 
	'FY43YNXG3BSQ2YOGHPFXOV46FLPFRDYF', 
	1, 
	1, 
	1, 
	'ADMIN',
	'ADMIN@ABC.COM', 
	'0e277741-f0da-4852-aad5-48f193f04c3a'
)

DECLARE @UserId		INT = SCOPE_IDENTITY();

INSERT INTO AbpUserRoles
(
	TenantId, 
	UserId, 
	RoleId, 
	CreationTime
)
VALUES
(
	@TenantId, 
	@UserId, 
	@RoleId, 
	GETDATE()
);