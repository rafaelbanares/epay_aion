USE [AionDb]
GO
/****** Object:  StoredProcedure [tk].[usp_DeleteProcess]    Script Date: 24/04/2023 12:25:51 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('tk.usp_DeleteAuditLogs', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE tk.usp_DeleteAuditLogs
END
GO

CREATE PROCEDURE [tk].[usp_DeleteAuditLogs]
AS
BEGIN
	SET NOCOUNT ON	

	BEGIN TRY
		
		BEGIN TRANSACTION

		--Delete records for the host tenant
		DELETE FROM AionDb.dbo.AbpAuditLogs 
		WHERE TenantId IS NULL 
			AND ExecutionTime < DATEADD(MONTH, -3, GETDATE());

		--Delete records for tenants in host database
		DECLARE @tenantId INT;
		DECLARE @tenancyName VARCHAR(MAX);

		DECLARE tenantCursor CURSOR LOCAL FORWARD_ONLY FOR
			SELECT Id FROM AbpTenants
			WHERE ConnectionString IS NULL
			ORDER BY Id;

		OPEN tenantCursor;
		FETCH NEXT FROM tenantCursor INTO @tenantId;

		WHILE @@FETCH_STATUS = 0
		BEGIN
			EXECUTE ('DELETE FROM AionDb.dbo.AbpAuditLogs WHERE TenantId = ' 
				+ @tenantId
				+ ' AND ExecutionTime < DATEADD(MONTH, -3, GETDATE())');

			FETCH NEXT FROM tenantCursor INTO @tenantId;
		END;

		CLOSE tenantCursor;
		DEALLOCATE tenantCursor;

		--Delete records for the host tenant
		DELETE FROM AionDb_Jason.dbo.AbpAuditLogs 
		WHERE TenantId = (SELECT Id FROM AionDb.dbo.AbpTenants WHERE TenancyName = 'Jason')
			AND ExecutionTime < DATEADD(MONTH, -3, GETDATE());

		--Delete records for the MMB tenant
		DELETE FROM AionDb_MMB.dbo.AbpAuditLogs 
		WHERE TenantId = (SELECT Id FROM AionDb.dbo.AbpTenants WHERE TenancyName = 'MMB')
			AND ExecutionTime < DATEADD(MONTH, -3, GETDATE());

		--Delete records for the Caritas tenant
		DELETE FROM AionDb_Caritas.dbo.AbpAuditLogs 
		WHERE TenantId = (SELECT Id FROM AionDb.dbo.AbpTenants WHERE TenancyName = 'Caritas')
			AND ExecutionTime < DATEADD(MONTH, -3, GETDATE());

		--Delete records for the Taisho tenant
		DELETE FROM AionDb_Taisho.dbo.AbpAuditLogs 
		WHERE TenantId = (SELECT Id FROM AionDb.dbo.AbpTenants WHERE TenancyName = 'Taisho')
			AND ExecutionTime < DATEADD(MONTH, -3, GETDATE());

		--Delete records for the AgencyVA tenant
		DELETE FROM AionDb_AgencyVA.dbo.AbpAuditLogs 
		WHERE TenantId = (SELECT Id FROM AionDb.dbo.AbpTenants WHERE TenancyName = 'AgencyVA')
			AND ExecutionTime < DATEADD(MONTH, -3, GETDATE());

		--Delete records for the Benreys tenant
		DELETE FROM AionDb_BMC.dbo.AbpAuditLogs 
		WHERE TenantId = (SELECT Id FROM AionDb.dbo.AbpTenants WHERE TenancyName = 'Benreys')
			AND ExecutionTime < DATEADD(MONTH, -3, GETDATE());

		--Delete records for the ABC tenant
		DELETE FROM AionDb_ABC.dbo.AbpAuditLogs 
		WHERE TenantId = (SELECT Id FROM AionDb.dbo.AbpTenants WHERE TenancyName = 'ABC')
			AND ExecutionTime < DATEADD(MONTH, -3, GETDATE());

		COMMIT TRANSACTION
		
	END TRY

	BEGIN CATCH
		IF @@trancount > 0 ROLLBACK TRANSACTION
		EXEC dbo.usp_error_handler_sp
		RETURN 55555
	END CATCH
	
END

