---APPLY TO ALL TENANTS---
SELECT Id, TenancyName from AionDb.dbo.AbpTenants
ORDER BY Id

USE AionDb --Change Table per tenant
GO

DECLARE @TenantId INT = 3; --Change TenantId per tenant

IF NOT EXISTS (
	SELECT * FROM tk.Reports
	WHERE TenantId = @TenantId
		AND ControllerName = 'AttendanceWithBreaks'
)
BEGIN
INSERT INTO tk.Reports
	(TenantId, DisplayName, ControllerName, HasEmployeeFilter, HasPeriodFilter, IsLandscape, [Enabled], [Format], [Output], CreationTime)
	VALUES
	(@TenantId, 'Attendance with Breaks', 'AttendanceWithBreaks', 1, 1, 1, 1, 'Letter', 'P', '2020-01-01');
END

IF NOT EXISTS (
	SELECT * FROM tk.TKSettings
	WHERE TenantId = @TenantId
		AND [Key] = 'HAS_PM_BREAK'
)
BEGIN
	DECLARE @GroupSettingId INT = (
		SELECT Id FROM hr.GroupSettings 
		WHERE TenantId = @TenantId
			AND SysCode = 'MISC'
	);

	DECLARE @DisplayOrder INT = (
		SELECT MAX(DisplayOrder) + 1 FROM tk.TKSettings
		WHERE TenantId = @TenantId
	);

	INSERT INTO tk.TKSettings
	(TenantId, [Key], [Value], DataType, Caption, DisplayOrder, GroupSettingId)
	VALUES
	(@TenantId, 'HAS_PM_BREAK', 'false', 'bool', 'Enable PM Breaks', @DisplayOrder, @GroupSettingId);
END

IF EXISTS (
	SELECT * FROM hr.GroupSettings
	WHERE SysCode = 'ORG'
)
BEGIN
	DELETE FROM hr.GroupSettings
	WHERE SysCode = 'ORG';

	UPDATE hr.GroupSettings
	SET OrderNo = 1
	WHERE SysCode = 'MISC';
END

