USE [AionDb]
GO
/****** Object:  StoredProcedure [tk].[usp_DeleteProcess]    Script Date: 24/04/2023 12:25:51 am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF OBJECT_ID('tk.usp_DeleteEmail', 'P') IS NOT NULL
BEGIN
    DROP PROCEDURE tk.usp_DeleteEmail
END
GO

CREATE PROCEDURE [tk].[usp_DeleteEmail]
AS
BEGIN
	SET NOCOUNT ON	

	BEGIN TRY
		
		BEGIN TRANSACTION

		--Delete records for the all tenants
		DELETE FROM AionDb.tk.EmailQueue 
		WHERE CreationTime < DATEADD(MONTH, -3, GETDATE())
			AND [Status] IN ('X', 'S');

		COMMIT TRANSACTION
		
	END TRY

	BEGIN CATCH
		IF @@trancount > 0 ROLLBACK TRANSACTION
		EXEC dbo.usp_error_handler_sp
		RETURN 55555
	END CATCH
	
END

