--Apply to All Tenant DBs
SET ANSI_PADDING ON
GO

IF NOT EXISTS (
    SELECT 1 
    FROM sys.indexes 
    WHERE name = 'IX_TKSettings_TenantId_Key' 
    AND object_id = OBJECT_ID('tk.TKSettings')
)
BEGIN
	WITH CTE_Duplicates AS (
		SELECT
			TenantId,
			[Key],
			ROW_NUMBER() OVER (PARTITION BY TenantId, [Key] ORDER BY Id ASC) AS RowNumber
		FROM
			[tk].[TKSettings]
	)
	DELETE FROM CTE_Duplicates
	WHERE RowNumber > 1;

	CREATE UNIQUE NONCLUSTERED INDEX [IX_TKSettings_TenantId_Key] ON [tk].[TKSettings]
	(
		[TenantId] ASC,
		[Key] ASC
	)
	WHERE ([TenantId] IS NOT NULL)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO

