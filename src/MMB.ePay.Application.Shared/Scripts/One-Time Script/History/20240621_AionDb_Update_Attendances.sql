IF NOT EXISTS (
    SELECT * FROM AionDb.INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'tk'
      AND TABLE_NAME = 'Attendances'
      AND COLUMN_NAME IN ('PMBreaktimeIn', 'PMBreaktimeOut')
)
BEGIN
	ALTER TABLE AionDb.tk.Attendances
	ADD PMBreaktimeIn [smalldatetime] NULL,
		PMBreaktimeOut [smalldatetime] NULL;
END

IF NOT EXISTS (
    SELECT * FROM AionDb_Jason.INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'tk'
      AND TABLE_NAME = 'Attendances'
      AND COLUMN_NAME IN ('PMBreaktimeIn', 'PMBreaktimeOut')
)
BEGIN
    ALTER TABLE AionDb_Jason.tk.Attendances
    ADD PMBreaktimeIn [smalldatetime] NULL,
        PMBreaktimeOut [smalldatetime] NULL;
END

IF NOT EXISTS (
    SELECT * FROM AionDb_MMB.INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'tk'
      AND TABLE_NAME = 'Attendances'
      AND COLUMN_NAME IN ('PMBreaktimeIn', 'PMBreaktimeOut')
)
BEGIN
    ALTER TABLE AionDb_MMB.tk.Attendances
    ADD PMBreaktimeIn [smalldatetime] NULL,
        PMBreaktimeOut [smalldatetime] NULL;
END

IF NOT EXISTS (
    SELECT * FROM AionDb_Caritas.INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'tk'
      AND TABLE_NAME = 'Attendances'
      AND COLUMN_NAME IN ('PMBreaktimeIn', 'PMBreaktimeOut')
)
BEGIN
    ALTER TABLE AionDb_Caritas.tk.Attendances
    ADD PMBreaktimeIn [smalldatetime] NULL,
        PMBreaktimeOut [smalldatetime] NULL;
END

IF NOT EXISTS (
    SELECT * FROM AionDb_Taisho.INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'tk'
      AND TABLE_NAME = 'Attendances'
      AND COLUMN_NAME IN ('PMBreaktimeIn', 'PMBreaktimeOut')
)
BEGIN
    ALTER TABLE AionDb_Taisho.tk.Attendances
    ADD PMBreaktimeIn [smalldatetime] NULL,
        PMBreaktimeOut [smalldatetime] NULL;
END


IF NOT EXISTS (
    SELECT * FROM AionDb_AgencyVA.INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'tk'
      AND TABLE_NAME = 'Attendances'
      AND COLUMN_NAME IN ('PMBreaktimeIn', 'PMBreaktimeOut')
)
BEGIN
    ALTER TABLE AionDb_AgencyVA.tk.Attendances
    ADD PMBreaktimeIn [smalldatetime] NULL,
        PMBreaktimeOut [smalldatetime] NULL;
END


IF NOT EXISTS (
    SELECT * FROM AionDb_BMC.INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'tk'
      AND TABLE_NAME = 'Attendances'
      AND COLUMN_NAME IN ('PMBreaktimeIn', 'PMBreaktimeOut')
)
BEGIN
    ALTER TABLE AionDb_BMC.tk.Attendances
    ADD PMBreaktimeIn [smalldatetime] NULL,
        PMBreaktimeOut [smalldatetime] NULL;
END

---

IF NOT EXISTS (
    SELECT * FROM AionDb.INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'tk'
      AND TABLE_NAME = 'PostedAttendances'
      AND COLUMN_NAME IN ('PMBreaktimeIn', 'PMBreaktimeOut')
)
BEGIN
	ALTER TABLE AionDb.tk.PostedAttendances
	ADD PMBreaktimeIn [smalldatetime] NULL,
		PMBreaktimeOut [smalldatetime] NULL;
END

IF NOT EXISTS (
    SELECT * FROM AionDb_Jason.INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'tk'
      AND TABLE_NAME = 'PostedAttendances'
      AND COLUMN_NAME IN ('PMBreaktimeIn', 'PMBreaktimeOut')
)
BEGIN
    ALTER TABLE AionDb_Jason.tk.PostedAttendances
    ADD PMBreaktimeIn [smalldatetime] NULL,
        PMBreaktimeOut [smalldatetime] NULL;
END

IF NOT EXISTS (
    SELECT * FROM AionDb_MMB.INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'tk'
      AND TABLE_NAME = 'PostedAttendances'
      AND COLUMN_NAME IN ('PMBreaktimeIn', 'PMBreaktimeOut')
)
BEGIN
    ALTER TABLE AionDb_MMB.tk.PostedAttendances
    ADD PMBreaktimeIn [smalldatetime] NULL,
        PMBreaktimeOut [smalldatetime] NULL;
END

IF NOT EXISTS (
    SELECT * FROM AionDb_Caritas.INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'tk'
      AND TABLE_NAME = 'PostedAttendances'
      AND COLUMN_NAME IN ('PMBreaktimeIn', 'PMBreaktimeOut')
)
BEGIN
    ALTER TABLE AionDb_Caritas.tk.PostedAttendances
    ADD PMBreaktimeIn [smalldatetime] NULL,
        PMBreaktimeOut [smalldatetime] NULL;
END

IF NOT EXISTS (
    SELECT * FROM AionDb_Taisho.INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'tk'
      AND TABLE_NAME = 'PostedAttendances'
      AND COLUMN_NAME IN ('PMBreaktimeIn', 'PMBreaktimeOut')
)
BEGIN
    ALTER TABLE AionDb_Taisho.tk.PostedAttendances
    ADD PMBreaktimeIn [smalldatetime] NULL,
        PMBreaktimeOut [smalldatetime] NULL;
END


IF NOT EXISTS (
    SELECT * FROM AionDb_AgencyVA.INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'tk'
      AND TABLE_NAME = 'PostedAttendances'
      AND COLUMN_NAME IN ('PMBreaktimeIn', 'PMBreaktimeOut')
)
BEGIN
    ALTER TABLE AionDb_AgencyVA.tk.PostedAttendances
    ADD PMBreaktimeIn [smalldatetime] NULL,
        PMBreaktimeOut [smalldatetime] NULL;
END


IF NOT EXISTS (
    SELECT * FROM AionDb_BMC.INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_SCHEMA = 'tk'
      AND TABLE_NAME = 'PostedAttendances'
      AND COLUMN_NAME IN ('PMBreaktimeIn', 'PMBreaktimeOut')
)
BEGIN
    ALTER TABLE AionDb_BMC.tk.PostedAttendances
    ADD PMBreaktimeIn [smalldatetime] NULL,
        PMBreaktimeOut [smalldatetime] NULL;
END