ALTER TABLE AionDb.tk.AttendanceRequests
ADD [BreaktimeIn] [smalldatetime] NULL,
	[BreaktimeOut] [smalldatetime] NULL,
	[PMBreaktimeIn] [smalldatetime] NULL,
	[PMBreaktimeOut] [smalldatetime] NULL;

ALTER TABLE AionDb_Jason.tk.AttendanceRequests
ADD [BreaktimeIn] [smalldatetime] NULL,
	[BreaktimeOut] [smalldatetime] NULL,
	[PMBreaktimeIn] [smalldatetime] NULL,
	[PMBreaktimeOut] [smalldatetime] NULL;

ALTER TABLE AionDb_MMB.tk.AttendanceRequests
ADD [BreaktimeIn] [smalldatetime] NULL,
	[BreaktimeOut] [smalldatetime] NULL,
	[PMBreaktimeIn] [smalldatetime] NULL,
	[PMBreaktimeOut] [smalldatetime] NULL;

ALTER TABLE AionDb_Caritas.tk.AttendanceRequests
ADD [BreaktimeIn] [smalldatetime] NULL,
	[BreaktimeOut] [smalldatetime] NULL,
	[PMBreaktimeIn] [smalldatetime] NULL,
	[PMBreaktimeOut] [smalldatetime] NULL;

ALTER TABLE AionDb_Taisho.tk.AttendanceRequests
ADD [BreaktimeIn] [smalldatetime] NULL,
	[BreaktimeOut] [smalldatetime] NULL,
	[PMBreaktimeIn] [smalldatetime] NULL,
	[PMBreaktimeOut] [smalldatetime] NULL;

ALTER TABLE AionDb_AgencyVA.tk.AttendanceRequests
ADD [BreaktimeIn] [smalldatetime] NULL,
	[BreaktimeOut] [smalldatetime] NULL,
	[PMBreaktimeIn] [smalldatetime] NULL,
	[PMBreaktimeOut] [smalldatetime] NULL;

ALTER TABLE AionDb_BMC.tk.AttendanceRequests
ADD [BreaktimeIn] [smalldatetime] NULL,
	[BreaktimeOut] [smalldatetime] NULL,
	[PMBreaktimeIn] [smalldatetime] NULL,
	[PMBreaktimeOut] [smalldatetime] NULL;