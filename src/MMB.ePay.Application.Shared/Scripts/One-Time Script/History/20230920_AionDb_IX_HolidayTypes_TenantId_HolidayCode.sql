IF EXISTS (
    SELECT 1
    FROM sys.indexes
    WHERE name = 'IX_HolidayTypes_TenantId_HolidayCode'
        AND object_id = OBJECT_ID('tk.HolidayTypes')
)
BEGIN
    DROP INDEX IX_HolidayTypes_TenantId_HolidayCode 
		ON tk.HolidayTypes;

	ALTER TABLE tk.HolidayTypes 
	ALTER COLUMN HolidayCode NVARCHAR(3) NOT NULL;

    CREATE INDEX IX_HolidayTypes_TenantId_HolidayCode 
		ON tk.HolidayTypes (TenantId, HolidayCode);
END