﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace MMB.ePay.Application.Shared
{
    public class CustomValidations
    {


        public class DecimalGreaterThanAttribute : ValidationAttribute
        {
            public string SecondNumPropertyName { get; set; }

            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                PropertyInfo secondValueProperty = validationContext.ObjectType.GetProperty(SecondNumPropertyName);
                var raw2ndNumber = secondValueProperty.GetValue(validationContext.ObjectInstance, null);

                var errorMessage = ErrorMessage ?? "Has to be greater than " + SecondNumPropertyName;

                if (value == null || raw2ndNumber == null)
                    return new ValidationResult(errorMessage);

                if ((decimal)value < (decimal)raw2ndNumber)
                    return new ValidationResult(errorMessage);

                return ValidationResult.Success;
            }
        }

        public class NotEqualTo : ValidationAttribute
        {
            public double Number { get; set; }

            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                if ((decimal?)value == (decimal)Number)
                {
                    if (ErrorMessage == null)
                        return new ValidationResult(ErrorMessage);

                    return new ValidationResult(validationContext.DisplayName + " must not be equal to " + Number);
                }

                return ValidationResult.Success;
            }
        }

        public class BeforeEndDateAttribute : ValidationAttribute
        {
            public string EndDatePropertyName { get; set; }

            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                PropertyInfo endDateProperty = validationContext.ObjectType.GetProperty(EndDatePropertyName);
                var rawEndDate = endDateProperty.GetValue(validationContext.ObjectInstance, null);

                if (rawEndDate == null || value == null)
                    return ValidationResult.Success;

                var endDate = (DateTime)rawEndDate;
                var startDate = DateTime.Parse(value.ToString());

                if (endDate >= startDate)
                {
                    return ValidationResult.Success;
                }

                return new ValidationResult("Invalid Date/Time Range.");
            }
        }

        public class OneDayBeforeEndDateAttribute : ValidationAttribute
        {
            public string StartDatePropertyName { get; set; }
            public string StartTimePropertyName { get; set; }
            public string EndDatePropertyName { get; set; }
            public string EndTimePropertyName { get; set; }

            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                var startDate = (DateTime)validationContext.ObjectType.GetProperty(StartDatePropertyName)
                    .GetValue(validationContext.ObjectInstance, null);

                var startTime = (DateTime)validationContext.ObjectType.GetProperty(StartTimePropertyName)
                    .GetValue(validationContext.ObjectInstance, null);

                var endDate = (DateTime)validationContext.ObjectType.GetProperty(EndDatePropertyName)
                    .GetValue(validationContext.ObjectInstance, null);

                var endTime = (DateTime)validationContext.ObjectType.GetProperty(EndTimePropertyName)
                    .GetValue(validationContext.ObjectInstance, null);

                var start = startDate + startTime.TimeOfDay;
                var end = endDate + endTime.TimeOfDay;

                if (end <= start.AddHours(24) && end >= start)
                    return ValidationResult.Success;

                return new ValidationResult(string.Format("{0} must be within 1 day after {1}.",
                    EndDatePropertyName, StartDatePropertyName));
            }
        }

        public class ListHasElements : ValidationAttribute
        {
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                if (value == null)
                    new ValidationResult(ErrorMessage ?? "At least 1 is required.");

                var list = (List<string>)value;
                if (list.Any())
                    return ValidationResult.Success;

                return new ValidationResult(ErrorMessage ?? "At least 1 is required.");
            }
        }

        public class UniqueList : ValidationAttribute
        {
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                if (value == null)
                    return ValidationResult.Success;

                var list = value as List<string>;
                if (list.Distinct().Count() == list.Count())
                {
                    return ValidationResult.Success;
                }

                return new ValidationResult(ErrorMessage ?? "List is not unique");
            }
        }

        public class MustBeTrue : ValidationAttribute
        {
            protected override ValidationResult IsValid(object value, ValidationContext validationContext)
            {
                if ((bool)value)
                    return ValidationResult.Success;

                return new ValidationResult(ErrorMessage);
            }
        }
    }
}
