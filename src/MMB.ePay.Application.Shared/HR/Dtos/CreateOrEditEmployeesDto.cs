﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.HR.Dtos
{
    public class CreateOrEditEmployeesDto : EntityDto<int?>
    {

        [Required]
        [StringLength(EmployeesConsts.MaxEmployeeCodeLength, MinimumLength = EmployeesConsts.MinEmployeeCodeLength)]
        public string EmployeeCode { get; set; }

        [Required]
        [StringLength(EmployeesConsts.MaxLastNameLength, MinimumLength = EmployeesConsts.MinLastNameLength)]
        public string LastName { get; set; }

        [Required]
        [StringLength(EmployeesConsts.MaxFirstNameLength, MinimumLength = EmployeesConsts.MinFirstNameLength)]
        public string FirstName { get; set; }

        [StringLength(EmployeesConsts.MaxMiddleNameLength, MinimumLength = EmployeesConsts.MinMiddleNameLength)]
        public string MiddleName { get; set; }

        [Required]
        [StringLength(EmployeesConsts.MaxEmailLength, MinimumLength = EmployeesConsts.MinEmailLength)]
        public string Email { get; set; }

        [StringLength(EmployeesConsts.MaxGenderLength, MinimumLength = EmployeesConsts.MinGenderLength)]
        public string Gender { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public DateTime? DateEmployed { get; set; }

        public DateTime? DateTerminated { get; set; }

        [StringLength(EmployeesConsts.MaxAddressLength, MinimumLength = EmployeesConsts.MinAddressLength)]
        public string Address { get; set; }

        [StringLength(EmployeesConsts.MaxPositionLength, MinimumLength = EmployeesConsts.MinPositionLength)]
        public string Position { get; set; }

        [StringLength(EmployeesConsts.MaxEmployeeStatusLength, MinimumLength = EmployeesConsts.MinEmployeeStatusLength)]
        public string EmployeeStatus { get; set; }

        [Required]
        public int EmploymentTypeId { get; set; }

        public string Rank { get; set; }
    }
}