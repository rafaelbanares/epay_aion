﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.HR.Dtos
{
    public class EmploymentTypesDto : EntityDto<string>
    {
        public string Description { get; set; }

    }
}