﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.HR.Dtos
{
    public class EmployeesDto : EntityDto
    {
        public string EmployeeId { get; set; }
        public string FullName { get; set; }

        public string Shift { get; set; }

        public string Type { get; set; }

        public string Status { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string Position { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public DateTime? DateEmployed { get; set; }

        public DateTime? DateTerminated { get; set; }

        public string Gender { get; set; }

        public string EmployeeStatus { get; set; }

        public string EmploymentType { get; set; }

        public string Rank { get; set; }

        public string RestDay { get; set; }

        public string Location { get; set; }

        public string Frequency { get; set; }

        public string ShiftType { get; set; }

        public bool Overtime { get; set; }

        public bool Undertime { get; set; }

        public bool Tardy { get; set; }

        public bool NonSwiper { get; set; }
    }
}