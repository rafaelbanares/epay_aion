﻿using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.HR.Dtos
{
    public class CreateOrEditGroupSettingsDto : EntityDto<int?>
    {

        [Required]
        [StringLength(GroupSettingsConsts.MaxDisplayNameLength, MinimumLength = GroupSettingsConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [Required]
        public short OrderNo { get; set; }

    }
}