﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.HR.Dtos
{
    public class GetAllForLookupTableInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}