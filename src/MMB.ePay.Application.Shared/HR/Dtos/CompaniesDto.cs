﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.HR.Dtos
{
    public class CompaniesDto : EntityDto
    {
        public string DisplayName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string OrgUnitLevel1Name { get; set; }
        public string OrgUnitLevel2Name { get; set; }
        public string OrgUnitLevel3Name { get; set; }
    }
}