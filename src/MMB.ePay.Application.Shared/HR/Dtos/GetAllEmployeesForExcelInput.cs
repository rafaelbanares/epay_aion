﻿using Abp.Application.Services.Dto;
using System;

namespace MMB.ePay.HR.Dtos
{
    public class GetAllEmployeesForExcelInput
    {
        public string Filter { get; set; }

        public string EmployeeCodeFilter { get; set; }

        public string LastNameFilter { get; set; }

        public string FirstNameFilter { get; set; }

        public string MiddleNameFilter { get; set; }

        public string EmailFilter { get; set; }

        public string AddressFilter { get; set; }

        public string PositionFilter { get; set; }

        public DateTime? MaxDateOfBirthFilter { get; set; }
        public DateTime? MinDateOfBirthFilter { get; set; }

        public DateTime? MaxDateEmployedFilter { get; set; }
        public DateTime? MinDateEmployedFilter { get; set; }

        public DateTime? MaxDateTerminatedFilter { get; set; }
        public DateTime? MinDateTerminatedFilter { get; set; }

        public string GenderFilter { get; set; }

        public string EmployeeStatusFilter { get; set; }

        public string EmploymentTypeCodeFilter { get; set; }

    }
}