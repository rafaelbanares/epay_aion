﻿using Abp.Application.Services.Dto;
using MMB.ePay.HR;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.HR.Dtos
{
    public class CreateOrEditCompaniesDto : EntityDto<int?>
    {
        [Required]
        [StringLength(CompaniesConsts.MaxDisplayNameLength, MinimumLength = CompaniesConsts.MinDisplayNameLength)]
        public string DisplayName { get; set; }

        [StringLength(CompaniesConsts.MaxAddress1Length, MinimumLength = CompaniesConsts.MinAddress1Length)]
        public string Address1 { get; set; }

        [StringLength(CompaniesConsts.MaxAddress2Length, MinimumLength = CompaniesConsts.MinAddress2Length)]
        public string Address2 { get; set; }

        [StringLength(CompaniesConsts.MaxOrgUnitLevel1NameLength, MinimumLength = CompaniesConsts.MinOrgUnitLevel1NameLength)]
        public string OrgUnitLevel1Name { get; set; }

        [StringLength(CompaniesConsts.MaxOrgUnitLevel2NameLength, MinimumLength = CompaniesConsts.MinOrgUnitLevel2NameLength)]
        public string OrgUnitLevel2Name { get; set; }

        [StringLength(CompaniesConsts.MaxOrgUnitLevel3NameLength, MinimumLength = CompaniesConsts.MinOrgUnitLevel3NameLength)]
        public string OrgUnitLevel3Name { get; set; }
    }
}