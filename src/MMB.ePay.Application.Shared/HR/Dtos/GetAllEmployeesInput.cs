﻿using Abp.Application.Services.Dto;

namespace MMB.ePay.HR.Dtos
{
    public class GetAllEmployeesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public int TypeFilter { get; set; }
        public string StatusFilter { get; set; }
    }
}