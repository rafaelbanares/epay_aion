﻿using System;
using Abp.Application.Services.Dto;

namespace MMB.ePay.HR.Dtos
{
    public class GroupSettingsDto : EntityDto<string>
    {
        public string DisplayName { get; set; }

        public short OrderNo { get; set; }

    }
}