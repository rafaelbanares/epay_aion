﻿namespace MMB.ePay.HR.Dtos
{
    public class GetCompaniesForEditOutput
    {
        public CreateOrEditCompaniesDto Companies { get; set; }

    }
}