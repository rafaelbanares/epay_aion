﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using MMB.ePay.HR.Dtos;
using System;
using System.Threading.Tasks;

namespace MMB.ePay.Timekeeping
{
    public interface ICompaniesAppService : IApplicationService
    {
        Task<Tuple<string, string, string>> GetOrgNames();

        Task<PagedResultDto<GetCompaniesForViewDto>> GetAll(GetAllCompaniesInput input);

        Task<GetCompaniesForViewDto> GetCompaniesForView(int id);

        Task<GetCompaniesForEditOutput> GetCompaniesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditCompaniesDto input);

        Task Delete(EntityDto input);

    }
}