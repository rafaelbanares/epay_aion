﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.HR.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.HR
{
    public interface IEmploymentTypesAppService : IApplicationService
    {
        Task<IList<SelectListItem>> GetEmploymentTypeList();

        Task<IList<SelectListItem>> GetEmploymentTypeListForFilter();

        Task<PagedResultDto<GetEmploymentTypesForViewDto>> GetAll(GetAllEmploymentTypesInput input);

        Task<GetEmploymentTypesForViewDto> GetEmploymentTypesForView(int id);

        Task<GetEmploymentTypesForEditOutput> GetEmploymentTypesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditEmploymentTypesDto input);

        Task Delete(EntityDto input);
    }
}