﻿using System;
using Abp.Notifications;
using MMB.ePay.Dto;

namespace MMB.ePay.Notifications.Dto
{
    public class GetUserNotificationsInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}