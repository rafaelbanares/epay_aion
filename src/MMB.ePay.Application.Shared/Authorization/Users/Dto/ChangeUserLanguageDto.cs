﻿using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Authorization.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}
