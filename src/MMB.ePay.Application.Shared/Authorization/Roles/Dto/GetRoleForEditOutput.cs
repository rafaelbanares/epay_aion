using System.Collections.Generic;
using MMB.ePay.Authorization.Permissions.Dto;

namespace MMB.ePay.Authorization.Roles.Dto
{
    public class GetRoleForEditOutput
    {
        public RoleEditDto Role { get; set; }

        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}