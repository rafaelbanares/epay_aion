﻿using System.Collections.Generic;

namespace MMB.ePay.Authorization.Roles.Dto
{
    public class GetRolesInput
    {
        public List<string> Permissions { get; set; }
    }
}
