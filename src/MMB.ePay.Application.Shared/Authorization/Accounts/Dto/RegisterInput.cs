﻿using Abp.Auditing;
using Abp.Authorization.Users;
using Abp.Extensions;
using MMB.ePay.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Authorization.Accounts.Dto
{
    public class RegisterInput : IValidatableObject
    {
        [Required]
        [StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxSurnameLength)]
        public string Surname { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxPlainPasswordLength)]
        [DisableAuditing]
        public string Password { get; set; }

        [DisableAuditing]
        public string CaptchaResponse { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!UserName.IsNullOrEmpty())
            {
                if (!UserName.Equals(EmailAddress, StringComparison.OrdinalIgnoreCase) && ValidationHelper.IsEmail(UserName))
                {
                    yield return new ValidationResult("Username cannot be an email address unless it's same with your email address !");
                }
            }
        }
    }

    public class TimeEntryLogsDto
    {
        public string UserNameOrEmailAddress { get; set; }

        public string Password { get; set; }

        public bool WorkFromHome { get; set; }

        public string InOut { get; set; }

        public bool IsMobile { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }

    public class GetAllTimeEntryInput
    {
        public string UserNameOrEmailAddressFilter { get; set; }

        public IList<TimeEntries> TimeEntriesList { get; set; }
    }

    public class TimeEntries
    {
        public DateTime LogTime { get; set; }

        public string InOut { get; set; }

        public string Device { get; set; }

        public bool WorkFromHome { get; set; }
    }

}