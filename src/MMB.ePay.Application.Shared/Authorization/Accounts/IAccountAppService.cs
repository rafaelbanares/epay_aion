﻿using Abp.Application.Services;
using MMB.ePay.Authorization.Accounts.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<int?> GetTenancyNameByUsernameOrEmailAddress(string userNameOrEmail);

        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<int?> ResolveTenantId(ResolveTenantIdInput input);

        Task<RegisterOutput> Register(RegisterInput input);

        Task SendPasswordResetCode(SendPasswordResetCodeInput input);

        Task<ResetPasswordOutput> ResetPassword(ResetPasswordInput input);

        Task SendEmailActivationLink(SendEmailActivationLinkInput input);

        Task ActivateEmail(ActivateEmailInput input);

        Task<ImpersonateOutput> Impersonate(ImpersonateInput input);

        Task<ImpersonateOutput> BackToImpersonator();

        Task<SwitchToLinkedAccountOutput> SwitchToLinkedAccount(SwitchToLinkedAccountInput input);

        Task<IList<TimeEntries>> GetTimeEntries(GetAllTimeEntryInput input);

        Task TimeEntry(TimeEntryLogsDto input);
    }
}
