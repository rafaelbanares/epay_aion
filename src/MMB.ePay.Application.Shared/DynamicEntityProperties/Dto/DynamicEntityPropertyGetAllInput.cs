﻿namespace MMB.ePay.DynamicEntityProperties
{
    public class DynamicEntityPropertyGetAllInput
    {
        public string EntityFullName { get; set; }
    }
}
