﻿namespace MMB.ePay.DynamicEntityProperties.Dto
{
    public class GetAllEntitiesHasDynamicPropertyOutput
    {
        public string EntityFullName { get; set; }
    }
}