﻿namespace MMB.ePay.Tenants.Dashboard.Dto
{
    public class GetProfitShareOutput
    {
        public int[] ProfitShares { get; set; }
    }
}