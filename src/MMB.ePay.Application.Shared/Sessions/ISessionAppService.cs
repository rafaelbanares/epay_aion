﻿using System.Threading.Tasks;
using Abp.Application.Services;
using MMB.ePay.Sessions.Dto;

namespace MMB.ePay.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();

        Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken();
    }
}
