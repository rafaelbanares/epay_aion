﻿using System.Threading.Tasks;
using Abp.Webhooks;

namespace MMB.ePay.WebHooks
{
    public interface IWebhookEventAppService
    {
        Task<WebhookEvent> Get(string id);
    }
}
