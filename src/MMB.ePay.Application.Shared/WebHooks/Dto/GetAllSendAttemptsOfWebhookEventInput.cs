﻿namespace MMB.ePay.WebHooks.Dto
{
    public class GetAllSendAttemptsOfWebhookEventInput
    {
        public string Id { get; set; }
    }
}
