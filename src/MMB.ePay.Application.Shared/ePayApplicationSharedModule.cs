﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace MMB.ePay
{
    [DependsOn(typeof(ePayCoreSharedModule))]
    public class ePayApplicationSharedModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ePayApplicationSharedModule).GetAssembly());
        }
    }
}