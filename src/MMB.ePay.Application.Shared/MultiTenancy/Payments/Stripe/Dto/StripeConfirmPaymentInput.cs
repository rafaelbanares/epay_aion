﻿namespace MMB.ePay.MultiTenancy.Payments.Stripe.Dto
{
    public class StripeConfirmPaymentInput
    {
        public string StripeSessionId { get; set; }
    }
}