﻿using MMB.ePay.EntityFrameworkCore;

namespace MMB.ePay.Migrations.Seed.Host
{
    public class InitialHostDbBuilder
    {
        private readonly ePayDbContext _context;

        public InitialHostDbBuilder(ePayDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();

            _context.SaveChanges();
        }
    }
}
