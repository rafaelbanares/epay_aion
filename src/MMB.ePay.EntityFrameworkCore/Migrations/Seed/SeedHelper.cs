﻿using System;
using System.Transactions;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore.Uow;
using Abp.MultiTenancy;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Migrations.Seed.Host;
using MMB.ePay.Migrations.Seed.Tenants;

namespace MMB.ePay.Migrations.Seed
{
    public static class SeedHelper
    {
        public static void SeedHostDb(IIocResolver iocResolver)
        {
            WithDbContext<ePayDbContext>(iocResolver, SeedHostDb);
        }

        public static void SeedHostDb(ePayDbContext context)
        {
            context.SuppressAutoSetTenantId = true;

            //Host seed
            new InitialHostDbBuilder(context).Create();

            //Default tenant seed(in host database).
            var tenantId = new DefaultTenantBuilder(context).Create();

            if (tenantId > 0)
            {
                //Default data
                new TenantRoleAndUserBuilder(context, tenantId).Create();

                new InitialEmploymentTypesCreator(context, tenantId).Create();
                new InitialHolidayTypesCreator(context, tenantId).Create();
                new InitialLeaveTypesCreator(context, tenantId).Create();
                new InitialOvertimeTypesCreator(context, tenantId).Create();
                new InitialShiftTypesCreator(context, tenantId).Create();

                new InitialApproverOrdersCreator(context, tenantId).Create();
                new InitialGroupSettingsCreator(context, tenantId).Create();
                new InitialTKSettingsCreator(context, tenantId).Create();
                new InitialFrequenciesCreator(context, tenantId).Create();
                new InitialReportsCreator(context, tenantId).Create();
                new InitialStatusCreator(context, tenantId).Create();
                new InitialRequestReasonsCreator(context, tenantId).Create();

                //Test data
                new InitialCompaniesCreator(context, tenantId).Create();
                new InitialHolidaysCreator(context, tenantId).Create();
                new InitialLocationsCreator(context, tenantId).Create();
                new InitialAttendancePeriodsCreator(context, tenantId).Create();
                new InitialEmployeesCreator(context, tenantId).Create();
                new InitialEmployeeApproversCreator(context, tenantId).Create();
                new InitialAttendancesCreator(context, tenantId).Create();
                new InitialRequestsCreator(context, tenantId).Create();
                new InitialProcessedOvertimesCreator(context, tenantId).Create();
                new InitialProcessedTimesheetsCreator(context, tenantId).Create();
                new InitialPostedTimesheetsCreator(context, tenantId).Create();
                new InitialPostedOvertimesCreator(context, tenantId).Create();
            }
        }

        private static void WithDbContext<TDbContext>(IIocResolver iocResolver, Action<TDbContext> contextAction)
            where TDbContext : DbContext
        {
            using var uowManager = iocResolver.ResolveAsDisposable<IUnitOfWorkManager>();
            using var uow = uowManager.Object.Begin(TransactionScopeOption.Suppress);
            var context = uowManager.Object.Current.GetDbContext<TDbContext>(MultiTenancySides.Host);

            contextAction(context);

            uow.Complete();
        }
    }
}
