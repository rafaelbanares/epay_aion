﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{
    public class InitialAttendancesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialAttendancesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            if (!_context.Attendances.Any(e => e.TenantId == _tenantId))
            {
                var employeeIds = _context.Employees
                    .Where(e => e.TenantId == _tenantId)
                    .Select(e => e.Id)
                    .ToList();

                var shiftTypeId = _context.ShiftTypes
                    .Where(e => e.TenantId == _tenantId)
                    .Select(e => e.Id)
                    .FirstOrDefault();

                foreach (var employeeId in employeeIds)
                {
                    var attendances = Enumerable.Range(1, 15)
                        .Select(e => new Attendances
                        {
                            TenantId = _tenantId,
                            EmployeeId = employeeId,
                            RestDayCode = "67",
                            Date = new DateTime(2022, 1, e),
                            TimeIn = new DateTime(2022, 1, e, 9, 0, 0),
                            BreaktimeIn = new DateTime(2022, 1, e, 12, 0, 0),
                            BreaktimeOut = new DateTime(2022, 1, e, 13, 0, 0),
                            TimeOut = new DateTime(2022, 1, e, 18, 0, 0),
                            ShiftTypeId = shiftTypeId
                        }).ToList();

                    _context.Attendances.AddRange(attendances);
                }

                _context.SaveChanges();
            }
        }
    }
}
