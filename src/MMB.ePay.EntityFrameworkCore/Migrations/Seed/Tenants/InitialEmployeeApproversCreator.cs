﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{
    public class InitialEmployeeApproversCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialEmployeeApproversCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            if (!_context.EmployeeApprovers.Any(e => e.TenantId == _tenantId))
            {
                var employeeIds = _context.Employees.Where(e => e.TenantId == _tenantId).Select(e => e.Id).ToList();
                var approverOrderIds = _context.ApproverOrders.Where(e => e.TenantId == _tenantId).Select(e => e.Id).ToList();

                var approvers = new List<Tuple<int, int>>
                {
                    new Tuple<int, int>( employeeIds[0], approverOrderIds[0] ), //approver 1
                    new Tuple<int, int>( employeeIds[1], approverOrderIds[1] ), //backup approver 1
                    new Tuple<int, int>( employeeIds[2], approverOrderIds[2] ), //approver 2
                    new Tuple<int, int>( employeeIds[3], approverOrderIds[3] ), //backup approver 2
                };

                foreach(var employeeId in employeeIds)
                {
                    var employeeApprovers = approvers
                        .Select(e => new EmployeeApprovers
                        {
                            TenantId = _tenantId,
                            EmployeeId = employeeId,
                            ApproverId = e.Item1,
                            ApproverOrderId = e.Item2
                        }).ToList();

                    _context.EmployeeApprovers.AddRange(employeeApprovers);
                }

                _context.SaveChanges();
            }
        }
    }
}
