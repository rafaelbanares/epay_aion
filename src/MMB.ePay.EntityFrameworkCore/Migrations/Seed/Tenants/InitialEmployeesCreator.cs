﻿using Abp;
using Abp.Authorization.Users;
using Abp.Notifications;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MMB.ePay.Authorization.Roles;
using MMB.ePay.Authorization.Users;
using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.HR;
using MMB.ePay.Notifications;
using MMB.ePay.Timekeeping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{
    public class InitialEmployeesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialEmployeesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var employmentTypeId = _context.EmploymentTypes
                .Where(e => e.TenantId == _tenantId && e.SysCode == "R")
                .Select(e => e.Id)
                .FirstOrDefault();

            var shiftTypeId = _context.ShiftTypes
                .Where(e => e.TenantId == _tenantId)
                .Select(e => e.Id)
                .FirstOrDefault();

            var frequencyId = _context.Frequencies
                .Where(e => e.TenantId == _tenantId && e.DisplayName == "Semi-Monthly")
                .Select(e => e.Id)
                .FirstOrDefault();

            //User role
            var adminRole = _context.Roles.IgnoreQueryFilters()
                    .FirstOrDefault(e => e.TenantId == _tenantId && e.Name == StaticRoleNames.Tenants.Admin);

            var employees = new List<Employees>
            {
                //new Employees { FirstName = "Arthur", LastName = "Morgan", Gender = "M" }, //Approver 1
                //new Employees { FirstName = "John", LastName = "Marston", Gender = "M" },  //Backup Approver 1
                //new Employees { FirstName = "Dutch", LastName = "Van der Linde", Gender = "M" }, //Approver 2
                //new Employees { FirstName = "Bessie", LastName = "Matthews", Gender = "F" }, //Backup Approver 2
                //new Employees { FirstName = "Bill", LastName = "Williamson", Gender = "M" }
                new Employees { EmployeeCode = "000001", FirstName = "Rene", LastName = "Barnachea", MiddleName = "Sudo", Gender = "M", Email = "rene.barnachea@mmbisolutions.com" },
                new Employees { EmployeeCode = "000002", FirstName = "Remedios", LastName = "Barnachea", MiddleName = "Barnachea", Gender = "F", Email = "remie.barnachea@mmbisolutions.com" },
                new Employees { EmployeeCode = "000013", FirstName = "Rodel", LastName = "Barnachea", MiddleName = "Sudo", Gender = "M", Email = "rodel.barnachea@mmbisolutions.com" },
                new Employees { EmployeeCode = "000009", FirstName = "Rogelio", LastName = "Barnachea", MiddleName = "Belandres", Gender = "M", Email = "rogelio.barnachea@mmbisolutions.com" },
                new Employees { EmployeeCode = "000017", FirstName = "Alma Liza", LastName = "Mendoza", MiddleName = "Manalili", Gender = "F", Email = "alma.mendoza@mmbisolutions.com" },
                new Employees { EmployeeCode = "000022", FirstName = "Joan", LastName = "Espiritu", MiddleName = "Pablo", Gender = "F", Email = "joan.espiritu@mmbisolutions.com" },
                new Employees { EmployeeCode = "000023", FirstName = "Ernesto Jr.", LastName = "Lagura", MiddleName = "Dela Rosa", Gender = "M", Email = "ernesto.lagura@mmbisolutions.com" },
                new Employees { EmployeeCode = "000028", FirstName = "Maria Donna", LastName = "Belandres", MiddleName = "Tejada", Gender = "F", Email = "ipayroll@mmbisolutions.com" },
                new Employees { EmployeeCode = "000029", FirstName = "Bonn Elexis", LastName = "Aguilar", MiddleName = "Gamboa", Gender = "M", Email = "Bonn.Aguilar@mmbisolutions.com" },
                new Employees { EmployeeCode = "000032", FirstName = "Rafael", LastName = "Bañares", MiddleName = "Franco", Gender = "M", Email = "rafael.banares@mmbisolutions.com" },
                new Employees { EmployeeCode = "000037", FirstName = "Lyka", LastName = "Mangbanag", MiddleName = "Murao", Gender = "F", Email = "ipayroll@mmbisolutions.com" },
                new Employees { EmployeeCode = "000040", FirstName = "JANILEN", LastName = "FLORES", MiddleName = "BAYON", Gender = "F", Email = "accounting@mmbisolutions.com" },
                new Employees { EmployeeCode = "000041", FirstName = "Benedict Bernard", LastName = "Topinio", MiddleName = "Bartolome", Gender = "M", Email = "implem@mmbisolutions.com" },
            };

            foreach(var employee in employees.Select((value, i) => new { i, value }))
            {
                //employee.value.EmployeeCode = "Default" + employee.i;

                if (!_context.Employees.Any(e => e.TenantId == _tenantId && e.EmployeeCode == employee.value.EmployeeCode))
                {
                    employee.value.TenantId = _tenantId;
                    //employee.value.Email = string.Format("{0}.{1}@default.com",
                    //    employee.value.FirstName.Replace(" ", "").ToLower(),
                    //    employee.value.LastName.Replace(" ", "").ToLower());
                    employee.value.Address = "test address";
                    employee.value.Position = "Officer";
                    employee.value.DateOfBirth = new DateTime(1990, 1, 1);
                    employee.value.DateEmployed = new DateTime(2020, 1, 1);
                    employee.value.EmployeeStatus = "ACTIVE";
                    employee.value.EmploymentTypeId = employmentTypeId;
                    employee.value.Rank = "R&F";

                    _context.Employees.Add(employee.value);
                    _context.SaveChanges();

                    _context.EmployeeTimeInfo.Add(new EmployeeTimeInfo
                    {
                        TenantId = _tenantId,
                        EmployeeId = employee.value.Id,
                        ShiftTypeId = shiftTypeId,
                        FrequencyId = frequencyId,
                        RestDay = "67",
                        Overtime = true,
                        Undertime = true,
                        Tardy = true,
                        NonSwiper = true,
                        SwipeCode = employee.value.EmployeeCode,
                    });

                    var user = new User
                    {
                        TenantId = _tenantId,
                        UserName = employee.value.Email,
                        Name = employee.value.FirstName,
                        Surname = employee.value.LastName,
                        EmployeeId = employee.value.Id,
                        EmailAddress = employee.value.Email,
                        IsEmailConfirmed = true,
                        ShouldChangePasswordOnNextLogin = false,
                        IsActive = true
                    };

                    user.SetNormalizedNames();
                    user.Password = new PasswordHasher<User>(new OptionsWrapper<PasswordHasherOptions>
                        (new PasswordHasherOptions())).HashPassword(user, "passwordI1!");

                    _context.User.Add(user);
                    _context.SaveChanges();

                    //Assign User role to user
                    _context.UserRoles.Add(new UserRole(_tenantId, user.Id, adminRole.Id));

                    //Notification subscription
                    _context.NotificationSubscriptions.Add(
                        new NotificationSubscriptionInfo(SequentialGuidGenerator.Instance.Create(),
                            _tenantId,
                            user.Id,
                            AppNotificationNames.NewUserRegistered)
                    );

                    _context.SaveChanges();
                }
            }

        }
    }
}
