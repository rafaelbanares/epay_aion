﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{
    public class InitialProcessedOvertimesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialProcessedOvertimesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var employees = _context.Employees
                .OrderBy(e => e.Id)
                .Select(e => new { e.Id, e.EmployeeCode })
                .ToDictionary(p => p.Id, p => p.EmployeeCode);

            var overtimeTypes = _context.OvertimeTypes
                .Select(e => new { e.Id, e.ShortName })
                .ToDictionary(p => p.Id, p => p.ShortName);

            var overtimes = new List<(string, DateTime, string, int)>
            {
                ("000017", new DateTime(2024, 11, 22), "Regular OT", 175),
                ("000009", new DateTime(2024, 11, 25), "Regular OT", 120),
                ("000009", new DateTime(2024, 11, 26), "Regular OT", 120),
                ("000009", new DateTime(2024, 11, 22), "Regular OT", 120),
                ("000009", new DateTime(2024, 11, 21), "Regular OT", 120),
                ("000009", new DateTime(2024, 11, 20), "Regular OT", 120),
                ("000009", new DateTime(2024, 11, 19), "Regular OT", 120),
                ("000009", new DateTime(2024, 11, 18), "Regular OT", 120)
            };

            foreach (var overtime in overtimes)
            {
                var employeeId = employees.FirstOrDefault(e => e.Value == overtime.Item1).Key;
                var overtimeTypeId = overtimeTypes.FirstOrDefault(e => e.Value == overtime.Item3).Key;

                if (!_context.ProcessedOvertimes.Any(e => e.EmployeeId == employeeId 
                    && e.Date == overtime.Item2
                    && e.OvertimeTypeId == overtimeTypeId))
                {
                    _context.ProcessedOvertimes.Add(
                        new ProcessedOvertimes
                        {
                            TenantId = _tenantId,
                            EmployeeId = employeeId,
                            OvertimeTypeId = overtimeTypeId,
                            Date = overtime.Item2,
                            Minutes = overtime.Item4
                        });

                    _context.SaveChanges();
                }
            }
        }
    }
}
