﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System.Collections.Generic;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialFrequenciesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialFrequenciesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var frequencies = new List<Frequencies>
            {
                new() { DisplayName = "Monthly" },
                new() { DisplayName = "Semi-Monthly" },
                new() { DisplayName = "Weekly" },
            };

            foreach(var frequency in frequencies)
            {
                frequency.TenantId = _tenantId;
                frequency.Description = frequency.DisplayName;

                _context.Frequencies.Add(frequency);
            }

            _context.SaveChanges();
        }
    }
}
