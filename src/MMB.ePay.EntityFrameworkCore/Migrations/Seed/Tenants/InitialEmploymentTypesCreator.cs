﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.HR;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialEmploymentTypesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialEmploymentTypesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var employmentTypes = new List<EmploymentTypes>
            {
                new() { SysCode = "C", Description = "Contractual" },
                new() { SysCode = "P", Description = "Probationary" },
                new() { SysCode = "R", Description = "Regular" },
                new() { SysCode = "S", Description = "Consultant" }
            };

            foreach(var employmentType in employmentTypes)
            {
                if (!_context.EmploymentTypes.Any(e => e.TenantId == _tenantId && e.SysCode == employmentType.SysCode))
                {
                    employmentType.TenantId = _tenantId;
                    _context.EmploymentTypes.Add(employmentType);
                }
            }

            _context.SaveChanges();
        }
    }
}
