﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{
    public class InitialProcessedTimesheetsCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialProcessedTimesheetsCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var employeeCodes = new List<string> { "000009", "000017", "000022", "000023", "000028", "000029", "000032", "000037", "000040", "000041" };
            var employees = _context.Employees
                .Where(e => employeeCodes.Contains(e.EmployeeCode))
                .OrderBy(e => e.Id)
                .Select(e => new { e.Id, e.EmployeeCode })
                .ToDictionary(p => p.Id, p => p.EmployeeCode);

            var timesheets = new List<(string, int, double, double, string, bool, bool, DateTime)>
            {
                ("000009", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 18)),
                ("000009", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 19)),
                ("000009", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 20)),
                ("000009", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 21)),
                ("000009", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 22)),
                ("000009", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 25)),
                ("000009", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 26)),
                ("000009", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 27)),
                ("000009", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 28)),
                ("000009", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 29)),
                ("000017", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 18)),
                ("000017", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 19)),
                ("000017", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 20)),
                ("000017", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 21)),
                ("000017", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 22)),
                ("000017", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 25)),
                ("000017", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 26)),
                ("000017", 0, 0.50, 0.50, "SL, Half-Day", true, true, new DateTime(2024, 11, 27)),
                ("000017", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 28)),
                ("000017", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 29)),
                ("000022", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 18)),
                ("000022", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 19)),
                ("000022", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 20)),
                ("000022", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 21)),
                ("000022", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 22)),
                ("000022", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 25)),
                ("000022", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 26)),
                ("000022", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 27)),
                ("000022", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 28)),
                ("000022", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 29)),
                ("000023", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 18)),
                ("000023", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 19)),
                ("000023", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 20)),
                ("000023", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 21)),
                ("000023", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 22)),
                ("000023", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 25)),
                ("000023", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 26)),
                ("000023", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 27)),
                ("000023", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 28)),
                ("000023", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 29)),
                ("000028", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 18)),
                ("000028", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 19)),
                ("000028", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 20)),
                ("000028", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 21)),
                ("000028", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 22)),
                ("000028", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 25)),
                ("000028", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 26)),
                ("000028", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 27)),
                ("000028", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 28)),
                ("000028", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 29)),
                ("000029", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 18)),
                ("000029", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 19)),
                ("000029", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 20)),
                ("000029", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 21)),
                ("000029", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 22)),
                ("000029", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 25)),
                ("000029", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 26)),
                ("000029", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 27)),
                ("000029", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 28)),
                ("000029", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 29)),
                ("000032", 0, 0.00, 1.00, "SL", true, true, new DateTime(2024, 11, 18)),
                ("000032", 480, 0.00, 0.00, "", true, true, new DateTime(2024, 11, 19)),
                ("000032", 480, 0.00, 0.00, "", true, true, new DateTime(2024, 11, 20)),
                ("000032", 480, 0.00, 0.00, "", true, true, new DateTime(2024, 11, 21)),
                ("000032", 480, 0.00, 0.00, "", true, true, new DateTime(2024, 11, 22)),
                ("000032", 480, 0.00, 0.00, "", true, true, new DateTime(2024, 11, 25)),
                ("000032", 480, 0.00, 0.00, "", true, true, new DateTime(2024, 11, 26)),
                ("000032", 480, 0.00, 0.00, "", true, true, new DateTime(2024, 11, 27)),
                ("000032", 0, 1.00, 0.00, "Invalid", true, true, new DateTime(2024, 11, 28)),
                ("000032", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 29)),
                ("000037", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 18)),
                ("000037", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 19)),
                ("000037", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 20)),
                ("000037", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 21)),
                ("000037", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 22)),
                ("000037", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 25)),
                ("000037", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 26)),
                ("000037", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 27)),
                ("000037", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 28)),
                ("000037", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 29)),
                ("000040", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 18)),
                ("000040", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 19)),
                ("000040", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 20)),
                ("000040", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 21)),
                ("000040", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 22)),
                ("000040", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 25)),
                ("000040", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 26)),
                ("000040", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 27)),
                ("000040", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 28)),
                ("000040", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 29)),
                ("000041", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 18)),
                ("000041", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 19)),
                ("000041", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 20)),
                ("000041", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 21)),
                ("000041", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 22)),
                ("000041", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 25)),
                ("000041", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 26)),
                ("000041", 0, 1.00, 0.00, "Absent", true, true, new DateTime(2024, 11, 27)),
                ("000041", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 28)),
                ("000041", 0, 1.00, 0.00, "", true, true, new DateTime(2024, 11, 29)),
            };

            foreach (var timesheet in timesheets)
            {
                var employeeId = employees.FirstOrDefault(e => e.Value == timesheet.Item1).Key;

                if (!_context.ProcessedTimesheets.Any(e => e.EmployeeId == employeeId && e.Date == timesheet.Item8))
                {
                    _context.ProcessedTimesheets.Add(
                        new ProcessedTimesheets
                        {
                            TenantId = _tenantId,
                            EmployeeId = employeeId,
                            RegMins = timesheet.Item2,
                            Absence = (decimal)timesheet.Item3,
                            LeaveDays = (decimal)timesheet.Item4,
                            Remarks = timesheet.Item5,
                            IsRestDay = timesheet.Item6,
                            IsHoliday = timesheet.Item7,
                            Date = timesheet.Item8
                        });

                    _context.SaveChanges();
                }
            }

            //int[] dates = { 1, 4, 5, 6, 7, 8 };

            //for (var i = 0; i < dates.Length; i++)
            //{
            //    var date = new DateTime(2022, 1, dates[i]);

            //    if (!_context.ProcessedTimesheets.Any(e => e.TenantId == _tenantId && e.EmployeeId == 1 && e.Date == date))
            //    {
            //        var testVal = (i + 1) * 60;

            //        _context.ProcessedTimesheets.Add(
            //            new ProcessedTimesheets
            //            {
            //                TenantId = _tenantId,
            //                EmployeeId = 1,
            //                Date = date,
            //                UtMins = testVal,
            //                PaidHoliday = testVal,
            //                TardyMins = testVal,
            //                Absence = testVal,
            //                RegMins = testVal,
            //                RegNd1Mins = testVal,
            //                RegNd2Mins = testVal
            //            });

            //        _context.SaveChanges();
            //    }
            //}
        }
    }
}
