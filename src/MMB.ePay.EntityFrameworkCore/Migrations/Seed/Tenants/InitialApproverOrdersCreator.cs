﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialApproverOrdersCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialApproverOrdersCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var approverOrders = new List<ApproverOrders>
            {
                new() { DisplayName = "Approver 1", Level = 1, IsBackup = false },
                new() { DisplayName = "Backup Approver 1", Level = 1, IsBackup = false },
                new() { DisplayName = "Approver 2", Level = 2, IsBackup = false },
                new() { DisplayName = "Backup Approver 2", Level = 2, IsBackup = false },
            };

            foreach(var approverOrder in approverOrders)
            {
                if (!_context.ApproverOrders.Any(e => e.TenantId == _tenantId && e.DisplayName == approverOrder.DisplayName))
                {
                    approverOrder.TenantId = _tenantId;

                    _context.ApproverOrders.Add(approverOrder);
                    _context.SaveChanges();
                }
            };
        }
    }
}
