﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialLeaveTypesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialLeaveTypesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var leaveTypes = new List<LeaveTypes>
            {
                new() { DisplayName = "SL", Description = "Sick Leave", WithPay = true },
                new() { DisplayName = "VL", Description = "Vacation Leave", WithPay = true },
                new() { DisplayName = "EL", Description = "Emergency Leave", WithPay = true },
                new() { DisplayName = "ML", Description = "Maternity Leave", WithPay = true },
                new() { DisplayName = "PL", Description = "Paternity Leave", WithPay = true }
            };

            foreach(var leaveType in leaveTypes)
            {
                if (!_context.LeaveTypes.Any(e => e.TenantId == _tenantId && e.DisplayName == leaveType.DisplayName))
                {
                    leaveType.TenantId = _tenantId;
                    _context.LeaveTypes.Add(leaveType);
                }
            }

            _context.SaveChanges();
        }
    }
}
