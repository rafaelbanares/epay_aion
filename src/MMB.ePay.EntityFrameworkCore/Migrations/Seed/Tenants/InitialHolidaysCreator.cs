﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialHolidaysCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialHolidaysCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var legalHolidayTypeId = _context.HolidayTypes
                .Where(e => e.TenantId == _tenantId && e.HolidayCode == "L")
                .Select(e => e.Id)
                .FirstOrDefault();

            var specialHolidayTypeId = _context.HolidayTypes
                .Where(e => e.TenantId == _tenantId && e.HolidayCode == "S")
                .Select(e => e.Id)
                .FirstOrDefault();

            var holidays = new List<Holidays>
            {
                new() { DisplayName = "New Year's Day", Date = new DateTime(2022, 01, 01), HolidayTypeId = legalHolidayTypeId },
                new() { DisplayName = "Araw ng Kagitingan | Maundy Thursday", Date = new DateTime(2022, 04, 14), HolidayTypeId = legalHolidayTypeId },
                new() { DisplayName = "Good Friday", Date = new DateTime(2022, 04, 15), HolidayTypeId = legalHolidayTypeId},
                new() { DisplayName = "Labor Day", Date = new DateTime(2022, 05, 01), HolidayTypeId = legalHolidayTypeId },
                new() { DisplayName = "Rizal Day", Date = new DateTime(2022, 12, 30), HolidayTypeId = legalHolidayTypeId },
                new() { DisplayName = "Independence Day", Date = new DateTime(2022, 06, 13), HolidayTypeId = legalHolidayTypeId },
                new() { DisplayName = "National Heroes Day", Date = new DateTime(2022, 08, 29), HolidayTypeId = legalHolidayTypeId },
                new() { DisplayName = "Bonifacio Day", Date = new DateTime(2022, 11, 30), HolidayTypeId = legalHolidayTypeId },
                new() { DisplayName = "Christmas Day", Date = new DateTime(2022, 12, 25), HolidayTypeId = legalHolidayTypeId },
                new() { DisplayName = "Chinese New Year", Date = new DateTime(2022, 02, 01), HolidayTypeId = specialHolidayTypeId },
                new() { DisplayName = "EDSA Revolution Anniversary", Date = new DateTime(2022, 02, 25), HolidayTypeId = specialHolidayTypeId },
                new() { DisplayName = "Black Saturday", Date = new DateTime(2022, 04, 16), HolidayTypeId = specialHolidayTypeId },
                new() { DisplayName = "Ninoy Aquino Day", Date = new DateTime(2022, 08, 21), HolidayTypeId = specialHolidayTypeId },
                new() { DisplayName = "All Saint's Day", Date = new DateTime(2022, 11, 01), HolidayTypeId = specialHolidayTypeId },
                new() { DisplayName = "All Soul's Day", Date = new DateTime(2022, 11, 02), HolidayTypeId = specialHolidayTypeId },
                new() { DisplayName = "Feast of the Immaculate Conception", Date = new DateTime(2022, 12, 08), HolidayTypeId = specialHolidayTypeId },
                new() { DisplayName = "New Year's Eve", Date = new DateTime(2022, 12, 24), HolidayTypeId = specialHolidayTypeId },
                new() { DisplayName = "Last Day of the Year", Date = new DateTime(2022, 12, 31), HolidayTypeId = specialHolidayTypeId },
            };

            foreach (var holiday in holidays)
            {
                if (!_context.Holidays.Any(e => e.TenantId == _tenantId && e.Date == holiday.Date))
                {
                    holiday.TenantId = _tenantId;
                    holiday.HolidayTypeId = 1;
                    holiday.HalfDay = false;
                    holiday.Year = 2022;

                    _context.Holidays.Add(holiday);
                }
            }

            _context.SaveChanges();
        }
    }
}
