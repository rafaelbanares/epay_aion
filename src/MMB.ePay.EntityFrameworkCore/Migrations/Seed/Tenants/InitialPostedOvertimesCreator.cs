﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{
    public class InitialPostedOvertimesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialPostedOvertimesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var employees = _context.Employees
                .OrderBy(e => e.Id)
                .Select(e => new { e.Id, e.EmployeeCode })
                .ToDictionary(p => p.Id, p => p.EmployeeCode);

            var overtimeTypes = _context.OvertimeTypes
                .Select(e => new { e.Id, e.ShortName })
                .ToDictionary(p => p.Id, p => p.ShortName);

            var overtimes = new List<(string, DateTime, string, int)>
            {
                ("000009", new DateTime(2023, 4, 18), "Regular OT", 60),
                ("000009", new DateTime(2023, 4, 18), "Regular OT ND2", 60),
                ("000009", new DateTime(2023, 4, 19), "Regular OT ND2", 60),
                ("000009", new DateTime(2023, 4, 19), "Regular OT", 60),
                ("000009", new DateTime(2023, 4, 20), "Regular OT ND2", 60),
                ("000009", new DateTime(2023, 4, 20), "Regular OT", 60),
                ("000009", new DateTime(2023, 4, 21), "Regular OT ND2", 60),
                ("000009", new DateTime(2023, 4, 21), "Regular OT", 60),
                ("000009", new DateTime(2024, 4, 10), "Regular OT", 120),
                ("000009", new DateTime(2024, 4, 10), "Regular OT ND1", 120),
                ("000009", new DateTime(2024, 4, 11), "Regular OT", 120),
                ("000009", new DateTime(2024, 4, 11), "Regular OT ND1", 120),
                ("000009", new DateTime(2024, 4, 12), "Regular OT", 120),
                ("000009", new DateTime(2024, 4, 12), "Regular OT ND1", 120),
                ("000009", new DateTime(2024, 4, 15), "Regular OT ND1", 120),
                ("000009", new DateTime(2024, 4, 15), "Regular OT", 120),
                ("000017", new DateTime(2024, 4, 13), "Regular OT", 60),
                ("000017", new DateTime(2024, 4, 16), "Regular OT", 120),
                ("000017", new DateTime(2024, 8, 9), "Regular OT", 60),
                ("000017", new DateTime(2024, 8, 10), "Regular OT", 120),
                ("000017", new DateTime(2024, 8, 13), "Regular OT", 60),
                ("000017", new DateTime(2024, 8, 14), "Regular OT", 60),
                ("000017", new DateTime(2024, 8, 15), "Regular OT", 60),
                ("000017", new DateTime(2024, 8, 16), "Regular OT", 60),
                ("000017", new DateTime(2024, 8, 17), "Regular OT", 120),
                ("000017", new DateTime(2024, 8, 20), "Regular OT", 60),
                ("000017", new DateTime(2024, 8, 21), "Regular OT", 60),
                ("000017", new DateTime(2024, 8, 22), "Regular OT", 60),
                ("000017", new DateTime(2024, 8, 23), "Regular OT", 60),
                ("000017", new DateTime(2024, 8, 24), "Regular OT", 60),
                ("000017", new DateTime(2024, 8, 27), "Regular OT", 60),
                ("000017", new DateTime(2024, 8, 28), "Regular OT", 60),
                ("000017", new DateTime(2024, 8, 30), "Regular OT", 60),
                ("000017", new DateTime(2024, 10, 11), "Regular OT", 60),
                ("000017", new DateTime(2024, 10, 12), "Regular OT", 60),
                ("000017", new DateTime(2024, 10, 15), "Regular OT", 60),
                ("000017", new DateTime(2024, 10, 16), "Regular OT", 60),
                ("000017", new DateTime(2024, 10, 17), "Regular OT", 60),
                ("000017", new DateTime(2024, 10, 22), "Regular OT", 60),
                ("000017", new DateTime(2024, 10, 31), "Regular OT", 60),
                ("000017", new DateTime(2024, 11, 1), "Regular OT", 60),
                ("000022", new DateTime(2023, 10, 3), "Regular OT ND2", 60),
                ("000022", new DateTime(2023, 10, 3), "Regular OT", 240),
                ("000022", new DateTime(2023, 10, 4), "Regular OT", 240),
                ("000022", new DateTime(2023, 10, 4), "Regular OT ND2", 60),
                ("000022", new DateTime(2023, 10, 5), "Regular OT", 240),
                ("000022", new DateTime(2023, 10, 5), "Regular OT ND2", 60),
                ("000022", new DateTime(2023, 10, 6), "Regular OT", 120),
                ("000022", new DateTime(2023, 10, 6), "Regular OT ND2", 60),
                ("000022", new DateTime(2023, 10, 10), "Regular OT", 240),
                ("000022", new DateTime(2023, 10, 10), "Regular OT ND2", 60),
                ("000022", new DateTime(2023, 10, 11), "Regular OT", 240),
                ("000022", new DateTime(2023, 10, 11), "Regular OT ND2", 60),
                ("000022", new DateTime(2023, 10, 12), "Regular OT", 240),
                ("000022", new DateTime(2023, 10, 12), "Regular OT ND2", 60),
                ("000022", new DateTime(2023, 10, 13), "Regular OT", 240),
                ("000022", new DateTime(2023, 10, 13), "Regular OT ND2", 60),
                ("000022", new DateTime(2023, 10, 23), "Regular OT", 120),
                ("000022", new DateTime(2023, 10, 30), "Regular OT", 120),
                ("000022", new DateTime(2023, 11, 6), "Regular OT", 120),
                ("000022", new DateTime(2023, 11, 13), "Regular OT", 120),
                ("000022", new DateTime(2023, 11, 20), "Regular OT", 120),
                ("000022", new DateTime(2023, 12, 18), "Regular OT", 120),
                ("000022", new DateTime(2024, 1, 2), "Regular OT", 120),
                ("000023", new DateTime(2022, 12, 3), "Rest Day OT ND2", 240),
                ("000023", new DateTime(2022, 12, 3), "Rest Day OT ND1", 180),
                ("000023", new DateTime(2022, 12, 3), "Rest Day OT", 420),
                ("000023", new DateTime(2022, 12, 4), "Rest Day OT ND2", 240),
                ("000023", new DateTime(2022, 12, 4), "Rest Day OT ND1", 180),
                ("000023", new DateTime(2022, 12, 4), "Rest Day OT", 420),
                ("000023", new DateTime(2022, 12, 10), "Rest Day OT ND2", 240),
                ("000023", new DateTime(2022, 12, 10), "Rest Day OT ND1", 180),
                ("000023", new DateTime(2022, 12, 10), "Rest Day OT", 420),
                ("000023", new DateTime(2024, 2, 6), "Regular OT", 60),
                ("000023", new DateTime(2024, 2, 7), "Regular OT", 54),
                ("000023", new DateTime(2024, 2, 8), "Regular OT", 55),
                ("000023", new DateTime(2024, 2, 9), "Regular OT", 60),
                ("000023", new DateTime(2024, 2, 13), "Regular OT", 60),
                ("000023", new DateTime(2024, 2, 14), "Regular OT", 60),
                ("000023", new DateTime(2024, 2, 15), "Regular OT", 60),
                ("000023", new DateTime(2024, 10, 28), "Regular OT", 59),
                ("000023", new DateTime(2024, 10, 29), "Regular OT", 60),
                ("000023", new DateTime(2024, 10, 30), "Regular OT", 60),
                ("000028", new DateTime(2023, 11, 1), "Regular OT", 180),
                ("000028", new DateTime(2023, 11, 1), "Regular OT ND2", 60),
                ("000028", new DateTime(2023, 11, 2), "Regular OT", 180),
                ("000028", new DateTime(2023, 11, 2), "Regular OT ND2", 60),
                ("000028", new DateTime(2023, 11, 3), "Regular OT", 180),
                ("000028", new DateTime(2023, 11, 3), "Regular OT ND2", 60),
                ("000028", new DateTime(2023, 11, 6), "Regular OT", 60),
                ("000028", new DateTime(2023, 11, 7), "Regular OT", 60),
                ("000028", new DateTime(2023, 11, 9), "Regular OT", 60),
                ("000028", new DateTime(2023, 11, 16), "Regular OT", 60),
                ("000028", new DateTime(2023, 11, 20), "Regular OT", 60),
                ("000028", new DateTime(2023, 11, 21), "Regular OT", 180),
                ("000028", new DateTime(2023, 11, 27), "Regular OT", 180),
                ("000028", new DateTime(2023, 11, 28), "Regular OT", 180),
                ("000028", new DateTime(2023, 11, 29), "Regular OT", 120),
                ("000028", new DateTime(2024, 11, 11), "Regular OT", 117),
                ("000028", new DateTime(2024, 11, 12), "Regular OT", 120)
            };

            foreach (var overtime in overtimes)
            {
                var employeeId = employees.FirstOrDefault(e => e.Value == overtime.Item1).Key;
                var overtimeTypeId = overtimeTypes.FirstOrDefault(e => e.Value == overtime.Item3).Key;

                if (!_context.PostedOvertimes.Any(e => e.EmployeeId == employeeId
                    && e.Date == overtime.Item2
                    && e.OvertimeTypeId == overtimeTypeId))
                {
                    _context.PostedOvertimes.Add(
                        new PostedOvertimes
                        {
                            TenantId = _tenantId,
                            EmployeeId = employeeId,
                            OvertimeTypeId = overtimeTypeId,
                            Date = overtime.Item2,
                            Minutes = overtime.Item4
                        });

                    _context.SaveChanges();
                }
            }
        }
    }
}
