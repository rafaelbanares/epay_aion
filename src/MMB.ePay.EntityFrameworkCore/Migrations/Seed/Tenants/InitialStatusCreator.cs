﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialStatusCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialStatusCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            if (!_context.Status.Any(e => e.TenantId == _tenantId))
            {
                var newStatus = new Status
                {
                    TenantId = _tenantId,
                    DisplayName = "New",
                    AfterStatusMessage = "Created",
                    BeforeStatusActionText = null,
                    IsEditable = true,
                    AccessLevel = 0
                };

                var forApprovalStatus = new Status
                {
                    TenantId = _tenantId,
                    DisplayName = "For Approval",
                    AfterStatusMessage = "Sent For Approval",
                    BeforeStatusActionText = "Send For Approval",
                    IsEditable = false,
                    AccessLevel = 1
                };

                var forNextApprovalStatus = new Status
                {
                    TenantId = _tenantId,
                    DisplayName = "For Next Approval",
                    AfterStatusMessage = "Pre-Approved",
                    BeforeStatusActionText = "Approve",
                    IsEditable = false,
                    AccessLevel = 2
                };

                var declinedStatus = new Status
                {
                    TenantId = _tenantId,
                    DisplayName = "Declined",
                    AfterStatusMessage = "Declined",
                    BeforeStatusActionText = "Decline",
                    IsEditable = false,
                    AccessLevel = 0
                };

                var cancelledStatus = new Status
                {
                    TenantId = _tenantId,
                    DisplayName = "Cancelled",
                    AfterStatusMessage = "Cancelled",
                    BeforeStatusActionText = "Cancel Request",
                    IsEditable = false,
                    AccessLevel = 0
                };

                var approvedStatus = new Status
                {
                    TenantId = _tenantId,
                    DisplayName = "Approved",
                    AfterStatusMessage = "Approved",
                    BeforeStatusActionText = "Approve",
                    IsEditable = false,
                    AccessLevel = 0
                };

                var reopenedStatus = new Status
                {
                    TenantId = _tenantId,
                    DisplayName = "Reopened",
                    AfterStatusMessage = "Reopened",
                    BeforeStatusActionText = "Reopen",
                    IsEditable = true,
                    AccessLevel = 0
                };

                var forUpdateStatus = new Status
                {
                    TenantId = _tenantId,
                    DisplayName = "For Update",
                    AfterStatusMessage = "Changed Status",
                    BeforeStatusActionText = "For Update",
                    IsEditable = true,
                    AccessLevel = 0
                };

                _context.Status.Add(newStatus);
                _context.Status.Add(forApprovalStatus);
                _context.Status.Add(forNextApprovalStatus);
                _context.Status.Add(declinedStatus);
                _context.Status.Add(cancelledStatus);
                _context.Status.Add(approvedStatus);
                _context.Status.Add(reopenedStatus);
                _context.Status.Add(forUpdateStatus);

                _context.SaveChanges();

                _context.WorkFlow.AddRange(new List<WorkFlow>
                {
                    new() { TenantId = _tenantId, StatusId = newStatus.Id, NextStatus = forApprovalStatus.Id },
                    new() { TenantId = _tenantId, StatusId = newStatus.Id, NextStatus = cancelledStatus.Id },
                    new() { TenantId = _tenantId, StatusId = forApprovalStatus.Id, NextStatus = forNextApprovalStatus.Id },
                    new() { TenantId = _tenantId, StatusId = forApprovalStatus.Id, NextStatus = declinedStatus.Id },
                    new() { TenantId = _tenantId, StatusId = forApprovalStatus.Id, NextStatus = forUpdateStatus.Id },
                    new() { TenantId = _tenantId, StatusId = forNextApprovalStatus.Id, NextStatus = declinedStatus.Id },
                    new() { TenantId = _tenantId, StatusId = forNextApprovalStatus.Id, NextStatus = approvedStatus.Id },
                    new() { TenantId = _tenantId, StatusId = forNextApprovalStatus.Id, NextStatus = forUpdateStatus.Id },
                    new() { TenantId = _tenantId, StatusId = declinedStatus.Id, NextStatus = null },
                    new() { TenantId = _tenantId, StatusId = declinedStatus.Id, NextStatus = reopenedStatus.Id },
                    new() { TenantId = _tenantId, StatusId = cancelledStatus.Id, NextStatus = null },
                    new() { TenantId = _tenantId, StatusId = cancelledStatus.Id, NextStatus = reopenedStatus.Id },
                    new() { TenantId = _tenantId, StatusId = approvedStatus.Id, NextStatus = null },
                    new() { TenantId = _tenantId, StatusId = approvedStatus.Id, NextStatus = cancelledStatus.Id },
                    new() { TenantId = _tenantId, StatusId = reopenedStatus.Id, NextStatus = forApprovalStatus.Id },
                    new() { TenantId = _tenantId, StatusId = reopenedStatus.Id, NextStatus = cancelledStatus.Id },
                    new() { TenantId = _tenantId, StatusId = forUpdateStatus.Id, NextStatus = forApprovalStatus.Id },
                    new() { TenantId = _tenantId, StatusId = forUpdateStatus.Id, NextStatus = cancelledStatus.Id }
                });

                _context.SaveChanges();
            }
        }
    }
}
