﻿using Abp.MultiTenancy;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Editions;
using MMB.ePay.EntityFrameworkCore;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{
    public class DefaultTenantBuilder
    {
        private readonly ePayDbContext _context;

        public DefaultTenantBuilder(ePayDbContext context)
        {
            _context = context;
        }

        public int Create()
        {
            return CreateDefaultTenant();
        }

        private int CreateDefaultTenant()
        {
            //Default tenant

            var defaultTenant = _context.Tenants.IgnoreQueryFilters().FirstOrDefault(t => t.TenancyName == AbpTenantBase.DefaultTenantName);
            if (defaultTenant == null)
            {
                defaultTenant = new MultiTenancy.Tenant(AbpTenantBase.DefaultTenantName, AbpTenantBase.DefaultTenantName);

                var defaultEdition = _context.Editions.IgnoreQueryFilters()
                    .FirstOrDefault(e => e.Name == EditionManager.DefaultEditionName);

                if (defaultEdition != null)
                {
                    defaultTenant.EditionId = defaultEdition.Id;
                }

                _context.Tenants.Add(defaultTenant);
                _context.SaveChanges();

                return defaultTenant.Id;
            }

            return 0;
        }
    }
}
