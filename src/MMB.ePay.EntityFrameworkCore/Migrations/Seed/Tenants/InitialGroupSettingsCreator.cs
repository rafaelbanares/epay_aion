﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.HR;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialGroupSettingsCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialGroupSettingsCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var groupSettings = new List<GroupSettings>
            {
                new() { SysCode = "MISC", DisplayName = "Miscellaneous", OrderNo = 1 }
            };

            foreach (var groupSetting in groupSettings)
            {
                if (!_context.GroupSettings.Any(e => e.TenantId == _tenantId && e.SysCode == groupSetting.SysCode))
                {
                    groupSetting.TenantId = _tenantId;
                    _context.GroupSettings.Add(groupSetting);
                }
            }

            _context.SaveChanges();
        }
    }
}
