﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.HR;
using System;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialCompaniesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialCompaniesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            if (!_context.Companies.Any(e => e.TenantId == _tenantId && e.DisplayName == "Default"))
            {
                _context.Companies.Add(
                    new Companies
                    {
                        TenantId = _tenantId,
                        DisplayName = "Default",
                        OrgUnitLevel1Name = "Group 1",
                        OrgUnitLevel2Name = "Group 2",
                        OrgUnitLevel3Name = "Group 3"
                    });

                _context.SaveChanges();
            }
        }
    }
}
