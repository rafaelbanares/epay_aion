﻿using Microsoft.EntityFrameworkCore;
using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialTKSettingsCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialTKSettingsCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var groupSettingId = _context.GroupSettings.IgnoreQueryFilters()
                .Where(e => e.TenantId == _tenantId && e.SysCode == "MISC")
                .Select(e => e.Id)
                .FirstOrDefault();

            var tkSettings = new List<TKSettings>
            {
                new()
                {
                    Key = "DATE_DISPLAY_FORMAT",
                    Value = "MM/dd/yyyy",
                    DataType = "string",
                    Caption = "Date Display Format",
                    DisplayOrder = 1
                },
                new()
                {
                    Key = "ENABLE_EMAIL_NOTIFICATION",
                    Value = "false",
                    DataType = "bool",
                    Caption = "Enable Email Notification",
                    DisplayOrder = 2
                },
                new()
                {
                    Key = "MAX_PAGE_SIZE",
                    Value = "10",
                    DataType = "int",
                    Caption = "Maximum Page Size",
                    DisplayOrder = 3
                },
                new()
                {
                    Key = "MULTI_SHIFT",
                    Value = "true",
                    DataType = "bool",
                    Caption = "Multipe Shift for the Week",
                    DisplayOrder = 4
                },
                new()
                {
                    Key = "RAWDATA_HAS_IO_INDICATOR",
                    Value = "true",
                    DataType = "bool",
                    Caption = "Raw Data has IO Indicator",
                    DisplayOrder = 5
                },
                new()
                {
                    Key = "ND1_START",
                    Value = "22:00",
                    DataType = "time",
                    Caption = "ND1 Start",
                    DisplayOrder = 6
                },
                new()
                {
                    Key = "ND1_END",
                    Value = "06:00",
                    DataType = "time",
                    Caption = "ND1 End",
                    DisplayOrder = 7
                },
                new()
                {
                    Key = "ND2_START",
                    Value = "06:00",
                    DataType = "time",
                    Caption = "ND2 Start",
                    DisplayOrder = 8
                },
                new()
                {
                    Key = "ND2_END",
                    Value = "06:00",
                    DataType = "time",
                    Caption = "ND2 End",
                    DisplayOrder = 9
                },
                new()
                {
                    Key = "GEOLOCATION",
                    Value = "DISABLED",
                    DataType = "string",
                    Caption = "Geolocation",
                    DisplayOrder = 10
                },
                new()
                {
                    Key = "HAS_PM_BREAK",
                    Value = "false",
                    DataType = "bool",
                    Caption = "Enable PM Breaks",
                    DisplayOrder = 11
                }
            };

            foreach(var tkSetting in tkSettings)
            {
                if (!_context.TKSettings.Any(e => e.TenantId == _tenantId && e.Key == tkSetting.Key))
                {
                    tkSetting.TenantId = _tenantId;
                    tkSetting.GroupSettingId = groupSettingId;

                    _context.TKSettings.Add(tkSetting);
                }
            }

            _context.SaveChanges();
        }
    }
}
