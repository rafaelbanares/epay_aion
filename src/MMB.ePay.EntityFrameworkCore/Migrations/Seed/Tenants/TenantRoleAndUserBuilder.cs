﻿using Abp;
using Abp.Authorization.Users;
using Abp.Notifications;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MMB.ePay.Authorization.Roles;
using MMB.ePay.Authorization.Users;
using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Notifications;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{
    public class TenantRoleAndUserBuilder
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public TenantRoleAndUserBuilder(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            CreateRolesAndUsers();
        }

        private void CreateRolesAndUsers()
        {
            var adminRole = new Role();

            //Admin role
            if (!_context.Roles.IgnoreQueryFilters().Any(e => e.TenantId == _tenantId && e.Name == StaticRoleNames.Tenants.Admin))
            {
                adminRole = new Role(_tenantId, StaticRoleNames.Tenants.Admin, StaticRoleNames.Tenants.Admin) { IsStatic = true };

                _context.Roles.Add(adminRole);
                _context.SaveChanges();
            }

            //User role
            if (!_context.Roles.IgnoreQueryFilters().Any(e => e.TenantId == _tenantId && e.Name == StaticRoleNames.Tenants.User))
            {
                var userRole = new Role(_tenantId, StaticRoleNames.Tenants.User, StaticRoleNames.Tenants.User) { IsStatic = true, IsDefault = true };

                _context.Roles.Add(userRole);
                _context.SaveChanges();
            }

            //default admin user
            if (!_context.Users.IgnoreQueryFilters().Any(e => e.TenantId == _tenantId && e.UserName == AbpUserBase.AdminUserName))
            {
                var adminUser = User.CreateTenantAdminUser(_tenantId, "admin@default.com");
                adminUser = User.CreateTenantAdminUser(_tenantId, "admin@default.com");
                adminUser.Password = new PasswordHasher<User>(new OptionsWrapper<PasswordHasherOptions>
                    (new PasswordHasherOptions())).HashPassword(adminUser, "123qwe");
                adminUser.IsEmailConfirmed = true;
                adminUser.ShouldChangePasswordOnNextLogin = false;
                adminUser.IsActive = true;

                _context.Users.Add(adminUser);
                _context.SaveChanges();

                //Assign Admin role to admin user
                _context.UserRoles.Add(new UserRole(_tenantId, adminUser.Id, adminRole.Id));

                //Notification subscription
                _context.NotificationSubscriptions.Add(
                    new NotificationSubscriptionInfo(SequentialGuidGenerator.Instance.Create(),
                    _tenantId,
                    adminUser.Id,
                    AppNotificationNames.NewUserRegistered));

                _context.SaveChanges();
            }
        }
    }
}
