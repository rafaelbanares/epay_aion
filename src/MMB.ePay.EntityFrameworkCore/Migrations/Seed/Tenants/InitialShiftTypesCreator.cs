﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialShiftTypesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialShiftTypesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var shiftTypes = new List<Tuple<ShiftTypes, ShiftTypeDetails>>
            {
                new Tuple<ShiftTypes, ShiftTypeDetails>(
                    new ShiftTypes
                    {
                        DisplayName = "09:00 AM - 06:00 PM",
                        Description = "09:00 AM - 06:00 PM",
                        Flexible1 = 0,
                        GracePeriod1 = 20,
                        MinimumOvertime = 30,
                    }, 
                    new ShiftTypeDetails
                    {
                        TimeIn = "09:00",
                        BreaktimeIn = "12:00",
                        BreaktimeOut = "13:00",
                        TimeOut = "18:00",
                    }
                ),
                new Tuple<ShiftTypes, ShiftTypeDetails>
                (
                    new ShiftTypes
                    {
                        DisplayName = "10:00 AM - 07:00 PM",
                        Description = "10:00 AM - 07:00 PM",
                        Flexible1 = 0,
                        GracePeriod1 = 0,
                        MinimumOvertime = 30,
                    },
                    new ShiftTypeDetails
                    {
                        TimeIn = "10:00",
                        BreaktimeIn = "13:00",
                        BreaktimeOut = "14:00",
                        TimeOut = "19:00",
                    }
                )
            };

            foreach(var shiftType in shiftTypes)
            {
                if (!_context.ShiftTypes.Any(e => e.TenantId == _tenantId && e.DisplayName == shiftType.Item1.DisplayName))
                {
                    shiftType.Item1.TenantId = _tenantId;
                    _context.ShiftTypes.Add(shiftType.Item1);
                    _context.SaveChanges();

                    shiftType.Item2.TenantId = _tenantId;
                    shiftType.Item2.Days = "1234567";
                    shiftType.Item2.ShiftTypeId = shiftType.Item1.Id;
                    shiftType.Item2.StartOnPreviousDay = false;

                    _context.ShiftTypeDetails.Add(shiftType.Item2);
                    _context.SaveChanges();
                }
            }
        }
    }
}
