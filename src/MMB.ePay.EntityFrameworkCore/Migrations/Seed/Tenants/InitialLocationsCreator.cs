﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialLocationsCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialLocationsCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var locations = new List<Locations>
            {
                new Locations
                {
                    Description = "Test",
                },
            };

            foreach (var location in locations)
            {
                if (!_context.Locations.Any(e => e.TenantId == _tenantId && e.Description == location.Description))
                {
                    location.TenantId = _tenantId;
                    _context.Locations.Add(location);
                }
            }

            _context.SaveChanges();
        }
    }
}
