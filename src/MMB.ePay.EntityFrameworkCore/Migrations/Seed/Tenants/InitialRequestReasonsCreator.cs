﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialRequestReasonsCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialRequestReasonsCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var reasons = new List<string> { "Activities", "Holidays", "OTHERS" };

            foreach(var reason in reasons)
            {
                if (!_context.AttendanceReasons.Any(e => e.TenantId == _tenantId && e.DisplayName == reason))
                {
                    _context.AttendanceReasons.Add(new AttendanceReasons
                    {
                        TenantId = _tenantId,
                        DisplayName = reason,
                        Description = reason
                    });
                }

                if (!_context.ExcuseReasons.Any(e => e.TenantId == _tenantId && e.DisplayName == reason))
                {
                    _context.ExcuseReasons.Add(new ExcuseReasons
                    {
                        TenantId = _tenantId,
                        DisplayName = reason,
                        Description = reason
                    });
                }

                if (!_context.LeaveReasons.Any(e => e.TenantId == _tenantId && e.DisplayName == reason))
                {
                    _context.LeaveReasons.Add(new LeaveReasons
                    {
                        TenantId = _tenantId,
                        DisplayName = reason,
                        Description = reason
                    });
                }

                if (!_context.OfficialBusinessReasons.Any(e => e.TenantId == _tenantId && e.DisplayName == reason))
                {
                    _context.OfficialBusinessReasons.Add(new OfficialBusinessReasons
                    {
                        TenantId = _tenantId,
                        DisplayName = reason,
                        Description = reason
                    });
                }

                if (!_context.OvertimeReasons.Any(e => e.TenantId == _tenantId && e.DisplayName == reason))
                {
                    _context.OvertimeReasons.Add(new OvertimeReasons
                    {
                        TenantId = _tenantId,
                        DisplayName = reason,
                        Description = reason
                    });
                }

                if (!_context.RestDayReasons.Any(e => e.TenantId == _tenantId && e.DisplayName == reason))
                {
                    _context.RestDayReasons.Add(new RestDayReasons
                    {
                        TenantId = _tenantId,
                        DisplayName = reason,
                        Description = reason
                    });
                }

                if (!_context.ShiftReasons.Any(e => e.TenantId == _tenantId && e.DisplayName == reason))
                {
                    _context.ShiftReasons.Add(new ShiftReasons
                    {
                        TenantId = _tenantId,
                        DisplayName = reason,
                        Description = reason
                    });
                }
            }

            _context.SaveChanges();
        }
    }
}
