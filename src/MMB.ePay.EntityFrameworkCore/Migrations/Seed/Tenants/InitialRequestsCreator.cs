﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{
    public class InitialRequestsCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialRequestsCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            int[] days = { 3, 4, 5, 6, 7, 10, 11, 12, 13 };
            var appyear = 2024;

            var employeeIds = _context.Employees
                .Where(e => e.TenantId == _tenantId)
                .Select(e => e.Id)
                .ToList();

            var statusIds = _context.Status
                .Where(e => e.TenantId == _tenantId)
                .Select(e => e.Id)
                .ToList();

			foreach (var employeeId in employeeIds)
            {
                if (!_context.AttendanceRequests.Any(e => e.TenantId == _tenantId && e.EmployeeId == employeeId))
                {
                    var requests = Enumerable.Range(0, statusIds.Count)
                        .Select(i => new AttendanceRequests
                        {
                            TenantId = _tenantId,
                            Date = new DateTime(appyear, 1, days[i]),
                            TimeIn = new DateTime(appyear, 1, days[i], 10, 0, 0),
                            TimeOut = new DateTime(appyear, 1, days[i], 19, 0, 0),
                            Remarks = "Test",
                            EmployeeId = employeeId,
                            AttendanceReasonId = 1,
                            EarlyLogin = false,
                            StatusId = statusIds[i]
                        }).ToList();

                    _context.AttendanceRequests.AddRange(requests);
                    _context.SaveChanges();
                }

                if (!_context.ExcuseRequests.Any(e => e.TenantId == _tenantId && e.EmployeeId == employeeId))
                {
                    var requests = Enumerable.Range(0, statusIds.Count)
                        .Select(i => new ExcuseRequests
                        {
                            TenantId = _tenantId,
                            ExcuseDate = new DateTime(appyear, 1, days[i]),
                            ExcuseStart = new DateTime(appyear, 1, days[i], 9, 0, 0),
                            ExcuseEnd = new DateTime(appyear, 1, days[i], 18, 0, 0),
                            Remarks = "Test",
                            EmployeeId = employeeId,
                            ExcuseReasonId = 1,
                            StatusId = statusIds[i]
                        }).ToList();

                    _context.ExcuseRequests.AddRange(requests);
                    _context.SaveChanges();
                }

                if (!_context.LeaveRequests.Any(e => e.TenantId == _tenantId && e.EmployeeId == employeeId))
                {
                    var requests = Enumerable.Range(0, statusIds.Count)
                        .Select(i => new LeaveRequests
                        {
                            TenantId = _tenantId,
                            Remarks = "Test",
                            EmployeeId = employeeId,
                            LeaveTypeId = 1,
                            LeaveReasonId = 1,
                            StatusId = statusIds[i]
                        }).ToList();

                    _context.LeaveRequests.AddRange(requests);
                    _context.SaveChanges();

                    var details = requests
                        .Select((request, i) => new LeaveRequestDetails
                        {
                            TenantId = _tenantId,
                            LeaveDate = new DateTime(appyear, 1, days[i]),
                            Days = 1,
                            FirstHalf = false,
                            LeaveRequestId = request.Id,
                            EmployeeId = employeeId
                        }).ToList();

                    _context.LeaveRequestDetails.AddRange(details);
                    _context.SaveChanges();
                }

                if (!_context.OfficialBusinessRequests.Any(e => e.TenantId == _tenantId && e.EmployeeId == employeeId))
                {
                    var requests = Enumerable.Range(0, statusIds.Count)
                        .Select(i => new OfficialBusinessRequests
                        {
                            TenantId = _tenantId,
                            Remarks = "Test",
                            EmployeeId = employeeId,
                            OfficialBusinessReasonId = 1,
                            StatusId = statusIds[i]
                        }).ToList();

                    _context.OfficialBusinessRequests.AddRange(requests);
                    _context.SaveChanges();

                    var details = requests
                        .Select((request, i) => new OfficialBusinessRequestDetails
                        {
                            TenantId = _tenantId,
                            OfficialBusinessDate = new DateTime(appyear, 1, days[i]),
                            OfficialBusinessRequestId = request.Id,
                            EmployeeId = employeeId
                        }).ToList();

                    _context.OfficialBusinessRequestDetails.AddRange(details);
                    _context.SaveChanges();
                }

                //if (!_context.OvertimeRequests.Any(e => e.TenantId == _tenantId && e.EmployeeId == employeeId))
                //{
                //    var requests = Enumerable.Range(0, statusIds.Count)
                //        .Select(i => new OvertimeRequests
                //        {
                //            TenantId = _tenantId,
                //            OvertimeDate = new DateTime(appyear, 1, days[i]),
                //            OvertimeStart = new DateTime(appyear, 1, days[i], 18, 0, 0),
                //            OvertimeEnd = new DateTime(appyear, 1, days[i], 21, 0, 0),
                //            Remarks = "Test",
                //            EmployeeId = employeeId,
                //            OvertimeReasonId = 1,
                //            StatusId = statusIds[i]
                //        }).ToList();

                //    _context.OvertimeRequests.AddRange(requests);
                //    _context.SaveChanges();
                //}

                if (!_context.RestDayRequests.Any(e => e.TenantId == _tenantId && e.EmployeeId == employeeId))
                {
                    var requests = Enumerable.Range(0, statusIds.Count)
                        .Select(i => new RestDayRequests
                        {
                            TenantId = _tenantId,
                            RestDayCode = "1234567",
                            RestDayStart = new DateTime(appyear, 1, days[i]),
                            RestDayEnd = new DateTime(appyear, 1, days[i]),
                            Remarks = "Test",
                            EmployeeId = employeeId,
                            RestDayReasonId = 1,
                            StatusId = statusIds[i]
                        }).ToList();

                    _context.RestDayRequests.AddRange(requests);
                    _context.SaveChanges();
                }

                if (!_context.ShiftRequests.Any(e => e.TenantId == _tenantId && e.EmployeeId == employeeId))
                {
                    var requests = Enumerable.Range(0, statusIds.Count)
                        .Select(i => new ShiftRequests
                        {
                            TenantId = _tenantId,
                            ShiftStart = new DateTime(appyear, 1, days[i]),
                            ShiftEnd = new DateTime(appyear, 1, days[i]),
                            Remarks = "Test",
                            EmployeeId = employeeId,
                            ShiftTypeId = 2,
                            ShiftReasonId = 1,
                            StatusId = statusIds[i]
                        }).ToList();

                    _context.ShiftRequests.AddRange(requests);
                    _context.SaveChanges();
                }
            }
        }
    }
}
