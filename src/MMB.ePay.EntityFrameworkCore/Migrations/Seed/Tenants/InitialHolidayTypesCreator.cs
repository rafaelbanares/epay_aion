﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialHolidayTypesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialHolidayTypesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var holidayTypes = new List<HolidayTypes>
            {
                new() { DisplayName = "Legal", HolidayCode = "L", HalfDayEnabled = false },
                new() { DisplayName = "Special", HolidayCode = "S", HalfDayEnabled = false },
                new() { DisplayName = "Legal & Special", HolidayCode = "LS", HalfDayEnabled = false },
                new() { DisplayName = "Double Legal", HolidayCode = "D", HalfDayEnabled = false },
                new() { DisplayName = "Company Holiday", HolidayCode = "C", HalfDayEnabled = true },
                new() { DisplayName = "Regional Holiday", HolidayCode = "R", HalfDayEnabled = false }
            };

            foreach(var holidayType in holidayTypes)
            {
                if (!_context.HolidayTypes.Any(e => e.TenantId == _tenantId && e.DisplayName == holidayType.DisplayName))
                {
                    holidayType.TenantId = _tenantId;
                    holidayType.Description = holidayType.DisplayName;

                    _context.HolidayTypes.Add(holidayType);
                }
            }

            _context.SaveChanges();
        }
    }
}
