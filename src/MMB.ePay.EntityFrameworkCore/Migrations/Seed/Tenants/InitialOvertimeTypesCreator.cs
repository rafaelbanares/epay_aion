﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{
    public class InitialOvertimeTypesCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialOvertimeTypesCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var overtimeTypes = new List<Tuple<string, string>>
            {
                new ("Regular OT", "REG"),
                new ("Rest Day OT", "RST"),
                new ("Special Holiday", "S"),
                new ("Legal Holiday", "L"),
                new ("Legal Rest Day", "L+RST"),
                new ("Special Rest Day", "S+RST"),
                new ("Legal Special", "LS"),
                new ("Double Legal Holiday", "D"),
                new ("Company Holiday", "C"),
                new ("Regional Holiday", "R")
            };

            foreach (var type in overtimeTypes)
            {
                for (short i = 1; i <= 4; i++)
                {

                    var shortName = type.Item1;
                    var otCode = type.Item2;

                    switch (i)
                    {
                        case 2:
                            shortName += " > 8";
                            break;
                        case 3:
                            shortName += " ND1";
                            break;
                        case 4:
                            shortName += " ND2";
                            break;
                        default:
                            break;
                    }

                    //Default
                    if (!_context.OvertimeTypes.Any(e => e.TenantId == _tenantId && e.OTCode == otCode && e.SubType == i))
                    {
                        _context.OvertimeTypes.Add(
                            new OvertimeTypes
                            {
                                TenantId = _tenantId,
                                ShortName = shortName,
                                Description = shortName,
                                OTCode = otCode,
                                SubType = i
                            });

                        _context.SaveChanges();
                    }
                }
            }
        }
    }
}
