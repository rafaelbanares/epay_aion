﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{

    public class InitialAttendancePeriodsCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialAttendancePeriodsCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            var frequencyId = _context.Frequencies
                .Where(e => e.TenantId == _tenantId && e.DisplayName == "Semi-Monthly")
                .Select(e => e.Id)
                .FirstOrDefault();

            _context.SaveChanges();

            var appyear = 2024;
            var attendancePeriods = new List<AttendancePeriods>
            {
                new AttendancePeriods 
                { 
                    TenantId = _tenantId,
                    StartDate = new DateTime(appyear - 1, 12, 16),
                    EndDate = new DateTime(appyear - 1, 12, 31),
                    AppYear = ((short)(appyear - 1)),
                    AppMonth = 12, 
                    Posted = true, 
                    FrequencyId = frequencyId
                }
            };

            var firstPeriod = Enumerable.Range(1, 12)
                .Select(e =>
                    new AttendancePeriods
                    {
                        TenantId = _tenantId,
                        StartDate = new DateTime(appyear, e, 1),
                        EndDate = new DateTime(appyear, e, 15),
                        AppYear = 2024,
                        AppMonth = (short)e,
                        Posted = (e != 11 && e != 12),
                        FrequencyId = frequencyId
                    }
                ).ToList();

            var secondPeriod = Enumerable.Range(1, 12)
                .Select(e =>
                    new AttendancePeriods
                    {
                        TenantId = _tenantId,
                        StartDate = new DateTime(appyear, e, 16),
                        EndDate = new DateTime(appyear, e, DateTime.DaysInMonth(2024, e)),
                        AppYear = 2024,
                        AppMonth = (short)e,
                        Posted = (e != 11 && e != 12),
                        FrequencyId = frequencyId
                    }
                ).ToList();

            attendancePeriods.AddRange(firstPeriod);
            attendancePeriods.AddRange(secondPeriod);

            foreach (var attendancePeriod in attendancePeriods.OrderBy(e => e.StartDate))
            {
                if (!_context.AttendancePeriods
                    .Any(e => e.TenantId == _tenantId
                        && e.StartDate == attendancePeriod.StartDate
                        && e.FrequencyId == attendancePeriod.FrequencyId))
                {
                    _context.AttendancePeriods.Add(attendancePeriod);
                }
            }

            _context.SaveChanges();
        }
    }
}
