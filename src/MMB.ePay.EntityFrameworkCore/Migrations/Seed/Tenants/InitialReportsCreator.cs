﻿using MMB.ePay.EntityFrameworkCore;
using MMB.ePay.Timekeeping;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Migrations.Seed.Tenants
{
    public class InitialReportsCreator
    {
        private readonly ePayDbContext _context;
        private readonly int _tenantId;

        public InitialReportsCreator(ePayDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }
        public void Create()
        {
            var reports = new List<Reports>
            {
                new()
                {
                    DisplayName = "Attendance Transaction",
                    ControllerName = "AttendanceTransaction",
                    IsLandscape = false,
                    Output = "P"
                },
                new()
                {
                    DisplayName = "Excuse Transaction",
                    ControllerName = "ExcuseTransaction",
                    IsLandscape = false,
                    Output = "P"
                },
                new()
                {
                    DisplayName = "Leave Transaction",
                    ControllerName = "LeaveTransaction",
                    IsLandscape = false,
                    Output = "P"
                },
                new()
                {
                    DisplayName = "Official Business Transaction",
                    ControllerName = "OfficialBusinessTransaction",
                    IsLandscape = false,
                    Output = "P"
                },
                new()
                {
                    DisplayName = "Overtime Transaction",
                    ControllerName = "OvertimeTransaction",
                    IsLandscape = false,
                    Output = "P" ,
                    StoredProcedure = "usp_OvertimeTransaction"
                },
                new()
                {
                    DisplayName = "Rest Day Transaction",
                    ControllerName = "RestDayTransaction",
                    IsLandscape = false,
                    Output = "P"
                },
                new()
                {
                    DisplayName = "Shift Transaction",
                    ControllerName = "ShiftTransaction",
                    IsLandscape = false,
                    Output = "P"
                },
                new()
                {
                    DisplayName = "Attendance Summary",
                    ControllerName = "AttendanceSummary",
                    IsLandscape = true,
                    Output = "P",
                    StoredProcedure = "usp_AttendanceSummary"
                },
                new()
                {
                    DisplayName = "Attendance Summary (Vertical Format)",
                    ControllerName = "AttendanceSummary2",
                    IsLandscape = false,
                    Output = "P",
                    StoredProcedure = "usp_AttendanceSummary2"
                },
                new()
                {
                    DisplayName = "Leave Summary",
                    ControllerName = "LeaveSummary",
                    IsLandscape = false,
                    Output = "P"
                },
                new()
                {
                    DisplayName = "Overtime Summary",
                    ControllerName = "OvertimeSummary",
                    IsLandscape = false,
                    Output = "P",
                    StoredProcedure = "usp_OvertimeSummary"
                },
                new()
                {
                    DisplayName = "Daily Attendance",
                    ControllerName = "DailyAttendance",
                    IsLandscape = true,
                    Output = "P"
                },
                new()
                {
                    DisplayName = "Leave Balance",
                    ControllerName = "LeaveBalance",
                    IsLandscape = false,
                    Output = "P"
                },
                new()
                {
                    DisplayName = "Work From Home",
                    ControllerName = "WorkFromHome",
                    IsLandscape = false,
                    Output = "P"
                },
                new()
                {
                    DisplayName = "Attendance Summary (Payroll)",
                    ControllerName = "AttendanceSummaryPayroll",
                    IsLandscape = true,
                    Output = "E",
                    StoredProcedure = "usp_AttendanceSummaryPayroll"
                },
                new()
                {
                    DisplayName = "Leave Summary (Payroll)",
                    ControllerName = "LeaveSummaryPayroll",
                    IsLandscape = true,
                    Output = "E"
                },
                new()
                {
                    DisplayName = "Overtime Summary (Payroll)",
                    ControllerName = "OvertimeSummaryPayroll",
                    IsLandscape = true,
                    Output = "E"
                },
            };

            foreach (var report in reports)
            {
                if (!_context.Reports.Any(e => e.TenantId == _tenantId && e.ControllerName == report.ControllerName))
                {
                    report.TenantId = _tenantId;
                    report.HasEmployeeFilter = true;
                    report.HasPeriodFilter = true;
                    report.Enabled = true;
                    report.Format = "Letter";
                    report.ExportFileName = report.DisplayName.Replace(" ", "_");

                    _context.Reports.Add(report);
                }

                _context.SaveChanges();
            }

        }
    }
}
