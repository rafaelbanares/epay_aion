﻿using Abp.IdentityServer4vNext;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MMB.ePay.Authorization.Delegation;
using MMB.ePay.Authorization.Roles;
using MMB.ePay.Authorization.Users;
using MMB.ePay.Chat;
using MMB.ePay.Editions;
using MMB.ePay.Friendships;
using MMB.ePay.HR;
using MMB.ePay.MultiTenancy;
using MMB.ePay.MultiTenancy.Accounting;
using MMB.ePay.MultiTenancy.Payments;
using MMB.ePay.Storage;
using MMB.ePay.Timekeeping;

namespace MMB.ePay.EntityFrameworkCore
{
    public class ePayDbContext : AbpZeroDbContext<Tenant, Role, User, ePayDbContext>, IAbpPersistedGrantDbContext
    {
        public virtual DbSet<ApproverOrders> ApproverOrders { get; set; }

        public virtual DbSet<AttendanceApprovals> AttendanceApprovals { get; set; }

        public virtual DbSet<AttendancePeriods> AttendancePeriods { get; set; }

        public virtual DbSet<AttendanceReasons> AttendanceReasons { get; set; }

        public virtual DbSet<AttendanceRequests> AttendanceRequests { get; set; }

        public virtual DbSet<Attendances> Attendances { get; set; }

        public virtual DbSet<AttendanceUploads> AttendanceUploads { get; set; }

        public virtual DbSet<Companies> Companies { get; set; }
        
        public virtual DbSet<EmailQueue> EmailQueue { get; set; }
        
        public virtual DbSet<EmployeeApprovers> EmployeeApprovers { get; set; }
        
        public virtual DbSet<Employees> Employees { get; set; }
        
        public virtual DbSet<EmployeeTimeInfo> EmployeeTimeInfo { get; set; }
        
        public virtual DbSet<EmploymentTypes> EmploymentTypes { get; set; }
        
        public virtual DbSet<ExcuseApprovals> ExcuseApprovals { get; set; }
        
        public virtual DbSet<ExcuseReasons> ExcuseReasons { get; set; }
        
        public virtual DbSet<ExcuseRequests> ExcuseRequests { get; set; }
        
        public virtual DbSet<Frequencies> Frequencies { get; set; }
        
        public virtual DbSet<GroupSettings> GroupSettings { get; set; }
        
        public virtual DbSet<Holidays> Holidays { get; set; }
        
        public virtual DbSet<HolidayTypes> HolidayTypes { get; set; }
        
        public virtual DbSet<LeaveApprovals> LeaveApprovals { get; set; }
        
        public virtual DbSet<LeaveEarningDetails> LeaveEarningDetails { get; set; }
        
        public virtual DbSet<LeaveEarnings> LeaveEarnings { get; set; }
        
        public virtual DbSet<LeaveReasons> LeaveReasons { get; set; }
        
        public virtual DbSet<LeaveRequestDetails> LeaveRequestDetails { get; set; }
        
        public virtual DbSet<LeaveRequests> LeaveRequests { get; set; }
        
        public virtual DbSet<LeaveTypes> LeaveTypes { get; set; }
        
        public virtual DbSet<Locations> Locations { get; set; }
        
        public virtual DbSet<OfficialBusinessApprovals> OfficialBusinessApprovals { get; set; }
        
        public virtual DbSet<OfficialBusinessReasons> OfficialBusinessReasons { get; set; }
        
        public virtual DbSet<OfficialBusinessRequestDetails> OfficialBusinessRequestDetails { get; set; }
        
        public virtual DbSet<OfficialBusinessRequests> OfficialBusinessRequests { get; set; }
        
        public virtual DbSet<OvertimeApprovals> OvertimeApprovals { get; set; }
        
        public virtual DbSet<OvertimeReasons> OvertimeReasons { get; set; }
        
        public virtual DbSet<OvertimeRequests> OvertimeRequests { get; set; }
        
        public virtual DbSet<OvertimeTypes> OvertimeTypes { get; set; }
        
        public virtual DbSet<PostedAttendances> PostedAttendances { get; set; }
        
        public virtual DbSet<PostedLeaves> PostedLeaves { get; set; }
        
        public virtual DbSet<PostedOvertimes> PostedOvertimes { get; set; }
        
        public virtual DbSet<PostedTimesheets> PostedTimesheets { get; set; }
        
        public virtual DbSet<ProcessedLeaves> ProcessedLeaves { get; set; }
        
        public virtual DbSet<ProcessedOvertimes> ProcessedOvertimes { get; set; }
        
        public virtual DbSet<ProcessedTimesheets> ProcessedTimesheets { get; set; }
        
        public virtual DbSet<ProcSessionBatches> ProcSessionBatches { get; set; }
        
        public virtual DbSet<ProcSessions> ProcSessions { get; set; }
        
        public virtual DbSet<RawTimeLogs> RawTimeLogs { get; set; }
        
        public virtual DbSet<RawTimeLogStaging> RawTimeLogStaging { get; set; }
        
        public virtual DbSet<Reports> Reports { get; set; }
        
        public virtual DbSet<RestDayApprovals> RestDayApprovals { get; set; }
        
        public virtual DbSet<RestDayReasons> RestDayReasons { get; set; }
        
        public virtual DbSet<RestDayRequests> RestDayRequests { get; set; }
        
        public virtual DbSet<ShiftApprovals> ShiftApprovals { get; set; }
        
        public virtual DbSet<ShiftReasons> ShiftReasons { get; set; }
        
        public virtual DbSet<ShiftRequests> ShiftRequests { get; set; }
        
        public virtual DbSet<ShiftTypeDetails> ShiftTypeDetails { get; set; }
        
        public virtual DbSet<ShiftTypes> ShiftTypes { get; set; }
        
        public virtual DbSet<Status> Status { get; set; }
        
        public virtual DbSet<TKSettings> TKSettings { get; set; }
        
        public virtual DbSet<WorkFlow> WorkFlow { get; set; }

        /* Define an IDbSet for each entity of the application */

        public virtual DbSet<BinaryObject> BinaryObjects { get; set; }

        public virtual DbSet<Friendship> Friendships { get; set; }

        public virtual DbSet<ChatMessage> ChatMessages { get; set; }

        public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }

        public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }

        public virtual DbSet<Invoice> Invoices { get; set; }

        public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }

        public virtual DbSet<SubscriptionPaymentExtensionData> SubscriptionPaymentExtensionDatas { get; set; }

        public virtual DbSet<UserDelegation> UserDelegations { get; set; }

        public virtual DbSet<User> User { get; set; }

        public ePayDbContext(DbContextOptions<ePayDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<Hash>(x => 
            //{ 
            //    x.ToTable("Hash", "HangFire"); 
            //    x.HasKey(d => d.Key);
            //});

            //modelBuilder.Entity<Job>(x => 
            //{ 
            //    x.ToTable("Job", "HangFire");
            //});

            //modelBuilder.Entity<JobParameter>(x => 
            //{ 
            //    x.ToTable("JobParameter", "HangFire");

            //    x.HasKey(d => new { d.JobId, d.Name });

            //    x.HasOne(d => d.Job)
            //        .WithMany(p => p.JobParameter)
            //        .HasForeignKey(d => d.JobId)
            //        .OnDelete(DeleteBehavior.ClientSetNull);
            //});

            modelBuilder.Entity<ApproverOrders>(a =>
            {
                a.ToTable("ApproverOrders", "tk");
                a.HasIndex(e => new { e.TenantId });

                a.Property(e => e.DisplayName).IsUnicode(false);
            });

            modelBuilder.Entity<AttendanceApprovals>(a =>
            {
                a.ToTable("AttendanceApprovals", "tk");

                a.HasIndex(e => new { e.TenantId });

                a.HasOne(d => d.AttendanceRequest)
                    .WithMany(p => p.AttendanceApprovals)
                    .HasForeignKey(d => d.AttendanceRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                a.HasOne(d => d.Status)
                    .WithMany(p => p.AttendanceApprovals)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<AttendancePeriods>(a =>
            {
                a.ToTable("AttendancePeriods", "tk");

                a.Property(e => e.StartDate).HasColumnType("date");
                a.Property(e => e.EndDate).HasColumnType("date");

                a.HasIndex(e => new { e.TenantId });

                a.HasOne(d => d.Frequencies)
                    .WithMany(p => p.AttendancePeriods)
                    .HasForeignKey(d => d.FrequencyId);
            });

            modelBuilder.Entity<AttendanceReasons>(a =>
            {
                a.ToTable("AttendanceReasons", "tk");
                a.HasIndex(e => new { e.TenantId });

                a.Property(e => e.DisplayName).IsUnicode(false);
                a.Property(e => e.Description).IsUnicode(false);
            });

            modelBuilder.Entity<AttendanceRequests>(a =>
            {
                a.ToTable("AttendanceRequests", "tk");
                a.HasIndex(e => new { e.TenantId });

                a.HasIndex(e => new { e.EmployeeId, e.Date }).IsUnique();

                a.Property(e => e.Date).HasColumnType("date");
                a.Property(e => e.TimeIn).HasColumnType("smalldatetime");
                a.Property(e => e.BreaktimeIn).HasColumnType("smalldatetime");
                a.Property(e => e.BreaktimeOut).HasColumnType("smalldatetime");
                a.Property(e => e.PMBreaktimeIn).HasColumnType("smalldatetime");
                a.Property(e => e.PMBreaktimeOut).HasColumnType("smalldatetime");
                a.Property(e => e.TimeOut).HasColumnType("smalldatetime");

                a.HasOne(d => d.Employees)
                    .WithMany(p => p.AttendanceRequests)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                a.HasOne(d => d.AttendanceReason)
                    .WithMany(p => p.AttendanceRequests)
                    .HasForeignKey(d => d.AttendanceReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                a.HasOne(d => d.Status)
                    .WithMany(p => p.AttendanceRequests)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Attendances>(a =>
            {
                a.ToTable("Attendances", "tk");
                a.HasIndex(e => new { e.TenantId });

                a.HasIndex(e => new { e.EmployeeId, e.Date }).IsUnique();

                a.Property(e => e.Date).HasColumnType("date");
                a.Property(e => e.TimeIn).HasColumnType("smalldatetime");
                a.Property(e => e.BreaktimeIn).HasColumnType("smalldatetime");
                a.Property(e => e.BreaktimeOut).HasColumnType("smalldatetime");
                a.Property(e => e.PMBreaktimeIn).HasColumnType("smalldatetime");
                a.Property(e => e.PMBreaktimeOut).HasColumnType("smalldatetime");
                a.Property(e => e.TimeOut).HasColumnType("smalldatetime");
                a.Property(e => e.RestDayCode).IsUnicode(false);
                //a.Property(e => e.DayType).IsUnicode(false);

                a.HasOne(d => d.Employees)
                    .WithMany(p => p.Attendances)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                a.HasOne(d => d.ShiftTypes)
                    .WithMany(p => p.Attendances)
                    .HasForeignKey(d => d.ShiftTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<AttendanceUploads>(a =>
            {
                a.ToTable("AttendanceUploads", "tk");
                a.HasIndex(e => new { e.TenantId });

                a.HasIndex(e => new { e.EmployeeId, e.Date }).IsUnique();

                a.Property(e => e.Date).HasColumnType("date");
                a.Property(e => e.TimeIn).IsUnicode(false);
                a.Property(e => e.LunchBreaktimeIn).IsUnicode(false);
                a.Property(e => e.LunchBreaktimeOut).IsUnicode(false);
                a.Property(e => e.PMBreaktimeIn).IsUnicode(false);
                a.Property(e => e.PMBreaktimeOut).IsUnicode(false);
                a.Property(e => e.TimeOut).IsUnicode(false);

                a.HasOne(d => d.Employees)
                    .WithMany(p => p.AttendanceUploads)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Companies>(c =>
            {
                c.ToTable("Companies", "hr");
                c.HasIndex(e => new { e.TenantId });

                c.Property(e => e.DisplayName).IsUnicode(false);
                c.Property(e => e.Address1).IsUnicode(false);
                c.Property(e => e.Address2).IsUnicode(false);

                c.HasIndex(e => new { e.TenantId, e.DisplayName }).IsUnique();

            });

            modelBuilder.Entity<EmailQueue>(e =>
            {
                e.ToTable("EmailQueue", "tk");
                e.HasIndex(e => new { e.TenantId });

                e.Property(e => e.StartDate).HasColumnType("date");
                e.Property(e => e.EndDate).HasColumnType("date");
                e.Property(e => e.Request).IsUnicode(false);
                e.Property(e => e.FullName).IsUnicode(false);
                e.Property(e => e.ApprovalStatus).IsUnicode(false);
                e.Property(e => e.Status).IsUnicode(false);
                e.Property(e => e.StatusError).IsUnicode(false);

                e.HasIndex(e => new { e.Status });

                e.HasIndex(e => new { e.TenantId, e.CreationTime }).IsUnique();
            });

            modelBuilder.Entity<EmployeeApprovers>(x =>
            {
                x.ToTable("EmployeeApprovers", "tk");
                x.HasIndex(e => new { e.TenantId });

                x.HasOne(d => d.Approvers)
                    .WithMany(p => p.EmployeeApproversApprover)
                    .HasForeignKey(d => d.ApproverId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                x.HasOne(d => d.Employees)
                    .WithMany(p => p.EmployeeApproversEmployee)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                x.HasOne(d => d.ApproverOrders)
                    .WithMany(p => p.EmployeeApprovers)
                    .HasForeignKey(d => d.ApproverOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<Employees>(e =>
            {
                e.ToTable("Employees", "hr");
                e.HasIndex(e => new { e.TenantId });

                e.Property(e => e.DateOfBirth).HasColumnType("date");
                e.Property(e => e.DateEmployed).HasColumnType("date");
                e.Property(e => e.DateTerminated).HasColumnType("date");
                e.Property(e => e.DateRegularized).HasColumnType("date");
                e.Property(e => e.EmployeeCode).IsUnicode(false);
                e.Property(e => e.LastName).IsUnicode(false);
                e.Property(e => e.FirstName).IsUnicode(false);
                e.Property(e => e.MiddleName).IsUnicode(false);
                e.Property(e => e.Position).IsUnicode(false);
                e.Property(e => e.Gender).IsUnicode(false);
                e.Property(e => e.EmployeeStatus).IsUnicode(false);
                e.Property(e => e.Rank).IsUnicode(false);

                e.HasOne(d => d.EmploymentTypes)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.EmploymentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                e.HasOne(d => d.EmployeeTimeInfo)
                    .WithOne(p => p.Employees)
                    .HasForeignKey<Employees>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<EmployeeTimeInfo>(x =>
            {
                x.ToTable("EmployeeTimeInfo", "tk");
                x.HasIndex(e => new { e.TenantId });

                x.Property(e => e.RestDay).IsUnicode(false);
                x.Property(e => e.SwipeCode).IsUnicode(false);

                x.HasOne(d => d.Employees)
                    .WithOne(p => p.EmployeeTimeInfo)
                    .HasForeignKey<EmployeeTimeInfo>(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                x.HasOne(d => d.Locations)
                   .WithMany(p => p.EmployeeTimeInfo)
                   .HasForeignKey(d => d.LocationId)
                   .OnDelete(DeleteBehavior.ClientSetNull);

                x.HasOne(d => d.ShiftTypes)
                    .WithMany(p => p.EmployeeTimeInfo)
                    .HasForeignKey(d => d.ShiftTypeId);

                x.HasOne(d => d.Frequencies)
                    .WithMany(p => p.EmployeeTimeInfo)
                    .HasForeignKey(d => d.FrequencyId);

                x.HasIndex(e => new { e.TenantId, e.SwipeCode }).IsUnique();
            });

            modelBuilder.Entity<EmploymentTypes>(x =>
            {
                x.ToTable("EmploymentTypes", "hr");
                x.HasIndex(e => new { e.TenantId });

                x.HasIndex(e => new { e.TenantId, e.SysCode }).IsUnique();

                x.Property(e => e.Description).IsUnicode(false);
            });

            modelBuilder.Entity<ExcuseApprovals>(x =>
            {
                x.ToTable("ExcuseApprovals", "tk");
                x.HasIndex(e => new { e.TenantId });

                x.HasOne(d => d.ExcuseRequests)
                    .WithMany(p => p.ExcuseApprovals)
                    .HasForeignKey(d => d.ExcuseRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                x.HasOne(d => d.Status)
                    .WithMany(p => p.ExcuseApprovals)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ExcuseReasons>(x =>
            {
                x.ToTable("ExcuseReasons", "tk");
                x.HasIndex(e => new { e.TenantId });

                x.Property(e => e.DisplayName).IsUnicode(false);
                x.Property(e => e.Description).IsUnicode(false);
            });

            modelBuilder.Entity<ExcuseRequests>(x =>
            {
                x.ToTable("ExcuseRequests", "tk");
                x.HasIndex(e => new { e.TenantId });

                x.HasIndex(e => new { e.EmployeeId, e.ExcuseDate }).IsUnique();

                x.Property(e => e.ExcuseDate).HasColumnType("date");
                x.Property(e => e.ExcuseStart).HasColumnType("smalldatetime");
                x.Property(e => e.ExcuseEnd).HasColumnType("smalldatetime");

                x.HasOne(d => d.Employees)
                    .WithMany(p => p.ExcuseRequests)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                x.HasOne(d => d.ExcuseReasons)
                    .WithMany(p => p.ExcuseRequests)
                    .HasForeignKey(d => d.ExcuseReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                x.HasOne(d => d.Status)
                    .WithMany(p => p.ExcuseRequests)
                    .HasForeignKey(d => d.StatusId);
            });

            modelBuilder.Entity<Frequencies>(f =>
            {
                f.ToTable("Frequencies", "tk");
                f.HasIndex(e => new { e.TenantId });

                f.Property(e => e.DisplayName).IsUnicode(false);
                f.Property(e => e.Description).IsUnicode(false);
            });

            modelBuilder.Entity<GroupSettings>(g =>
            {
                g.ToTable("GroupSettings", "hr");
                g.HasIndex(e => new { e.TenantId });

                g.HasIndex(e => new { e.TenantId, e.SysCode }).IsUnique();

                g.Property(e => e.DisplayName).IsUnicode(false);
            });

            modelBuilder.Entity<Holidays>(h =>
            {
                h.ToTable("Holidays", "tk");
                h.HasIndex(e => new { e.TenantId });

                h.HasOne(d => d.HolidayTypes)
                .WithMany(p => p.Holidays)
                .HasForeignKey(d => d.HolidayTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull);

                h.HasOne(d => d.Locations)
                .WithMany(p => p.Holidays)
                .HasForeignKey(d => d.LocationId)
                .OnDelete(DeleteBehavior.ClientSetNull);

                h.Property(e => e.Date).HasColumnType("date");
                h.Property(e => e.DisplayName).IsUnicode(false);
                //h.Property(e => e.HolidayType).IsUnicode(false);

                h.HasIndex(e => new { e.TenantId, e.Date }).IsUnique();
                h.HasIndex(e => new { e.TenantId, e.Year, e.DisplayName }).IsUnique();
            });

            modelBuilder.Entity<HolidayTypes>(l =>
            {
                l.ToTable("HolidayTypes", "tk");
                l.HasIndex(e => new { e.TenantId });

                l.Property(e => e.DisplayName).IsUnicode(false);
                l.Property(e => e.Description).IsUnicode(false);

                l.HasIndex(e => new { e.TenantId, e.DisplayName }).IsUnique();
                l.HasIndex(e => new { e.TenantId, e.HolidayCode }).IsUnique();
            });

            modelBuilder.Entity<LeaveApprovals>(l =>
            {
                l.ToTable("LeaveApprovals", "tk");
                l.HasIndex(e => new { e.TenantId });

                l.HasOne(d => d.LeaveRequests)
                    .WithMany(p => p.LeaveApprovals)
                    .HasForeignKey(d => d.LeaveRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                l.HasOne(d => d.Status)
                    .WithMany(p => p.LeaveApprovals)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<LeaveEarningDetails>(l =>
            {
                l.ToTable("LeaveEarningDetails", "tk");
                l.HasIndex(e => new { e.TenantId });

                l.HasOne(d => d.Employees)
                    .WithMany(p => p.LeaveEarningDetails)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                l.HasOne(d => d.LeaveEarnings)
                    .WithMany(p => p.LeaveEarningDetails)
                    .HasForeignKey(d => d.LeaveEarningId);

                l.HasOne(d => d.LeaveTypes)
                    .WithMany(p => p.LeaveEarningDetails)
                    .HasForeignKey(d => d.LeaveTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<LeaveEarnings>(l =>
            {
                l.ToTable("LeaveEarnings", "tk");
                l.HasIndex(e => new { e.TenantId });

                l.Property(e => e.Date).HasColumnType("date");
                l.Property(e => e.ValidFrom).HasColumnType("date");
                l.Property(e => e.ValidTo).HasColumnType("date");
            });

            modelBuilder.Entity<LeaveReasons>(l =>
            {
                l.ToTable("LeaveReasons", "tk");
                l.HasIndex(e => new { e.TenantId });

                l.Property(e => e.DisplayName).IsUnicode(false);
                l.Property(e => e.Description).IsUnicode(false);
            });

            modelBuilder.Entity<LeaveRequestDetails>(l =>
            {
                l.ToTable("LeaveRequestDetails", "tk");
                l.HasIndex(e => new { e.TenantId });

                l.HasIndex(e => new { e.EmployeeId, e.LeaveDate }).IsUnique();

                l.Property(e => e.LeaveDate).HasColumnType("date");

                l.HasOne(d => d.Employees)
                    .WithMany(p => p.LeaveRequestDetails)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                l.HasOne(d => d.LeaveRequests)
                    .WithMany(p => p.LeaveRequestDetails)
                    .HasForeignKey(d => d.LeaveRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

            });

            modelBuilder.Entity<LeaveRequests>(l =>
            {
                l.ToTable("LeaveRequests", "tk");
                l.HasIndex(e => new { e.TenantId });

                l.HasOne(d => d.Employees)
                    .WithMany(p => p.LeaveRequests)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                l.HasOne(d => d.LeaveReasons)
                    .WithMany(p => p.LeaveRequests)
                    .HasForeignKey(d => d.LeaveReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                l.HasOne(d => d.LeaveTypes)
                    .WithMany(p => p.LeaveRequests)
                    .HasForeignKey(d => d.LeaveTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                l.HasOne(d => d.Status)
                    .WithMany(p => p.LeaveRequests)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<LeaveTypes>(l =>
            {
                l.ToTable("LeaveTypes", "tk");
                l.HasIndex(e => new { e.TenantId });

                l.Property(e => e.DisplayName).IsUnicode(false);
                l.Property(e => e.Description).IsUnicode(false);

                l.HasIndex(e => new { e.TenantId, e.DisplayName }).IsUnique();
            });

            modelBuilder.Entity<Locations>(l =>
            {
                l.ToTable("Locations", "tk");
                l.HasIndex(e => new { e.TenantId });

                l.Property(e => e.Description).IsUnicode(false);

                l.HasIndex(e => new { e.TenantId, e.Description }).IsUnique();
            });

            modelBuilder.Entity<OfficialBusinessApprovals>(o =>
            {
                o.ToTable("OfficialBusinessApprovals", "tk");
                o.HasIndex(e => new { e.TenantId });

                o.HasOne(d => d.OfficialBusinessRequests)
                    .WithMany(p => p.OfficialBusinessApprovals)
                    .HasForeignKey(d => d.OfficialBusinessRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                o.HasOne(d => d.Status)
                    .WithMany(p => p.OfficialBusinessApprovals)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<OfficialBusinessReasons>(o =>
            {
                o.ToTable("OfficialBusinessReasons", "tk");
                o.HasIndex(e => new { e.TenantId });

                o.Property(e => e.DisplayName).IsUnicode(false);
                o.Property(e => e.Description).IsUnicode(false);
            });

            modelBuilder.Entity<OfficialBusinessRequestDetails>(o =>
            {
                o.ToTable("OfficialBusinessRequestDetails", "tk");
                o.HasIndex(e => new { e.TenantId });

                o.HasIndex(e => new { e.EmployeeId, e.OfficialBusinessDate }).IsUnique();

                o.Property(e => e.OfficialBusinessDate).HasColumnType("smalldatetime");

                o.HasOne(d => d.OfficialBusinessRequests)
                    .WithMany(p => p.OfficialBusinessRequestDetails)
                    .HasForeignKey(d => d.OfficialBusinessRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<OfficialBusinessRequests>(o =>
            {
                o.ToTable("OfficialBusinessRequests", "tk");
                o.HasIndex(e => new { e.TenantId });

                o.HasOne(d => d.Employees)
                    .WithMany(p => p.OfficialBusinessRequests)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                o.HasOne(d => d.OfficialBusinessReasons)
                    .WithMany(p => p.OfficialBusinessRequests)
                    .HasForeignKey(d => d.OfficialBusinessReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                o.HasOne(d => d.Status)
                    .WithMany(p => p.OfficialBusinessRequests)
                    .HasForeignKey(d => d.StatusId);
            });

            modelBuilder.Entity<OvertimeApprovals>(o =>
            {
                o.ToTable("OvertimeApprovals", "tk");
                o.HasIndex(e => new { e.TenantId });

                o.HasOne(d => d.OvertimeRequests)
                    .WithMany(p => p.OvertimeApprovals)
                    .HasForeignKey(d => d.OvertimeRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                o.HasOne(d => d.Status)
                    .WithMany(p => p.OvertimeApprovals)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<OvertimeReasons>(o =>
            {
                o.ToTable("OvertimeReasons", "tk");
                o.HasIndex(e => new { e.TenantId });

                o.Property(e => e.DisplayName).IsUnicode(false);
                o.Property(e => e.Description).IsUnicode(false);
            });

            modelBuilder.Entity<OvertimeRequests>(o =>
            {
                o.ToTable("OvertimeRequests", "tk");
                o.HasIndex(e => new { e.TenantId });

                o.HasIndex(e => new { e.EmployeeId, e.OvertimeStart, e.OvertimeEnd }).IsUnique();

                o.Property(e => e.OvertimeDate).HasColumnType("date");
                o.Property(e => e.OvertimeStart).HasColumnType("smalldatetime");
                o.Property(e => e.OvertimeEnd).HasColumnType("smalldatetime");

                o.HasOne(d => d.Employees)
                    .WithMany(p => p.OvertimeRequests)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                o.HasOne(d => d.OvertimeReasons)
                    .WithMany(p => p.OvertimeRequests)
                    .HasForeignKey(d => d.OvertimeReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                o.HasOne(d => d.Status)
                    .WithMany(p => p.OvertimeRequests)
                    .HasForeignKey(d => d.StatusId);
            });

            modelBuilder.Entity<OvertimeTypes>(o =>
            {
                o.ToTable("OvertimeTypes", "tk");
                o.HasIndex(o => new { o.TenantId });

                o.Property(e => e.ShortName).IsUnicode(false);
                o.Property(e => e.Description).IsUnicode(false);
                o.Property(e => e.OTCode).IsUnicode(false);

                o.HasIndex(o => new { o.TenantId, o.OTCode, o.SubType }).IsUnique();
            });

            modelBuilder.Entity<PostedAttendances>(a =>
            {
                a.ToTable("PostedAttendances", "tk");
                a.HasIndex(e => new { e.TenantId });

                a.Property(e => e.Date).HasColumnType("date");
                a.Property(e => e.TimeIn).HasColumnType("smalldatetime");
                a.Property(e => e.BreaktimeIn).HasColumnType("smalldatetime");
                a.Property(e => e.BreaktimeOut).HasColumnType("smalldatetime");
                a.Property(e => e.PMBreaktimeIn).HasColumnType("smalldatetime");
                a.Property(e => e.PMBreaktimeOut).HasColumnType("smalldatetime");
                a.Property(e => e.TimeOut).HasColumnType("smalldatetime");
                a.Property(e => e.RestDayCode).IsUnicode(false);
                a.Property(e => e.Remarks).IsUnicode(false);

                a.HasOne(d => d.Employees)
                    .WithMany(p => p.PostedAttendances)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                a.HasOne(d => d.ShiftTypes)
                    .WithMany(p => p.PostedAttendances)
                    .HasForeignKey(d => d.ShiftTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<PostedLeaves>(x =>
            {
                x.ToTable("PostedLeaves", "tk");
                x.HasIndex(e => new { e.TenantId });

                x.HasIndex(e => new { e.TenantId, e.EmployeeId, e.LeaveTypeId, e.Date }).IsUnique();

                x.Property(e => e.Date).HasColumnType("date");

                x.HasOne(d => d.Employees)
                    .WithMany(p => p.PostedLeaves)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                x.HasOne(d => d.LeaveTypes)
                    .WithMany(p => p.PostedLeaves)
                    .HasForeignKey(d => d.LeaveTypeId);
            });

            modelBuilder.Entity<PostedOvertimes>(p =>
            {
                p.ToTable("PostedOvertimes", "tk");
                p.HasIndex(e => new { e.TenantId });

                p.HasIndex(e => new { e.TenantId, e.EmployeeId, e.OvertimeTypeId, e.Date }).IsUnique();

                p.Property(e => e.Date).HasColumnType("date");

                p.HasOne(d => d.Employees)
                    .WithMany(p => p.PostedOvertimes)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                p.HasOne(d => d.OvertimeTypes)
                    .WithMany(p => p.PostedOvertimes)
                    .HasForeignKey(d => d.OvertimeTypeId);
            });

            modelBuilder.Entity<PostedTimesheets>(p =>
            {
                p.ToTable("PostedTimesheets", "tk");
                p.HasIndex(e => new { e.TenantId });

                p.HasIndex(e => new { e.TenantId, e.EmployeeId, e.Date }).IsUnique();

                p.Property(e => e.Date).HasColumnType("date");
                p.Property(e => e.Absence).HasColumnType("decimal(7, 2)");
                p.Property(e => e.PaidHoliday).HasColumnType("decimal(7, 2)");
                p.Property(e => e.ObMins).HasComment("decimal(7, 2)");

                p.HasOne(d => d.Employees)
                    .WithMany(p => p.PostedTimesheets)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ProcessedLeaves>(x =>
            {
                x.ToTable("ProcessedLeaves", "tk");
                x.HasIndex(e => new { e.TenantId });

                x.HasIndex(e => new { e.TenantId, e.EmployeeId, e.LeaveTypeId, e.Date }).IsUnique();

                x.Property(e => e.Date).HasColumnType("date");

                x.HasOne(d => d.Employees)
                    .WithMany(p => p.ProcessedLeaves)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                x.HasOne(d => d.LeaveTypes)
                    .WithMany(p => p.ProcessedLeaves)
                    .HasForeignKey(d => d.LeaveTypeId);
            });

            modelBuilder.Entity<ProcessedOvertimes>(p =>
            {
                p.ToTable("ProcessedOvertimes", "tk");
                p.HasIndex(e => new { e.TenantId });

                p.HasIndex(e => new { e.TenantId, e.EmployeeId, e.OvertimeTypeId, e.Date }).IsUnique();

                p.Property(e => e.Date).HasColumnType("date");

                p.HasOne(d => d.Employees)
                    .WithMany(p => p.ProcessedOvertimes)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                p.HasOne(d => d.OvertimeTypes)
                    .WithMany(p => p.ProcessedOvertimes)
                    .HasForeignKey(d => d.OvertimeTypeId);
            });

            modelBuilder.Entity<ProcessedTimesheets>(p =>
            {
                p.ToTable("ProcessedTimesheets", "tk");
                p.HasIndex(e => new { e.TenantId });

                p.HasIndex(e => new { e.TenantId, e.EmployeeId, e.Date }).IsUnique();

                p.Property(e => e.Date).HasColumnType("date");
                p.Property(e => e.Absence).HasColumnType("decimal(7, 2)");
                p.Property(e => e.PaidHoliday).HasColumnType("decimal(7, 2)");
                p.Property(e => e.ObMins).HasComment("decimal(7, 2)");

                p.HasOne(d => d.Employees)
                    .WithMany(p => p.ProcessedTimesheets)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ProcSessionBatches>(x =>
            {
                x.ToTable("ProcSessionBatches", "tk");

                x.HasIndex(e => new { e.PsId, e.EmployeeId }).IsUnique();

                x.HasOne(d => d.ProcSessions)
                    .WithMany(p => p.ProcSessionBatches)
                    .HasForeignKey(d => d.PsId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                x.HasOne(d => d.Employees)
                    .WithMany(p => p.ProcSessionBatches)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ProcSessions>(x =>
            {
                x.ToTable("ProcSessions", "tk");

                x.Property(e => e.ProcessStart).HasColumnType("smalldatetime");
                x.Property(e => e.ProcessEnd).HasColumnType("smalldatetime");

                x.HasOne(d => d.AttendancePeriods)
                    .WithMany(p => p.ProcSessions)
                    .HasForeignKey(d => d.AttendancePeriodId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<RawTimeLogs>(r =>
            {
                r.ToTable("RawTimeLogs", "tk");
                r.HasIndex(e => new { e.TenantId });

                r.Property(e => e.SwipeCode).IsUnicode(false);
                r.Property(e => e.InOut).IsUnicode(false);
                r.Property(e => e.Device).IsUnicode(false);
                r.Property(e => e.Geolocation).HasColumnType("geometry");

                //r.Property(e => e.CreationTime).HasColumnType("smalldatetime");

                r.HasIndex(e => new { e.SwipeCode, e.LogTime, e.InOut, e.TenantId }).IsUnique();
            });

            modelBuilder.Entity<RawTimeLogStaging>(r =>
            {
                r.ToTable("RawTimeLogStaging", "tk");
                r.HasIndex(e => new { e.TenantId });

                r.Property(e => e.SwipeCode).IsUnicode(false);
                r.Property(e => e.InOut).IsUnicode(false);
                r.Property(e => e.Device).IsUnicode(false);
            });

            modelBuilder.Entity<Reports>(r =>
            {
                r.ToTable("Reports", "tk");
                r.HasIndex(e => new { e.TenantId });

                r.Property(e => e.DisplayName).IsUnicode(false);
                r.Property(e => e.StoredProcedure).IsUnicode(false);
                r.Property(e => e.ControllerName).IsUnicode(false);
                r.Property(e => e.Format).IsUnicode(false);
                r.Property(e => e.Output).IsUnicode(false);
                r.Property(e => e.ExportFileName).IsUnicode(false);

                r.HasIndex(e => new { e.TenantId, e.ControllerName }).IsUnique();
            });

            modelBuilder.Entity<RestDayApprovals>(r =>
            {
                r.ToTable("RestDayApprovals", "tk");
                r.HasIndex(e => new { e.TenantId });

                r.HasOne(d => d.RestDayRequests)
                    .WithMany(p => p.RestDayApprovals)
                    .HasForeignKey(d => d.RestDayRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                r.HasOne(d => d.Status)
                    .WithMany(p => p.RestDayApprovals)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<RestDayReasons>(r =>
            {
                r.ToTable("RestDayReasons", "tk");
                r.HasIndex(e => new { e.TenantId });

                r.Property(e => e.DisplayName).IsUnicode(false);
                r.Property(e => e.Description).IsUnicode(false);
            });

            modelBuilder.Entity<RestDayRequests>(r =>
            {
                r.ToTable("RestDayRequests", "tk");
                r.HasIndex(e => new { e.TenantId });

                r.HasIndex(e => new { e.EmployeeId, e.RestDayEnd, e.RestDayStart }).IsUnique();

                r.Property(e => e.RestDayStart).HasColumnType("date");
                r.Property(e => e.RestDayEnd).HasColumnType("date");
                r.Property(e => e.RestDayCode).IsUnicode(false);

                r.HasOne(d => d.Employees)
                    .WithMany(p => p.RestDayRequests)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                r.HasOne(d => d.RestDayReasons)
                    .WithMany(p => p.RestDayRequests)
                    .HasForeignKey(d => d.RestDayReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                r.HasOne(d => d.Status)
                    .WithMany(p => p.RestDayRequests)
                    .HasForeignKey(d => d.StatusId);
            });

            modelBuilder.Entity<ShiftApprovals>(s =>
            {
                s.ToTable("ShiftApprovals", "tk");
                s.HasIndex(e => new { e.TenantId });

                s.HasOne(d => d.ShiftRequests)
                    .WithMany(p => p.ShiftApprovals)
                    .HasForeignKey(d => d.ShiftRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                s.HasOne(d => d.Status)
                    .WithMany(p => p.ShiftApprovals)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ShiftReasons>(s =>
            {
                s.ToTable("ShiftReasons", "tk");
                s.HasIndex(e => new { e.TenantId });

                s.Property(e => e.DisplayName).IsUnicode(false);
                s.Property(e => e.Description).IsUnicode(false);
            });

            modelBuilder.Entity<ShiftRequests>(s =>
            {
                s.ToTable("ShiftRequests", "tk");
                s.HasIndex(e => new { e.TenantId });

                s.HasIndex(e => new { e.EmployeeId, e.ShiftStart, e.ShiftEnd }).IsUnique();

                s.Property(e => e.ShiftStart).HasColumnType("date");
                s.Property(e => e.ShiftEnd).HasColumnType("date");

                s.HasOne(d => d.Employees)
                    .WithMany(p => p.ShiftRequests)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                s.HasOne(d => d.ShiftReasons)
                    .WithMany(p => p.ShiftRequests)
                    .HasForeignKey(d => d.ShiftReasonId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                s.HasOne(d => d.ShiftTypes)
                    .WithMany(p => p.ShiftRequests)
                    .HasForeignKey(d => d.ShiftTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                s.HasOne(d => d.Status)
                    .WithMany(p => p.ShiftRequests)
                    .HasForeignKey(d => d.StatusId);
            });

            modelBuilder.Entity<ShiftTypeDetails>(s =>
            {
                s.ToTable("ShiftTypeDetails", "tk");
                s.HasIndex(e => new { e.TenantId });

                s.Property(e => e.Days).IsUnicode(false);
                s.Property(e => e.TimeIn).IsUnicode(false);
                s.Property(e => e.BreaktimeIn).IsUnicode(false);
                s.Property(e => e.BreaktimeOut).IsUnicode(false);
                s.Property(e => e.TimeOut).IsUnicode(false);

                s.HasOne(d => d.ShiftTypes)
                    .WithMany(p => p.ShiftTypeDetails)
                    .HasForeignKey(d => d.ShiftTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<ShiftTypes>(s =>
            {
                s.ToTable("ShiftTypes", "tk");
                s.HasIndex(e => new { e.TenantId });

                s.Property(e => e.DisplayName).IsUnicode(false);
                s.Property(e => e.Description).IsUnicode(false);

                s.HasIndex(e => new { e.TenantId, e.DisplayName }).IsUnique();
            });

            modelBuilder.Entity<Status>(s =>
            {
                s.ToTable("Status", "tk");
                s.HasIndex(e => new { e.TenantId });

                s.Property(e => e.DisplayName).IsUnicode(false);
                s.Property(e => e.AfterStatusMessage).IsUnicode(false);
                s.Property(e => e.BeforeStatusActionText).IsUnicode(false);
            });

            modelBuilder.Entity<TKSettings>(t =>
            {
                t.ToTable("TKSettings", "tk");
                t.HasIndex(e => new { e.TenantId });

                t.Property(e => e.Key).IsUnicode(false);
                t.Property(e => e.Value).IsUnicode(false);
                t.Property(e => e.DataType).IsUnicode(false);
                t.Property(e => e.Caption).IsUnicode(false);

                t.HasOne(d => d.GroupSettings)
                    .WithMany(p => p.TKSettings)
                    .HasForeignKey(d => d.GroupSettingId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                t.HasIndex(e => new { e.TenantId, e.Key }).IsUnique();
            });

            modelBuilder.Entity<WorkFlow>(w =>
            {
                w.ToTable("WorkFlow", "tk");
                w.HasIndex(e => new { e.TenantId });

                w.HasOne(d => d.NextStatusNavigation)
                    .WithMany(p => p.WorkFlowNextStatusNavigation)
                    .HasForeignKey(d => d.NextStatus);

                w.HasOne(d => d.Status)
                    .WithMany(p => p.WorkFlowStatus)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            //ABP
            modelBuilder.Entity<BinaryObject>(b =>
                        {
                            b.HasIndex(e => new { e.TenantId });
                        });

            modelBuilder.Entity<ChatMessage>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
            });

            modelBuilder.Entity<Friendship>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId });
                b.HasIndex(e => new { e.TenantId, e.FriendUserId });
                b.HasIndex(e => new { e.FriendTenantId, e.UserId });
                b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
            });

            modelBuilder.Entity<Tenant>(b =>
            {
                b.HasIndex(e => new { e.SubscriptionEndDateUtc });
                b.HasIndex(e => new { e.CreationTime });
            });

            modelBuilder.Entity<SubscriptionPayment>(b =>
            {
                b.HasIndex(e => new { e.Status, e.CreationTime });
                b.HasIndex(e => new { PaymentId = e.ExternalPaymentId, e.Gateway });
            });

            modelBuilder.Entity<SubscriptionPaymentExtensionData>(b =>
            {
                b.HasQueryFilter(m => !m.IsDeleted)
                    .HasIndex(e => new { e.SubscriptionPaymentId, e.Key, e.IsDeleted })
                    .IsUnique();
            });

            modelBuilder.Entity<UserDelegation>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.SourceUserId });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId });
            });

            modelBuilder.Entity<User>(b =>
            {
                b.HasOne(d => d.Employees)
                    .WithOne(p => p.User)
                    .HasForeignKey<User>(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.ConfigurePersistedGrantEntity();
        }
    }
}