﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMB.ePay;
using Microsoft.Data.SqlClient;
using System.Data;
using System.Data.Common;
using Abp.Data;
using Abp.EntityFrameworkCore;

namespace MMB.ePay.EntityFrameworkCore.Repositories
{
    public class ProcessRepository : ePayRepositoryBase<ProcessBatch, int>, IProcessRepository
    {
        private readonly IActiveTransactionProvider _transactionProvider;

        public ProcessRepository(IDbContextProvider<ePayDbContext> dbContextProvider, IActiveTransactionProvider transactionProvider)
            : base(dbContextProvider)
        {
            _transactionProvider = transactionProvider;
        }

        
        public async Task InitializeProcessSession(Guid processSessionId, int tenantId)
        {
            await InitializeProcessSession(processSessionId, tenantId, -1);
        }
        

        public async Task InitializeProcessSession(Guid processSessionId, int tenantId, int employeeId)
        {
            EnsureConnectionOpen();

            SqlParameter[] sqlParams = {
                new("@AppSessionId", SqlDbType.UniqueIdentifier) { Value = processSessionId, Direction = ParameterDirection.Input },
                new("@TenantId", SqlDbType.Int) { Value = tenantId, Direction = ParameterDirection.Input },
                new("@EmployeeId", SqlDbType.Int) { Value = employeeId, Direction = ParameterDirection.Input }
            };

            using var command = CreateCommand("[tk].usp_InitializeProcSession", CommandType.StoredProcedure, sqlParams);
            await command.ExecuteNonQueryAsync();
        }

        public async Task<Guid?> GetProcessBatch(Guid appSessionId)
        {
            EnsureConnectionOpen();

            SqlParameter[] sqlParams = {
                new("@AppSessionId", SqlDbType.UniqueIdentifier) { Value = appSessionId, Direction = ParameterDirection.Input },
                new("@BatchId", SqlDbType.UniqueIdentifier) { Value = null, Direction = ParameterDirection.Output }
            };

            using var command = CreateCommand("[pay].usp_GetProcessBatch", CommandType.StoredProcedure, sqlParams);
            await command.ExecuteNonQueryAsync();

            if (command.Parameters["@BatchId"].Value == DBNull.Value)
                return null;
            else
                return (Guid)command.Parameters["@BatchId"].Value;

        }



        private DbCommand CreateCommand(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            var command = GetConnection().CreateCommand();

            command.CommandText = commandText;
            command.CommandType = commandType;
            command.Transaction = GetActiveTransaction();

            foreach (var parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }

            return command;
        }

        private void EnsureConnectionOpen()
        {
            var connection = GetConnection();

            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }
        }

        private DbTransaction GetActiveTransaction()
        {
            return (DbTransaction)_transactionProvider.GetActiveTransaction(new ActiveTransactionProviderArgs
            {
                {"ContextType", typeof(ePayDbContext) },
                {"MultiTenancySide", MultiTenancySide }
            });
        }
    }
}
