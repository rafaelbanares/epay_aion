using Microsoft.EntityFrameworkCore;
using System.Data.Common;

namespace MMB.ePay.EntityFrameworkCore
{
    public static class ePayDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<ePayDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString, options => options.UseNetTopologySuite());
        }

        public static void Configure(DbContextOptionsBuilder<ePayDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection, options => options.UseNetTopologySuite());
        }
    }
}