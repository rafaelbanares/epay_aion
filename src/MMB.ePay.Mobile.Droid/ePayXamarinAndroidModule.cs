﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace MMB.ePay
{
    [DependsOn(typeof(ePayXamarinSharedModule))]
    public class ePayXamarinAndroidModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ePayXamarinAndroidModule).GetAssembly());
        }
    }
}