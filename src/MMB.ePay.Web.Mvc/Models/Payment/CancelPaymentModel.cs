﻿using MMB.ePay.MultiTenancy.Payments;

namespace MMB.ePay.Web.Models.Payment
{
    public class CancelPaymentModel
    {
        public string PaymentId { get; set; }

        public SubscriptionPaymentGatewayType Gateway { get; set; }
    }
}