﻿namespace MMB.ePay.Web.Models.Account
{
    public class LoginViewModel : LoginModel
    {
        public bool RememberMe { get; set; }
    }

    public class EntriesModalViewModel
    {
        public string UserNameOrEmailAddress { get; set; }
    }
}