﻿namespace MMB.ePay.Web.Models.Account
{
    public class LoginFormViewModel
    {
        public string TenantName { get; set; }

        public string SuccessMessage { get; set; }

        public string UserNameOrEmailAddress { get; set; }

        public bool IsSelfRegistrationEnabled { get; set; }

        public bool IsTenantSelfRegistrationEnabled { get; set; }
    }
}