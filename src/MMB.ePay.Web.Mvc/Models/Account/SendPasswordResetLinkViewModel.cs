using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Web.Models.Account
{
    public class SendPasswordResetLinkViewModel
    {
        [Required]
        public string EmailAddress { get; set; }
    }
}