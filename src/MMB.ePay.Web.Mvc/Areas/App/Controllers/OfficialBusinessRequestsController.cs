﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.OfficialBusinessApprovals;
using MMB.ePay.Web.Areas.App.Models.OfficialBusinessRequests;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_OfficialBusinessRequests)]
    public class OfficialBusinessRequestsController : ePayControllerBase
    {
        private readonly IOfficialBusinessRequestsAppService _officialBusinessRequestsAppService;
        private readonly IOfficialBusinessReasonsAppService _officialBusinessReasonsAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public OfficialBusinessRequestsController(IOfficialBusinessRequestsAppService officialBusinessRequestsAppService,
            IOfficialBusinessReasonsAppService officialBusinessReasonsAppService,
            IApprovalsAppService approvalsAppService)
        {
            _officialBusinessRequestsAppService = officialBusinessRequestsAppService;
            _officialBusinessReasonsAppService = officialBusinessReasonsAppService;
            _approvalsAppService = approvalsAppService;
        }

        public async Task<ActionResult> Index() =>
            View(await _approvalsAppService.GetRequestsFilters());

        [AbpMvcAuthorize(AppPermissions.Pages_OfficialBusinessRequests_Create, AppPermissions.Pages_OfficialBusinessRequests_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var obRequests = new CreateOrEditOfficialBusinessRequestsDto { WholeDay = true };

            if (id.HasValue)
            {
                var output = await _officialBusinessRequestsAppService.GetOfficialBusinessRequestsForEdit(id.Value);
                obRequests = output.OfficialBusinessRequests;
            }

            var viewModel = new CreateOrEditOfficialBusinessRequestsModalViewModel()
            {
                OfficialBusinessRequests = obRequests,
                OfficialBusinessReasonList = await _officialBusinessReasonsAppService.GetOfficialBusinessReasonList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewOfficialBusinessRequestsModal(int id)
        {
            var viewDto = await _officialBusinessRequestsAppService.GetOfficialBusinessRequestsForView(id);
            var model = new OfficialBusinessRequestsViewModel()
            {
                OfficialBusinessDetails = viewDto.OfficialBusinessDetails
            };

            return PartialView("_ViewOfficialBusinessRequestsModal", model);
        }

        public PartialViewResult RemarksOfficialBusinessRequestsModal(OfficialBusinessRemarksViewModel model) =>
            PartialView("_RemarksOfficialBusinessRequestsModal", model);
    }
}