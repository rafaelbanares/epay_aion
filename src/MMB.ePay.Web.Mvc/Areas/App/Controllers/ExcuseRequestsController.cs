﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.ExcuseApprovals;
using MMB.ePay.Web.Areas.App.Models.ExcuseRequests;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_ExcuseRequests)]
    public class ExcuseRequestsController : ePayControllerBase
    {
        private readonly IExcuseRequestsAppService _excuseRequestsAppService;
        private readonly IExcuseReasonsAppService _excuseReasonsAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public ExcuseRequestsController(IExcuseRequestsAppService excuseRequestsAppService,
            IExcuseReasonsAppService excuseReasonsAppService,
            IApprovalsAppService approvalsAppService)
        {
            _excuseRequestsAppService = excuseRequestsAppService;
            _excuseReasonsAppService = excuseReasonsAppService;
            _approvalsAppService = approvalsAppService;
        }

        public async Task<ActionResult> Index() =>
            View(await _approvalsAppService.GetRequestsFilters());

        [AbpMvcAuthorize(AppPermissions.Pages_ExcuseRequests_Create, AppPermissions.Pages_ExcuseRequests_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var excuseRequests = new CreateOrEditExcuseRequestsDto();

            if (id.HasValue)
            {
                var output = await _excuseRequestsAppService.GetExcuseRequestsForEdit(id.Value);
                excuseRequests = output.ExcuseRequests;
            }

            var viewModel = new CreateOrEditExcuseRequestsModalViewModel()
            {
                ExcuseRequests = excuseRequests,
                ExcuseReasonList = await _excuseReasonsAppService.GetExcuseReasonList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewExcuseRequestsModal(int id)
        {
            var viewDto = await _excuseRequestsAppService.GetExcuseRequestsForView(id);
            var model = new ExcuseRequestsViewModel()
            {
                ExcuseDetails = viewDto.ExcuseDetails
            };

            return PartialView("_ViewExcuseRequestsModal", model);
        }

        public PartialViewResult RemarksExcuseRequestsModal(ExcuseRemarksViewModel model) =>
            PartialView("_RemarksExcuseRequestsModal", model);
    }
}