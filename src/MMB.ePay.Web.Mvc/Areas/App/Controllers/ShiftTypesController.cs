﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.ShiftTypeDetails;
using MMB.ePay.Web.Areas.App.Models.ShiftTypes;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_ShiftTypes)]
    public class ShiftTypesController : ePayControllerBase
    {
        private readonly IShiftTypesAppService _shiftTypesAppService;

        public ShiftTypesController(IShiftTypesAppService shiftTypesAppService) =>
            _shiftTypesAppService = shiftTypesAppService;

        public ActionResult Index() =>
            View(new ShiftTypesViewModel());

        public async Task<IActionResult> Details(int id)
        {
            var shiftTypes = await _shiftTypesAppService.GetShiftTypesForView(id);
            var model = new ShiftTypeDetailsViewModel
            {
                ShiftTypes = shiftTypes.ShiftTypes
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_ShiftTypes_Edit)]
        public async Task<PartialViewResult> EditModal(int? id)
        {
            var output = await _shiftTypesAppService.GetShiftTypesForEdit(new EntityDto { Id = (int)id });

            var viewModel = new EditShiftTypesModalViewModel()
            {
                ShiftTypes = new EditShiftTypesDto
                {
                    DisplayName = output.ShiftTypes.DisplayName,
                    Description = output.ShiftTypes.Description,
                    Flexible1 = output.ShiftTypes.Flexible1,
                    GracePeriod1 = output.ShiftTypes.GracePeriod1,
                    MinimumOvertime = output.ShiftTypes.MinimumOvertime,
                    Id = id
                }
            };

            return PartialView("_EditModal", viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_ShiftTypes_Create, AppPermissions.Pages_ShiftTypes_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            CreateOrEditShiftTypesDto shiftTypes;

            if (id.HasValue)
            {
                var output = await _shiftTypesAppService.GetShiftTypesForEdit(new EntityDto { Id = (int)id });
                shiftTypes = output.ShiftTypes;
            }
            else
            {
                shiftTypes = new CreateOrEditShiftTypesDto
                {
                    Monday = true,
                    Tuesday = true,
                    Wednesday = true,
                    Thursday = true,
                    Friday = true,
                    Saturday = true,
                    Sunday = true
                };
            }

            var viewModel = new CreateOrEditShiftTypesModalViewModel()
            {
                ShiftTypes = shiftTypes
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewShiftTypesModal(int id)
        {
            var viewDto = await _shiftTypesAppService.GetShiftTypesForView(id);
            var model = new ShiftTypesViewModel()
            {
                ShiftTypes = viewDto.ShiftTypes
            };

            return PartialView("_ViewShiftTypesModal", model);
        }
    }
}