﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.EmployeeApprovers;
using MMB.ePay.Web.Controllers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_EmployeeApprovers)]
    public class EmployeeApproversController : ePayControllerBase
    {
        private readonly IEmployeeApproversAppService _employeeApproversAppService;
        private readonly IApproverOrdersAppService _approverOrdersAppService;

        public EmployeeApproversController(IEmployeeApproversAppService employeeApproversAppService,
            IApproverOrdersAppService approverOrdersAppService)
        {
            _employeeApproversAppService = employeeApproversAppService;
            _approverOrdersAppService = approverOrdersAppService;
        }

        public async Task<ActionResult> Index() =>
            View(new EmployeeApproversViewModel
            {
                ApproverNameList = await _approverOrdersAppService.GetApproverNameList()
            });

        [AbpMvcAuthorize(AppPermissions.Pages_EmployeeApprovers_Create, AppPermissions.Pages_EmployeeApprovers_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var viewModel = new CreateOrEditEmployeeApproversModalViewModel()
            {
                ApproverOrderList = await _approverOrdersAppService.GetApproverOrderList()
            };

            var approverCount = viewModel.ApproverOrderList.Count;

            if (id.HasValue)
            {
                var output = await _employeeApproversAppService.GetEmployeeApproversForEdit(new EntityDto { Id = (int)id });
                var idCount = output.EmployeeApprovers.ApproverIdList.Count;

                if (idCount < approverCount)
                {
                    for (var i = 0; i < approverCount - idCount; i++)
                    {
                        output.EmployeeApprovers.ApproverIdList.Add(null);
                        output.EmployeeApprovers.ApproverNameList.Add(string.Empty);
                    }
                }
                
                viewModel.EmployeeApprovers = output.EmployeeApprovers;
            }
            else
            {
                var approverList = new List<int?>();

                for(var i = 0; i <= viewModel.ApproverOrderList.Count; i++)
                {
                    approverList.Add(null);
                }

                var output = new GetEmployeeApproversForEditOutput
                {
                    EmployeeApprovers = new CreateOrEditEmployeeApproversDto
                    {
                        ApproverIdList = approverList
                    }
                };

                viewModel.EmployeeApprovers = output.EmployeeApprovers;
            }

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewEmployeeApproversModal(int id)
        {
            var viewDto = await _employeeApproversAppService.GetEmployeeApproversForView(id);
            var model = new EmployeeApproversViewModel()
            {
                EmployeeApprovers = viewDto.EmployeeApprovers
            };

            return PartialView("_ViewEmployeeApproversModal", model);
        }
    }
}