﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.LeaveTypes;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_LeaveTypes)]
    public class LeaveTypesController : ePayControllerBase
    {
        private readonly ILeaveTypesAppService _leaveTypesAppService;

        public LeaveTypesController(ILeaveTypesAppService leaveTypesAppService) =>
            _leaveTypesAppService = leaveTypesAppService;

        public ActionResult Index() =>
            View(new LeaveTypesViewModel());

        [AbpMvcAuthorize(AppPermissions.Pages_LeaveTypes_Create, AppPermissions.Pages_LeaveTypes_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var leaveTypes = new CreateOrEditLeaveTypesDto();

            if (id.HasValue)
            {
                var output = await _leaveTypesAppService.GetLeaveTypesForEdit(id.Value);
                leaveTypes = output.LeaveTypes;
            }

            var viewModel = new CreateOrEditLeaveTypesModalViewModel()
            {
                LeaveTypes = leaveTypes
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewLeaveTypesModal(int id)
        {
            var viewDto = await _leaveTypesAppService.GetLeaveTypesForView(id);
            var model = new LeaveTypesViewModel()
            {
                LeaveTypes = viewDto.LeaveTypes
            };

            return PartialView("_ViewLeaveTypesModal", model);
        }
    }
}