﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.ExcuseApprovals;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_ExcuseApprovals)]
    public class ExcuseApprovalsController : ePayControllerBase
    {
        private readonly IExcuseApprovalsAppService _excuseApprovalsAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public ExcuseApprovalsController(IExcuseApprovalsAppService excuseApprovalsAppService,
            IApprovalsAppService approvalsAppService)
        {
            _excuseApprovalsAppService = excuseApprovalsAppService;
            _approvalsAppService = approvalsAppService;
        }

        public async Task<ActionResult> Index(bool type) =>
            View(await _approvalsAppService.GetApprovalFilters(type));

        [AbpMvcAuthorize(AppPermissions.Pages_ExcuseApprovals_Admin)]
        public async Task<ActionResult> AdminApprovals() =>
            View(await _approvalsAppService.GetAdminApprovalFilters());

        public async Task<ActionResult> ApprovalRequests() =>
            View(await _approvalsAppService.GetAdminApprovalFilters());

        public async Task<PartialViewResult> ViewExcuseApprovalsModal(int id)
        {
            var viewDto = await _excuseApprovalsAppService.GetExcuseApprovalsForView(id);

            return PartialView("_ViewExcuseApprovalsModal", new ExcuseApprovalsViewModel()
            {
                ExcuseDetails = viewDto.ExcuseDetails
            });
        }

        public async Task<PartialViewResult> ViewExcuseApprovalRequestsModal(int id)
        {
            var viewDto = await _excuseApprovalsAppService.GetExcuseApprovalsForView(id);

            return PartialView("_ViewExcuseApprovalRequestsModal", new ExcuseApprovalsViewModel()
            {
                ExcuseDetails = viewDto.ExcuseDetails
            });
        }

        public PartialViewResult RemarksExcuseApprovalsModal(RemarksViewModel model) =>
            PartialView("_RemarksExcuseApprovalsModal", model);
    }
}