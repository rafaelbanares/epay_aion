using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Timekeeping;
using MMB.ePay.Web.Areas.App.Models.Welcome;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize]
    public class WelcomeController : ePayControllerBase
    {
        private readonly IWelcomeAppService _welcomeAppService;

        public WelcomeController(IWelcomeAppService attendancesAppService) =>
            _welcomeAppService = attendancesAppService;

        public async Task<ActionResult> Index()
        {
            var getWelcomeForViewDto = await _welcomeAppService.GetWelcomeForView();
            var model = new WelcomeViewModel
            {
                Welcome = getWelcomeForViewDto.Welcome
            };

            return View(model);
        }
    }
}