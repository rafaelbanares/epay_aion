﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Web.Areas.App.Models.SpeedTest;
using MMB.ePay.Web.Controllers;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize]
    public class SpeedTestController : ePayControllerBase
    {
        public SpeedTestController()
        {
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Administration_SpeedTest)]
        public ActionResult Index() =>

                View(new SpeedTestViewModel());
    }
}