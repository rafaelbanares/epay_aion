﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.LeaveApprovals;
using MMB.ePay.Web.Areas.App.Models.LeaveRequests;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_LeaveRequests)]
    public class LeaveRequestsController : ePayControllerBase
    {
        private readonly ILeaveRequestsAppService _leaveRequestsAppService;
        private readonly ILeaveTypesAppService _leaveTypesAppService;
        private readonly ILeaveReasonsAppService _leaveReasonsAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public LeaveRequestsController(ILeaveRequestsAppService leaveRequestsAppService,
            ILeaveTypesAppService leaveTypesAppService,
            ILeaveReasonsAppService leaveReasonsAppService,
            IApprovalsAppService approvalsAppService)
        {
            _leaveRequestsAppService = leaveRequestsAppService;
            _leaveTypesAppService = leaveTypesAppService;
            _leaveReasonsAppService = leaveReasonsAppService;
            _approvalsAppService = approvalsAppService;
        }

        public async Task<ActionResult> Index() =>
            View(await _approvalsAppService.GetRequestsFilters());

        [AbpMvcAuthorize(AppPermissions.Pages_LeaveRequests_Create, AppPermissions.Pages_LeaveRequests_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var leaveRequests = new CreateOrEditLeaveRequestsDto();

            if (id.HasValue)
            {
                var output = await _leaveRequestsAppService.GetLeaveRequestsForEdit(id.Value);
                leaveRequests = output.LeaveRequests;
            }

            var viewModel = new CreateOrEditLeaveRequestsModalViewModel()
            {
                LeaveRequests = leaveRequests,
                LeaveTypeList = await _leaveTypesAppService.GetLeaveTypeList(),
                LeaveReasonList = await _leaveReasonsAppService.GetLeaveReasonList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewLeaveRequestsModal(int id)
        {
            var viewDto = await _leaveRequestsAppService.GetLeaveRequestsForView(id);
            var model = new LeaveRequestsViewModel()
            {
                LeaveDetails = viewDto.LeaveDetails
            };

            return PartialView("_ViewLeaveRequestsModal", model);
        }

        public PartialViewResult RemarksLeaveRequestsModal(LeaveRemarksViewModel model) =>
            PartialView("_RemarksLeaveRequestsModal", model);
    }
}