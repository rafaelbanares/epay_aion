﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.LeaveApprovals;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_LeaveApprovals)]
    public class LeaveApprovalsController : ePayControllerBase
    {
        private readonly ILeaveApprovalsAppService _leaveApprovalsAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public LeaveApprovalsController(ILeaveApprovalsAppService leaveApprovalsAppService,
            IApprovalsAppService approvalsAppService)
        {
            _leaveApprovalsAppService = leaveApprovalsAppService;
            _approvalsAppService = approvalsAppService;
        }

        public async Task<ActionResult> Index(bool type) =>
            View(await _approvalsAppService.GetApprovalFilters(type));

        [AbpMvcAuthorize(AppPermissions.Pages_LeaveApprovals_Admin)]
        public async Task<ActionResult> AdminApprovals() =>
            View(await _approvalsAppService.GetAdminApprovalFilters());

        public async Task<ActionResult> ApprovalRequests() =>
            View(await _approvalsAppService.GetAdminApprovalFilters());

        public async Task<PartialViewResult> ViewLeaveApprovalsModal(int id)
        {
            var viewDto = await _leaveApprovalsAppService.GetLeaveApprovalsForView(id);

            return PartialView("_ViewLeaveApprovalsModal", new LeaveApprovalsViewModel() 
            { 
                LeaveDetails = viewDto.LeaveDetails 
            });
        }

        public async Task<PartialViewResult> ViewLeaveApprovalRequestsModal(int id)
        {
            var viewDto = await _leaveApprovalsAppService.GetLeaveApprovalsForView(id);

            return PartialView("_ViewLeaveApprovalRequestsModal", new LeaveApprovalsViewModel() 
            { 
                LeaveDetails = viewDto.LeaveDetails 
            });
        }

        public PartialViewResult RemarksLeaveApprovalsModal(RemarksViewModel model) =>
            PartialView("_RemarksLeaveApprovalsModal", model);
    }
}