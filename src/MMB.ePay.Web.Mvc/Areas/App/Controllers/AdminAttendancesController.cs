﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.Attendances;
using MMB.ePay.Web.Controllers;
using System;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Attendances)]
    public class AdminAttendancesController : ePayControllerBase
    {
        private readonly IAttendancesAppService _attendancesAppService;
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;
        private readonly ITKSettingsAppService _tKSettingsAppService;

        public AdminAttendancesController(IAttendancesAppService attendancesAppService,
            IAttendancePeriodsAppService attendancePeriodsAppService,
            ITKSettingsAppService tKSettingsAppService)
        {
            _attendancesAppService = attendancesAppService;
            _attendancePeriodsAppService = attendancePeriodsAppService;
            _tKSettingsAppService = tKSettingsAppService;
        }

        public async Task<ActionResult> Index()
        {
            return View(new AttendancesViewModel
            {
                HasPMBreak = await _tKSettingsAppService.HasPMBreak(),
                YearList = await _attendancePeriodsAppService.GetYearListForFilter(),
                AttendancePeriodList = await _attendancePeriodsAppService
                    .GetAttendancePeriodListByYearForTimesheet(DateTime.Now.Year, null, null)
            });
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Attendances_Create, AppPermissions.Pages_Attendances_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var attendances = new CreateOrEditAttendancesDto();

            if (id.HasValue)
            {
                var output = await _attendancesAppService.GetAttendancesForEdit(new EntityDto { Id = (int)id });
                attendances = output.Attendances;
            }

            var viewModel = new CreateOrEditAttendancesModalViewModel
            {
                Attendances = attendances
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewAttendancesModal(int id, bool isPosted)
        {
            var viewDto = await _attendancesAppService.GetAttendancesForView(id, isPosted);

            var model = new AttendancesViewModel()
            {
                Attendances = viewDto.Attendances,
                HasPMBreak = await _tKSettingsAppService.HasPMBreak()
            };

            return PartialView("_ViewAttendancesModal", model);
        }
    }
}