﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Web.Areas.App.Models.Hangfire;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_HangfireDashboard)]
    public class RecurringJobsController : ePayControllerBase
    {
        private readonly IHangfireAppService _hangfireAppService;

        public RecurringJobsController(IHangfireAppService hangfireAppService)
        {
            _hangfireAppService = hangfireAppService;
        }

        public ActionResult Index()
        {
            return View(new HangfireViewModel());
        }

        public PartialViewResult ViewRecurringJobsModal(string id)
        {
            var viewDto = _hangfireAppService.GetHangfireForView(id);
            var model = new HangfireViewModel()
            {
                Hangfire = viewDto.Hangfire
            };

            return PartialView("_ViewRecurringJobsModal", model);
        }
    }
}