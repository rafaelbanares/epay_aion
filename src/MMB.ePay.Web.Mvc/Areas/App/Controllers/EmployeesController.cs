﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.HR.Dtos;
using MMB.ePay.Timekeeping;
using MMB.ePay.Web.Areas.App.Models.Employees;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Employees)]
    public class EmployeesController : ePayControllerBase
    {
        private readonly IEmployeesAppService _employeesManager;
        private readonly IEmployeeTimeInfoAppService _employeeTimeInfoManager;
        private readonly IEmploymentTypesAppService _employmentTypesManager;
        private readonly IShiftTypesAppService _shiftTypesManager;
        private readonly IFrequenciesAppService _frequenciesManager;
        private readonly ILocationsAppService _locationsManager;
        private readonly IHelperAppService _helperAppService;

        public EmployeesController(IEmployeesAppService employeesManager,
            IEmployeeTimeInfoAppService employeeTimeInfoManager,
            IEmploymentTypesAppService employmentTypesManager,
            IShiftTypesAppService shiftTypesManager,
            IFrequenciesAppService frequenciesManager,
            ILocationsAppService locationsManager,
            IHelperAppService helperAppService)
        {
            _employeesManager = employeesManager;
            _employeeTimeInfoManager = employeeTimeInfoManager;
            _employmentTypesManager = employmentTypesManager;
            _shiftTypesManager = shiftTypesManager;
            _frequenciesManager = frequenciesManager;
            _locationsManager = locationsManager;
            _helperAppService = helperAppService;
        }

        public async Task<ActionResult> Index()
        {
            var model = new EmployeesViewModel
            {
                TypeList = await _employmentTypesManager.GetEmploymentTypeListForFilter(),
                StatusList = _helperAppService.GetEmployeeStatusListForFilter()
            };

            return View(model);
        }

        public async Task<IActionResult> Profile(int id)
        {
            var getEmployeesForEditOutput = await _employeesManager.GetEmployeesForEdit(new EntityDto { Id = id });
            var getTimeInfoForEditOutput = await _employeeTimeInfoManager.GetEmployeeTimeInfoForEdit(new EntityDto { Id = id });

            var viewModel = new ProfileEmployeesModalViewModel
            {
                Employees = getEmployeesForEditOutput.Employees,
                EmployeeStatusList = _helperAppService.GetEmployeeStatusList(),
                EmploymentTypeList = await _employmentTypesManager.GetEmploymentTypeList(),
                RankList = _helperAppService.GetRankList(),
            };

            if (getTimeInfoForEditOutput != null)
            {
                viewModel.EmployeeTimeInfo = getTimeInfoForEditOutput.EmployeeTimeInfo;
                viewModel.LocationList = await _locationsManager.GetLocationList();
                viewModel.FrequencyList = await _frequenciesManager.GetFrequencyList();
                viewModel.ShiftTypeList = await _shiftTypesManager.GetShiftTypeList();
            }

            return View(viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Employees_Create, AppPermissions.Pages_Employees_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var employees = new CreateOrEditEmployeesDto();

            if (id.HasValue)
            {
                var output = await _employeesManager.GetEmployeesForEdit(new EntityDto { Id = (int)id });
                employees = output.Employees;
            }

            var viewModel = new CreateOrEditEmployeesModalViewModel()
            {
                Employees = employees,
                EmploymentTypeList = await _employmentTypesManager.GetEmploymentTypeList(),
                RankList = _helperAppService.GetRankList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewEmployeesModal(int id)
        {
            var getEmployeesForViewDto = await _employeesManager.GetEmployeesForView(id);

            var model = new EmployeesViewModel()
            {
                Employees = getEmployeesForViewDto.Employees
            };

            return PartialView("_ViewEmployeesModal", model);
        }

    }
}