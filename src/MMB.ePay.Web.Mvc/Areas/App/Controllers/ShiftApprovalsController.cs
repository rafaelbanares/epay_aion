﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.ShiftApprovals;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_ShiftApprovals)]
    public class ShiftApprovalsController : ePayControllerBase
    {
        private readonly IShiftApprovalsAppService _shiftApprovalsAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public ShiftApprovalsController(IShiftApprovalsAppService shiftApprovalsAppService,
            IApprovalsAppService approvalsAppService)
        {
            _shiftApprovalsAppService = shiftApprovalsAppService;
            _approvalsAppService = approvalsAppService;
        }

        public async Task<ActionResult> Index(bool type) =>
            View(await _approvalsAppService.GetApprovalFilters(type));

        [AbpMvcAuthorize(AppPermissions.Pages_ShiftApprovals_Admin)]
        public async Task<ActionResult> AdminApprovals() =>
            View(await _approvalsAppService.GetAdminApprovalFilters());

        public async Task<ActionResult> ApprovalRequests() =>
            View(await _approvalsAppService.GetAdminApprovalFilters());

        public async Task<PartialViewResult> ViewShiftApprovalsModal(int id)
        {
            var viewDto = await _shiftApprovalsAppService.GetShiftApprovalsForView(id);

            return PartialView("_ViewShiftApprovalsModal", new ShiftApprovalsViewModel()
            {
                ShiftDetails = viewDto.ShiftDetails
            });
        }

        public async Task<PartialViewResult> ViewShiftApprovalRequestsModal(int id)
        {
            var viewDto = await _shiftApprovalsAppService.GetShiftApprovalsForView(id);

            return PartialView("_ViewShiftApprovalRequestsModal", new ShiftApprovalsViewModel()
            {
                ShiftDetails = viewDto.ShiftDetails
            });
        }

        public PartialViewResult RemarksShiftApprovalsModal(RemarksViewModel model) =>
            PartialView("_RemarksShiftApprovalsModal", model);
    }
}