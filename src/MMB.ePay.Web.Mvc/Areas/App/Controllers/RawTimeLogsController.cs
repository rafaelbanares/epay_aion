﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.RawTimeLogs;
using MMB.ePay.Web.Controllers;
using System;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
	[Area("App")]
    public class RawTimeLogsController : ePayControllerBase
    {
        private readonly IRawTimeLogsAppService _rawTimeLogsAppService;

        public RawTimeLogsController(IRawTimeLogsAppService rawTimeLogsAppService) =>
            _rawTimeLogsAppService = rawTimeLogsAppService;

        [AbpMvcAuthorize(AppPermissions.Pages_RawTimeLogs)]
        public ActionResult Index() =>
            View(new RawTimeLogsViewModel());

        [AbpMvcAuthorize(AppPermissions.Pages_RawTimeLogs_Create, AppPermissions.Pages_RawTimeLogs_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var rawTimeLogs = new CreateOrEditRawTimeLogsDto { LogTime = DateTime.Now };

            if (id.HasValue)
            {
                var output = await _rawTimeLogsAppService.GetRawTimeLogsForEdit(id.Value);
                rawTimeLogs = output.RawTimeLogs;
            }

            var viewModel = new CreateOrEditRawTimeLogsModalViewModel()
            {
                RawTimeLogs = rawTimeLogs
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_RawTimeLogs)]
        public async Task<PartialViewResult> ViewRawTimeLogsModal(int id)
        {
            var viewDto = await _rawTimeLogsAppService.GetRawTimeLogsForView(id);

            var model = new RawTimeLogsViewModel()
            {
                RawTimeLogs = viewDto.RawTimeLogs
            };

            return PartialView("_ViewRawTimeLogsModal", model);
        }

        public async Task<PartialViewResult> ViewAttendancesRawEntriesModal(int id)
        {
            var viewDto = await _rawTimeLogsAppService.GetRawTimeLogsForAttendances(id);
			var model = new AttendancesRawEntriesModel()
            {
                RawTimeLogsList = viewDto.RawTimeLogsList
			};

            return PartialView("_ViewAttendancesRawEntriesModal", model);
        }
    }
}