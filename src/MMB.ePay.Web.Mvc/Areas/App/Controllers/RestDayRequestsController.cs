﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.RestDayApprovals;
using MMB.ePay.Web.Areas.App.Models.RestDayRequests;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_RestDayRequests)]
    public class RestDayRequestsController : ePayControllerBase
    {
        private readonly IRestDayRequestsAppService _restDayRequestsAppService;
        private readonly IRestDayReasonsAppService _restDayReasonsAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public RestDayRequestsController(IRestDayRequestsAppService restDayRequestsAppService,
            IRestDayReasonsAppService restDayReasonsAppService,
            IApprovalsAppService approvalsAppService)
        {
            _restDayRequestsAppService = restDayRequestsAppService;
            _restDayReasonsAppService = restDayReasonsAppService;
            _approvalsAppService = approvalsAppService;
        }

        public async Task<ActionResult> Index() =>
            View(await _approvalsAppService.GetRequestsFilters());

        [AbpMvcAuthorize(AppPermissions.Pages_RestDayRequests_Create, AppPermissions.Pages_RestDayRequests_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var restDayRequests = new CreateOrEditRestDayRequestsDto();

            if (id.HasValue)
            {
                var output = await _restDayRequestsAppService.GetRestDayRequestsForEdit(id.Value);
                restDayRequests = output.RestDayRequests;
            }

            var viewModel = new CreateOrEditRestDayRequestsModalViewModel()
            {
                RestDayRequests = restDayRequests,
                RestDayReasonList = await _restDayReasonsAppService.GetRestDayReasonList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewRestDayRequestsModal(int id)
        {
            var viewDto = await _restDayRequestsAppService.GetRestDayRequestsForView(id);
            var model = new RestDayRequestsViewModel()
            {
                RestDayDetails = viewDto.RestDayDetails
            };

            return PartialView("_ViewRestDayRequestsModal", model);
        }

        public PartialViewResult RemarksRestDayRequestsModal(RestDayRemarksViewModel model) =>
            PartialView("_RemarksRestDayRequestsModal", model);
    }
}