﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.OfficialBusinessApprovals;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_OfficialBusinessApprovals)]
    public class OfficialBusinessApprovalsController : ePayControllerBase
    {
        private readonly IOfficialBusinessApprovalsAppService _obApprovalsAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public OfficialBusinessApprovalsController(IOfficialBusinessApprovalsAppService obApprovalsAppService,
            IApprovalsAppService approvalsAppService)
        {
            _obApprovalsAppService = obApprovalsAppService;
            _approvalsAppService = approvalsAppService;
        }

        public async Task<ActionResult> Index(bool type) =>
            View(await _approvalsAppService.GetApprovalFilters(type));

        [AbpMvcAuthorize(AppPermissions.Pages_OfficialBusinessApprovals_Admin)]
        public async Task<ActionResult> AdminApprovals() =>
            View(await _approvalsAppService.GetAdminApprovalFilters());

        public async Task<ActionResult> ApprovalRequests() =>
            View(await _approvalsAppService.GetAdminApprovalFilters());

        public async Task<PartialViewResult> ViewOfficialBusinessApprovalsModal(int id)
        {
            var viewDto = await _obApprovalsAppService.GetOfficialBusinessApprovalsForView(id);

            return PartialView("_ViewOfficialBusinessApprovalsModal", new OfficialBusinessApprovalsViewModel()
            {
                OfficialBusinessDetails = viewDto.OfficialBusinessDetails
            });
        }

        public async Task<PartialViewResult> ViewOfficialBusinessApprovalRequestsModal(int id)
        {
            var viewDto = await _obApprovalsAppService.GetOfficialBusinessApprovalsForView(id);

            return PartialView("_ViewOfficialBusinessApprovalRequestsModal", new OfficialBusinessApprovalsViewModel()
            {
                OfficialBusinessDetails = viewDto.OfficialBusinessDetails
            });
        }

        public PartialViewResult RemarksOfficialBusinessApprovalsModal(RemarksViewModel model) =>
            PartialView("_RemarksOfficialBusinessApprovalsModal", model);
    }
}