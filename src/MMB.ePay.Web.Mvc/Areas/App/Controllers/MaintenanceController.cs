﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Caching;
using MMB.ePay.Web.Areas.App.Models.Maintenance;
using MMB.ePay.Web.Controllers;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_Host_Maintenance)]
    public class MaintenanceController : ePayControllerBase
    {
        private readonly ICachingAppService _cachingAppService;

        public MaintenanceController(ICachingAppService cachingAppService) =>
            _cachingAppService = cachingAppService;

        public ActionResult Index() =>
            View(new MaintenanceViewModel
            {
                Caches = _cachingAppService.GetAllCaches().Items
            });
    }
}