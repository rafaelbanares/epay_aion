﻿using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Configuration;
using Abp.MultiTenancy;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization.Users.Profile;
using MMB.ePay.Configuration;
using MMB.ePay.Timing;
using MMB.ePay.Timing.Dto;
using MMB.ePay.Web.Areas.App.Models.Profile;
using MMB.ePay.Web.Controllers;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize]
    public class ProfileController : ePayControllerBase
    {
        private readonly IProfileAppService _profileAppService;
        private readonly ITimingAppService _timingAppService;
        private readonly ITenantCache _tenantCache;

        public ProfileController(
            IProfileAppService profileAppService,
            ITimingAppService timingAppService, 
            ITenantCache tenantCache)
        {
            _profileAppService = profileAppService;
            _timingAppService = timingAppService;
            _tenantCache = tenantCache;
        }

        public async Task<PartialViewResult> MySettingsModal()
        {
            var output = await _profileAppService.GetCurrentUserProfileForEdit();
            var viewModel = ObjectMapper.Map<MySettingsViewModel>(output);
            viewModel.TimezoneItems = await _timingAppService.GetTimezoneComboboxItems(new GetTimezoneComboboxItemsInput
            {
                DefaultTimezoneScope = SettingScopes.User,
                SelectedTimezoneId = output.Timezone
            });
            viewModel.SmsVerificationEnabled = await SettingManager.GetSettingValueAsync<bool>(AppSettings.UserManagement.SmsVerificationEnabled);

            return PartialView("_MySettingsModal", viewModel);
        }

        public PartialViewResult ChangePictureModal() =>
            PartialView("_ChangePictureModal");

        public PartialViewResult ChangePasswordModal() =>
            PartialView("_ChangePasswordModal");

        public PartialViewResult SmsVerificationModal() =>
            PartialView("_SmsVerificationModal");


        public PartialViewResult LinkedAccountsModal() =>
            PartialView("_LinkedAccountsModal");

        public PartialViewResult LinkAccountModal()
        {
            ViewBag.TenancyName = GetTenancyNameOrNull();
            return PartialView("_LinkAccountModal");
        }

        public PartialViewResult UserDelegationsModal() =>
            PartialView("_UserDelegationsModal");

        public PartialViewResult CreateNewUserDelegationModal() =>
            PartialView("_CreateNewUserDelegationModal");

        private string GetTenancyNameOrNull()
        {
            if (!AbpSession.TenantId.HasValue)
                return null;

            return _tenantCache.GetOrNull(AbpSession.TenantId.Value)?.TenancyName;
        }
    }
}