﻿using System.Collections.Generic;
using System.Linq;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization.Permissions;
using MMB.ePay.Authorization.Permissions.Dto;
using MMB.ePay.Web.Areas.App.Models.Common.Modals;
using MMB.ePay.Web.Controllers;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize]
    public class CommonController : ePayControllerBase
    {
        private readonly IPermissionAppService _permissionAppService;

        public CommonController(IPermissionAppService permissionAppService) =>
            _permissionAppService = permissionAppService;

        public PartialViewResult LookupModal(LookupModalViewModel model) =>
            PartialView("Modals/_LookupModal", model);

        public PartialViewResult EntityTypeHistoryModal(EntityHistoryModalViewModel input) =>
            PartialView("Modals/_EntityTypeHistoryModal", ObjectMapper.Map<EntityHistoryModalViewModel>(input));

        public PartialViewResult PermissionTreeModal(List<string> grantedPermissionNames = null)
        {
            var permissions = _permissionAppService.GetAllPermissions().Items.ToList();

            var model = new PermissionTreeModalViewModel
            {
                Permissions = ObjectMapper.Map<List<FlatPermissionDto>>(permissions).OrderBy(p => p.DisplayName).ToList(),
                GrantedPermissionNames = grantedPermissionNames
            };

            return PartialView("Modals/_PermissionTreeModal", model);
        }

        public PartialViewResult InactivityControllerNotifyModal() =>
            PartialView("Modals/_InactivityControllerNotifyModal");
    }
}