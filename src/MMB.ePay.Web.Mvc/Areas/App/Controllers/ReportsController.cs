﻿using Abp.AspNetCore.Mvc.Authorization;
using DevExpress.XtraReports.UI;
using jsreport.AspNetCore;
using jsreport.Types;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Organizations;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.Reports;
using MMB.ePay.Web.Controllers;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Reports)]
    public class ReportsController : ePayControllerBase
    {
        private readonly IJsReportMVCService _jsReportMVCService;
        private readonly IReportsAppService _reportsAppService;
        private readonly IStoredProcedureReportsAppService _storedProcedureReportsAppService;
        private readonly IHelperAppService _helperAppService;
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;
        private readonly IFrequenciesAppService _frequenciesAppService;
        private readonly IOrganizationUnitAppService _organizationUnitAppService;
        private readonly ICompaniesAppService _companiesAppService;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ReportsController(
            IJsReportMVCService jsReportMVCService,
            IReportsAppService reportsAppService,
            IStoredProcedureReportsAppService storedProcedureReportsAppService,
            IHelperAppService helperAppService,
            IAttendancePeriodsAppService attendancePeriodsAppService,
            IFrequenciesAppService frequenciesAppService,
            IOrganizationUnitAppService organizationUnitAppService,
            ICompaniesAppService companiesAppService,
            IWebHostEnvironment webHostEnvironment)
        {
            _jsReportMVCService = jsReportMVCService;
            _reportsAppService = reportsAppService;
            _storedProcedureReportsAppService = storedProcedureReportsAppService;
            _helperAppService = helperAppService;
            _attendancePeriodsAppService = attendancePeriodsAppService;
            _frequenciesAppService = frequenciesAppService;
            _organizationUnitAppService = organizationUnitAppService;
            _companiesAppService = companiesAppService;
            _webHostEnvironment = webHostEnvironment;
        }

        public async Task<IActionResult> Index()
        {
            var frequencyList = await _frequenciesAppService.GetFrequencyListForFilter();
            var frequencyId = int.Parse(frequencyList.FirstOrDefault(e => e.Selected).Value);

            var monthList = _helperAppService.GetMonthList();
            monthList.RemoveAt(0);

            var company = await _companiesAppService.GetOrgNames();

            var model = new ReportsViewModel
            {
                Reports = new ReportsDto
                {
                    OrgUnitLevel1Name = company?.Item1,
                    OrgUnitLevel2Name = company?.Item2,
                    OrgUnitLevel3Name = company?.Item3,
                    ReportList = await _reportsAppService.GetReportTypeList(),
                    RankList = _helperAppService.GetRankList(),
                    OrganizationUnitList = await _organizationUnitAppService.GetOrganizationUnitList(),
                    YearList = await _attendancePeriodsAppService.GetYearListForFilter(),
                    MonthList = monthList,
                    CoveringPeriodList = _helperAppService.GetCoveringPeriodList(),
                    PostList = _helperAppService.GetPostList(),
                    PostFilterId = true,
                    FrequencyList = frequencyList,
                    FrequencyFilterId = frequencyId,
                    AttendancePeriodList = await _attendancePeriodsAppService
                        .GetAttendancePeriodListByYear(DateTime.Now.Year, frequencyId, true)
                }
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Index(ReportsViewModel model)
        {
            var names = await _reportsAppService.GetControllerName(model.Reports.ReportId);
            model.Reports.ExportFileName = names.ExportFileName;
            model.Reports.StoredProcedure = names.StoredProcedure;

            return RedirectToAction(names.ControllerName, model.Reports);
        }

        public IActionResult DevExpressOutputReport(XtraReport xtraReport, string output, string fileName)
        {
            using MemoryStream stream = new();
            stream.Position = 0;

            var fileType = output == "E" ? "xlsx" : "pdf";

            if (output == "E")
                xtraReport.ExportToXlsx(stream);
            else
                xtraReport.ExportToPdf(stream);

            Response.Headers.Add("Content-Disposition", $"inline; filename={fileName}_Report.{fileType}");
            return File(stream.ToArray(), "application/xlsx", $"{fileName}_Report.{fileType}");
        }

        private async Task HttpContextSettings(ReportsDto model)
        {
            var output = await _reportsAppService.GetReportsForView(model.ReportId);
            var reports = output.Reports;

            model.Format = reports.Format;
            model.IsLandscape = reports.IsLandscape;
            model.OrgUnitIds = await _organizationUnitAppService.GetOrganizationUnitIdListByParent(model.MainOrganizationUnitId);
            model.ControllerName = reports.ControllerName;

            if (!reports.HasPeriodFilter)
            {
                model.MaxDate = null;
                model.MinDate = null;
            }
            else
                model.MaxDate = model.MaxDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59);

            if (!reports.HasEmployeeFilter)
                model.EmployeeId = 0;

            if (model.Output == "E")
            {
                HttpContext.JsReportFeature()
                    .Recipe(Recipe.HtmlToXlsx)
                    .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });
            }
            else
            {
                var rank = _helperAppService.GetRankById(model.Rank);
                var reportName = Regex.Replace(model.ControllerName, "([A-Z])", " $1").Trim() + " Report";

                var headerModel = new ReportHeaderDto
                {
                    ReportName = reportName,
                    Period = $"{model.MinDate:MM/dd/yyyy} - {model.MaxDate:MM/dd/yyyy}",
                    Rank = rank
                };

                var header = await _jsReportMVCService.RenderViewToStringAsync(HttpContext, RouteData, "Header", headerModel);

                HttpContext.JsReportFeature()
                    .Recipe(Recipe.ChromePdf)
                    .Configure((r) => r.Options.Timeout = 120000)
                    .Configure((r) =>
                    {
                        r.Options.Timeout = 120000;
                        r.Options.ReportName = model.ExportFileName;
                    })
                    .Configure((r) => r.Template.Chrome = new Chrome
                    {
                        HeaderTemplate = header,
                        DisplayHeaderFooter = false,
                        MarginTop = "2cm",
                        MarginLeft = "1cm",
                        MarginBottom = "1cm",
                        MarginRight = "1cm",
                        Landscape = model.IsLandscape,
                        Format = model.Format
                    });
            }
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> ActiveEmployees(ReportsDto model)
        {
            await HttpContextSettings(model);

            var transaction = await _reportsAppService.GetActiveEmployees(model);
            var output = new GetReportsForActiveEmployees
            {
                ActiveEmployees = transaction.ActiveEmployees
            };

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> DailyAttendance(ReportsDto model)
        {
            await HttpContextSettings(model);

            var transaction = await _reportsAppService.GetDailyAttendance(model);
            var output = new GetReportsForDailyAttendance
            {
                DailyAttendance = transaction.DailyAttendance
            };

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> AttendanceWithBreaks(ReportsDto model)
        {
            await HttpContextSettings(model);

            var transaction = await _reportsAppService.GetAttendanceWithBreaks(model);
            var output = new GetReportsForAttendanceWithBreaks
            {
                AttendanceWithBreaks = transaction.AttendanceWithBreaks
            };

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> WorkFromHome(ReportsDto model)
        {
            await HttpContextSettings(model);

            var transaction = await _reportsAppService.GetWorkFromHome(model);
            var output = new GetReportsForWorkFromHome
            {
                WorkFromHome = transaction.WorkFromHome
            };

            return View(output);
        }

        //[MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> AttendanceSummary(ReportsDto model)
        {
            var reportName = await _reportsAppService.GetReportName(model.ReportId);
            var fileName = reportName.Replace(" ", "");

            GetReportsForAttendanceSummary reportData;

            if (string.IsNullOrEmpty(model.StoredProcedure))
                reportData = await _reportsAppService.GetAttendanceSummary(model);
            else
                reportData = await _storedProcedureReportsAppService.GetAttendanceSummary(model);

            var report = new XtraReport();
            var reportPath = Path.Combine(_webHostEnvironment.ContentRootPath, "Reports", fileName + ".repx");
            report.LoadLayout(reportPath);

            report.DataSource = reportData.AttendanceSummary;

            return DevExpressOutputReport(report, model.Output, fileName);

            //await HttpContextSettings(model);

            //var transaction = await _reportsAppService.GetAttendanceSummary(model);
            //var output = new GetReportsForAttendanceSummary
            //{
            //    AttendanceSummary = transaction.AttendanceSummary
            //};

            //return View(output);
        }

        //public async Task<IActionResult> AttendanceSummaryDX(ReportsDto model)
        //{
        //    return View();
        //}

        //[MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> AttendanceSummary2(ReportsDto model)
        {
            var reportName = await _reportsAppService.GetReportName(model.ReportId);
            var fileName = reportName.Replace(" ", "");

            GetReportsForAttendanceSummary reportData;

            if (string.IsNullOrEmpty(model.StoredProcedure))
                reportData = await _reportsAppService.GetAttendanceSummary(model);
            else
                reportData = await _storedProcedureReportsAppService.GetAttendanceSummary(model);

            var report = new XtraReport();
            var reportPath = Path.Combine(_webHostEnvironment.ContentRootPath, "Reports", fileName + ".repx");
            report.LoadLayout(reportPath);

            report.DataSource = reportData.AttendanceSummary;

            return DevExpressOutputReport(report, model.Output, fileName);

            //await HttpContextSettings(model);

            //var transaction = await _reportsAppService.GetAttendanceSummary(model);
            //var output = new GetReportsForAttendanceSummary
            //{
            //    AttendanceSummary = transaction.AttendanceSummary
            //};

            //return View(output);
        }

        //[MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> AttendanceSummaryPayroll(ReportsDto model)
        {
            var reportName = await _reportsAppService.GetReportName(model.ReportId);
            var fileName = reportName.Replace(" ", "");

            GetReportsForAttendanceSummaryPayroll reportData;

            if (string.IsNullOrEmpty(model.StoredProcedure))
                reportData = await _reportsAppService.GetAttendanceSummaryPayroll(model);
            else
                reportData = await _storedProcedureReportsAppService.GetAttendanceSummaryPayroll(model);

            var report = new XtraReport();
            var reportPath = Path.Combine(_webHostEnvironment.ContentRootPath, "Reports", fileName + ".repx");
            report.LoadLayout(reportPath);

            report.DataSource = reportData.AttendanceSummaryPayroll;

            return DevExpressOutputReport(report, model.Output, fileName);

            //await HttpContextSettings(model);

            //var transaction = await _reportsAppService.GetAttendanceSummaryPayroll(model);
            //var output = new GetReportsForAttendanceSummaryPayroll
            //{
            //    AttendanceSummaryPayroll = transaction.AttendanceSummaryPayroll
            //};

            //return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> AttendanceSummaryPayrollWithMinuteFormat(ReportsDto model)
        {
            await HttpContextSettings(model);

            var transaction = await _reportsAppService.GetAttendanceSummaryPayrollWithMinuteFormat(model);
            var output = new GetReportsForAttendanceSummaryPayroll
            {
                AttendanceSummaryPayroll = transaction.AttendanceSummaryPayroll
            };

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> LeaveBalance(ReportsDto model)
        {
            await HttpContextSettings(model);

            var transaction = await _reportsAppService.GetLeaveBalance(model);
            var output = new GetReportsForLeaveBalance
            {
                LeaveBalance = transaction.LeaveBalance
            };

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> LeaveSummary(ReportsDto model)
        {
            await HttpContextSettings(model);

            var transaction = await _reportsAppService.GetLeaveSummary(model);
            var output = new GetReportsForLeaveSummary
            {
                LeaveTypes = transaction.LeaveTypes,
                LeaveSummary = transaction.LeaveSummary
            };

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> LeaveSummaryPayroll(ReportsDto model)
        {
            await HttpContextSettings(model);

            var transaction = await _reportsAppService.GetLeaveSummaryPayroll(model);
            var output = new GetReportsForLeaveSummaryPayroll
            {
                LeaveSummaryPayroll = transaction.LeaveSummaryPayroll
            };

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> OvertimeSummary(ReportsDto model)
        {
            var reportName = await _reportsAppService.GetReportName(model.ReportId);
            var fileName = reportName.Replace(" ", "");

            //GetReportsForOvertimeSummary reportData;

            //if (string.IsNullOrEmpty(model.StoredProcedure))
            //    reportData = await _reportsAppService.GetOvertimeSummary(model);
            //else
            //    reportData = await _storedProcedureReportsAppService.GetOvertimeSummary(model);

            var reportData = await _reportsAppService.GetOvertimeSummary(model);

            var report = new XtraReport();
            var reportPath = Path.Combine(_webHostEnvironment.ContentRootPath, "Reports", fileName + ".repx");
            report.LoadLayout(reportPath);

            var overtimeSummary = reportData.OvertimeSummary;
            var overtimeTypes = reportData.OvertimeTypes;

            report.DataSource = overtimeSummary;

            return DevExpressOutputReport(report, model.Output, fileName);

            //await HttpContextSettings(model);

            //var transaction = await _reportsAppService.GetOvertimeSummary(model);
            //var output = new GetReportsForOvertimeSummary
            //{
            //    OvertimeTypes = transaction.OvertimeTypes,
            //    OvertimeSummary = transaction.OvertimeSummary
            //};

            //return View(output);
        }

        //[MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> OvertimeSummaryPayroll(ReportsDto model)
        {
            var reportName = await _reportsAppService.GetReportName(model.ReportId);
            var fileName = reportName.Replace(" ", "");

            GetReportsForOvertimeSummaryPayroll reportData;

            if (string.IsNullOrEmpty(model.StoredProcedure))
                reportData = await _reportsAppService.GetOvertimeSummaryPayroll(model);
            else
                reportData = await _storedProcedureReportsAppService.GetOvertimeSummaryPayroll(model);

            var report = new XtraReport();
            var reportPath = Path.Combine(_webHostEnvironment.ContentRootPath, "Reports", fileName + ".repx");
            report.LoadLayout(reportPath);

            report.DataSource = reportData.OvertimeSummaryPayroll;

            return DevExpressOutputReport(report, model.Output, fileName);

            //await HttpContextSettings(model);

            //var transaction = await _reportsAppService.GetOvertimeSummaryPayroll(model);
            //var output = new GetReportsForOvertimeSummaryPayroll
            //{
            //    OvertimeTypes = transaction.OvertimeTypes,
            //    OvertimeSummaryPayroll = transaction.OvertimeSummaryPayroll
            //};

            //return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> OvertimeSummaryPayrollWithMinuteFormat(ReportsDto model)
        {
            await HttpContextSettings(model);

            var transaction = await _reportsAppService.GetOvertimeSummaryPayrollWithMinuteFormat(model);
            var output = new GetReportsForOvertimeSummaryPayroll
            {
                OvertimeTypes = transaction.OvertimeTypes,
                OvertimeSummaryPayroll = transaction.OvertimeSummaryPayroll
            };

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> AttendanceTransaction(ReportsDto model)
        {
            await HttpContextSettings(model);

            var transaction = await _reportsAppService.GetAttendanceTransaction(model);
            var output = new GetReportsForAttendanceTransaction
            {
                AttendanceTransaction = transaction.AttendanceTransaction
            };

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> ExcuseTransaction(ReportsDto model)
        {
            await HttpContextSettings(model);

            var transaction = await _reportsAppService.GetExcuseTransaction(model);
            var output = new GetReportsForExcuseTransaction
            {
                ExcuseTransaction = transaction.ExcuseTransaction
            };

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> LeaveTransaction(ReportsDto model)
        {
            await HttpContextSettings(model);

            var transaction = await _reportsAppService.GetLeaveTransaction(model);
            var output =  new GetReportsForLeaveTransaction
            {
                LeaveTransaction = transaction.LeaveTransaction
            };

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> OfficialBusinessTransaction(ReportsDto model)
        {
            await HttpContextSettings(model);

            var transaction = await _reportsAppService.GetOfficialBusinessTransaction(model);
            var output = new GetReportsForOfficialBusinessTransaction
            {
                OfficialBusinessTransaction = transaction.OfficialBusinessTransaction
            };

            return View(output);
        }

        //[MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> OvertimeTransaction(ReportsDto model)
        {
            var reportName = await _reportsAppService.GetReportName(model.ReportId);
            var fileName = reportName.Replace(" ", "");

            GetReportsForOvertimeTransaction reportData;

            if (string.IsNullOrEmpty(model.StoredProcedure))
                reportData = await _reportsAppService.GetOvertimeTransaction(model);
            else
                reportData = await _storedProcedureReportsAppService.GetOvertimeTransaction(model);

            var report = new XtraReport();
            var reportPath = Path.Combine(_webHostEnvironment.ContentRootPath, "Reports", fileName + ".repx");
            report.LoadLayout(reportPath);

            report.DataSource = reportData.OvertimeTransaction;

            return DevExpressOutputReport(report, model.Output, fileName);

            //await HttpContextSettings(model);

            //var transaction = await _reportsAppService.GetOvertimeTransaction(model);
            //var output = new GetReportsForOvertimeTransaction
            //{
            //    OvertimeTransaction = transaction.OvertimeTransaction
            //};

            //return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> RestDayTransaction(ReportsDto model)
        {
            await HttpContextSettings(model);

            var transaction = await _reportsAppService.GetRestDayTransaction(model);
            var output = new GetReportsForRestDayTransaction
            {
                RestDayTransaction = transaction.RestDayTransaction
            };

            return View(output);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public async Task<IActionResult> ShiftTransaction(ReportsDto model)
        {
            await HttpContextSettings(model);

            var transaction = await _reportsAppService.GetShiftTransaction(model);
            var output = new GetReportsForShiftTransaction
            {
                ShiftTransaction = transaction.ShiftTransaction
            };

            return View(output);
        }
    }
}
