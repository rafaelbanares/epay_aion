﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.RestDayApprovals;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_RestDayApprovals)]
    public class RestDayApprovalsController : ePayControllerBase
    {
        private readonly IRestDayApprovalsAppService _restDayApprovalsAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public RestDayApprovalsController(IRestDayApprovalsAppService restDayApprovalsAppService,
            IApprovalsAppService approvalsAppService)
        {
            _restDayApprovalsAppService = restDayApprovalsAppService;
            _approvalsAppService = approvalsAppService;
        }

        public async Task<ActionResult> Index(bool type) =>
            View(await _approvalsAppService.GetApprovalFilters(type));

        [AbpMvcAuthorize(AppPermissions.Pages_RestDayApprovals_Admin)]
        public async Task<ActionResult> AdminApprovals() =>
            View(await _approvalsAppService.GetAdminApprovalFilters());

        public async Task<ActionResult> ApprovalRequests() =>
            View(await _approvalsAppService.GetAdminApprovalFilters());

        public async Task<PartialViewResult> ViewRestDayApprovalsModal(int id)
        {
            var viewDto = await _restDayApprovalsAppService.GetRestDayApprovalsForView(id);

            return PartialView("_ViewRestDayApprovalsModal", new RestDayApprovalsViewModel()
            {
                RestDayDetails = viewDto.RestDayDetails
            });
        }

        public async Task<PartialViewResult> ViewRestDayApprovalRequestsModal(int id)
        {
            var viewDto = await _restDayApprovalsAppService.GetRestDayApprovalsForView(id);

            return PartialView("_ViewRestDayApprovalRequestsModal", new RestDayApprovalsViewModel()
            {
                RestDayDetails = viewDto.RestDayDetails
            });
        }

        public PartialViewResult RemarksRestDayApprovalsModal(RemarksViewModel model) =>
            PartialView("_RemarksRestDayApprovalsModal", model);
    }
}