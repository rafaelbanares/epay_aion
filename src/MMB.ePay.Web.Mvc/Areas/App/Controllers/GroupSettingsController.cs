﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.HR.Dtos;
using MMB.ePay.Web.Areas.App.Models.GroupSettings;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_GroupSettings)]
    public class GroupSettingsController : ePayControllerBase
    {
        private readonly IGroupSettingsAppService _groupSettingsAppService;

        public GroupSettingsController(IGroupSettingsAppService groupSettingsAppService) =>
            _groupSettingsAppService = groupSettingsAppService;

        public ActionResult Index() =>
            View(new GroupSettingsViewModel());
        

        [AbpMvcAuthorize(AppPermissions.Pages_GroupSettings_Create, AppPermissions.Pages_GroupSettings_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var groupSettings = new CreateOrEditGroupSettingsDto();

            if (id != null)
            {
                var output = await _groupSettingsAppService.GetGroupSettingsForEdit(new EntityDto { Id = (int)id });
                groupSettings = output.GroupSettings;
            }

            var viewModel = new CreateOrEditGroupSettingsModalViewModel()
            {
                GroupSettings = groupSettings
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewGroupSettingsModal(int id)
        {
            var viewDto = await _groupSettingsAppService.GetGroupSettingsForView(id);
            var model = new GroupSettingsViewModel()
            {
                GroupSettings = viewDto.GroupSettings
            };

            return PartialView("_ViewGroupSettingsModal", model);
        }
    }
}