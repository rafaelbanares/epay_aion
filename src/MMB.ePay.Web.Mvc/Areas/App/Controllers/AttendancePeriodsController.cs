﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.HR;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.AttendancePeriods;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_AttendancePeriods)]
    public class AttendancePeriodsController : ePayControllerBase
    {
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;
        private readonly IFrequenciesAppService _frequenciesAppService;
        private readonly IHelperAppService _helperAppService;

        public AttendancePeriodsController(IAttendancePeriodsAppService attendancePeriodsAppService,
            IFrequenciesAppService frequenciesAppService,
            IHelperAppService helperAppService)
        {
            _attendancePeriodsAppService = attendancePeriodsAppService;
            _frequenciesAppService = frequenciesAppService;
            _helperAppService = helperAppService;
        }

        public async Task<ActionResult> Index()
        {
            var model = new AttendancePeriodsViewModel
            {
                FrequencyList = await _frequenciesAppService.GetFrequencyListForFilter(),
                YearList = await _attendancePeriodsAppService.GetYearListForFilter()
            };

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_AttendancePeriods_Create, AppPermissions.Pages_AttendancePeriods_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var attendancePeriods = new CreateOrEditAttendancePeriodsDto();

            if (id.HasValue)
            {
                var output = await _attendancePeriodsAppService.GetAttendancePeriodsForEdit(new EntityDto { Id = (int)id });
                attendancePeriods = output.AttendancePeriods;
            }

            var viewModel = new CreateOrEditAttendancePeriodsModalViewModel()
            {
                AttendancePeriods = attendancePeriods,
                AppYearList = _helperAppService.GetYearList(),
                AppMonthList = _helperAppService.GetMonthList(),
                FrequencyList = await _frequenciesAppService.GetFrequencyList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewAttendancePeriodsModal(int id)
        {
            var viewDto = await _attendancePeriodsAppService.GetAttendancePeriodsForView(id);
            var model = new AttendancePeriodsViewModel()
            {
                AttendancePeriods = viewDto.AttendancePeriods
            };

            return PartialView("_ViewAttendancePeriodsModal", model);
        }
    }
}