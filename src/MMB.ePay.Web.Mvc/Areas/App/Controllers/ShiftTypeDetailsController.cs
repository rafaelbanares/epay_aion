﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.ShiftTypeDetails;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_ShiftTypeDetails)]
    public class ShiftTypeDetailsController : ePayControllerBase
    {
        private readonly IShiftTypeDetailsAppService _shiftTypeDetailsAppService;

        public ShiftTypeDetailsController(IShiftTypeDetailsAppService shiftTypeDetailsAppService) =>
            _shiftTypeDetailsAppService = shiftTypeDetailsAppService;

        public ActionResult Index() =>
            View(new ShiftTypeDetailsViewModel());

        [AbpMvcAuthorize(AppPermissions.Pages_ShiftTypeDetails_Create, AppPermissions.Pages_ShiftTypeDetails_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id, int? shiftTypeId)
        {
            var shiftTypeDetails = new CreateOrEditShiftTypeDetailsDto();

            if (id.HasValue)
            {
                var output = await _shiftTypeDetailsAppService.GetShiftTypeDetailsForEdit(new EntityDto { Id = (int)id });
                shiftTypeDetails = output.ShiftTypeDetails;
            }

            var viewModel = new CreateOrEditShiftTypeDetailsModalViewModel()
            {
                ShiftTypeDetails = shiftTypeDetails
            };

            if (shiftTypeId != null)
                viewModel.ShiftTypeDetails.ShiftTypeId = (int)shiftTypeId;

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewShiftTypeDetailsModal(int id)
        {
            var viewDto = await _shiftTypeDetailsAppService.GetShiftTypeDetailsForView(id);
            var model = new ShiftTypeDetailsViewModel()
            {
                ShiftTypeDetails = viewDto.ShiftTypeDetails
            };

            return PartialView("_ViewShiftTypeDetailsModal", model);
        }
    }
}