﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.OfficialBusinessRequestDetails;
using MMB.ePay.Web.Controllers;
using System;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_OfficialBusinessRequestDetails)]
    public class OfficialBusinessRequestDetailsController : ePayControllerBase
    {
        private readonly IOfficialBusinessRequestDetailsAppService _officialBusinessRequestDetailsAppService;

        public OfficialBusinessRequestDetailsController(IOfficialBusinessRequestDetailsAppService officialBusinessRequestDetailsAppService) =>
            _officialBusinessRequestDetailsAppService = officialBusinessRequestDetailsAppService;

        public ActionResult Index() =>

            View(new OfficialBusinessRequestDetailsViewModel());

        [AbpMvcAuthorize(AppPermissions.Pages_OfficialBusinessRequestDetails_Create, AppPermissions.Pages_OfficialBusinessRequestDetails_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var obRequestDetails = new CreateOrEditOfficialBusinessRequestDetailsDto { OfficialBusinessDate = DateTime.Now };

            if (id.HasValue)
            {
                var output = await _officialBusinessRequestDetailsAppService.GetOfficialBusinessRequestDetailsForEdit(new EntityDto { Id = (int)id });
                obRequestDetails = output.OfficialBusinessRequestDetails;
            }

            var viewModel = new CreateOrEditOfficialBusinessRequestDetailsModalViewModel()
            {
                OfficialBusinessRequestDetails = obRequestDetails
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewOfficialBusinessRequestDetailsModal(int id)
        {
            var viewDto = await _officialBusinessRequestDetailsAppService.GetOfficialBusinessRequestDetailsForView(id);
            var model = new OfficialBusinessRequestDetailsViewModel()
            {
                OfficialBusinessRequestDetails = viewDto.OfficialBusinessRequestDetails
            };

            return PartialView("_ViewOfficialBusinessRequestDetailsModal", model);
        }
    }
}