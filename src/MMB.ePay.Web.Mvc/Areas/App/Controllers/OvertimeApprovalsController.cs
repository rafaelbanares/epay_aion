﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.OvertimeApprovals;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_OvertimeApprovals)]
    public class OvertimeApprovalsController : ePayControllerBase
    {
        private readonly IOvertimeApprovalsAppService _overtimeApprovalsAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public OvertimeApprovalsController(IOvertimeApprovalsAppService overtimeApprovalsAppService,
            IApprovalsAppService approvalsAppService)
        {
            _overtimeApprovalsAppService = overtimeApprovalsAppService;
            _approvalsAppService = approvalsAppService;
        }

        public async Task<ActionResult> Index(bool type) =>
            View(await _approvalsAppService.GetApprovalFilters(type));

        [AbpMvcAuthorize(AppPermissions.Pages_OvertimeApprovals_Admin)]
        public async Task<ActionResult> AdminApprovals() =>
            View(await _approvalsAppService.GetAdminApprovalFilters());

        public async Task<ActionResult> ApprovalRequests() =>
            View(await _approvalsAppService.GetAdminApprovalFilters());

        public async Task<PartialViewResult> ViewOvertimeApprovalsModal(int id)
        {
            var viewDto = await _overtimeApprovalsAppService.GetOvertimeApprovalsForView(id);

            return PartialView("_ViewOvertimeApprovalsModal", new OvertimeApprovalsViewModel()
            {
                OvertimeDetails = viewDto.OvertimeDetails
            });
        }

        public async Task<PartialViewResult> ViewOvertimeApprovalRequestsModal(int id)
        {
            var viewDto = await _overtimeApprovalsAppService.GetOvertimeApprovalsForView(id);

            return PartialView("_ViewOvertimeApprovalRequestsModal", new OvertimeApprovalsViewModel()
            {
                OvertimeDetails = viewDto.OvertimeDetails
            });
        }

        public PartialViewResult RemarksOvertimeApprovalsModal(RemarksViewModel model) =>
            PartialView("_RemarksOvertimeApprovalsModal", model);
    }
}