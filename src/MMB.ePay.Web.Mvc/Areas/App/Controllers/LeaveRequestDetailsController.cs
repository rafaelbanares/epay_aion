﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.LeaveRequestDetails;
using MMB.ePay.Web.Controllers;
using System;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_LeaveRequestDetails)]
    public class LeaveRequestDetailsController : ePayControllerBase
    {
        private readonly ILeaveRequestDetailsAppService _leaveRequestDetailsAppService;

        public LeaveRequestDetailsController(ILeaveRequestDetailsAppService leaveRequestDetailsAppService) =>
            _leaveRequestDetailsAppService = leaveRequestDetailsAppService;

        public ActionResult Index() =>
            View(new LeaveRequestDetailsViewModel());

        [AbpMvcAuthorize(AppPermissions.Pages_LeaveRequestDetails_Create, AppPermissions.Pages_LeaveRequestDetails_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var leaveRequestDetails = new CreateOrEditLeaveRequestDetailsDto { LeaveDate = DateTime.Now };

            if (id.HasValue)
            {
                var output = await _leaveRequestDetailsAppService.GetLeaveRequestDetailsForEdit(new EntityDto { Id = (int)id });
                leaveRequestDetails = output.LeaveRequestDetails;
            }

            var viewModel = new CreateOrEditLeaveRequestDetailsModalViewModel()
            {
                LeaveRequestDetails = leaveRequestDetails
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewLeaveRequestDetailsModal(int id)
        {
            var viewDto = await _leaveRequestDetailsAppService.GetLeaveRequestDetailsForView(id);
            var model = new LeaveRequestDetailsViewModel()
            {
                LeaveRequestDetails = viewDto.LeaveRequestDetails
            };

            return PartialView("_ViewLeaveRequestDetailsModal", model);
        }

    }
}