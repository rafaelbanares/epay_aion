﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.MultiTenancy;
using MMB.ePay.Timekeeping;
using MMB.ePay.Web.Areas.App.Models.EmailQueue;
using MMB.ePay.Web.Controllers;
using System;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_Host_EmailQueue)]
    public class EmailQueueController : ePayControllerBase
    {
        private readonly IEmailQueueAppService _emailQueueAppService;
        private readonly ITenantAppService _tenantAppService;

        public EmailQueueController(IEmailQueueAppService emailQueueAppService,
            ITenantAppService tenantAppService)
        {
            _emailQueueAppService = emailQueueAppService;
            _tenantAppService = tenantAppService;
        }

        public async Task<ActionResult> Index() =>
            View(new EmailQueueViewModel
            {
                TenantList = await _tenantAppService.GetTenantList()
            });

        public async Task<PartialViewResult> ViewEmailQueueModal(int id)
        {
            var viewDto = await _emailQueueAppService.GetEmailQueueForView(id);
            var model = new EmailQueueViewModel()
            {
                EmailQueue = viewDto.EmailQueue
            };

            return PartialView("_ViewEmailQueueModal", model);
        }

        public async Task<PartialViewResult> DeleteModal()
        {
            var startDate = await _emailQueueAppService.GetOldestNewEmailQueue();

            if (startDate == null)
                startDate = DateTime.Now;

            return PartialView("_DeleteModal", new DeleteEmailQueueModalViewModel()
            {
                StartDate = startDate,
                EndDate = DateTime.Now
            });
        }
            
    }
}