﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.ApproverOrders;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_ApproverOrders)]
    public class ApproverOrdersController : ePayControllerBase
    {
        private readonly IApproverOrdersAppService _approverOrdersAppService;

        public ApproverOrdersController(IApproverOrdersAppService approverOrdersAppService) =>
            _approverOrdersAppService = approverOrdersAppService;

        public ActionResult Index() =>
            View(new ApproverOrdersViewModel());

        [AbpMvcAuthorize(AppPermissions.Pages_ApproverOrders_Create, AppPermissions.Pages_ApproverOrders_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var approverOrders = new CreateOrEditApproverOrdersDto();

            if (id.HasValue)
            {
                var output = await _approverOrdersAppService.GetApproverOrdersForEdit(id.Value);
                approverOrders = output.ApproverOrders;
            }
            
            return PartialView("_CreateOrEditModal", new CreateOrEditApproverOrdersModalViewModel 
            { 
                ApproverOrders = approverOrders 
            });
        }

        public async Task<PartialViewResult> ViewApproverOrdersModal(int id)
        {
            var viewDto = await _approverOrdersAppService.GetApproverOrdersForView(id);
            var model = new ApproverOrdersViewModel()
            {
                ApproverOrders = viewDto.ApproverOrders
            };

            return PartialView("_ViewApproverOrdersModal", model);
        }
    }
}