﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.OvertimeTypes;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_OvertimeTypes)]
    public class OvertimeTypesController : ePayControllerBase
    {
        private readonly IOvertimeTypesAppService _overtimeTypesAppService;
        private readonly IHelperAppService _helperAppService;

        public OvertimeTypesController(IOvertimeTypesAppService overtimeTypesAppService,
            IHelperAppService helperAppService)
        {
            _overtimeTypesAppService = overtimeTypesAppService;
            _helperAppService = helperAppService;
        }

        public ActionResult Index() =>
            View(new OvertimeTypesViewModel());

        [AbpMvcAuthorize(AppPermissions.Pages_OvertimeTypes_Create, AppPermissions.Pages_OvertimeTypes_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var type = new CreateOrEditOvertimeTypesDto();

            if (id.HasValue)
            {
                var output = await _overtimeTypesAppService.GetOvertimeTypesForEdit(id.Value);
                type = output.OvertimeTypes;   
            }

            var viewModel = new CreateOrEditOvertimeTypesModalViewModel()
            {
                OvertimeTypes = type,
                SubTypeList = id.HasValue ? null : _helperAppService.GetSubTypeList(),
                OvertimeTypeOTCodeList = await _overtimeTypesAppService.GetOvertimeTypeOTCodeList(type.OTCode)
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewOvertimeTypesModal(int id)
        {
            var viewDto = await _overtimeTypesAppService.GetOvertimeTypesForView(id);
            var model = new OvertimeTypesViewModel()
            {
                OvertimeTypes = viewDto.OvertimeTypes
            };

            return PartialView("_ViewOvertimeTypesModal", model);
        }
    }
}