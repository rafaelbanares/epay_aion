﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.Locations;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Locations)]
    public class LocationsController : ePayControllerBase
    {
        private readonly ILocationsAppService _locationsAppService;

        public LocationsController(ILocationsAppService locationsAppService) =>
            _locationsAppService = locationsAppService;

        public IActionResult Index() =>
            View(new LocationsViewModel());

        [AbpMvcAuthorize(AppPermissions.Pages_Locations_Create, AppPermissions.Pages_Locations_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var locations = new CreateOrEditLocationsDto();

            if (id.HasValue)
            {
                var output = await _locationsAppService.GetLocationsForEdit(id.Value);
                locations = output.Locations;
            }

            var viewModel = new CreateOrEditLocationsModalViewModel
            {
                Locations = locations
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewLocationsModal(int id)
        {
            var viewDto = await _locationsAppService.GetLocationsForView(id);
            var model = new LocationsViewModel()
            {
                Locations = viewDto.Locations
            };

            return PartialView("_ViewLocationsModal", model);
        }
    }
}