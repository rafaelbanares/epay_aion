﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Web.Areas.App.Models.TKSettings;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_TKSettings)]
    public class TKSettingsController : ePayControllerBase
    {
        private readonly ITKSettingsAppService _tkSettingsAppService;

        public TKSettingsController(ITKSettingsAppService tkSettingsAppService) =>
            _tkSettingsAppService = tkSettingsAppService;

        [AbpMvcAuthorize(AppPermissions.Pages_TKSettings_Edit)]
        public async Task<ActionResult> Index() 
        {
            var output = await _tkSettingsAppService.GetTKSettingsForEdit();
            var viewModel = new TKSettingsViewModel
            {
                TKSettings = output.TKSettings
            };

            return View(viewModel);
        }
    }
}