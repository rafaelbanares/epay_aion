﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.HolidayTypes;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_HolidayTypes)]
    public class HolidayTypesController : ePayControllerBase
    {
        private readonly IHolidayTypesAppService _holidayTypesAppService;

        public HolidayTypesController(HolidayTypesAppService holidayTypesAppService) =>
            _holidayTypesAppService = holidayTypesAppService;

        public ActionResult Index() =>
            View(new HolidayTypesViewModel());

        [AbpMvcAuthorize(AppPermissions.Pages_HolidayTypes_Create, AppPermissions.Pages_HolidayTypes_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var holidayTypes = new CreateOrEditHolidayTypesDto();

            if (id.HasValue)
            {
                var output = await _holidayTypesAppService.GetHolidayTypesForEdit(new EntityDto { Id = (int)id });
                holidayTypes = output.HolidayTypes;
            }

            var viewModel = new CreateOrEditHolidayTypesModalViewModel()
            {
                HolidayTypes = holidayTypes
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewHolidayTypesModal(int id)
        {
            var viewDto = await _holidayTypesAppService.GetHolidayTypesForView(id);
            var model = new HolidayTypesViewModel()
            {
                HolidayTypes = viewDto.HolidayTypes
            };

            return PartialView("_ViewHolidayTypesModal", model);
        }
    }
}