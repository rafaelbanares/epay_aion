﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.AttendanceApprovals;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_AttendanceApprovals)]
    public class AttendanceApprovalsController : ePayControllerBase
    {
        private readonly IAttendanceApprovalsAppService _attendanceApprovalsAppService;
        private readonly IApprovalsAppService _approvalsAppService;
        private readonly ITKSettingsAppService _tKSettingsAppService;

        public AttendanceApprovalsController(IAttendanceApprovalsAppService attendanceApprovalsAppService,
            IApprovalsAppService approvalsAppService,
            ITKSettingsAppService tKSettingsAppService)
        {
            _attendanceApprovalsAppService = attendanceApprovalsAppService;
            _approvalsAppService = approvalsAppService;
            _tKSettingsAppService = tKSettingsAppService;
        }

        public async Task<ActionResult> Index(bool type) =>
            View(await _approvalsAppService.GetApprovalFilters(type));

        [AbpMvcAuthorize(AppPermissions.Pages_AttendanceApprovals_Admin)]
        public async Task<ActionResult> AdminApprovals() =>
            View(await _approvalsAppService.GetAdminApprovalFilters());

        public async Task<ActionResult> ApprovalRequests() =>
            View(await _approvalsAppService.GetAdminApprovalFilters());

        public async Task<PartialViewResult> ViewAttendanceApprovalsModal(int id)
        {
            var viewDto = await _attendanceApprovalsAppService.GetAttendanceApprovalsForView(id);

            return PartialView("_ViewAttendanceApprovalsModal", new AttendanceApprovalsViewModel() 
            { 
                AttendanceDetails = viewDto.AttendanceDetails,
                HasPMBreak = await _tKSettingsAppService.HasPMBreak()
            });
        }

        public async Task<PartialViewResult> ViewAttendanceApprovalRequestsModal(int id)
        {
            var viewDto = await _attendanceApprovalsAppService.GetAttendanceApprovalsForView(id);

            return PartialView("_ViewAttendanceApprovalRequestsModal", new AttendanceApprovalsViewModel() 
            { 
                AttendanceDetails = viewDto.AttendanceDetails 
            });
        }

        public PartialViewResult RemarksAttendanceApprovalsModal(RemarksViewModel model) =>
            PartialView("_RemarksAttendanceApprovalsModal", model);
    }
}