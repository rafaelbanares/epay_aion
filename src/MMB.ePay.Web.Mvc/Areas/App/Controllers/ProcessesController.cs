﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Web.Areas.App.Models.Hangfire;
using MMB.ePay.Web.Controllers;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Processes)]
    public class ProcessesController : ePayControllerBase
    {
        public ProcessesController()
        {

        }

        public IActionResult Index() =>
            View(new HangfireViewModel());
    }
}