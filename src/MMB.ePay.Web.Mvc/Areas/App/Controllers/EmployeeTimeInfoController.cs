﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.EmployeeTimeInfo;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_EmployeeTimeInfo)]
    public class EmployeeTimeInfoController : ePayControllerBase
    {
        private readonly IEmployeeTimeInfoAppService _employeeTimeInfoAppService;

        public EmployeeTimeInfoController(IEmployeeTimeInfoAppService employeeTimeInfoAppService) =>
            _employeeTimeInfoAppService = employeeTimeInfoAppService;

        public ActionResult Index() =>
            View(new EmployeeTimeInfoViewModel());

        [AbpMvcAuthorize(AppPermissions.Pages_EmployeeTimeInfo_Create, AppPermissions.Pages_EmployeeTimeInfo_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var employeeTimeInfo = new CreateOrEditEmployeeTimeInfoDto();

            if (id.HasValue)
            {
                var output = await _employeeTimeInfoAppService.GetEmployeeTimeInfoForEdit(new EntityDto { Id = (int)id });
                employeeTimeInfo = output.EmployeeTimeInfo;
            }

            var viewModel = new CreateOrEditEmployeeTimeInfoModalViewModel()
            {
                EmployeeTimeInfo = employeeTimeInfo
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewEmployeeTimeInfoModal(int id)
        {
            var viewDto = await _employeeTimeInfoAppService.GetEmployeeTimeInfoForView(id);
            var model = new EmployeeTimeInfoViewModel()
            {
                EmployeeTimeInfo = viewDto.EmployeeTimeInfo
            };

            return PartialView("_ViewEmployeeTimeInfoModal", model);
        }
    }
}