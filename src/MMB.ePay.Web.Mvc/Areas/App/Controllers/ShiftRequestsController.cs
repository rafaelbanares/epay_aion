﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.ShiftApprovals;
using MMB.ePay.Web.Areas.App.Models.ShiftRequests;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_ShiftRequests)]
    public class ShiftRequestsController : ePayControllerBase
    {
        private readonly IShiftRequestsAppService _shiftRequestsAppService;
        private readonly IShiftTypesAppService _shiftTypesAppService;
        private readonly IShiftReasonsAppService _shiftReasonsAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public ShiftRequestsController(IShiftRequestsAppService shiftRequestsAppService,
            IShiftTypesAppService shiftTypesAppService,
            IShiftReasonsAppService shiftReasonsAppService,
            IApprovalsAppService approvalsAppService)
        {
            _shiftRequestsAppService = shiftRequestsAppService;
            _shiftTypesAppService = shiftTypesAppService;
            _shiftReasonsAppService = shiftReasonsAppService;
            _approvalsAppService = approvalsAppService;
        }

        public async Task<ActionResult> Index() =>
            View(await _approvalsAppService.GetRequestsFilters());

        [AbpMvcAuthorize(AppPermissions.Pages_ShiftRequests_Create, AppPermissions.Pages_ShiftRequests_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var shiftRequests = new CreateOrEditShiftRequestsDto();

            if (id.HasValue)
            {
                var output = await _shiftRequestsAppService.GetShiftRequestsForEdit(id.Value);
                shiftRequests = output.ShiftRequests;
            }

            var viewModel = new CreateOrEditShiftRequestsModalViewModel()
            {
                ShiftRequests = shiftRequests,
                ShiftTypeList = await _shiftTypesAppService.GetShiftTypeList(),
                ShiftReasonList = await _shiftReasonsAppService.GetShiftReasonList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewShiftRequestsModal(int id)
        {
            var viewDto = await _shiftRequestsAppService.GetShiftRequestsForView(id);
            var model = new ShiftRequestsViewModel()
            {
                ShiftDetails = viewDto.ShiftDetails
            };

            return PartialView("_ViewShiftRequestsModal", model);
        }

        public PartialViewResult RemarksShiftRequestsModal(ShiftRemarksViewModel model) =>
            PartialView("_RemarksShiftRequestsModal", model);
    }
}