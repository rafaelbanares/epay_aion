﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.IO.Extensions;
using Abp.UI;
using Abp.Web.Models;
using jsreport.AspNetCore;
using jsreport.Types;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Dto;
using MMB.ePay.Storage;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.LeaveEarnings;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    public class LeaveEarningsController : ePayControllerBase
    {
        private readonly ILeaveEarningsAppService _leaveEarningsAppService;
        private readonly ILeaveTypesAppService _leaveTypeAppService;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IHelperAppService _helperAppService;

        public LeaveEarningsController(ILeaveEarningsAppService leaveEarningsAppService,
            ILeaveTypesAppService leaveTypesAppService,
            ITempFileCacheManager tempFileCacheManager,
            IHelperAppService helperAppService)
        {
            _leaveEarningsAppService = leaveEarningsAppService;
            _leaveTypeAppService = leaveTypesAppService;
            _tempFileCacheManager = tempFileCacheManager;
            _helperAppService = helperAppService;
        }

        [AbpMvcAuthorize(AppPermissions.Pages_LeaveEarnings)]
        public async Task<ActionResult> Index()
        {
            var model = new LeaveEarningsViewModel
            {
                AppYearList = await _leaveEarningsAppService.GetAppYearListForFilter(),
                AppMonthList = _helperAppService.GetMonthListForFilter()
            };

            return View(model);
        }

        [MiddlewareFilter(typeof(JsReportPipeline))]
        public ActionResult LeaveEarningsTemplate(LeaveEarningsViewModel model)
        {
            HttpContext.JsReportFeature()
                .Recipe(Recipe.HtmlToXlsx)
                .Configure((r) => r.Template.HtmlToXlsx = new HtmlToXlsx() { HtmlEngine = "chrome" });

            return View();
        }

        [AbpMvcAuthorize(AppPermissions.Pages_LeaveRequests)]
        public async Task<ActionResult> LeaveBalances() =>
            View(new LeaveBalancesViewModel
            {
                AppYearList = await _leaveEarningsAppService.GetAppYearListForFilter()
            });

        [AbpMvcAuthorize(AppPermissions.Pages_LeaveEarnings)]
        public async Task<ActionResult> AdminLeaveBalances() =>
            View(new LeaveBalancesViewModel
            {
                AppYearList = await _leaveEarningsAppService.GetAppYearListForFilter()
            });

        [AbpMvcAuthorize(AppPermissions.Pages_LeaveEarnings_Create, AppPermissions.Pages_LeaveEarnings_Edit)]
        public PartialViewResult UploadModal() =>
            PartialView("_UploadModal");

        public UploadLeaveEarningFileOutput Upload(FileDto input)
        {
            try
            {
                var leaveEarningFile = Request.Form.Files[0];

                //Check input
                if (leaveEarningFile == null)
                {
                    throw new UserFriendlyException(L("LeaveEarningFile_Find_Error"));
                }

                byte[] fileBytes;
                using (var stream = leaveEarningFile.OpenReadStream())
                {
                    fileBytes = stream.GetAllBytes();
                }

                _tempFileCacheManager.SetFile(input.FileToken, fileBytes);

                return new UploadLeaveEarningFileOutput
                {
                    FileToken = input.FileToken,
                    FileName = input.FileName,
                    FileType = input.FileType,
                };

                //using (var bmpImage = new Bitmap(new MemoryStream(fileBytes)))
                //{
                //    return new UploadProfilePictureOutput
                //    {
                //        FileToken = input.FileToken,
                //        FileName = input.FileName,
                //        FileType = input.FileType,
                //        Width = bmpImage.Width,
                //        Height = bmpImage.Height
                //    };
                //}
            }
            catch (UserFriendlyException ex)
            {
                return new UploadLeaveEarningFileOutput(new ErrorInfo(ex.Message));
            }
        }

        [AbpMvcAuthorize(AppPermissions.Pages_LeaveEarnings_Create, AppPermissions.Pages_LeaveEarnings_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var leaveEarnings = new CreateOrEditLeaveEarningsDto();

            if (id.HasValue)
            {
                var output = await _leaveEarningsAppService.GetLeaveEarningsForEdit(id.Value);
                leaveEarnings = output.LeaveEarnings;
            }

            var viewModel = new CreateOrEditLeaveEarningsModalViewModel()
            {
                LeaveEarnings = leaveEarnings,
                AppYearList = _helperAppService.GetYearList(),
                AppMonthList = _helperAppService.GetMonthList(),
                LeaveTypeList = await _leaveTypeAppService.GetLeaveTypeList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_LeaveEarnings_Create, AppPermissions.Pages_LeaveEarnings_Edit)]
        public async Task<PartialViewResult> CreateOrEditMultiModal(int? id)
        {
            var leaveEarnings = new CreateOrEditLeaveEarningsDto();

            if (id.HasValue)
            {
                var output = await _leaveEarningsAppService.GetLeaveEarningsForEdit(id.Value);
                leaveEarnings = output.LeaveEarnings;
            }

            var viewModel = new CreateOrEditLeaveEarningsModalViewModel()
            {
                LeaveEarnings = leaveEarnings,
                AppYearList = _helperAppService.GetYearList(),
                AppMonthList = _helperAppService.GetMonthList(),
                LeaveTypeList = await _leaveTypeAppService.GetLeaveTypeList()
            };

            return PartialView("_CreateOrEditMultiModal", viewModel);
        }

        public async Task<PartialViewResult> ViewLeaveEarningsModal(int id)
        {
            var viewDto = await _leaveEarningsAppService.GetLeaveEarningsForView(id);
            var model = new LeaveEarningsViewModel()
            {
                LeaveEarnings = viewDto.LeaveEarnings
            };

            return PartialView("_ViewLeaveEarningsModal", model);
        }

        public async Task<PartialViewResult> ViewLeaveBalancesModal(int id, int employeeId, int year)
        {
            var viewDto = await _leaveEarningsAppService.GetLeaveBalancesForView(id, employeeId, year);
            var model = new LeaveBalancesViewModel()
            {
                LeaveBalanceRequestList = viewDto.LeaveBalanceRequestList
            };

            return PartialView("_ViewLeaveBalancesModal", model);
        }
    }
}