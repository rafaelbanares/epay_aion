﻿using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Notifications;
using MMB.ePay.Web.Controllers;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize]
    public class NotificationsController : ePayControllerBase
    {
        private readonly INotificationAppService _notificationApp;

        public NotificationsController(INotificationAppService notificationApp) =>
            _notificationApp = notificationApp;

        public ActionResult Index() =>
            View();

        public async Task<PartialViewResult> SettingsModal() =>
            PartialView("_SettingsModal", await _notificationApp.GetNotificationSettings());
    }
}