﻿using Abp.AspNetCore.Mvc.Authorization;
using Abp.Runtime.Security;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Web.Areas.App.Models.Encryption;
using MMB.ePay.Web.Controllers;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_Encryption)]
    public class EncryptionController : ePayControllerBase
    {
        public EncryptionController()
        {

        }

        public ActionResult Index(EncryptionViewModel model)
        {
            return View(model);
        }

        [HttpPost]
        public ActionResult Encrypt(EncryptionViewModel model)
        {
            model.EncryptionOutput = SimpleStringCipher.Instance.Encrypt(model.EncryptionInput);

            return RedirectToAction("Index", model);
        }

        [HttpPost]
        public ActionResult Decrypt(EncryptionViewModel model)
        {
            model.DecryptionOutput = SimpleStringCipher.Instance.Decrypt(model.DecryptionInput);

            return RedirectToAction("Index", model);
        }
    }
}