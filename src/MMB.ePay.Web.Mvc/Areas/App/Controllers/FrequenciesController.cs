﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.Frequencies;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Frequencies)]
    public class FrequenciesController : ePayControllerBase
    {
        private readonly IFrequenciesAppService _frequenciesAppService;

        public FrequenciesController(IFrequenciesAppService frequenciesAppService) =>
            _frequenciesAppService = frequenciesAppService;

        public ActionResult Index() =>
            View(new FrequenciesViewModel());

        [AbpMvcAuthorize(AppPermissions.Pages_Frequencies_Create, AppPermissions.Pages_Frequencies_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var frequencies = new CreateOrEditFrequenciesDto();

            if (id.HasValue)
            {
                var output = await _frequenciesAppService.GetFrequenciesForEdit(new EntityDto { Id = (int)id });
                frequencies = output.Frequencies;
            }

            var viewModel = new CreateOrEditFrequenciesModalViewModel()
            {
                Frequencies = frequencies
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewFrequenciesModal(int id)
        {
            var viewDto = await _frequenciesAppService.GetFrequenciesForView(id);
            var model = new FrequenciesViewModel()
            {
                Frequencies = viewDto.Frequencies
            };

            return PartialView("_ViewFrequenciesModal", model);
        }
    }
}