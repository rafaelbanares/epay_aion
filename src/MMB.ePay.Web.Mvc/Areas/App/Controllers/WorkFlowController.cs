﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Web.Areas.App.Models.WorkFlow;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_WorkFlow)]
    public class WorkFlowController : ePayControllerBase
    {
        private readonly IWorkFlowAppService _workFlowAppService;

        public WorkFlowController(IWorkFlowAppService workFlowAppService)
        {
            _workFlowAppService = workFlowAppService;
        }

        public ActionResult Index()
        {
            return View(new WorkFlowViewModel());
        }

        //[AbpMvcAuthorize(AppPermissions.Pages_WorkFlow_Edit)]
        //public async Task<PartialViewResult> CreateOrEditModal(int id)
        //{
        //    var output = await _workFlowAppService.GetWorkFlowForEdit(id);

        //    return PartialView("_CreateOrEditModal", new CreateOrEditWorkFlowModalViewModel() 
        //    { 
        //        WorkFlow = output.WorkFlow 
        //    });
        //}

        public async Task<PartialViewResult> ViewWorkFlowModal(int id)
        {
            var viewDto = await _workFlowAppService.GetWorkFlowForView(id);
            var model = new WorkFlowViewModel()
            {
                WorkFlow = viewDto.WorkFlow
            };

            return PartialView("_ViewWorkFlowModal", model);
        }
    }
}