﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.AttendanceApprovals;
using MMB.ePay.Web.Areas.App.Models.AttendanceRequests;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_AttendanceRequests)]
    public class AttendanceRequestsController : ePayControllerBase
    {
        private readonly IAttendanceRequestsAppService _attendanceRequestsAppService;
        private readonly IAttendanceReasonsAppService _attendanceReasonsAppService;
        private readonly IApprovalsAppService _approvalsAppService;
        private readonly ITKSettingsAppService _tKSettingsAppService;

        public AttendanceRequestsController(IAttendanceRequestsAppService attendanceRequestsAppService,
            IAttendanceReasonsAppService attendanceReasonsAppService,
            IApprovalsAppService approvalsAppService,
            ITKSettingsAppService tKSettingsAppService)
        {
            _attendanceRequestsAppService = attendanceRequestsAppService;
            _attendanceReasonsAppService = attendanceReasonsAppService;
            _approvalsAppService = approvalsAppService;
            _tKSettingsAppService = tKSettingsAppService;
        }

        public async Task<ActionResult> Index()
        {
            var model = await _approvalsAppService.GetRequestsFilters();
            model.HasPMBreak = await _tKSettingsAppService.HasPMBreak();

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_AttendanceRequests_Create, AppPermissions.Pages_AttendanceRequests_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id, int? attendanceId)
        {
            var output = new GetAttendanceRequestsForEditOutput();

            if (id.HasValue)
                output = await _attendanceRequestsAppService
                    .GetAttendanceRequestsForEdit(id.Value);

            else if (attendanceId != null)
                output = await _attendanceRequestsAppService
                    .GetAttendanceRequestsForCreate(attendanceId.Value);

            var viewModel = new CreateOrEditAttendanceRequestsModalViewModel()
            {
                AttendanceRequests = output.AttendanceRequests ?? new CreateOrEditAttendanceRequestsDto(),
                AttendanceReasonList = await _attendanceReasonsAppService.GetAttendanceReasonList(),
                HasPMBreak = await _tKSettingsAppService.HasPMBreak()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewAttendanceRequestsModal(int id)
        {
            var viewDto = await _attendanceRequestsAppService.GetAttendanceRequestsForView(id);

            var model = new AttendanceRequestsViewModel 
            { 
                AttendanceDetails = viewDto.AttendanceDetails,
                HasPMBreak = await _tKSettingsAppService.HasPMBreak()
            };

            return PartialView("_ViewAttendanceRequestsModal", model);
        }

        public PartialViewResult RemarksAttendanceRequestsModal(AttendanceRemarksViewModel model) =>
            PartialView("_RemarksAttendanceRequestsModal", model);
    }
}