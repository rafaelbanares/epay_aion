﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.OvertimeApprovals;
using MMB.ePay.Web.Areas.App.Models.OvertimeRequests;
using MMB.ePay.Web.Controllers;
using System;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_OvertimeRequests)]
    public class OvertimeRequestsController : ePayControllerBase
    {
        private readonly IOvertimeRequestsAppService _overtimeRequestsAppService;
        private readonly IOvertimeReasonsAppService _overtimeReasonsAppService;
        private readonly IApprovalsAppService _approvalsAppService;

        public OvertimeRequestsController(IOvertimeRequestsAppService overtimeRequestsAppService,
            IOvertimeReasonsAppService overtimeReasonsAppService,
            IApprovalsAppService approvalsAppService)
        {
            _overtimeRequestsAppService = overtimeRequestsAppService;
            _overtimeReasonsAppService = overtimeReasonsAppService;
            _approvalsAppService = approvalsAppService;
        }

        public async Task<ActionResult> Index() =>
            View(await _approvalsAppService.GetRequestsFilters());

        [AbpMvcAuthorize(AppPermissions.Pages_OvertimeRequests_Create, AppPermissions.Pages_OvertimeRequests_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id, DateTime? date)
        {
            var overtimeRequests = new CreateOrEditOvertimeRequestsDto { OvertimeDate = date };

            if (id.HasValue)
            {
                var output = await _overtimeRequestsAppService.GetOvertimeRequestsForEdit(id.Value);
                overtimeRequests = output.OvertimeRequests;
            }

            var viewModel = new CreateOrEditOvertimeRequestsModalViewModel()
            {
                OvertimeRequests = overtimeRequests,
                OvertimeReasonList = await _overtimeReasonsAppService.GetOvertimeReasonList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewOvertimeRequestsModal(int id)
        {
            var viewDto = await _overtimeRequestsAppService.GetOvertimeRequestsForView(id);
            var model = new OvertimeRequestsViewModel()
            {
                OvertimeDetails = viewDto.OvertimeDetails
            };

            return PartialView("_ViewOvertimeRequestsModal", model);
        }

        public PartialViewResult RemarksOvertimeRequestsModal(OvertimeRemarksViewModel model) =>
            PartialView("_RemarksOvertimeRequestsModal", model);

    }
}