﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.HR.Dtos;
using MMB.ePay.Timekeeping;
using MMB.ePay.Web.Areas.App.Models.Companies;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Companies)]
    public class CompaniesController : ePayControllerBase
    {
        private readonly ICompaniesAppService _companiesAppService;

        public CompaniesController(ICompaniesAppService companiesAppService) =>
            _companiesAppService = companiesAppService;

        public ActionResult Index() =>
            View(new CompaniesViewModel());

        [AbpMvcAuthorize(AppPermissions.Pages_Companies_Create, AppPermissions.Pages_Companies_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var companies = new CreateOrEditCompaniesDto();

            if (id.HasValue)
            {
                var output = await _companiesAppService.GetCompaniesForEdit(new EntityDto { Id = (int)id });
                companies = output.Companies;
            }

            var viewModel = new CreateOrEditCompaniesModalViewModel()
            {
                Companies = companies
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewCompaniesModal(int id)
        {
            var viewDto = await _companiesAppService.GetCompaniesForView(id);
            var model = new CompaniesViewModel()
            {
                Companies = viewDto.Companies
            };

            return PartialView("_ViewCompaniesModal", model);
        }
    }
}