﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Abp.Domain.Uow;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Processor;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.Attendances;
using MMB.ePay.Web.Controllers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Attendances)]
    public class AttendancesController : ePayControllerBase
    {
        private readonly IAttendancesAppService _attendancesAppService;
        private readonly IFrequenciesAppService _frequenciesAppService;
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;
        private readonly IProcessorAppService _processorAppService;
        private readonly ILeaveRequestsAppService _leaveRequestsAppService;
        private readonly ITKSettingsAppService _tKSettingsAppService;

        public AttendancesController(IAttendancesAppService attendancesAppService,
            IFrequenciesAppService frequenciesAppService,
            IAttendancePeriodsAppService attendancePeriodsAppService,
            IProcessorAppService processorAppService,
            ILeaveRequestsAppService leaveRequestsAppService,
            ITKSettingsAppService tKSettingsAppService)
        {
            _attendancesAppService = attendancesAppService;
            _frequenciesAppService = frequenciesAppService;
            _attendancePeriodsAppService = attendancePeriodsAppService;
            _processorAppService = processorAppService;
            _leaveRequestsAppService = leaveRequestsAppService;
            _tKSettingsAppService = tKSettingsAppService;
        }

        public async Task<ActionResult> Index()
        {
            var model = new AttendancesViewModel
            {
                HasPMBreak = await _tKSettingsAppService.HasPMBreak(),
                YearList = await _attendancePeriodsAppService.GetYearListForFilter(),
                AttendancePeriodList = await _attendancePeriodsAppService
                    .GetAttendancePeriodListByYearForTimesheet(DateTime.Now.Year, null, null)
            };

            return View(model);
        }

        public async Task<ActionResult> Upload()
        {
            var frequencyList = await _frequenciesAppService.GetFrequencyListForFilter();
            var frequencyId = int.Parse(frequencyList.FirstOrDefault(e => e.Selected).Value);

            var model = new AttendancesUploadViewModel
            {
                FrequencyList = frequencyList,
                AttendancesUpload = new AttendancesUploadDto { FrequencyId = frequencyId }
            };

            return View(model);
        }

        [HttpPost]
        [DisableRequestSizeLimit]
        [UnitOfWork]
        public async Task<ActionResult> Upload(AttendancesUploadViewModel model)
        {
            model.FrequencyList = await _frequenciesAppService.GetFrequencyListForFilter();

            if (model.AttendancesUpload.File != null)
            {
                var error = Validate(model.AttendancesUpload.File);
                if (error != string.Empty)
                {
                    var output = new AttendancesUploadDto
                    {
                        CustomError = error
                    };

                    return View(output);
                }

                CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
                var staging = await _attendancesAppService.UploadStaging(model.AttendancesUpload);

                if (staging.CustomError != string.Empty)
                    return View(model);

                await UnitOfWorkManager.Current.SaveChangesAsync();
                model.AttendancesUpload = staging;

                await _processorAppService.Process(
                    AbpSession.TenantId.Value,
                    await _attendancePeriodsAppService.GetFirstUnpostedPeriodId(
                        model.AttendancesUpload.FrequencyId),
                    null);
            }
            else
                model.AttendancesUpload.Count = 0;

            return View(model);
        }

        private string Validate(IFormFile file)
        {
            if (file == null || file?.Length == 0)
                return "File is required.";

            if (file?.Length > 20971520) //20MB
                return L("Warn_SizeLimit", "20MB");

            return string.Empty;
        }

        public async Task<IActionResult> Process()
        {
            var frequencyList = await _frequenciesAppService.GetFrequencyListForFilter();
            var frequencyId = int.Parse(frequencyList.FirstOrDefault(e => e.Selected).Value);

            var model = new AttendancesProcessViewModel
            {
                FrequencyList = frequencyList,
                FrequencyFilterId = frequencyId,
                AttendancePeriodList = await _attendancePeriodsAppService.GetUnpostedAttendancePeriodList(frequencyId),
                StatusMessage = string.Empty
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Process(AttendancesProcessViewModel model)
        {
            model.FrequencyList = await _frequenciesAppService.GetFrequencyListForFilter();
            model.AttendancePeriodList = await _attendancePeriodsAppService.GetUnpostedAttendancePeriodList(model.FrequencyFilterId);

            if (model.AttendancePeriodId == 0)
                model.StatusMessage = "The Attendance Period field is required.";
            else
            {
                model.StatusMessage = "Process complete.";
                await _processorAppService.Process(AbpSession.TenantId.Value, model.AttendancePeriodId, model.EmployeeId);
            }

            return View(model);
        }

        public async Task<IActionResult> Post()
        {
            var frequencyList = await _frequenciesAppService.GetFrequencyListForFilter();
            var frequencyId = int.Parse(frequencyList.FirstOrDefault(e => e.Selected).Value);
            //var pendingLeaveRequestCount = await _leaveRequestsAppService.GetPendingLeaveRequestCount();

            var model = new AttendancesPostViewModel
            {
                FrequencyList = frequencyList,
                FrequencyFilterId = frequencyId,
                Posted = false,
                IsForced = false
            };

            return View(model);
        }

        [HttpPost]
        [UnitOfWork]
        public async Task<IActionResult> Post(AttendancesPostViewModel model)
        {
            var frequencyList = await _frequenciesAppService.GetFrequencyListForFilter();
            var periodId = await _attendancePeriodsAppService.GetFirstUnpostedPeriodId(model.FrequencyFilterId);
            var dateRange = await _attendancePeriodsAppService.GetUnpostedDateRangeFromFrequencyId(model.FrequencyFilterId);

            model.FrequencyList = frequencyList;
            model.InvalidAttendances = await _attendancesAppService.GetInvalidAttendanceList(periodId);
            model.PendingLeaveRequestCount = await _leaveRequestsAppService.GetPendingLeaveRequestCount(dateRange);

            if ((model.InvalidAttendances.Count == 0 && model.PendingLeaveRequestCount == 0) || model.IsForced)
            {
                CurrentUnitOfWork.SetTenantId(AbpSession.TenantId);
                await _processorAppService.Process(AbpSession.TenantId.Value, periodId, null);
                await UnitOfWorkManager.Current.SaveChangesAsync();

                model.Posted = await _processorAppService.Post(AbpSession.TenantId.Value, periodId, model.IsForced);
            }
            else
                model.Posted = false;

            return View(model);
        }

        [AbpMvcAuthorize(AppPermissions.Pages_Attendances_Create, AppPermissions.Pages_Attendances_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var attendances = new CreateOrEditAttendancesDto();

            if (id.HasValue)
            {
                var output = await _attendancesAppService.GetAttendancesForEdit(new EntityDto { Id = (int)id });
                output.Attendances = attendances;
            }

            var viewModel = new CreateOrEditAttendancesModalViewModel()
            {
                Attendances = attendances
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewAttendancesModal(int id, bool isPosted)
        {
            var viewDto = await _attendancesAppService.GetAttendancesForView(id, isPosted);

            var model = new AttendancesViewModel()
            {
                Attendances = viewDto.Attendances,
                HasPMBreak = await _tKSettingsAppService.HasPMBreak()
            };

            return PartialView("_ViewAttendancesModal", model);
        }
    }
}