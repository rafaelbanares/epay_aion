﻿using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Authorization;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using MMB.ePay.Web.Areas.App.Models.Holidays;
using MMB.ePay.Web.Controllers;
using System.Threading.Tasks;

namespace MMB.ePay.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Holidays)]
    public class HolidaysController : ePayControllerBase
    {
        private readonly IHolidaysAppService _holidaysAppService;
        private readonly IHolidayTypesAppService _holidayTypesAppService;
        private readonly ILocationsAppService _locationsAppService;

        public HolidaysController(IHolidaysAppService holidaysAppService,
            IHolidayTypesAppService holidayTypesAppService,
            ILocationsAppService locationsAppService)
        {
            _holidaysAppService = holidaysAppService;
            _holidayTypesAppService = holidayTypesAppService;
            _locationsAppService = locationsAppService;
        }

        public async Task<ActionResult> Index() =>

            View(new HolidaysViewModel
            {
                YearList = await _holidaysAppService.GetYearListForFilter()
            });

        [AbpMvcAuthorize(AppPermissions.Pages_Holidays_Create, AppPermissions.Pages_Holidays_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(int? id)
        {
            var holidays = new CreateOrEditHolidaysDto();

            if (id.HasValue)
            {
                var output = await _holidaysAppService.GetHolidaysForEdit(new EntityDto { Id = (int)id });
                holidays = output.Holidays;
            }

            var viewModel = new CreateOrEditHolidaysModalViewModel()
            {
                Holidays = holidays,
                HolidayTypeList = await _holidayTypesAppService.GetHolidayTypeList(),
                LocationList = await _locationsAppService.GetLocationList()
            };

            return PartialView("_CreateOrEditModal", viewModel);
        }

        public async Task<PartialViewResult> ViewHolidaysModal(int id)
        {
            var viewDto = await _holidaysAppService.GetHolidaysForView(id);
            var model = new HolidaysViewModel()
            {
                Holidays = viewDto.Holidays
            };

            return PartialView("_ViewHolidaysModal", model);
        }
    }
}