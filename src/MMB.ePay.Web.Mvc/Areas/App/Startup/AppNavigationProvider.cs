﻿using Abp.Application.Navigation;
using Abp.Authorization;
using Abp.Localization;
using MMB.ePay.Authorization;

namespace MMB.ePay.Web.Areas.App.Startup
{
    public class AppNavigationProvider : NavigationProvider
    {
        public const string MenuName = "App";

        public override void SetNavigation(INavigationProviderContext context)
        {
            var menu = context.Manager.Menus[MenuName] = new MenuDefinition(MenuName, new FixedLocalizableString("Main Menu"));

            menu
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Tenant.Home,
                        L("Dashboard"),
                        url: "App/Welcome",
                        icon: "fas fa-layer-group"
                    )
                )
                //.AddItem(new MenuItemDefinition(
                //        AppPageNames.Tenant.Dashboard,
                //        L("Dashboard"),
                //        url: "App/TenantDashboard",
                //        icon: "fas fa-layer-group",
                //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Tenant_Dashboard)
                //    )
                //)
                //.AddItem(new MenuItemDefinition(
                //    AppPageNames.Host.Dashboard,
                //    L("Dashboard"),
                //    url: "App/HostDashboard",
                //    icon: "fas fa-layer-group",
                //    permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Host_Dashboard))
                //)
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Tenant.AdminAttendances,
                        L("AdminTimesheet"),
                        url: "App/AdminAttendances",
                        icon: "flaticon-calendar-with-a-clock-time-tools",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_AdminAttendances)
                    )
                )
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Tenant.Attendances,
                        L("Timesheet"),
                        url: "App/Attendances",
                        icon: "flaticon-calendar-with-a-clock-time-tools",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Attendances)
                    )
                )
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Tenant.AdminLeaveBalances,
                        L("AdminLeaveBalance"),
                        url: "App/LeaveEarnings/AdminLeaveBalances",
                        icon: "flaticon-edit-1",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_LeaveEarnings)
                    )
                )
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Tenant.Reports,
                        L("Reports"),
                        url: "App/Reports",
                        icon: "flaticon2-line-chart",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Reports)
                    )
                )

                .AddItem(new MenuItemDefinition(
                        AppPageNames.Tenant.Requests,
                        L("MyRequests"),
                        icon: "flaticon2-pen"
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.AttendanceRequests,
                            L("Attendance"),
                            url: "App/AttendanceRequests",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_AttendanceRequests)
                        )
                    //).AddItem(new MenuItemDefinition(
                    //        AppPageNames.Tenant.ExcuseRequests,
                    //        L("Excuse"),
                    //        url: "App/ExcuseRequests",
                    //        icon: "flaticon2-checking",
                    //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_ExcuseRequests)
                    //    )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.LeaveRequests,
                            L("Leave"),
                            url: "App/LeaveRequests",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_LeaveRequests)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.OfficialBusinessRequests,
                            L("OfficialBusiness"),
                            url: "App/OfficialBusinessRequests",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_OfficialBusinessRequests)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.OvertimeRequests,
                            L("Overtime"),
                            url: "App/OvertimeRequests",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_OvertimeRequests)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.RestDayRequests,
                            L("RestDay"),
                            url: "App/RestDayRequests",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_RestDayRequests)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.ShiftRequests,
                            L("Shift"),
                            url: "App/ShiftRequests",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_ShiftRequests)
                        )
                    )
                )
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Tenant.Requests,
                        L("MyApprovals"),
                        icon: "flaticon2-checkmark"
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.AttendanceApprovals,
                            L("Attendance"),
                            url: "App/AttendanceApprovals",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_AttendanceApprovals)
                        )
                    //).AddItem(new MenuItemDefinition(
                    //        AppPageNames.Tenant.ExcuseApprovals,
                    //        L("Excuse"),
                    //        url: "App/ExcuseApprovals",
                    //        icon: "flaticon2-checking",
                    //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_ExcuseApprovals)
                    //    )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.LeaveApprovals,
                            L("Leave"),
                            url: "App/LeaveApprovals",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_LeaveApprovals)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.OfficialBusinessApprovals,
                            L("OfficialBusiness"),
                            url: "App/OfficialBusinessApprovals",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_OfficialBusinessApprovals)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.OvertimeApprovals,
                            L("Overtime"),
                            url: "App/OvertimeApprovals",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_OvertimeApprovals)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.RestDayApprovals,
                            L("RestDay"),
                            url: "App/RestDayApprovals",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_RestDayApprovals)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.ShiftApprovals,
                            L("Shift"),
                            url: "App/ShiftApprovals",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_ShiftApprovals)
                        )
                    )
                )

                .AddItem(new MenuItemDefinition(
                        AppPageNames.Tenant.AdminApprovals,
                        L("AdminApprovals"),
                        icon: "flaticon2-checkmark"
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.AttendanceAdminApprovals,
                            L("Attendance"),
                            url: "App/AttendanceApprovals/AdminApprovals",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_AttendanceApprovals_Admin)
                        )
                    //).AddItem(new MenuItemDefinition(
                    //        AppPageNames.Tenant.ExcuseAdminApprovals,
                    //        L("Excuse"),
                    //        url: "App/ExcuseApprovals/AdminApprovals",
                    //        icon: "flaticon2-checking",
                    //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_ExcuseApprovals_Admin)
                    //    )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.LeaveAdminApprovals,
                            L("Leave"),
                            url: "App/LeaveApprovals/AdminApprovals",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_LeaveApprovals_Admin)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.OfficialBusinessAdminApprovals,
                            L("OfficialBusiness"),
                            url: "App/OfficialBusinessApprovals/AdminApprovals",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_OfficialBusinessApprovals_Admin)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.OvertimeAdminApprovals,
                            L("Overtime"),
                            url: "App/OvertimeApprovals/AdminApprovals",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_OvertimeApprovals_Admin)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.RestDayAdminApprovals,
                            L("RestDay"),
                            url: "App/RestDayApprovals/AdminApprovals",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_RestDayApprovals_Admin)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.ShiftAdminApprovals,
                            L("Shift"),
                            url: "App/ShiftApprovals/AdminApprovals",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_ShiftApprovals_Admin)
                        )
                    )
                )

                .AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.Attendance,
                            L("Attendance"),
                            icon: "flaticon2-calendar-9",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Attendances)
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.Upload,
                            L("Upload"),
                            url: "App/Attendances/Upload",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Attendances_Upload)
                        )
                    //).AddItem(new MenuItemDefinition(
                    //        AppPageNames.Tenant.Attendances,
                    //        L("EmployeeLogs"),
                    //        url: "App/Attendances/EmployeeLogs",
                    //        icon: "flaticon-download-1",
                    //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Attendances_EmployeeLogs)
                    //    )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.Process,
                            L("Process"),
                            url: "App/Attendances/Process",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Attendances_Process)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.Post,
                            L("Post"),
                            url: "App/Attendances/Post",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Attendances_Post)
                        )
                    //).AddItem(new MenuItemDefinition(
                    //        AppPageNames.Tenant.Attendances,
                    //        L("Summarize"),
                    //        url: "App/Attendances/Summarize",
                    //        icon: "flaticon-interface-8",
                    //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Attendances_Summarize)
                    //    )
                    )
                )
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Administration,
                        L("Administration"),
                        icon: "flaticon-interface-8"

                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.LeaveTypes,
                            L("LeaveTypes"),
                            url: "App/LeaveTypes",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_LeaveTypes)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.ShiftTypes,
                            L("ShiftTypes"),
                            url: "App/ShiftTypes",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_ShiftTypes)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.OvertimeTypes,
                            L("OvertimeTypes"),
                            url: "App/OvertimeTypes",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_OvertimeTypes)
                        )
                    //).AddItem(new MenuItemDefinition(
                    //        AppPageNames.Tenant.TimesheetTypes,
                    //        L("TimesheetTypes"),
                    //        url: "App/TimesheetTypes",
                    //        icon: "flaticon2-calendar-4",
                    //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_TimesheetTypes)
                    //    )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.HolidayTypes,
                            L("HolidayTypes"),
                            url: "App/HolidayTypes",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_HolidayTypes)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.Holidays,
                            L("Holidays"),
                            url: "App/Holidays",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Holidays)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.RawTimeLogs,
                            L("RawTimeLogs"),
                            url: "App/RawTimeLogs",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_RawTimeLogs)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.Locations,
                            L("Locations"),
                            url: "App/Locations",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Locations)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.OrganizationUnits,
                            L("OrganizationUnits"),
                            url: "App/OrganizationUnits",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_OrganizationUnits)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.Employees,
                            L("Employees"),
                            url: "App/Employees",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Employees)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.LeaveEarnings,
                            L("LeaveEarnings"),
                            url: "App/LeaveEarnings",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_LeaveEarnings)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.EmployeeApprovers,
                            L("EmployeeApprovers"),
                            url: "App/EmployeeApprovers",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_EmployeeApprovers)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.ApproverOrders,
                            L("ApproverOrders"),
                            url: "App/ApproverOrders",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_ApproverOrders)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.AttendancePeriods,
                            L("AttendancePeriods"),
                            url: "App/AttendancePeriods",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_AttendancePeriods)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.Roles,
                            L("Roles"),
                            url: "App/Roles",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Roles)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.Users,
                            L("Users"),
                            url: "App/Users",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Users)
                        )
                    //).AddItem(new MenuItemDefinition(
                    //        AppPageNames.Common.Languages,
                    //        L("Languages"),
                    //        url: "App/Languages",
                    //        icon: "flaticon-tabs",
                    //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Languages)
                    //    )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.AuditLogs,
                            L("AuditLogs"),
                            url: "App/AuditLogs",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_AuditLogs)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Host.Maintenance,
                            L("Maintenance"),
                            url: "App/Maintenance",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Host_Maintenance)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Host.EmailQueue,
                            L("EmailQueue"),
                            url: "App/EmailQueue",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Host_EmailQueue)
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Host.Hangfire,
                            L("RecurringJobs"),
                            url: "App/RecurringJobs",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_HangfireDashboard)
                        )
                    //).AddItem(new MenuItemDefinition(
                    //        AppPageNames.Tenant.SubscriptionManagement,
                    //        L("Subscription"),
                    //        url: "App/SubscriptionManagement",
                    //        icon: "flaticon-refresh",
                    //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement)
                    //    )
                    //).AddItem(new MenuItemDefinition(
                    //        AppPageNames.Common.UiCustomization,
                    //        L("VisualSettings"),
                    //        url: "App/UiCustomization",
                    //        icon: "flaticon-medical",
                    //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_UiCustomization)
                    //    )
                    //).AddItem(new MenuItemDefinition(
                    //        AppPageNames.Common.WebhookSubscriptions,
                    //        L("WebhookSubscriptions"),
                    //        url: "App/WebhookSubscription",
                    //        icon: "flaticon2-world",
                    //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_WebhookSubscription)
                    //    )
                    //).AddItem(new MenuItemDefinition(
                    //        AppPageNames.Common.DynamicProperties,
                    //        L("DynamicProperties"),
                    //        url: "App/DynamicProperty",
                    //        icon: "flaticon-interface-8",
                    //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_DynamicProperties)
                    //    )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Host.Settings,
                            L("HostSettings"),
                            url: "App/HostSettings",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Host_Settings)
                        )
                    )
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Tenant.TKSettings,
                        L("Settings"),
                        url: "App/TKSettings",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_TKSettings)
                    )
                )
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Tenant.Settings,
                        L("AdminSettings"),
                        url: "App/Settings",
                        icon: "flaticon2-hexagonal",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Tenant_Settings)
                    )
                )
                .AddItem(new MenuItemDefinition(
                            AppPageNames.Common.Companies,
                            L("Companies"),
                            url: "App/Companies",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Companies)
                        )
                )
                .AddItem(new MenuItemDefinition(
                            AppPageNames.Common.WorkFlow,
                            L("WorkFlow"),
                            url: "App/WorkFlow",
                            icon: "flaticon2-hexagonal",
                            permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_WorkFlow)
                        )
                )
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Host.Tenants,
                        L("Tenants"),
                        url: "App/Tenants",
                        icon: "flaticon-settings",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Tenants)
                    )
                )

                //.AddItem(new MenuItemDefinition(
                //        AppPageNames.Tenant.Processes,
                //        L("Processes"),
                //        url: "App/Processes",
                //        icon: "flaticon-settings",
                //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Processes)
                //    )
                //)
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Host.SpeedTest,
                        L("SpeedTest"),
                        url: "App/SpeedTest",
                        icon: "flaticon-settings",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_SpeedTest)
                    )
                )
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Host.Hangfire,
                        L("Hangfire"),
                        url: "App/Hangfire",
                        icon: "flaticon-settings",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_HangfireDashboard)
                    )
                )
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Host.Encryption,
                        L("Encryption"),
                        url: "App/Encryption",
                        icon: "flaticon-settings",
                        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Administration_Encryption)
                    )
                )

                //).AddItem(new MenuItemDefinition(
                //        AppPageNames.Common.DemoUiComponents,
                //        L("DemoUiComponents"),
                //        url: "App/DemoUiComponents",
                //        icon: "flaticon-shapes",
                //        permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_DemoUiComponents)
                //    )
                );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, ePayConsts.LocalizationSourceName);
        }
    }
}