﻿namespace MMB.ePay.Web.Areas.App.Startup
{
    public class AppPageNames
    {
        public static class Common
        {
            public const string WorkFlow = "Timekeeping.WorkFlow";
            public const string Transactions = "Transactions";
            public const string Maintenance = "Maintenance";
            public const string Types = "Types";

            public const string EmploymentTypes = "HR.EmploymentTypes";
            public const string GroupSettings = "HR.GroupSettings";

            public const string Companies = "HR.Companies";
            public const string Employees = "HR.Employees";

            public const string Administration = "Administration";
            public const string Roles = "Administration.Roles";
            public const string Users = "Administration.Users";
            public const string AuditLogs = "Administration.AuditLogs";
            public const string OrganizationUnits = "Administration.OrganizationUnits";
            public const string Languages = "Administration.Languages";
            public const string DemoUiComponents = "Administration.DemoUiComponents";
            public const string UiCustomization = "Administration.UiCustomization";
            public const string WebhookSubscriptions = "Administration.WebhookSubscriptions";
            public const string DynamicProperties = "Administration.DynamicProperties";
            public const string DynamicEntityProperties = "Administration.DynamicEntityProperties";
        }

        public static class Host
        {
            public const string Tenants = "Tenants";
            public const string Editions = "Editions";
            public const string Maintenance = "Administration.Maintenance";
            public const string EmailQueue = "Administration.EmailQueue";
            public const string Settings = "Administration.Settings.Host";
            public const string Dashboard = "Dashboard";
            public const string SpeedTest = "Administration.SpeedTest";
            public const string Hangfire = "Administration.Hangfire";
            public const string Encryption = "Administration.Encryption";
            public const string RecurringJobs = "Administration.RecurringJobs";
        }

        public static class Tenant
        {
            public const string Home = "Timekeeping.Home";
            public const string Announcement = "Timekeeping.Announcement";

            public const string PostedAttendances = "Timekeeping.PostedAttendances";
            public const string Requests = "Timekeeping.Requests";
            public const string Reports = "Timekeeping.Reports";
            public const string Locations = "Timekeeping.Locations";
            public const string Holidays = "Timekeeping.Holidays";
            public const string HolidayTypes = "Timekeeping.HolidayTypes";
            public const string LeaveTypes = "Timekeeping.LeaveTypes";
            public const string AdminAttendances = "Timekeeping.AdminAttendances";
            public const string AdminApprovals = "Timekeeping.AdminApprovals";
            public const string ApproverOrders = "Timekeeping.ApproverOrders";
            public const string AttendanceApprovals = "Timekeeping.AttendanceApprovals";
            public const string AttendanceAdminApprovals = "Timekeeping.AttendanceAdminApprovals";
            public const string AttendancePeriods = "Timekeeping.AttendancePeriods";
            public const string AttendanceReasons = "Timekeeping.AttendanceReasons";
            public const string AttendanceRequests = "Timekeeping.AttendanceRequests";
            public const string Attendances = "Timekeeping.Attendances";
            public const string Attendance = "Timekeeping.Attendance";
            public const string Upload = "Timekeeping.Attendances.Upload";
            public const string Process = "Timekeeping.Process";
            public const string EmployeeLogs = "Timekeeping.EmployeeLogs";
            public const string EmployeeApprovers = "Timekeeping.EmployeeApprovers";
            public const string EmployeeTimeInfo = "Timekeeping.EmployeeTimeInfo";
            public const string ExcuseApprovals = "Timekeeping.ExcuseApprovals";
            public const string ExcuseAdminApprovals = "Timekeeping.ExcuseAdminApprovals";
            public const string ExcuseReasons = "Timekeeping.ExcuseReasons";
            public const string ExcuseRequests = "Timekeeping.ExcuseRequests";
            public const string Frequencies = "Timekeeping.Frequencies";
            public const string LeaveApprovals = "Timekeeping.LeaveApprovals";
            public const string LeaveAdminApprovals = "Timekeeping.LeaveAdminApprovals";
            public const string LeaveBalances = "Timekeeping.LeaveBalances";
            public const string AdminLeaveBalances = "Timekeeping.AdminLeaveBalances";
            public const string LeaveEarnings = "Timekeeping.LeaveEarnings";
            public const string LeaveReasons = "Timekeeping.LeaveReasons";
            public const string LeaveRequestDetails = "Timekeeping.LeaveRequestDetails";
            public const string LeaveRequests = "Timekeeping.LeaveRequests";
            public const string OfficialBusinessApprovals = "Timekeeping.OfficialBusinessApprovals";
            public const string OfficialBusinessAdminApprovals = "Timekeeping.OfficialBusinessAdminApprovals";
            public const string OfficialBusinessReasons = "Timekeeping.OfficialBusinessReasons";
            public const string OfficialBusinessRequestDetails = "Timekeeping.OfficialBusinessRequestDetails";
            public const string OfficialBusinessRequests = "Timekeeping.OfficialBusinessRequests";
            public const string OvertimeApprovals = "Timekeeping.OvertimeApprovals";
            public const string OvertimeAdminApprovals = "Timekeeping.OvertimeAdminApprovals";
            public const string OvertimeReasons = "Timekeeping.OvertimeReasons";
            public const string OvertimeRequests = "Timekeeping.OvertimeRequests";
            public const string OvertimeTypes = "Timekeeping.OvertimeTypes";
            public const string RawTimeLogs = "Timekeeping.RawTimeLogs";
            public const string RestDayApprovals = "Timekeeping.RestDayApprovals";
            public const string RestDayAdminApprovals = "Timekeeping.RestDayAdminApprovals";
            public const string RestDayReasons = "Timekeeping.RestDayReasons";
            public const string RestDayRequests = "Timekeeping.RestDayRequests";
            public const string ShiftApprovals = "Timekeeping.ShiftApprovals";
            public const string ShiftAdminApprovals = "Timekeeping.ShiftAdminApprovals";
            public const string ShiftReasons = "Timekeeping.ShiftReasons";
            public const string ShiftRequests = "Timekeeping.ShiftRequests";
            public const string ShiftTypeDetails = "Timekeeping.ShiftTypeDetails";
            public const string ShiftTypes = "Timekeeping.ShiftTypes";
            public const string Status = "Timekeeping.Status";
            public const string Timesheet = "Timekeeping.Timesheet";
            public const string TKSettings = "Timekeeping.TKSettings";
            public const string Dashboard = "Dashboard.Tenant";
            public const string Settings = "Administration.Settings.Tenant";
            public const string Processes = "Administration.Settings.Processes";
            public const string Post = "Administration.Settings.Post";
            public const string SubscriptionManagement = "Administration.SubscriptionManagement.Tenant";
            public const string ePay = "Administation.ePay.Tenant";
        }
    }
}