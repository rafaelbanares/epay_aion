﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.Layout;
using MMB.ePay.Web.Session;
using MMB.ePay.Web.Views;

namespace MMB.ePay.Web.Areas.App.Views.Shared.Themes.Theme2.Components.AppTheme2Brand
{
    public class AppTheme2BrandViewComponent : ePayViewComponent
    {
        private readonly IPerRequestSessionCache _sessionCache;

        public AppTheme2BrandViewComponent(IPerRequestSessionCache sessionCache)
        {
            _sessionCache = sessionCache;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var headerModel = new HeaderViewModel
            {
                LoginInformations = await _sessionCache.GetCurrentLoginInformationsAsync()
            };

            return View(headerModel);
        }
    }
}
