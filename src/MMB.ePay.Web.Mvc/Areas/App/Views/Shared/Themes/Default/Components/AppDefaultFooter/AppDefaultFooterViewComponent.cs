﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.Layout;
using MMB.ePay.Web.Session;
using MMB.ePay.Web.Views;

namespace MMB.ePay.Web.Areas.App.Views.Shared.Themes.Default.Components.AppDefaultFooter
{
    public class AppDefaultFooterViewComponent : ePayViewComponent
    {
        private readonly IPerRequestSessionCache _sessionCache;

        public AppDefaultFooterViewComponent(IPerRequestSessionCache sessionCache)
        {
            _sessionCache = sessionCache;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var footerModel = new FooterViewModel
            {
                LoginInformations = await _sessionCache.GetCurrentLoginInformationsAsync()
            };

            return View(footerModel);
        }
    }
}
