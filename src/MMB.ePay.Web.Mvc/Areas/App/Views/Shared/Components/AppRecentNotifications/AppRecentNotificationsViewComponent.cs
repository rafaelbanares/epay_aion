﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.Layout;
using MMB.ePay.Web.Views;

namespace MMB.ePay.Web.Areas.App.Views.Shared.Components.AppRecentNotifications
{
    public class AppRecentNotificationsViewComponent : ePayViewComponent
    {
        public Task<IViewComponentResult> InvokeAsync(string cssClass)
        {
            var model = new RecentNotificationsViewModel
            {
                CssClass = cssClass
            };
            
            return Task.FromResult<IViewComponentResult>(View(model));
        }
    }
}
