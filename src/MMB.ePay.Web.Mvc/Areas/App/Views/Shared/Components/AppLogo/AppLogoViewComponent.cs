﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Areas.App.Models.Layout;
using MMB.ePay.Web.Session;
using MMB.ePay.Web.Views;

namespace MMB.ePay.Web.Areas.App.Views.Shared.Components.AppLogo
{
    public class AppLogoViewComponent : ePayViewComponent
    {
        private readonly IPerRequestSessionCache _sessionCache;

        public AppLogoViewComponent(
            IPerRequestSessionCache sessionCache
        )
        {
            _sessionCache = sessionCache;
        }

        public async Task<IViewComponentResult> InvokeAsync(string logoSkin = null, string logoClass = "")
        {
            var headerModel = new LogoViewModel
            {
                LoginInformations = await _sessionCache.GetCurrentLoginInformationsAsync(),
                LogoSkinOverride = logoSkin,
                LogoClassOverride = logoClass
            };

            return View(headerModel);
        }
    }
}
