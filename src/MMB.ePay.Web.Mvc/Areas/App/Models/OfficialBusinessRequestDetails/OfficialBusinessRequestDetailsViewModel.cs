﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.OfficialBusinessRequestDetails
{
    public class OfficialBusinessRequestDetailsViewModel : GetOfficialBusinessRequestDetailsForViewDto
    {
        public string FilterText { get; set; }
    }
}