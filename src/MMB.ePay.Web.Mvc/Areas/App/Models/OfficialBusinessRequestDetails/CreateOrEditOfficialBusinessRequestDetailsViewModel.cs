﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.OfficialBusinessRequestDetails
{
    public class CreateOrEditOfficialBusinessRequestDetailsModalViewModel
    {
        public CreateOrEditOfficialBusinessRequestDetailsDto OfficialBusinessRequestDetails { get; set; }

        public bool IsEditMode => OfficialBusinessRequestDetails.Id.HasValue;
    }
}