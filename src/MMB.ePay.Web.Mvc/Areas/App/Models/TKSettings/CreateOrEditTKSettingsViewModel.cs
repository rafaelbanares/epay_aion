﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.TKSettings
{
    public class CreateOrEditTKSettingsModalViewModel
    {
        public CreateOrEditTKSettingsDto TKSettings { get; set; }

        public bool IsEditMode => TKSettings.Id.HasValue;
    }
}