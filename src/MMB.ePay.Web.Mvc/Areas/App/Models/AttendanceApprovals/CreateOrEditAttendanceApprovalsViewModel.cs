﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.AttendanceApprovals
{
    public class CreateOrEditAttendanceApprovalsModalViewModel
    {
        public CreateOrEditAttendanceApprovalsDto AttendanceApprovals { get; set; }

        public bool IsEditMode => AttendanceApprovals.Id.HasValue;
    }
}