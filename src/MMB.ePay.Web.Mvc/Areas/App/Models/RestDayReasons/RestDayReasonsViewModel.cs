﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.RestDayReasons
{
    public class RestDayReasonsViewModel : GetRestDayReasonsForViewDto
    {
        public string FilterText { get; set; }
    }
}