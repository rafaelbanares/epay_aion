﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.RestDayReasons
{
    public class CreateOrEditRestDayReasonsModalViewModel
    {
        public CreateOrEditRestDayReasonsDto RestDayReasons { get; set; }

        public bool IsEditMode => RestDayReasons.Id.HasValue;
    }
}