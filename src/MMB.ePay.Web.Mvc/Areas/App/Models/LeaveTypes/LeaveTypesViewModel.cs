﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.LeaveTypes
{
    public class LeaveTypesViewModel : GetLeaveTypesForViewDto
    {
        public string FilterText { get; set; }
    }
}