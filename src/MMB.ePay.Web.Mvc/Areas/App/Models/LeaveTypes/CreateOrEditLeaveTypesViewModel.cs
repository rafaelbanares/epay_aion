﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.LeaveTypes
{
    public class CreateOrEditLeaveTypesModalViewModel
    {
        public CreateOrEditLeaveTypesDto LeaveTypes { get; set; }

        public bool IsEditMode => LeaveTypes.Id.HasValue;
    }
}