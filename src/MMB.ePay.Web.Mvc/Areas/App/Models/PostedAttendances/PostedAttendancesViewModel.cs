﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.PostedAttendances
{
    public class PostedAttendancesViewModel : GetPostedAttendancesForViewDto
    {
        public string FilterText { get; set; }
    }
}