﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.PostedAttendances
{
    public class CreateOrEditPostedAttendancesModalViewModel
    {
        public CreateOrEditPostedAttendancesDto PostedAttendances { get; set; }

        public bool IsEditMode => PostedAttendances.Id.HasValue;
    }
}