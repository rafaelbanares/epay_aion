﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.ShiftRequests
{
    public class CreateOrEditShiftRequestsModalViewModel
    {
        public CreateOrEditShiftRequestsModalViewModel()
        {
            ShiftTypeList = new List<SelectListItem>();
            ShiftReasonList = new List<SelectListItem>();
        }

        public CreateOrEditShiftRequestsDto ShiftRequests { get; set; }

        public bool IsEditMode => ShiftRequests.Id.HasValue;

        public IList<SelectListItem> ShiftTypeList { get; set; }
        public IList<SelectListItem> ShiftReasonList { get; set; }
    }
}