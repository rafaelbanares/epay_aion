﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.HolidayTypes
{
    public class HolidayTypesViewModel : GetHolidayTypesForViewDto
    {
        public string FilterText { get; set; }
    }
}