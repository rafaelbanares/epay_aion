﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.HolidayTypes
{
    public class CreateOrEditHolidayTypesModalViewModel
    {
        public CreateOrEditHolidayTypesDto HolidayTypes { get; set; }

        public bool IsEditMode => HolidayTypes.Id.HasValue;
    }
}