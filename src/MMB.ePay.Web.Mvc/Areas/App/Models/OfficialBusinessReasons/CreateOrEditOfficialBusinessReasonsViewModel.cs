﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.OfficialBusinessReasons
{
    public class CreateOrEditOfficialBusinessReasonsModalViewModel
    {
        public CreateOrEditOfficialBusinessReasonsDto OfficialBusinessReasons { get; set; }

        public bool IsEditMode => OfficialBusinessReasons.Id.HasValue;
    }
}