﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.OfficialBusinessReasons
{
    public class OfficialBusinessReasonsViewModel : GetOfficialBusinessReasonsForViewDto
    {
        public string FilterText { get; set; }
    }
}