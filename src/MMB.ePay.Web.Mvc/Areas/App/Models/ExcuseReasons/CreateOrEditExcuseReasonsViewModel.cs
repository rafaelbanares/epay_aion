﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.ExcuseReasons
{
    public class CreateOrEditExcuseReasonsModalViewModel
    {
        public CreateOrEditExcuseReasonsDto ExcuseReasons { get; set; }

        public bool IsEditMode => ExcuseReasons.Id.HasValue;
    }
}