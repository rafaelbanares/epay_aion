﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.ExcuseReasons
{
    public class ExcuseReasonsViewModel : GetExcuseReasonsForViewDto
    {
        public string FilterText { get; set; }
    }
}