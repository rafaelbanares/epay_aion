﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.LeaveReasons
{
    public class LeaveReasonsViewModel : GetLeaveReasonsForViewDto
    {
        public string FilterText { get; set; }

    }
}