﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.LeaveReasons
{
    public class CreateOrEditLeaveReasonsModalViewModel
    {
        public CreateOrEditLeaveReasonsDto LeaveReasons { get; set; }

        public bool IsEditMode => LeaveReasons.Id.HasValue;
    }
}