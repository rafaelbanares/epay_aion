﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.RestDayRequests
{
    public class CreateOrEditRestDayRequestsModalViewModel
    {
        public CreateOrEditRestDayRequestsModalViewModel()
        {
            RestDayReasonList = new List<SelectListItem>();
        }

        public CreateOrEditRestDayRequestsDto RestDayRequests { get; set; }

        public bool IsEditMode => RestDayRequests.Id.HasValue;

        public IList<SelectListItem> RestDayReasonList { get; set; }
    }
}