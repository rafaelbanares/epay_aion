﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.OvertimeRequests
{
    public class CreateOrEditOvertimeRequestsModalViewModel
    {
        public CreateOrEditOvertimeRequestsModalViewModel()
        {
            OvertimeReasonList = new List<SelectListItem>();
        }

        public CreateOrEditOvertimeRequestsDto OvertimeRequests { get; set; }

        public bool IsEditMode => OvertimeRequests.Id.HasValue;

        public IList<SelectListItem> OvertimeReasonList { get; set; }
    }
}