﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.ShiftTypeDetails
{
    public class ShiftTypeDetailsViewModel : GetShiftTypeDetailsForViewDto
    {
        public string FilterText { get; set; }
    }
}