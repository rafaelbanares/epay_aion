﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.ShiftTypeDetails
{
    public class CreateOrEditShiftTypeDetailsModalViewModel
    {
        public CreateOrEditShiftTypeDetailsDto ShiftTypeDetails { get; set; }

        public bool IsEditMode => ShiftTypeDetails.Id.HasValue;
    }
}