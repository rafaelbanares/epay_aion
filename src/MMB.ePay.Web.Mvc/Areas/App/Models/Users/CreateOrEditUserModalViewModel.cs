﻿using Abp.Authorization.Users;
using Abp.AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Authorization.Users.Dto;
using MMB.ePay.Security;
using MMB.ePay.Web.Areas.App.Models.Common;
using System.Collections.Generic;
using System.Linq;

namespace MMB.ePay.Web.Areas.App.Models.Users
{
    [AutoMapFrom(typeof(GetUserForEditOutput))]
    public class CreateOrEditUserModalViewModel : GetUserForEditOutput, IOrganizationUnitsEditViewModel
    {
        public bool CanChangeUserName => User.UserName != AbpUserBase.AdminUserName;

        public int AssignedRoleCount
        {
            get { return Roles.Count(r => r.IsAssigned); }
        }

        public bool IsEditMode => User.Id.HasValue;

        public PasswordComplexitySetting PasswordComplexitySetting { get; set; }

        public IList<SelectListItem> EmployeeList { get; set; }
    }
}