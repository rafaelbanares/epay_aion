﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.ShiftTypes
{
    public class CreateOrEditShiftTypesModalViewModel
    {
        public CreateOrEditShiftTypesDto ShiftTypes { get; set; }

        public bool IsEditMode => ShiftTypes.Id.HasValue;
    }

    public class EditShiftTypesModalViewModel
    {
        public EditShiftTypesDto ShiftTypes { get; set; }
    }
}