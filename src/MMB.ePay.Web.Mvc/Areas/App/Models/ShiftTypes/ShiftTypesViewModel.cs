﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.ShiftTypes
{
    public class ShiftTypesViewModel : GetShiftTypesForViewDto
    {
        public string FilterText { get; set; }
    }
}