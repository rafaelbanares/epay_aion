﻿namespace MMB.ePay.Web.Areas.App.Models.Encryption
{
    public class EncryptionViewModel
    {
        public EncryptionViewModel()
        {
        }

        public string EncryptionInput { get; set; }
        public string EncryptionOutput { get; set; }

        public string DecryptionInput { get; set; }
        public string DecryptionOutput { get; set; }
    }
}