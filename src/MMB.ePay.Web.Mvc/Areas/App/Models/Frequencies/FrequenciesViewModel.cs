﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Frequencies
{
    public class FrequenciesViewModel : GetFrequenciesForViewDto
    {
        public string FilterText { get; set; }
    }
}