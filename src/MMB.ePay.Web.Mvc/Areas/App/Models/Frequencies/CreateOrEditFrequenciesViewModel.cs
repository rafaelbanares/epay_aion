﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.Frequencies
{
    public class CreateOrEditFrequenciesModalViewModel
    {
        public CreateOrEditFrequenciesDto Frequencies { get; set; }

        public bool IsEditMode => Frequencies.Id.HasValue;
    }
}