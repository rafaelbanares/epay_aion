﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.AttendancePeriods
{
    public class AttendancePeriodsViewModel : GetAttendancePeriodsForViewDto
    {
        public AttendancePeriodsViewModel()
        {
            FrequencyList = new List<SelectListItem>();
            YearList = new List<SelectListItem>();
        }

        public IList<SelectListItem> FrequencyList { get; set; }
        public IList<SelectListItem> YearList { get; set; }

        public string FilterText { get; set; }
    }
}