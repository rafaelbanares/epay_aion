﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.AttendancePeriods
{
    public class CreateOrEditAttendancePeriodsModalViewModel
    {
        public CreateOrEditAttendancePeriodsModalViewModel()
        {
            AppYearList = new List<SelectListItem>();
            AppMonthList = new List<SelectListItem>();
            FrequencyList = new List<SelectListItem>();
        }

        public CreateOrEditAttendancePeriodsDto AttendancePeriods { get; set; }

        public IList<SelectListItem> AppYearList { get; set; }
        public IList<SelectListItem> AppMonthList { get; set; }
        public IList<SelectListItem> FrequencyList { get; set; }

        public bool IsEditMode => AttendancePeriods.Id.HasValue;
    }
}