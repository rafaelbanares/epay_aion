﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Hangfire
{
    public class HangfireViewModel : GetHangfireForViewDto
    {
        public string FilterText { get; set; }
    }
}
