﻿using System;

namespace MMB.ePay.Web.Areas.App.Models.EmailQueue
{
    public class DeleteEmailQueueModalViewModel
    {
        public DeleteEmailQueueModalViewModel()
        {
        }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

    }
}