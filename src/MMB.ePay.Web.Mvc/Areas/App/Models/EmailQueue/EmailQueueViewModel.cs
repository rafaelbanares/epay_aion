﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.EmailQueue
{
    public class EmailQueueViewModel : GetEmailQueueForViewDto
    {
        public EmailQueueViewModel()
        {
            TenantList = new List<SelectListItem>();
        }

        public IList<SelectListItem> TenantList { get; set; }
    }
}