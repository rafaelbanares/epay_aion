﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.LeaveRequests
{
    public class CreateOrEditLeaveRequestsModalViewModel
    {
        public CreateOrEditLeaveRequestsModalViewModel()
        {
            LeaveTypeList = new List<SelectListItem>();
            LeaveReasonList = new List<SelectListItem>();
        }

        public CreateOrEditLeaveRequestsDto LeaveRequests { get; set; }

        public bool IsEditMode => LeaveRequests.Id.HasValue;

        public IList<SelectListItem> LeaveTypeList { get; set; }
        public IList<SelectListItem> LeaveReasonList { get; set; }
    }
}