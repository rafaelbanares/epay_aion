﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.LeaveRequestDetails
{
    public class LeaveRequestDetailsViewModel : GetLeaveRequestDetailsForViewDto
    {
        public string FilterText { get; set; }
    }
}