﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.LeaveRequestDetails
{
    public class CreateOrEditLeaveRequestDetailsModalViewModel
    {
        public CreateOrEditLeaveRequestDetailsDto LeaveRequestDetails { get; set; }

        public bool IsEditMode => LeaveRequestDetails.Id.HasValue;
    }
}