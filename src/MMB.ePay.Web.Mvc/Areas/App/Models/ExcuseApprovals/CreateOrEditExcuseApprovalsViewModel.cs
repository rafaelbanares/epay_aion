﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.ExcuseApprovals
{
    public class CreateOrEditExcuseApprovalsModalViewModel
    {
        public CreateOrEditExcuseApprovalsDto ExcuseApprovals { get; set; }

        public bool IsEditMode => ExcuseApprovals.Id.HasValue;
    }
}