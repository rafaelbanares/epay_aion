﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.HR.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.Employees
{
    public class EmployeesViewModel : GetEmployeesForViewDto
    {
        public EmployeesViewModel()
        {
            TypeList = new List<SelectListItem>();
            StatusList = new List<SelectListItem>();
        }

        public IList<SelectListItem> TypeList { get; set; }
        public IList<SelectListItem> StatusList { get; set; }

        public string FilterText { get; set; }
    }
}