﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.HR.Dtos;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.Employees
{
    public class CreateOrEditEmployeesModalViewModel
    {
        public CreateOrEditEmployeesModalViewModel()
        {
            EmploymentTypeList = new List<SelectListItem>();

            RankList = new List<SelectListItem>();
        }

        public CreateOrEditEmployeesDto Employees { get; set; }

        public bool IsEditMode => Employees.Id.HasValue;

        public IList<SelectListItem> EmploymentTypeList { get; set; }

        public IList<SelectListItem> RankList { get; set; }
    }

    public class ProfileEmployeesModalViewModel
    {
        public ProfileEmployeesModalViewModel()
        {
            Employees = new CreateOrEditEmployeesDto();
            EmployeeTimeInfo = new CreateOrEditEmployeeTimeInfoDto();
            EmploymentTypeList = new List<SelectListItem>();
            RankList = new List<SelectListItem>();
            LocationList = new List<SelectListItem>();
            FrequencyList = new List<SelectListItem>();
            ShiftTypeList = new List<SelectListItem>();
        }

        public CreateOrEditEmployeesDto Employees { get; set; }

        public CreateOrEditEmployeeTimeInfoDto EmployeeTimeInfo { get; set; }

        public bool HasTimeInfo => EmployeeTimeInfo.Id.HasValue;

        public IList<SelectListItem> EmployeeStatusList { get; set; }
        public IList<SelectListItem> EmploymentTypeList { get; set; }
        public IList<SelectListItem> RankList { get; set; }
        public IList<SelectListItem> LocationList { get; set; }
        public IList<SelectListItem> FrequencyList { get; set; }
        public IList<SelectListItem> ShiftTypeList { get; set; }
    }
}