﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.ApproverOrders
{
    public class CreateOrEditApproverOrdersModalViewModel
    {
        public CreateOrEditApproverOrdersDto ApproverOrders { get; set; }

        public bool IsEditMode => ApproverOrders.Id.HasValue;
    }
}