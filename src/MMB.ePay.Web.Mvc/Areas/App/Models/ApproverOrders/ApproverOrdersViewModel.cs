﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.ApproverOrders
{
    public class ApproverOrdersViewModel : GetApproverOrdersForViewDto
    {
        public string FilterText { get; set; }
    }
}