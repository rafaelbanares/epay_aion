﻿using MMB.ePay.HR.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Companies
{
    public class CompaniesViewModel : GetCompaniesForViewDto
    {
        public string FilterText { get; set; }
    }
}