﻿using MMB.ePay.HR.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Companies
{
    public class CreateOrEditCompaniesModalViewModel
    {
        public CreateOrEditCompaniesDto Companies { get; set; }

        public bool IsEditMode => Companies.Id.HasValue;
    }
}