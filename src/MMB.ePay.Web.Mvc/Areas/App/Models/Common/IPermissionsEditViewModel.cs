using System.Collections.Generic;
using MMB.ePay.Authorization.Permissions.Dto;

namespace MMB.ePay.Web.Areas.App.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }

        List<string> GrantedPermissionNames { get; set; }
    }
}