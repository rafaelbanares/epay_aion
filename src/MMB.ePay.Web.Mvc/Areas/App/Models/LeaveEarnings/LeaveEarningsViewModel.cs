﻿using Abp.Web.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.LeaveEarnings
{
    public class LeaveEarningsViewModel : GetLeaveEarningsForViewDto
    {
        public LeaveEarningsViewModel()
        {
            AppYearList = new List<SelectListItem>();

            AppMonthList = new List<SelectListItem>();
        }

        public IList<SelectListItem> AppYearList { get; set; }

        public IList<SelectListItem> AppMonthList { get; set; }

        public string FilterText { get; set; }
    }

    public class LeaveBalancesViewModel : GetLeaveBalancesForViewDto
    {
        public LeaveBalancesViewModel()
        {
            AppYearList = new List<SelectListItem>();
        }

        public IList<SelectListItem> AppYearList { get; set; }

        public string FilterText { get; set; }
    }

    public class UploadLeaveEarningFileOutput : ErrorInfo
    {
        public string FileName { get; set; }

        public string FileType { get; set; }

        public string FileToken { get; set; }

        public UploadLeaveEarningFileOutput()
        {

        }

        public UploadLeaveEarningFileOutput(ErrorInfo error)
        {
            Code = error.Code;
            Details = error.Details;
            Message = error.Message;
            ValidationErrors = error.ValidationErrors;
        }
    }
}