﻿using Microsoft.AspNetCore.Http;
using MMB.ePay.Timekeeping.Dtos;
using System.ComponentModel.DataAnnotations;

namespace MMB.ePay.Web.Areas.App.Models.LeaveEarnings
{
    public class UploadLeaveEarningsModalViewModel
    {
        public UploadLeaveEarningsDto LeaveEarnings { get; set; }

        [Required]
        public IFormFile Test { get; set; }
    }
}