﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.LeaveEarnings
{
    public class CreateOrEditLeaveEarningsModalViewModel
    {
        public CreateOrEditLeaveEarningsModalViewModel()
        {
            AppYearList = new List<SelectListItem>();
            AppMonthList = new List<SelectListItem>();
            LeaveTypeList = new List<SelectListItem>();
        }

        public CreateOrEditLeaveEarningsDto LeaveEarnings { get; set; }

        public IList<SelectListItem> AppYearList { get; set; }
        public IList<SelectListItem> AppMonthList { get; set; }
        public IList<SelectListItem> LeaveTypeList { get; set; }

        public bool IsEditMode => LeaveEarnings.Id.HasValue;
    }
}