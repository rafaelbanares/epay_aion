﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.Attendances
{
    public class AttendancesViewModel : GetAttendancesForViewDto
    {
        public AttendancesViewModel()
        {
            YearList = new List<SelectListItem>();
            AttendancePeriodList = new List<SelectListItem>();
        }

        public bool HasPMBreak { get; set; }

        public IList<SelectListItem> YearList { get; set; }
        public IList<SelectListItem> AttendancePeriodList { get; set; }
    }

	public class AttendancesUploadViewModel : GetAttendancesForUploadDto
    {
        public AttendancesUploadViewModel()
        {
            FrequencyList = new List<SelectListItem>();
            //BiometricList = new List<SelectListItem>();
        }

        public IList<SelectListItem> FrequencyList { get; set; }
        //public IList<SelectListItem> BiometricList { get; set; }
    }

    public class AttendancesProcessViewModel
    {
        public AttendancesProcessViewModel()
        {
            FrequencyList = new List<SelectListItem>();
            AttendancePeriodList = new List<SelectListItem>();
        }

        public int? EmployeeId { get; set; }

        public string SearchFilterId { get; set; }

        public int FrequencyFilterId { get; set; }

        public int AttendancePeriodId { get; set; }

        public IList<SelectListItem> FrequencyList { get; set; }

        public IList<SelectListItem> AttendancePeriodList { get; set; }

        public string StatusMessage { get; set; }
    }

    public class AttendancesPostViewModel
    {
        public AttendancesPostViewModel()
        {
            FrequencyList = new List<SelectListItem>();
            InvalidAttendances = new List<InvalidAttendancesDto>();
        }

        public int FrequencyFilterId { get; set; }

        public int PendingLeaveRequestCount { get; set; }

        public bool Posted { get; set; }

		public bool IsForced { get; set; }

        public IList<SelectListItem> FrequencyList { get; set; }

        public IList<InvalidAttendancesDto> InvalidAttendances { get; set; }
    }
}