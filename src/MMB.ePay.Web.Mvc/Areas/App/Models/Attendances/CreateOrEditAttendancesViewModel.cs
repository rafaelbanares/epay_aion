﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Attendances
{
    public class CreateOrEditAttendancesModalViewModel
    {
        public CreateOrEditAttendancesDto Attendances { get; set; }

        public bool IsEditMode => Attendances.Id.HasValue;
    }
}