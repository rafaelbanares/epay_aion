﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.AttendanceRequests
{
    public class AttendanceRequestsViewModel : GetAttendanceRequestsForViewDto
    {
        public AttendanceRequestsViewModel()
        {
            YearList = new List<SelectListItem>();
            AttendancePeriodList = new List<SelectListItem>();
            StatusList = new List<SelectListItem>();
        }

        public bool HasPMBreak { get; set; }

        public IList<SelectListItem> YearList { get; set; }
        public IList<SelectListItem> AttendancePeriodList { get; set; }
        public IList<SelectListItem> StatusList { get; set; }
    }
}