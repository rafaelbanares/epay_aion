﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.AttendanceRequests
{
    public class CreateOrEditAttendanceRequestsModalViewModel
    {
        public CreateOrEditAttendanceRequestsModalViewModel()
        {
            AttendanceReasonList = new List<SelectListItem>();
        }

        public CreateOrEditAttendanceRequestsDto AttendanceRequests { get; set; }

        public bool IsEditMode => AttendanceRequests.Id.HasValue;

        public bool HasPMBreak { get; set; }

        public IList<SelectListItem> AttendanceReasonList { get; set; }
    }
}