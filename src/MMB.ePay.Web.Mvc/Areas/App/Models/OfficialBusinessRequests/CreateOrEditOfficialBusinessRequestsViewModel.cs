﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.OfficialBusinessRequests
{
    public class CreateOrEditOfficialBusinessRequestsModalViewModel
    {
        public CreateOrEditOfficialBusinessRequestsModalViewModel()
        {
            OfficialBusinessReasonList = new List<SelectListItem>();
        }

        public CreateOrEditOfficialBusinessRequestsDto OfficialBusinessRequests { get; set; }

        public bool IsEditMode => OfficialBusinessRequests.Id.HasValue;

        //public bool IsMulti => OfficialBusinessRequests.StartDate?.Date != OfficialBusinessRequests.EndDate?.Date;

        public IList<SelectListItem> OfficialBusinessReasonList { get; set; }
    }
}