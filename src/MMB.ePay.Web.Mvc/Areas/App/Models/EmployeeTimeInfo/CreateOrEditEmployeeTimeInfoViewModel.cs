﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.EmployeeTimeInfo
{
    public class CreateOrEditEmployeeTimeInfoModalViewModel
    {
        public CreateOrEditEmployeeTimeInfoDto EmployeeTimeInfo { get; set; }

        public bool IsEditMode => EmployeeTimeInfo.Id.HasValue;
    }
}