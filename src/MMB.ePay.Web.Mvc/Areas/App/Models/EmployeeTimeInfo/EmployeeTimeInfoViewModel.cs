﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.EmployeeTimeInfo
{
    public class EmployeeTimeInfoViewModel : GetEmployeeTimeInfoForViewDto
    {
        public string FilterText { get; set; }
    }
}