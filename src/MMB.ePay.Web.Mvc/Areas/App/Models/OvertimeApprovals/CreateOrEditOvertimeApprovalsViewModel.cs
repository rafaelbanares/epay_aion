﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.OvertimeApprovals
{
    public class CreateOrEditOvertimeApprovalsModalViewModel
    {
        public CreateOrEditOvertimeApprovalsDto OvertimeApprovals { get; set; }

        public bool IsEditMode => OvertimeApprovals.Id.HasValue;
    }
}