﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.Holidays
{
    public class CreateOrEditHolidaysModalViewModel
    {
        public CreateOrEditHolidaysModalViewModel()
        {
            HolidayTypeList = new List<SelectListItem>();
            LocationList = new List<SelectListItem>();
        }

        public CreateOrEditHolidaysDto Holidays { get; set; }

        public bool IsEditMode => Holidays.Id.HasValue;

        public IList<SelectListItem> HolidayTypeList { get; set; }
        public IList<SelectListItem> LocationList { get; set; }
    }
}