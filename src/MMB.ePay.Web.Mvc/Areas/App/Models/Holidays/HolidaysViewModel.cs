﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.Holidays
{
    public class HolidaysViewModel : GetHolidaysForViewDto
    {
        public HolidaysViewModel()
        {
            YearList = new List<SelectListItem>();
        }

        public IList<SelectListItem> YearList { get; set; }
    }
}