﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.OfficialBusinessApprovals
{
    public class CreateOrEditOfficialBusinessApprovalsModalViewModel
    {
        public CreateOrEditOfficialBusinessApprovalsDto OfficialBusinessApprovals { get; set; }

        public bool IsEditMode => OfficialBusinessApprovals.Id.HasValue;
    }
}