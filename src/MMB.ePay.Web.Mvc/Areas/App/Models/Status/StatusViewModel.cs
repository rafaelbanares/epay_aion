﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Status
{
    public class StatusViewModel : GetStatusForViewDto
    {
        public string FilterText { get; set; }
    }
}