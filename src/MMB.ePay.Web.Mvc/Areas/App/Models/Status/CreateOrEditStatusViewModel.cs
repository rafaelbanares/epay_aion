﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.Status
{
    public class CreateOrEditStatusModalViewModel
    {
        public CreateOrEditStatusDto Status { get; set; }

        public bool IsEditMode => Status.Id.HasValue;
    }
}