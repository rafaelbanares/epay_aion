﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.RawTimeLogs
{
	public class RawTimeLogsViewModel : GetRawTimeLogsForViewDto
    {
    }

    public class AttendancesRawEntriesModel : GetRawTimeLogsForAttendancesDto
    {
    }
}