﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.RawTimeLogs
{
    public class CreateOrEditRawTimeLogsModalViewModel
    {
        public CreateOrEditRawTimeLogsDto RawTimeLogs { get; set; }

        public bool IsEditMode => RawTimeLogs.Id.HasValue;
    }
}