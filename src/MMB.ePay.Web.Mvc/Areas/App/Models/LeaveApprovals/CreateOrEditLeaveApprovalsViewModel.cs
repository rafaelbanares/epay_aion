﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.LeaveApprovals
{
    public class CreateOrEditLeaveApprovalsModalViewModel
    {
        public CreateOrEditLeaveApprovalsDto LeaveApprovals { get; set; }

        public bool IsEditMode => LeaveApprovals.Id.HasValue;
    }
}