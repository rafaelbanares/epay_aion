﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.RestDayApprovals
{
    public class CreateOrEditRestDayApprovalsModalViewModel
    {
        public CreateOrEditRestDayApprovalsDto RestDayApprovals { get; set; }

        public bool IsEditMode => RestDayApprovals.Id.HasValue;
    }
}