﻿using System.Collections.Generic;
using MMB.ePay.Editions.Dto;

namespace MMB.ePay.Web.Areas.App.Models.Tenants
{
    public class TenantIndexViewModel
    {
        public List<SubscribableEditionComboboxItemDto> EditionItems { get; set; }
    }
}