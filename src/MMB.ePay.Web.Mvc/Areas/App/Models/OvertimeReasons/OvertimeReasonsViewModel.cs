﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.OvertimeReasons
{
    public class OvertimeReasonsViewModel : GetOvertimeReasonsForViewDto
    {
        public string FilterText { get; set; }
    }
}