﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.OvertimeReasons
{
    public class CreateOrEditOvertimeReasonsModalViewModel
    {
        public CreateOrEditOvertimeReasonsDto OvertimeReasons { get; set; }

        public bool IsEditMode => OvertimeReasons.Id.HasValue;
    }
}