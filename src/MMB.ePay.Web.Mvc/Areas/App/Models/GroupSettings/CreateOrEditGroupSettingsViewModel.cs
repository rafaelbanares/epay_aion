﻿using MMB.ePay.HR.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.GroupSettings
{
    public class CreateOrEditGroupSettingsModalViewModel
    {
        public CreateOrEditGroupSettingsDto GroupSettings { get; set; }

        public bool IsEditMode => GroupSettings.Id != null;
    }
}