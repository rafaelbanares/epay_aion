﻿using System.Collections.Generic;
using MMB.ePay.Authorization.Delegation;
using MMB.ePay.Authorization.Users.Delegation.Dto;

namespace MMB.ePay.Web.Areas.App.Models.Layout
{
    public class ActiveUserDelegationsComboboxViewModel
    {
        public IUserDelegationConfiguration UserDelegationConfiguration { get; set; }
        
        public List<UserDelegationDto> UserDelegations { get; set; }
    }
}
