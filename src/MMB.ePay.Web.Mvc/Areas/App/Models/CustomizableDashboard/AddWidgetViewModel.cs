﻿using System.Collections.Generic;
using MMB.ePay.DashboardCustomization.Dto;

namespace MMB.ePay.Web.Areas.App.Models.CustomizableDashboard
{
    public class AddWidgetViewModel
    {
        public List<WidgetOutput> Widgets { get; set; }

        public string DashboardName { get; set; }

        public string PageId { get; set; }
    }
}
