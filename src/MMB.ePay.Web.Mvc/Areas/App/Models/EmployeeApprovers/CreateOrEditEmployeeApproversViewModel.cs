﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.EmployeeApprovers
{
    public class CreateOrEditEmployeeApproversModalViewModel
    {
        public CreateOrEditEmployeeApproversModalViewModel()
        {
            //EmployeeList = new List<SelectListItem>();
            ApproverOrderList = new List<SelectListItem>();
        }

        public CreateOrEditEmployeeApproversDto EmployeeApprovers { get; set; }

        public bool IsEditMode => EmployeeApprovers.Id.HasValue;

        //public IList<SelectListItem> EmployeeList { get; set; }

        public IList<SelectListItem> ApproverOrderList { get; set; }
    }
}