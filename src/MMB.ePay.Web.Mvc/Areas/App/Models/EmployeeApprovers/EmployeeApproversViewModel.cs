﻿using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.EmployeeApprovers
{
    public class EmployeeApproversViewModel : GetEmployeeApproversForViewDto
    {
        public EmployeeApproversViewModel()
        {
            ApproverNameList = new List<string>();
        }

        public IList<string> ApproverNameList { get; set; }
        public string FilterText { get; set; }
    }
}