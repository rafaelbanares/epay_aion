﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.ExcuseRequests
{
    public class CreateOrEditExcuseRequestsModalViewModel
    {
        public CreateOrEditExcuseRequestsModalViewModel()
        {
            ExcuseReasonList = new List<SelectListItem>();
        }

        public CreateOrEditExcuseRequestsDto ExcuseRequests { get; set; }

        public bool IsEditMode => ExcuseRequests.Id.HasValue;

        public IList<SelectListItem> ExcuseReasonList { get; set; }
    }
}