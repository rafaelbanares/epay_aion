﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.WorkFlow
{
    public class CreateOrEditWorkFlowModalViewModel
    {
        public CreateOrEditWorkFlowDto WorkFlow { get; set; }

        public bool IsEditMode => WorkFlow.Id.HasValue;
    }
}