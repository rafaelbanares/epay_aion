﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.ShiftApprovals
{
    public class ShiftApprovalsViewModel : GetShiftApprovalsForViewDto
    {
        public ShiftApprovalsViewModel()
        {
            YearList = new List<SelectListItem>();
            FrequencyList = new List<SelectListItem>();
            AttendancePeriodList = new List<SelectListItem>();
            ApproverTypeList = new List<SelectListItem>();
            StatusTypeList = new List<SelectListItem>();
        }

        public string FilterText { get; set; }
        public bool ApproverTypeId { get; set; }

        public IList<SelectListItem> YearList { get; set; }
        public IList<SelectListItem> FrequencyList { get; set; }
        public IList<SelectListItem> AttendancePeriodList { get; set; }
        public IList<SelectListItem> ApproverTypeList { get; set; }
        public IList<SelectListItem> StatusTypeList { get; set; }
    }

    public class ShiftRemarksViewModel
    {
        public int Id { get; set; }

        public int StatusId { get; set; }

        public string Status { get; set; }
    }
}