﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.ShiftApprovals
{
    public class CreateOrEditShiftApprovalsModalViewModel
    {
        public CreateOrEditShiftApprovalsDto ShiftApprovals { get; set; }

        public bool IsEditMode => ShiftApprovals.Id.HasValue;
    }
}