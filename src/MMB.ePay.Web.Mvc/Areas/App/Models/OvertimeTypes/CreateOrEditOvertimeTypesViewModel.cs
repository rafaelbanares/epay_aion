﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MMB.ePay.Timekeeping.Dtos;
using System.Collections.Generic;

namespace MMB.ePay.Web.Areas.App.Models.OvertimeTypes
{
    public class CreateOrEditOvertimeTypesModalViewModel
    {
        public CreateOrEditOvertimeTypesModalViewModel()
        {
            SubTypeList = new List<SelectListItem>();
            OvertimeTypeOTCodeList = new List<SelectListItem>();
        }

        public CreateOrEditOvertimeTypesDto OvertimeTypes { get; set; }

        public IList<SelectListItem> SubTypeList { get; set; }
        public IList<SelectListItem> OvertimeTypeOTCodeList { get; set; }

        public bool IsEditMode => OvertimeTypes.Id.HasValue;
    }
}