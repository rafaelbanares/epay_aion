﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.OvertimeTypes
{
    public class OvertimeTypesViewModel : GetOvertimeTypesForViewDto
    {
        public string FilterText { get; set; }
    }
}