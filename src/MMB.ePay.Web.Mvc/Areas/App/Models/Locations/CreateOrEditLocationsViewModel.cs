﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Locations
{
    public class CreateOrEditLocationsModalViewModel
    {
        public CreateOrEditLocationsDto Locations { get; set; }

        public bool IsEditMode => Locations.Id.HasValue;
    }
}