﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.Locations
{
    public class LocationsViewModel : GetLocationsForViewDto
    {
        public string FilterText { get; set; }
    }
}