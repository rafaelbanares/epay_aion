﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.ShiftReasons
{
    public class ShiftReasonsViewModel : GetShiftReasonsForViewDto
    {
        public string FilterText { get; set; }
    }
}