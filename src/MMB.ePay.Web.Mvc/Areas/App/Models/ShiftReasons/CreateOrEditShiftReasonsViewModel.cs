﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.ShiftReasons
{
    public class CreateOrEditShiftReasonsModalViewModel
    {
        public CreateOrEditShiftReasonsDto ShiftReasons { get; set; }

        public bool IsEditMode => ShiftReasons.Id.HasValue;
    }
}