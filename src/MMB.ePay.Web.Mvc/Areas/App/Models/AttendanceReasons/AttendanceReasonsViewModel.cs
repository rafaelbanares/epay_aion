﻿using MMB.ePay.Timekeeping.Dtos;

namespace MMB.ePay.Web.Areas.App.Models.AttendanceReasons
{
    public class AttendanceReasonsViewModel : GetAttendanceReasonsForViewDto
    {
        public string FilterText { get; set; }
    }
}