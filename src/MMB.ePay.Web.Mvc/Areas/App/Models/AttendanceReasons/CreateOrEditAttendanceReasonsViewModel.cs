﻿using MMB.ePay.Timekeeping.Dtos;

using Abp.Extensions;

namespace MMB.ePay.Web.Areas.App.Models.AttendanceReasons
{
    public class CreateOrEditAttendanceReasonsModalViewModel
    {
        public CreateOrEditAttendanceReasonsDto AttendanceReasons { get; set; }

        public bool IsEditMode => AttendanceReasons.Id.HasValue;
    }
}