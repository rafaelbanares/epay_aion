﻿(function () {
    $(function () {

        var _$rawTimeLogsTable = $('#RawTimeLogsTable');
        var _rawTimeLogsService = abp.services.app.rawTimeLogs;
        var _employeesService = abp.services.app.employees;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        }).on('dp.change', function () {
            getRawTimeLogs();
        });

        var _viewRawTimeLogsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RawTimeLogs/ViewrawTimeLogsModal',
            modalClass: 'ViewRawTimeLogsModal'
        });

        var dataTable = _$rawTimeLogsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _rawTimeLogsService.getAll,
                inputFilter: function () {
                    return {
                        employeeIdFilter: $('#EmployeeFilterId').val(),
                        dateFilter: $('#DateFilter').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('Location'),
                                iconStyle: 'fas fa-map-marker-alt mr-2',
                                action: function (data) {
                                    _viewRawTimeLogsModal.open({ id: data.record.rawTimeLogs.id });
                                }
                            },
                            {
                                text: '',
                                iconStyle: 'far fa-edit mr-2',
                                visible: function (data) {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "rawTimeLogs.logTime",
                    name: "logTime",
                    render: function (date) {
                        if (date) {
                            return moment(date).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 3,
                    data: "rawTimeLogs.inOut",
                    name: "inOut"
                },
                {
                    targets: 4,
                    data: "rawTimeLogs.workFromHome",
                    name: "workFromHome",
                    render: function (workFromHome) {
                        if (workFromHome) {
                            return '<div class="text-center"><i class="fa fa-check text-success" title="True"></i></div>';
                        }
                        return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
                    }
                },
                {
                    targets: 5,
                    data: "rawTimeLogs.device",
                    name: "device"
                },
                {
                    targets: 6,
                    data: "rawTimeLogs.latitude",
                    name: "latitude"
                },
                {
                    targets: 7,
                    data: "rawTimeLogs.longitude",
                    name: "longitude"
                }
            ]
        });

        function getRawTimeLogs() {

            if ($('#EmployeeFilterName').val() == '') {
                $('#EmployeeFilterId').val('');
            }

            dataTable.ajax.reload();
        }

        abp.event.on('app.createOrEditRawTimeLogsModalSaved', function () {
            getRawTimeLogs();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getRawTimeLogs();
            }
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $('#EmployeeFilterName').autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeFilterId').val(ui.item.id);
                        $('#EmployeeFilterName').val(ui.item.label);
                        dataTable.ajax.reload();
                        return false;
                    }
                });
            });

        $('#EmployeeFilterName').change(function () {
            getRawTimeLogs();
        });
    });
})();
