(function($) {
    app.modals.CreateOrEditApproverOrdersModal = function() {
        var _approverOrdersService = abp.services.app.approverOrders;
        var _modalManager;
        var _$approverOrdersInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$approverOrdersInformationForm = _modalManager.getModal().find('form[name=ApproverOrdersInformationsForm]');
            _$approverOrdersInformationForm.validate();
        };
        this.save = function() {
            if (!_$approverOrdersInformationForm.valid()) {
                return;
            }

            var approverOrders = _$approverOrdersInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _approverOrdersService.createOrEdit(
                approverOrders
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditApproverOrdersModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);