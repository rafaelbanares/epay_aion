﻿(function () {
    $(function () {

        var _$approverOrdersTable = $('#ApproverOrdersTable');
        var _approverOrdersService = abp.services.app.approverOrders;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.ApproverOrders.Create'),
            edit: abp.auth.hasPermission('Pages.ApproverOrders.Edit')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ApproverOrders/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ApproverOrders/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditApproverOrdersModal'
        });

        var dataTable = _$approverOrdersTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _approverOrdersService.getAll
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                visible: function () {
                                    return false;
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return _permissions.edit;
                                },
                                action: function (data) {
                                    _createOrEditModal.open({ id: data.record.approverOrders.id });
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "approverOrders.displayName",
                    name: "displayName"
                },
                {
                    targets: 3,
                    data: "approverOrders.level",
                    name: "level"
                },
                {
                    targets: 4,
                    data: "approverOrders.isBackup",
                    name: "isBackup",
                    render: function (isBackup) {
                        if (isBackup) {
                            return '<div class="text-center"><i class="fa fa-check text-success" title="True"></i></div>';
                        }
                        return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
                    }

                }
            ]
        });

        function getApproverOrders() {
            dataTable.ajax.reload();
        }

        $('#CreateNewApproverOrdersButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditApproverOrdersModalSaved', function () {
            getApproverOrders();
        });

        $('#GetApproverOrdersButton').click(function (e) {
            e.preventDefault();
            getApproverOrders();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getApproverOrders();
            }
        });

    });
})();
