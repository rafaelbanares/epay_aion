(function($) {
    app.modals.CreateOrEditReviewersModal = function() {
        var _reviewersService = abp.services.app.reviewers;
        var _modalManager;
        var _$reviewersInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$reviewersInformationForm = _modalManager.getModal().find('form[name=ReviewersInformationsForm]');
            _$reviewersInformationForm.validate();
        };
        this.save = function() {
            if (!_$reviewersInformationForm.valid()) {
                return;
            }
            var reviewers = _$reviewersInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _reviewersService.createOrEdit(
                reviewers
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditReviewersModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);