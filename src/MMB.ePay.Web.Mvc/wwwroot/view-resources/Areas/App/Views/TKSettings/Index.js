﻿(function () {
    $(function () {
        var _tkSettingsService = abp.services.app.tKSettings;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        function save(successCallback) {
            var idList = [];
            var valueList = [];

            $('input[name=tksettings]').each(function () {
                idList.push($(this).data('id'));

                if ($(this).attr('type') == 'checkbox') {
                    if ($(this).is(':checked')) {
                        valueList.push('true');
                    }
                    else {
                        valueList.push('false');
                    }
                }
                else {
                    valueList.push($(this).val());
                }
            });

            _tkSettingsService.createOrEdit({
                idList, valueList
            }).done(function () {
                abp.notify.info(app.localize('SavedSuccessfully'));
                if (typeof (successCallback) === 'function') {
                    successCallback();
                }
            }).always(function () {
                abp.ui.clearBusy();
            });

            //abp.ui.setBusy();
            //_tkSettingsService.createOrEdit(
            //    1
            //).done(function () {
            //    abp.notify.info(app.localize('SavedSuccessfully'));
            //    abp.event.trigger('app.createOrEditHolidaysModalSaved');
            //    if (typeof (successCallback) === 'function') {
            //        successCallback();
            //    }
            //}).always(function () {
            //    abp.ui.clearBusy();
            //});
        };

        $('#saveBtn').click(function () {
            save(function () {
                //window.location = "/App/TKSettings";
            });
        });

    });
})();


//(function () {
//    $(function () {

//        var _$tkSettingsTable = $('#TKSettingsTable');
//        var _tkSettingsService = abp.services.app.tkSettings;

//        $('.date-picker').datetimepicker({
//            locale: abp.localization.currentLanguage.name,
//            format: 'L'
//        });

//        var _permissions = {
//            create: abp.auth.hasPermission('Pages.TKSettings.Create'),
//            edit: abp.auth.hasPermission('Pages.TKSettings.Edit'),
//            'delete': abp.auth.hasPermission('Pages.TKSettings.Delete')
//        };

//         var _createOrEditModal = new app.ModalManager({
//                    viewUrl: abp.appPath + 'App/TKSettings/CreateOrEditModal',
//                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/TKSettings/_CreateOrEditModal.js',
//                    modalClass: 'CreateOrEditTKSettingsModal'
//                });


//		 var _viewTKSettingsModal = new app.ModalManager({
//            viewUrl: abp.appPath + 'App/TKSettings/ViewtkSettingsModal',
//            modalClass: 'ViewTKSettingsModal'
//        });




//        var getDateFilter = function (element) {
//            if (element.data("DateTimePicker").date() == null) {
//                return null;
//            }
//            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
//        }

//        var getMaxDateFilter = function (element) {
//            if (element.data("DateTimePicker").date() == null) {
//                return null;
//            }
//            return element.data("DateTimePicker").date().format("YYYY-MM-DDT23:59:59Z"); 
//        }

//        var dataTable = _$tkSettingsTable.DataTable({
//            paging: true,
//            serverSide: true,
//            processing: true,
//            listAction: {
//                ajaxFunction: _tkSettingsService.getAll,
//                inputFilter: function () {
//                    return {
//					filter: $('#TKSettingsTableFilter').val(),
//					keyFilter: $('#KeyFilterId').val(),
//					valueFilter: $('#ValueFilterId').val(),
//					dataTypeFilter: $('#DataTypeFilterId').val(),
//					captionFilter: $('#CaptionFilterId').val(),
//					descriptionFilter: $('#DescriptionFilterId').val(),
//					minDisplayOrderFilter: $('#MinDisplayOrderFilterId').val(),
//					maxDisplayOrderFilter: $('#MaxDisplayOrderFilterId').val(),
//					groupCodeFilter: $('#GroupCodeFilterId').val()
//                    };
//                }
//            },
//            columnDefs: [
//                {
//                    className: 'control responsive',
//                    orderable: false,
//                    render: function () {
//                        return '';
//                    },
//                    targets: 0
//                },
//                {
//                    width: 120,
//                    targets: 1,
//                    data: null,
//                    orderable: false,
//                    autoWidth: false,
//                    defaultContent: '',
//                    rowAction: {
//                        cssClass: 'btn btn-brand dropdown-toggle',
//                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
//                        items: [
//						{
//                                text: app.localize('View'),
//                                iconStyle: 'far fa-eye mr-2',
//                                action: function (data) {
//                                    _viewTKSettingsModal.open({ id: data.record.tkSettings.id });
//                                }
//                        },
//						{
//                            text: app.localize('Edit'),
//                            iconStyle: 'far fa-edit mr-2',
//                            visible: function () {
//                                return _permissions.edit;
//                            },
//                            action: function (data) {
//                            _createOrEditModal.open({ id: data.record.tkSettings.id });                                
//                            }
//                        }, 
//						{
//                            text: app.localize('Delete'),
//                            iconStyle: 'far fa-trash-alt mr-2',
//                            visible: function () {
//                                return _permissions.delete;
//                            },
//                            action: function (data) {
//                                deleteTKSettings(data.record.tkSettings);
//                            }
//                        }]
//                    }
//                },
//					{
//						targets: 2,
//						 data: "tkSettings.key",
//						 name: "key"   
//					},
//					{
//						targets: 3,
//						 data: "tkSettings.value",
//						 name: "value"   
//					},
//					{
//						targets: 4,
//						 data: "tkSettings.dataType",
//						 name: "dataType"   
//					},
//					{
//						targets: 5,
//						 data: "tkSettings.caption",
//						 name: "caption"   
//					},
//					{
//						targets: 6,
//						 data: "tkSettings.description",
//						 name: "description"   
//					},
//					{
//						targets: 7,
//						 data: "tkSettings.displayOrder",
//						 name: "displayOrder"   
//					},
//					{
//						targets: 8,
//						 data: "tkSettings.groupCode",
//						 name: "groupCode"   
//					}
//            ]
//        });

//        function getTKSettings() {
//            dataTable.ajax.reload();
//        }

//        function deleteTKSettings(tkSettings) {
//            abp.message.confirm(
//                '',
//                app.localize('AreYouSure'),
//                function (isConfirmed) {
//                    if (isConfirmed) {
//                        _tkSettingsService.delete({
//                            id: tkSettings.id
//                        }).done(function () {
//                            getTKSettings(true);
//                            abp.notify.success(app.localize('SuccessfullyDeleted'));
//                        });
//                    }
//                }
//            );
//        }

//		$('#ShowAdvancedFiltersSpan').click(function () {
//            $('#ShowAdvancedFiltersSpan').hide();
//            $('#HideAdvancedFiltersSpan').show();
//            $('#AdvacedAuditFiltersArea').slideDown();
//        });

//        $('#HideAdvancedFiltersSpan').click(function () {
//            $('#HideAdvancedFiltersSpan').hide();
//            $('#ShowAdvancedFiltersSpan').show();
//            $('#AdvacedAuditFiltersArea').slideUp();
//        });

//        $('#CreateNewTKSettingsButton').click(function () {
//            _createOrEditModal.open();
//        });        



//        abp.event.on('app.createOrEditTKSettingsModalSaved', function () {
//            getTKSettings();
//        });

//		$('#GetTKSettingsButton').click(function (e) {
//            e.preventDefault();
//            getTKSettings();
//        });

//		$(document).keypress(function(e) {
//		  if(e.which === 13) {
//			getTKSettings();
//		  }
//		});



//    });
//})();
