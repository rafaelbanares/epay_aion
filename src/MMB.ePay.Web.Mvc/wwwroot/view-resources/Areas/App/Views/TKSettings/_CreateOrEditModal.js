(function($) {
    app.modals.CreateOrEditTKSettingsModal = function() {
        var _tkSettingsService = abp.services.app.tkSettings;
        var _modalManager;
        var _$tkSettingsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$tkSettingsInformationForm = _modalManager.getModal().find('form[name=TKSettingsInformationsForm]');
            _$tkSettingsInformationForm.validate();
        };
        this.save = function() {
            if (!_$tkSettingsInformationForm.valid()) {
                return;
            }
            var tkSettings = _$tkSettingsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _tkSettingsService.createOrEdit(
                tkSettings
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditTKSettingsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);