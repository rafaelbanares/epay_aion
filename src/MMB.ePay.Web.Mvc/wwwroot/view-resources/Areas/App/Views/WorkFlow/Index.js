﻿(function () {
    $(function () {

        var _$workFlowTable = $('#WorkFlowTable');
        var _workFlowService = abp.services.app.workFlow;

        var _permissions = {
            create: abp.auth.hasPermission('Pages.WorkFlow.Create'),
            edit: abp.auth.hasPermission('Pages.WorkFlow.Edit'),
            'delete': abp.auth.hasPermission('Pages.WorkFlow.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/WorkFlow/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/WorkFlow/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditWorkFlowModal'
        });

        var _viewWorkFlowModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/WorkFlow/ViewworkFlowModal',
            modalClass: 'ViewWorkFlowModal'
        });

        var dataTable = _$workFlowTable.DataTable({
            paging: false,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _workFlowService.getAll
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewWorkFlowModal.open({ id: data.record.workFlow.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function (data) {
                                    return _permissions.edit;
                                },
                                action: function (data) {
                                    if (data.record.workFlow.isEditable) {
                                        _createOrEditModal.open({ id: data.record.workFlow.id });
                                    }

                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "workFlow.status",
                    name: "status"
                },
                {
                    targets: 3,
                    data: "workFlow.nextStatus",
                    name: "nextStatus"
                },
            ]
        });

        function getWorkFlow() {
            dataTable.ajax.reload();
        }

        function deleteWorkFlow(workFlow) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _workFlowService.delete({
                            id: workFlow.id
                        }).done(function () {
                            getWorkFlow(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        $('#CreateNewWorkFlowButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditWorkFlowModalSaved', function () {
            getWorkFlow();
        });

        $('#GetWorkFlowButton').click(function (e) {
            e.preventDefault();
            getWorkFlow();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getWorkFlow();
            }
        });

        $('#YearFilterId').change(function () {
            getWorkFlow();
        });

    });
})();
