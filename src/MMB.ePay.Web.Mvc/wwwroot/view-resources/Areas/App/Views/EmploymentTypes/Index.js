﻿(function () {
    $(function () {

        var _$employmentTypesTable = $('#EmploymentTypesTable');
        var _employmentTypesService = abp.services.app.employmentTypes;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.EmploymentTypes.Create'),
            edit: abp.auth.hasPermission('Pages.EmploymentTypes.Edit'),
            'delete': abp.auth.hasPermission('Pages.EmploymentTypes.Delete')
        };

         var _createOrEditModal = new app.ModalManager({
                    viewUrl: abp.appPath + 'App/EmploymentTypes/CreateOrEditModal',
                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/EmploymentTypes/_CreateOrEditModal.js',
                    modalClass: 'CreateOrEditEmploymentTypesModal'
                });
                   

		 var _viewEmploymentTypesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/EmploymentTypes/ViewemploymentTypesModal',
            modalClass: 'ViewEmploymentTypesModal'
        });

		
		

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }
        
        var getMaxDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT23:59:59Z"); 
        }

        var dataTable = _$employmentTypesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _employmentTypesService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#EmploymentTypesTableFilter').val(),
					descriptionFilter: $('#DescriptionFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewEmploymentTypesModal.open({ id: data.record.employmentTypes.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.employmentTypes.id });                                
                            }
                        }, 
						{
                            text: app.localize('Delete'),
                            iconStyle: 'far fa-trash-alt mr-2',
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteEmploymentTypes(data.record.employmentTypes);
                            }
                        }]
                    }
                },
					{
						targets: 2,
						 data: "employmentTypes.description",
						 name: "description"   
					}
            ]
        });

        function getEmploymentTypes() {
            dataTable.ajax.reload();
        }

        function deleteEmploymentTypes(employmentTypes) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _employmentTypesService.delete({
                            id: employmentTypes.id
                        }).done(function () {
                            getEmploymentTypes(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewEmploymentTypesButton').click(function () {
            _createOrEditModal.open();
        });        

		$('#ExportToExcelButton').click(function () {
            _employmentTypesService
                .getEmploymentTypesToExcel({
				filter : $('#EmploymentTypesTableFilter').val(),
					descriptionFilter: $('#DescriptionFilterId').val()
				})
                .done(function (result) {
                    app.downloadTempFile(result);
                });
        });

        abp.event.on('app.createOrEditEmploymentTypesModalSaved', function () {
            getEmploymentTypes();
        });

		$('#GetEmploymentTypesButton').click(function (e) {
            e.preventDefault();
            getEmploymentTypes();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getEmploymentTypes();
		  }
		});
		
		
		
    });
})();
