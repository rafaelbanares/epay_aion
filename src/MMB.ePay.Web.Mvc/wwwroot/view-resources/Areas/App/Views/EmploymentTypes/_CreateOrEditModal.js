(function($) {
    app.modals.CreateOrEditEmploymentTypesModal = function() {
        var _employmentTypesService = abp.services.app.employmentTypes;
        var _modalManager;
        var _$employmentTypesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$employmentTypesInformationForm = _modalManager.getModal().find('form[name=EmploymentTypesInformationsForm]');
            _$employmentTypesInformationForm.validate();
        };
        this.save = function() {
            if (!_$employmentTypesInformationForm.valid()) {
                return;
            }
            var employmentTypes = _$employmentTypesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _employmentTypesService.createOrEdit(
                employmentTypes
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditEmploymentTypesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);