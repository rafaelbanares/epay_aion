(function ($) {
    app.modals.CreateOrEditEmployeeApproversModal = function() {
        var _employeeApproversService = abp.services.app.employeeApprovers;
        var _employeesService = abp.services.app.employees;

        var _modalManager;
        var _$employeeApproversInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$employeeApproversInformationForm = _modalManager.getModal().find('form[name=EmployeeApproversInformationsForm]');
            _$employeeApproversInformationForm.validate();

            _employeesService.getEmployeeList()
                .done(function (data) {
                    var employees = [];

                    $(data).each(function (index) {
                        employees.push({ "label": data[index].text, "id": data[index].value });
                    });

                    $('#EmployeeFilterId').autocomplete({
                        source: employees,
                        select: function (event, ui) {
                            $('#EmployeeApprovers_EmployeeId').val(ui.item.id);
                            $('#EmployeeFilterId').val(ui.item.label);
                            return false;
                        }
                    });

                    $('input[name=approverName]').each(function (index) {
                        var approverName = $(this);

                        approverName.autocomplete({
                            source: employees,
                            change: function (event, ui) {
                                if (approverName.val() == '') {
                                    $('#approver' + index).val('');
                                }
                            },
                            select: function (event, ui) {
                                $('#approver' + index).val(ui.item.id);
                                approverName.val(ui.item.label);
                                return false;
                            },
                            minLength: 3,
                            messages: {
                                noResults: "No results found."
                            }
                        });
                    });

                    $('button[name=clear]').each(function (index) {
                        $(this).click(function () {
                            $('#approver' + index).val('');
                            $('input[name=approverName]').eq(index).val('');
                        });
                    });


                });
        };
        this.save = function() {
            if (!_$employeeApproversInformationForm.valid()) {
                return;
            }

            var approverIdList = [];
            $('select[name=approver]').each(function () {
                approverIdList.push($(this).val());
            });

            $('input[name=approver]').each(function () {
                approverIdList.push($(this).val());
            });

            var employeeApprovers = {};
            employeeApprovers['id'] = $('input[name=id]').val();
            employeeApprovers['employeeId'] = $('#EmployeeApprovers_EmployeeId').val();
            employeeApprovers['approverIdList'] = approverIdList;

            _modalManager.setBusy(true);

            _employeeApproversService.createOrEdit(
                employeeApprovers
            ).done(function () {
                abp.notify.info(app.localize('Saved Successfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditEmployeeApproversModalSaved');
            }).always(function () {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);