﻿(function () {
    $(function () {

        var _$employeeApproversTable = $('#EmployeeApproversTable');
        var _employeeApproversService = abp.services.app.employeeApprovers;
        var _employeesService = abp.services.app.employees;
        var _approverOrdersService = abp.services.app.approverOrders;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.EmployeeApprovers.Create'),
            edit: abp.auth.hasPermission('Pages.EmployeeApprovers.Edit')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/EmployeeApprovers/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/EmployeeApprovers/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditEmployeeApproversModal'
        });

        var _viewEmployeeApproversModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/EmployeeApprovers/ViewemployeeApproversModal',
            modalClass: 'ViewEmployeeApproversModal'
        });

        var dataTable;

        _approverOrdersService.getApproverNameList()
            .done(function (result) {

                var cols = [
                    {
                        className: 'control responsive',
                        orderable: false,
                        render: function () {
                            return '';
                        },
                        targets: 0
                    },
                    {
                        width: 120,
                        targets: 1,
                        data: null,
                        orderable: false,
                        autoWidth: false,
                        defaultContent: '',
                        rowAction: {
                            cssClass: 'btn btn-brand dropdown-toggle',
                            text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                            items: [
                                {
                                    text: app.localize('View'),
                                    iconStyle: 'far fa-eye mr-2',
                                    action: function (data) {
                                        _viewEmployeeApproversModal.open({ id: data.record.employeeApprovers.id });
                                    }
                                },
                                {
                                    text: app.localize('Edit'),
                                    iconStyle: 'far fa-edit mr-2',
                                    visible: function () {
                                        return _permissions.edit;
                                    },
                                    action: function (data) {
                                        _createOrEditModal.open({ id: data.record.employeeApprovers.id });
                                    }
                                }]
                        }
                    },
                    {
                        targets: 2,
                        orderable: false,
                        data: "employeeApprovers.employee",
                        name: "employee"
                    }
                ];

                $.each(result, function (index) {
                    cols.push({
                        targets: index + 3,
                        orderable: false,
                        autoWidth: false,
                        defaultContent: '',
                        data: "employeeApprovers.approvers",
                        render: function (approvers) {
                            return approvers[index];
                        }
                    });
                });

                dataTable = _$employeeApproversTable.DataTable({
                    paging: true,
                    serverSide: true,
                    processing: true,
                    listAction: {
                        ajaxFunction: _employeeApproversService.getAll,
                        inputFilter: function (result) {
                            return {
                                filter: $('#SearchFilterId').val()
                            };
                        }
                    },
                    columnDefs: cols
                });
            });

        function getEmployeeApprovers() {
            dataTable.ajax.reload();
        }

        $('#CreateNewEmployeeApproversButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditEmployeeApproversModalSaved', function () {
            getEmployeeApprovers();
        });

        $('#SearchFilterId').keyup(function () {
            getEmployeeApprovers();
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push(data[index].text);
                });

                $("#SearchFilterId").autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#SearchFilterId').val(ui.item.label);
                        getEmployeeApprovers();
                        return false;
                    },
                    minLength: 3
                });
            });

        //$(document).keypress(function (e) {
        //    if (e.which === 13) {
        //        getEmployeeApprovers();
        //    }
        //});

        //$('#SearchFilterId').change(function () {
        //    getEmployeeApprovers();
        //});
    });
})();
