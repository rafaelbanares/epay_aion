(function($) {
    app.modals.CreateOrEditAttendancePeriodsModal = function() {
        var _attendancePeriodsService = abp.services.app.attendancePeriods;
        var _modalManager;
        var _$attendancePeriodsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$attendancePeriodsInformationForm = _modalManager.getModal().find('form[name=AttendancePeriodsInformationsForm]');
            _$attendancePeriodsInformationForm.validate();
        };
        this.save = function() {
            if (!_$attendancePeriodsInformationForm.valid()) {
                return;
            }
            var attendancePeriods = _$attendancePeriodsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _attendancePeriodsService.createOrEdit(
                attendancePeriods
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditAttendancePeriodsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);