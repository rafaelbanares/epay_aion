﻿(function () {
    $(function () {

        var _$attendancePeriodsTable = $('#AttendancePeriodsTable');
        var _attendancePeriodsService = abp.services.app.attendancePeriods;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.AttendancePeriods.Create'),
            edit: abp.auth.hasPermission('Pages.AttendancePeriods.Edit'),
            'delete': abp.auth.hasPermission('Pages.AttendancePeriods.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/AttendancePeriods/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/AttendancePeriods/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditAttendancePeriodsModal'
        });


        var _viewAttendancePeriodsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/AttendancePeriods/ViewattendancePeriodsModal',
            modalClass: 'ViewAttendancePeriodsModal'
        });

        var dataTable = _$attendancePeriodsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _attendancePeriodsService.getAll,
                inputFilter: function () {
                    return {
                        frequencyFilter: $('#FrequencyFilterId').val(),
                        yearFilter: $('#YearFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewAttendancePeriodsModal.open({ id: data.record.attendancePeriods.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function (data) {
                                    if (data.record.attendancePeriods.posted) {
                                        return false;
                                    }

                                    return _permissions.edit;
                                },
                                action: function (data) {
                                    _createOrEditModal.open({ id: data.record.attendancePeriods.id });
                                }
                            },
                            {
                                text: app.localize('Delete'),
                                iconStyle: 'far fa-trash-alt mr-2',
                                visible: function (data) {
                                    if (data.record.attendancePeriods.posted) {
                                        return false;
                                    }

                                    return _permissions.delete;
                                },
                                action: function (data) {
                                    deleteAttendancePeriods(data.record.attendancePeriods);
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "attendancePeriods.startDate",
                    name: "startDate",
                    render: function (startDate) {
                        if (startDate) {
                            return moment(startDate).format('L');
                        }
                        return "";
                    }

                },
                {
                    targets: 3,
                    data: "attendancePeriods.endDate",
                    name: "endDate",
                    render: function (endDate) {
                        if (endDate) {
                            return moment(endDate).format('L');
                        }
                        return "";
                    }

                },
                {
                    targets: 4,
                    data: "attendancePeriods.appYear",
                    name: "appYear"
                },
                {
                    targets: 5,
                    data: "attendancePeriods.appMonth",
                    name: "appMonth"
                },
                {
                    targets: 6,
                    data: "attendancePeriods.posted",
                    name: "posted",
                    render: function (posted) {
                        if (posted) {
                            return '<div class="text-center"><i class="fa fa-check text-success" title="True"></i></div>';
                        }
                        return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
                    }

                },
                {
                    targets: 7,
                    data: "attendancePeriods.frequency",
                    name: "frequencies.displayName"
                }
            ]
        });

        function getAttendancePeriods() {
            dataTable.ajax.reload();
        }

        function deleteAttendancePeriods(attendancePeriods) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _attendancePeriodsService.delete({
                            id: attendancePeriods.id
                        }).done(function () {
                            getAttendancePeriods(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        $('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewAttendancePeriodsButton').click(function () {
            _createOrEditModal.open();
        });



        abp.event.on('app.createOrEditAttendancePeriodsModalSaved', function () {
            getAttendancePeriods();
        });

        $('#GetAttendancePeriodsButton').click(function (e) {
            e.preventDefault();
            getAttendancePeriods();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getAttendancePeriods();
            }
        });

        $('#FrequencyFilterId, #YearFilterId').change(function () {
            getAttendancePeriods();
        });

    });
})();
