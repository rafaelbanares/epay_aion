(function ($) {
    app.modals.RemarksAttendanceRequestsModal = function () {

        var _attendanceRequestsService = abp.services.app.attendanceRequests;
        var _modalManager;
        this.init = function (modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
        };

        $('.status_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var reason = $('#AttendanceRequests_Reason').val();

            _modalManager.setBusy(true);
            _attendanceRequestsService.updateStatus(
                { id, statusId, reason }
            ).done(function () {
                abp.notify.info(app.localize('Updated Successfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditAttendanceRequestsModalSaved');
            }).always(function () {
                _modalManager.setBusy(false);
            });
        });
    };
})(jQuery);