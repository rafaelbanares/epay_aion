(function($) {
    app.modals.CreateOrEditAttendanceRequestsModal = function() {
        var _attendanceRequestsService = abp.services.app.attendanceRequests;
        var _modalManager;
        var _$attendanceRequestsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$attendanceRequestsInformationForm = _modalManager.getModal().find('form[name=AttendanceRequestsInformationsForm]');
            _$attendanceRequestsInformationForm.validate();
        };
        this.save = function() {
            if (!_$attendanceRequestsInformationForm.valid()) {
                return;
            }
            var attendanceRequests = _$attendanceRequestsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _attendanceRequestsService.createOrEdit(
                attendanceRequests
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();

                if (window.location.href.indexOf('Attendances') > -1) {
                    window.location = "AttendanceRequests";
                } else {
                    abp.event.trigger('app.createOrEditAttendanceRequestsModalSaved');
                }

            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);