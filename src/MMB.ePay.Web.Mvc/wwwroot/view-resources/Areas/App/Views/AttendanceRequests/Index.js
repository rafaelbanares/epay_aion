﻿(function () {
    $(function () {
        var _$attendanceRequestsTable = $('#AttendanceRequestsTable');
        var _attendanceRequestsService = abp.services.app.attendanceRequests;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;
        var hasPMBreak = $('#HasPMBreak').val().toLowerCase() == 'true';

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.AttendanceRequests.Create'),
            edit: abp.auth.hasPermission('Pages.AttendanceRequests.Edit')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/AttendanceRequests/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/AttendanceRequests/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditAttendanceRequestsModal'
        });

        var _viewAttendanceRequestsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/AttendanceRequests/ViewattendanceRequestsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/AttendanceRequests/_DetailsModal.js',
            modalClass: 'ViewAttendanceRequestsModal'
        });

        var dataTable = _$attendanceRequestsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _attendanceRequestsService.getAll,
                inputFilter: function () {
                    return {
                        attendancePeriodIdFilter: $('#AttendancePeriodFilterId').val(),
                        statusIdFilter: $('#StatusFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewAttendanceRequestsModal.open({ id: data.record.attendanceRequests.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function (data) {
                                    if (data.record.attendanceRequests.isEditable) {
                                        return _permissions.edit
                                    } else {
                                        return false;
                                    }
                                },
                                action: function (data) {
                                    _createOrEditModal.open({ id: data.record.attendanceRequests.id });
                                }
                            }]
                    }
                },
                //{
                //    targets: 2,
                //    data: "attendanceRequests.employeeId",
                //    name: "employees.employeeCode"
                //},
                //{
                //    targets: 3,
                //    data: "attendanceRequests.employee",
                //    name: "employees.lastName + employees.firstName + employees.middlename"
                //},
                {
                    targets: 2,
                    data: "attendanceRequests.date",
                    name: "date",
                    render: function (date) {
                        if (date) {
                            return moment(date).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 3,
                    data: "attendanceRequests.timeIn",
                    name: "timeIn",
                    render: function (timeIn) {
                        if (timeIn) {
                            return moment(timeIn).format('hh:mm a');
                        }
                        return "";
                    }
                },
                {
                    targets: 4,
                    data: "attendanceRequests.breaktimeIn",
                    name: "breaktimeIn",
                    render: function (breaktimeIn) {
                        if (breaktimeIn) {
                            return moment(breaktimeIn).format('hh:mm a');
                        }
                        return "";
                    },
                    visible: hasPMBreak
                },
                {
                    targets: 5,
                    data: "attendanceRequests.breaktimeOut",
                    name: "breaktimeOut",
                    render: function (breaktimeOut) {
                        if (breaktimeOut) {
                            return moment(breaktimeOut).format('hh:mm a');
                        }
                        return "";
                    },
                    visible: hasPMBreak
                },
                {
                    targets: 6,
                    data: "attendanceRequests.pmBreaktimeIn",
                    name: "pmBreaktimeIn",
                    render: function (pmBreaktimeIn) {
                        if (pmBreaktimeIn) {
                            return moment(pmBreaktimeIn).format('hh:mm a');
                        }
                        return "";
                    },
                    visible: hasPMBreak
                },
                {
                    targets: 7,
                    data: "attendanceRequests.pmBreaktimeOut",
                    name: "pmBreaktimeOut",
                    render: function (pmBreaktimeOut) {
                        if (pmBreaktimeOut) {
                            return moment(pmBreaktimeOut).format('hh:mm a');
                        }
                        return "";
                    },
                    visible: hasPMBreak
                },
                {
                    targets: 8,
                    data: "attendanceRequests.timeOut",
                    name: "timeOut",
                    render: function (timeOut) {
                        if (timeOut) {
                            return moment(timeOut).format('hh:mm a');
                        }
                        return "";
                    }
                },
                {
                    targets: 9,
                    data: "attendanceRequests.reason",
                    name: "attendanceReason.displayName"
                },
                {
                    targets: 10,
                    data: "attendanceRequests.remarks",
                    name: "remarks"
                },
                {
                    targets: 11,
                    data: "attendanceRequests.status",
                    name: "status.displayName"
                }
            ]
        });

        function getAttendanceRequests() {
            dataTable.ajax.reload();
        }

        $('#CreateNewAttendanceRequestsButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditAttendanceRequestsModalSaved', function () {
            getAttendanceRequests();
        });

        $('#GetAttendanceRequestsButton').click(function (e) {
            e.preventDefault();
            getAttendanceRequests();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getAttendanceRequests();
            }
        });

        $('#StatusFilterId, #AttendancePeriodFilterId').change(function () {
            getAttendanceRequests();
        });

        $('#YearFilterId').change(function () {
            var year = $(this).val();

            _attendancePeriodsService.getAttendancePeriodListByYear(year, null, null)
                .done(function (data) {
                    $('#AttendancePeriodFilterId').empty();

                    $(data).each(function (index) {
                        var isSelected = '';

                        if (data[index].selected == true) {
                            isSelected = 'selected';
                        }

                        $('#AttendancePeriodFilterId').append(
                            '<option value="' + data[index].value + '" ' + isSelected + '>' + data[index].text + '</option>'
                        );
                    });

                    getAttendanceRequests();
                });
        });

        $('#MinAttendanceDateFilterId, #MaxAttendanceDateFilterId').focusout(function () {
            getAttendanceRequests();
        });
    });
})();
