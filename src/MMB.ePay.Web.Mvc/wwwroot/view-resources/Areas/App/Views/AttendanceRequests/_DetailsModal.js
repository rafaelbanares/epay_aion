(function ($) {
    app.modals.ViewAttendanceRequestsModal = function () {
        var _attendanceRequestsService = abp.services.app.attendanceRequests;
        var _modalManager;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
        };

        var _remarksAttendanceRequestsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/AttendanceRequests/RemarksAttendanceRequestsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/AttendanceRequests/_RemarksModal.js',
            modalClass: 'RemarksAttendanceRequestsModal'
        });

        $('.status_button').click(function () {

            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var reason = $('#AttendanceRequests_Reason').val();

            _modalManager.setBusy(true);
            _attendanceRequestsService.updateStatus(
                { id, statusId, reason }
            ).done(function () {
                abp.notify.info(app.localize('Updated Successfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditAttendanceRequestsModalSaved');
                }).always(function() {
                _modalManager.setBusy(false);
            });
        });

        $('.remarks_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var status = $(this).data('status');

            _remarksAttendanceRequestsModal.open({ id: id, statusId: statusId, status: status });
            _modalManager.close();
        });
    };
})(jQuery);