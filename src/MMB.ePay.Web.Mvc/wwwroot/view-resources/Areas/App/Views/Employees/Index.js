﻿(function () {
    $(function () {
        var _$employeesTable = $('#EmployeesTable');
        var _employeesService = abp.services.app.employees;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Employees.Create'),
            edit: abp.auth.hasPermission('Pages.Employees.Edit')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Employees/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Employees/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditEmployeesModal'
        });

        var _viewEmployeesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Employees/ViewemployeesModal',
            modalClass: 'ViewEmployeesModal'
        });

        var dataTable = _$employeesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _employeesService.getAll,
                inputFilter: function () {
                    return {
                        filter: $('#SearchFilterId').val(),
                        typeFilter: $('#TypeFilterId').val(),
                        statusFilter: $('#StatusFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewEmployeesModal.open({ id: data.record.employees.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return _permissions.edit;
                                },
                                action: function (data) {
                                    window.location = "/App/Employees/Profile/" + data.record.employees.id;
                                }
                            }
                        ]
                    }
                },
                {
                    targets: 2,
                    data: "employees.fullName",
                    name: "lastName + firstName + middleName"
                },
                {
                    targets: 3,
                    data: "employees.employeeId",
                    name: "employeeCode"
                },
                {
                    targets: 4,
                    data: "employees.position",
                    name: "position"
                },
                {
                    targets: 5,
                    data: "employees.restDay",
                    name: "employeeTimeInfo.restDay"
                },
                {
                    targets: 6,
                    data: "employees.shift",
                    name: "employeeTimeInfo.shiftTypes.displayName"
                },
                {
                    targets: 7,
                    data: "employees.type",
                    name: "employmentTypes.description"
                },
                {
                    targets: 8,
                    data: "employees.status",
                    name: "employeeStatus"
                },
                {
                    targets: 9,
                    data: "employees.dateEmployed",
                    name: "dateEmployed",
                    render: function (dateEmployed) {
                        if (dateEmployed) {
                            return moment(dateEmployed).format('L');
                        }
                        return "";
                    }

                }
            ]
        });

        function getEmployees() {
            dataTable.ajax.reload();
        }

        $('#CreateNewEmployeesButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditEmployeesModalSaved', function () {
            getEmployees();
        });

        $('#GetEmployeesButton').click(function (e) {
            e.preventDefault();
            getEmployees();
        });

        $('#TypeFilterId, #StatusFilterId').change(function () {
            getEmployees();
        });

        $('#SearchFilterId').keyup(function () {
            getEmployees();
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push(data[index].text);
                });

                $("#SearchFilterId").autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#SearchFilterId').val(ui.item.label);
                        getEmployees();
                        return false;
                    },
                    minLength: 3
                });
            });
    });
})();
