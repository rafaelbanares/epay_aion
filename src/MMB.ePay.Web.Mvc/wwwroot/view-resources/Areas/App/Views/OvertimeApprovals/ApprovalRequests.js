﻿(function () {
    $(function () {
        var _$overtimeApprovalsTable = $('#OvertimeApprovalsTable');
        var _overtimeApprovalsService = abp.services.app.overtimeApprovals;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;
        var _employeesService = abp.services.app.employees;

        var _viewOvertimeApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OvertimeApprovals/ViewovertimeApprovalRequestsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OvertimeApprovals/_DetailsModal.js',
            modalClass: 'ViewOvertimeApprovalsModal'
        });

        var dataTable = _$overtimeApprovalsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            rowId: 'overtimeApprovals.id',
            listAction: {
                ajaxFunction: _overtimeApprovalsService.getApprovalRequests,
                inputFilter: function () {
                    return {
                        attendancePeriodIdFilter: $('#AttendancePeriodFilterId').val(),
                        employeeIdFilter: $('#EmployeeFilterId').val(),
                        isBackup: $('#ApproverFilterId').val()
                    };
                }
            },
            select: {
                style: 'multi',
                selector: 'td:first-child + td + td'
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewOvertimeApprovalsModal.open({ id: data.record.overtimeApprovals.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "overtimeApprovals.employeeId",
                    name: "employees.employeeCode"
                },
                {
                    targets: 3,
                    data: "overtimeApprovals.employee",
                    name: "employees.lastName + employees.firstName + employees.middleName"
                },
                {
                    targets: 4,
                    data: "overtimeApprovals.overtimeDate",
                    name: "overtimeDate",
                    render: function (overtimeDate) {
                        if (overtimeDate) {
                            return moment(overtimeDate).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 5,
                    data: "overtimeApprovals.overtimeStart",
                    name: "overtimeStart",
                    render: function (overtimeStart) {
                        if (overtimeStart) {
                            return time = moment(overtimeStart).format('hh:mm A');

                            //var time = moment(overtimeStart).format('hh:mm A')

                            //if (time == '12:00 AM') {
                            //    return moment(overtimeStart).format('MM/DD/YYYY');
                            //}

                            //return moment(overtimeStart).format('MM/DD/YYYY hh:mm A');
                        }
                        return "";
                    }
                },
                {
                    targets: 6,
                    data: "overtimeApprovals.overtimeEnd",
                    name: "overtimeEnd",
                    render: function (overtimeEnd) {
                        if (overtimeEnd) {
                            return time = moment(overtimeEnd).format('hh:mm A');

                            //var time = moment(overtimeEnd).format('hh:mm A')

                            //if (time == '12:00 AM') {
                            //    return moment(overtimeEnd).format('MM/DD/YYYY');
                            //}

                            //return moment(overtimeEnd).format('MM/DD/YYYY hh:mm A');
                        }
                        return "";
                    }
                },
                {
                    targets: 7,
                    data: "overtimeApprovals.reason",
                    name: "overtimeReasons.displayName"
                },
                {
                    targets: 8,
                    data: "overtimeApprovals.remarks",
                    name: "remarks"
                },
                {
                    targets: 9,
                    data: "overtimeApprovals.status",
                    name: "status.displayName"
                }
            ]
        });

        function getOvertimeApprovals() {
            if ($('#EmployeeFilterName').val() == '') {
                $('#EmployeeFilterId').val('');
            }

            dataTable.ajax.reload();
        }

        abp.event.on('app.createOrEditOvertimeApprovalsModalSaved', function () {
            getOvertimeApprovals();
        });

        $('#BackButton').click(function () {
            window.location = "/App/OvertimeApprovals";
        });

        $('#GetOvertimeApprovalsButton').click(function (e) {
            e.preventDefault();
            getOvertimeApprovals();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getOvertimeApprovals();
            }
        });

        $('#AttendancePeriodFilterId').change(function () {
            getOvertimeApprovals();
        });

        $('#YearFilterId, #FrequencyFilterId').change(function () {
            var year = $('#YearFilterId').val();
            var frequency = $('#FrequencyFilterId').val();

            _attendancePeriodsService.getAttendancePeriodListByYear(year, frequency, null)
                .done(function (data) {
                    $('#AttendancePeriodFilterId').empty();

                    $(data).each(function (index) {
                        var isSelected = '';

                        if (data[index].selected == true) {
                            isSelected = 'selected';
                        }

                        $('#AttendancePeriodFilterId').append(
                            '<option value="' + data[index].value + '" ' + isSelected + '>' + data[index].text + '</option>'
                        );
                    });

                    getOvertimeApprovals();
                });
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $('#EmployeeFilterName').autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeFilterId').val(ui.item.id);
                        $('#EmployeeFilterName').val(ui.item.label);
                        dataTable.ajax.reload();
                        return false;
                    }
                });
            });

        $('#ApproverFilterId').change(function () {
            getOvertimeApprovals();
        });
    });
})();
