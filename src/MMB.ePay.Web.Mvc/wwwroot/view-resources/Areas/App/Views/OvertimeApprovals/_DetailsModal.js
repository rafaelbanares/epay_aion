(function ($) {
    app.modals.ViewOvertimeApprovalsModal = function () {

        var _overtimeApprovalsService = abp.services.app.overtimeApprovals;
        var _modalManager;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
        };

        var _remarksOvertimeApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OvertimeApprovals/RemarksOvertimeApprovalsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OvertimeApprovals/_RemarksModal.js',
            modalClass: 'RemarksOvertimeApprovalsModal'
        });

        $('.status_button').click(function () {

            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var reason = $('#OvertimeRequests_Reason').val();

            _modalManager.setBusy(true);
            _overtimeApprovalsService.updateStatus(
                { id, statusId, reason }
            ).done(function () {
                abp.notify.info(app.localize('Updated Successfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditOvertimeApprovalsModalSaved');
                }).always(function() {
                _modalManager.setBusy(false);
            });
        });

        $('.remarks_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var status = $(this).data('status');

            _remarksOvertimeApprovalsModal.open({ id: id, statusId: statusId, status: status });
            _modalManager.close();
        });
    };
})(jQuery);