﻿(function () {
    $(function () {
        var _$overtimeApprovalsTable = $('#OvertimeApprovalsTable');
        var _overtimeApprovalsService = abp.services.app.overtimeApprovals;
        var _employeesService = abp.services.app.employees;

        var _viewOvertimeApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OvertimeApprovals/ViewovertimeApprovalsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OvertimeApprovals/_DetailsModal.js',
            modalClass: 'ViewOvertimeApprovalsModal'
        });

        var dataTable = _$overtimeApprovalsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            rowId: 'overtimeApprovals.id',
            listAction: {
                ajaxFunction: _overtimeApprovalsService.getAll,
                inputFilter: function () {
                    return {
                        frequencyIdFilter: $('#FrequencyFilterId').val(),
                        employeeIdFilter: $('#EmployeeFilterId').val(),
                        isBackup: $('#ApproverFilterId').val()
                    };
                }
            },
            select: {
                style: 'multi',
                selector: 'td:first-child + td + td'
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewOvertimeApprovalsModal.open({ id: data.record.overtimeApprovals.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    className: 'select-checkbox',
                    orderable: false,
                    data: null,
                    defaultContent: ''
                },
                {
                    targets: 3,
                    data: "overtimeApprovals.employeeId",
                    name: "employees.employeeCode"
                },
                {
                    targets: 4,
                    data: "overtimeApprovals.employee",
                    name: "employees.lastName + employees.firstName + employees.middleName"
                },
                {
                    targets: 5,
                    data: "overtimeApprovals.overtimeDate",
                    name: "overtimeDate",
                    render: function (overtimeDate) {
                        if (overtimeDate) {
                            return moment(overtimeDate).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 6,
                    data: "overtimeApprovals.overtimeStart",
                    name: "overtimeStart",
                    render: function (overtimeStart) {
                        if (overtimeStart) {
                            return time = moment(overtimeStart).format('hh:mm A');
                        }
                        return "";
                    }
                },
                {
                    targets: 7,
                    data: "overtimeApprovals.overtimeEnd",
                    name: "overtimeEnd",
                    render: function (overtimeEnd) {
                        if (overtimeEnd) {
                            return time = moment(overtimeEnd).format('hh:mm A');
                        }
                        return "";
                    }
                },
                {
                    targets: 8,
                    data: "overtimeApprovals.reason",
                    name: "overtimeReasons.displayName"
                },
                {
                    targets: 9,
                    data: "overtimeApprovals.remarks",
                    name: "remarks"
                },
                {
                    targets: 10,
                    data: "overtimeApprovals.status",
                    name: "status.displayName"
                }
            ]
        });

        dataTable.on('click', '#checkBox', function () {
            var checkBox = document.getElementById("checkBox");

            if (checkBox.checked == false) {
                dataTable.rows().deselect();
                checkBox.checked = false;
            } else {
                dataTable.rows().select();
                checkBox.checked = true;
            }
        }).on('select deselect', function () {
            ('Some selection or deselection going on')
            if (dataTable.rows({
                selected: true
            }).count() !== dataTable.rows().count()) {
                checkBox.checked = false;
            } else {
                checkBox.checked = true;
            }
        });

        function getOvertimeApprovals() {
            if ($('#EmployeeFilterName').val() == '') {
                $('#EmployeeFilterId').val('');
            }

            dataTable.ajax.reload();
        }

        abp.event.on('app.createOrEditOvertimeApprovalsModalSaved', function () {
            getOvertimeApprovals();
        });

        $('#ApprovalRequestsButton').click(function () {
            window.location = "/App/OvertimeApprovals/ApprovalRequests";
        });

        $('#GetOvertimeApprovalsButton').click(function (e) {
            e.preventDefault();
            getOvertimeApprovals();
        });

        $('#ApproveAllButton').click(function (e) {
            var ids = dataTable.rows({ selected: true }).ids().toArray();;

            if (ids.length > 0) {
                abp.ui.setBusy();
                _overtimeApprovalsService.approveAll(
                    { ids }
                ).done(function () {
                    abp.notify.info(app.localize('Updated Successfully'));
                    dataTable.rows().deselect();
                    $('#checkBox').removeClass("selected");
                    getOvertimeApprovals();
                }).always(function () {
                    abp.ui.clearBusy();
                });
            }
            else {
                abp.notify.info(app.localize('No request selected.'));
            }
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getOvertimeApprovals();
            }
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $('#EmployeeFilterName').autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeFilterId').val(ui.item.id);
                        $('#EmployeeFilterName').val(ui.item.label);
                        dataTable.ajax.reload();
                        return false;
                    }
                });
            });

        $('#ApproverFilterId, #FrequencyFilterId').change(function () {
            getOvertimeApprovals();
        });
    });
})();
