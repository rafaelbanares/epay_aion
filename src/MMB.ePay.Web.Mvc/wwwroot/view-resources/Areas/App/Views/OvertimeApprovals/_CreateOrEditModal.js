(function($) {
    app.modals.CreateOrEditOvertimeApprovalsModal = function() {
        var _overtimeApprovalsService = abp.services.app.overtimeApprovals;
        var _modalManager;
        var _$overtimeApprovalsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$overtimeApprovalsInformationForm = _modalManager.getModal().find('form[name=OvertimeApprovalsInformationsForm]');
            _$overtimeApprovalsInformationForm.validate();
        };
        this.save = function() {
            if (!_$overtimeApprovalsInformationForm.valid()) {
                return;
            }
            var overtimeApprovals = _$overtimeApprovalsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _overtimeApprovalsService.createOrEdit(
                overtimeApprovals
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditOvertimeApprovalsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);