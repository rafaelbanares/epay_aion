(function($) {
    app.modals.CreateOrEditOfficialBusinessRequestDetailsModal = function() {
        var _officialBusinessRequestDetailsService = abp.services.app.officialBusinessRequestDetails;
        var _modalManager;
        var _$officialBusinessRequestDetailsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$officialBusinessRequestDetailsInformationForm = _modalManager.getModal().find('form[name=OfficialBusinessRequestDetailsInformationsForm]');
            _$officialBusinessRequestDetailsInformationForm.validate();
        };
        this.save = function() {
            if (!_$officialBusinessRequestDetailsInformationForm.valid()) {
                return;
            }
            var officialBusinessRequestDetails = _$officialBusinessRequestDetailsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _officialBusinessRequestDetailsService.createOrEdit(
                officialBusinessRequestDetails
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditOfficialBusinessRequestDetailsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);