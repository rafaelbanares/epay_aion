﻿(function () {
    $(function () {

        var _$officialBusinessRequestDetailsTable = $('#OfficialBusinessRequestDetailsTable');
        var _officialBusinessRequestDetailsService = abp.services.app.officialBusinessRequestDetails;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.OfficialBusinessRequestDetails.Create'),
            edit: abp.auth.hasPermission('Pages.OfficialBusinessRequestDetails.Edit'),
            'delete': abp.auth.hasPermission('Pages.OfficialBusinessRequestDetails.Delete')
        };

         var _createOrEditModal = new app.ModalManager({
                    viewUrl: abp.appPath + 'App/OfficialBusinessRequestDetails/CreateOrEditModal',
                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OfficialBusinessRequestDetails/_CreateOrEditModal.js',
                    modalClass: 'CreateOrEditOfficialBusinessRequestDetailsModal'
                });
                   

		 var _viewOfficialBusinessRequestDetailsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OfficialBusinessRequestDetails/ViewofficialBusinessRequestDetailsModal',
            modalClass: 'ViewOfficialBusinessRequestDetailsModal'
        });

		
		

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }
        
        var getMaxDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT23:59:59Z"); 
        }

        var dataTable = _$officialBusinessRequestDetailsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _officialBusinessRequestDetailsService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#OfficialBusinessRequestDetailsTableFilter').val(),
					minOfficialBusinessDateFilter:  getDateFilter($('#MinOfficialBusinessDateFilterId')),
					maxOfficialBusinessDateFilter:  getMaxDateFilter($('#MaxOfficialBusinessDateFilterId')),
					minDaysFilter: $('#MinDaysFilterId').val(),
					maxDaysFilter: $('#MaxDaysFilterId').val(),
					minOfficialBusinessRequestIdFilter: $('#MinOfficialBusinessRequestIdFilterId').val(),
					maxOfficialBusinessRequestIdFilter: $('#MaxOfficialBusinessRequestIdFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewOfficialBusinessRequestDetailsModal.open({ id: data.record.officialBusinessRequestDetails.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.officialBusinessRequestDetails.id });                                
                            }
                        }, 
						{
                            text: app.localize('Delete'),
                            iconStyle: 'far fa-trash-alt mr-2',
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteOfficialBusinessRequestDetails(data.record.officialBusinessRequestDetails);
                            }
                        }]
                    }
                },
					{
						targets: 2,
						 data: "officialBusinessRequestDetails.officialBusinessDate",
						 name: "officialBusinessDate" ,
					render: function (officialBusinessDate) {
						if (officialBusinessDate) {
							return moment(officialBusinessDate).format('L');
						}
						return "";
					}
			  
					},
					{
						targets: 3,
						 data: "officialBusinessRequestDetails.days",
						 name: "days"   
					},
					{
						targets: 4,
						 data: "officialBusinessRequestDetails.officialBusinessRequestId",
						 name: "officialBusinessRequestId"   
					}
            ]
        });

        function getOfficialBusinessRequestDetails() {
            dataTable.ajax.reload();
        }

        function deleteOfficialBusinessRequestDetails(officialBusinessRequestDetails) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _officialBusinessRequestDetailsService.delete({
                            id: officialBusinessRequestDetails.id
                        }).done(function () {
                            getOfficialBusinessRequestDetails(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewOfficialBusinessRequestDetailsButton').click(function () {
            _createOrEditModal.open();
        });        

		

        abp.event.on('app.createOrEditOfficialBusinessRequestDetailsModalSaved', function () {
            getOfficialBusinessRequestDetails();
        });

		$('#GetOfficialBusinessRequestDetailsButton').click(function (e) {
            e.preventDefault();
            getOfficialBusinessRequestDetails();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getOfficialBusinessRequestDetails();
		  }
		});
		
		
		
    });
})();
