﻿(function () {
    $(function () {

        var _$frequenciesTable = $('#FrequenciesTable');
        var _frequenciesService = abp.services.app.frequencies;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Frequencies.Create'),
            edit: abp.auth.hasPermission('Pages.Frequencies.Edit'),
            'delete': abp.auth.hasPermission('Pages.Frequencies.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Frequencies/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Frequencies/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditFrequenciesModal'
        });

        var _viewFrequenciesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Frequencies/ViewfrequenciesModal',
            modalClass: 'ViewFrequenciesModal'
        });

        var dataTable = _$frequenciesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _frequenciesService.getAll,
                inputFilter: function () {
                    return {
                        filter: $('#FrequenciesTableFilter').val(),
                        displayNameFilter: $('#DisplayNameFilterId').val(),
                        descriptionFilter: $('#DescriptionFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewFrequenciesModal.open({ id: data.record.frequencies.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return _permissions.edit;
                                },
                                action: function (data) {
                                    _createOrEditModal.open({ id: data.record.frequencies.id });
                                }
                            },
                            {
                                text: app.localize('Delete'),
                                iconStyle: 'far fa-trash-alt mr-2',
                                visible: function () {
                                    return _permissions.delete;
                                },
                                action: function (data) {
                                    deleteFrequencies(data.record.frequencies);
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "frequencies.displayName",
                    name: "displayName"
                },
                {
                    targets: 3,
                    data: "frequencies.description",
                    name: "description"
                }
            ]
        });

        function getFrequencies() {
            dataTable.ajax.reload();
        }

        function deleteFrequencies(frequencies) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _frequenciesService.delete({
                            id: frequencies.id
                        }).done(function () {
                            getFrequencies(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        $('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewFrequenciesButton').click(function () {
            _createOrEditModal.open();
        });



        abp.event.on('app.createOrEditFrequenciesModalSaved', function () {
            getFrequencies();
        });

        $('#GetFrequenciesButton').click(function (e) {
            e.preventDefault();
            getFrequencies();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getFrequencies();
            }
        });

    });
})();
