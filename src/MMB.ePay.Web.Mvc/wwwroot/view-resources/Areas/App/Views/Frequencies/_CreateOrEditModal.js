(function($) {
    app.modals.CreateOrEditFrequenciesModal = function() {
        var _frequenciesService = abp.services.app.frequencies;
        var _modalManager;
        var _$frequenciesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$frequenciesInformationForm = _modalManager.getModal().find('form[name=FrequenciesInformationsForm]');
            _$frequenciesInformationForm.validate();
        };
        this.save = function() {
            if (!_$frequenciesInformationForm.valid()) {
                return;
            }
            var frequencies = _$frequenciesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _frequenciesService.createOrEdit(
                frequencies
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditFrequenciesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);