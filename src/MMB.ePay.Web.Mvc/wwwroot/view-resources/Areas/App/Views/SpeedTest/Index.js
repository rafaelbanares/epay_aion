﻿(function () {
    $(function () {

        var _$speedTestTable = $('#SpeedTestTable');
        var _speedTestService = abp.services.app.speedTest;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.SpeedTest.Create'),
            edit: abp.auth.hasPermission('Pages.SpeedTest.Edit'),
            'delete': abp.auth.hasPermission('Pages.SpeedTest.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/SpeedTest/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/SpeedTest/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditSpeedTestModal'
        });

        var _viewSpeedTestModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/SpeedTest/ViewspeedTestModal',
            modalClass: 'ViewSpeedTestModal'
        });

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z");
        }

        var getMaxDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT23:59:59Z");
        }

        var dataTable = _$speedTestTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _speedTestService.getAll,
                inputFilter: function () {
                    return {
                        yearFilter: $('#YearFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewSpeedTestModal.open({ id: data.record.speedTest.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function (data) {
                                    return _permissions.edit;
                                },
                                action: function (data) {
                                    if (data.record.speedTest.isEditable) {
                                        _createOrEditModal.open({ id: data.record.speedTest.id });
                                    }

                                    return false;
                                }
                            },
                            {
                                text: app.localize('Delete'),
                                iconStyle: 'far fa-trash-alt mr-2',
                                visible: function (data) {
                                    return _permissions.delete;
                                },
                                action: function (data) {
                                    if (data.record.speedTest.isEditable) {
                                        deleteSpeedTest(data.record.speedTest);
                                    }

                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "speedTest.displayName",
                    name: "displayName"
                },
                {
                    targets: 3,
                    data: "speedTest.date",
                    name: "date",
                    render: function (date) {
                        if (date) {
                            return moment(date).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 4,
                    data: "speedTest.location",
                    name: "locations.description"
                },
                {
                    targets: 5,
                    data: "speedTest.holidayType",
                    name: "holidayTypes.DisplayName"
                },
                {
                    targets: 6,
                    data: "speedTest.halfDay",
                    name: "halfDay",
                    render: function (halfDay) {
                        if (halfDay) {
                            return '<div class="text-center"><i class="fa fa-check text-success" title="True"></i></div>';
                        }
                        return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
                    }

                }
            ]
        });

        function getSpeedTest() {
            dataTable.ajax.reload();
        }

        function deleteSpeedTest(speedTest) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _speedTestService.delete({
                            id: speedTest.id
                        }).done(function () {
                            getSpeedTest(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        $('#CreateNewSpeedTestButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditSpeedTestModalSaved', function () {
            getSpeedTest();
        });

        $('#GetSpeedTestButton').click(function (e) {
            e.preventDefault();
            getSpeedTest();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getSpeedTest();
            }
        });

        $('#YearFilterId').change(function () {
            getSpeedTest();
        });

    });
})();
