(function($) {
    app.modals.CreateOrEditLeaveRequestDetailsModal = function() {
        var _leaveRequestDetailsService = abp.services.app.leaveRequestDetails;
        var _modalManager;
        var _$leaveRequestDetailsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$leaveRequestDetailsInformationForm = _modalManager.getModal().find('form[name=LeaveRequestDetailsInformationsForm]');
            _$leaveRequestDetailsInformationForm.validate();
        };
        this.save = function() {
            if (!_$leaveRequestDetailsInformationForm.valid()) {
                return;
            }
            var leaveRequestDetails = _$leaveRequestDetailsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _leaveRequestDetailsService.createOrEdit(
                leaveRequestDetails
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditLeaveRequestDetailsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);