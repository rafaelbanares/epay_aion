﻿(function () {
    $(function () {

        var _$leaveRequestDetailsTable = $('#LeaveRequestDetailsTable');
        var _leaveRequestDetailsService = abp.services.app.leaveRequestDetails;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.LeaveRequestDetails.Create'),
            edit: abp.auth.hasPermission('Pages.LeaveRequestDetails.Edit'),
            'delete': abp.auth.hasPermission('Pages.LeaveRequestDetails.Delete')
        };

         var _createOrEditModal = new app.ModalManager({
                    viewUrl: abp.appPath + 'App/LeaveRequestDetails/CreateOrEditModal',
                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/LeaveRequestDetails/_CreateOrEditModal.js',
                    modalClass: 'CreateOrEditLeaveRequestDetailsModal'
                });
                   

		 var _viewLeaveRequestDetailsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/LeaveRequestDetails/ViewleaveRequestDetailsModal',
            modalClass: 'ViewLeaveRequestDetailsModal'
        });

		
		

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }
        
        var getMaxDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT23:59:59Z"); 
        }

        var dataTable = _$leaveRequestDetailsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _leaveRequestDetailsService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#LeaveRequestDetailsTableFilter').val(),
					minLeaveDateFilter:  getDateFilter($('#MinLeaveDateFilterId')),
					maxLeaveDateFilter:  getMaxDateFilter($('#MaxLeaveDateFilterId')),
					minDaysFilter: $('#MinDaysFilterId').val(),
					maxDaysFilter: $('#MaxDaysFilterId').val(),
					firstHalfFilter: $('#FirstHalfFilterId').val(),
					minLeaveRequestIdFilter: $('#MinLeaveRequestIdFilterId').val(),
					maxLeaveRequestIdFilter: $('#MaxLeaveRequestIdFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewLeaveRequestDetailsModal.open({ id: data.record.leaveRequestDetails.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.leaveRequestDetails.id });                                
                            }
                        }, 
						{
                            text: app.localize('Delete'),
                            iconStyle: 'far fa-trash-alt mr-2',
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteLeaveRequestDetails(data.record.leaveRequestDetails);
                            }
                        }]
                    }
                },
					{
						targets: 2,
						 data: "leaveRequestDetails.leaveDate",
						 name: "leaveDate" ,
					render: function (leaveDate) {
						if (leaveDate) {
							return moment(leaveDate).format('L');
						}
						return "";
					}
			  
					},
					{
						targets: 3,
						 data: "leaveRequestDetails.days",
						 name: "days"   
					},
					{
						targets: 4,
						 data: "leaveRequestDetails.firstHalf",
						 name: "firstHalf"  ,
						render: function (firstHalf) {
							if (firstHalf) {
								return '<div class="text-center"><i class="fa fa-check text-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 5,
						 data: "leaveRequestDetails.leaveRequestId",
						 name: "leaveRequestId"   
					}
            ]
        });

        function getLeaveRequestDetails() {
            dataTable.ajax.reload();
        }

        function deleteLeaveRequestDetails(leaveRequestDetails) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _leaveRequestDetailsService.delete({
                            id: leaveRequestDetails.id
                        }).done(function () {
                            getLeaveRequestDetails(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewLeaveRequestDetailsButton').click(function () {
            _createOrEditModal.open();
        });        

		

        abp.event.on('app.createOrEditLeaveRequestDetailsModalSaved', function () {
            getLeaveRequestDetails();
        });

		$('#GetLeaveRequestDetailsButton').click(function (e) {
            e.preventDefault();
            getLeaveRequestDetails();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getLeaveRequestDetails();
		  }
		});
		
		
		
    });
})();
