﻿(function () {
    $(function () {

        var _$shiftTypeDetailsTable = $('#ShiftTypeDetailsTable');
        var _shiftTypeDetailsService = abp.services.app.shiftTypeDetails;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.ShiftTypeDetails.Create'),
            edit: abp.auth.hasPermission('Pages.ShiftTypeDetails.Edit'),
            'delete': abp.auth.hasPermission('Pages.ShiftTypeDetails.Delete')
        };

        var url = window.location.pathname;
        var shiftTypeId = url.substring(url.lastIndexOf('/') + 1);

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ShiftTypeDetails/CreateOrEditModal?shiftTypeId=' + shiftTypeId,
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ShiftTypeDetails/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditShiftTypeDetailsModal'
        });

        var _editModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ShiftTypes/EditModal/' + shiftTypeId,
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ShiftTypes/_EditModal.js',
            modalClass: 'EditShiftTypesModal'
        });

        var _viewShiftTypeDetailsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ShiftTypeDetails/ViewshiftTypeDetailsModal',
            modalClass: 'ViewShiftTypeDetailsModal'
        });

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z");
        }

        var getMaxDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT23:59:59Z");
        }

        var dataTable = _$shiftTypeDetailsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _shiftTypeDetailsService.getAll,
                inputFilter: function () {
                    return {
                        shiftTypeIdFilter: shiftTypeId
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            //{
                            //    text: app.localize('View'),
                            //    iconStyle: 'far fa-eye mr-2',
                            //    action: function (data) {
                            //        _viewShiftTypeDetailsModal.open({ id: data.record.shiftTypeDetails.id });
                            //    }
                            //},
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return _permissions.edit;
                                },
                                action: function (data) {
                                    _createOrEditModal.open({ id: data.record.shiftTypeDetails.id });
                                }
                            },
                            {
                                text: app.localize('Delete'),
                                iconStyle: 'far fa-trash-alt mr-2',
                                visible: function () {
                                    return _permissions.delete;
                                },
                                action: function (data) {
                                    deleteShiftTypeDetails(data.record.shiftTypeDetails);
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "shiftTypeDetails.days",
                    name: "days"
                },
                {
                    targets: 3,
                    data: "shiftTypeDetails.timeIn",
                    name: "timeIn"
                },
                {
                    targets: 4,
                    data: "shiftTypeDetails.breaktimeIn",
                    name: "breaktimeIn"
                },
                {
                    targets: 5,
                    data: "shiftTypeDetails.breaktimeOut",
                    name: "breaktimeOut"
                },
                {
                    targets: 6,
                    data: "shiftTypeDetails.timeOut",
                    name: "timeOut"
                }
            ]
        });

        function getShiftTypeDetails() {
            dataTable.ajax.reload();
        }

        function deleteShiftTypeDetails(shiftTypeDetails) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _shiftTypeDetailsService.delete({
                            id: shiftTypeDetails.id
                        }).done(function (data) {
                            if (data) {
                                getShiftTypeDetails();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            }
                            else {
                                window.location.href = '/App/ShiftTypes/';
                            }
                        });
                    }
                }
            );
        }

        $('#CreateNewShiftTypeDetailsButton').click(function () {
            _createOrEditModal.open();
        });

        $('#EditShiftTypesButton').click(function () {
            _editModal.open();
        });

        abp.event.on('app.createOrEditShiftTypeDetailsModalSaved', function () {
            getShiftTypeDetails();
        });

        $('#GetShiftTypeDetailsButton').click(function (e) {
            e.preventDefault();
            getShiftTypeDetails();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getShiftTypeDetails();
            }
        });



    });
})();
