(function($) {
    app.modals.CreateOrEditShiftTypeDetailsModal = function() {
        var _shiftTypeDetailsService = abp.services.app.shiftTypeDetails;
        var _modalManager;
        var _$shiftTypeDetailsInformationForm = null;

        togglePrevDay();

        $('#ShiftTypeDetails_TimeIn, #ShiftTypeDetails_TimeOut').change(function () {
            togglePrevDay();
        });

        function togglePrevDay() {
            var txtStartTime = $('#ShiftTypeDetails_TimeIn').val();
            var txtEndTime = $('#ShiftTypeDetails_TimeOut').val();

            var checkbox = document.getElementById('ShiftTypeDetails_StartOnPreviousDay');

            if (txtStartTime != '' && txtEndTime != '') {
                var startTime = new Date("January 01, 2020 " + txtStartTime);
                var endTime = new Date("January 01, 2020 " + txtEndTime);

                if (startTime > endTime) {
                    $('#divPrevDay').show();
                } else {
                    $('#divPrevDay').hide();
                    checkbox.checked = false;
                }
            }
            else {
                $('#divPrevDay').hide();
                checkbox.checked = false;
            }
        }

        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$shiftTypeDetailsInformationForm = _modalManager.getModal().find('form[name=ShiftTypeDetailsInformationsForm]');
            _$shiftTypeDetailsInformationForm.validate();
        };
        this.save = function() {
            if (!_$shiftTypeDetailsInformationForm.valid()) {
                return;
            }
            var shiftTypeDetails = _$shiftTypeDetailsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _shiftTypeDetailsService.createOrEdit(
                shiftTypeDetails
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditShiftTypeDetailsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);