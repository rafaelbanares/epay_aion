(function($) {
    app.modals.CreateOrEditAttendancesModal = function() {
        var _attendancesService = abp.services.app.attendances;
        var _modalManager;
        var _$attendancesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$attendancesInformationForm = _modalManager.getModal().find('form[name=AttendancesInformationsForm]');
            _$attendancesInformationForm.validate();
        };
        this.save = function() {
            if (!_$attendancesInformationForm.valid()) {
                return;
            }
            var attendances = _$attendancesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _attendancesService.createOrEdit(
                attendances
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditAttendancesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);