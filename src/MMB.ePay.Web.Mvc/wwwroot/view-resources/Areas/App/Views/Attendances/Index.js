﻿(function () {
    $(function () {
        var _$attendancesTable = $('#AttendancesTable');
        var _attendancesService = abp.services.app.attendances;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;
        var hasPMBreak = $('#HasPMBreak').val().toLowerCase() == 'true';

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Attendances.Create'),
            edit: abp.auth.hasPermission('Pages.AttendanceRequests.Create'),
            overtime: abp.auth.hasPermission('Pages.OvertimeRequests.Create')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/AttendanceRequests/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/AttendanceRequests/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditAttendanceRequestsModal'
        });

        var _overtimeModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OvertimeRequests/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OvertimeRequests/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditOvertimeRequestsModal'
        });

        var _viewAttendancesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Attendances/ViewattendancesModal',
            modalClass: 'ViewAttendancesModal'
        });

        var _viewAttendancesRawEntriesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RawTimeLogs/ViewattendancesRawEntriesModal',
            modalClass: 'ViewAttendancesRawEntriesModal'
        });

        var dataTable = _$attendancesTable.DataTable({
            paging: false,
            ordering: false,
            serverSide: true,
            processing: true,
            //lengthMenu: [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
            lengthChange: false,
            pageLength: 25,
            listAction: {
                ajaxFunction: _attendancesService.getAll,
                inputFilter: function () {
                    return {
                        attendancePeriodIdFilter: $('#AttendancePeriodFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {

                                    _viewAttendancesModal.open({ id: data.record.attendances.id, isPosted: data.record.attendances.isPosted });
                                }
                            },
                            {
                                text: app.localize('RawEntries'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {

                                    _viewAttendancesRawEntriesModal.open({ id: data.record.attendances.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function (data) {
                                    if (data.record.attendances.isPosted) {
                                        return false;
                                    } else if (data.record.attendances.status == null) {
                                        return _permissions.edit;
                                    } else {
                                        return false;
                                    }
                                },
                                action: function (data) {
                                    //window.location = "AttendanceRequests/" + data.record.attendances.id;
                                    _createOrEditModal.open({ attendanceId: data.record.attendances.id });
                                }
                            },
                            {
                                text: app.localize('Overtime'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return _permissions.overtime;
                                },
                                action: function (data) {
                                    _overtimeModal.open({ date: data.record.attendances.date });
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "attendances.date",
                    name: "date",
                    render: function (date) {
                        if (date) {
                            return moment(date).format('L');
                        }
                        return "";
                    }

                },
                {
                    targets: 3,
                    data: "attendances.day",
                    name: "date"
                },
                {
                    targets: 4,
                    data: "attendances.shift",
                    name: "shiftTypes.displayName",
                },
                {
                    targets: 5,
                    data: "attendances.timeIn",
                    name: "timeIn",
                    render: function (timeIn) {
                        if (timeIn) {
                            return moment(timeIn).format('hh:mm A');
                        }
                        return "";
                    }
                },
                {
                    targets: 6,
                    data: "attendances.breaktimeIn",
                    name: "breaktimeIn",
                    render: function (breaktimeIn) {
                        if (breaktimeIn) {
                            return moment(breaktimeIn).format('hh:mm A');
                        }
                        return "";
                    }

                },
                {
                    targets: 7,
                    data: "attendances.breaktimeOut",
                    name: "breaktimeOut",
                    render: function (breaktimeOut) {
                        if (breaktimeOut) {
                            return moment(breaktimeOut).format('hh:mm A');
                        }
                        return "";
                    }

                },
                {
                    targets: 8,
                    data: "attendances.pmBreaktimeIn",
                    name: "pmBreaktimeIn",
                    render: function (pmBreaktimeIn) {
                        if (pmBreaktimeIn) {
                            return moment(pmBreaktimeIn).format('hh:mm A');
                        }
                        return "";
                    },
                    visible: hasPMBreak
                },
                {
                    targets: 9,
                    data: "attendances.pmBreaktimeOut",
                    name: "pmBreaktimeOut",
                    render: function (pmBreaktimeOut) {
                        if (pmBreaktimeOut) {
                            return moment(pmBreaktimeOut).format('hh:mm A');
                        }
                        return "";
                    },
                    visible: hasPMBreak
                },
                {
                    targets: 10,
                    data: "attendances.timeOut",
                    name: "timeOut",
                    render: function (timeOut) {
                        if (timeOut) {
                            return moment(timeOut).format('hh:mm A');
                        }
                        return "";
                    }

                },
                //{
                //    targets: 9,
                //    data: "attendances.dayType",
                //    name: "dayType",
                //    render: function (dayType) {

                //        switch (dayType) {
                //            case 'REG':
                //                return '<text style="color:black">Regular Day<text>';
                //                break;
                //            case 'RST':
                //                return '<text style="color:blue">Rest Day<text>';
                //                break;
                //            case 'HOL':
                //                return '<text style="color:green">Holiday<text>';
                //                break;
                //        }

                //        return "";
                //    }
                //},
                {
                    targets: 11,
                    data: "attendances.remarks",
                    name: "remarks"
                    //name: "AttendanceRequests.Select(e => e.Status.DisplayName).First()"
                }
                //{
                //    targets: 10,
                //    data: "attendances.dayType",
                //    name: "dayType"
                //}
            ],
            createdRow: function (row, data, dataIndex) {
                //$(row).addClass('red');

                $(row).css("background-blend-mode", "lighten");

                if (data.attendances.dayType == "HAR") {
                    $(row).css("background-color", "coral");
                } else if (data.attendances.dayType == "HOL") {
                    $(row).css("background-color", "skyblue");
                } else if (data.attendances.dayType == "RES") {
                    $(row).css("background-color", "darkgray");
                } else {
                    $(row).css("background-color", "white");
                }
            }
        });

        function getAttendances() {
            dataTable.ajax.reload();
        }

        abp.event.on('app.createOrEditAttendancesModalSaved', function () {
            getAttendances();
        });

        $('#GetAttendancesButton').click(function (e) {
            e.preventDefault();
            getAttendances();
        });

        $('#AttendancePeriodFilterId').change(function () {
            getAttendances();
        });

        $('#YearFilterId').change(function () {
            var year = $(this).val();

            _attendancePeriodsService.getAttendancePeriodListByYear(year, null, null)
                .done(function (data) {
                    $('#AttendancePeriodFilterId').empty();

                    $(data).each(function (index) {
                        var isSelected = '';

                        if (data[index].selected == true) {
                            isSelected = 'selected';
                        }

                        $('#AttendancePeriodFilterId').append(
                            '<option value="' + data[index].value + '" ' + isSelected + '>' + data[index].text + '</option>'
                        );
                    });

                    getAttendances();
                });
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getAttendances();
            }
        });

    });
})();
