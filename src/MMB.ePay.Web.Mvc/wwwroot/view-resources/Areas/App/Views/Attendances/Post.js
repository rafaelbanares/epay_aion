﻿(function () {
    $(function () {
        var _attendancePeriodsService = abp.services.app.attendancePeriods;

        SetAttendancePeriod($('#FrequencyFilterId').val());

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        $('#FrequencyFilterId').change(function () {
            var frequencyId = $(this).val();
            SetAttendancePeriod(frequencyId);
        });

        if ($('#IsPosted').val() == 'True') {
            abp.message.info('Post complete.');
        }

        function SetAttendancePeriod(frequencyId) {
            _attendancePeriodsService.getUnpostedDateRangeFromFrequencyId(frequencyId)
                .done(function (data) {

                    if (data == null) {
                        $('#dvAttendacePeriod').html('No Attendance Period.');
                        $('#saveBtn').prop('disabled', true);
                    }
                    else {
                        var attendancePeriod = moment(data.start).format('MM/DD/YYYY') + ' - ' + moment(data.end).format('MM/DD/YYYY');
                        $('#dvAttendacePeriod').html(attendancePeriod);
                        $('#saveBtn').prop('disabled', false);
                    }

                });
        };

        $('#saveBtn').click(function () {
            abp.message.confirm(
                '',
                app.localize('AttendancePostWarningMessage'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        $('#formPost').submit();
                    }
                }
            );
        });
    });
})();
