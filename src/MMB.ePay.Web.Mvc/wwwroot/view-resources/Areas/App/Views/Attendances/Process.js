﻿(function () {
    $(function () {
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _employeesService = abp.services.app.employees;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $("#SearchFilterId").autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeId').val(ui.item.id);
                        $('#SearchFilterId').val(ui.item.label);
                        return false;
                    },
                    minLength: 3,
                    messages: {
                        noResults: 'No results found.'
                    }
                });
            });

        $('#saveBtn').click(function () {
            abp.ui.setBusy();
            $('#frmProcess').submit()
                .always(function () {
                    abp.ui.clearBusy();
                });
        });

        $('#FrequencyFilterId').change(function () {
            var frequency = $('#FrequencyFilterId').val();

            _attendancePeriodsService.getUnpostedAttendancePeriodList(frequency)
                .done(function (data) {
                    $('#AttendancePeriodId').empty();

                    $(data).each(function (index) {
                        $('#AttendancePeriodId').append(
                            '<option value="' + data[index].value + '">' + data[index].text + '</option>'
                        );
                    });
                });
        });
    });
})();
