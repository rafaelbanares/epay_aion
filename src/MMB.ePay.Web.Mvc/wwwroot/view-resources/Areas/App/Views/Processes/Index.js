(function() {
    $(function() {
        var _$processTable = $('#ProcessesTable');
        var _hangfireService = abp.services.app.hangfire;
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Process.Create'),
            edit: abp.auth.hasPermission('Pages.Process.Edit'),
            'delete': abp.auth.hasPermission('Pages.Process.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Process/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Process/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditProcessModal'
        });
        var _viewProcessModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Process/ViewprocessModal',
            modalClass: 'ViewProcessModal'
        });
        var getDateFilter = function(element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z");
        }
        var getMaxDateFilter = function(element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT23:59:59Z");
        }
        var dataTable = _$processTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _hangfireService.getAll,
                inputFilter: function() {
                    return {
                        minDateFilter: getDateFilter($('#MinDateFilterId')),
                        maxDateFilter: getMaxDateFilter($('#MaxDateFilterId'))
                    };
                }
            },
            columnDefs: [{
                    className: 'control responsive',
                    orderable: false,
                    render: function() {
                        return '';
                    },
                    targets: 0
                },
                //{
                //    width: 120,
                //    targets: 1,
                //    data: null,
                //    orderable: false,
                //    autoWidth: false,
                //    defaultContent: '',
                //    rowAction: {
                //        cssClass: 'btn btn-brand dropdown-toggle',
                //        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                //        items: [{
                //                text: app.localize('View'),
                //                iconStyle: 'far fa-eye mr-2',
                //                action: function(data) {
                //                    _viewProcessModal.open({
                //                        id: data.record.process.id
                //                    });
                //                }
                //            },
                //            {
                //                text: app.localize('Edit'),
                //                iconStyle: 'far fa-edit mr-2',
                //                visible: function() {
                //                    return _permissions.edit;
                //                },
                //                action: function(data) {
                //                    _createOrEditModal.open({
                //                        id: data.record.process.id
                //                    });
                //                }
                //            }
                //        ]
                //    }
                //},
                {
                    targets: 1,
                    data: "hangfire.job",
                    name: "job"
                },
                {
                    targets: 2,
                    data: "hangfire.stateName",
                    name: "stateName"
                },
                
                {
                    targets: 3,
                    data: "hangfire.arguments",
                    name: "arguments"
                },
                {
                    targets: 4,
                    data: "hangfire.createdAt",
                    name: "createdAt"
                },
                {
                    targets: 5,
                    data: "hangfire.expireAt",
                    name: "stateName"
                },
            ]
        });

        function getProcess() {
            dataTable.ajax.reload();
        }

        $('#CreateNewProcessButton').click(function() {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditProcessModalSaved', function() {
            getProcess();
        });
        $('#GetProcessButton').click(function(e) {
            e.preventDefault();
            getProcess();
        });
        $(document).keypress(function(e) {
            if (e.which === 13) {
                getProcess();
            }
        });
    });
})();