(function($) {
    app.modals.CreateOrEditTimesheetsModal = function() {
        var _timesheetsService = abp.services.app.timesheets;
        var _modalManager;
        var _$timesheetsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$timesheetsInformationForm = _modalManager.getModal().find('form[name=TimesheetsInformationsForm]');
            _$timesheetsInformationForm.validate();
        };
        this.save = function() {
            if (!_$timesheetsInformationForm.valid()) {
                return;
            }
            var timesheets = _$timesheetsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _timesheetsService.createOrEdit(
                timesheets
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditTimesheetsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);