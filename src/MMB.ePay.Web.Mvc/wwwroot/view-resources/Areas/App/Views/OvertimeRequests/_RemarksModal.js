(function ($) {
    app.modals.RemarksOvertimeRequestsModal = function () {

        var _overtimeRequestsService = abp.services.app.overtimeRequests;
        var _modalManager;
        this.init = function (modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
        };

        $('.status_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var reason = $('#OvertimeRequests_Reason').val();

            _modalManager.setBusy(true);
            _overtimeRequestsService.updateStatus(
                { id, statusId, reason }
            ).done(function () {
                abp.notify.info(app.localize('Updated Successfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditOvertimeRequestsModalSaved');
            }).always(function () {
                _modalManager.setBusy(false);
            });
        });
    };
})(jQuery);