(function ($) {
    app.modals.CreateOrEditOvertimeRequestsModal = function () {
        $.getScript('/view-resources/Areas/App/Views/_Bundles/demo-ui-components.min.js');

        var _overtimeRequestsService = abp.services.app.overtimeRequests;
        var _shiftTypesService = abp.services.app.shiftTypes;

        var _modalManager;
        var _$overtimeRequestsInformationForm = null;

        this.init = function (modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();

            var overtimeDate = $('#OvertimeRequests_OvertimeDate');
            overtimeDate.datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L',
                useCurrent: false
            }).on('keypress paste', function (e) {
                e.preventDefault();
                return false;
            });

            var divOvertimeStart = $('#divOvertimeStart');
            var divOvertimeEnd = $('#divOvertimeEnd');

            InitDates();

            function InitDates() {
                var overtimeStart = $('#OvertimeRequests_OvertimeStart');
                var overtimeEnd = $('#OvertimeRequests_OvertimeEnd');

                overtimeStart.on('keypress paste', function (e) {
                    e.preventDefault();
                    return false;
                });

                overtimeEnd.on('keypress paste', function (e) {
                    e.preventDefault();
                    return false;
                })

                if (!divOvertimeStart.is('visible') && !divOvertimeEnd.is('visible')) {
                    overtimeStart.val('');
                    overtimeEnd.val('');
                }

                if (overtimeDate.val() != '') {
                    var defaultDate = new Date(overtimeDate.val());
                    var minDate = new Date(overtimeDate.val());
                    var maxDate = new Date(overtimeDate.val());

                    minDate.setDate(minDate.getDate() - 1);
                    maxDate.setDate(maxDate.getDate() + 2);

                    overtimeStart.datetimepicker({
                        locale: abp.localization.currentLanguage.name,
                        format: 'L',
                        useCurrent: false,
                        defaultDate: defaultDate,
                        minDate: minDate,
                        maxDate: maxDate,
                        disabledDates: [ maxDate ]
                    });

                    overtimeEnd.datetimepicker({
                        locale: abp.localization.currentLanguage.name,
                        format: 'L',
                        useCurrent: false,
                        defaultDate: defaultDate,
                        minDate: minDate,
                        maxDate: maxDate,
                        disabledDates: [maxDate]
                    });
                } else {
                    overtimeStart.datetimepicker({
                        locale: abp.localization.currentLanguage.name,
                        format: 'L',
                        useCurrent: false
                    });

                    overtimeEnd.datetimepicker({
                        locale: abp.localization.currentLanguage.name,
                        format: 'L',
                        useCurrent: false
                    });
                }
            }

            function CheckGraveyard(clearInputs) {
                _shiftTypesService.isGraveyardShift(
                    overtimeDate.val()
                ).done(function (isGraveyard) {
                    var overtimeStart = $('#OvertimeRequests_OvertimeStart');
                    var overtimeEnd = $('#OvertimeRequests_OvertimeEnd');

                    if (clearInputs) {
                        overtimeStart.val('');
                        overtimeEnd.val('');
                    }

                    if (isGraveyard) {
                        divOvertimeStart.show();
                        divOvertimeEnd.show();

                        overtimeStart.datetimepicker('destroy');
                        overtimeEnd.datetimepicker('destroy');
                        InitDates();

                    } else {
                        divOvertimeStart.hide();
                        divOvertimeEnd.hide();

                        overtimeStart.val('');
                        overtimeEnd.val('');
                    }
                });
            }

            if (overtimeDate.val() != '') {
                CheckGraveyard();
            } else {
                overtimeDate.on('dp.change', function () {
                    CheckGraveyard(true);
                });
            }

            _$overtimeRequestsInformationForm = modal.find('form[name=OvertimeRequestsInformationsForm]');
            _$overtimeRequestsInformationForm.validate();
        };
        this.save = function () {
            if (!_$overtimeRequestsInformationForm.valid()) {
                return;
            }
            var overtimeRequests = _$overtimeRequestsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _overtimeRequestsService.createOrEdit(
                overtimeRequests
            ).done(function () {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();

                if (window.location.href.indexOf('Attendances') > -1) {
                    window.location = "OvertimeRequests";
                } else {
                    abp.event.trigger('app.createOrEditOvertimeRequestsModalSaved');
                }

            }).always(function () {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);