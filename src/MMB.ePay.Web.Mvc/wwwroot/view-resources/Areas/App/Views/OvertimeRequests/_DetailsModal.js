(function ($) {
    app.modals.ViewOvertimeRequestsModal = function () {

        var _overtimeRequestsService = abp.services.app.overtimeRequests;
        var _modalManager;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
        };

        var _remarksOvertimeRequestsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OvertimeRequests/RemarksOvertimeRequestsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OvertimeRequests/_RemarksModal.js',
            modalClass: 'RemarksOvertimeRequestsModal'
        });

        $('.status_button').click(function () {

            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var reason = $('#OvertimeRequests_Reason').val();

            _modalManager.setBusy(true);
            _overtimeRequestsService.updateStatus(
                { id, statusId, reason }
            ).done(function () {
                abp.notify.info(app.localize('Updated Successfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditOvertimeRequestsModalSaved');
                }).always(function() {
                _modalManager.setBusy(false);
            });
        });

        $('.remarks_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var status = $(this).data('status');

            _remarksOvertimeRequestsModal.open({ id: id, statusId: statusId, status: status });
            _modalManager.close();
        });
    };
})(jQuery);