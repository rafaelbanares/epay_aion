﻿(function () {
    $(function () {
        var _$overtimeRequestsTable = $('#OvertimeRequestsTable');
        var _overtimeRequestsService = abp.services.app.overtimeRequests;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.OvertimeRequests.Create'),
            edit: abp.auth.hasPermission('Pages.OvertimeRequests.Edit')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OvertimeRequests/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OvertimeRequests/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditOvertimeRequestsModal'
        });

        var _viewOvertimeRequestsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OvertimeRequests/ViewovertimeRequestsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OvertimeRequests/_DetailsModal.js',
            modalClass: 'ViewOvertimeRequestsModal'
        });

        var dataTable = _$overtimeRequestsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _overtimeRequestsService.getAll,
                inputFilter: function () {
                    return {
                        attendancePeriodIdFilter: $('#AttendancePeriodFilterId').val(),
                        statusIdFilter: $('#StatusFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewOvertimeRequestsModal.open({ id: data.record.overtimeRequests.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function (data) {
                                    if (data.record.overtimeRequests.isEditable) {
                                        return _permissions.edit;
                                    } else {
                                        return false;
                                    }
                                },
                                action: function (data) {
                                    _createOrEditModal.open({ id: data.record.overtimeRequests.id });
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "overtimeRequests.overtimeDate",
                    name: "overtimeDate",
                    render: function (overtimeDate) {
                        if (overtimeDate) {
                            return moment(overtimeDate).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 3,
                    data: "overtimeRequests.overtimeStart",
                    name: "overtimeStart",
                    render: function (overtimeStart) {
                        if (overtimeStart) {
                            return moment(overtimeStart).format('hh:mm A');
                        }
                        return "";
                    }
                },
                {
                    targets: 4,
                    data: "overtimeRequests.overtimeEnd",
                    name: "overtimeEnd",
                    render: function (overtimeEnd) {
                        if (overtimeEnd) {
                            return moment(overtimeEnd).format('hh:mm A');
                        }
                        return "";
                    }
                },
                {
                    targets: 5,
                    data: "overtimeRequests.reason",
                    name: "overtimeReasons.displayName"
                },
                {
                    targets: 6,
                    data: "overtimeRequests.remarks",
                    name: "remarks"
                },
                {
                    targets: 7,
                    data: "overtimeRequests.status",
                    name: "status.displayName"
                }
            ]
        });

        function getOvertimeRequests() {
            dataTable.ajax.reload();
        }

        $('#CreateNewOvertimeRequestsButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditOvertimeRequestsModalSaved', function () {
            getOvertimeRequests();
        });

        $('#GetOvertimeRequestsButton').click(function (e) {
            e.preventDefault();
            getOvertimeRequests();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getOvertimeRequests();
            }
        });

        $('#StatusFilterId, #AttendancePeriodFilterId').change(function () {
            getOvertimeRequests();
        });

        $('#YearFilterId').change(function () {
            var year = $(this).val();

            _attendancePeriodsService.getAttendancePeriodListByYear(year, null, null)
                .done(function (data) {
                    $('#AttendancePeriodFilterId').empty();

                    $(data).each(function (index) {
                        var isSelected = '';

                        if (data[index].selected == true) {
                            isSelected = 'selected';
                        }

                        $('#AttendancePeriodFilterId').append(
                            '<option value="' + data[index].value + '" ' + isSelected + '>' + data[index].text + '</option>'
                        );
                    });

                    getOvertimeRequests();
                });
        });

        $('#MinOvertimeDateFilterId, #MaxOvertimeDateFilterId').focusout(function () {
            getOvertimeRequests();
        });
    });
})();
