(function($) {
    app.modals.CreateOrEditLeaveTypesModal = function() {
        var _leaveTypesService = abp.services.app.leaveTypes;
        var _modalManager;
        var _$leaveTypesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$leaveTypesInformationForm = _modalManager.getModal().find('form[name=LeaveTypesInformationsForm]');
            _$leaveTypesInformationForm.validate();
        };
        this.save = function() {
            if (!_$leaveTypesInformationForm.valid()) {
                return;
            }
            var leaveTypes = _$leaveTypesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _leaveTypesService.createOrEdit(
                leaveTypes
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditLeaveTypesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);