﻿(function () {
    $(function () {

        var _$leaveTypesTable = $('#LeaveTypesTable');
        var _leaveTypesService = abp.services.app.leaveTypes;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.LeaveTypes.Create'),
            edit: abp.auth.hasPermission('Pages.LeaveTypes.Edit'),
            'delete': abp.auth.hasPermission('Pages.LeaveTypes.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/LeaveTypes/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/LeaveTypes/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditLeaveTypesModal'
        });

        var _viewLeaveTypesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/LeaveTypes/ViewleaveTypesModal',
            modalClass: 'ViewLeaveTypesModal'
        });

        var dataTable = _$leaveTypesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _leaveTypesService.getAll,
                //inputFilter: function () {
                //    return {};
                //}
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewLeaveTypesModal.open({ id: data.record.leaveTypes.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return _permissions.edit;
                                },
                                action: function (data) {
                                    _createOrEditModal.open({ id: data.record.leaveTypes.id });
                                }
                            },
                            {
                                text: app.localize('Delete'),
                                iconStyle: 'far fa-trash-alt mr-2',
                                visible: function () {
                                    return _permissions.delete;
                                },
                                action: function (data) {
                                    deleteLeaveTypes(data.record.leaveTypes);
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "leaveTypes.displayName",
                    name: "displayName"
                },
                {
                    targets: 3,
                    data: "leaveTypes.description",
                    name: "description"
                },
                {
                    targets: 4,
                    data: "leaveTypes.withPay",
                    name: "withPay",
                    render: function (withPay) {
                        if (withPay) {
                            return '<div class="text-center"><i class="fa fa-check text-success" title="True"></i></div>';
                        }
                        return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
                    }
                },
                {
                    targets: 5,
                    data: "leaveTypes.advancedFilingInDays",
                    name: "advancedFilingInDays"
                }
            ]
        });

        function getLeaveTypes() {
            dataTable.ajax.reload();
        }

        function deleteLeaveTypes(leaveTypes) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _leaveTypesService.delete({
                            id: leaveTypes.id
                        }).done(function () {
                            getLeaveTypes(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        $('#CreateNewLeaveTypesButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditLeaveTypesModalSaved', function () {
            getLeaveTypes();
        });

        $('#GetLeaveTypesButton').click(function (e) {
            e.preventDefault();
            getLeaveTypes();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getLeaveTypes();
            }
        });

    });
})();
