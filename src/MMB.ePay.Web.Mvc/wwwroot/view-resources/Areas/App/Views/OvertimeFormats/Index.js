(function() {
    $(function() {
        var _$overtimeTypeGroup1Table = $('#OvertimeTypeGroup1Table');
        var _$overtimeTypeGroup2Table = $('#OvertimeTypeGroup2Table');

        var _overtimeTypeGroup1Service = abp.services.app.overtimeTypeGroup1;
        var _overtimeTypeGroup2Service = abp.services.app.overtimeTypeGroup2;

        var _permissions = {
            create: abp.auth.hasPermission('Pages.OvertimeTypeGroup1.Create'),
            edit: abp.auth.hasPermission('Pages.OvertimeTypeGroup1.Edit')
        };
        var _permissions2 = {
            create: abp.auth.hasPermission('Pages.OvertimeTypeGroup2.Create'),
            edit: abp.auth.hasPermission('Pages.OvertimeTypeGroup2.Edit')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OvertimeTypeGroup1/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OvertimeTypeGroup1/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditOvertimeTypeGroup2Modal'
        });
        var _createOrEditModal2 = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OvertimeTypeGroup2/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OvertimeTypeGroup2/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditOvertimeTypeGroup2Modal'
        });

        var _viewOvertimeTypeGroup1Modal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OvertimeTypeGroup1/ViewovertimeTypeGroup1Modal',
            modalClass: 'ViewOvertimeTypeGroup1Modal'
        });
        var _viewOvertimeTypeGroup2Modal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OvertimeTypeGroup2/ViewovertimeTypeGroup2Modal',
            modalClass: 'ViewOvertimeTypeGroup2Modal'
        });

        var dataTable = _$overtimeTypeGroup1Table.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _overtimeTypeGroup1Service.getAll
            },
            columnDefs: [{
                className: 'control responsive',
                orderable: false,
                render: function () {
                    return '';
                },
                targets: 0
            },
            {
                width: 120,
                targets: 1,
                data: null,
                orderable: false,
                autoWidth: false,
                defaultContent: '',
                rowAction: {
                    cssClass: 'btn btn-brand dropdown-toggle',
                    text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                    items: [{
                        text: app.localize('View'),
                        iconStyle: 'far fa-eye mr-2',
                        action: function (data) {
                            _viewOvertimeTypeGroup1Modal.open({
                                id: data.record.overtimeTypeGroup1.id
                            });
                        }
                    },
                    {
                        text: app.localize('Edit'),
                        iconStyle: 'far fa-edit mr-2',
                        visible: function () {
                            return _permissions.edit;
                        },
                        action: function (data) {
                            _createOrEditModal.open({
                                id: data.record.overtimeTypeGroup1.id
                            });
                        }
                    }
                    ]
                }
            },
            {
                targets: 2,
                data: "overtimeTypeGroup1.shortName",
                name: "shortName"
            },
            {
                targets: 3,
                data: "overtimeTypeGroup1.description",
                name: "description"
            }
            ]
        });
        var dataTable2 = _$overtimeTypeGroup2Table.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _overtimeTypeGroup2Service.getAll
            },
            columnDefs: [{
                    className: 'control responsive',
                    orderable: false,
                    render: function() {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function(data) {
                                    _viewOvertimeTypeGroup2Modal.open({
                                        id: data.record.overtimeTypeGroup2.id
                                    });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function() {
                                    return _permissions2.edit;
                                },
                                action: function(data) {
                                    _createOrEditModal2.open({
                                        id: data.record.overtimeTypeGroup2.id
                                    });
                                }
                            }
                        ]
                    }
                },
                {
                    targets: 2,
                    data: "overtimeTypeGroup2.shortName",
                    name: "shortName"
                },
                {
                    targets: 3,
                    data: "overtimeTypeGroup2.description",
                    name: "description"
                }
            ]
        });

        function getOvertimeTypeGroup1() {
            dataTable.ajax.reload();
        }
        function getOvertimeTypeGroup2() {
            dataTable2.ajax.reload();
        }

        $('#CreateNewOvertimeTypeGroup1Button').click(function () {
            _createOrEditModal.open();
        });

        $('#CreateNewOvertimeTypeGroup2Button').click(function() {
            _createOrEditModal2.open();
        });
    
        abp.event.on('app.createOrEditOvertimeTypeGroup1ModalSaved', function() {
            getOvertimeTypeGroup1();
        });
        abp.event.on('app.createOrEditOvertimeTypeGroup2ModalSaved', function () {
            getOvertimeTypeGroup2();
        });

        $('#GetOvertimeTypeGroup1Button').click(function (e) {
            e.preventDefault();
            getOvertimeTypeGroup1();
        });
        $('#GetOvertimeTypeGroup2Button').click(function(e) {
            e.preventDefault();
            getOvertimeTypeGroup2();
        });

        $(document).keypress(function(e) {
            if (e.which === 13) {
                getOvertimeTypeGroup1();
                getOvertimeTypeGroup2();
            }
        });
    });
})();