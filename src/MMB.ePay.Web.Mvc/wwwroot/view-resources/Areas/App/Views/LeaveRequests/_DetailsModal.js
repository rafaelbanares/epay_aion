(function ($) {
    app.modals.ViewLeaveRequestsModal = function () {

        var _leaveRequestsService = abp.services.app.leaveRequests;
        var _modalManager;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
        };

        var _remarksLeaveRequestsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/LeaveRequests/RemarksLeaveRequestsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/LeaveRequests/_RemarksModal.js',
            modalClass: 'RemarksLeaveRequestsModal'
        });

        $('.status_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var reason = $('#LeaveRequests_Reason').val();

            _modalManager.setBusy(true);
            _leaveRequestsService.updateStatus(
                { id, statusId, reason }
            ).done(function () {
                abp.notify.info(app.localize('Updated Successfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditLeaveRequestsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        });

        $('.remarks_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var status = $(this).data('status');

            _remarksLeaveRequestsModal.open({ id: id, statusId: statusId, status: status });
            _modalManager.close();
        });
    };
})(jQuery);