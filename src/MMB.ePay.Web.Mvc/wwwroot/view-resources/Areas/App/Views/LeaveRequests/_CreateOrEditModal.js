(function($) {
    app.modals.CreateOrEditLeaveRequestsModal = function() {
        var _leaveRequestsService = abp.services.app.leaveRequests;
        var _modalManager;
        var _$leaveRequestsInformationForm = null;

        function ToggleFirstHalf() {
            let halfDay = document.getElementById('LeaveRequests_HalfDay');
            let firstHalf = document.getElementById('LeaveRequests_FirstHalf');

            if (halfDay.checked) {
                $('#lblFirstHalf').removeClass('checkbox-disabled');
                $('#LeaveRequests_FirstHalf').removeAttr('disabled');
            } else {
                firstHalf.checked = false;
                $('#lblFirstHalf').addClass('checkbox-disabled');
                $('#LeaveRequests_FirstHalf').prop('disabled', 'disabled');
            }

            let endDate = $('#LeaveRequests_EndDate');

            if (halfDay.checked || firstHalf.checked) {
                endDate.val('');
                endDate.prop('disabled', 'disabled');
            }
            else {
                endDate.removeAttr('disabled');
            }
        }

        ToggleFirstHalf();

        $('#LeaveRequests_HalfDay, #LeaveRequests_FirstHalf').change(function () {
            ToggleFirstHalf()
        });

        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$leaveRequestsInformationForm = _modalManager.getModal().find('form[name=LeaveRequestsInformationsForm]');
            _$leaveRequestsInformationForm.validate();
        };
        this.save = function() {
            if (!_$leaveRequestsInformationForm.valid()) {
                return;
            }
            var leaveRequests = _$leaveRequestsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _leaveRequestsService.createOrEdit(
                leaveRequests
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditLeaveRequestsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);