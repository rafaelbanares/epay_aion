﻿(function () {
    $(function () {
        var _$leaveRequestsTable = $('#LeaveRequestsTable');
        var _leaveRequestsService = abp.services.app.leaveRequests;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.LeaveRequests.Create'),
            edit: abp.auth.hasPermission('Pages.LeaveRequests.Edit')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/LeaveRequests/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/LeaveRequests/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditLeaveRequestsModal'
        });

        var _viewLeaveRequestsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/LeaveRequests/ViewleaveRequestsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/LeaveRequests/_DetailsModal.js',
            modalClass: 'ViewLeaveRequestsModal'
        });

        var dataTable = _$leaveRequestsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _leaveRequestsService.getAll,
                inputFilter: function () {
                    return {
                        attendancePeriodIdFilter: $('#AttendancePeriodFilterId').val(),
                        statusIdFilter: $('#StatusFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewLeaveRequestsModal.open({ id: data.record.leaveRequests.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function (data) {
                                    if (data.record.leaveRequests.isEditable) {
                                        return _permissions.edit;
                                    } else {
                                        return false;
                                    }
                                },
                                action: function (data) {
                                    _createOrEditModal.open({ id: data.record.leaveRequests.id });
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "leaveRequests.leaveType",
                    name: "leaveTypes.displayName"
                },
                {
                    targets: 3,
                    data: "leaveRequests.startDate",
                    name: "leaveRequestDetails.first()",
                    render: function (startDate) {
                        if (startDate) {
                            return moment(startDate).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 4,
                    data: "leaveRequests.endDate",
                    name: "leaveRequestDetails.first()",
                    render: function (endDate) {
                        if (endDate) {
                            return moment(endDate).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 5,
                    data: "leaveRequests.reason",
                    name: "leaveReasons.displayName"
                },
                {
                    targets: 6,
                    data: "leaveRequests.remarks",
                    name: "remarks"
                },
                {
                    targets: 7,
                    data: "leaveRequests.status",
                    name: "status.displayName"
                }
            ]
        });

        function getLeaveRequests() {
            dataTable.ajax.reload();
        }

        $('#CreateNewLeaveRequestsButton').click(function () {
            _createOrEditModal.open();
        });

        $('#LeaveBalancesButton').click(function () {
            window.location = "/App/LeaveEarnings/LeaveBalances";
        });

        abp.event.on('app.createOrEditLeaveRequestsModalSaved', function () {
            getLeaveRequests();
        });

        $('#GetLeaveRequestsButton').click(function (e) {
            e.preventDefault();
            getLeaveRequests();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getLeaveRequests();
            }
        });

        $('#StatusFilterId, #AttendancePeriodFilterId').change(function () {
            getLeaveRequests();
        });

        $('#YearFilterId').change(function () {
            var year = $(this).val();

            _attendancePeriodsService.getAttendancePeriodListByYear(year, null, null)
                .done(function (data) {
                    $('#AttendancePeriodFilterId').empty();

                    $(data).each(function (index) {
                        var isSelected = '';

                        if (data[index].selected == true) {
                            isSelected = 'selected';
                        }

                        $('#AttendancePeriodFilterId').append(
                            '<option value="' + data[index].value + '" ' + isSelected + '>' + data[index].text + '</option>'
                        );
                    });

                    getLeaveRequests();
                });
        });

        $('#MinLeaveDateFilterId, #MaxLeaveDateFilterId').focusout(function () {
            getLeaveRequests();
        });
    });
})();
