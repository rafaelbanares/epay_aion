﻿(function () {
    $(function () {
        var _$restDayApprovalsTable = $('#RestDayApprovalsTable');
        var _restDayApprovalsService = abp.services.app.restDayApprovals;
        var _employeesService = abp.services.app.employees;

        var _viewRestDayApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RestDayApprovals/ViewrestDayApprovalsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/RestDayApprovals/_DetailsModal.js',
            modalClass: 'ViewRestDayApprovalsModal'
        });

        var dataTable = _$restDayApprovalsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            rowId: 'restDayApprovals.id',
            listAction: {
                ajaxFunction: _restDayApprovalsService.getAll,
                inputFilter: function () {
                    return {
                        frequencyIdFilter: $('#FrequencyFilterId').val(),
                        employeeIdFilter: $('#EmployeeFilterId').val(),
                        isBackup: $('#ApproverFilterId').val()
                    };
                }
            },
            select: {
                style: 'multi',
                selector: 'td:first-child + td + td'
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewRestDayApprovalsModal.open({ id: data.record.restDayApprovals.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    className: 'select-checkbox',
                    orderable: false,
                    data: null,
                    defaultContent: ''
                },
                {
                    targets: 3,
                    data: "restDayApprovals.employeeId",
                    name: "employees.employeeCode"
                },
                {
                    targets: 4,
                    data: "restDayApprovals.employee",
                    name: "employees.lastName + employees.firstName + employees.middleName"
                },
                {
                    targets: 5,
                    data: "restDayApprovals.restDayStart",
                    name: "restDayStart",
                    render: function (restDayStart) {
                        if (restDayStart) {
                            return moment(restDayStart).format('MM/DD/YYYY');
                        }
                        return "";
                    }
                },
                {
                    targets: 6,
                    data: "restDayApprovals.restDayEnd",
                    name: "restDayEnd",
                    render: function (restDayEnd) {
                        if (restDayEnd) {
                            return moment(restDayEnd).format('MM/DD/YYYY');
                        }
                        return "";
                    }
                },
                {
                    targets: 7,
                    data: "restDayApprovals.restDays",
                    name: "restDayCode"
                },
                {
                    targets: 8,
                    data: "restDayApprovals.reason",
                    name: "restDayReasons.displayName"
                },
                {
                    targets: 9,
                    data: "restDayApprovals.remarks",
                    name: "remarks"
                },
                {
                    targets: 10,
                    data: "restDayApprovals.status",
                    name: "status.displayName"
                }
            ]
        });

        dataTable.on('click', '#checkBox', function () {
            var checkBox = document.getElementById("checkBox");

            if (checkBox.checked == false) {
                dataTable.rows().deselect();
                checkBox.checked = false;
            } else {
                dataTable.rows().select();
                checkBox.checked = true;
            }
        }).on('select deselect', function () {
            ('Some selection or deselection going on')
            if (dataTable.rows({
                selected: true
            }).count() !== dataTable.rows().count()) {
                checkBox.checked = false;
            } else {
                checkBox.checked = true;
            }
        });

        function getRestDayApprovals() {
            if ($('#EmployeeFilterName').val() == '') {
                $('#EmployeeFilterId').val('');
            }

            dataTable.ajax.reload();
        }

        abp.event.on('app.createOrEditRestDayApprovalsModalSaved', function () {
            getRestDayApprovals();
        });

        $('#ApprovalRequestsButton').click(function () {
            window.location = "/App/RestDayApprovals/ApprovalRequests";
        });

        $('#GetRestDayApprovalsButton').click(function (e) {
            e.preventDefault();
            getRestDayApprovals();
        });

        $('#ApproveAllButton').click(function (e) {
            var ids = dataTable.rows({ selected: true }).ids().toArray();;

            if (ids.length > 0) {
                abp.ui.setBusy();
                _restDayApprovalsService.approveAll(
                    { ids }
                ).done(function () {
                    abp.notify.info(app.localize('Updated Successfully'));
                    dataTable.rows().deselect();
                    $('#checkBox').removeClass("selected");
                    getRestDayApprovals();
                }).always(function () {
                    abp.ui.clearBusy();
                });
            }
            else {
                abp.notify.info(app.localize('No request selected.'));
            }
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getRestDayApprovals();
            }
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $('#EmployeeFilterName').autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeFilterId').val(ui.item.id);
                        $('#EmployeeFilterName').val(ui.item.label);
                        dataTable.ajax.reload();
                        return false;
                    }
                });
            });

        $('#ApproverFilterId, #FrequencyFilterId').change(function () {
            getRestDayApprovals();
        });
    });
})();
