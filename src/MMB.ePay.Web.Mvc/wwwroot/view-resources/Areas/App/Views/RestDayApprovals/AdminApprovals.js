﻿(function () {
    $(function () {
        var _$restDayApprovalsTable = $('#RestDayApprovalsTable');
        var _restDayApprovalsService = abp.services.app.restDayApprovals;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;
        var _employeesService = abp.services.app.employees;

        var _viewRestDayApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RestDayApprovals/ViewrestDayApprovalsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/RestDayApprovals/_AdminDetailsModal.js',
            modalClass: 'ViewRestDayApprovalsModal'
        });

        var dataTable = _$restDayApprovalsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            rowId: 'restDayApprovals.id',
            listAction: {
                ajaxFunction: _restDayApprovalsService.getAdminApprovals,
                inputFilter: function () {
                    return {
                        frequencyIdFilter: $('#FrequencyFilterId').val(),
                        attendancePeriodIdFilter: $('#AttendancePeriodFilterId').val(),
                        employeeIdFilter: $('#EmployeeFilterId').val(),
                        isBackup: $('#ApproverFilterId').val(),
                        statusTypeIdFilter: $('#StatusFilterId').val()
                    };
                }
            },
            select: {
                style: 'multi',
                selector: 'td:first-child + td + td'
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewRestDayApprovalsModal.open({ id: data.record.restDayApprovals.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "restDayApprovals.employeeId",
                    name: "employees.employeeCode"
                },
                {
                    targets: 3,
                    data: "restDayApprovals.employee",
                    name: "employees.lastName + employees.firstName + employees.middleName"
                },
                {
                    targets: 4,
                    data: "restDayApprovals.restDayStart",
                    name: "restDayStart",
                    render: function (restDayStart) {
                        if (restDayStart) {
                            return moment(restDayStart).format('MM/DD/YYYY');
                        }
                        return "";
                    }
                },
                {
                    targets: 5,
                    data: "restDayApprovals.restDayEnd",
                    name: "restDayEnd",
                    render: function (restDayEnd) {
                        if (restDayEnd) {
                            return moment(restDayEnd).format('MM/DD/YYYY');
                        }
                        return "";
                    }
                },
                {
                    targets: 6,
                    data: "restDayApprovals.restDays",
                    name: "restDayCode"
                },
                {
                    targets: 7,
                    data: "restDayApprovals.reason",
                    name: "restDayReasons.displayName"
                },
                {
                    targets: 8,
                    data: "restDayApprovals.remarks",
                    name: "remarks"
                },
                {
                    targets: 9,
                    data: "restDayApprovals.status",
                    name: "status.displayName"
                }
            ]
        });

        function getRestDayApprovals() {
            if ($('#EmployeeFilterName').val() == '') {
                $('#EmployeeFilterId').val('');
            }

            dataTable.ajax.reload();
        }

        abp.event.on('app.createOrEditRestDayApprovalsModalSaved', function () {
            getRestDayApprovals();
        });

        $('#BackButton').click(function () {
            window.location = "/App/RestDayApprovals";
        });

        $('#GetRestDayApprovalsButton').click(function (e) {
            e.preventDefault();
            getRestDayApprovals();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getRestDayApprovals();
            }
        });

        $('#AttendancePeriodFilterId, #StatusFilterId').change(function () {
            getRestDayApprovals();
        });

        $('#YearFilterId, #FrequencyFilterId').change(function () {
            var year = $('#YearFilterId').val();
            var frequency = $('#FrequencyFilterId').val();

            _attendancePeriodsService.getAttendancePeriodListByYear(year, frequency, null)
                .done(function (data) {
                    $('#AttendancePeriodFilterId').empty();

                    $(data).each(function (index) {
                        var isSelected = '';

                        if (data[index].selected == true) {
                            isSelected = 'selected';
                        }

                        $('#AttendancePeriodFilterId').append(
                            '<option value="' + data[index].value + '" ' + isSelected + '>' + data[index].text + '</option>'
                        );
                    });

                    getRestDayApprovals();
                });
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $('#EmployeeFilterName').autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeFilterId').val(ui.item.id);
                        $('#EmployeeFilterName').val(ui.item.label);
                        dataTable.ajax.reload();
                        return false;
                    }
                });
            });

        $('#ApproverFilterId').change(function () {
            getRestDayApprovals();
        });
    });
})();
