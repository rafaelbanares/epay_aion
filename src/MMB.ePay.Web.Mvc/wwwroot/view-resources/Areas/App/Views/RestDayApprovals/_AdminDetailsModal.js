(function ($) {
    app.modals.ViewRestDayApprovalsModal = function () {

        var _restDayApprovalsService = abp.services.app.restDayApprovals;
        var _modalManager;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
        };

        var _remarksRestDayApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RestDayApprovals/RemarksRestDayApprovalsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/RestDayApprovals/_AdminRemarksModal.js',
            modalClass: 'RemarksRestDayApprovalsModal'
        });

        $('.status_button').click(function () {

            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var reason = $('#RestDayRequests_Reason').val();

            _modalManager.setBusy(true);
            _restDayApprovalsService.adminUpdateStatus(
                { id, statusId, reason }
            ).done(function () {
                abp.notify.info(app.localize('Updated Successfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditRestDayApprovalsModalSaved');
                }).always(function() {
                _modalManager.setBusy(false);
            });
        });

        $('.remarks_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var status = $(this).data('status');

            _remarksRestDayApprovalsModal.open({ id: id, statusId: statusId, status: status });
            _modalManager.close();
        });
    };
})(jQuery);