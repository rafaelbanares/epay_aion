(function($) {
    app.modals.CreateOrEditEmployeeTimeInfoModal = function() {
        var _employeeTimeInfoService = abp.services.app.employeeTimeInfo;
        var _modalManager;
        var _$employeeTimeInfoInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$employeeTimeInfoInformationForm = _modalManager.getModal().find('form[name=EmployeeTimeInfoInformationsForm]');
            _$employeeTimeInfoInformationForm.validate();
        };
        this.save = function() {
            if (!_$employeeTimeInfoInformationForm.valid()) {
                return;
            }
            var employeeTimeInfo = _$employeeTimeInfoInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _employeeTimeInfoService.createOrEdit(
                employeeTimeInfo
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditEmployeeTimeInfoModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);