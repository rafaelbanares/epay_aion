﻿(function () {
    $(function () {

        var _$employeeTimeInfoTable = $('#EmployeeTimeInfoTable');
        var _employeeTimeInfoService = abp.services.app.employeeTimeInfo;
		
        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.EmployeeTimeInfo.Create'),
            edit: abp.auth.hasPermission('Pages.EmployeeTimeInfo.Edit'),
            'delete': abp.auth.hasPermission('Pages.EmployeeTimeInfo.Delete')
        };

         var _createOrEditModal = new app.ModalManager({
                    viewUrl: abp.appPath + 'App/EmployeeTimeInfo/CreateOrEditModal',
                    scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/EmployeeTimeInfo/_CreateOrEditModal.js',
                    modalClass: 'CreateOrEditEmployeeTimeInfoModal'
                });
                   

		 var _viewEmployeeTimeInfoModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/EmployeeTimeInfo/ViewemployeeTimeInfoModal',
            modalClass: 'ViewEmployeeTimeInfoModal'
        });

		
		

        var getDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT00:00:00Z"); 
        }
        
        var getMaxDateFilter = function (element) {
            if (element.data("DateTimePicker").date() == null) {
                return null;
            }
            return element.data("DateTimePicker").date().format("YYYY-MM-DDT23:59:59Z"); 
        }

        var dataTable = _$employeeTimeInfoTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _employeeTimeInfoService.getAll,
                inputFilter: function () {
                    return {
					filter: $('#EmployeeTimeInfoTableFilter').val(),
					restDayFilter: $('#RestDayFilterId').val(),
					overtimeFilter: $('#OvertimeFilterId').val(),
					undertimeFilter: $('#UndertimeFilterId').val(),
					tardyFilter: $('#TardyFilterId').val(),
					nonSwiperFilter: $('#NonSwiperFilterId').val(),
					minEmployeeIdFilter: $('#MinEmployeeIdFilterId').val(),
					maxEmployeeIdFilter: $('#MaxEmployeeIdFilterId').val(),
					minShiftTypeIdFilter: $('#MinShiftTypeIdFilterId').val(),
					maxShiftTypeIdFilter: $('#MaxShiftTypeIdFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
						{
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewEmployeeTimeInfoModal.open({ id: data.record.employeeTimeInfo.id });
                                }
                        },
						{
                            text: app.localize('Edit'),
                            iconStyle: 'far fa-edit mr-2',
                            visible: function () {
                                return _permissions.edit;
                            },
                            action: function (data) {
                            _createOrEditModal.open({ id: data.record.employeeTimeInfo.id });                                
                            }
                        }, 
						{
                            text: app.localize('Delete'),
                            iconStyle: 'far fa-trash-alt mr-2',
                            visible: function () {
                                return _permissions.delete;
                            },
                            action: function (data) {
                                deleteEmployeeTimeInfo(data.record.employeeTimeInfo);
                            }
                        }]
                    }
                },
					{
						targets: 2,
						 data: "employeeTimeInfo.restDay",
						 name: "restDay"   
					},
					{
						targets: 3,
						 data: "employeeTimeInfo.overtime",
						 name: "overtime"  ,
						render: function (overtime) {
							if (overtime) {
								return '<div class="text-center"><i class="fa fa-check text-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 4,
						 data: "employeeTimeInfo.undertime",
						 name: "undertime"  ,
						render: function (undertime) {
							if (undertime) {
								return '<div class="text-center"><i class="fa fa-check text-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 5,
						 data: "employeeTimeInfo.tardy",
						 name: "tardy"  ,
						render: function (tardy) {
							if (tardy) {
								return '<div class="text-center"><i class="fa fa-check text-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 6,
						 data: "employeeTimeInfo.nonSwiper",
						 name: "nonSwiper"  ,
						render: function (nonSwiper) {
							if (nonSwiper) {
								return '<div class="text-center"><i class="fa fa-check text-success" title="True"></i></div>';
							}
							return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
					}
			 
					},
					{
						targets: 7,
						 data: "employeeTimeInfo.employeeId",
						 name: "employeeId"   
					},
					{
						targets: 8,
						 data: "employeeTimeInfo.shiftTypeId",
						 name: "shiftTypeId"   
					}
            ]
        });

        function getEmployeeTimeInfo() {
            dataTable.ajax.reload();
        }

        function deleteEmployeeTimeInfo(employeeTimeInfo) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _employeeTimeInfoService.delete({
                            id: employeeTimeInfo.id
                        }).done(function () {
                            getEmployeeTimeInfo(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

		$('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewEmployeeTimeInfoButton').click(function () {
            _createOrEditModal.open();
        });        

		

        abp.event.on('app.createOrEditEmployeeTimeInfoModalSaved', function () {
            getEmployeeTimeInfo();
        });

		$('#GetEmployeeTimeInfoButton').click(function (e) {
            e.preventDefault();
            getEmployeeTimeInfo();
        });

		$(document).keypress(function(e) {
		  if(e.which === 13) {
			getEmployeeTimeInfo();
		  }
		});
		
		
		
    });
})();
