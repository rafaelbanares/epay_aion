(function($) {
    app.modals.CreateOrEditHolidaysModal = function() {
        var _holidaysService = abp.services.app.holidays;
        var _holidayTypesService = abp.services.app.holidayTypes;
        var _modalManager;
        var _$holidaysInformationForm = null;

        function toggleHalfDay(holidayTypeId) {
            _holidayTypesService.checkHalfDayEnabled(
                holidayTypeId
            ).done(function (isCompanyDeclared) {

                var checkbox = document.getElementById('Holidays_HalfDay');

                if (isCompanyDeclared) {
                    $('#Holidays_HalfDay').removeAttr('disabled');
                    $('#lblHalfDay').removeClass('checkbox-disabled')
                }
                else {
                    $('#Holidays_HalfDay').prop('disabled', 'disabled');
                    $('#lblHalfDay').addClass('checkbox-disabled')
                    checkbox.checked = false;
                }
            });
        }

        toggleHalfDay($('#Holidays_HolidayTypeId').val());

        $('#Holidays_HolidayTypeId').change(function () {
            var holidayTypeId = $(this).val();

            toggleHalfDay(holidayTypeId);
        });

        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$holidaysInformationForm = _modalManager.getModal().find('form[name=HolidaysInformationsForm]');
            _$holidaysInformationForm.validate();
        };
        this.save = function() {
            if (!_$holidaysInformationForm.valid()) {
                return;
            }
            var holidays = _$holidaysInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _holidaysService.createOrEdit(
                holidays
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditHolidaysModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);