(function($) {
    app.modals.CreateOrEditHolidayTypesModal = function() {
        var _holidayTypesService = abp.services.app.holidayTypes;
        var _modalManager;
        var _$holidayTypesInformationForm = null;

        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$holidayTypesInformationForm = _modalManager.getModal().find('form[name=HolidayTypesInformationsForm]');
            _$holidayTypesInformationForm.validate();
        };
        this.save = function() {
            if (!_$holidayTypesInformationForm.valid()) {
                return;
            }
            var holidayTypes = _$holidayTypesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _holidayTypesService.createOrEdit(
                holidayTypes
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditHolidayTypesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);