﻿(function () {
    $(function () {

        var _$holidayTypesTable = $('#HolidayTypesTable');
        var _holidayTypesService = abp.services.app.holidayTypes;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.HolidayTypes.Create'),
            edit: abp.auth.hasPermission('Pages.HolidayTypes.Edit'),
            'delete': abp.auth.hasPermission('Pages.HolidayTypes.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/HolidayTypes/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/HolidayTypes/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditHolidayTypesModal'
        });

        var _viewHolidayTypesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/HolidayTypes/ViewholidayTypesModal',
            modalClass: 'ViewHolidayTypesModal'
        });

        var dataTable = _$holidayTypesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _holidayTypesService.getAll,
                inputFilter: function () {
                    return {
                        yearFilter: $('#YearFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewHolidayTypesModal.open({ id: data.record.holidayTypes.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function (data) {
                                    if (data.record.holidayTypes.isEditable) {
                                        return _permissions.edit;
                                    }

                                    return false;
                                },
                                action: function (data) {
                                    _createOrEditModal.open({ id: data.record.holidayTypes.id });
                                }
                            },
                            {
                                text: app.localize('Delete'),
                                iconStyle: 'far fa-trash-alt mr-2',
                                visible: function (data) {
                                    if (data.record.holidayTypes.isEditable) {
                                        return _permissions.delete;
                                    }

                                    return false;
                                },
                                action: function (data) {
                                    deleteHolidayTypes(data.record.holidayTypes);
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "holidayTypes.displayName",
                    name: "displayName"
                },
                {
                    targets: 3,
                    data: "holidayTypes.description",
                    name: "description"
                },
                {
                    targets: 4,
                    data: "holidayTypes.halfDayEnabled",
                    name: "halfDayEnabled",
                    render: function (halfDayEnabled) {
                        if (halfDayEnabled) {
                            return '<div class="text-center"><i class="fa fa-check text-success" title="True"></i></div>';
                        }
                        return '<div class="text-center"><i class="fa fa-times-circle" title="False"></i></div>';
                    }

                }
            ]
        });

        function getHolidayTypes() {
            dataTable.ajax.reload();
        }

        function deleteHolidayTypes(holidayTypes) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _holidayTypesService.delete({
                            id: holidayTypes.id
                        }).done(function () {
                            getHolidayTypes(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        $('#CreateNewHolidayTypesButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditHolidayTypesModalSaved', function () {
            getHolidayTypes();
        });

        $('#GetHolidayTypesButton').click(function (e) {
            e.preventDefault();
            getHolidayTypes();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getHolidayTypes();
            }
        });

        $('#YearFilterId').change(function () {
            getHolidayTypes();
        });

    });
})();
