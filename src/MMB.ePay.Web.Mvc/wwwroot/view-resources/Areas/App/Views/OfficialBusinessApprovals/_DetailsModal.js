(function ($) {
    app.modals.ViewOfficialBusinessApprovalsModal = function () {

        var _officialBusinessApprovalsService = abp.services.app.officialBusinessApprovals;
        var _modalManager;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
        };

        var _remarksOfficialBusinessApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OfficialBusinessApprovals/RemarksOfficialBusinessApprovalsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OfficialBusinessApprovals/_RemarksModal.js',
            modalClass: 'RemarksOfficialBusinessApprovalsModal'
        });

        $('.status_button').click(function () {

            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var reason = $('#OfficialBusinessRequests_Reason').val();

            _modalManager.setBusy(true);
            _officialBusinessApprovalsService.updateStatus(
                { id, statusId, reason }
            ).done(function () {
                abp.notify.info(app.localize('Updated Successfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditOfficialBusinessApprovalsModalSaved');
                }).always(function() {
                _modalManager.setBusy(false);
            });
        });

        $('.remarks_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var status = $(this).data('status');

            _remarksOfficialBusinessApprovalsModal.open({ id: id, statusId: statusId, status: status });
            _modalManager.close();
        });
    };
})(jQuery);