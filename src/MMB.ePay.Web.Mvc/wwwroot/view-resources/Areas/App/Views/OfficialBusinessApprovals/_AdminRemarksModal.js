(function ($) {
    app.modals.RemarksOfficialBusinessApprovalsModal = function () {

        var _officialBusinessApprovalsService = abp.services.app.officialBusinessApprovals;
        var _modalManager;
        this.init = function (modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
        };

        $('.status_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var reason = $('#OfficialBusinessRequests_Reason').val();

            _modalManager.setBusy(true);
            _officialBusinessApprovalsService.adminUpdateStatus(
                { id, statusId, reason }
            ).done(function () {
                abp.notify.info(app.localize('Updated Successfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditOfficialBusinessApprovalsModalSaved');
            }).always(function () {
                _modalManager.setBusy(false);
            });
        });
    };
})(jQuery);