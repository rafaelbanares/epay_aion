﻿(function () {
    $(function () {
        var _$officialBusinessApprovalsTable = $('#OfficialBusinessApprovalsTable');
        var _officialBusinessApprovalsService = abp.services.app.officialBusinessApprovals;
        var _employeesService = abp.services.app.employees;

        var _viewOfficialBusinessApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OfficialBusinessApprovals/ViewofficialBusinessApprovalsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OfficialBusinessApprovals/_DetailsModal.js',
            modalClass: 'ViewOfficialBusinessApprovalsModal'
        });

        var dataTable = _$officialBusinessApprovalsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            rowId: 'officialBusinessApprovals.id',
            listAction: {
                ajaxFunction: _officialBusinessApprovalsService.getAll,
                inputFilter: function () {
                    return {
                        frequencyIdFilter: $('#FrequencyFilterId').val(),
                        employeeIdFilter: $('#EmployeeFilterId').val(),
                        isBackup: $('#ApproverFilterId').val()
                    };
                }
            },
            select: {
                style: 'multi',
                selector: 'td:first-child + td + td'
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewOfficialBusinessApprovalsModal.open({ id: data.record.officialBusinessApprovals.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    className: 'select-checkbox',
                    orderable: false,
                    data: null,
                    defaultContent: ''
                },
                {
                    targets: 3,
                    data: "officialBusinessApprovals.employeeId",
                    name: "employees.employeeCode"
                },
                {
                    targets: 4,
                    data: "officialBusinessApprovals.employee",
                    name: "employees.lastName + employees.firstName + employees.middleName"
                },
                {
                    targets: 5,
                    data: "officialBusinessApprovals.startDate",
                    name: "officialBusinessRequestDetails.first()",
                    render: function (startDate) {
                        if (startDate) {
                            return moment(startDate).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 6,
                    data: "officialBusinessApprovals.endDate",
                    name: "officialBusinessRequestDetails.first()",
                    render: function (endDate) {
                        if (endDate) {
                            return moment(endDate).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 7,
                    data: "officialBusinessApprovals.reason",
                    name: "officialBusinessReasons.displayName"
                },
                {
                    targets: 8,
                    data: "officialBusinessApprovals.remarks",
                    name: "remarks"
                },
                {
                    targets: 9,
                    data: "officialBusinessApprovals.status",
                    name: "status.displayName"
                }
            ]
        });

        dataTable.on('click', '#checkBox', function () {
            var checkBox = document.getElementById("checkBox");

            if (checkBox.checked == false) {
                dataTable.rows().deselect();
                checkBox.checked = false;
            } else {
                dataTable.rows().select();
                checkBox.checked = true;
            }
        }).on('select deselect', function () {
            ('Some selection or deselection going on')
            if (dataTable.rows({
                selected: true
            }).count() !== dataTable.rows().count()) {
                checkBox.checked = false;
            } else {
                checkBox.checked = true;
            }
        });

        function getOfficialBusinessApprovals() {
            if ($('#EmployeeFilterName').val() == '') {
                $('#EmployeeFilterId').val('');
            }

            dataTable.ajax.reload();
        }

        abp.event.on('app.createOrEditOfficialBusinessApprovalsModalSaved', function () {
            getOfficialBusinessApprovals();
        });

        $('#ApprovalRequestsButton').click(function () {
            window.location = "/App/OfficialBusinessApprovals/ApprovalRequests";
        });

        $('#GetOfficialBusinessApprovalsButton').click(function (e) {
            e.preventDefault();
            getOfficialBusinessApprovals();
        });

        $('#ApproveAllButton').click(function (e) {
            var ids = dataTable.rows({ selected: true }).ids().toArray();;

            if (ids.length > 0) {
                abp.ui.setBusy();
                _officialBusinessApprovalsService.approveAll(
                    { ids }
                ).done(function () {
                    abp.notify.info(app.localize('Updated Successfully'));
                    dataTable.rows().deselect();
                    $('#checkBox').removeClass("selected");
                    getOfficialBusinessApprovals();
                }).always(function () {
                    abp.ui.clearBusy();
                });
            }
            else {
                abp.notify.info(app.localize('No request selected.'));
            }
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getOfficialBusinessApprovals();
            }
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $('#EmployeeFilterName').autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeFilterId').val(ui.item.id);
                        $('#EmployeeFilterName').val(ui.item.label);
                        dataTable.ajax.reload();
                        return false;
                    }
                });
            });

        $('#ApproverFilterId, #FrequencyFilterId').change(function () {
            getOfficialBusinessApprovals();
        });
    });
})();
