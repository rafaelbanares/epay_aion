(function($) {
    app.modals.CreateOrEditOfficialBusinessApprovalsModal = function() {
        var _officialBusinessApprovalsService = abp.services.app.officialBusinessApprovals;
        var _modalManager;
        var _$officialBusinessApprovalsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$officialBusinessApprovalsInformationForm = _modalManager.getModal().find('form[name=OfficialBusinessApprovalsInformationsForm]');
            _$officialBusinessApprovalsInformationForm.validate();
        };
        this.save = function() {
            if (!_$officialBusinessApprovalsInformationForm.valid()) {
                return;
            }
            var officialBusinessApprovals = _$officialBusinessApprovalsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _officialBusinessApprovalsService.createOrEdit(
                officialBusinessApprovals
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditOfficialBusinessApprovalsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);