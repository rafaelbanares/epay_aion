﻿(function () {
    $(function () {
        var _$officialBusinessApprovalsTable = $('#OfficialBusinessApprovalsTable');
        var _officialBusinessApprovalsService = abp.services.app.officialBusinessApprovals;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;
        var _employeesService = abp.services.app.employees;

        var _viewOfficialBusinessApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OfficialBusinessApprovals/ViewofficialBusinessApprovalRequestsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OfficialBusinessApprovals/_DetailsModal.js',
            modalClass: 'ViewOfficialBusinessApprovalsModal'
        });

        var dataTable = _$officialBusinessApprovalsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            rowId: 'officialBusinessApprovals.id',
            listAction: {
                ajaxFunction: _officialBusinessApprovalsService.getApprovalRequests,
                inputFilter: function () {
                    return {
                        attendancePeriodIdFilter: $('#AttendancePeriodFilterId').val(),
                        employeeIdFilter: $('#EmployeeFilterId').val(),
                        isBackup: $('#ApproverFilterId').val()
                    };
                }
            },
            select: {
                style: 'multi',
                selector: 'td:first-child + td + td'
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewOfficialBusinessApprovalsModal.open({ id: data.record.officialBusinessApprovals.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "officialBusinessApprovals.employeeId",
                    name: "employees.employeeCode"
                },
                {
                    targets: 3,
                    data: "officialBusinessApprovals.employee",
                    name: "employees.lastName + employees.firstName + employees.middleName"
                },
                {
                    targets: 4,
                    data: "officialBusinessApprovals.startDate",
                    name: "officialBusinessRequestDetails.first()",
                    render: function (startDate) {
                        if (startDate) {
                            return moment(startDate).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 5,
                    data: "officialBusinessApprovals.endDate",
                    name: "officialBusinessRequestDetails.first()",
                    render: function (endDate) {
                        if (endDate) {
                            return moment(endDate).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 6,
                    data: "officialBusinessApprovals.reason",
                    name: "officialBusinessReasons.displayName"
                },
                {
                    targets: 7,
                    data: "officialBusinessApprovals.remarks",
                    name: "remarks"
                },
                {
                    targets: 8,
                    data: "officialBusinessApprovals.status",
                    name: "status.displayName"
                }
            ]
        });

        function getOfficialBusinessApprovals() {
            if ($('#EmployeeFilterName').val() == '') {
                $('#EmployeeFilterId').val('');
            }

            dataTable.ajax.reload();
        }

        abp.event.on('app.createOrEditOfficialBusinessApprovalsModalSaved', function () {
            getOfficialBusinessApprovals();
        });

        $('#BackButton').click(function () {
            window.location = "/App/OfficialBusinessApprovals";
        });

        $('#GetOfficialBusinessApprovalsButton').click(function (e) {
            e.preventDefault();
            getOfficialBusinessApprovals();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getOfficialBusinessApprovals();
            }
        });

        $('#AttendancePeriodFilterId').change(function () {
            getOfficialBusinessApprovals();
        });

        $('#YearFilterId, #FrequencyFilterId').change(function () {
            var year = $('#YearFilterId').val();
            var frequency = $('#FrequencyFilterId').val();

            _attendancePeriodsService.getAttendancePeriodListByYear(year, frequency, null)
                .done(function (data) {
                    $('#AttendancePeriodFilterId').empty();

                    $(data).each(function (index) {
                        var isSelected = '';

                        if (data[index].selected == true) {
                            isSelected = 'selected';
                        }

                        $('#AttendancePeriodFilterId').append(
                            '<option value="' + data[index].value + '" ' + isSelected + '>' + data[index].text + '</option>'
                        );
                    });

                    getOfficialBusinessApprovals();
                });
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $('#EmployeeFilterName').autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeFilterId').val(ui.item.id);
                        $('#EmployeeFilterName').val(ui.item.label);
                        dataTable.ajax.reload();
                        return false;
                    }
                });
            });

        $('#ApproverFilterId').change(function () {
            getOfficialBusinessApprovals();
        });
    });
})();
