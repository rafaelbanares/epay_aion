(function($) {
    app.modals.CreateOrEditExcuseApprovalsModal = function() {
        var _excuseApprovalsService = abp.services.app.excuseApprovals;
        var _modalManager;
        var _$excuseApprovalsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$excuseApprovalsInformationForm = _modalManager.getModal().find('form[name=ExcuseApprovalsInformationsForm]');
            _$excuseApprovalsInformationForm.validate();
        };
        this.save = function() {
            if (!_$excuseApprovalsInformationForm.valid()) {
                return;
            }
            var excuseApprovals = _$excuseApprovalsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _excuseApprovalsService.createOrEdit(
                excuseApprovals
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditExcuseApprovalsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);