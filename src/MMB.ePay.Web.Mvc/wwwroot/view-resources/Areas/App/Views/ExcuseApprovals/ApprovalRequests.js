﻿(function () {
    $(function () {
        var _$excuseApprovalsTable = $('#ExcuseApprovalsTable');
        var _excuseApprovalsService = abp.services.app.excuseApprovals;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;
        var _employeesService = abp.services.app.employees;

        var _viewExcuseApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ExcuseApprovals/ViewexcuseApprovalRequestsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ExcuseApprovals/_DetailsModal.js',
            modalClass: 'ViewExcuseApprovalsModal'
        });

        var dataTable = _$excuseApprovalsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            rowId: 'excuseApprovals.id',
            listAction: {
                ajaxFunction: _excuseApprovalsService.getApprovalRequests,
                inputFilter: function () {
                    return {
                        attendancePeriodIdFilter: $('#AttendancePeriodFilterId').val(),
                        employeeIdFilter: $('#EmployeeFilterId').val(),
                        isBackup: $('#ApproverFilterId').val()
                    };
                }
            },
            select: {
                style: 'multi',
                selector: 'td:first-child + td + td'
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewExcuseApprovalsModal.open({ id: data.record.excuseApprovals.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "excuseApprovals.employeeId",
                    name: "employees.employeeCode"
                },
                {
                    targets: 3,
                    data: "excuseApprovals.employee",
                    name: "employees.lastName + employees.firstName + employees.middleName"
                },
                {
                    targets: 4,
                    data: "excuseApprovals.excuseDate",
                    name: "excuseDate",
                    render: function (excuseDate) {
                        if (excuseDate) {
                            return moment(excuseDate).format('L');
                        }
                        return "";
                    }

                },
                {
                    targets: 5,
                    data: "excuseApprovals.excuseStart",
                    name: "excuseStart",
                    render: function (excuseStart) {
                        if (excuseStart) {
                            return moment(excuseStart).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 6,
                    data: "excuseApprovals.excuseEnd",
                    name: "excuseEnd",
                    render: function (excuseEnd) {
                        if (excuseEnd) {
                            return moment(excuseEnd).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 7,
                    data: "excuseApprovals.reason",
                    name: "excuseReasons.displayName"
                },
                {
                    targets: 8,
                    data: "excuseApprovals.remarks",
                    name: "remarks"
                },
                {
                    targets: 9,
                    data: "excuseApprovals.status",
                    name: "status.displayName"
                }
            ]
        });

        function getExcuseApprovals() {
            if ($('#EmployeeFilterName').val() == '') {
                $('#EmployeeFilterId').val('');
            }

            dataTable.ajax.reload();
        }

        abp.event.on('app.createOrEditExcuseApprovalsModalSaved', function () {
            getExcuseApprovals();
        });

        $('#BackButton').click(function () {
            window.location = "/App/ExcuseApprovals";
        });

        $('#GetExcuseApprovalsButton').click(function (e) {
            e.preventDefault();
            getExcuseApprovals();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getExcuseApprovals();
            }
        });

        $('#AttendancePeriodFilterId').change(function () {
            getExcuseApprovals();
        });

        $('#YearFilterId, #FrequencyFilterId').change(function () {
            var year = $('#YearFilterId').val();
            var frequency = $('#FrequencyFilterId').val();

            _attendancePeriodsService.getAttendancePeriodListByYear(year, frequency, null)
                .done(function (data) {
                    $('#AttendancePeriodFilterId').empty();

                    $(data).each(function (index) {
                        var isSelected = '';

                        if (data[index].selected == true) {
                            isSelected = 'selected';
                        }

                        $('#AttendancePeriodFilterId').append(
                            '<option value="' + data[index].value + '" ' + isSelected + '>' + data[index].text + '</option>'
                        );
                    });

                    getExcuseApprovals();
                });
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $('#EmployeeFilterName').autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeFilterId').val(ui.item.id);
                        $('#EmployeeFilterName').val(ui.item.label);
                        dataTable.ajax.reload();
                        return false;
                    }
                });
            });

        $('#ApproverFilterId').change(function () {
            getExcuseApprovals();
        });
    });
})();
