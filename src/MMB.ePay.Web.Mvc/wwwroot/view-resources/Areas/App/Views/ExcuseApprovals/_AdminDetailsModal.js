(function ($) {
    app.modals.ViewExcuseApprovalsModal = function () {

        var _excuseApprovalsService = abp.services.app.excuseApprovals;
        var _modalManager;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
        };

        var _remarksExcuseApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ExcuseApprovals/RemarksExcuseApprovalsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ExcuseApprovals/_AdminRemarksModal.js',
            modalClass: 'RemarksExcuseApprovalsModal'
        });

        $('.status_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var reason = $('#ExcuseRequests_Reason').val();

            _modalManager.setBusy(true);
            _excuseApprovalsService.adminUpdateStatus(
                { id, statusId, reason }
            ).done(function () {
                abp.notify.info(app.localize('Updated Successfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditExcuseApprovalsModalSaved');
                }).always(function() {
                _modalManager.setBusy(false);
            });
        });

        $('.remarks_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var status = $(this).data('status');

            _remarksExcuseApprovalsModal.open({ id: id, statusId: statusId, status: status });
            _modalManager.close();
        });
    };
})(jQuery);