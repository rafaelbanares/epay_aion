(function ($) {
    app.modals.DeleteEmailQueueModal = function () {
        var _emailQueueService = abp.services.app.emailQueue;
        var _modalManager;
        var _$emailQueueInformationForm = null;

        this.init = function (modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });

            _$emailQueueInformationForm = _modalManager.getModal().find('form[name=EmailQueueInformationsForm]');
            _$emailQueueInformationForm.validate();
        };
        this.save = function () {
            if (!_$emailQueueInformationForm.valid()) {
                return;
            }
            var emailQueue = _$emailQueueInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _emailQueueService.deleteAll(
                emailQueue
            ).done(function () {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.deleteEmailQueueModalSaved');
            }).always(function () {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);