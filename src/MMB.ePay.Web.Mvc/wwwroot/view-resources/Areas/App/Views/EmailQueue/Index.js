﻿(function () {
    $(function () {
        var _$emailQueueTable = $('#EmailQueueTable');
        var _emailQueueService = abp.services.app.emailQueue;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _deleteModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/EmailQueue/DeleteModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/EmailQueue/_DeleteModal.js',
            modalClass: 'DeleteEmailQueueModal'
        });

        var _viewEmailQueueModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/EmailQueue/ViewemailQueueModal',
            modalClass: 'ViewEmailQueueModal'
        });

        var dataTable = _$emailQueueTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _emailQueueService.getAll,
                inputFilter: function () {
                    return {
                        tenantFilter: $('#TenantFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewEmailQueueModal.open({ id: data.record.emailQueue.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "emailQueue.request",
                    name: "request"
                },
                {
                    targets: 3,
                    data: "emailQueue.fullName",
                    name: "fullName"
                },
                {
                    targets: 4,
                    data: "emailQueue.startDate",
                    name: "startDate",
                    render: function (date) {
                        if (date) {
                            return moment(date).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 5,
                    data: "emailQueue.endDate",
                    name: "endDate",
                    render: function (date) {
                        if (date) {
                            return moment(date).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 6,
                    data: "emailQueue.recipient",
                    name: "recipient"
                },
                {
                    targets: 7,
                    data: "emailQueue.status",
                    name: "status"
                },
                {
                    targets: 8,
                    data: "emailQueue.statusError",
                    name: "statusError"
                }
            ]
        });

        function getEmailQueue() {
            dataTable.ajax.reload();
        }

        $('#DeleteNewEmailQueueButton').click(function () {
            _deleteModal.open();
        });

        abp.event.on('app.deleteEmailQueueModalSaved', function () {
            getEmailQueue();
        });

        $('#GetEmailQueueButton').click(function (e) {
            e.preventDefault();
            getEmailQueue();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getEmailQueue();
            }
        });

        $('#TenantFilterId').change(function () {
            getEmailQueue();
        });

    });
})();
