(function ($) {
    app.modals.CreateOrEditExcuseRequestsModal = function () {
        $.getScript('/view-resources/Areas/App/Views/_Bundles/demo-ui-components.js');

        var _excuseRequestsService = abp.services.app.excuseRequests;
        var _modalManager;
        var _$excuseRequestsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$excuseRequestsInformationForm = _modalManager.getModal().find('form[name=ExcuseRequestsInformationsForm]');
            _$excuseRequestsInformationForm.validate();
        };
        this.save = function() {
            if (!_$excuseRequestsInformationForm.valid()) {
                return;
            }
            var excuseRequests = _$excuseRequestsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _excuseRequestsService.createOrEdit(
                excuseRequests
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditExcuseRequestsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);