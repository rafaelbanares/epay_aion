﻿(function () {
    $(function () {
        var _$excuseRequestsTable = $('#ExcuseRequestsTable');
        var _excuseRequestsService = abp.services.app.excuseRequests;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.ExcuseRequests.Create'),
            edit: abp.auth.hasPermission('Pages.ExcuseRequests.Edit')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ExcuseRequests/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ExcuseRequests/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditExcuseRequestsModal'
        });

        var _viewExcuseRequestsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ExcuseRequests/ViewexcuseRequestsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ExcuseRequests/_DetailsModal.js',
            modalClass: 'ViewExcuseRequestsModal'
        });

        var dataTable = _$excuseRequestsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _excuseRequestsService.getAll,
                inputFilter: function () {
                    return {
                        attendancePeriodIdFilter: $('#AttendancePeriodFilterId').val(),
                        statusIdFilter: $('#StatusFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewExcuseRequestsModal.open({ id: data.record.excuseRequests.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function (data) {
                                    if (data.record.excuseRequests.isEditable) {
                                        return _permissions.edit;
                                    } else {
                                        return false;
                                    }
                                },
                                action: function (data) {
                                    _createOrEditModal.open({ id: data.record.excuseRequests.id });
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "excuseRequests.excuseDate",
                    name: "excuseDate",
                    render: function (excuseDate) {
                        if (excuseDate) {
                            return moment(excuseDate).format('L');
                        }
                        return "";
                    }

                },
                {
                    targets: 3,
                    data: "excuseRequests.excuseStart",
                    name: "excuseStart",
                    render: function (excuseStart) {
                        if (excuseStart) {
                            return moment(excuseStart).format('hh:mm A');
                        }
                        return "";
                    }
                },
                {
                    targets: 4,
                    data: "excuseRequests.excuseEnd",
                    name: "excuseEnd",
                    render: function (excuseEnd) {
                        if (excuseEnd) {
                            return moment(excuseEnd).format('hh:mm A');
                        }
                        return "";
                    }
                },
                {
                    targets: 5,
                    data: "excuseRequests.reason",
                    name: "excuseReasons.displayName"
                },
                {
                    targets: 6,
                    data: "excuseRequests.remarks",
                    name: "remarks"
                },
                {
                    targets: 7,
                    data: "excuseRequests.status",
                    name: "status.displayName"
                }
            ]
        });

        function getExcuseRequests() {
            dataTable.ajax.reload();
        }

        $('#CreateNewExcuseRequestsButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditExcuseRequestsModalSaved', function () {
            getExcuseRequests();
        });

        $('#GetExcuseRequestsButton').click(function (e) {
            e.preventDefault();
            getExcuseRequests();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getExcuseRequests();
            }
        });

        $('#StatusFilterId, #AttendancePeriodFilterId').change(function () {
            getExcuseRequests();
        });

        $('#YearFilterId').change(function () {
            var year = $(this).val();

            _attendancePeriodsService.getAttendancePeriodListByYear(year, null, null)
                .done(function (data) {
                    $('#AttendancePeriodFilterId').empty();

                    $(data).each(function (index) {
                        var isSelected = '';

                        if (data[index].selected == true) {
                            isSelected = 'selected';
                        }

                        $('#AttendancePeriodFilterId').append(
                            '<option value="' + data[index].value + '" ' + isSelected + '>' + data[index].text + '</option>'
                        );
                    });

                    getExcuseRequests();
                });
        });

        $('#MinExcuseDateFilterId, #MaxExcuseDateFilterId').focusout(function () {
            getExcuseRequests();
        });
    });
})();
