(function ($) {
    app.modals.ViewShiftRequestsModal = function () {

        var _shiftRequestsService = abp.services.app.shiftRequests;
        var _modalManager;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
        };

        var _remarksShiftRequestsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ShiftRequests/RemarksShiftRequestsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ShiftRequests/_RemarksModal.js',
            modalClass: 'RemarksShiftRequestsModal'
        });

        $('.status_button').click(function () {

            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var reason = $('#ShiftRequests_Reason').val();

            _modalManager.setBusy(true);
            _shiftRequestsService.updateStatus(
                { id, statusId, reason }
            ).done(function () {
                abp.notify.info(app.localize('Updated Successfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditShiftRequestsModalSaved');
                }).always(function() {
                _modalManager.setBusy(false);
            });
        });

        $('.remarks_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var status = $(this).data('status');

            _remarksShiftRequestsModal.open({ id: id, statusId: statusId, status: status });
            _modalManager.close();
        });
    };
})(jQuery);