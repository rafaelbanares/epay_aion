(function($) {
    app.modals.CreateOrEditShiftRequestsModal = function() {
        var _shiftRequestsService = abp.services.app.shiftRequests;
        var _modalManager;
        var _$shiftRequestsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$shiftRequestsInformationForm = _modalManager.getModal().find('form[name=ShiftRequestsInformationsForm]');
            _$shiftRequestsInformationForm.validate();
        };
        this.save = function() {
            if (!_$shiftRequestsInformationForm.valid()) {
                return;
            }
            var shiftRequests = _$shiftRequestsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _shiftRequestsService.createOrEdit(
                shiftRequests
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditShiftRequestsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);