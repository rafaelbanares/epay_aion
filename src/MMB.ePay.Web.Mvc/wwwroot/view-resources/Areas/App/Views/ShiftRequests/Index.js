﻿(function () {
    $(function () {
        var _$shiftRequestsTable = $('#ShiftRequestsTable');
        var _shiftRequestsService = abp.services.app.shiftRequests;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.ShiftRequests.Create'),
            edit: abp.auth.hasPermission('Pages.ShiftRequests.Edit')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ShiftRequests/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ShiftRequests/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditShiftRequestsModal'
        });

        var _viewShiftRequestsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ShiftRequests/ViewshiftRequestsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ShiftRequests/_DetailsModal.js',
            modalClass: 'ViewShiftRequestsModal'
        });

        var dataTable = _$shiftRequestsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _shiftRequestsService.getAll,
                inputFilter: function () {
                    return {
                        attendancePeriodIdFilter: $('#AttendancePeriodFilterId').val(),
                        statusIdFilter: $('#StatusFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewShiftRequestsModal.open({ id: data.record.shiftRequests.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function (data) {
                                    if (data.record.shiftRequests.isEditable) {
                                        return _permissions.edit;
                                    } else {
                                        return false;
                                    }
                                },
                                action: function (data) {
                                    _createOrEditModal.open({ id: data.record.shiftRequests.id });
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "shiftRequests.shiftType",
                    name: "shiftTypes.displayName"
                },

                {
                    targets: 3,
                    data: "shiftRequests.shiftStart",
                    name: "shiftStart",
                    render: function (shiftStart) {
                        if (shiftStart) {
                            return moment(shiftStart).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 4,
                    data: "shiftRequests.shiftEnd",
                    name: "shiftEnd",
                    render: function (shiftEnd) {
                        if (shiftEnd) {
                            return moment(shiftEnd).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 5,
                    data: "shiftRequests.reason",
                    name: "shiftReasons.displayName"
                },
                {
                    targets: 6,
                    data: "shiftRequests.remarks",
                    name: "remarks"
                },
                {
                    targets: 7,
                    data: "shiftRequests.status",
                    name: "status.displayName"
                }
            ]
        });

        function getShiftRequests() {
            dataTable.ajax.reload();
        }

        $('#CreateNewShiftRequestsButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditShiftRequestsModalSaved', function () {
            getShiftRequests();
        });

        $('#GetShiftRequestsButton').click(function (e) {
            e.preventDefault();
            getShiftRequests();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getShiftRequests();
            }
        });

        $('#StatusFilterId, #AttendancePeriodFilterId').change(function () {
            getShiftRequests();
        });

        $('#YearFilterId').change(function () {
            var year = $(this).val();

            _attendancePeriodsService.getAttendancePeriodListByYear(year, null, null)
                .done(function (data) {
                    $('#AttendancePeriodFilterId').empty();

                    $(data).each(function (index) {
                        var isSelected = '';

                        if (data[index].selected == true) {
                            isSelected = 'selected';
                        }

                        $('#AttendancePeriodFilterId').append(
                            '<option value="' + data[index].value + '" ' + isSelected + '>' + data[index].text + '</option>'
                        );
                    });

                    getShiftRequests();
                });
        });

        $('#MinShiftDateFilterId, #MaxShiftDateFilterId').focusout(function () {
            getShiftRequests();
        })
    });
})();
