(function($) {
    app.modals.CreateOrEditCompaniesModal = function() {
        var _companiesService = abp.services.app.companies;
        var _modalManager;
        var _$companiesInformationForm = null;

        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$companiesInformationForm = _modalManager.getModal().find('form[name=CompaniesInformationsForm]');
            _$companiesInformationForm.validate();
        };
        this.save = function() {
            if (!_$companiesInformationForm.valid()) {
                return;
            }
            var companies = _$companiesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _companiesService.createOrEdit(
                companies
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditCompaniesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);