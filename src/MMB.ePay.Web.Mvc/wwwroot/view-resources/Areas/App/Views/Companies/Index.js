﻿(function () {
    $(function () {

        var _$companiesTable = $('#CompaniesTable');
        var _companiesService = abp.services.app.companies;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Companies.Create'),
            edit: abp.auth.hasPermission('Pages.Companies.Edit'),
            'delete': abp.auth.hasPermission('Pages.Companies.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Companies/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Companies/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditCompaniesModal'
        });

        var _viewCompaniesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Companies/ViewcompaniesModal',
            modalClass: 'ViewCompaniesModal'
        });

        var dataTable = _$companiesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _companiesService.getAll,
                inputFilter: function () {
                    return {
                        yearFilter: $('#YearFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewCompaniesModal.open({ id: data.record.companies.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return _permissions.edit;
                                },
                                action: function (data) {
                                    _createOrEditModal.open({ id: data.record.companies.id });
                                }
                            }
                            //{
                            //    text: app.localize('Delete'),
                            //    iconStyle: 'far fa-trash-alt mr-2',
                            //    visible: function () {
                            //        return _permissions.delete;
                            //    },
                            //    action: function (data) {
                            //        deleteCompanies(data.record.companies);
                            //    }
                            //}
                        ]
                    }
                },
                {
                    targets: 2,
                    data: "companies.displayName",
                    name: "displayName"
                },
                {
                    targets: 3,
                    data: "companies.address1",
                    name: "address1"
                },
                {
                    targets: 4,
                    data: "companies.address2",
                    name: "address2"
                },
                {
                    targets: 5,
                    data: "companies.orgUnitLevel1Name",
                    name: "orgUnitLevel1Name"
                },
                {
                    targets: 6,
                    data: "companies.orgUnitLevel2Name",
                    name: "orgUnitLevel2Name"
                },
                {
                    targets: 7,
                    data: "companies.orgUnitLevel3Name",
                    name: "orgUnitLevel3Name"
                },
            ]
        });

        function getCompanies() {
            dataTable.ajax.reload();
        }

        function deleteCompanies(companies) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _companiesService.delete({
                            id: companies.id
                        }).done(function () {
                            getCompanies(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        $('#CreateNewCompaniesButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditCompaniesModalSaved', function () {
            getCompanies();
        });

        $('#GetCompaniesButton').click(function (e) {
            e.preventDefault();
            getCompanies();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getCompanies();
            }
        });

        $('#YearFilterId').change(function () {
            getCompanies();
        });

    });
})();
