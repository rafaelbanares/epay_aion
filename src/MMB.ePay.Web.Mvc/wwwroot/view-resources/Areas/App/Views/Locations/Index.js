﻿(function () {
    $(function () {

        var _$locationsTable = $('#LocationsTable');
        var _locationsService = abp.services.app.locations;

        var _permissions = {
            create: abp.auth.hasPermission('Pages.Locations.Create'),
            edit: abp.auth.hasPermission('Pages.Locations.Edit'),
            'delete': abp.auth.hasPermission('Pages.Locations.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Locations/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Locations/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditLocationsModal'
        });

        var _viewLocationsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/Locations/ViewlocationsModal',
            modalClass: 'ViewLocationsModal'
        });

        var dataTable = _$locationsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _locationsService.getAll,
                inputFilter: function () {
                    return {
                        yearFilter: $('#YearFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewLocationsModal.open({ id: data.record.locations.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function (data) {
                                    //return _permissions.edit;
                                    return false;
                                },
                                action: function (data) {
                                    _createOrEditModal.open({ id: data.record.locations.id });
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "locations.description",
                    name: "description"
                }
            ]
        });

        function getLocations() {
            dataTable.ajax.reload();
        }

        $('#CreateNewLocationsButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditLocationsModalSaved', function () {
            getLocations();
        });

        $('#GetLocationsButton').click(function (e) {
            e.preventDefault();
            getLocations();
        });
    });
})();
