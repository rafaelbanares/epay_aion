(function($) {
    app.modals.CreateOrEditLocationsModal = function () {
        var _locationsService = abp.services.app.locations;
        var _modalManager;
        var _$locationsInformationForm = null;

        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$locationsInformationForm = _modalManager.getModal().find('form[name=LocationsInformationsForm]');
            _$locationsInformationForm.validate();
        };
        this.save = function() {
            if (!_$locationsInformationForm.valid()) {
                return;
            }
            var locations = _$locationsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _locationsService.createOrEdit(
                locations
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditLocationsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);