(function($) {
    app.modals.CreateOrEditShiftTypesModal = function () {
        //$('#ShiftTypes_RotateShiftWeekly').change(function () {
        //    $('#divDays').toggle(this.checked);
        //});

        function togglePrevDay() {
            var txtStartTime = $('#ShiftTypes_TimeIn').val();
            var txtEndTime = $('#ShiftTypes_TimeOut').val();

            var checkbox = document.getElementById('ShiftTypes_StartOnPreviousDay');

            if (txtStartTime != '' && txtEndTime != '') {
                var startTime = new Date("January 01, 2020 " + txtStartTime);
                var endTime = new Date("January 01, 2020 " + txtEndTime);

                if (startTime > endTime) {
                    $('#ShiftTypes_StartOnPreviousDay').removeAttr('disabled');
                    $('#lblStartOnPreviousDay').removeClass('checkbox-disabled')
                    //$('#divPrevDay').show();
                } else {
                    $('#ShiftTypes_StartOnPreviousDay').prop('disabled', 'disabled');
                    $('#lblStartOnPreviousDay').addClass('checkbox-disabled')
                    //$('#divPrevDay').hide();
                    checkbox.checked = false;
                }
            }
            else {
                $('#ShiftTypes_StartOnPreviousDay').prop('disabled', 'disabled');
                $('#lblStartOnPreviousDay').addClass('checkbox-disabled')
                //$('#divPrevDay').hide();
                checkbox.checked = false;
            }
        }

        togglePrevDay();

        $('#ShiftTypes_TimeIn, #ShiftTypes_TimeOut').change(function () {
            togglePrevDay();
        });

        var _shiftTypesService = abp.services.app.shiftTypes;
        var _modalManager;
        var _$shiftTypesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$shiftTypesInformationForm = _modalManager.getModal().find('form[name=ShiftTypesInformationsForm]');
            _$shiftTypesInformationForm.validate();
        };
        this.save = function() {
            if (!_$shiftTypesInformationForm.valid()) {
                return;
            }
            var shiftTypes = _$shiftTypesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _shiftTypesService.createOrEdit(
                shiftTypes
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditShiftTypesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);