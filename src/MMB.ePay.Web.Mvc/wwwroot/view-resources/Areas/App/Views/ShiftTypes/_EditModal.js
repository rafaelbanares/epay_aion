(function($) {
    app.modals.EditShiftTypesModal = function () {

        //$('#ShiftTypes_RotateShiftWeekly').change(function () {
        //    $('#divAlert').show();
        //});

        var _shiftTypesService = abp.services.app.shiftTypes;
        var _modalManager;
        var _$shiftTypesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$shiftTypesInformationForm = _modalManager.getModal().find('form[name=ShiftTypesInformationsForm]');
            _$shiftTypesInformationForm.validate();
        };
        this.save = function () {
            if (!_$shiftTypesInformationForm.valid()) {
                return;
            }
            var shiftTypes = _$shiftTypesInformationForm.serializeFormToObject();

            _modalManager.setBusy(true);
            _shiftTypesService.edit(
                shiftTypes
            ).done(function() {
                window.location.reload();
                //_modalManager.close();
                //abp.notify.info(app.localize('SavedSuccessfully'));
                //abp.event.trigger('app.createOrEditShiftTypeDetailsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);