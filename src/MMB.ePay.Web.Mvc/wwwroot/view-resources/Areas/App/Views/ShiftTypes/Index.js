﻿(function () {
    $(function () {

        var _$shiftTypesTable = $('#ShiftTypesTable');
        var _shiftTypesService = abp.services.app.shiftTypes;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.ShiftTypes.Create'),
            edit: abp.auth.hasPermission('Pages.ShiftTypes.Edit'),
            'delete': abp.auth.hasPermission('Pages.ShiftTypes.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ShiftTypes/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ShiftTypes/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditShiftTypesModal'
        });

        var _viewShiftTypesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ShiftTypes/ViewshiftTypesModal',
            modalClass: 'ViewShiftTypesModal'
        });

        var dataTable = _$shiftTypesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _shiftTypesService.getAll
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewShiftTypesModal.open({ id: data.record.shiftTypes.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function (data) {
                                    return !data.record.shiftTypes.inUse && _permissions.edit;
                                },
                                action: function (data) {

                                    if (data.record.shiftTypes.rotateShiftWeekly) {
                                        window.location = "/App/ShiftTypes/Details/" + data.record.shiftTypes.id;
                                    } else {
                                        _createOrEditModal.open({ id: data.record.shiftTypes.id });
                                    }
                                }
                            },
                            {
                                text: app.localize('Delete'),
                                iconStyle: 'far fa-trash-alt mr-2',
                                visible: function (data) {
                                    return !data.record.shiftTypes.inUse && _permissions.delete;
                                },
                                action: function (data) {
                                    deleteShiftTypes(data.record.shiftTypes);
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "shiftTypes.displayName",
                    name: "displayName"
                },
                {
                    targets: 3,
                    data: "shiftTypes.description",
                    name: "description"
                },
                {
                    targets: 4,
                    data: "shiftTypes.flexible1",
                    name: "flexible1"
                },
                {
                    targets: 5,
                    data: "shiftTypes.gracePeriod1",
                    name: "gracePeriod1"
                },
                {
                    targets: 6,
                    data: "shiftTypes.minimumOvertime",
                    name: "minimumOvertime"
                }
            ]
        });

        function getShiftTypes() {
            dataTable.ajax.reload();
        }

        function deleteShiftTypes(shiftTypes) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _shiftTypesService.delete(
                            shiftTypes.id
                        ).done(function () {
                            getShiftTypes(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        $('#CreateNewShiftTypesButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditShiftTypesModalSaved', function () {
            getShiftTypes();
        });

        $('#GetShiftTypesButton').click(function (e) {
            e.preventDefault();
            getShiftTypes();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getShiftTypes();
            }
        });

    });
})();
