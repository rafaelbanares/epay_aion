﻿(function () {
    $(function () {

        var _$attendanceApprovalsTable = $('#AttendanceApprovalsTable');
        var _attendanceApprovalsService = abp.services.app.attendanceApprovals;
        var _employeesService = abp.services.app.employees;

        var _viewAttendanceApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/AttendanceApprovals/ViewattendanceApprovalsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/AttendanceApprovals/_DetailsModal.js',
            modalClass: 'ViewAttendanceApprovalsModal'
        });

        var dataTable = _$attendanceApprovalsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            rowId: 'attendanceApprovals.id',
            listAction: {
                ajaxFunction: _attendanceApprovalsService.getAll,
                inputFilter: function () {
                    return {
                        frequencyIdFilter: $('#FrequencyFilterId').val(),
                        employeeIdFilter: $('#EmployeeFilterId').val(),
                        isBackup: $('#ApproverFilterId').val()
                    };
                }
            },
            select: {
                style: 'multi',
                selector: 'td:first-child + td + td'
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewAttendanceApprovalsModal.open({ id: data.record.attendanceApprovals.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    className: 'select-checkbox',
                    orderable: false,
                    data: null,
                    defaultContent: ''
                },
                {
                    targets: 3,
                    data: "attendanceApprovals.employeeId",
                    name: "employees.employeeCode"
                },
                {
                    targets: 4,
                    data: "attendanceApprovals.employee",
                    name: "employees.lastName + employees.firstName + employees.middlename"
                },
                {
                    targets: 5,
                    data: "attendanceApprovals.date",
                    name: "date",
                    render: function (timeLogDate) {
                        if (timeLogDate) {
                            return moment(timeLogDate).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 6,
                    data: "attendanceApprovals.timeIn",
                    name: "timeIn",
                    render: function (timeIn) {
                        if (timeIn) {
                            return moment(timeIn).format('hh:mm a');
                        }
                        return "";
                    }
                },
                {
                    targets: 7,
                    data: "attendanceApprovals.timeOut",
                    name: "timeOut",
                    render: function (timeOut) {
                        if (timeOut) {
                            return moment(timeOut).format('hh:mm a');
                        }
                        return "";
                    }
                },
                {
                    targets: 8,
                    data: "attendanceApprovals.reason",
                    name: "attendanceReason.displayName"
                },
                {
                    targets: 9,
                    data: "attendanceApprovals.remarks",
                    name: "remarks"
                },
                {
                    targets: 10,
                    data: "attendanceApprovals.status",
                    name: "status.displayName"
                }
            ]
        });

        dataTable.on('click', '#checkBox', function () {
            var checkBox = document.getElementById("checkBox");

            if (checkBox.checked == false) {
                dataTable.rows().deselect();
                checkBox.checked = false;
            } else {
                dataTable.rows().select();
                checkBox.checked = true;
            }
        }).on('select deselect', function () {
            ('Some selection or deselection going on')
            if (dataTable.rows({
                selected: true
            }).count() !== dataTable.rows().count()) {
                checkBox.checked = false;
            } else {
                checkBox.checked = true;
            }
        });

        function getAttendanceApprovals() {
            if ($('#EmployeeFilterName').val() == '') {
                $('#EmployeeFilterId').val('');
            }

            dataTable.ajax.reload();
        }

        abp.event.on('app.createOrEditAttendanceApprovalsModalSaved', function () {
            getAttendanceApprovals();
        });

        $('#ApprovalRequestsButton').click(function () {
            window.location = "/App/AttendanceApprovals/ApprovalRequests";
        });

        $('#GetAttendanceApprovalsButton').click(function (e) {
            e.preventDefault();
            getAttendanceApprovals();
        });

        $('#ApproveAllButton').click(function (e) {
            var ids = dataTable.rows({ selected: true }).ids().toArray();;

            if (ids.length > 0) {
                abp.ui.setBusy();
                _attendanceApprovalsService.approveAll(
                    { ids }
                ).done(function () {
                    abp.notify.info(app.localize('Updated Successfully'));
                    dataTable.rows().deselect();
                    $('#checkBox').removeClass("selected");
                    getAttendanceApprovals();
                }).always(function () {
                    abp.ui.clearBusy();
                });
            }
            else {
                abp.notify.info(app.localize('No request selected.'));
            }
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getAttendanceApprovals();
            }
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $('#EmployeeFilterName').autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeFilterId').val(ui.item.id);
                        $('#EmployeeFilterName').val(ui.item.label);
                        dataTable.ajax.reload();
                        return false;
                    }
                });
            });

        $('#ApproverFilterId, #FrequencyFilterId').change(function () {
            getAttendanceApprovals();
        });
    });
})();
