(function($) {
    app.modals.CreateOrEditAttendanceApprovalsModal = function() {
        var _attendanceApprovalsService = abp.services.app.attendanceApprovals;
        var _modalManager;
        var _$attendanceApprovalsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$attendanceApprovalsInformationForm = _modalManager.getModal()
                .find('form[name=AttendanceApprovalsInformationsForm]');
            _$attendanceApprovalsInformationForm.validate();
        };
        this.save = function() {
            if (!_$attendanceApprovalsInformationForm.valid()) {
                return;
            }
            var attendanceApprovals = _$attendanceApprovalsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _attendanceApprovalsService.createOrEdit(
                attendanceApprovals
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditAttendanceApprovalsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);