﻿(function () {
    $(function () {
        var _$attendanceApprovalsTable = $('#AttendanceApprovalsTable');
        var _attendanceApprovalsService = abp.services.app.attendanceApprovals;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;
        var _employeesService = abp.services.app.employees;

        var _viewAttendanceApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/AttendanceApprovals/ViewattendanceApprovalsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/AttendanceApprovals/_AdminDetailsModal.js',
            modalClass: 'ViewAttendanceApprovalsModal'
        });

        var dataTable = _$attendanceApprovalsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            rowId: 'attendanceApprovals.id',
            listAction: {
                ajaxFunction: _attendanceApprovalsService.getAdminApprovals,
                inputFilter: function () {
                    return {
                        frequencyIdFilter: $('#FrequencyFilterId').val(),
                        attendancePeriodIdFilter: $('#AttendancePeriodFilterId').val(),
                        employeeIdFilter: $('#EmployeeFilterId').val(),
                        isBackup: $('#ApproverFilterId').val(),
                        statusTypeIdFilter: $('#StatusFilterId').val()
                    };
                }
            },
            select: {
                style: 'multi',
                selector: 'td:first-child + td + td'
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewAttendanceApprovalsModal.open({ id: data.record.attendanceApprovals.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "attendanceApprovals.employeeId",
                    name: "employees.employeeCode"
                },
                {
                    targets: 3,
                    data: "attendanceApprovals.employee",
                    name: "employees.lastName + employees.firstName + employees.middlename"
                },
                {
                    targets: 4,
                    data: "attendanceApprovals.date",
                    name: "date",
                    render: function (timeLogDate) {
                        if (timeLogDate) {
                            return moment(timeLogDate).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 5,
                    data: "attendanceApprovals.timeIn",
                    name: "timeIn",
                    render: function (timeIn) {
                        if (timeIn) {
                            return moment(timeIn).format('hh:mm a');
                        }
                        return "";
                    }
                },
                {
                    targets: 6,
                    data: "attendanceApprovals.timeOut",
                    name: "timeOut",
                    render: function (timeOut) {
                        if (timeOut) {
                            return moment(timeOut).format('hh:mm a');
                        }
                        return "";
                    }
                },
                {
                    targets: 7,
                    data: "attendanceApprovals.reason",
                    name: "attendanceReason.displayName"
                },
                {
                    targets: 8,
                    data: "attendanceApprovals.remarks",
                    name: "remarks"
                },
                {
                    targets: 9,
                    data: "attendanceApprovals.status",
                    name: "status.displayName"
                }
            ]
        });

        function getAttendanceApprovals() {
            if ($('#EmployeeFilterName').val() == '') {
                $('#EmployeeFilterId').val('');
            }

            dataTable.ajax.reload();
        }

        abp.event.on('app.createOrEditAttendanceApprovalsModalSaved', function () {
            getAttendanceApprovals();
        });

        $('#BackButton').click(function () {
            window.location = "/App/AttendanceApprovals";
        });

        $('#GetAttendanceApprovalsButton').click(function (e) {
            e.preventDefault();
            getAttendanceApprovals();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getAttendanceApprovals();
            }
        });

        $('#AttendancePeriodFilterId, #StatusFilterId').change(function () {
            getAttendanceApprovals();
        });

        $('#YearFilterId, #FrequencyFilterId').change(function () {
            var year = $('#YearFilterId').val();
            var frequency = $('#FrequencyFilterId').val();

            _attendancePeriodsService.getAttendancePeriodListByYear(year, frequency, null)
                .done(function (data) {
                    $('#AttendancePeriodFilterId').empty();

                    $(data).each(function (index) {
                        var isSelected = '';

                        if (data[index].selected == true) {
                            isSelected = 'selected';
                        }

                        $('#AttendancePeriodFilterId').append(
                            '<option value="' + data[index].value + '" ' + isSelected + '>' + data[index].text + '</option>'
                        );
                    });

                    getAttendanceApprovals();
                });
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $('#EmployeeFilterName').autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeFilterId').val(ui.item.id);
                        $('#EmployeeFilterName').val(ui.item.label);
                        dataTable.ajax.reload();
                        return false;
                    }
                });
            });

        $('#ApproverFilterId').change(function () {
            getAttendanceApprovals();
        });
    });
})();
