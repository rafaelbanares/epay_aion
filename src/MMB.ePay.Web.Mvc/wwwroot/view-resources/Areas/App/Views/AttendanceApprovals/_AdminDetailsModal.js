(function ($) {
    app.modals.ViewAttendanceApprovalsModal = function () {

        var _attendanceApprovalsService = abp.services.app.attendanceApprovals;
        var _modalManager;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
        };

        var _remarksAttendanceApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/AttendanceApprovals/RemarksAttendanceApprovalsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/AttendanceApprovals/_AdminRemarksModal.js',
            modalClass: 'RemarksAttendanceApprovalsModal'
        });

        $('.status_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var reason = $('#AttendanceRequests_Reason').val();

            _modalManager.setBusy(true);
            _attendanceApprovalsService.adminUpdateStatus(
                { id, statusId, reason }
            ).done(function () {
                abp.notify.info(app.localize('Updated Successfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditAttendanceApprovalsModalSaved');
            }).always(function () {
                _modalManager.setBusy(false);
            });
        });

        $('.remarks_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var status = $(this).data('status');

            _remarksAttendanceApprovalsModal.open({ id: id, statusId: statusId, status: status });
            _modalManager.close();
        });
    };
})(jQuery);