﻿(function () {
    $(function () {

        var _$overtimeTypesTable = $('#OvertimeTypesTable');
        var _overtimeTypesService = abp.services.app.overtimeTypes;

        var _permissions = {
            create: abp.auth.hasPermission('Pages.OvertimeTypes.Create'),
            edit: abp.auth.hasPermission('Pages.OvertimeTypes.Edit'),
            'delete': abp.auth.hasPermission('Pages.OvertimeTypes.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OvertimeTypes/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OvertimeTypes/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditOvertimeTypesModal'
        });

        var dataTable = _$overtimeTypesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _overtimeTypesService.getAll
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return _permissions.edit;
                                },
                                action: function (data) {

                                    if (data.record.overtimeTypes.rotateOvertimeWeekly) {
                                        window.location = "/App/OvertimeTypes/Details/" + data.record.overtimeTypes.id;
                                    } else {
                                        _createOrEditModal.open({ id: data.record.overtimeTypes.id });
                                    }

                                }
                            },
                            {
                                text: app.localize('Delete'),
                                iconStyle: 'far fa-trash-alt mr-2',
                                visible: function () {
                                    return _permissions.delete;
                                },
                                action: function (data) {
                                    deleteOvertimeTypes(data.record.overtimeTypes);
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "overtimeTypes.shortName",
                    name: "shortName"
                },
                {
                    targets: 3,
                    data: "overtimeTypes.description",
                    name: "description"
                },
                {
                    targets: 4,
                    data: "overtimeTypes.otCode",
                    name: "otCode"
                },
                {
                    targets: 5,
                    data: "overtimeTypes.displayFormat",
                    name: "displayFormat"
                }
            ]
        });

        function getOvertimeTypes() {
            dataTable.ajax.reload();
        }

        function deleteOvertimeTypes(overtimeTypes) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _overtimeTypesService.delete({
                            id: overtimeTypes.id
                        }).done(function () {
                            getOvertimeTypes(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        $('#CreateNewOvertimeTypesButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditOvertimeTypesModalSaved', function () {
            getOvertimeTypes();
        });

        $('#GetOvertimeTypesButton').click(function (e) {
            e.preventDefault();
            getOvertimeTypes();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getOvertimeTypes();
            }
        });

    });
})();
