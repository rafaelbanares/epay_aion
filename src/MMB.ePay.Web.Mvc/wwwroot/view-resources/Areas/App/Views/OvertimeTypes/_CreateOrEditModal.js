(function($) {
    app.modals.CreateOrEditOvertimeTypesModal = function () {
        var _overtimeTypesService = abp.services.app.overtimeTypes;
        var _modalManager;
        var _$overtimeTypesInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$overtimeTypesInformationForm = _modalManager.getModal().find('form[name=OvertimeTypesInformationsForm]');
            _$overtimeTypesInformationForm.validate();
        };
        this.save = function() {
            if (!_$overtimeTypesInformationForm.valid()) {
                return;
            }
            var overtimeTypes = _$overtimeTypesInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _overtimeTypesService.createOrEdit(
                overtimeTypes
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditOvertimeTypesModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);