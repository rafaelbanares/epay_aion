(function () {
    $(function () {
        var _reportsService = abp.services.app.reports;
        var _employeesService = abp.services.app.employees;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;
        var _organizationUnitService = abp.services.app.organizationUnit;

        _employeesService.getEmployeeList()
            .done(function (data) {

                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $("#EmployeeList").autocomplete({
                    source: employees,
                    focus: function (event, ui) {
                        $("#EmployeeList").val(ui.item.label);
                        return false;
                    },
                    select: function (event, ui) {
                        $("#EmployeeList").val(ui.item.label);
                        $('#Reports_EmployeeId').val(ui.item.id);
                        return false;
                    },
                    minLength: 3,
                    messages: {
                        noResults: 'No results found.'
                    }
                });
            });

        $('#Reports_OrganizationUnitId1').change(function () {
            var parentId = $(this).val();

            if (parentId > 0) {
                _organizationUnitService.getOrganizationUnitListByParent(parentId)
                    .done(function (data) {
                        $('#Reports_OrganizationUnitId2').empty();
                        $('#Reports_OrganizationUnitId3').empty();

                        if (data.length > 1) {
                            $('#divGroup2').show();

                            $(data).each(function (index) {
                                $('#Reports_OrganizationUnitId2').append(
                                    '<option value="' + data[index].value + '">' + data[index].text + '</option>'
                                );
                            });

                        } else {
                            $('#Reports_OrganizationUnitId2').empty();
                            $('#divGroup2').hide();
                            $('#Reports_OrganizationUnitId3').empty();
                            $('#divGroup3').hide();
                        };
                    });

            } else {
                $('#Reports_OrganizationUnitId2').empty();
                $('#divGroup2').hide();
                $('#Reports_OrganizationUnitId3').empty();
                $('#divGroup3').hide();
            }
        });

        $('#Reports_OrganizationUnitId2').change(function () {
            var parentId = $(this).val();

            if (parentId > 0) {
                _organizationUnitService.getOrganizationUnitListByParent(parentId)
                    .done(function (data) {
                        $('#Reports_OrganizationUnitId3').empty();

                        if (data.length > 1) {
                            $('#divGroup3').show();

                            $(data).each(function (index) {
                                $('#Reports_OrganizationUnitId3').append(
                                    '<option value="' + data[index].value + '">' + data[index].text + '</option>'
                                );
                            });

                        } else {
                            $('#Reports_OrganizationUnitId3').empty();
                            $('#divGroup3').hide();
                        }
                    });

            } else {
                $('#Reports_OrganizationUnitId3').empty();
                $('#divGroup3').hide();
            }
        });

        $('#EmployeeList').keyup(function () {
            if ($(this).val() == '') {
                $('#Reports_EmployeeId').val('0');
            };
        });

        if ($('#AttendancePeriodId').val() > 0) {
            var selectedText = $('#AttendancePeriodId option:selected').html().split(" - ");

            $('#MinDateFilterId').val(selectedText[0]);
            $('#MaxDateFilterId').val(selectedText[1]);
        }

        $('#CoveringPeriodFilterId').change(function () {
            var coveringPeriod = $('#CoveringPeriodFilterId').val();
            var divPeriodic = $('#divPeriodic');
            var divMonthly = $('#divMonthly');

            if (coveringPeriod == 'P') {
                divPeriodic.show();
                divMonthly.hide();

                AttendancePeriodUpdate();

            } else if (coveringPeriod == 'M') {
                divPeriodic.hide();
                divMonthly.show();

                AttendanceMonthlyUpdate();
            }
        });

        $('#YearFilterId, #Reports_PostFilterId, #FrequencyFilterId, #MonthFilterId').change(function () {
            var coveringPeriod = $('#CoveringPeriodFilterId').val();

            if (coveringPeriod == 'P') {
                var year = $('#YearFilterId').val();
                var frequency = $('#FrequencyFilterId').val();
                var post = $('#Reports_PostFilterId').val();

                _attendancePeriodsService.getAttendancePeriodListByYear(year, frequency, post)
                    .done(function (data) {
                        $('#AttendancePeriodId').empty();

                        $(data).each(function (index) {
                            $('#AttendancePeriodId').append(
                                '<option value="' + data[index].value + '">' + data[index].text + '</option>'
                            );
                        });

                        AttendancePeriodUpdate();
                    });
            }
            else if (coveringPeriod == 'M') {
                AttendanceMonthlyUpdate();
            }
        });

        $('#AttendancePeriodId').change(function () {
            AttendancePeriodUpdate();

            //if ($(this).val() > 0) {

            //    var selectedText = $('#AttendancePeriodId option:selected').html().split(" - ");

            //    $('#MinDateFilterId').val(selectedText[0]);
            //    $('#MaxDateFilterId').val(selectedText[1]);

            //} else {
            //    $('#MinDateFilterId').val('');
            //    $('#MaxDateFilterId').val('');
            //}
        });

        function AttendanceMonthlyUpdate() {
            var year = $('#YearFilterId').val();
            var month = $('#MonthFilterId').val();

            var firstDay = moment(new Date(year, month - 1, 1)).format('MMM D, YYYY');
            var lastDay = moment(new Date(year, month, 0)).format('MMM D, YYYY');

            $('#MinDateFilterId').val(firstDay);
            $('#MaxDateFilterId').val(lastDay);
        }

        function AttendancePeriodUpdate() {
            var attendancePeriodId = $('#AttendancePeriodId option:selected').val();

            if (attendancePeriodId > 0) {

                var selectedText = $('#AttendancePeriodId option:selected').html().split(" - ");

                $('#MinDateFilterId').val(selectedText[0]);
                $('#MaxDateFilterId').val(selectedText[1]);

                $('#btnGenerate').prop('disabled', false);

            } else {
                $('#MinDateFilterId').val('');
                $('#MaxDateFilterId').val('');

                $('#btnGenerate').prop('disabled', true);
            }
        }

        $('#Reports_ReportTypeId').change(function () {
            var reportId = $(this).val();

            if (reportId != 0) {
                $('#divOutput').show();

                _reportsService.getReportsForView(reportId)
                    .done(function (data) {

                        let rbtnExcel = document.getElementById("Excel");
                        let rbtnPdf = document.getElementById("Pdf");

                        if (data.reports.output === 'P') {
                            //$('input[name=excel]').prop("checked", false);
                            //$('input[name=pdf]').prop("checked", true);

                            rbtnExcel.checked = false;
                            rbtnPdf.checked = true;
                        }
                        else if (data.reports.output === 'E') {
                            //$('input[name=pdf]').checked = false;
                            //$('input[name=excel]').checked = true;

                            rbtnExcel.checked = true;
                            rbtnPdf.checked = false;
                        }

                        if (data.reports.hasEmployeeFilter) {
                            $('#divEmployee').show();
                        }
                        else {
                            $('#divEmployee').hide();
                        }

                        if (data.reports.hasPeriodFilter) {
                            $('#divPeriod').show();
                        }
                        else {
                            $('#divPeriod').hide();
                        }
                    });
            }
            else {
                $('#divEmployee').hide();
                $('#divPeriod').hide();
                $('#divCoveringPeriod').hide();
                $('#divOutput').hide();
            }

        });


        //$('#btnGenerate').click(function () {
        //    var reportType = $('#Reports_ReportTypeId').val();
        //    var employeeId = $('#Reports_EmployeeId').val();
        //    var minDate = $('#MinDateFilterId').val();
        //    var maxDate = $('#MaxDateFilterId').val();
        //    var output = $('input[name=output]').val();

        //    var url = "/App/Reports/" + reportType
        //        + '?employeeId=' + employeeId
        //        + '&minDate' + minDate
        //        + '&maxDate' + maxDate
        //        + '&output' + output;

        //    window.open(url, '_blank');
        //}); 

    });
})();