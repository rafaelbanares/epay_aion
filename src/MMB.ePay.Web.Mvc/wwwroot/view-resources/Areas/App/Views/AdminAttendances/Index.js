﻿(function () {
    $(function () {
        var _$attendancesTable = $('#AttendancesTable');
        var _attendancesService = abp.services.app.attendances;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;
        var _employeesService = abp.services.app.employees;
        var hasPMBreak = $('#HasPMBreak').val().toLowerCase() == 'true';

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _viewAttendancesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/AdminAttendances/ViewattendancesModal',
            modalClass: 'ViewAttendancesModal'
        });

        var _viewAttendancesRawEntriesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RawTimeLogs/ViewattendancesRawEntriesModal',
            modalClass: 'ViewAttendancesRawEntriesModal'
        });

        var dataTable = _$attendancesTable.DataTable({
            paging: false,
            ordering: false,
            serverSide: true,
            processing: true,
            lengthChange: false,
            pageLength: 25,
            listAction: {
                ajaxFunction: _attendancesService.getAll,
                inputFilter: function () {
                    return {
                        attendancePeriodIdFilter: $('#AttendancePeriodFilterId').val(),
                        employeeIdFilter: $('#EmployeeFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewAttendancesModal.open({ id: data.record.attendances.id, isPosted: data.record.attendances.isPosted });
                                }
                            },
                            {
                                text: app.localize('RawEntries'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {

                                    _viewAttendancesRawEntriesModal.open({ id: data.record.attendances.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function (data) {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "attendances.date",
                    name: "date",
                    render: function (date) {
                        if (date) {
                            return moment(date).format('L');
                        }
                        return "";
                    }

                },
                {
                    targets: 3,
                    data: "attendances.day",
                    name: "date"
                },
                {
                    targets: 4,
                    data: "attendances.shift",
                    name: "shiftTypes.displayName"
                },
                {
                    targets: 5,
                    data: "attendances.timeIn",
                    name: "timeIn",
                    render: function (timeIn) {
                        if (timeIn) {
                            return moment(timeIn).format('hh:mm A');
                        }
                        return "";
                    }

                },
                {
                    targets: 6,
                    data: "attendances.breaktimeIn",
                    name: "breaktimeIn",
                    render: function (breaktimeIn) {
                        if (breaktimeIn) {
                            return moment(breaktimeIn).format('hh:mm A');
                        }
                        return "";
                    }

                },
                {
                    targets: 7,
                    data: "attendances.breaktimeOut",
                    name: "breaktimeOut",
                    render: function (breaktimeOut) {
                        if (breaktimeOut) {
                            return moment(breaktimeOut).format('hh:mm A');
                        }
                        return "";
                    }

                },
                {
                    targets: 8,
                    data: "attendances.pmBreaktimeIn",
                    name: "pmBreaktimeIn",
                    render: function (pmBreaktimeIn) {
                        if (pmBreaktimeIn) {
                            return moment(pmBreaktimeIn).format('hh:mm A');
                        }
                        return "";
                    },
                    visible: hasPMBreak
                },
                {
                    targets: 9,
                    data: "attendances.pmBreaktimeOut",
                    name: "pmBreaktimeOut",
                    render: function (pmBreaktimeOut) {
                        if (pmBreaktimeOut) {
                            return moment(pmBreaktimeOut).format('hh:mm A');
                        }
                        return "";
                    },
                    visible: hasPMBreak
                },
                {
                    targets: 10,
                    data: "attendances.timeOut",
                    name: "timeOut",
                    render: function (timeOut) {
                        if (timeOut) {
                            return moment(timeOut).format('hh:mm A');
                        }
                        return "";
                    }
                },
                {
                    targets: 11,
                    data: "attendances.remarks",
                    name: "remarks"
                }
            ],
            createdRow: function (row, data, dataIndex) {
                $(row).css("background-blend-mode", "lighten");

                if (data.attendances.dayType == "HAR") {
                    $(row).css("background-color", "coral");
                } else if (data.attendances.dayType == "HOL") {
                    $(row).css("background-color", "skyblue");
                } else if (data.attendances.dayType == "RES") {
                    $(row).css("background-color", "darkgray");
                } else {
                    $(row).css("background-color", "white");
                }
            }
        });

        function getAttendances() {
            if ($('#EmployeeFilterName').val() == '') {
                $('#EmployeeFilterId').val('');
            }

            dataTable.ajax.reload();
        }

        abp.event.on('app.createOrEditAttendancesModalSaved', function () {
            getAttendances();
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $('#EmployeeFilterName').autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeFilterId').val(ui.item.id);
                        $('#EmployeeFilterName').val(ui.item.label);

                        UpdateAttendancePeriod();
                        return false;
                    }
                });
            });

        $('#GetAttendancesButton').click(function (e) {
            e.preventDefault();
            getAttendances();
        });

        $('#AttendancePeriodFilterId').change(function () {
            getAttendances();
        });

        $('#YearFilterId').change(function () {
            UpdateAttendancePeriod();
        });

        function UpdateAttendancePeriod() {
            var year = $('#YearFilterId').val();
            var employeeId = $('#EmployeeFilterId').val();

            _attendancePeriodsService.getAttendancePeriodListByEmployeeId(year, employeeId, null)
                .done(function (data) {
                    $('#AttendancePeriodFilterId').empty();

                    $(data).each(function (index) {

                        var isSelected = '';

                        if (data[index].selected == true) {
                            isSelected = 'selected';
                        }

                        $('#AttendancePeriodFilterId').append(
                            '<option value="' + data[index].value + '" ' + isSelected + '>' + data[index].text + '</option>'
                        );
                    });

                    getAttendances();
                });
        }

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getAttendances();
            }
        });

    });
})();
