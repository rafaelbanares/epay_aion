(function($) {
    app.modals.CreateOrEditShiftApprovalsModal = function() {
        var _shiftApprovalsService = abp.services.app.shiftApprovals;
        var _modalManager;
        var _$shiftApprovalsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$shiftApprovalsInformationForm = _modalManager.getModal().find('form[name=ShiftApprovalsInformationsForm]');
            _$shiftApprovalsInformationForm.validate();
        };
        this.save = function() {
            if (!_$shiftApprovalsInformationForm.valid()) {
                return;
            }
            var shiftApprovals = _$shiftApprovalsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _shiftApprovalsService.createOrEdit(
                shiftApprovals
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditShiftApprovalsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);