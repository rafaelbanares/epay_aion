﻿(function () {
    $(function () {
        var _$shiftApprovalsTable = $('#ShiftApprovalsTable');
        var _shiftApprovalsService = abp.services.app.shiftApprovals;
        var _employeesService = abp.services.app.employees;

        var _viewShiftApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ShiftApprovals/ViewshiftApprovalsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ShiftApprovals/_DetailsModal.js',
            modalClass: 'ViewShiftApprovalsModal'
        });

        var dataTable = _$shiftApprovalsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            rowId: 'shiftApprovals.id',
            listAction: {
                ajaxFunction: _shiftApprovalsService.getAll,
                inputFilter: function () {
                    return {
                        frequencyIdFilter: $('#FrequencyFilterId').val(),
                        employeeIdFilter: $('#EmployeeFilterId').val(),
                        isBackup: $('#ApproverFilterId').val()
                    };
                }
            },
            select: {
                style: 'multi',
                selector: 'td:first-child + td + td'
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewShiftApprovalsModal.open({ id: data.record.shiftApprovals.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    className: 'select-checkbox',
                    orderable: false,
                    data: null,
                    defaultContent: ''
                },
                {
                    targets: 3,
                    data: "shiftApprovals.employeeId",
                    name: "employees.employeeCode"
                },
                {
                    targets: 4,
                    data: "shiftApprovals.employee",
                    name: "employees.lastName + employees.firstName + employees.middleName"
                },
                {
                    targets: 5,
                    data: "shiftApprovals.shiftType",
                    name: "shiftTypes.displayName"
                },
                {
                    targets: 6,
                    data: "shiftApprovals.shiftStart",
                    name: "shiftStart",
                    render: function (shiftStart) {
                        if (shiftStart) {
                            return moment(shiftStart).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 7,
                    data: "shiftApprovals.shiftEnd",
                    name: "shiftEnd",
                    render: function (shiftEnd) {
                        if (shiftEnd) {
                            return moment(shiftEnd).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 8,
                    data: "shiftApprovals.reason",
                    name: "shiftReasons.displayName"
                },
                {
                    targets: 9,
                    data: "shiftApprovals.remarks",
                    name: "remarks"
                },
                {
                    targets: 10,
                    data: "shiftApprovals.status",
                    name: "status.displayName"
                }
            ]
        });

        dataTable.on('click', '#checkBox', function () {
            var checkBox = document.getElementById("checkBox");

            if (checkBox.checked == false) {
                dataTable.rows().deselect();
                checkBox.checked = false;
            } else {
                dataTable.rows().select();
                checkBox.checked = true;
            }
        }).on('select deselect', function () {
            ('Some selection or deselection going on')
            if (dataTable.rows({
                selected: true
            }).count() !== dataTable.rows().count()) {
                checkBox.checked = false;
            } else {
                checkBox.checked = true;
            }
        });

        function getShiftApprovals() {
            if ($('#EmployeeFilterName').val() == '') {
                $('#EmployeeFilterId').val('');
            }

            dataTable.ajax.reload();
        }

        abp.event.on('app.createOrEditShiftApprovalsModalSaved', function () {
            getShiftApprovals();
        });

        $('#ApprovalRequestsButton').click(function () {
            window.location = "/App/ShiftApprovals/ApprovalRequests";
        });

        $('#GetShiftApprovalsButton').click(function (e) {
            e.preventDefault();
            getShiftApprovals();
        });

        $('#ApproveAllButton').click(function (e) {
            var ids = dataTable.rows({ selected: true }).ids().toArray();;

            if (ids.length > 0) {
                abp.ui.setBusy();
                _shiftApprovalsService.approveAll(
                    { ids }
                ).done(function () {
                    abp.notify.info(app.localize('Updated Successfully'));
                    dataTable.rows().deselect();
                    $('#checkBox').removeClass("selected");
                    getShiftApprovals();
                }).always(function () {
                    abp.ui.clearBusy();
                });
            }
            else {
                abp.notify.info(app.localize('No request selected.'));
            }
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getShiftApprovals();
            }
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $('#EmployeeFilterName').autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeFilterId').val(ui.item.id);
                        $('#EmployeeFilterName').val(ui.item.label);
                        dataTable.ajax.reload();
                        return false;
                    }
                });
            });

        $('#ApproverFilterId, #FrequencyFilterId').change(function () {
            getShiftApprovals();
        });
    });
})();
