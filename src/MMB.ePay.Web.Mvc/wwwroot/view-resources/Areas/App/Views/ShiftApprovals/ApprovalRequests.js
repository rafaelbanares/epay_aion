﻿(function () {
    $(function () {
        var _$shiftApprovalsTable = $('#ShiftApprovalsTable');
        var _shiftApprovalsService = abp.services.app.shiftApprovals;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;
        var _employeesService = abp.services.app.employees;

        var _viewShiftApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ShiftApprovals/ViewshiftApprovalRequestsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ShiftApprovals/_DetailsModal.js',
            modalClass: 'ViewShiftApprovalsModal'
        });

        var dataTable = _$shiftApprovalsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            rowId: 'shiftApprovals.id',
            listAction: {
                ajaxFunction: _shiftApprovalsService.getApprovalRequests,
                inputFilter: function () {
                    return {
                        attendancePeriodIdFilter: $('#AttendancePeriodFilterId').val(),
                        employeeIdFilter: $('#EmployeeFilterId').val(),
                        isBackup: $('#ApproverFilterId').val()
                    };
                }
            },
            select: {
                style: 'multi',
                selector: 'td:first-child + td + td'
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewShiftApprovalsModal.open({ id: data.record.shiftApprovals.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "shiftApprovals.employeeId",
                    name: "employees.employeeCode"
                },
                {
                    targets: 3,
                    data: "shiftApprovals.employee",
                    name: "employees.lastName + employees.firstName + employees.middleName"
                },
                {
                    targets: 4,
                    data: "shiftApprovals.shiftType",
                    name: "shiftTypes.displayName"
                },
                {
                    targets: 5,
                    data: "shiftApprovals.shiftStart",
                    name: "shiftStart",
                    render: function (shiftStart) {
                        if (shiftStart) {
                            return moment(shiftStart).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 6,
                    data: "shiftApprovals.shiftEnd",
                    name: "shiftEnd",
                    render: function (shiftEnd) {
                        if (shiftEnd) {
                            return moment(shiftEnd).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 7,
                    data: "shiftApprovals.reason",
                    name: "shiftReasons.displayName"
                },
                {
                    targets: 8,
                    data: "shiftApprovals.remarks",
                    name: "remarks"
                },
                {
                    targets: 9,
                    data: "shiftApprovals.status",
                    name: "status.displayName"
                }
            ]
        });

        function getShiftApprovals() {
            if ($('#EmployeeFilterName').val() == '') {
                $('#EmployeeFilterId').val('');
            }

            dataTable.ajax.reload();
        }

        abp.event.on('app.createOrEditShiftApprovalsModalSaved', function () {
            getShiftApprovals();
        });

        $('#BackButton').click(function () {
            window.location = "/App/ShiftApprovals";
        });

        $('#GetShiftApprovalsButton').click(function (e) {
            e.preventDefault();
            getShiftApprovals();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getShiftApprovals();
            }
        });

        $('#AttendancePeriodFilterId').change(function () {
            getShiftApprovals();
        });

        $('#YearFilterId, #FrequencyFilterId').change(function () {
            var year = $('#YearFilterId').val();
            var frequency = $('#FrequencyFilterId').val();

            _attendancePeriodsService.getAttendancePeriodListByYear(year, frequency, null)
                .done(function (data) {
                    $('#AttendancePeriodFilterId').empty();

                    $(data).each(function (index) {
                        var isSelected = '';

                        if (data[index].selected == true) {
                            isSelected = 'selected';
                        }

                        $('#AttendancePeriodFilterId').append(
                            '<option value="' + data[index].value + '" ' + isSelected + '>' + data[index].text + '</option>'
                        );
                    });

                    getShiftApprovals();
                });
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $('#EmployeeFilterName').autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeFilterId').val(ui.item.id);
                        $('#EmployeeFilterName').val(ui.item.label);
                        dataTable.ajax.reload();
                        return false;
                    }
                });
            });

        $('#ApproverFilterId').change(function () {
            getShiftApprovals();
        });
    });
})();
