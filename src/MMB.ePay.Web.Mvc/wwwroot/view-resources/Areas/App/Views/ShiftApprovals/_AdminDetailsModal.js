(function ($) {
    app.modals.ViewShiftApprovalsModal = function () {

        var _shiftApprovalsService = abp.services.app.shiftApprovals;
        var _modalManager;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
        };

        var _remarksShiftApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/ShiftApprovals/RemarksShiftApprovalsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/ShiftApprovals/_AdminRemarksModal.js',
            modalClass: 'RemarksShiftApprovalsModal'
        });

        $('.status_button').click(function () {

            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var reason = $('#ShiftRequests_Reason').val();

            _modalManager.setBusy(true);
            _shiftApprovalsService.adminUpdateStatus(
                { id, statusId, reason }
            ).done(function () {
                abp.notify.info(app.localize('Updated Successfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditShiftApprovalsModalSaved');
                }).always(function() {
                _modalManager.setBusy(false);
            });
        });

        $('.remarks_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var status = $(this).data('status');

            _remarksShiftApprovalsModal.open({ id: id, statusId: statusId, status: status });
            _modalManager.close();
        });
    };
})(jQuery);