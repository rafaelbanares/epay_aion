﻿(function () {
    $(function () {
        var _$leaveApprovalsTable = $('#LeaveApprovalsTable');
        var _leaveApprovalsService = abp.services.app.leaveApprovals;
        var _employeesService = abp.services.app.employees;

        var _viewLeaveApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/LeaveApprovals/ViewleaveApprovalsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/LeaveApprovals/_DetailsModal.js',
            modalClass: 'ViewLeaveApprovalsModal'
        });

        var dataTable = _$leaveApprovalsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            rowId: 'leaveApprovals.id',
            listAction: {
                ajaxFunction: _leaveApprovalsService.getAll,
                inputFilter: function () {
                    return {
                        frequencyIdFilter: $('#FrequencyFilterId').val(),
                        employeeIdFilter: $('#EmployeeFilterId').val(),
                        isBackup: $('#ApproverFilterId').val()
                    };
                }
            },
            select: {
                style: 'multi',
                selector: 'td:first-child + td + td'
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewLeaveApprovalsModal.open({ id: data.record.leaveApprovals.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    className: 'select-checkbox',
                    orderable: false,
                    data: null,
                    defaultContent: ''
                },
                {
                    targets: 3,
                    data: "leaveApprovals.employeeId",
                    name: "employees.employeeCode"
                },
                {
                    targets: 4,
                    data: "leaveApprovals.employee",
                    name: "employees.lastName + employees.firstName + employees.middleName"
                },
                {
                    targets: 5,
                    data: "leaveApprovals.leaveType",
                    name: "leaveTypes.displayName"
                },
                {
                    targets: 6,
                    data: "leaveApprovals.startDate",
                    name: "leaveRequestDetails.first()",
                    render: function (startDate) {
                        if (startDate) {
                            return moment(startDate).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 7,
                    data: "leaveApprovals.endDate",
                    name: "leaveRequestDetails.first()",
                    render: function (endDate) {
                        if (endDate) {
                            return moment(endDate).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 8,
                    data: "leaveApprovals.days",
                    name: "leaveRequestDetails.sum(f => f.Days)"
                },
                {
                    targets: 9,
                    data: "leaveApprovals.reason",
                    name: "leaveReasons.displayName"
                },
                {
                    targets: 10,
                    data: "leaveApprovals.remarks",
                    name: "remarks"
                },
                {
                    targets: 11,
                    data: "leaveApprovals.status",
                    name: "status.displayName"
                }
            ]
        });

        dataTable.on('click', '#checkBox', function () {
            var checkBox = document.getElementById("checkBox");

            if (checkBox.checked == false) {
                dataTable.rows().deselect();
                checkBox.checked = false;
            } else {
                dataTable.rows().select();
                checkBox.checked = true;
            }
        }).on('select deselect', function () {
            ('Some selection or deselection going on')
            if (dataTable.rows({
                selected: true
            }).count() !== dataTable.rows().count()) {
                checkBox.checked = false;
            } else {
                checkBox.checked = true;
            }
        });

        function getLeaveApprovals() {
            if ($('#EmployeeFilterName').val() == '') {
                $('#EmployeeFilterId').val('');
            }

            dataTable.ajax.reload();
        }

        abp.event.on('app.createOrEditLeaveApprovalsModalSaved', function () {
            getLeaveApprovals();
        });

        $('#ApprovalRequestsButton').click(function () {
            window.location = "/App/LeaveApprovals/ApprovalRequests";
        });

        $('#GetLeaveApprovalsButton').click(function (e) {
            e.preventDefault();
            getLeaveApprovals();
        });

        $('#ApproveAllButton').click(function (e) {
            var ids = dataTable.rows({ selected: true }).ids().toArray();;

            if (ids.length > 0) {
                abp.ui.setBusy();
                _leaveApprovalsService.approveAll(
                    { ids }
                ).done(function () {
                    abp.notify.info(app.localize('Updated Successfully'));
                    dataTable.rows().deselect();
                    $('#checkBox').removeClass("selected");
                    getLeaveApprovals();
                }).always(function () {
                    abp.ui.clearBusy();
                });
            }
            else {
                abp.notify.info(app.localize('No request selected.'));
            }
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getLeaveApprovals();
            }
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $('#EmployeeFilterName').autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeFilterId').val(ui.item.id);
                        $('#EmployeeFilterName').val(ui.item.label);
                        dataTable.ajax.reload();
                        return false;
                    }
                });
            });

        $('#ApproverFilterId, #FrequencyFilterId').change(function () {
            getLeaveApprovals();
        });
    });
})();
