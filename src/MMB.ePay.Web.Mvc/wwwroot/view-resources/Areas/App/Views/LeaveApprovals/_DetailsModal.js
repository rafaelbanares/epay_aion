(function ($) {
    app.modals.ViewLeaveApprovalsModal = function () {

        var _leaveApprovalsService = abp.services.app.leaveApprovals;
        var _modalManager;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
        };

        var _remarksLeaveApprovalsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/LeaveApprovals/RemarksLeaveApprovalsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/LeaveApprovals/_RemarksModal.js',
            modalClass: 'RemarksLeaveApprovalsModal'
        });

        $('.status_button').click(function () {

            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var reason = $('#LeaveRequests_Reason').val();

            _modalManager.setBusy(true);
            _leaveApprovalsService.updateStatus(
                { id, statusId, reason }
            ).done(function () {
                abp.notify.info(app.localize('Updated Successfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditLeaveApprovalsModalSaved');
                }).always(function() {
                _modalManager.setBusy(false);
            });
        });

        $('.remarks_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var status = $(this).data('status');

            _remarksLeaveApprovalsModal.open({ id: id, statusId: statusId, status: status });
            _modalManager.close();
        });
    };
})(jQuery);