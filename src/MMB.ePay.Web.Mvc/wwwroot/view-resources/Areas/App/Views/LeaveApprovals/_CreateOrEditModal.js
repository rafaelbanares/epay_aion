(function($) {
    app.modals.CreateOrEditLeaveApprovalsModal = function() {
        var _leaveApprovalsService = abp.services.app.leaveApprovals;
        var _modalManager;
        var _$leaveApprovalsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$leaveApprovalsInformationForm = _modalManager.getModal().find('form[name=LeaveApprovalsInformationsForm]');
            _$leaveApprovalsInformationForm.validate();
        };
        this.save = function() {
            if (!_$leaveApprovalsInformationForm.valid()) {
                return;
            }
            var leaveApprovals = _$leaveApprovalsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _leaveApprovalsService.createOrEdit(
                leaveApprovals
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditLeaveApprovalsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);