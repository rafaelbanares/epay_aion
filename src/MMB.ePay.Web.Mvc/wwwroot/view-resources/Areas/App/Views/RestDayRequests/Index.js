﻿(function () {
    $(function () {
        var _$restDayRequestsTable = $('#RestDayRequestsTable');
        var _restDayRequestsService = abp.services.app.restDayRequests;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.RestDayRequests.Create'),
            edit: abp.auth.hasPermission('Pages.RestDayRequests.Edit')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RestDayRequests/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/RestDayRequests/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditRestDayRequestsModal'
        });

        var _viewRestDayRequestsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RestDayRequests/ViewrestDayRequestsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/RestDayRequests/_DetailsModal.js',
            modalClass: 'ViewRestDayRequestsModal'
        });

        var dataTable = _$restDayRequestsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _restDayRequestsService.getAll,
                inputFilter: function () {
                    return {
                        attendancePeriodIdFilter: $('#AttendancePeriodFilterId').val(),
                        statusIdFilter: $('#StatusFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewRestDayRequestsModal.open({ id: data.record.restDayRequests.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function (data) {
                                    if (data.record.restDayRequests.isEditable) {
                                        return _permissions.edit;
                                    } else {
                                        return false;
                                    }
                                },
                                action: function (data) {
                                    _createOrEditModal.open({ id: data.record.restDayRequests.id });
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "restDayRequests.restDayStart",
                    name: "restDayStart",
                    render: function (restDayStart) {
                        if (restDayStart) {
                            //return moment(restDayStart).format('MM/DD/YYYY hh:mm A');
                            return moment(restDayStart).format('MM/DD/YYYY');
                        }
                        return "";
                    }
                },
                {
                    targets: 3,
                    data: "restDayRequests.restDayEnd",
                    name: "restDayEnd",
                    render: function (restDayEnd) {
                        if (restDayEnd) {
                            //return moment(restDayEnd).format('MM/DD/YYYY hh:mm A');
                            return moment(restDayEnd).format('MM/DD/YYYY');
                        }
                        return "";
                    }
                },
                {
                    targets: 4,
                    data: "restDayRequests.restDays",
                    name: "restDayCode"
                },
                {
                    targets: 5,
                    data: "restDayRequests.reason",
                    name: "restDayReasons.displayName"
                },
                {
                    targets: 6,
                    data: "restDayRequests.remarks",
                    name: "remarks"
                },
                {
                    targets: 7,
                    data: "restDayRequests.status",
                    name: "status.displayName"
                }
            ]
        });

        function getRestDayRequests() {
            dataTable.ajax.reload();
        }

        $('#CreateNewRestDayRequestsButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditRestDayRequestsModalSaved', function () {
            getRestDayRequests();
        });

        $('#GetRestDayRequestsButton').click(function (e) {
            e.preventDefault();
            getRestDayRequests();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getRestDayRequests();
            }
        });

        $('#StatusFilterId, #AttendancePeriodFilterId').change(function () {
            getRestDayRequests();
        });

        $('#YearFilterId').change(function () {
            var year = $(this).val();

            _attendancePeriodsService.getAttendancePeriodListByYear(year, null, null)
                .done(function (data) {
                    $('#AttendancePeriodFilterId').empty();

                    $(data).each(function (index) {
                        var isSelected = '';

                        if (data[index].selected == true) {
                            isSelected = 'selected';
                        }

                        $('#AttendancePeriodFilterId').append(
                            '<option value="' + data[index].value + '" ' + isSelected + '>' + data[index].text + '</option>'
                        );
                    });

                    getRestDayRequests();
                });
        });

        $('#MinRestDayDateFilterId, #MaxRestDayDateFilterId').focusout(function () {
            getRestDayRequests();
        });
    });
})();
