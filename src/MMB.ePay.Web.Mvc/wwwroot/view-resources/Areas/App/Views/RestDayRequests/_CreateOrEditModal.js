(function($) {
    app.modals.CreateOrEditRestDayRequestsModal = function () {
        var _restDayRequestsService = abp.services.app.restDayRequests;
        var _modalManager;
        var _$restDayRequestsInformationForm = null;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$restDayRequestsInformationForm = _modalManager.getModal().find('form[name=RestDayRequestsInformationsForm]');
            _$restDayRequestsInformationForm.validate();
        };
        this.save = function() {
            if (!_$restDayRequestsInformationForm.valid()) {
                return;
            }
            var restDayRequests = _$restDayRequestsInformationForm.serializeFormToObject();

            _modalManager.setBusy(true);
            _restDayRequestsService.createOrEdit(
                restDayRequests
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditRestDayRequestsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);