(function ($) {
    app.modals.ViewRestDayRequestsModal = function () {

        var _restDayRequestsService = abp.services.app.restDayRequests;
        var _modalManager;
        this.init = function(modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
        };

        var _remarksRestDayRequestsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RestDayRequests/RemarksRestDayRequestsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/RestDayRequests/_RemarksModal.js',
            modalClass: 'RemarksRestDayRequestsModal'
        });

        $('.status_button').click(function () {

            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var reason = $('#RestDayRequests_Reason').val();

            _modalManager.setBusy(true);
            _restDayRequestsService.updateStatus(
                { id, statusId, reason }
            ).done(function () {
                abp.notify.info(app.localize('Updated Successfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditRestDayRequestsModalSaved');
                }).always(function() {
                _modalManager.setBusy(false);
            });
        });

        $('.remarks_button').click(function () {
            var id = $('input[name=id]').val();
            var statusId = $(this).data('id');
            var status = $(this).data('status');

            _remarksRestDayRequestsModal.open({ id: id, statusId: statusId, status: status });
            _modalManager.close();
        });
    };
})(jQuery);