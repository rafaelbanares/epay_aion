(function ($) {
    app.modals.ViewLeaveEarningsModal = function () {
        var _modalManager;
        var _$leaveEarningDetailsTable = $('#LeaveEarningDetailsTable');
        var _leaveEarningsService = abp.services.app.leaveEarnings;

        this.init = function (modalManager) {
            _modalManager = modalManager;

            var dataTable = _$leaveEarningDetailsTable.DataTable({
                paging: true,
                serverSide: true,
                processing: true,
                listAction: {
                    ajaxFunction: _leaveEarningsService.getLeaveEarningDetails,
                    inputFilter: function () {
                        return {
                            id: $('#LeaveEarningId').val()
                        };
                    }
                },
                columnDefs: [
                    //{
                    //    className: 'control responsive',
                    //    orderable: false,
                    //    render: function () {
                    //        return '';
                    //    },
                    //    targets: 0
                    //},
                    {
                        targets: 0,
                        data: 'leaveEarningDetails.employee',
                        name: 'employees.lastName',
                    },
                    {
                        targets: 1,
                        data: 'leaveEarningDetails.earned',
                        name: 'earned'
                    }
                ]
            });
        };
    };
})(jQuery);