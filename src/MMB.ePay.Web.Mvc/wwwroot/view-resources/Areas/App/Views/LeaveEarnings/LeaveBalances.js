﻿(function () {
    $(function () {

        var _$leaveBalancesTable = $('#LeaveBalancesTable');
        var _leaveEarningsService = abp.services.app.leaveEarnings;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _viewLeaveBalancesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/LeaveEarnings/ViewleaveBalancesModal',
            modalClass: 'ViewleaveBalancesModal'
        });

        var dataTable = _$leaveBalancesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            //lengthMenu: [[1, 25, 50, 100, 200, -1], [1, 25, 50, 100, 200, "All"]],
            listAction: {
                ajaxFunction: _leaveEarningsService.getLeaveBalances,
                inputFilter: function () {
                    return {
                        appYearFilter: $('#AppYearFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewLeaveBalancesModal.open({
                                        id: data.record.leaveBalances.id,
                                        year: $('#AppYearFilterId').val()
                                    });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return false;
                                }
                            },
                            {
                                text: app.localize('Delete'),
                                iconStyle: 'far fa-trash-alt mr-2',
                                visible: function () {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "leaveBalances.leaveType",
                    name: "displayName"
                },
                {
                    targets: 3,
                    orderable: false,
                    data: "leaveBalances.earned",
                    name: "earned"
                },
                {
                    targets: 4,
                    orderable: false,
                    data: "leaveBalances.used",
                    name: "used"
                },
                {
                    targets: 5,
                    orderable: false,
                    data: "leaveBalances.balance",
                    name: "leaveEarning"
                }
            ]
        });

        function getLeaveBalances() {
            dataTable.ajax.reload();
        }

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getLeaveBalances();
            }
        });

        $('#BackButton').click(function () {
            window.location = "/App/LeaveRequests";
        });

        $('#AppYearFilterId').change(function () {
            getLeaveBalances();
        });

    });
})();
