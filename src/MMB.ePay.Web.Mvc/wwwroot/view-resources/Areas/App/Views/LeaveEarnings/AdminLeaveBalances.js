﻿(function () {
    $(function () {
        var _$leaveBalancesTable = $('#LeaveBalancesTable');
        var _leaveEarningsService = abp.services.app.leaveEarnings;
        var _employeesService = abp.services.app.employees;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _viewLeaveBalancesModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/LeaveEarnings/ViewleaveBalancesModal',
            modalClass: 'ViewleaveBalancesModal'
        });

        var dataTable = _$leaveBalancesTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            //lengthMenu: [[1, 25, 50, 100, 200, -1], [1, 25, 50, 100, 200, "All"]],
            listAction: {
                ajaxFunction: _leaveEarningsService.getAdminLeaveBalances,
                inputFilter: function () {
                    return {
                        appYearFilter: $('#AppYearFilterId').val(),
                        employeeIdFilter: $('#EmployeeFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    var employeeId = $('#EmployeeFilterId').val() == '' ? '0' : $('#EmployeeFilterId').val();

                                    _viewLeaveBalancesModal.open({
                                        id: data.record.adminLeaveBalances.id,
                                        employeeId: employeeId,
                                        year: $('#AppYearFilterId').val()
                                    });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return false;
                                }
                            },
                            {
                                text: app.localize('Delete'),
                                iconStyle: 'far fa-trash-alt mr-2',
                                visible: function () {
                                    return false;
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "adminLeaveBalances.employee",
                    name: "leaveRequests.First().employees.lastName"
                },
                {
                    targets: 3,
                    data: "adminLeaveBalances.leaveType",
                    name: "displayName"
                },
                {
                    targets: 4,
                    orderable: false,
                    data: "adminLeaveBalances.earned",
                    name: "earned"
                },
                {
                    targets: 5,
                    orderable: false,
                    data: "adminLeaveBalances.used",
                    name: "used"
                },
                {
                    targets: 6,
                    orderable: false,
                    data: "adminLeaveBalances.balance",
                    name: "leaveEarning"
                }
            ]
        });

        function getLeaveBalances() {
            if ($('#EmployeeFilterName').val() == '') {
                $('#EmployeeFilterId').val('');
            }

            dataTable.ajax.reload();
        }

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getLeaveBalances();
            }
        });

        _employeesService.getEmployeeList()
            .done(function (data) {
                var employees = [];

                $(data).each(function (index) {
                    employees.push({ "label": data[index].text, "id": data[index].value });
                });

                $('#EmployeeFilterName').autocomplete({
                    source: employees,
                    select: function (event, ui) {
                        $('#EmployeeFilterId').val(ui.item.id);
                        $('#EmployeeFilterName').val(ui.item.label);
                        dataTable.ajax.reload();
                        return false;
                    }
                });
            });

        $('#AppYearFilterId').change(function () {
            getLeaveBalances();
        });

    });
})();
