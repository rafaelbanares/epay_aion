(function ($) {
    app.modals.UploadLeaveEarningsModal = function () {

        var _modalManager;
        var uploadedFileToken = null;

        var _leaveEarningsService = abp.services.app.leaveEarnings;

        this.init = function (modalManager) {
            _modalManager = modalManager;

            $('#UploadLeaveEarningsModalForm input[name=LeaveEarningsFile]').change(function () {
                $('#UploadLeaveEarningsModalForm').submit();
            });

            $('#UploadLeaveEarningsModalForm').ajaxForm({
                beforeSubmit: function (formData, jqForm, options) {

                    var $fileInput = $('#UploadLeaveEarningsModalForm input[name=LeaveEarningsFile]');
                    var files = $fileInput.get()[0].files;

                    if (files.length == 0) {
                        return false;
                    }

                    var file = files[0];

                    ////File type check
                    //var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                    //if ('|vnd.openxmlformats-officedocument.spreadsheetml.sheet|'.indexOf(type) === -1) {
                    //    abp.message.warn(app.localize('LeaveEarnings_Warn_FileType'));
                    //    return false;
                    //}

                    ////File size check
                    //if (file.size > 20971520) //20MB
                    //{
                    //    abp.message.warn(app.localize('LeaveEarningsFile_Warn_SizeLimit', app.maxLeaveEarningFileBytesUserFriendlyValue));
                    //    return false;
                    //}

                    var mimeType = _.filter(formData, { name: 'LeaveEarningsFile' })[0].value.type;

                    formData.push({ name: 'FileType', value: mimeType });
                    formData.push({ name: 'FileName', value: 'LeaveEarningsFile' });
                    formData.push({ name: 'FileToken', value: app.guid() });

                    return true;
                },
                success: function (response) {
                    if (response.success) {
                        uploadedFileToken = response.result.fileToken;
                    } else {
                        abp.message.error(response.error.message);
                    }
                }
            });
        };

        this.save = function () {
            if (!uploadedFileToken) {
                abp.notify.warn(app.localize("PleaseSelectAFile"));
                return;
            }

            var input = {
                fileToken: uploadedFileToken
            };

            _leaveEarningsService.updateByFile(input)
                .done(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    _modalManager.close();
                    abp.event.trigger('app.createOrEditLeaveEarningsModalSaved');
            });
        };
    };
})(jQuery);