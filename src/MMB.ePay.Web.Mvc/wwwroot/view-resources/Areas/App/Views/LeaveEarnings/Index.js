﻿(function () {
    $(function () {

        var _$leaveEarningsTable = $('#LeaveEarningsTable');
        var _leaveEarningsService = abp.services.app.leaveEarnings;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        //var _permissions = {
        //    create: abp.auth.hasPermission('Pages.LeaveEarnings.Create'),
        //    edit: abp.auth.hasPermission('Pages.LeaveEarnings.Edit'),
        //    'delete': abp.auth.hasPermission('Pages.LeaveEarnings.Delete')
        //};

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/LeaveEarnings/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/LeaveEarnings/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditLeaveEarningsModal'
        });

        var _createOrEditMultiModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/LeaveEarnings/CreateOrEditMultiModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/LeaveEarnings/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditLeaveEarningsModal'
        });

        var _uploadLeaveEarningsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/LeaveEarnings/UploadModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/LeaveEarnings/_UploadModal.js',
            modalClass: 'UploadLeaveEarningsModal'
        });

        var _viewLeaveEarningsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/LeaveEarnings/ViewleaveEarningsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/LeaveEarnings/_ViewModal.js',
            modalClass: 'ViewLeaveEarningsModal'
        });

        var dataTable = _$leaveEarningsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _leaveEarningsService.getAll,
                inputFilter: function () {
                    return {
                        appYearFilter: $('#AppYearFilterId').val(),
                        appMonthFilter: $('#AppMonthFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewLeaveEarningsModal.open({ id: data.record.leaveEarnings.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function () {
                                    return false;
                                    //return _permissions.edit;
                                },
                                action: function (data) {

                                    if (data.record.leaveEarnings.employeeCount > 1) {
                                        _createOrEditMultiModal.open({ id: data.record.leaveEarnings.id });

                                    }
                                    else {
                                        _createOrEditModal.open({ id: data.record.leaveEarnings.id });
                                    }
                                }
                            },
                            {
                                text: app.localize('Delete'),
                                iconStyle: 'far fa-trash-alt mr-2',
                                visible: function () {
                                    return false;
                                    //return _permissions.delete;
                                },
                                action: function (data) {
                                    deleteLeaveEarnings(data.record.leaveEarnings);
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "leaveEarnings.date",
                    name: "date",
                    render: function (date) {
                        if (date) {
                            return moment(date).format('L');
                        }
                        return "";
                    }

                },
                //{
                //    targets: 3,
                //    data: "leaveEarnings.validFrom",
                //    name: "validFrom",
                //    render: function (validFrom) {
                //        if (validFrom) {
                //            return moment(validFrom).format('L');
                //        }
                //        return "";
                //    }

                //},
                //{
                //    targets: 4,
                //    data: "leaveEarnings.validTo",
                //    name: "validTo",
                //    render: function (validTo) {
                //        if (validTo) {
                //            return moment(validTo).format('L');
                //        }
                //        return "";
                //    }

                //},
                {
                    targets: 3,
                    data: "leaveEarnings.appYear",
                    name: "appYear"
                },
                {
                    targets: 4,
                    data: "leaveEarnings.appMonth",
                    name: "appMonth"
                },

                {
                    targets: 5,
                    data: "leaveEarnings.leaveType",
                    name: "leaveEarningDetails.first().leaveTypes.displayName"
                },
                {
                    targets: 6,
                    data: "leaveEarnings.employeeCount",
                    name: "LeaveEarningDetails.count"
                },
                {
                    targets: 7,
                    data: "leaveEarnings.totalEarned",
                    name: "LeaveEarningDetails.sum(earned)"
                },
                {
                    targets: 8,
                    data: "leaveEarnings.remarks",
                    name: "remarks"
                }
            ]
        });

        function getLeaveEarnings() {
            dataTable.ajax.reload();
        }

        function deleteLeaveEarnings(leaveEarnings) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _leaveEarningsService.delete({
                            id: leaveEarnings.id
                        }).done(function () {
                            getLeaveEarnings(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        $('#UploadLeaveEarningsButton').click(function () {
            _uploadLeaveEarningsModal.open();
        });

        $('#CreateNewLeaveEarningsButton').click(function () {
            _createOrEditModal.open();
        });

        $('#CreateNewMultiLeaveEarningsButton').click(function () {
            _createOrEditMultiModal.open();
        });

        abp.event.on('app.createOrEditLeaveEarningsModalSaved', function () {
            getLeaveEarnings();
        });

        $('#GetLeaveEarningsButton').click(function (e) {
            e.preventDefault();
            getLeaveEarnings();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getLeaveEarnings();
            }
        });

        $('#AppYearFilterId, #AppMonthFilterId').change(function () {
            getLeaveEarnings();
        });

    });
})();
