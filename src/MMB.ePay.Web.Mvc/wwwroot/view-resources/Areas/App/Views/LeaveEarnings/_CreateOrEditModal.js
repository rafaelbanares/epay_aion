(function($) {
    app.modals.CreateOrEditLeaveEarningsModal = function () {
        var _leaveEarningsService = abp.services.app.leaveEarnings;
        var _employeesService = abp.services.app.employees;

        var _modalManager;
        var _$leaveEarningsInformationForm = null;
        this.init = function (modalManager) {
            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            });
            _$leaveEarningsInformationForm = _modalManager.getModal().find('form[name=LeaveEarningsInformationsForm]');
            _$leaveEarningsInformationForm.validate();

            _employeesService.getEmployeeList()
                .done(function (data) {

                    var employees = [];

                    $(data).each(function (index) {
                        employees.push({ "label": data[index].text, "id": data[index].value });
                    });

                    $("#EmployeeFilterId").autocomplete({
                        source: employees,
                        select: function (event, ui) {
                            $('#LeaveEarnings_EmployeeId').val(ui.item.id);
                            $('#EmployeeFilterId').val(ui.item.label);
                            return false;
                        },
                        minLength: 3,
                        messages: {
                            noResults: 'No results found.'
                        }
                    });
                });

        };
        this.save = function() {
            if (!_$leaveEarningsInformationForm.valid()) {
                return;
            }

            _modalManager.setBusy(true);
            var leaveEarnings = _$leaveEarningsInformationForm.serializeFormToObject();

            function CreateLeaveEarnings() {
                _leaveEarningsService.createOrEdit(
                    leaveEarnings
                ).done(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    _modalManager.close();
                    abp.event.trigger('app.createOrEditLeaveEarningsModalSaved');
                }).always(function () {
                    _modalManager.setBusy(false);
                });
            }

            _employeesService.checkDateRegularized()
                .done(function (isRegularized) {

                    if (isRegularized) {

                        abp.message.confirm(
                            'There are employees who do not have date of regularization. ' +
                            'If you continue, all of these employees will still earn leave credits. ' +
                            'Do you want to continue?',
                            app.localize('AreYouSure'),
                            function (isConfirmed) {

                                if (isConfirmed) {
                                    CreateLeaveEarnings()
                                }
                                else {
                                    _modalManager.setBusy(false);
                                }

                            }
                        );

                    } else {
                        CreateLeaveEarnings()
                    }

                });
            
        };
    };
})(jQuery);