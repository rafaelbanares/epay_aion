﻿(function () {
    $(function () {

        var _$recurringJobsTable = $('#RecurringJobsTable');
        var _hangfireService = abp.services.app.hangfire;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            'delete': abp.auth.hasPermission('Pages.Administration.HangfireDashboard')
        };

        var _viewRecurringJobsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/RecurringJobs/ViewrecurringJobsModal',
            modalClass: 'ViewRecurringJobsModal'
        });

        var dataTable = _$recurringJobsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _hangfireService.getAll
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewRecurringJobsModal.open({ id: data.record.hangfire.id });
                                }
                            },
                            {
                                text: app.localize('Delete'),
                                iconStyle: 'far fa-trash-alt mr-2',
                                visible: function (data) {
                                    return _permissions.delete;
                                },
                                action: function (data) {
                                    deleteRecurringJobs(data.record.hangfire.id);
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "hangfire.displayName",
                    name: "displayName"
                },
                {
                    targets: 3,
                    data: "hangfire.queue",
                    name: "queue"
                },
                {
                    targets: 4,
                    data: "hangfire.cron",
                    name: "cron"
                },
                {
                    targets: 5,
                    data: "hangfire.nextExecution",
                    name: "nextExecution",
                    render: function (nextExecution) {
                        if (nextExecution) {
                            return moment(nextExecution).format('L');
                        }
                        return "";
                    }
                },
                {
                    targets: 6,
                    data: "hangfire.lastExecution",
                    name: "lastExecution",
                    render: function (lastExecution) {
                        if (lastExecution) {
                            return moment(lastExecution).format('L');
                        }
                        return "";
                    }
                }
            ]
        });

        function getRecurringJobs() {
            dataTable.ajax.reload();
        }

        function deleteRecurringJobs(id) {
            abp.message.confirm(
                '',
                app.localize('AreYouSure'),
                function (isConfirmed) {
                    if (isConfirmed) {
                        _hangfireService.delete(
                            id
                        ).done(function () {
                            getRecurringJobs(true);
                            abp.notify.success(app.localize('SuccessfullyDeleted'));
                        });
                    }
                }
            );
        }

        abp.event.on('app.createOrEditRecurringJobsModalSaved', function () {
            getRecurringJobs();
        });

        $('#GetRecurringJobsButton').click(function (e) {
            e.preventDefault();
            getRecurringJobs();
        });
    });
})();
