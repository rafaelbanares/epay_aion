﻿(function () {
    $(function () {
        var _$officialBusinessRequestsTable = $('#OfficialBusinessRequestsTable');
        var _officialBusinessRequestsService = abp.services.app.officialBusinessRequests;
        var _attendancePeriodsService = abp.services.app.attendancePeriods;

        $('.date-picker').datetimepicker({
            locale: abp.localization.currentLanguage.name,
            format: 'L'
        });

        var _permissions = {
            create: abp.auth.hasPermission('Pages.OfficialBusinessRequests.Create'),
            edit: abp.auth.hasPermission('Pages.OfficialBusinessRequests.Edit')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OfficialBusinessRequests/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OfficialBusinessRequests/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditOfficialBusinessRequestsModal'
        });

        var _viewOfficialBusinessRequestsModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/OfficialBusinessRequests/ViewofficialBusinessRequestsModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/OfficialBusinessRequests/_DetailsModal.js',
            modalClass: 'ViewOfficialBusinessRequestsModal'
        });

        var dataTable = _$officialBusinessRequestsTable.DataTable({
            paging: true,
            serverSide: true,
            processing: true,
            listAction: {
                ajaxFunction: _officialBusinessRequestsService.getAll,
                inputFilter: function () {
                    return {
                        attendancePeriodIdFilter: $('#AttendancePeriodFilterId').val(),
                        statusIdFilter: $('#StatusFilterId').val()
                    };
                }
            },
            columnDefs: [
                {
                    className: 'control responsive',
                    orderable: false,
                    render: function () {
                        return '';
                    },
                    targets: 0
                },
                {
                    width: 120,
                    targets: 1,
                    data: null,
                    orderable: false,
                    autoWidth: false,
                    defaultContent: '',
                    rowAction: {
                        cssClass: 'btn btn-brand dropdown-toggle',
                        text: '<i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span>',
                        items: [
                            {
                                text: app.localize('View'),
                                iconStyle: 'far fa-eye mr-2',
                                action: function (data) {
                                    _viewOfficialBusinessRequestsModal.open({ id: data.record.officialBusinessRequests.id });
                                }
                            },
                            {
                                text: app.localize('Edit'),
                                iconStyle: 'far fa-edit mr-2',
                                visible: function (data) {
                                    if (data.record.officialBusinessRequests.isEditable) {
                                        return _permissions.edit;
                                    } else {
                                        return false;
                                    }
                                },
                                action: function (data) {
                                    _createOrEditModal.open({ id: data.record.officialBusinessRequests.id });
                                }
                            }]
                    }
                },
                {
                    targets: 2,
                    data: "officialBusinessRequests.startDate",
                    name: "officialBusinessRequestDetails.first()",
                    render: function (startDate) {
                        if (startDate) {
                            var time = moment(startDate).format('hh:mm A')

                            if (time == '12:00 AM') {
                                return moment(startDate).format('MM/DD/YYYY');
                            }

                            return moment(startDate).format('MM/DD/YYYY hh:mm A');
                        }
                        return "";
                    }
                },
                {
                    targets: 3,
                    data: "officialBusinessRequests.endDate",
                    name: "officialBusinessRequestDetails.first()",
                    render: function (endDate) {
                        if (endDate) {
                            var time = moment(endDate).format('hh:mm A')

                            if (time == '12:00 AM') {
                                return moment(endDate).format('MM/DD/YYYY');
                            }

                            return moment(endDate).format('MM/DD/YYYY hh:mm A');
                        }
                        return "";
                    }
                },
                {
                    targets: 4,
                    data: "officialBusinessRequests.reason",
                    name: "officialBusinessReasons.displayName"
                },
                {
                    targets: 5,
                    data: "officialBusinessRequests.remarks",
                    name: "remarks"
                },
                {
                    targets: 6,
                    data: "officialBusinessRequests.status",
                    name: "status.displayName"
                }
            ]
        });

        function getOfficialBusinessRequests() {
            dataTable.ajax.reload();
        }

        $('#CreateNewOfficialBusinessRequestsButton').click(function () {
            _createOrEditModal.open();
        });

        abp.event.on('app.createOrEditOfficialBusinessRequestsModalSaved', function () {
            getOfficialBusinessRequests();
        });

        $('#GetOfficialBusinessRequestsButton').click(function (e) {
            e.preventDefault();
            getOfficialBusinessRequests();
        });

        $(document).keypress(function (e) {
            if (e.which === 13) {
                getOfficialBusinessRequests();
            }
        });

        $('#StatusFilterId, #AttendancePeriodFilterId').change(function () {
            getOfficialBusinessRequests();
        });

        $('#YearFilterId').change(function () {
            var year = $(this).val();

            _attendancePeriodsService.getAttendancePeriodListByYear(year, null, null)
                .done(function (data) {
                    $('#AttendancePeriodFilterId').empty();

                    $(data).each(function (index) {
                        var isSelected = '';

                        if (data[index].selected == true) {
                            isSelected = 'selected';
                        }

                        $('#AttendancePeriodFilterId').append(
                            '<option value="' + data[index].value + '" ' + isSelected + '>' + data[index].text + '</option>'
                        );
                    });

                    getOfficialBusinessRequests();
                });
        });

        $('#MinOfficialBusinessDateFilterId, #MaxOfficialBusinessDateFilterId').focusout(function () {
            getOfficialBusinessRequests();
        });
    });
})();
