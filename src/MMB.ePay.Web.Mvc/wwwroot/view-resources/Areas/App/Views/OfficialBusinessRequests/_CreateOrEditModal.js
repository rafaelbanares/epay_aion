(function($) {
    app.modals.CreateOrEditOfficialBusinessRequestsModal = function() {
        //$.getScript('/view-resources/Areas/App/Views/_Bundles/demo-ui-components.js');

        var _officialBusinessRequestsService = abp.services.app.officialBusinessRequests;
        var _modalManager;
        var _$officialBusinessRequestsInformationForm = null;
        this.init = function (modalManager) {

            function ToggleWholeDay() {
                let wholeDay = document.getElementById("OfficialBusinessRequests_WholeDay");

                if ($('#OfficialBusinessRequests_StartDate').val() == $('#OfficialBusinessRequests_EndDate').val()) {
                    $('#lblWholeDay').removeClass('checkbox-disabled');
                    $('#OfficialBusinessRequests_WholeDay').removeAttr('disabled');
                }
                else {
                    wholeDay.checked = true;
                    $('#lblWholeDay').addClass('checkbox-disabled');
                    $('#OfficialBusinessRequests_WholeDay').prop('disabled', 'disabled');
                }

                ToggleTime();
            }

            function ToggleTime() {
                let wholeDay = document.getElementById("OfficialBusinessRequests_WholeDay");
                let startTime = $('#OfficialBusinessRequests_StartTime');
                let endTime = $('#OfficialBusinessRequests_EndTime');

                if (wholeDay.checked) {
                    startTime.prop('disabled', 'disabled');
                    endTime.prop('disabled', 'disabled');

                    startTime.val('');
                    endTime.val('');
                }
                else {
                    startTime.removeAttr('disabled');
                    endTime.removeAttr('disabled');
                }
            }

            ToggleWholeDay();

            _modalManager = modalManager;
            var modal = _modalManager.getModal();
            modal.find('.date-picker').datetimepicker({
                locale: abp.localization.currentLanguage.name,
                format: 'L'
            }).on('dp.change', function () {
                ToggleWholeDay();
            });

            $('#OfficialBusinessRequests_WholeDay').change(function () {
                ToggleTime();
            });

            _$officialBusinessRequestsInformationForm = _modalManager.getModal().find('form[name=OfficialBusinessRequestsInformationsForm]');
            _$officialBusinessRequestsInformationForm.validate();
        };
        this.save = function() {
            if (!_$officialBusinessRequestsInformationForm.valid()) {
                return;
            }
            var officialBusinessRequests = _$officialBusinessRequestsInformationForm.serializeFormToObject();
            _modalManager.setBusy(true);
            _officialBusinessRequestsService.createOrEdit(
                officialBusinessRequests
            ).done(function() {
                abp.notify.info(app.localize('SavedSuccessfully'));
                _modalManager.close();
                abp.event.trigger('app.createOrEditOfficialBusinessRequestsModalSaved');
            }).always(function() {
                _modalManager.setBusy(false);
            });
        };
    };
})(jQuery);