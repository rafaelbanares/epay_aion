/// //<reference path="_entriesmodal.js" />
(function () {
    $(function () {
        var interval = setInterval(function () {
            var momentNow = moment();
            $('#date-part').html(momentNow.format('dddd') + ', ' + momentNow.format('MMMM D, YYYY'));
            $('#time-part').html(momentNow.format('hh:mm:ss A'));
        });

        var _accountService = abp.services.app.account;
        var _tkSettingsService = abp.services.app.tKSettings;

        var _$timeEntryInformationForm = $('form[name=TimeEntryInformationsForm]');

        function ClearInputs() {
            $('input[name=usernameOrEmailAddress]').val('');
            $('input[name=password]').val('');
            $('input[name=workFromHome]').prop("checked", false);
        }

        function CheckIfMobile() {
            return Math.min(window.screen.width, window.screen.height) < 768 && navigator.userAgent.indexOf("Mobi") > -1;
        }

        function SetTimeEntry(inOut, position) {
            var isMobile = CheckIfMobile();
            var latitude = 0;
            var longitude = 0;

            if (position != null) {
                latitude = position.coords.latitude;
                longitude = position.coords.longitude;
            }

            var timeEntry = {
                userNameOrEmailAddress: $('input[name=usernameOrEmailAddress]').val(),
                password: $('input[name=password]').val(),
                workFromHome: $('input[name=workFromHome]').is(":checked"),
                inOut: inOut,
                isMobile: isMobile,
                latitude: latitude,
                longitude: longitude
            };

            abp.ui.setBusy();
            _accountService.timeEntry(
                timeEntry
            ).done(function () {
                ClearInputs();

                if (inOut == 'I') {
                    abp.notify.info(app.localize('Time IN Successful'));
                } else if (inOut == 'O') {
                    abp.notify.info(app.localize('Time OUT Successful'));
                }

            }).always(function () {
                abp.ui.clearBusy();
            });
        }

        function TimeInOut(inOut) {
            _tkSettingsService.geolocation(
            ).done(function (keySetting) {

                switch (keySetting) {
                    case 'DISABLED':
                        SetTimeEntry(inOut, null);
                        break;

                    case 'REQUIRED':
                        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition((position) => {
                                SetTimeEntry(inOut, position);
                            }, (error) => {
                                abp.notify.error('Location access is required');
                            });
                        }
                        else {
                            abp.notify.error('Location access is required');
                        }

                        break;

                    case 'OPTIONAL':
                        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition((position) => {
                                SetTimeEntry(inOut, position);
                            }, (error) => {
                                SetTimeEntry(inOut, null);
                            });
                        }
                        else {
                            SetTimeEntry(inOut, null);
                        }

                        break;

                    default:
                        SetTimeEntry(inOut, null);
                }

            });

            //if (navigator.geolocation) {


            //    navigator.geolocation.getCurrentPosition((position) => {
            //        SetTimeEntry(inOut, position);
            //    }, (error) => {
            //        SetTimeEntry(inOut, null);
            //    });
            //}
            //else {
            //    SetTimeEntry(inOut, null);
            //}
        }

        $('#TimeInBtn').click(function () {
            if (!_$timeEntryInformationForm.valid()) {
                return;
            }

            TimeInOut('I');
        });

        $('#TimeOutBtn').click(function () {
            if (!_$timeEntryInformationForm.valid()) {
                return;
            }

            TimeInOut('O');
        });

        $('#EntriesBtn').click(function () {
            if (!_$timeEntryInformationForm.valid()) {
                return;
            }

            var _entriesModal = new app.ModalManager({
                viewUrl: abp.appPath + 'Account/EntriesModal',
                modalClass: 'EntriesModal'
            });

            abp.ui.setBusy();
            _entriesModal.open({
                userNameOrEmailAddress: $('input[name=usernameOrEmailAddress]').val()
            });
        });

    });
})();