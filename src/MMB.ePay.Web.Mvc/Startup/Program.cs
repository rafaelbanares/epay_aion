﻿using Microsoft.AspNetCore.Hosting;
using MMB.ePay.Web.Helpers;
using System.IO;
using System.Security.Authentication;

namespace MMB.ePay.Web.Startup
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CurrentDirectoryHelpers.SetCurrentDirectory();
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return new WebHostBuilder()
                .UseKestrel(opt =>
                {
                    opt.AddServerHeader = false;
                    opt.Limits.MaxRequestLineSize = 16 * 1024;
                    opt.ConfigureHttpsDefaults(s =>
                    {
                        s.SslProtocols = SslProtocols.Tls13;
                    });
                })
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIIS()
                .UseIISIntegration()
                .UseStartup<Startup>();
        }
    }
}
