﻿using Hangfire;
using Hangfire.Storage;
using MMB.ePay.Authorization.Users;
using MMB.ePay.Timekeeping;
using System;
using System.Linq;

namespace MMB.ePay.Web.Startup.Jobs
{
    public class HangfireScheduler
    {
        public static void ScheduleJobs()
        {
            var recurringJobs = JobStorage.Current.GetConnection().GetRecurringJobs();

            //Automated Email
            if (!recurringJobs.Any(f => f.Id == "AutomatedEmail"))
            {
                RecurringJob.AddOrUpdate<IUserEmailer>
                ("AutomatedEmail", x =>
                    x.AutomatedEmail(),
                    "*/30 * * * * ", //Every 30 minutes
                    TimeZoneInfo.Utc);
            }

            //Delete Email except last month
            if (!recurringJobs.Any(f => f.Id == "DeleteEmail"))
            {

                //RecurringJob.AddOrUpdate<IStoredProceduresAppService>
                //    ("DeleteEmail", x =>
                //        x.DeleteEmail(),
                //        "0 0 1 * *", //Every month
                //        TimeZoneInfo.Utc);

                RecurringJob.AddOrUpdate<IStoredProceduresAppService>
                ("DeleteEmail", x =>
                    x.DeleteEmail(),
                    "0 0 1 */3 *", //Every 3 months
                    TimeZoneInfo.Utc);
            }

            //Delete AuditLogs except last 3 months
            if (!recurringJobs.Any(f => f.Id == "DeleteAuditLogs"))
            {
                RecurringJob.AddOrUpdate<IStoredProceduresAppService>
                ("DeleteAuditLogs", x =>
                    x.DeleteAuditLogs(),
                    "0 0 1 */3 *", //Every 3 months
                    TimeZoneInfo.Utc);
            }
        }
    }
}
