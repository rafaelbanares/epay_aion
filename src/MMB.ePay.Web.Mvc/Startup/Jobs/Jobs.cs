﻿using Abp.BackgroundJobs;
using MMB.ePay.Timekeeping;

namespace MMB.ePay.Web.Startup.Jobs
{
    public class Jobs
    {
    }

    public class TestJob : BackgroundJob<int>
    {
        public override void Execute(int number)
        {
            

            Logger.Debug(number.ToString());
        }
    }
}
