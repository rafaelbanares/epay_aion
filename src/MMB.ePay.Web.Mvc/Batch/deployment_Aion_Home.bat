@echo off
setlocal enabledelayedexpansion

set file=C:\Users\Rafael\source\repos\Aion\src\MMB.ePay.Core\AppVersionHelper.cs

(for /L %%i in (1,1,17) do set /P "version=") < %file%

set "versionLength=0"

for /L %%A in (0,1,9999) do (
    set "char=!version:~%%A,1!"
    if "!char!" neq "" (
        set /a "versionLength+=1"
    ) else (
        goto :done
    )
)

:done

set /a "versionLength-=43"
set /a "finalLength=versionLength-2"

set versionNo=!version:~-%versionLength%,%finalLength%!
set /a versionNo+=1
set finalStr=        public const string Version = "1.1.%versionNo%";

echo !versionNo!>temp.txt

<"!file!" >"!file!.~tmp" (
  for /f %%i in ('type "!file!"^|find /c /v ""') do for /l %%j in (1 1 %%i) do (
    set "line=" &set /p "line="

    if "!line:~8,27!"=="public const string Version" (
      echo(%finalStr%
    ) else (
      echo(!line!)
  )
)
>nul move /y "!file!.~tmp" "!file!"
endlocal

set /p "versionNo=" < temp.txt
del temp.txt

echo Version updated.
pause

set aion=C:\Users\Rafael\source\repos\Aion
set msBuild=C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\amd64
set solution=C:\Users\Rafael\source\repos\Aion\MMB.ePay.Web.sln
set pubxml=%aion%\src\MMB.ePay.Web.Mvc\Properties\PublishProfiles\FolderProfile1.pubxml
set vsVersion=16.11.24

cls
cd %msBuild%
msbuild.exe %solution% /p:DeployOnBuild=true /p:PublishProfile=%pubxml% /p:AllowUntrustedCertificate=true /p:VisualStudioVersion=%vsVersion%
pause


set destination=C:\Users\Rafael\Desktop
set publish=%aion%\src\MMB.ePay.Web.Mvc\bin\Release\net5.0\publish
set scripts=%aion%\src\MMB.ePay.Application.Shared\Scripts\One-Time Script\ForDeployment
set deploy-project=%destination%\Deploy\Aion
set deploy-scripts=%destination%\Deploy\Scripts


robocopy "%publish%" "%deploy-project%" /E
robocopy "%scripts%" "%deploy-scripts%" /E

del %destination%\Deploy\Aion\web.config
del %destination%\Deploy\Aion\app.config
del %destination%\Deploy\Aion\appsettings.json
del %destination%\Deploy\Aion\appsettings.Production.json
del %destination%\Deploy\Aion\appsettings.Staging.json

echo Deployment completed.
pause


set sevenZip=C:\Program Files\7-Zip\7z.exe
for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"
set "timestamp=%YY%%MM%%DD%_%HH%%Min%"

for /f "tokens=3,2,4 delims=/- " %%x in ("%date%") do set d=%%y%%x%%z
set data=%d%
echo zipping...

"%sevenZip%" a -tzip "%destination%\Aion_v1.1.%versionNo%_%timestamp%.zip" "%destination%\Deploy"

echo Zip completed.
pause