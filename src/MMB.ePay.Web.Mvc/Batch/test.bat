@echo off
setlocal enabledelayedexpansion

set file=C:\Users\Rafael\source\repos\Aion\src\MMB.ePay.Core\AppVersionHelper.cs

(for /L %%i in (1,1,17) do set /P "version=") < %file%

set "versionLength=0"

for /L %%A in (0,1,9999) do (
    set "char=!version:~%%A,1!"
    if "!char!" neq "" (
        set /a "versionLength+=1"
    ) else (
        goto :done
    )
)

:done

set /a "versionLength-=43"
set /a "finalLength=versionLength-2"

set versionNo=!version:~-%versionLength%,%finalLength%!
set /a versionNo+=1
set finalStr=        public const string Version = "1.1.%versionNo%";

echo !versionNo! > temp.txt

<"!file!" >"!file!.~tmp" (
  for /f %%i in ('type "!file!"^|find /c /v ""') do for /l %%j in (1 1 %%i) do (
    set "line=" &set /p "line="

    if "!line:~8,27!"=="public const string Version" (
      echo(%finalStr%
    ) else (
      echo(!line!)
  )
)
>nul move /y "!file!.~tmp" "!file!"
endlocal

set /p "versionNo=" < temp.txt
del temp.txt

echo %versionNo%
echo Version updated.
pause