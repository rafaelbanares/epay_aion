﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace MMB.ePay.Web.Views
{
    public abstract class ePayViewComponent : AbpViewComponent
    {
        protected ePayViewComponent()
        {
            LocalizationSourceName = ePayConsts.LocalizationSourceName;
        }
    }
}