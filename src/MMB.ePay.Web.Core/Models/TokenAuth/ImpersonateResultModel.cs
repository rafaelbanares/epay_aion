namespace MMB.ePay.Web.Models.TokenAuth
{
    public class ImpersonateResultModel
    {
        public string ImpersonationToken { get; set; }
    }
}