﻿using Microsoft.AspNetCore.Mvc;
using MMB.ePay.Web.Controllers;

namespace MMB.ePay.Web.Public.Controllers
{
    public class AboutController : ePayControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}