﻿using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace MMB.ePay.Web.Public.Views
{
    public abstract class ePayRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected ePayRazorPage()
        {
            LocalizationSourceName = ePayConsts.LocalizationSourceName;
        }
    }
}
