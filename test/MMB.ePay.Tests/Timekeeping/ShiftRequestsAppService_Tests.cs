﻿using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using Shouldly;
using System;
using System.Linq;
using Xunit;

namespace MMB.ePay.Tests.Timekeeping
{
    public class ShiftRequestsAppService_Tests : AppTestBase
    {
        private readonly IShiftRequestsAppService _shiftRequestsAppService;

        public ShiftRequestsAppService_Tests()
        {
            _shiftRequestsAppService = Resolve<IShiftRequestsAppService>();
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_Without_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _shiftRequestsAppService.GetAll(new GetAllShiftRequestsInput());
            requests.Result.TotalCount.ShouldBe(9);
        }

        //[Fact]
        //public void Should_Get_Default_Admin_Requests_With_Date_Filter()
        //{
        //    LoginAsTenant("Default", "admin");

        //    var requests = _shiftRequestsAppService.GetAll(new GetAllShiftRequestsInput
        //    {
        //        MinShiftDateFilter = new DateTime(2022, 1, 1),
        //        MaxShiftDateFilter = new DateTime(2022, 12, 30),
        //    });
        //    requests.Result.TotalCount.ShouldBe(0);

        //    requests = _shiftRequestsAppService.GetAll(new GetAllShiftRequestsInput
        //    {
        //        MinShiftDateFilter = new DateTime(2021, 1, 1),
        //        MaxShiftDateFilter = new DateTime(2021, 12, 30),
        //    });
        //    requests.Result.TotalCount.ShouldBe(9);
        //}

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_New_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _shiftRequestsAppService.GetAll(new GetAllShiftRequestsInput
            {
                StatusIdFilter = 1
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_For_request_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _shiftRequestsAppService.GetAll(new GetAllShiftRequestsInput
            {
                StatusIdFilter = 2
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_For_Next_request_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _shiftRequestsAppService.GetAll(new GetAllShiftRequestsInput
            {
                StatusIdFilter = 3
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_Declined_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _shiftRequestsAppService.GetAll(new GetAllShiftRequestsInput
            {
                StatusIdFilter = 4
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_Cancelled_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _shiftRequestsAppService.GetAll(new GetAllShiftRequestsInput
            {
                StatusIdFilter = 5
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_Approved_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _shiftRequestsAppService.GetAll(new GetAllShiftRequestsInput
            {
                StatusIdFilter = 6
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_Reopened_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _shiftRequestsAppService.GetAll(new GetAllShiftRequestsInput
            {
                StatusIdFilter = 7
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_For_Update_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _shiftRequestsAppService.GetAll(new GetAllShiftRequestsInput
            {
                StatusIdFilter = 8
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_System_Approved_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _shiftRequestsAppService.GetAll(new GetAllShiftRequestsInput
            {
                StatusIdFilter = 9
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        //[Fact]
        //public void Should_Create_New_Default_Admin_Requests()
        //{
        //    LoginAsTenant("Default", "admin");

        //    var input = new CreateOrEditShiftRequestsDto
        //    {
        //        ShiftStart = new DateTime(2021, 1, 9),
        //        ShiftEnd = new DateTime(2021, 1, 9),
        //        ShiftTypeId = 1,
        //        Remarks = "test",
        //        ShiftReasonId = 1
        //    };

        //    _shiftRequestsAppService.CreateOrEdit(input);

        //    var request = UsingDbContext(e => e.ShiftRequests
        //        .Where(f => f.ShiftStart == new DateTime(2021, 1, 9) && f.EmployeeId == 1 && f.TenantId == 1)
        //        .FirstOrDefault());

        //    request.EmployeeId.ShouldBe(1);
        //    request.ShiftStart.ShouldBe(new DateTime(2021, 1, 9));
        //    request.ShiftEnd.ShouldBe(new DateTime(2021, 1, 9));
        //    request.ShiftTypeId.ShouldBe(1);
        //    request.Remarks.ShouldBe("test");
        //    request.ShiftReasonId.ShouldBe(1);
        //}

        //[Fact]
        //public void Should_Update_New_Default_Admin_Requests()
        //{
        //    LoginAsTenant("Default", "admin");

        //    var input = new CreateOrEditShiftRequestsDto
        //    {
        //        Id = 1,
        //        ShiftStart = new DateTime(2021, 1, 1),
        //        ShiftEnd = new DateTime(2021, 1, 10),
        //        ShiftTypeId = 2,
        //        Remarks = "test123",
        //        ShiftReasonId = 2
        //    };

        //    _shiftRequestsAppService.CreateOrEdit(input);

        //    var request = UsingDbContext(e => e.ShiftRequests.Where(f => f.Id == 1).FirstOrDefault());

        //    request.EmployeeId.ShouldBe(1);
        //    request.ShiftStart.ShouldBe(new DateTime(2021, 1, 1));
        //    request.ShiftEnd.ShouldBe(new DateTime(2021, 1, 10));
        //    request.ShiftTypeId.ShouldBe(2);
        //    request.Remarks.ShouldBe("test123");
        //    request.ShiftReasonId.ShouldBe(2);
        //}
    }
}