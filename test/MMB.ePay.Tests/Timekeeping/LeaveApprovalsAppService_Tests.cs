﻿using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using Shouldly;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace MMB.ePay.Tests.Timekeeping
{
    public class LeaveApprovalsAppService_Tests : AppTestBase
    {
        private readonly ILeaveRequestsAppService _leaveRequestsAppService;
        private readonly ILeaveApprovalsAppService _leaveApprovalsAppService;

        public LeaveApprovalsAppService_Tests()
        {
            _leaveRequestsAppService = Resolve<ILeaveRequestsAppService>();
            _leaveApprovalsAppService = Resolve<ILeaveApprovalsAppService>();
        }

        [Fact]
        public async Task Employee_Should_Update_Status_From_New_To_ForApproval()
        {
            LoginAsTenant("Default", "bill.williamson@default.com");

            var oldStatusId = UsingDbContext(e => e.Status
                 .Where(f => f.DisplayName == "New").Select(f => f.Id).First());

            var newStatusId = UsingDbContext(e => e.Status
                .Where(f => f.DisplayName == "For Approval").Select(f => f.Id).First());

            var requestId = UsingDbContext(e => e.LeaveRequests
                .Where(f => f.StatusId == oldStatusId && f.Employees.User.Id == AbpSession.UserId)
                .Select(f => f.Id).First());

            var input = new UpdateLeaveRequestsStatus
            {
                Id = requestId,
                StatusId = newStatusId,
            };

            await _leaveRequestsAppService.UpdateStatus(input);

            UsingDbContext(e => e.LeaveRequests
                .Where(f => f.Id == requestId).Select(f => f.StatusId).First())
                .ShouldBe(newStatusId);
        }

        [Fact]
        public async Task Approver1_Should_Update_Status_From_ForApproval_To_ForNextApproval()
        {
            LoginAsTenant("Default", "arthur.morgan@default.com");

            var oldStatusId = UsingDbContext(e => e.Status
                .Where(f => f.DisplayName == "For Approval").Select(f => f.Id).First());

            var newStatusId = UsingDbContext(e => e.Status
                 .Where(f => f.DisplayName == "For Next Approval").Select(f => f.Id).First());

            var employeeId = UsingDbContext(e => e.User
                .Where(f => f.UserName == "bill.williamson@default.com").Select(f => f.EmployeeId).First());

            var requestId = UsingDbContext(e => e.LeaveRequests
                .Where(f => f.StatusId == oldStatusId && f.EmployeeId == employeeId)
                .Select(e => e.Id).First());

            var input = new UpdateLeaveRequestsStatus
            {
                Id = requestId,
                StatusId = newStatusId,
            };

            await _leaveApprovalsAppService.UpdateStatus(input);

            UsingDbContext(e => e.LeaveRequests
                .Where(f => f.Id == requestId).Select(e => e.StatusId).First())
                .ShouldBe(newStatusId);
        }

        [Fact]
        public async Task Approver2_Should_Update_Status_From_ForNextApproval_To_Approved()
        {
            LoginAsTenant("Default", "dutch.vanderlinde@default.com");

            var oldStatusId = UsingDbContext(e => e.Status
                .Where(f => f.DisplayName == "For Next Approval").Select(f => f.Id).First());

            var newStatusId = UsingDbContext(e => e.Status
                 .Where(f => f.DisplayName == "Approved").Select(f => f.Id).First());

            var employeeId = UsingDbContext(e => e.User
                .Where(f => f.UserName == "bill.williamson@default.com").Select(f => f.EmployeeId).First());

            var requestId = UsingDbContext(e => e.LeaveRequests
                .Where(f => f.StatusId == oldStatusId && f.EmployeeId == employeeId)
                .Select(e => e.Id).First());

            var input = new UpdateLeaveRequestsStatus
            {
                Id = requestId,
                StatusId = newStatusId,
            };

            await _leaveApprovalsAppService.UpdateStatus(input);

            UsingDbContext(e => e.LeaveRequests
                .Where(f => f.Id == requestId).Select(e => e.StatusId).First())
                .ShouldBe(newStatusId);
        }

        //[Fact]
        //public void Should_Get_All_User_Approvals_Without_Any_Filter()
        //{
        //    LoginAsTenant("Default", "admin");

        //    var approvals = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput());
        //    approvals.Result.TotalCount.ShouldBe(9);
        //}

        //[Fact]
        //public void Should_Get_All_Fist_Main_Approver_Approvals_Without_Any_Filter()
        //{
        //    LoginAsTenant("Default", "remie.barnachea@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].LeaveApprovals.Status.ShouldBe("For Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Fist_Backup_Approver_Approvals_Without_Any_Filter()
        //{
        //    LoginAsTenant("Default", "may.villanueva@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].LeaveApprovals.Status.ShouldBe("For Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Second_Main_Approver_Approvals_Without_Any_Filter()
        //{
        //    LoginAsTenant("Default", "rodel.barnachea@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].LeaveApprovals.Status.ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Second_Backup_Approver_Approvals_Without_Any_Filter()
        //{
        //    LoginAsTenant("Default", "alma.mendoza@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].LeaveApprovals.Status.ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void User_Should_Approve_Request_Status_From_New_To_For_Approval()
        //{
        //    LoginAsTenant("Default", "admin");

        //    var newStatusId = UsingDbContext(e =>
        //        e.Status.Where(e => e.DisplayName == "New").Select(e => e.Id).First());

        //    var forApprovalStatusId = UsingDbContext(e =>
        //        e.Status.Where(e => e.DisplayName == "For Approval").Select(e => e.Id).First());

        //    var newRequests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
        //    { StatusIdFilter = newStatusId });

        //    var input = new UpdateLeaveRequestsStatus
        //    {
        //        Id = newRequests.Result.Items[0].LeaveRequests.Id,
        //        StatusId = forApprovalStatusId
        //    };

        //    _leaveRequestsAppService.UpdateStatus(input);

        //    var forApprovalRequests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
        //    { StatusIdFilter = forApprovalStatusId });

        //    forApprovalRequests.Result.TotalCount.ShouldBe(2);
        //    forApprovalRequests.Result.Items[0].LeaveRequests.Status.ShouldBe("For Approval");
        //    forApprovalRequests.Result.Items[1].LeaveRequests.Status.ShouldBe("For Approval");
        //}

        //[Fact]
        //public void First_Main_Approver_Should_Approve_Request_From_For_Approval_To_For_Next_Approval()
        //{
        //    LoginAsTenant("Default", "remie.barnachea@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    var id = approvals.Result.Items[0].LeaveApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "For Next Approval").Select(e => e.Id).First());

        //    var input = new UpdateLeaveRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _leaveApprovalsAppService.UpdateStatus(input);

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.LeaveRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void First_Backup_Approver_Should_Approve_Request_From_For_Approval_To_For_Next_Approval()
        //{
        //    LoginAsTenant("Default", "may.villanueva@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    var id = approvals.Result.Items[0].LeaveApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "For Next Approval").Select(e => e.Id).First());

        //    var input = new UpdateLeaveRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _leaveApprovalsAppService.UpdateStatus(input);

        //    approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.LeaveRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void Second_Main_Approver_Should_Approve_Request_From_For_Next_Approval_To_Approved()
        //{
        //    LoginAsTenant("Default", "rodel.barnachea@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    var id = approvals.Result.Items[0].LeaveApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "Approved").Select(e => e.Id).First());

        //    var input = new UpdateLeaveRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _leaveApprovalsAppService.UpdateStatus(input);

        //    approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.LeaveRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("Approved");
        //}

        //[Fact]
        //public void Second_Backup_Approver_Should_Approve_Request_From_For_Next_Approval_To_Approved()
        //{
        //    LoginAsTenant("Default", "alma.mendoza@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    var id = approvals.Result.Items[0].LeaveApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "Approved").Select(e => e.Id).First());

        //    var input = new UpdateLeaveRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _leaveApprovalsAppService.UpdateStatus(input);

        //    approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.LeaveRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("Approved");
        //}

        //[Fact]
        //public void Should_Get_All_User_Approvals_Without_Any_Filter2()
        //{
        //    LoginAsTenant("Default", "rafael.banares@mmbisolutions.com");

        //    var approvals = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput());
        //    approvals.Result.TotalCount.ShouldBe(9);
        //}

        //[Fact]
        //public void Should_Get_All_Fist_Main_Approver_Approvals_Without_Any_Filter2()
        //{
        //    LoginAsTenant("Default", "bonn.aguilar@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].LeaveApprovals.Status.ShouldBe("For Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Fist_Backup_Approver_Approvals_Without_Any_Filter2()
        //{
        //    LoginAsTenant("Default", "amielle.uno@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].LeaveApprovals.Status.ShouldBe("For Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Second_Main_Approver_Approvals_Without_Any_Filter2()
        //{
        //    LoginAsTenant("Default", "donna.belandres@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].LeaveApprovals.Status.ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Second_Backup_Approver_Approvals_Without_Any_Filter2()
        //{
        //    LoginAsTenant("Default", "joan.espiritu@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].LeaveApprovals.Status.ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void User_Should_Approve_Request_Status_From_New_To_For_Approval2()
        //{
        //    LoginAsTenant("Default", "rafael.banares@mmbisolutions.com");

        //    var newStatusId = UsingDbContext(e =>
        //        e.Status.Where(e => e.DisplayName == "New").Select(e => e.Id).First());

        //    var forApprovalStatusId = UsingDbContext(e =>
        //        e.Status.Where(e => e.DisplayName == "For Approval").Select(e => e.Id).First());

        //    var newRequests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
        //    { StatusIdFilter = newStatusId });

        //    var input = new UpdateLeaveRequestsStatus
        //    {
        //        Id = newRequests.Result.Items[0].LeaveRequests.Id,
        //        StatusId = forApprovalStatusId
        //    };

        //    _leaveRequestsAppService.UpdateStatus(input);


        //    var forApprovalRequests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
        //    { StatusIdFilter = forApprovalStatusId });

        //    forApprovalRequests.Result.TotalCount.ShouldBe(2);
        //    forApprovalRequests.Result.Items[0].LeaveRequests.Status.ShouldBe("For Approval");
        //    forApprovalRequests.Result.Items[1].LeaveRequests.Status.ShouldBe("For Approval");
        //}

        //[Fact]
        //public void First_Main_Approver_Should_Approve_Request_From_For_Approval_To_For_Next_Approval2()
        //{
        //    LoginAsTenant("Default", "bonn.aguilar@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    var id = approvals.Result.Items[0].LeaveApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "For Next Approval").Select(e => e.Id).First());

        //    var input = new UpdateLeaveRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _leaveApprovalsAppService.UpdateStatus(input);

        //    approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.LeaveRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void First_Backup_Approver_Should_Approve_Request_From_For_Approval_To_For_Next_Approval2()
        //{
        //    LoginAsTenant("Default", "amielle.uno@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    var id = approvals.Result.Items[0].LeaveApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "For Next Approval").Select(e => e.Id).First());

        //    var input = new UpdateLeaveRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _leaveApprovalsAppService.UpdateStatus(input);

        //    approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.LeaveRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void Second_Main_Approver_Should_Approve_Request_From_For_Next_Approval_To_Approved2()
        //{
        //    LoginAsTenant("Default", "donna.belandres@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    var id = approvals.Result.Items[0].LeaveApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "Approved").Select(e => e.Id).First());

        //    var input = new UpdateLeaveRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _leaveApprovalsAppService.UpdateStatus(input);

        //    approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.LeaveRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("Approved");
        //}

        //[Fact]
        //public void Second_Backup_Approver_Should_Approve_Request_From_For_Next_Approval_To_Approved2()
        //{
        //    LoginAsTenant("Default", "joan.espiritu@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    var id = approvals.Result.Items[0].LeaveApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "Approved").Select(e => e.Id).First());

        //    var input = new UpdateLeaveRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _leaveApprovalsAppService.UpdateStatus(input);

        //    approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.LeaveRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("Approved");
        //}

        //[Fact]
        //public void Should_Get_All_User_Approvals_Without_Any_Filter3()
        //{
        //    LoginAsTenant("MMB", "arthur.morgan@mmbisolutions.com");

        //    var approvals = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput());
        //    approvals.Result.TotalCount.ShouldBe(9);
        //}

        //[Fact]
        //public void Should_Get_All_Fist_Main_Approver_Approvals_Without_Any_Filter3()
        //{
        //    LoginAsTenant("MMB", "john.marston@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].LeaveApprovals.Status.ShouldBe("For Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Fist_Backup_Approver_Approvals_Without_Any_Filter3()
        //{
        //    LoginAsTenant("MMB", "dutch.vanderlinde@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].LeaveApprovals.Status.ShouldBe("For Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Second_Main_Approver_Approvals_Without_Any_Filter3()
        //{
        //    LoginAsTenant("MMB", "bessie.matthews@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].LeaveApprovals.Status.ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Second_Backup_Approver_Approvals_Without_Any_Filter3()
        //{
        //    LoginAsTenant("MMB", "bill.williamson@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].LeaveApprovals.Status.ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void User_Should_Approve_Request_Status_From_New_To_For_Approval3()
        //{
        //    LoginAsTenant("MMB", "arthur.morgan@mmbisolutions.com");

        //    var newStatusId = UsingDbContext(e =>
        //        e.Status.Where(e => e.DisplayName == "New").Select(e => e.Id).First());

        //    var forApprovalStatusId = UsingDbContext(e =>
        //        e.Status.Where(e => e.DisplayName == "For Approval").Select(e => e.Id).First());

        //    var newRequests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
        //    { StatusIdFilter = newStatusId });

        //    var input = new UpdateLeaveRequestsStatus
        //    {
        //        Id = newRequests.Result.Items[0].LeaveRequests.Id,
        //        StatusId = forApprovalStatusId
        //    };

        //    _leaveRequestsAppService.UpdateStatus(input);


        //    var forApprovalRequests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
        //    { StatusIdFilter = forApprovalStatusId });

        //    forApprovalRequests.Result.TotalCount.ShouldBe(3);
        //    forApprovalRequests.Result.Items[0].LeaveRequests.Status.ShouldBe("For Approval");
        //    forApprovalRequests.Result.Items[1].LeaveRequests.Status.ShouldBe("For Approval");
        //}

        //[Fact]
        //public void First_Main_Approver_Should_Approve_Request_From_For_Approval_To_For_Next_Approval3()
        //{
        //    LoginAsTenant("MMB", "john.marston@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    var id = approvals.Result.Items[0].LeaveApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "For Next Approval").Select(e => e.Id).First());

        //    var input = new UpdateLeaveRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _leaveApprovalsAppService.UpdateStatus(input);

        //    approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.LeaveRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void First_Backup_Approver_Should_Approve_Request_From_For_Approval_To_For_Next_Approval3()
        //{
        //    LoginAsTenant("MMB", "dutch.vanderlinde@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    var id = approvals.Result.Items[0].LeaveApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "For Next Approval").Select(e => e.Id).First());

        //    var input = new UpdateLeaveRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _leaveApprovalsAppService.UpdateStatus(input);

        //    approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.LeaveRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void Second_Main_Approver_Should_Approve_Request_From_For_Next_Approval_To_Approved3()
        //{
        //    LoginAsTenant("MMB", "bessie.matthews@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    var id = approvals.Result.Items[0].LeaveApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "Approved").Select(e => e.Id).First());

        //    var input = new UpdateLeaveRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _leaveApprovalsAppService.UpdateStatus(input);

        //    approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.LeaveRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("Approved");
        //}

        //[Fact]
        //public void Second_Backup_Approver_Should_Approve_Request_From_For_Next_Approval_To_Approved3()
        //{
        //    LoginAsTenant("MMB", "bill.williamson@mmbisolutions.com");

        //    var approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    var id = approvals.Result.Items[0].LeaveApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "Approved").Select(e => e.Id).First());

        //    var input = new UpdateLeaveRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _leaveApprovalsAppService.UpdateStatus(input);

        //    approvals = _leaveApprovalsAppService.GetAll(new GetAllLeaveApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.LeaveRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("Approved");
        //}
    }
}