﻿using Abp.Application.Services.Dto;
using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using Shouldly;
using System;
using System.Linq;
using Xunit;

namespace MMB.ePay.Tests.Timekeeping
{
    public class LeaveRequestsAppService_Tests : AppTestBase
    {
        private readonly ILeaveRequestsAppService _leaveRequestsAppService;

        public LeaveRequestsAppService_Tests()
        {
            _leaveRequestsAppService = Resolve<ILeaveRequestsAppService>();
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_Without_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput());
            requests.Result.TotalCount.ShouldBe(9);
        }

        //[Fact]
        //public void Should_Get_Default_Admin_Requests_With_Date_Filter()
        //{
        //    LoginAsTenant("Default", "admin");

        //    var requests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
        //    {
        //        MinLeaveDateFilter = new DateTime(2022, 1, 1),
        //        MaxLeaveDateFilter = new DateTime(2022, 12, 30),
        //    });
        //    requests.Result.TotalCount.ShouldBe(0);

        //    requests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
        //    {
        //        MinLeaveDateFilter = new DateTime(2021, 1, 1),
        //        MaxLeaveDateFilter = new DateTime(2021, 12, 30),
        //    });
        //    requests.Result.TotalCount.ShouldBe(9);
        //}

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_New_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
            {
                StatusIdFilter = 1
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_For_request_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
            {
                StatusIdFilter = 2
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_For_Next_request_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
            {
                StatusIdFilter = 3
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_Declined_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
            {
                StatusIdFilter = 4
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_Cancelled_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
            {
                StatusIdFilter = 5
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_Approved_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
            {
                StatusIdFilter = 6
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_Reopened_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
            {
                StatusIdFilter = 7
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_For_Update_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
            {
                StatusIdFilter = 8
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        [Fact]
        public void Should_Get_Default_Admin_Requests_With_System_Approved_Status_Filter()
        {
            LoginAsTenant("Default", "admin");

            var requests = _leaveRequestsAppService.GetAll(new GetAllLeaveRequestsInput
            {
                StatusIdFilter = 9
            });
            requests.Result.TotalCount.ShouldBe(1);
        }

        //[Fact]
        //public void Should_Create_New_Default_Admin_Requests()
        //{
        //    LoginAsTenant("Default", "admin");

        //    var input = new CreateOrEditLeaveRequestsDto
        //    {
        //        Date = new DateTime(2021, 1, 9),
        //        TimeIn = new DateTime(2021, 1, 9, 9, 0, 0),
        //        TimeOut = new DateTime(2021, 1, 9, 18, 0, 0),
        //        EarlyLogin = false,
        //        Remarks = "test",
        //        LeaveReasonId = 1
        //    };

        //    _leaveRequestsAppService.CreateOrEdit(input);

        //    var request = UsingDbContext(e => e.LeaveRequests
        //        .Where(f => f.Date == new DateTime(2021, 1, 9) && f.EmployeeId == 1 && f.TenantId == 1)
        //        .FirstOrDefault());

        //    request.EmployeeId.ShouldBe(1);
        //    request.Date.ShouldBe(new DateTime(2021, 1, 9));
        //    request.TimeIn.ShouldBe(new DateTime(2021, 1, 9, 9, 0, 0));
        //    request.TimeOut.ShouldBe(new DateTime(2021, 1, 9, 18, 0, 0));
        //    request.EarlyLogin.ShouldBe(false);
        //    request.Remarks.ShouldBe("test");
        //    request.LeaveReasonId.ShouldBe(1);
        //}

        //[Fact]
        //public void Should_Update_New_Default_Admin_Requests()
        //{
        //    LoginAsTenant("Default", "admin");

        //    var input = new CreateOrEditLeaveRequestsDto
        //    {
        //        Id = 1,
        //        Date = new DateTime(2021, 1, 1),
        //        TimeIn = new DateTime(2021, 1, 1, 10, 0, 0),
        //        TimeOut = new DateTime(2021, 1, 1, 19, 0, 0),
        //        EarlyLogin = false,
        //        Remarks = "test123",
        //        LeaveReasonId = 2
        //    };

        //    _leaveRequestsAppService.CreateOrEdit(input);

        //    var request = UsingDbContext(e => e.LeaveRequests.Where(f => f.Id == 1).FirstOrDefault());

        //    request.EmployeeId.ShouldBe(1);
        //    request.Date.ShouldBe(new DateTime(2021, 1, 1));
        //    request.TimeIn.ShouldBe(new DateTime(2021, 1, 1, 10, 0, 0));
        //    request.TimeOut.ShouldBe(new DateTime(2021, 1, 1, 19, 0, 0));
        //    request.EarlyLogin.ShouldBe(false);
        //    request.Remarks.ShouldBe("test123");
        //    request.LeaveReasonId.ShouldBe(2);
        //}
    }
}