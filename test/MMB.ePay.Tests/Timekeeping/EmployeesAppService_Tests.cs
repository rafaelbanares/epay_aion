﻿using MMB.ePay.Authorization.Users;
using MMB.ePay.HR;
using MMB.ePay.HR.Dtos;
using Shouldly;
using Xunit;

namespace MMB.ePay.Tests.Timekeeping
{
    public class EmployeesAppService_Tests : AppTestBase
    {
        private readonly IEmployeesAppService _employeesAppService;
        private readonly IUserAppService _userAppService;

        public EmployeesAppService_Tests()
        {
            _employeesAppService = Resolve<IEmployeesAppService>();
            _userAppService = Resolve<IUserAppService>();
        }

        [Fact]
        public void Should_Get_All_User_Approvals_Without_Any_Filter()
        {
            LoginAsDefaultTenantAdmin();

            //Act
            var employees = _employeesAppService.GetAll(new GetAllEmployeesInput());

            //Assert
            employees.Result.TotalCount.ShouldBe(12);
        }
    }
}
