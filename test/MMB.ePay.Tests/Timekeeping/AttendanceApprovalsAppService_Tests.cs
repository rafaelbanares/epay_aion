﻿using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using Shouldly;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace MMB.ePay.Tests.Timekeeping
{
    public class AttendanceApprovalsAppService_Tests : AppTestBase
    {
        private readonly IAttendanceRequestsAppService _attendanceRequestsAppService;
        private readonly IAttendanceApprovalsAppService _attendanceApprovalsAppService;

        public AttendanceApprovalsAppService_Tests()
        {
            _attendanceRequestsAppService = Resolve<IAttendanceRequestsAppService>();
            _attendanceApprovalsAppService = Resolve<IAttendanceApprovalsAppService>();
        }

        [Fact]
        public async Task Employee_Should_Update_Status_From_New_To_ForApproval()
        {
            LoginAsTenant("Default", "bill.williamson@default.com");

            var oldStatusId = UsingDbContext(e => e.Status
                 .Where(f => f.DisplayName == "New").Select(f => f.Id).First());

            var newStatusId = UsingDbContext(e => e.Status
                .Where(f => f.DisplayName == "For Approval").Select(f => f.Id).First());

            var requestId = UsingDbContext(e => e.AttendanceRequests
                .Where(f => f.StatusId == oldStatusId && f.Employees.User.Id == AbpSession.UserId)
                .Select(f => f.Id).First());

            var input = new UpdateAttendanceRequestsStatus
            {
                Id = requestId,
                StatusId = newStatusId,
            };

            await _attendanceRequestsAppService.UpdateStatus(input);

            UsingDbContext(e => e.AttendanceRequests
                .Where(f => f.Id == requestId).Select(f => f.StatusId).First())
                .ShouldBe(newStatusId);
        }

        [Fact]
        public async Task Approver1_Should_Update_Status_From_ForApproval_To_ForNextApproval()
        {
            LoginAsTenant("Default", "arthur.morgan@default.com");

            var oldStatusId = UsingDbContext(e => e.Status
                .Where(f => f.DisplayName == "For Approval").Select(f => f.Id).First());

            var newStatusId = UsingDbContext(e => e.Status
                 .Where(f => f.DisplayName == "For Next Approval").Select(f => f.Id).First());

            var employeeId = UsingDbContext(e => e.User
                .Where(f => f.UserName == "bill.williamson@default.com").Select(f => f.EmployeeId).First()); 

            var requestId = UsingDbContext(e => e.AttendanceRequests
                .Where(f => f.StatusId == oldStatusId && f.EmployeeId == employeeId)
                .Select(e => e.Id).First());

            var input = new UpdateAttendanceRequestsStatus
            {
                Id = requestId,
                StatusId = newStatusId,
            };

            await _attendanceApprovalsAppService.UpdateStatus(input);

            UsingDbContext(e => e.AttendanceRequests
                .Where(f => f.Id == requestId).Select(e => e.StatusId).First())
                .ShouldBe(newStatusId);
        }

        [Fact]
        public async Task Approver2_Should_Update_Status_From_ForNextApproval_To_Approved()
        {
            LoginAsTenant("Default", "dutch.vanderlinde@default.com");

            var oldStatusId = UsingDbContext(e => e.Status
                .Where(f => f.DisplayName == "For Next Approval").Select(f => f.Id).First());

            var newStatusId = UsingDbContext(e => e.Status
                 .Where(f => f.DisplayName == "Approved").Select(f => f.Id).First());

            var employeeId = UsingDbContext(e => e.User
                .Where(f => f.UserName == "bill.williamson@default.com").Select(f => f.EmployeeId).First());

            var requestId = UsingDbContext(e => e.AttendanceRequests
                .Where(f => f.StatusId == oldStatusId && f.EmployeeId == employeeId)
                .Select(e => e.Id).First());

            var input = new UpdateAttendanceRequestsStatus
            {
                Id = requestId,
                StatusId = newStatusId,
            };

            await _attendanceApprovalsAppService.UpdateStatus(input);

            UsingDbContext(e => e.AttendanceRequests
                .Where(f => f.Id == requestId).Select(e => e.StatusId).First())
                .ShouldBe(newStatusId);
        }

        //[Fact]
        //public void Should_Get_All_User_Approvals_Without_Any_Filter()
        //{
        //    LoginAsTenant("Default", "admin");

        //    var approvals = _attendanceRequestsAppService.GetAll(new GetAllAttendanceRequestsInput());
        //    approvals.Result.TotalCount.ShouldBe(9);
        //}

        //[Fact]
        //public void Should_Get_All_Fist_Main_Approver_Approvals_Without_Any_Filter()
        //{
        //    LoginAsTenant("Default", "remie.barnachea@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].AttendanceApprovals.Status.ShouldBe("For Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Fist_Backup_Approver_Approvals_Without_Any_Filter()
        //{
        //    LoginAsTenant("Default", "may.villanueva@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].AttendanceApprovals.Status.ShouldBe("For Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Second_Main_Approver_Approvals_Without_Any_Filter()
        //{
        //    LoginAsTenant("Default", "rodel.barnachea@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].AttendanceApprovals.Status.ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Second_Backup_Approver_Approvals_Without_Any_Filter()
        //{
        //    LoginAsTenant("Default", "alma.mendoza@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].AttendanceApprovals.Status.ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void First_Main_Approver_Should_Approve_Request_From_For_Approval_To_For_Next_Approval()
        //{
        //    LoginAsTenant("Default", "remie.barnachea@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    var id = approvals.Result.Items[0].AttendanceApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "For Next Approval").Select(e => e.Id).First());

        //    var input = new UpdateAttendanceRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _attendanceApprovalsAppService.UpdateStatus(input);

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.AttendanceRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void First_Backup_Approver_Should_Approve_Request_From_For_Approval_To_For_Next_Approval()
        //{
        //    LoginAsTenant("Default", "may.villanueva@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    var id = approvals.Result.Items[0].AttendanceApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "For Next Approval").Select(e => e.Id).First());

        //    var input = new UpdateAttendanceRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _attendanceApprovalsAppService.UpdateStatus(input);

        //    approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.AttendanceRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void Second_Main_Approver_Should_Approve_Request_From_For_Next_Approval_To_Approved()
        //{
        //    LoginAsTenant("Default", "rodel.barnachea@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    var id = approvals.Result.Items[0].AttendanceApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "Approved").Select(e => e.Id).First());

        //    var input = new UpdateAttendanceRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _attendanceApprovalsAppService.UpdateStatus(input);

        //    approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.AttendanceRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("Approved");
        //}

        //[Fact]
        //public void Second_Backup_Approver_Should_Approve_Request_From_For_Next_Approval_To_Approved()
        //{
        //    LoginAsTenant("Default", "alma.mendoza@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    var id = approvals.Result.Items[0].AttendanceApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "Approved").Select(e => e.Id).First());

        //    var input = new UpdateAttendanceRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _attendanceApprovalsAppService.UpdateStatus(input);

        //    approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.AttendanceRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("Approved");
        //}

        //[Fact]
        //public void Should_Get_All_User_Approvals_Without_Any_Filter2()
        //{
        //    LoginAsTenant("Default", "rafael.banares@mmbisolutions.com");

        //    var approvals = _attendanceRequestsAppService.GetAll(new GetAllAttendanceRequestsInput());
        //    approvals.Result.TotalCount.ShouldBe(9);
        //}

        //[Fact]
        //public void Should_Get_All_Fist_Main_Approver_Approvals_Without_Any_Filter2()
        //{
        //    LoginAsTenant("Default", "bonn.aguilar@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].AttendanceApprovals.Status.ShouldBe("For Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Fist_Backup_Approver_Approvals_Without_Any_Filter2()
        //{
        //    LoginAsTenant("Default", "amielle.uno@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].AttendanceApprovals.Status.ShouldBe("For Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Second_Main_Approver_Approvals_Without_Any_Filter2()
        //{
        //    LoginAsTenant("Default", "donna.belandres@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].AttendanceApprovals.Status.ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Second_Backup_Approver_Approvals_Without_Any_Filter2()
        //{
        //    LoginAsTenant("Default", "joan.espiritu@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].AttendanceApprovals.Status.ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void First_Main_Approver_Should_Approve_Request_From_For_Approval_To_For_Next_Approval2()
        //{
        //    LoginAsTenant("Default", "bonn.aguilar@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    var id = approvals.Result.Items[0].AttendanceApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "For Next Approval").Select(e => e.Id).First());

        //    var input = new UpdateAttendanceRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _attendanceApprovalsAppService.UpdateStatus(input);

        //    approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.AttendanceRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void First_Backup_Approver_Should_Approve_Request_From_For_Approval_To_For_Next_Approval2()
        //{
        //    LoginAsTenant("Default", "amielle.uno@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    var id = approvals.Result.Items[0].AttendanceApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "For Next Approval").Select(e => e.Id).First());

        //    var input = new UpdateAttendanceRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _attendanceApprovalsAppService.UpdateStatus(input);

        //    approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.AttendanceRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void Second_Main_Approver_Should_Approve_Request_From_For_Next_Approval_To_Approved2()
        //{
        //    LoginAsTenant("Default", "donna.belandres@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    var id = approvals.Result.Items[0].AttendanceApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "Approved").Select(e => e.Id).First());

        //    var input = new UpdateAttendanceRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _attendanceApprovalsAppService.UpdateStatus(input);

        //    approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.AttendanceRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("Approved");
        //}

        //[Fact]
        //public void Second_Backup_Approver_Should_Approve_Request_From_For_Next_Approval_To_Approved2()
        //{
        //    LoginAsTenant("Default", "joan.espiritu@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    var id = approvals.Result.Items[0].AttendanceApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "Approved").Select(e => e.Id).First());

        //    var input = new UpdateAttendanceRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _attendanceApprovalsAppService.UpdateStatus(input);

        //    approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.AttendanceRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("Approved");
        //}

        //[Fact]
        //public void Should_Get_All_User_Approvals_Without_Any_Filter3()
        //{
        //    LoginAsTenant("MMB", "arthur.morgan@mmbisolutions.com");

        //    var approvals = _attendanceRequestsAppService.GetAll(new GetAllAttendanceRequestsInput());
        //    approvals.Result.TotalCount.ShouldBe(9);
        //}

        //[Fact]
        //public void Should_Get_All_Fist_Main_Approver_Approvals_Without_Any_Filter3()
        //{
        //    LoginAsTenant("MMB", "john.marston@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].AttendanceApprovals.Status.ShouldBe("For Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Fist_Backup_Approver_Approvals_Without_Any_Filter3()
        //{
        //    LoginAsTenant("MMB", "dutch.vanderlinde@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].AttendanceApprovals.Status.ShouldBe("For Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Second_Main_Approver_Approvals_Without_Any_Filter3()
        //{
        //    LoginAsTenant("MMB", "bessie.matthews@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].AttendanceApprovals.Status.ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void Should_Get_All_Second_Backup_Approver_Approvals_Without_Any_Filter3()
        //{
        //    LoginAsTenant("MMB", "bill.williamson@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(1);
        //    approvals.Result.Items[0].AttendanceApprovals.Status.ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void First_Main_Approver_Should_Approve_Request_From_For_Approval_To_For_Next_Approval3()
        //{
        //    LoginAsTenant("MMB", "john.marston@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    var id = approvals.Result.Items[0].AttendanceApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "For Next Approval").Select(e => e.Id).First());

        //    var input = new UpdateAttendanceRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _attendanceApprovalsAppService.UpdateStatus(input);

        //    approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.AttendanceRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void First_Backup_Approver_Should_Approve_Request_From_For_Approval_To_For_Next_Approval3()
        //{
        //    LoginAsTenant("MMB", "dutch.vanderlinde@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    var id = approvals.Result.Items[0].AttendanceApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "For Next Approval").Select(e => e.Id).First());

        //    var input = new UpdateAttendanceRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _attendanceApprovalsAppService.UpdateStatus(input);

        //    approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.AttendanceRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("For Next Approval");
        //}

        //[Fact]
        //public void Second_Main_Approver_Should_Approve_Request_From_For_Next_Approval_To_Approved3()
        //{
        //    LoginAsTenant("MMB", "bessie.matthews@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    var id = approvals.Result.Items[0].AttendanceApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "Approved").Select(e => e.Id).First());

        //    var input = new UpdateAttendanceRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _attendanceApprovalsAppService.UpdateStatus(input);

        //    approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = false
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.AttendanceRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("Approved");
        //}

        //[Fact]
        //public void Second_Backup_Approver_Should_Approve_Request_From_For_Next_Approval_To_Approved3()
        //{
        //    LoginAsTenant("MMB", "bill.williamson@mmbisolutions.com");

        //    var approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    var id = approvals.Result.Items[0].AttendanceApprovals.Id;
        //    var statusId = UsingDbContext(e => e.Status.Where(e => e.DisplayName == "Approved").Select(e => e.Id).First());

        //    var input = new UpdateAttendanceRequestsStatus
        //    {
        //        Id = id,
        //        StatusId = statusId
        //    };

        //    _attendanceApprovalsAppService.UpdateStatus(input);

        //    approvals = _attendanceApprovalsAppService.GetAll(new GetAllAttendanceApprovalsInput
        //    {
        //        IsBackup = true
        //    });

        //    approvals.Result.TotalCount.ShouldBe(0);
        //    UsingDbContext(e => e.AttendanceRequests.Where(f => f.Id == id).Select(f => f.Status.DisplayName).FirstOrDefault())
        //        .ShouldBe("Approved");
        //}
    }
}