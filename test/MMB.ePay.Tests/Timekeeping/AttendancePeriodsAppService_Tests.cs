﻿using MMB.ePay.Timekeeping;
using MMB.ePay.Timekeeping.Dtos;
using Shouldly;
using Xunit;

namespace MMB.ePay.Tests.Timekeeping
{
    public class AttendancePeriodsAppService_Tests : AppTestBase
    {
        private readonly IAttendancePeriodsAppService _attendancePeriodsAppService;

        public AttendancePeriodsAppService_Tests()
        {
            _attendancePeriodsAppService = Resolve<IAttendancePeriodsAppService>();
        }


        [Fact]
        public void Should_Get_All_Attendance_Periods_Without_Any_Filter()
        {
            LoginAsTenant("Default", "admin");

            //Act
            var attendancePeriods = _attendancePeriodsAppService.GetAll(new GetAllAttendancePeriodsInput());

            //Assert
            attendancePeriods.Result.TotalCount.ShouldBe(25);
        }
    }
}
