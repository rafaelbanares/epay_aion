﻿using System.Threading.Tasks;
using MMB.ePay.Security.Recaptcha;

namespace MMB.ePay.Test.Base.Web
{
    public class FakeRecaptchaValidator : IRecaptchaValidator
    {
        public Task ValidateAsync(string captchaResponse)
        {
            return Task.CompletedTask;
        }
    }
}
